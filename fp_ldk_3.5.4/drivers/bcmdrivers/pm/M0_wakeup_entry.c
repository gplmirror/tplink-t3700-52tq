/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 * !!! DO NOT ADD ANY OTHER FUNCTIONS IN THIS FILE !!!
 */
#include "socregs.h"
#include "M0.h"

int MCU_SoC_Wakeup_Handler(uint32_t code0, uint32_t code1);

//
// !!! DO NOT CHANGE THE NAME OF THIS ROUTINE, IT IS REFERRED IN MAKEFILE !!!
//
/* Uniform user handler for wakeup interrupts: aon gpio, crmu rtc alarm, crmu wdog*/
int user_wakeup_handler(void)
{
	int ret = 0;
	uint32_t code0 = cpu_reg32_rd(IPROC_CRMU_MAIL_BOX0);
	uint32_t code1 = cpu_reg32_rd(IPROC_CRMU_MAIL_BOX1);

	MCU_set_wakeup_aon_gpio_output(1);
	
	MCU_led_print_hex(0x27);

	// CRMU timer intr
	if(cpu_reg32_getbit(CRMU_MCU_INTR_STATUS, CRMU_MCU_INTR_STATUS__MCU_TIMER_INTR))
	{
		MCU_led_print_hex(0x1);

		cpu_reg32_setbit(CRMU_TIM_TIMER1IntClr, CRMU_TIM_TIMER1IntClr__Intclr); //Write clear the interrupt from Timer 1 (Write Only)
		if((MCU_WAKEUP_SOURCE & iproc_bit_mask(MCU_TIMER_INTR)))
			goto wakeup;
	}
	// CRMU wdog intr
	if(cpu_reg32_getbit(CRMU_MCU_INTR_STATUS, CRMU_MCU_INTR_STATUS__MCU_TIMER_INTR))
	{
		MCU_led_print_hex(0x2);

		cpu_reg32_wr(CRMU_WDT_WDOGLOCK, 0x1ACCE551);//enable write other crmu wdog regs
		cpu_reg32_wr(CRMU_WDT_WDOGINTCLR, 1);
		cpu_reg32_wr(CRMU_WDT_WDOGLOAD, 0xFFFFFFFF);

		if((MCU_WAKEUP_SOURCE & iproc_bit_mask(MCU_WDOG_INTR)))
			goto wakeup;
	}
	// AON GPIO intr from valid src
	if(cpu_reg32_getbit(CRMU_MCU_INTR_STATUS, CRMU_MCU_INTR_STATUS__MCU_AON_GPIO_INTR)
		&& cpu_reg32_getbit(GP_INT_MSTAT, IPROC_MCU_AON_GPIO_WAKEUP_SRC))
	{
		MCU_led_print_hex(0x3);

		cpu_reg32_setbit(GP_INT_CLR, IPROC_MCU_AON_GPIO_WAKEUP_SRC);//AON GPIO interrupt clear register. Writing a 1 to any bit in this register clears the corresponding interrupt signal. Writing a 0 has no effect. 

		if((MCU_WAKEUP_SOURCE & iproc_bit_mask(MCU_AON_GPIO_INTR)))
			goto wakeup;
	}
	// SPRU RTC intr
	if(cpu_reg32_getbit(CRMU_MCU_EVENT_STATUS, CRMU_MCU_EVENT_STATUS__MCU_SPRU_RTC_EVENT))
	{
		MCU_led_print_hex(0x4);

		// BBL regs are indirect access, don't directly access them!
		// read clear BBL_INTERRUPT_clr
		cpu_reg32_setbit(CRMU_MCU_EVENT_CLEAR, CRMU_MCU_EVENT_CLEAR__MCU_SPRU_RTC_EVENT_CLR);

		if((MCU_WAKEUP_SOURCE & iproc_bit_mask(MCU_SPRU_RTC_EVENT)))
			goto wakeup;
	}

	MCU_led_print_hex(0x5);

	return 0;

wakeup:
	MCU_led_print_hex(0x11);
	
	ret = MCU_SoC_Wakeup_Handler(code0, code1);

	// Clear mailbox values, this will trigger a mbox interrupt again
	if(ret==0)
	{
		MCU_led_print_hex(0x13);
		
//		cpu_reg32_wr(IPROC_CRMU_MAIL_BOX0, 0x0);
//		cpu_reg32_wr(IPROC_CRMU_MAIL_BOX1, 0x0);
//		cpu_reg32_setbit(CRMU_MCU_EVENT_CLEAR, CRMU_MCU_EVENT_CLEAR__MCU_MAILBOX_EVENT_CLR); //only clear MBOX event
	}

	MCU_led_print_hex(0x14);

	MCU_led_on(IPROC_AON_GPIO_LED_ALL_ON);
	MCU_led_on(IPROC_AON_GPIO_LED_ALL_OFF);

	cpu_reg32_wr(GP_INT_CLR, 0x3f);//just clear aon gpio intr in case triggered by this code

	return 0;
}
