/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef _IPROC_PM_DEVICE_H
#define _IPROC_PM_DEVICE_H

struct iproc_pm_reg {
	void __iomem  *regaddr;
	unsigned long regval;
};

struct iproc_pm_device_regs{
	char *devname;
	struct iproc_pm_reg (*regs)[];
	int regs_num;
	unsigned int (*read)(const unsigned int );
	void (*write)(const unsigned int, const unsigned int);
};

#ifndef IPROC_SAVEREG
#define IPROC_SAVEREG(x) {.regaddr = (void __iomem *)x, .regval = 0}
#endif

extern void iproc_pm_save_device_regs(struct iproc_pm_device_regs *pm_dev);
extern void iproc_pm_restore_device_regs(struct iproc_pm_device_regs *pm_dev);



#endif //_IPROC_PM_DEVICE_H
