/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <linux/pm.h>
#include <linux/suspend.h>
#include <linux/proc_fs.h>
#include <linux/sysfs.h>
#include <linux/delay.h>
#include <linux/kmod.h>

#include "iproc_pm.h"

////////////////////////////////////////////////////////////////////////////////////
static void iproc_pm_wait_gmac_stable(void)
{
	#define iproc_reg_bit0(addr) (iproc_reg32_read(addr) & 0x1)

	iproc_dbg("\n");

	iproc_reg32_write(AMAC_IDM0_IO_CONTROL_DIRECT, iproc_reg32_read(AMAC_IDM0_IO_CONTROL_DIRECT)|0x1);
	while(!iproc_reg_bit0(AMAC_IDM0_IO_CONTROL_DIRECT));//clk on
	iproc_dbg("\n");

	iproc_reg32_write(AMAC_IDM0_IDM_RESET_CONTROL, 0);
	while(iproc_reg_bit0(AMAC_IDM0_IDM_RESET_CONTROL));//reset clear
	iproc_dbg("\n");

	iproc_reg32_write(AMAC_IDM1_IO_CONTROL_DIRECT, iproc_reg32_read(AMAC_IDM1_IO_CONTROL_DIRECT)|0x1);
	while(!iproc_reg_bit0(AMAC_IDM1_IO_CONTROL_DIRECT));//clk on
	iproc_dbg("\n");

	iproc_reg32_write(AMAC_IDM1_IDM_RESET_CONTROL, 0);
	while(iproc_reg_bit0(AMAC_IDM1_IDM_RESET_CONTROL));//reset clear
	iproc_dbg("\n");
	
	while(!iproc_reg_bit0(CRMU_PWR_GOOD_STATUS));//pwr stable
	iproc_dbg("\n");
}

/*
 * This must be called before suspend_freeze_processes()
 * So, don't call it in bcm5301x_gmac_drv_suspend()
 */
static int iproc_pm_set_gmac_updown(int en)
{
	int ret=0;
	
#if defined(CONFIG_IPROC_GMAC) || defined(CONFIG_IPROC_GMAC_MODULE)
	volatile int temp=1;
	char *app_name="/sbin/ifconfig";
	char *argv[] = {app_name, "eth0", "", NULL};
	char *envp[] = {"HOME=/", "TERM=linux", "PATH=/sbin:/usr/sbin:/bin:/usr/bin", NULL};

	iproc_dbg("\n");

	argv[2] = en ? "up" : "down";

	if(en)
	{
		iproc_pm_wait_gmac_stable();
	}

	ret = call_usermodehelper(app_name, argv, envp, UMH_WAIT_PROC);
	if(ret)
	{
		iproc_err("Failed to execute `%s %s %s', ret=%d!\n", argv[0], argv[1], argv[2], ret);
	}
	else
	{
		iproc_dbg("Success to execute `%s %s %s'\n", argv[0], argv[1], argv[2]);
	}
#endif

	return ret;	
}

static int iproc_pm_notifier_fn(struct notifier_block *notifier, unsigned long pm_event, void *unused)
{
	iproc_dbg("\n");
	
	switch (pm_event)
	{
		case PM_SUSPEND_PREPARE:/*Executed before system freezone, add you callback here*/
			iproc_pm_set_gmac_updown(0);
			break;
		case PM_POST_SUSPEND:
			iproc_pm_set_gmac_updown(1);
			break;
	}

	return NOTIFY_DONE;
}

////////////////////////////////////////////////////////////////////////////////////////////
static int iproc_pm_valid(suspend_state_t pm_state)
{
	return  (pm_state == PM_SUSPEND_STANDBY) || (pm_state == PM_SUSPEND_MEM);
}

static int iproc_pm_prepare(void)/*Save the current PM state*/
{
	int ret=0;

	iproc_dbg("\n");

	return ret;
}

static int iproc_pm_enter(suspend_state_t state)
{
	int ret=0;
	iproc_power_status_e cur_stat = iproc_get_current_pm_state();

	iproc_dbg("\n");
	
	switch (state)
	{
		case PM_SUSPEND_STANDBY: // this is mapped to SoC SLEEP
			iproc_dbg("Entering PM_SUSPEND_STANDBY\n");
			ret = iproc_soc_enter_sleep();
			iproc_dbg("Leaving PM_SUSPEND_STANDBY\n");
			break;
		case PM_SUSPEND_MEM: // this is mapped to SoC DEEPSLEEP
			iproc_dbg("Entering PM_SUSPEND_MEM\n");
			ret = iproc_soc_enter_deepsleep();
			iproc_dbg("Leaving PM_SUSPEND_MEM\n");
			break;
		default:
			ret = -EINVAL;
	}
	
	return ret;
}

static void iproc_pm_finish(void)
{
	iproc_dbg("\n");
	
	return ;
}

////////////////////////////////////////////////////////////////////////////////
static struct platform_suspend_ops iproc_pm_ops ={
	.valid   = iproc_pm_valid,
	.prepare = iproc_pm_prepare,
	.enter   = iproc_pm_enter,
	.finish  = iproc_pm_finish,
};

int iproc_suspend_init(void)
{
	iproc_prt("Enable suspend support for this machine!\n");
	
	suspend_set_ops(&iproc_pm_ops);

	pm_notifier(iproc_pm_notifier_fn, 0);

	return 0;
}

void iproc_suspend_exit(void)
{
	iproc_prt("Disable suspend support for this machine!\n");

	suspend_set_ops(NULL);
	
	return;
}
