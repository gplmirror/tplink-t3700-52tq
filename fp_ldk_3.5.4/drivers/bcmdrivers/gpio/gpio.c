/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
 
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/version.h>

#include <mach/iproc_regs.h>
#include <mach/memory.h>


#include "gpio.h"


#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 37)
#define irq_get_chip_data get_irq_chip_data
#define irq_set_chip_data set_irq_chip_data
#define  irq_set_chip set_irq_chip
#define irq_set_handler set_irq_handler
#define status_use_accessors status
#endif


static struct iproc_gpio_chip *iproc_gpio_dev[MAX_NS_GPIO] = {};
static int dev = 0;

static unsigned int _iproc_gpio_readl(struct iproc_gpio_chip *chip, int reg)
{
    return readl(chip->ioaddr + reg);
}

static void _iproc_gpio_writel(struct iproc_gpio_chip *chip, unsigned int val, int reg)
{
	writel(val, chip->ioaddr + reg);
}


/*
@ pin : the actual pin number of the gpiochip
*/
static int iproc_gpio_to_irq(struct iproc_gpio_chip *chip, unsigned int pin) {
    return (chip->irq_base + pin - chip->pin_offset);
}

/*
returns the actual pin number of the gpiochip
*/
static int iproc_irq_to_gpio(struct iproc_gpio_chip *chip, unsigned int irq) {
    return (irq - chip->irq_base + chip->pin_offset);
}
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 6, 5)
static void iproc_gpio_irq_ack(unsigned int irq)
{
#else
static void iproc_gpio_irq_ack(struct irq_data *d)
{
    unsigned int irq = d->irq;
#endif
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);

    if (ourchip) {
    	struct iproc_gpio_irqcfg *irqcfg = ourchip->irqcfg;
	if (irqcfg && irqcfg->ack)
	    irqcfg->ack(irq);
		
    }    
}
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 6, 5)
static void iproc_gpio_irq_unmask(unsigned int irq)
{
#else
static void iproc_gpio_irq_unmask(struct irq_data *d)
{
    unsigned int irq = d->irq;
#endif
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);

    if (ourchip) {
    	struct iproc_gpio_irqcfg *irqcfg = ourchip->irqcfg;
	if (irqcfg && irqcfg->unmask)
	    irqcfg->unmask(irq);
    }
}
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 6, 5)
static void iproc_gpio_irq_mask(unsigned int irq)
{
#else
static void iproc_gpio_irq_mask(struct irq_data *d)
{
    unsigned int irq = d->irq;
#endif
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);

    if (ourchip) {
    	struct iproc_gpio_irqcfg *irqcfg = ourchip->irqcfg;
	if (irqcfg && irqcfg->mask)
	    irqcfg->mask(irq);
    }
}
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 6, 5)
static int iproc_gpio_irq_set_type(unsigned int irq, unsigned int type)
{
#else
static int iproc_gpio_irq_set_type(struct irq_data *d, unsigned int type)
{
    unsigned int irq = d->irq;
#endif
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);

    if (ourchip) {
    	struct iproc_gpio_irqcfg *irqcfg = ourchip->irqcfg;
	if (irqcfg && irqcfg->set_type)
	    return irqcfg->set_type(irq, type);
    }
    return -EINVAL;
}

#if defined(IPROC_GPIO_CCA)
static irqreturn_t 
iproc_gpio_irq_handler_cca(int irq, void *dev)

{
    unsigned int  val, irq_type;
    unsigned int  int_mask, int_pol, in;
    unsigned int  event_mask, event, event_pol, tmp = 0;
    int iter, g_irq, max_pin;
    struct iproc_gpio_chip *ourchip = dev;


    val = readl(ourchip->intr_ioaddr + IPROC_CCA_INT_STS);
    
    if (val & IPROC_CCA_INT_F_GPIOINT) {
        int_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_LEVEL_MASK);
        int_pol = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_LEVEL);
        in = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_DIN);
        event_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_EVENT_MASK);
        event = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_EVENT);
        event_pol = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_EDGE);

		max_pin = ourchip->pin_offset + ourchip->chip.ngpio;
        for (iter = ourchip->pin_offset; iter < max_pin; iter ++) {
            g_irq = iproc_gpio_to_irq(ourchip, iter);  
            irq_type = irq_desc[g_irq].status_use_accessors & IRQ_TYPE_SENSE_MASK;
            switch(irq_type) {
                case IRQ_TYPE_EDGE_RISING:
                    tmp = event_mask;
                    tmp &= event;
                    tmp &= ~event_pol;
                    if (tmp & (1 << iter)) {
                        generic_handle_irq(g_irq);
                    }
                    break;
                case IRQ_TYPE_EDGE_FALLING:
                    tmp = event_mask;
                    tmp &= event;
                    tmp &= event_pol;
                    if (tmp & (1 << iter)) {
                        generic_handle_irq(g_irq);
                    }                
                    break;
                case IRQ_TYPE_LEVEL_LOW:
                    tmp = in ^ int_pol;
                    tmp &= int_mask;
                    tmp &= int_pol;
                    if (tmp & (1 << iter)) {
                        generic_handle_irq(g_irq);
                    }                
                    break;
                case IRQ_TYPE_LEVEL_HIGH:
                    tmp = in ^ int_pol;
                    tmp &= int_mask;
                    tmp &= ~int_pol;
                    if (tmp & (1 << iter)) {
                        generic_handle_irq(g_irq);
                    }                
                    break;
                default:                        
                    break;
            }
        }
    }else {
        return IRQ_NONE;
    }

    return IRQ_HANDLED;
}

static void iproc_gpio_irq_ack_cca(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
	if (ourchip->id == IPROC_GPIO_CCA_ID) {
	    unsigned int  event_status, irq_type;
	
	    event_status = 0;
	    irq_type = irq_desc[irq].status_use_accessors & IRQ_TYPE_SENSE_MASK;
	    if (irq_type & IRQ_TYPE_EDGE_BOTH) 
	    {
		event_status |= (1 << pin);	       
		_iproc_gpio_writel(ourchip, event_status,
		    IPROC_GPIO_CCA_INT_EVENT);
	    }
	
	}
}

static void iproc_gpio_irq_unmask_cca(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	unsigned int int_mask, irq_type;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	irq_type = irq_desc[irq].status_use_accessors & IRQ_TYPE_SENSE_MASK;
	
	if (ourchip->id == IPROC_GPIO_CCA_ID) {
	    unsigned int  event_mask; 
	
	    event_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_EVENT_MASK);
	    int_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_LEVEL_MASK);
	
	    if (irq_type & IRQ_TYPE_EDGE_BOTH) {
		event_mask |= 1 << pin;
		_iproc_gpio_writel(ourchip, event_mask, 
		    IPROC_GPIO_CCA_INT_EVENT_MASK);
	    } else {
		int_mask |= 1 << pin;
		_iproc_gpio_writel(ourchip, int_mask, 
		    IPROC_GPIO_CCA_INT_LEVEL_MASK);
	    }
	}

}

static void iproc_gpio_irq_mask_cca(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	unsigned int irq_type, int_mask;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	irq_type = irq_desc[irq].status_use_accessors & IRQ_TYPE_SENSE_MASK;
	
	if (ourchip->id == IPROC_GPIO_CCA_ID) {
	    unsigned int  event_mask;
	    
	    event_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_EVENT_MASK);
	    int_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_LEVEL_MASK);
	
	    if (irq_type & IRQ_TYPE_EDGE_BOTH) {
		event_mask &= ~(1 << pin);
		_iproc_gpio_writel(ourchip, event_mask,
		    IPROC_GPIO_CCA_INT_EVENT_MASK);
	    } else {
		int_mask &= ~(1 << pin);
		_iproc_gpio_writel(ourchip, int_mask, 
		    IPROC_GPIO_CCA_INT_LEVEL_MASK);
	    }
	}
}

static int iproc_gpio_irq_set_type_cca(unsigned int irq, unsigned int type)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;    
	
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
	if (ourchip->id == IPROC_GPIO_CCA_ID) {
	    unsigned int  event_pol, int_pol;
	
	    switch (type & IRQ_TYPE_SENSE_MASK) {
	    case IRQ_TYPE_EDGE_RISING:
		event_pol = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_EDGE);
		event_pol &= ~(1 << pin);
		_iproc_gpio_writel(ourchip, event_pol, IPROC_GPIO_CCA_INT_EDGE);
		break;
	    case IRQ_TYPE_EDGE_FALLING:
		event_pol = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_EDGE);
		event_pol |= (1 << pin);
		_iproc_gpio_writel(ourchip, event_pol, IPROC_GPIO_CCA_INT_EDGE);
		break;
	    case IRQ_TYPE_LEVEL_HIGH:
		int_pol = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCA_INT_LEVEL);
		int_pol &= ~(1 << pin);
		_iproc_gpio_writel(ourchip, int_pol, IPROC_GPIO_CCA_INT_LEVEL);
		break;
	    case IRQ_TYPE_LEVEL_LOW:
		int_pol = _iproc_gpio_readl(ourchip,IPROC_GPIO_CCA_INT_LEVEL);
		int_pol |= (1 << pin);
		_iproc_gpio_writel(ourchip, int_pol, IPROC_GPIO_CCA_INT_LEVEL);
		break;
	    default:
		printk(KERN_ERR "unsupport irq type !\n");
		return -EINVAL;
	    }
	}
	
	return 0;
}

struct iproc_gpio_irqcfg cca_gpio_irqcfg = {
        .flags = IRQF_NO_SUSPEND|IRQF_SHARED,
	.handler = iproc_gpio_irq_handler_cca,
	.ack = iproc_gpio_irq_ack_cca,
	.mask = iproc_gpio_irq_mask_cca,
	.unmask = iproc_gpio_irq_unmask_cca,
	.set_type = iproc_gpio_irq_set_type_cca,
};
#endif /* IPROC_GPIO_CCA */

#if defined(IPROC_GPIO_CCB) || defined(IPROC_GPIO_CCG)
static irqreturn_t 
iproc_gpio_irq_handler_ccb(int irq, void *dev)
{
    struct iproc_gpio_chip *ourchip = dev;
    int iter, max_pin;
    unsigned int  val;

    val = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_MSTAT);
    if(!val){
        return IRQ_NONE;
    }

	max_pin = ourchip->pin_offset + ourchip->chip.ngpio;
    for (iter = ourchip->pin_offset; iter < max_pin; iter ++) {
        if (val & (1 << iter)) {
            generic_handle_irq(iproc_gpio_to_irq(ourchip, iter));
        }
    }
    
    return IRQ_HANDLED;
}

static void iproc_gpio_irq_ack_ccb(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
	if ((ourchip->id == IPROC_GPIO_CCB_ID) || 
		(ourchip->id == IPROC_GPIO_CCG_ID)) {
	    unsigned int int_clear = 0;
	
	    int_clear |= (1 << pin);
	    _iproc_gpio_writel(ourchip, int_clear, IPROC_GPIO_CCB_INT_CLR);
	
	}
}

static void iproc_gpio_irq_unmask_ccb(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	unsigned int int_mask;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
	if ((ourchip->id == IPROC_GPIO_CCB_ID) ||
		(ourchip->id == IPROC_GPIO_CCG_ID)) {
	    int_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_MASK);
	    int_mask |= (1 << pin);
	    _iproc_gpio_writel(ourchip, int_mask, IPROC_GPIO_CCB_INT_MASK);
	}
	
}

static void iproc_gpio_irq_mask_ccb(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	unsigned int int_mask;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
	if ((ourchip->id == IPROC_GPIO_CCB_ID) ||
		(ourchip->id == IPROC_GPIO_CCG_ID)) {
	    int_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_MASK);
	    int_mask &= ~(1 << pin);
	    _iproc_gpio_writel(ourchip, int_mask,IPROC_GPIO_CCB_INT_MASK);
	}
}

static int iproc_gpio_irq_set_type_ccb(unsigned int irq, unsigned int type)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;    
	
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
	if ((ourchip->id == IPROC_GPIO_CCB_ID) ||
		(ourchip->id == IPROC_GPIO_CCG_ID)) {
	    unsigned int  int_type, int_de, int_edge;
	    int_type = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_TYPE);
	    int_edge = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_EDGE);
	    switch (type) {
		case IRQ_TYPE_EDGE_BOTH:
		    int_type &= ~(1 << pin); 
		    int_de = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_DE);
		    int_de |= (1 << pin); 
		    _iproc_gpio_writel(ourchip, int_de, IPROC_GPIO_CCB_INT_DE);
		    break;
		case IRQ_TYPE_EDGE_RISING:
		    int_type &= ~(1 << pin); 
		    int_edge |= (1 << pin); 
		    
		    int_de = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_DE);
		    int_de  &= ~(1 << pin); 
		    _iproc_gpio_writel(ourchip, int_de, IPROC_GPIO_CCB_INT_DE);
		    break;
		case IRQ_TYPE_EDGE_FALLING:
		    int_type &= ~(1 << pin);
		    int_edge &= ~(1 << pin);
		    
		    int_de = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_DE);
		    int_de  &= ~(1 << pin); 
		    _iproc_gpio_writel(ourchip, int_de, IPROC_GPIO_CCB_INT_DE);
		    break;
		case IRQ_TYPE_LEVEL_HIGH:
		    int_type |= (1 << pin); 
		    int_edge |= (1 << pin); 
		    break;
		case IRQ_TYPE_LEVEL_LOW:
		    int_type |= (1 << pin); 
		    int_edge &= ~(1 << pin); 
		    break;
		default:
		    printk(KERN_ERR "unsupport irq type !\n");
		    return -EINVAL;
	    }
	    _iproc_gpio_writel(ourchip, int_type, IPROC_GPIO_CCB_INT_TYPE);
	    _iproc_gpio_writel(ourchip, int_edge, IPROC_GPIO_CCB_INT_EDGE);
	}
	
	return 0;
}

struct iproc_gpio_irqcfg ccb_gpio_irqcfg = {
        .flags = IRQF_NO_SUSPEND,
	.handler = iproc_gpio_irq_handler_ccb,
	.ack = iproc_gpio_irq_ack_ccb,
	.mask = iproc_gpio_irq_mask_ccb,
	.unmask = iproc_gpio_irq_unmask_ccb,
	.set_type = iproc_gpio_irq_set_type_ccb,
};
#endif /* IPROC_GPIO_CCB || IPROC_GPIO_CCG*/


#if defined(ASIU_GPIO)
static irqreturn_t 
asiu_gpio_irq_handler(int irq, void *dev)
{
    struct iproc_gpio_chip *ourchip = dev;
    int iter, max_pin;
    int round, i, start_pin, end_pin;
    unsigned int  val;
    int irq_handled = 0;

    max_pin = ourchip->pin_offset + ourchip->chip.ngpio;
    round = (max_pin - 1)/32 + 1;
    for(i=0; i< round; i++) {
        val = _iproc_gpio_readl(ourchip, ASIU_GP_INT_MSTAT_0_BASE + i*ASIU_GPIO_REGOFFSET);
	if (val) {
	    start_pin = i * 32;
	    end_pin = start_pin + 32;
	    if (start_pin < ourchip->pin_offset)
	    	start_pin = ourchip->pin_offset;
	    if (end_pin > max_pin)
	    	end_pin = max_pin;
            for (iter = start_pin; iter < end_pin; iter ++) {
	    	if (val & (1 << ASIU_GPIO_REGBIT(iter)))
                    generic_handle_irq(iproc_gpio_to_irq(ourchip, iter));
            }
	    irq_handled = 1;
	}
    }

    if (!irq_handled)
    	return IRQ_NONE;
    else
     return IRQ_HANDLED;
}

static void asiu_gpio_irq_ack(unsigned int irq)
{
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
    int pin;
    unsigned int int_clear = 0;
	
    pin = iproc_irq_to_gpio(ourchip, irq);
	
    int_clear |= (1 << ASIU_GPIO_REGBIT(pin));
    _iproc_gpio_writel(ourchip, int_clear, 
                        ASIU_GP_INT_CLR_0_BASE + ASIU_GPIO_REGIDX(pin)*ASIU_GPIO_REGOFFSET);	
}

static void asiu_gpio_irq_unmask(unsigned int irq)
{
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
    int pin, reg_offset;
    unsigned int int_mask;
	
    pin = iproc_irq_to_gpio(ourchip, irq);

    reg_offset = ASIU_GP_INT_MSK_0_BASE	+ ASIU_GPIO_REGIDX(pin)*ASIU_GPIO_REGOFFSET;
    int_mask = _iproc_gpio_readl(ourchip, reg_offset);
    int_mask |= (1 << ASIU_GPIO_REGBIT(pin));
    _iproc_gpio_writel(ourchip, int_mask, reg_offset);
}

static void asiu_gpio_irq_mask(unsigned int irq)
{
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
    int pin, reg_offset;
    unsigned int int_mask;
	
    pin = iproc_irq_to_gpio(ourchip, irq);

    reg_offset = ASIU_GP_INT_MSK_0_BASE + ASIU_GPIO_REGIDX(pin)*ASIU_GPIO_REGOFFSET;
    int_mask = _iproc_gpio_readl(ourchip, reg_offset);
    int_mask &= ~(1 << ASIU_GPIO_REGBIT(pin));
    _iproc_gpio_writel(ourchip, int_mask, reg_offset);
}

static int asiu_gpio_irq_set_type(unsigned int irq, unsigned int type)
{
    struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
    int pin, reg_offset;    
    unsigned int  int_type, int_de, int_edge;
	
    pin = iproc_irq_to_gpio(ourchip, irq);
	
    reg_offset = ASIU_GPIO_REGIDX(pin)*ASIU_GPIO_REGOFFSET;
    
    int_type = _iproc_gpio_readl(ourchip, ASIU_GP_INT_TYPE_0_BASE + reg_offset);
    int_edge = _iproc_gpio_readl(ourchip, ASIU_GP_INT_EDGE_0_BASE + reg_offset);

    switch (type) {
    case IRQ_TYPE_EDGE_BOTH:
        int_type &= ~(1 << ASIU_GPIO_REGBIT(pin)); 
        int_de = _iproc_gpio_readl(ourchip, ASIU_GP_INT_DE_0_BASE + reg_offset);
        int_de |= (1 << ASIU_GPIO_REGBIT(pin)); 
        _iproc_gpio_writel(ourchip, int_de, ASIU_GP_INT_DE_0_BASE + reg_offset);
        break;
    case IRQ_TYPE_EDGE_RISING:
        int_type &= ~(1 << ASIU_GPIO_REGBIT(pin)); 
        int_edge |= (1 << ASIU_GPIO_REGBIT(pin)); 
		    
        int_de = _iproc_gpio_readl(ourchip, ASIU_GP_INT_DE_0_BASE + reg_offset);
        int_de  &= ~(1 << ASIU_GPIO_REGBIT(pin)); 
        _iproc_gpio_writel(ourchip, int_de, ASIU_GP_INT_DE_0_BASE + reg_offset);
        break;
    case IRQ_TYPE_EDGE_FALLING:
        int_type &= ~(1 << ASIU_GPIO_REGBIT(pin));
        int_edge &= ~(1 << ASIU_GPIO_REGBIT(pin));
		    
        int_de = _iproc_gpio_readl(ourchip, ASIU_GP_INT_DE_0_BASE + reg_offset);
        int_de  &= ~(1 << ASIU_GPIO_REGBIT(pin)); 
        _iproc_gpio_writel(ourchip, int_de, ASIU_GP_INT_DE_0_BASE + reg_offset);
        break;
    case IRQ_TYPE_LEVEL_HIGH:
        int_type |= (1 << ASIU_GPIO_REGBIT(pin)); 
        int_edge |= (1 << ASIU_GPIO_REGBIT(pin)); 
        break;
    case IRQ_TYPE_LEVEL_LOW:
        int_type |= (1 << ASIU_GPIO_REGBIT(pin)); 
        int_edge &= ~(1 << ASIU_GPIO_REGBIT(pin)); 
        break;
    default:
        printk(KERN_ERR "unsupport irq type !\n");
        return -EINVAL;
    }
    _iproc_gpio_writel(ourchip, int_type, ASIU_GP_INT_TYPE_0_BASE + reg_offset);
    _iproc_gpio_writel(ourchip, int_edge, ASIU_GP_INT_EDGE_0_BASE + reg_offset);
	
    return 0;
}

struct iproc_gpio_irqcfg asiu_gpio_irqcfg = {
    .flags = IRQF_NO_SUSPEND,
    .handler = asiu_gpio_irq_handler,
    .ack = asiu_gpio_irq_ack,
    .mask = asiu_gpio_irq_mask,
    .unmask = asiu_gpio_irq_unmask,
    .set_type = asiu_gpio_irq_set_type,
};
#endif /* ASIU_GPIO */


#if defined(AON_GPIO)
static irqreturn_t 
aon_gpio_irq_handler(int irq, void *dev)
{
    struct iproc_gpio_chip *ourchip = dev;
    int iter, max_pin;
    unsigned int  val;
	irqreturn_t rv;

    val = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_MSTAT);
    if(!val){
		rv = IRQ_NONE;
		goto clear_mailbox_irq;
    }

	max_pin = ourchip->pin_offset + ourchip->chip.ngpio;
    for (iter = ourchip->pin_offset; iter < max_pin; iter ++) {
        if (val & (1 << iter)) {
            generic_handle_irq(iproc_gpio_to_irq(ourchip, iter));
        }
    }
    
    rv = IRQ_HANDLED;

clear_mailbox_irq:
	writel(0, ourchip->dmu_ioaddr + CRMU_IPROC_MAIL_BOX0_BASE);
	writel(0, ourchip->dmu_ioaddr + CRMU_IPROC_MAIL_BOX1_BASE);
	writel(readl(ourchip->dmu_ioaddr + CRMU_IPROC_INTR_CLEAR_BASE) | (1 << CRMU_IPROC_INTR_CLEAR__IPROC_MAILBOX_INTR_CLR), 
			ourchip->dmu_ioaddr + CRMU_IPROC_INTR_CLEAR_BASE);		
	
	return rv;
}

static void aon_gpio_irq_ack(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
    unsigned int int_clear = 0;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
	
    int_clear |= (1 << pin);
    _iproc_gpio_writel(ourchip, int_clear, IPROC_GPIO_CCB_INT_CLR);
	
}

static void aon_gpio_irq_unmask(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	unsigned int int_mask;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
    int_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_MASK);
    int_mask |= (1 << pin);
    _iproc_gpio_writel(ourchip, int_mask, IPROC_GPIO_CCB_INT_MASK);
	
}

static void aon_gpio_irq_mask(unsigned int irq)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;
	unsigned int int_mask;
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
    int_mask = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_MASK);
    int_mask &= ~(1 << pin);
    _iproc_gpio_writel(ourchip, int_mask,IPROC_GPIO_CCB_INT_MASK);
}

static int aon_gpio_irq_set_type(unsigned int irq, unsigned int type)
{
	struct iproc_gpio_chip *ourchip = irq_get_chip_data(irq);
	int pin;    
    unsigned int  int_type, int_de, int_edge;
	
	
	pin = iproc_irq_to_gpio(ourchip, irq);
	
    int_type = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_TYPE);
    int_edge = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_EDGE);
    switch (type) {
	case IRQ_TYPE_EDGE_BOTH:
	    int_type &= ~(1 << pin); 
	    int_de = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_DE);
	    int_de |= (1 << pin); 
	    _iproc_gpio_writel(ourchip, int_de, IPROC_GPIO_CCB_INT_DE);
	    break;
	case IRQ_TYPE_EDGE_RISING:
	    int_type &= ~(1 << pin); 
	    int_edge |= (1 << pin); 
	    
	    int_de = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_DE);
	    int_de  &= ~(1 << pin); 
	    _iproc_gpio_writel(ourchip, int_de, IPROC_GPIO_CCB_INT_DE);
	    break;
	case IRQ_TYPE_EDGE_FALLING:
	    int_type &= ~(1 << pin);
	    int_edge &= ~(1 << pin);
		    
	    int_de = _iproc_gpio_readl(ourchip, IPROC_GPIO_CCB_INT_DE);
	    int_de  &= ~(1 << pin); 
	    _iproc_gpio_writel(ourchip, int_de, IPROC_GPIO_CCB_INT_DE);
	    break;
	case IRQ_TYPE_LEVEL_HIGH:
	    int_type |= (1 << pin); 
	    int_edge |= (1 << pin); 
	    break;
	case IRQ_TYPE_LEVEL_LOW:
	    int_type |= (1 << pin); 
	    int_edge &= ~(1 << pin); 
	    break;
	default:
	    printk(KERN_ERR "unsupport irq type !\n");
	    return -EINVAL;
    }
    _iproc_gpio_writel(ourchip, int_type, IPROC_GPIO_CCB_INT_TYPE);
    _iproc_gpio_writel(ourchip, int_edge, IPROC_GPIO_CCB_INT_EDGE);
	
	return 0;
}

struct iproc_gpio_irqcfg aon_gpio_irqcfg = {
    .flags = IRQF_NO_SUSPEND,
	.handler = aon_gpio_irq_handler,
	.ack = aon_gpio_irq_ack,
	.mask = aon_gpio_irq_mask,
	.unmask = aon_gpio_irq_unmask,
	.set_type = aon_gpio_irq_set_type,
};
#endif /* AON_GPIO */


static struct irq_chip iproc_gpio_irq_chip = {
    .name         = "IPROC-GPIO",
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 6, 5)
    .ack      = (void *) iproc_gpio_irq_ack,
    .mask     = (void *) iproc_gpio_irq_mask,
    .unmask   = (void *) iproc_gpio_irq_unmask,
    .set_type = (void *) iproc_gpio_irq_set_type,
#else
    .irq_ack      = (void *) iproc_gpio_irq_ack,
    .irq_mask     = (void *) iproc_gpio_irq_mask,
    .irq_unmask   = (void *) iproc_gpio_irq_unmask,
    .irq_set_type = (void *) iproc_gpio_irq_set_type,
#endif
};

struct iproc_gpio_chip *iproc_gpios[IPROC_GPIO_END];

static __init void iproc_gpiolib_track(struct iproc_gpio_chip *chip)
{
    unsigned int gpn;
    int i;

    gpn = chip->chip.base;
    for (i = 0; i < chip->chip.ngpio; i++, gpn++) {
        BUG_ON(gpn >= ARRAY_SIZE(iproc_gpios));
        iproc_gpios[gpn] = chip;
    }
}

static int iproc_gpiolib_input(struct gpio_chip *chip, unsigned gpio)
{
    struct iproc_gpio_chip *ourchip = to_iproc_gpio(chip);
    unsigned long flags;
    unsigned int  val, nBitMask;
    int reg_offset;
    unsigned int pin_offset = gpio + ourchip->pin_offset;

    iproc_gpio_lock(ourchip, flags);

    if (ourchip->id == ASIU_GPIO_ID) {
    	nBitMask = 1 << ASIU_GPIO_REGBIT(pin_offset);
    	reg_offset = REGOFFSET_GPIO_EN + ASIU_GPIO_REGIDX(pin_offset)*ASIU_GPIO_REGOFFSET;
    }
    else {
        nBitMask = 1 << pin_offset;
	reg_offset = REGOFFSET_GPIO_EN;
    }

    val = _iproc_gpio_readl(ourchip, reg_offset);
    val &= ~nBitMask;
    _iproc_gpio_writel(ourchip, val, reg_offset);
    
    iproc_gpio_unlock(ourchip, flags);
    return 0;
}

static int iproc_gpiolib_output(struct gpio_chip *chip,
			      unsigned gpio, int value)
{
    struct iproc_gpio_chip *ourchip = to_iproc_gpio(chip);
    unsigned long flags, val;
    unsigned int nBitMask;
    int reg_offset;
    unsigned int pin_offset = gpio + ourchip->pin_offset;

    iproc_gpio_lock(ourchip, flags);

    if (ourchip->id == ASIU_GPIO_ID) {
    	nBitMask = 1 << ASIU_GPIO_REGBIT(pin_offset);
    	reg_offset = REGOFFSET_GPIO_EN + ASIU_GPIO_REGIDX(pin_offset)*ASIU_GPIO_REGOFFSET;
    }
    else {
        nBitMask = 1 << pin_offset;
	reg_offset = REGOFFSET_GPIO_EN;
    }

    val = _iproc_gpio_readl(ourchip, reg_offset);
    val |= nBitMask;
    _iproc_gpio_writel(ourchip, val, reg_offset);

    iproc_gpio_unlock(ourchip, flags);
    return 0;
}

static void iproc_gpiolib_set(struct gpio_chip *chip,
			    unsigned gpio, int value)
{
    struct iproc_gpio_chip *ourchip = to_iproc_gpio(chip);
    unsigned long flags, val;
    unsigned int nBitMask;
    int reg_offset = 0;
    unsigned int pin_offset = gpio + ourchip->pin_offset;

    iproc_gpio_lock(ourchip, flags);


    /* determine the GPIO pin direction 
     */ 
    if (ourchip->id == ASIU_GPIO_ID) {
	nBitMask = 1 << ASIU_GPIO_REGBIT(pin_offset);
	reg_offset = ASIU_GPIO_REGIDX(pin_offset)*ASIU_GPIO_REGOFFSET;
    } else {
	nBitMask = 1 << pin_offset;
    }
    val = _iproc_gpio_readl(ourchip, REGOFFSET_GPIO_EN + reg_offset);
    val &= nBitMask;

    /* this function only applies to output pin
     */ 
    if (!val)
        return;

    val = _iproc_gpio_readl(ourchip, REGOFFSET_GPIO_DOUT + reg_offset);

    if ( value == 0 ){
        /* Set the pin to zero */
        val &= ~nBitMask;
    }else{
        /* Set the pin to 1 */
        val |= nBitMask;
    }    
    _iproc_gpio_writel(ourchip, val, REGOFFSET_GPIO_DOUT + reg_offset);

    iproc_gpio_unlock(ourchip, flags);

}


static int iproc_gpiolib_get(struct gpio_chip *chip, unsigned gpio)
{
    struct iproc_gpio_chip *ourchip = to_iproc_gpio(chip);
    unsigned long flags;
    unsigned int val, offset, nBitMask;
    int reg_offset = 0;
    unsigned int pin_offset = gpio + ourchip->pin_offset;

    iproc_gpio_lock(ourchip, flags);

    if (ourchip->id == ASIU_GPIO_ID) {
	nBitMask = 1 << ASIU_GPIO_REGBIT(pin_offset);
    	reg_offset = ASIU_GPIO_REGIDX(pin_offset)*ASIU_GPIO_REGOFFSET;
    }
    else {
        nBitMask = 1 << pin_offset;
    }

    /* determine the GPIO pin direction 
     */ 
    offset = _iproc_gpio_readl(ourchip, REGOFFSET_GPIO_EN + reg_offset);
    offset &= nBitMask;

    if (offset){
        val = _iproc_gpio_readl(ourchip, REGOFFSET_GPIO_DOUT + reg_offset);
    } else {
        val = _iproc_gpio_readl(ourchip, REGOFFSET_GPIO_DIN + reg_offset);    
    }
    
    if (ourchip->id == ASIU_GPIO_ID) {
    	val >>= ASIU_GPIO_REGBIT(pin_offset);
    } else {	
        val >>= pin_offset;
    }

    val &= 1;
    
    iproc_gpio_unlock(ourchip, flags);

    return val;
}

/*
@offset : the gpio pin index number from gpiolib view (minus gpio base only)
*/
static int iproc_gpiolib_to_irq(struct gpio_chip *chip,
                unsigned offset)
{
    struct iproc_gpio_chip *ourchip = to_iproc_gpio(chip);
    return iproc_gpio_to_irq(ourchip, offset + ourchip->pin_offset);
}

#if defined(CONFIG_MACH_CYGNUS)
struct muxfun_regdesc {
    unsigned long reg_offset;
    unsigned char startb;
    unsigned char endb;
};
struct cygnus_gpio_muxfun {
    struct muxfun_regdesc reg;
    unsigned char iomuxfun;
};
#define REG_DESC(reg, func) {\
	reg##_BASE, \
	reg##__CORE_TO_IOMUX_##func##_SEL_R, \
	reg##__CORE_TO_IOMUX_##func##_SEL_L  \
	}

static struct cygnus_gpio_muxfun ccg_gpio_muxfun_table[24] = {
    /* GPIO0 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO0), 0},
    /* GPIO1 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO1), 0},
    /* GPIO2 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO2), 0},
    /* GPIO3 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO3), 0},
    /* GPIO4 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO4), 0},
    /* GPIO5 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO5), 0},
    /* GPIO6 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO6), 0},
    /* GPIO7 */{REG_DESC(CRMU_IOMUX_CTRL0, GPIO7), 0},
    /* GPIO8 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO8), 0},
    /* GPIO9 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO9), 0},
    /* GPIO10 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO10), 0},
    /* GPIO11 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO11), 0},
    /* GPIO12 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO12), 0},
    /* GPIO13 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO13), 0},
    /* GPIO14 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO14), 0},
    /* GPIO15 */{REG_DESC(CRMU_IOMUX_CTRL1, GPIO15), 0},
    /* GPIO16 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO16), 0},
    /* GPIO17 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO17), 0},
    /* GPIO18 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO18), 0},
    /* GPIO19 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO19), 0},
    /* GPIO20 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO20), 0},
    /* GPIO21 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO21), 0},
    /* GPIO22 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO22), 0},
    /* GPIO23 */{REG_DESC(CRMU_IOMUX_CTRL2, GPIO23), 0},
};

static struct cygnus_gpio_muxfun asiu_gpio_muxfun_table[123][3] = {
    /* GPIO24 */{{REG_DESC(CRMU_IOMUX_CTRL3, SMART_CARD0), 3}},
    /* GPIO25 */{{REG_DESC(CRMU_IOMUX_CTRL3, SMART_CARD0), 3}},
    /* GPIO26 */{{REG_DESC(SMART_CARD_FCB_SEL, SMART_CARD0_FCB), 3}},
    /* GPIO27 */{{REG_DESC(CRMU_IOMUX_CTRL3, SMART_CARD0), 3}},
    /* GPIO28 */{{REG_DESC(CRMU_IOMUX_CTRL3, SMART_CARD1), 3}},
    /* GPIO29 */{{REG_DESC(CRMU_IOMUX_CTRL3, SMART_CARD1), 3}},
    /* GPIO30 */{{REG_DESC(SMART_CARD_FCB_SEL, SMART_CARD1_FCB), 3}},
    /* GPIO31 */{{REG_DESC(CRMU_IOMUX_CTRL3, SMART_CARD1), 3}},
    /* GPIO32 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO33 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO34 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO35 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO36 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO37 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO38 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO39 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO40 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO41 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO42 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO43 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO44 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO45 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO46 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO47 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO48 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO49 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO50 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO51 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO52 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO53 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO54 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO55 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO56 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO57 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3},
                {REG_DESC(CRMU_IOMUX_CTRL4, LCD), 2}},
    /* GPIO58 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3}},
    /* GPIO59 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3}},
    /* GPIO60 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3}},
    /* GPIO61 */{{REG_DESC(CRMU_IOMUX_CTRL4, LCD), 3}},
    /* GPIO62 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI0), 3}},
    /* GPIO63 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI0), 3}},
    /* GPIO64 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI0), 3}},
    /* GPIO65 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI1), 3}},
    /* GPIO66 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI1), 3}},
    /* GPIO67 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI1), 3}},
    /* GPIO68 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI2), 3}},
    /* GPIO69 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI2), 3}},
    /* GPIO70 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI2), 3}},
    /* GPIO71 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI3), 3}},
    /* GPIO72 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI3), 3}},
    /* GPIO73 */{{REG_DESC(CRMU_IOMUX_CTRL4, SPI3), 3}},
    /* GPIO74 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_LED), 3}},
    /* GPIO75 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_LED), 3}},
    /* GPIO76 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_CAN0_SPI4), 3}},
    /* GPIO77 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_CAN0_SPI4), 3}},
    /* GPIO78 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_CAN0_SPI4), 3}},
    /* GPIO79 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_CAN0_SPI4), 3}},
    /* GPIO80 */{{REG_DESC(CRMU_IOMUX_CTRL3, SDIO0), 3}},
    /* GPIO81 */{{REG_DESC(CRMU_IOMUX_CTRL3, SDIO0), 3}},
    /* GPIO82 */{{REG_DESC(CRMU_IOMUX_CTRL3, SDIO0), 3}},
    /* GPIO83 */{{REG_DESC(CRMU_IOMUX_CTRL3, SDIO0), 3}},
    /* GPIO84 */{{REG_DESC(CRMU_IOMUX_CTRL3, SDIO0), 3}},
    /* GPIO85 */{{REG_DESC(CRMU_IOMUX_CTRL3, SDIO0), 3}},
    /* GPIO86 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART0), 3}},
    /* GPIO87 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART0), 3}},
    /* GPIO88 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART1), 3}},
    /* GPIO89 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART1), 3}},
    /* GPIO90 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART1_DTE), 3}},
    /* GPIO91 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART1_DTE), 3}},
    /* GPIO92 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART1_DTE), 3}},
    /* GPIO93 */{{REG_DESC(CRMU_IOMUX_CTRL5, UART3), 3}},
    /* GPIO94 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_LED), 3}},
    /* GPIO95 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_LED), 3}},
    /* GPIO96 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_LED), 3}},
    /* GPIO97 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_LED), 3}},
    /* GPIO98 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_LED), 3},
                {REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_LED), 2},
                {REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_LED), 1}},
    /* GPIO99 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3}},
    /* GPIO100 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3}},
    /* GPIO101 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3}},
    /* GPIO102 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3}},
    /* GPIO103 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3}},
    /* GPIO104 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3}},
    /* GPIO105 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3}},
    /* GPIO106 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_SRAM_RGMII), 3},
                 {REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 1}},
    /* GPIO107 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 3},
                 {REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 1}},
    /* GPIO108 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 3},
                 {REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 1}},
    /* GPIO109 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 3},
                 {REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 1}},
    /* GPIO110 */{{REG_DESC(CRMU_IOMUX_CTRL7, CAMERA_RGMII), 3}},
    /* GPIO111 */{{REG_DESC(CRMU_IOMUX_CTRL5, QSPI), 3}},
    /* GPIO112 */{{REG_DESC(CRMU_IOMUX_CTRL5, QSPI), 3}},
    /* GPIO113 */{{REG_DESC(CRMU_IOMUX_CTRL5, QSPI), 3}},
    /* GPIO114 */{{REG_DESC(CRMU_IOMUX_CTRL5, QSPI), 3}},
    /* GPIO115 */{{REG_DESC(CRMU_IOMUX_CTRL7, QSPI_GPIO), 3}},
    /* GPIO116 */{{REG_DESC(CRMU_IOMUX_CTRL7, QSPI_GPIO), 3}},
    /* GPIO117 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO118 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO119 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO120 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO121 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO122 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO123 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO0_MMC), 3}},
    /* GPIO124 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO0_CD), 3}},
    /* GPIO125 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_MMC), 3}},
    /* GPIO126 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_MMC), 3}},
    /* GPIO127 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_MMC), 3}},
    /* GPIO128 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO1_CD), 3}},
    /* GPIO129 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO130 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO131 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO132 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO133 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO134 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO135 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO0_MMC), 3}},
    /* GPIO136 */{{REG_DESC(CRMU_IOMUX_CTRL6, SDIO0_MMC), 3}},
    /* GPIO137 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO138 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO139 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO140 */{{REG_DESC(CRMU_IOMUX_CTRL5, NAND), 3}},
    /* GPIO141 */{{REG_DESC(GPIO_3P3_IOMUX_SEL, GPIO0_3P3), 0}},
    /* GPIO142 */{{REG_DESC(GPIO_3P3_IOMUX_SEL, GPIO1_3P3), 0}},
    /* GPIO143 */{{REG_DESC(GPIO_3P3_IOMUX_SEL, GPIO2_3P3), 0}},
    /* GPIO144 */{{{-1 , 0, 0}, 0}}, /* Direct mapping to gpio3_3p3, no mux function */
    /* GPIO145 */{{{-1 , 0, 0}, 0}}, /* Direct mapping to usb_vbus_indication(input mode only), no mux function */
    /* GPIO146 */{{{-1 , 0, 0}, 0}}  /* Direct mapping to usb_id_indication(input mode only), no mux function */
};

static int cygnus_gpiolib_request(struct iproc_gpio_chip *chip, unsigned offset)
{
    unsigned int pin_offset = offset + chip->pin_offset;
    struct cygnus_gpio_muxfun *gpio_entry;
    struct muxfun_regdesc *regdesc;
    int i, j, gpiopin;
    unsigned long cur_iomux, iomux_mask, nBitMask, val;
	static int aon_gpio_init = 0;
    void __iomem *regaddr, *instram, *scratchram;
    void __iomem *baseaddr = ioremap_nocache(CRMU_GENPLL_CONTROL0, 0x200);

    if (chip->id == IPROC_GPIO_CCG_ID) {
        gpio_entry = &ccg_gpio_muxfun_table[pin_offset];
        if (gpio_entry && gpio_entry->reg.reg_offset != -1) {
            regdesc = &gpio_entry->reg;
            iomux_mask = 1 << regdesc->startb;
            for (j=regdesc->startb+1; j<=regdesc->endb; j++) {
                iomux_mask |= (1 << j);
            }
            val = readl(baseaddr + regdesc->reg_offset);
            cur_iomux = val & iomux_mask;
            if (cur_iomux != (gpio_entry->iomuxfun << regdesc->startb)) {
                val &= ~iomux_mask;
                val |= (gpio_entry->iomuxfun << regdesc->startb);
                writel(val, baseaddr + regdesc->reg_offset);
            }
        }
    } else if (chip->id == ASIU_GPIO_ID) {
        for (i=0; i<sizeof(asiu_gpio_muxfun_table[pin_offset])/sizeof(*gpio_entry); 
	            i++) {
            gpio_entry = &asiu_gpio_muxfun_table[pin_offset][i];
	    if (gpio_entry && gpio_entry->reg.reg_offset != -1) {
	    	regdesc = &gpio_entry->reg;
		iomux_mask = 1 << regdesc->startb;
		for (j=regdesc->startb+1; j<=regdesc->endb; j++) {
                    iomux_mask |= (1 << j);
                }
                cur_iomux = (readl(baseaddr + regdesc->reg_offset) & iomux_mask) >> regdesc->startb;
                if (cur_iomux == gpio_entry->iomuxfun) {
                    /* Current iomux func is suitable for the requested gpio. no need to change */
                    goto unmap;
                }
            }
        }
        /* Current iomux func is not matching to the requested gpio io mux func. 
             We need to set iomux function for the requested gpio. Set the io mux func
             to the first entry as it's to be the highest priority. */
        gpio_entry = &asiu_gpio_muxfun_table[pin_offset][0];
	if (gpio_entry && gpio_entry->reg.reg_offset!= -1) {
            regdesc = &gpio_entry->reg;
            cur_iomux = gpio_entry->iomuxfun << regdesc->startb;
            iomux_mask = 1 << regdesc->startb;
            for (j=regdesc->startb+1; j<= regdesc->endb; j++) {
                iomux_mask |= (1 << j);
            }
            writel((readl(baseaddr + regdesc->reg_offset) & ~iomux_mask) | cur_iomux, 
	    		baseaddr + regdesc->reg_offset);
            printk(KERN_WARNING "The mapping IO MUX function is being changed "
                    "by the gpio request.\nThis change might affect the modules "
                    "mapping to the corresponding IO MUX function.\n");
        }		    
    } else if (chip->id == AON_GPIO_ID) {
		if (aon_gpio_init == 0) {
		    /* Enable AON GPIO Interrupt */
			writel(readl(chip->dmu_ioaddr + CRMU_MCU_INTR_MASK_BASE) & (~(1 << CRMU_MCU_INTR_MASK__MCU_AON_GPIO_INTR_MASK)),
					chip->dmu_ioaddr + CRMU_MCU_INTR_MASK_BASE);
			/* Enable MCU to IPROC mailbox interrupt */
			writel(readl(chip->dmu_ioaddr + CRMU_IPROC_INTR_MASK_BASE) | (1 << CRMU_IPROC_INTR_MASK__IPROC_MAILBOX_INTR_MASK),
					chip->dmu_ioaddr + CRMU_IPROC_INTR_MASK_BASE);
			/* REMAP M0 inst RAM addr, and put M0 bin code on it. */
			/* M0 bin function : to set CRMU_IPROC_MAIL_BOX0 to 0x4f495047, and set CRMU_IPROC_MAIL_BOX1 to 0x00000001 */
			instram = ioremap_nocache(0x03013a00, 0x100);		
			writel(0x49024803, instram);
			writel(0x490362c1, instram+0x04);
			writel(0x47706301, instram+0x08);
			writel(0x4f495047, instram+0x0c);
			writel(0x03024000, instram+0x10);
			writel(0x00000001, instram+0x14);
			/* Redirect M0 AON_GPIO ISR to the ISR defined here */
			scratchram = ioremap_nocache(0x03018f00, 0x100);
			writel(0x03013a00, scratchram+0x88);
			iounmap(instram);
			iounmap(scratchram);
			aon_gpio_init = 1;
		}
		if (pin_offset == 5) {
			/* AON GPIO pin 5 is muxed with UART_RX */
			writel(readl(chip->dmu_ioaddr + CRMU_IOMUX_CONTROL_BASE) | (1 << CRMU_IOMUX_CONTROL__CRMU_OVERRIDE_UART_RX),
					chip->dmu_ioaddr + CRMU_IOMUX_CONTROL_BASE);
		}
		goto unmap;
    }

    /* Make sure the mux of sgpio and gpio is set to non secure */
    gpiopin = chip->chip.base + pin_offset;
    regaddr = baseaddr + CHIP_SECURE_GPIO_CONTROL0_BASE + (gpiopin/32)*4;
    nBitMask = 1 << (gpiopin % 32);
    val = __raw_readl(regaddr);
    if (val & nBitMask) {
    	val &= ~nBitMask;
    	writel(val, regaddr);
	printk(KERN_WARNING "Set chip pin %d to non secure gpio. "
                "Write 0x%08lx to 0x%08lx\n", gpiopin, (unsigned long)regaddr, val);
    }

unmap:
    iounmap(baseaddr);
    
    return 0;
}
#endif

static int iproc_gpiolib_request(struct gpio_chip *chip, unsigned offset)
{
    struct iproc_gpio_chip *ourchip = to_iproc_gpio(chip);

/* 
For Cygnus, we do GPIO IOMux funtion setting when ASIU and IPROC gpio is requested.
Further more, Cygnus gpio has mux for sgpio and gpio, we'll check this for cygnus gpio
both from iproc and asiu.
*/ 
#if defined(CONFIG_MACH_CYGNUS)
    return cygnus_gpiolib_request(ourchip, offset);
#else
    return 0;
#endif
}

void __init  iproc_gpiolib_add(struct iproc_gpio_chip *chip)
{
    struct resource *res;
    struct gpio_chip *gc = &chip->chip;
    int ret, i;    

    BUG_ON(!gc->label);
    BUG_ON(!gc->ngpio);
    
    spin_lock_init(&chip->lock);
    
    if (!gc->direction_input)
        gc->direction_input = iproc_gpiolib_input;
    if (!gc->direction_output)
        gc->direction_output = iproc_gpiolib_output;
    if (!gc->set)
        gc->set = iproc_gpiolib_set;
    if (!gc->get)
        gc->get = iproc_gpiolib_get;
    if (!gc->to_irq)
        gc->to_irq = iproc_gpiolib_to_irq;
    if (!gc->request)
    	gc->request = iproc_gpiolib_request;

    /* gpiochip_add() prints own failure message on error. */
    ret = gpiochip_add(gc);
    if (ret >= 0)
        iproc_gpiolib_track(chip);

    printk(KERN_INFO "iproc gpiochip add %s\n", gc->label);
    /* io remap */
    res = chip->resource;

    chip->ioaddr = ioremap_nocache(res->start, (res->end - res->start) + 1);
    printk(KERN_INFO "%s:ioaddr %p \n", gc->label, chip->ioaddr);
    chip->intr_ioaddr = NULL;
    chip->dmu_ioaddr = NULL;
    if(res->child){
        for (i=0; i< 2; i++){        
            if (!strcmp("intr", res->child[i].name)){
                chip->intr_ioaddr = 
                    ioremap_nocache(res->child[i].start, 
                    (res->child[i].end - res->child[i].start) + 1);
            }
            if (!strcmp("dmu", res->child[i].name)){
                chip->dmu_ioaddr = 
                    ioremap_nocache(res->child[i].start, 
                    (res->child[i].end - res->child[i].start) + 1);
            }
        }
        printk(KERN_INFO "%s:intr_ioaddr %p dmu_ioaddr %p\n",
            gc->label, chip->intr_ioaddr,chip->dmu_ioaddr);
    }


    if (chip->irq_base) {
        for (i = chip->irq_base; i < (chip->irq_base + gc->ngpio); i++) {
            irq_set_chip(i, &iproc_gpio_irq_chip);
            irq_set_chip_data(i,chip);
            irq_set_handler(i, handle_level_irq);
            set_irq_flags(i, IRQF_VALID);                            
    
        }
#if defined(IPROC_GPIO_CCA)	
        if (chip->id == IPROC_GPIO_CCA_ID ){
            unsigned int val;
            /* enable the GPIO in CCA interrupt mask */
            val = readl(chip->intr_ioaddr + IPROC_CCA_INT_MASK);
            val |= IPROC_CCA_INT_F_GPIOINT;
            writel(val, chip->intr_ioaddr + IPROC_CCA_INT_MASK);
        }
#endif	
	if (chip->irqcfg) {
	    struct iproc_gpio_irqcfg *irqcfg = chip->irqcfg;
	    if (irqcfg->handler) {
                ret = request_irq(chip->irq, irqcfg->handler, irqcfg->flags, 
		    gc->label, chip);
	        if (ret)
		    printk(KERN_ERR "Unable to request IRQ%d: %d\n",
		        chip->irq, ret);
	    }
	    else
	    	printk(KERN_ERR "%s is added without isr!\n", chip->chip.label);
	}
    }
    iproc_gpio_dev[dev] = chip;
    dev++;
}

static int __init gpio_init(void)
{      
    iproc_gpiolib_init();
    
    return 0;
}
static void __exit gpio_exit(void)
{
    int i=0;

    for (i = 0 ; i < MAX_NS_GPIO; i++) {        
        if(iproc_gpio_dev[i]){
            if(iproc_gpio_dev[i]->ioaddr){
                iounmap(iproc_gpio_dev[i]->ioaddr);
            }
            if(iproc_gpio_dev[i]->intr_ioaddr){
#if defined(IPROC_GPIO_CCA)	    	
                if (iproc_gpio_dev[i]->id == IPROC_GPIO_CCA_ID ){
                  unsigned int val;
                  val = readl(iproc_gpio_dev[i]->intr_ioaddr + IPROC_CCA_INT_MASK);
                  val &= ~(IPROC_CCA_INT_F_GPIOINT);
                  writel(val, iproc_gpio_dev[i]->intr_ioaddr + IPROC_CCA_INT_MASK);
                }
#endif		
                iounmap(iproc_gpio_dev[i]->intr_ioaddr);
            }
            if(iproc_gpio_dev[i]->dmu_ioaddr){
                iounmap(iproc_gpio_dev[i]->dmu_ioaddr);
            }                        
            if(iproc_gpio_dev[i]->irq_base) {
                free_irq(iproc_gpio_dev[i]->irq,iproc_gpio_dev[i]);
            }

            gpiochip_remove(&iproc_gpio_dev[i]->chip);
            iproc_gpio_dev[i] = NULL;
        }
    }
}

MODULE_DESCRIPTION("IPROC GPIO driver");
MODULE_LICENSE("GPL");

module_init(gpio_init);
module_exit(gpio_exit);
