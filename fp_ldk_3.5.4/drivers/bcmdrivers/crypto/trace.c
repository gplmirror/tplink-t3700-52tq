
unsigned trace_array[256];
uint8_t trace_idx = 0;
spinlock_t trace_lock;

//module_param_array(trace_array, uint, NULL, 0644);
//MODULE_PARM_DESC(trace_array, "Trace Array");
//module_param(trace_idx, byte, 0644);
//MODULE_PARM_DESC(trace_idx, "Max packets inflight at once");

inline void trace1(unsigned arg1)
{
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    {
        trace_array[trace_idx++] = arg1;
    }
    spin_unlock_irqrestore(&trace_lock, flags);
}
inline void trace2(unsigned arg1, unsigned arg2)
{
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    {
        trace_array[trace_idx++] = arg2;
        trace_array[trace_idx++] = arg1;
    }
    spin_unlock_irqrestore(&trace_lock, flags);
}
inline void trace4(unsigned arg1, unsigned arg2, unsigned arg3, unsigned arg4)
{
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    {
        trace_array[trace_idx++] = arg4;
        trace_array[trace_idx++] = arg3;
        trace_array[trace_idx++] = arg2;
        trace_array[trace_idx++] = arg1;
    }
    spin_unlock_irqrestore(&trace_lock, flags);
}
inline void trace8(unsigned arg1, unsigned arg2, unsigned arg3, unsigned arg4,
                   unsigned arg5, unsigned arg6, unsigned arg7, unsigned arg8)
{
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    {
        trace_array[trace_idx++] = arg8;
        trace_array[trace_idx++] = arg7;
        trace_array[trace_idx++] = arg6;
        trace_array[trace_idx++] = arg5;
        trace_array[trace_idx++] = arg4;
        trace_array[trace_idx++] = arg3;
        trace_array[trace_idx++] = arg2;
        trace_array[trace_idx++] = arg1;
    }
    spin_unlock_irqrestore(&trace_lock, flags);
}

inline void trace_dumplast(unsigned last, unsigned num)
{
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    {
        uint8_t idx = trace_idx;

        printk("Trace(%u, %u):", last, num);
        while (last) {
            if (idx % num == 0)
                printk("\n  %u: ", idx);

            idx--; last--;
            if (trace_array[idx] > 10000)
                printk("%08x ", trace_array[idx]);
            else
                printk("%u ", trace_array[idx]);
        }
    }
    spin_unlock_irqrestore(&trace_lock, flags);
}

static void trace_init(void)
{
    unsigned x;

    for (x = 0; x < 256; x++)
        trace_array[x] = 0;

    spin_lock_init(&trace_lock);
}

#define NUM_RECORDS 2

struct record_s {
    void* rctx_ptr;
    void* ctx_ptr;
    unsigned ctx_use;
};



struct record_s records[NUM_RECORDS];

void init_recs(void)
{
    unsigned i;
    for (i = 0; i < NUM_RECORDS; i++) {
        records[i].rctx_ptr = NULL;
        records[i].ctx_ptr = NULL;
        records[i].ctx_use = 0;        
    }
} 

void dump_recs(void)
{
    unsigned long flags;
    unsigned i;

    spin_lock_irqsave(&trace_lock, flags);
    {
        uint8_t idx = trace_idx;
        uint8_t last = 80;
        uint8_t num  = 8;

        for (i = 0; i < NUM_RECORDS; i++) {
            printk("  %u\trctx:%p\tctx:%p\tuses:%u\n", i, records[i].rctx_ptr, records[i].ctx_ptr, records[i].ctx_use);
        }

        printk("Trace(%u, %u):", last, num);
        while (last) {
            if (idx % num == 0)
                printk("\n  %u: ", idx);

            idx--; last--;
            if (trace_array[idx] > 10000)
                printk("%08x ", trace_array[idx]);
            else
                printk("%u ", trace_array[idx]);
        }
    }
    spin_unlock_irqrestore(&trace_lock, flags);

    BUG_ON(1 == 1);
}

void add_ctx(void* ctx)
{
    unsigned i;
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    for (i = 0; i < NUM_RECORDS; i++) {
        if (records[i].ctx_ptr == ctx) {
            printk("%s: already have that CTX:%p\n", __FUNCTION__, ctx);
            dump_recs();
            spin_unlock_irqrestore(&trace_lock, flags);
            return;
        }
        if (records[i].ctx_ptr == NULL)
            break;
    }

    if (i == NUM_RECORDS) {
        printk("%s: already have %u records\n", __FUNCTION__, i);
        dump_recs();
        spin_unlock_irqrestore(&trace_lock, flags);
        return;
    }
    
    printk("%s ctx:%p in slot:%u\n", __FUNCTION__, ctx, i);
    records[i].ctx_ptr = ctx;
    spin_unlock_irqrestore(&trace_lock, flags);
}

void rm_ctx(void* ctx)
{
    unsigned i;
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    for (i = 0; i < NUM_RECORDS; i++) {
        if (records[i].ctx_ptr == ctx)
            break;
    }

    if (i == NUM_RECORDS) {
        printk("%s: didn't have CTX:%p\n", __FUNCTION__, ctx);
        dump_recs();
    spin_unlock_irqrestore(&trace_lock, flags);
        return;
    }
    
    printk("%s ctx:%p from slot:%u\n", __FUNCTION__, ctx, i);
    records[i].ctx_ptr = NULL;
    records[i].ctx_use = 0;        
    spin_unlock_irqrestore(&trace_lock, flags);
}

void chk_ctx(void* ctx)
{
    unsigned i, j;
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    for (i = 0; i < NUM_RECORDS; i++) {
        if (records[i].ctx_ptr == ctx) {
            break;
        }
    }

    if (i == NUM_RECORDS) {
        printk("%s: didn't have that record: CTX:%p\n", __FUNCTION__, ctx);
        dump_recs();
        spin_unlock_irqrestore(&trace_lock, flags);
        return;
    }

    /* save this one, re-examine others */
    j = i;

    for (i = 0; i < NUM_RECORDS; i++) {
        if (i != j) {
            if (records[i].ctx_ptr == ctx) {
                printk("%s: that CTX:%p in two places %u %u\n", __FUNCTION__, ctx, i, j);
                dump_recs();
                spin_unlock_irqrestore(&trace_lock, flags);
                return;
            }
        }
    }

    // printk("%s good check ctx:%p from slot:%u\n", __FUNCTION__, ctx, j);
    spin_unlock_irqrestore(&trace_lock, flags);
}

void add_rctx(void* rctx, void* ctx)
{
    unsigned i;
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    for (i = 0; i < NUM_RECORDS; i++) {
        if (records[i].rctx_ptr == rctx) {
            if (records[i].ctx_ptr == ctx) {
                printk("%s: already have that RCTX:%p CTX:%p\n", __FUNCTION__, rctx, ctx);
                dump_recs();
                spin_unlock_irqrestore(&trace_lock, flags);
                return;
            } else {
                printk("%s: that RCTX:%p already has a different CTX:%p\n", __FUNCTION__, rctx, ctx);
                dump_recs();
                spin_unlock_irqrestore(&trace_lock, flags);
                return;
           }
        }
        if (records[i].ctx_ptr == ctx) {
            if (records[i].rctx_ptr) {
                printk("%s: that CTX:%p already has a different RCTX:%p\n", __FUNCTION__, ctx, rctx);
                dump_recs();
                spin_unlock_irqrestore(&trace_lock, flags);
                return;
            }
            break;
        }
    }

    if (i == NUM_RECORDS) {
        printk("%s: already have %u records\n", __FUNCTION__, i);
        dump_recs();
        return;
    }

    //printk("%s rctx:%p in slot:%u with ctx:%p\n", __FUNCTION__, rctx, i, ctx);
    records[i].rctx_ptr = rctx;
    records[i].ctx_use += 1;
   
    spin_unlock_irqrestore(&trace_lock, flags);
}

void rm_rctx(void* rctx, void* ctx)
{
    unsigned i;
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    for (i = 0; i < NUM_RECORDS; i++) {
        if ((records[i].rctx_ptr == rctx) && (records[i].ctx_ptr == ctx))
            break;
    }

    if (i == NUM_RECORDS) {
        printk("%s: didn't have that record: RCTX:%p CTX:%p\n", __FUNCTION__, rctx, ctx);
        dump_recs();
        spin_unlock_irqrestore(&trace_lock, flags);
        return;
    }

    //printk("%s rctx:%p ctx:%p from slot:%u\n", __FUNCTION__, rctx, ctx, i);
    records[i].rctx_ptr = NULL;
    spin_unlock_irqrestore(&trace_lock, flags);
}

void chk_rctx(void* rctx, void* ctx)
{
    unsigned i, j;
    unsigned long flags;

    spin_lock_irqsave(&trace_lock, flags);
    for (i = 0; i < NUM_RECORDS; i++) {
        if ((records[i].rctx_ptr == rctx) && (records[i].ctx_ptr == ctx)) {
            break;
        }
    }

    if (i == NUM_RECORDS) {
        printk("%s: didn't have that record: RCTX:%p CTX:%p\n", __FUNCTION__, rctx, ctx);
        dump_recs();
        spin_unlock_irqrestore(&trace_lock, flags);
        return;
    }

    /* save this one, re-examine others */
    j = i;

    for (i = 0; i < NUM_RECORDS; i++) {
        if (i != j) {
            if (records[i].rctx_ptr == rctx) {
                printk("%s: that RCTX:%p in two places %u %u\n", __FUNCTION__, rctx, i, j);
                dump_recs();
                spin_unlock_irqrestore(&trace_lock, flags);
                return;
            }
            if (records[i].ctx_ptr == ctx) {
                printk("%s: that CTX:%p in two places %u %u\n", __FUNCTION__, ctx, i, j);
                dump_recs();
                spin_unlock_irqrestore(&trace_lock, flags);
                return;
            }
        }
    }

    //printk("%s good check rctx:%p ctx:%p from slot:%u\n", __FUNCTION__, rctx, ctx, j);
    spin_unlock_irqrestore(&trace_lock, flags);
}
