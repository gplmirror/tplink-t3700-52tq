/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * This file contains the main device structures
 *
 */

#ifndef _CIPHER_H
#define _CIPHER_H

#include <crypto/aes.h>
#include <crypto/internal/hash.h>

#include "spu.h"

#define ARC4_MIN_KEY_SIZE   1
#define ARC4_MAX_KEY_SIZE   256
#define ARC4_BLOCK_SIZE     1

#define MAX_KEY_SIZE    (ARC4_MAX_KEY_SIZE)
#define MAX_IV_SIZE     (AES_BLOCK_SIZE)
#define MAX_DIGEST_SIZE (SHA256_DIGEST_SIZE)

/* MD5, SHA1, SHA224, SHA256 all have the same block size of 64 bytes */
#define HASH_BLOCK_SIZE (64)

#define MAX_INFLIGHT           (12)    /* Max concurrent in_flight packets (all streams) */
#define MAX_STREAMS            (8)     /* Need to change EMH bitfields to make larger than 16 */
                                       /* Needs to be <= firmware value */
#define REQUEST_QUEUE_LENGTH   (1)

#define FLAGS_BUSY          BIT(0)


typedef struct {
    CIPHER_ALG alg;
    CIPHER_MODE mode;
} CipherOp;

typedef struct {
    HASH_ALG alg;
    HASH_MODE mode;
} AuthOp;


struct iproc_alg_s {
    u32 type;
    union {
        struct crypto_alg crypto;
        struct ahash_alg hash;
    } alg;
    CipherOp cipher_info;
    AuthOp auth_info;
    bool authFirst;
    bool dtls_hmac;
    int max_payload;
};


struct iproc_ctx_s {
    u8 enckey[MAX_KEY_SIZE + 4];
    unsigned int enckeylen;

    u8 authkey[MAX_KEY_SIZE + 4];
    unsigned int authkeylen;

    unsigned int digestsize;

    struct iproc_alg_s *alg;

    CipherOp    cipher;
    CIPHER_TYPE cipher_type;

    AuthOp auth;
    bool authFirst;
    unsigned max_payload;

    u8 streamID;

    struct crypto_aead *fallback_cipher;

    /* auth_type is determined during processing of request */

    uint8_t ipad[HASH_BLOCK_SIZE];
    uint8_t opad[HASH_BLOCK_SIZE];
};


struct iproc_reqctx_s {
    /* general context */
    struct crypto_async_request *parent;
    struct iproc_ctx_s *ctx;
    struct list_head node;
    
    unsigned total_todo;       /* total todo, rx'd, and sent for this request */
    unsigned total_received;
    unsigned total_sent;

    unsigned src_sent;         /* this can differ from total_sent for hashes due to nbuf carried */
                               /* from the previous req if the src wasn't % BLOCK_SIZE */
    unsigned hmac_offset;

    bool in_use;               /* is this request being serviced right now? */

    /* cipher context */
    unsigned isEncrypt;
    u8 iv_ctr[MAX_IV_SIZE];    /* CBC mode: IV.  CTR mode: counter.  Else empty. */
    unsigned iv_ctr_len;       /* = block_size if either an IV or CTR is present, else 0 */
    
    /* auth context */
    u8 buf[MAX_SPU_PKT_SIZE];
    unsigned nbuf;
    unsigned is_final;         /* is this the final for the hash op? */

    /* hmac context */
    bool is_sw_hmac;

    /* aead context */
    struct crypto_tfm *old_tfm;
    crypto_completion_t old_complete;
    void *old_data;
};


struct device_private {
    struct platform_device *pdev;

    spinlock_t q_lock;           /* device lock */

    struct list_head active_q; /* list of active/inprocess requests */

    unsigned session_count;     /* number of streams active */
    unsigned remaining_slots;   /* number of inflight requests open for use */
    unsigned stream_count;      /* monotonic counter for streamID's */
};


extern struct device_private iproc_priv;

#endif /* _CIPHER_H */
