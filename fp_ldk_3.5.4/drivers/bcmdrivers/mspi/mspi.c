/*
 * Copyright (C) 2014, Broadcom Corporation. All Rights Reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/io.h>
#include <linux/delay.h>
#include <linux/mspi.h>
#include <linux/module.h>

static void * baseAddr;

#define R_REG(reg)       ioread32(baseAddr + (reg&0xffff))
#define W_REG(reg, val)        iowrite32(val, baseAddr + (reg&0xffff))
spinlock_t lock;
/*
 * soc_mspi_init: Initialise the MSPI device
 * return: 0 for success
 */
int soc_mspi_init(void) {
    uint32_t rval;

    baseAddr = ioremap(CMIC_BASE, 0x40000);

    /* Select SPI from I2C/SPI */
    rval = R_REG(CMIC_OVERRIDE_STRAP);
    rval |= 0x24;
    W_REG(CMIC_OVERRIDE_STRAP, rval);

    /* SPI bus driven by MSPI */
    rval = R_REG(CMICM_BSPI_MAST_N_BOOT);
    rval |= 0x01;
    W_REG(CMICM_BSPI_MAST_N_BOOT, rval);

    /* Set speed and transfer size */
    rval = R_REG(MSPI_SPCR0_LSB);
    rval = 0x08;
    W_REG(MSPI_SPCR0_LSB, rval);
   
    /* Lenght of delay after serial transfer*/ 
    rval = R_REG(MSPI_SPCR1_LSB);
    rval |= 0x80;
    W_REG(MSPI_SPCR1_LSB, rval);

    /* Lenght of delay from PSC valid to SCK transition*/
    rval = R_REG(MSPI_SPCR1_MSB);
    rval |= 0x80;
    W_REG(MSPI_SPCR1_MSB, rval);

    /* Set CMIC GPIO3 as output */
    rval = R_REG(GPIO_OUTEN);
    rval |= 0x08;
    W_REG(GPIO_OUTEN, rval);

    /* Set GPIO3 as High*/
    rval = R_REG(GPIO_OUT);
    rval |= (1 << 3);
    W_REG(GPIO_OUT, rval);

    spin_lock_init(&lock);

    return 0;
}

/*
 * mspi_freq_set: Set the frequency of MSPI device.
 * @speed_hz: Frequency to set
 * return : 0 for SUCCESS, -1 for FAILURE
 * Note: This function to be implemented.
 */
int soc_mspi_freq_set(int speed_hz)
{
    int rv = 0;
    /* Setting frequency is done in init */
    return rv;


}

#define NUM_CDRAM 16
#define MIN(x,y) ((x<y)?x:y)

/*
 * soc_mspi_writeread8: Write/Read 8bit data into/from FIFO of MSPI
 * @wbuf: Address of write buffer
 * @wlen: Write length
 * @rbuf: Address of read buffer
 * @rlen: Length to read
 * return: 0 for SUCCESS, -1 for FAILURE
 */
int soc_mspi_writeread8(uint8_t *wbuf, int wlen, uint8_t *rbuf, int rlen)
{
    int i, tlen, rv = -1;
    uint8_t *wdatptr;
    uint8_t *rdatptr;
    uint8_t *datptr;
    uint32_t rval=0;
    unsigned int chunk;
    unsigned int  queues;
    int bytes;
    unsigned long flags;

    spin_lock_irqsave(&lock, flags);
    /* Set 8 bits per transfet */
    rval = R_REG(MSPI_SPCR0_MSB);
    rval |= 0x00000020;
    W_REG(MSPI_SPCR0_MSB, rval);

    /* Clear Status register before transfer */
    W_REG(MSPI_STATUS, 0x00);

    bytes=wlen + rlen;
    tlen = wlen ;
    rdatptr = rbuf;
    wdatptr = wbuf;
    while(bytes){
        /* Clear Status register before every transfer */
	W_REG(MSPI_STATUS, 0x00);    
        chunk = MIN(bytes, NUM_CDRAM );
        queues = chunk;
        bytes -= chunk;
        
        if ((wbuf != NULL) && (tlen > NUM_CDRAM)) {
            //udelay(1000);
            for (i=0; i<chunk; i++){
		W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) *wdatptr);
                wdatptr++;
            }
        }else if ((wbuf != NULL) && (tlen >0)){
            //udelay(1000);
            for (i=0; i<chunk; i++) {
                /* Use only Even index of TXRAM for 8 bit xmit */
                if(i<tlen){
		    W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) *wdatptr);
                    wdatptr++;
                }else{
		    W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) 0xff);
                }
            }
        }else{
            for(i=0;i<chunk;i++)
		W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) 0xff);
        }

        for (i=0; i<chunk; i++) {
            /* Release CS ony on last byute */
            if(bytes==0)
		W_REG((MSPI_CDRAM_BASE + (4*i)), (i == (chunk-1)) ? 0 : 0x80);
            else
		W_REG((MSPI_CDRAM_BASE + (4*i)), 0x80);
        }

        /* Set queue pointers */
	W_REG(MSPI_NEWQP, 0);
	W_REG(MSPI_ENDQP, (chunk-1));

        /* Start SPI transfer */
        rval = 0xc0; /* SPE=1, SPIFIE=WREN=WRT0=LOOPQ=HIE=HALT=0 */
	W_REG(MSPI_SPCR2, rval);

        rval = 0;
 
        do {
            rval ++;
	    if (R_REG(MSPI_STATUS) & 0x1) {
                rv = 0;
                break;
            }
        } while(rval < 10000);

        if (rv == -1) {
            return -1;
        }

        if(tlen >NUM_CDRAM){
            tlen -=chunk;
        }else{
        
            if ((rbuf != NULL) && (rlen > 0)) {
            /* CMLAI workarround delay time*/
                //udelay(2000);

                for (i=tlen; i<chunk; i++) {
                    /* Use only Odd index of RXRAM for 8 bit Recv */
		    *rdatptr = (uint8_t) (R_REG(MSPI_RXRAM_BASE + 4*(2*i+1)) & 0xff);
                    rdatptr++;
                }
            }
            tlen =0;
        }
    }
    spin_unlock_irqrestore(&lock, flags);
    return 0;
}

/*
 * soc_mspi_read8: Read 8bit data from MSPI FIFO
 * @buf: Address of read buffer
 * len: Length to read
 * return: 0 for SUCCESS, -1 for FAILURE
 */
int soc_mspi_read8(uint8_t *buf, int len)
{
    return  soc_mspi_writeread8(NULL, 0, buf, len);
}

/*
 * soc_mspi_write8: Write 8bit data into MSPI FIFO
 * @buf: Address of write buffer
 * @len: Length to write
 * return: 0 for SUCCESS, -1 for FAILURE
 */
int soc_mspi_write8(uint8_t *buf, int len)
{
    return  soc_mspi_writeread8(buf, len, NULL, 0);
}

/*
 * max_mspi_ss_low: chip select low
 */
void max_mspi_ss_low(void){
    uint32_t rval;
    rval = R_REG(GPIO_OUT);
    rval &= ~(1 << 3);
    W_REG(GPIO_OUT, rval);

    return;
}

/*
 * max_mspi_ss_high: chip select high
 */
void max_mspi_ss_high(void){
    uint32_t rval;

    rval = R_REG(GPIO_OUT);
    rval |= (1 << 3);
    W_REG(GPIO_OUT, rval);

    return;
}

/*
 * max_spi_write: MSPI write
 * @datbuf: Address of data buffer
 * @datlen: Lenght of data to write
 * return: 0 for SUCCESS, -1 for FAILURE
 */
unsigned char max_spi_write(unsigned char *datbuf,int datlen)
{
    return soc_mspi_write8(datbuf,datlen);
}

/*
 * max_spi_read: MSPI read
 * @datbuf: Address of data buffer
 * @datlen: Lenght of data to write
 * return: 0 for SUCCESS, -1 for FAILURE
 */

unsigned char max_spi_read(unsigned char *datbuf,int datlen)
{
    return soc_mspi_read8(datbuf,datlen);
}

/*
 * max_spi_rw: MSPI write & write
 * @datbuf: Address of data buffer
 * @datlen: Lenght of data to write
 * return: 0 for SUCCESS, -1 for FAILURE
 */
unsigned char max_spi_rw(unsigned char *wbuf,int wlen,unsigned char *rbuf,int rlen)
{
    return soc_mspi_writeread8(wbuf,wlen,rbuf,rlen);
}


EXPORT_SYMBOL(soc_mspi_read8);
EXPORT_SYMBOL(soc_mspi_write8);
EXPORT_SYMBOL(soc_mspi_freq_set);
EXPORT_SYMBOL(max_mspi_ss_low);
EXPORT_SYMBOL(max_mspi_ss_high);


