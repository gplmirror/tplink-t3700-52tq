/*****************************************************************************
* Copyright 2008 - 2014 Broadcom Corporation.  All rights reserved.
*
* Unless you and Broadcom execute a separate written software license
* agreement governing use of this software, this software is licensed to you
* under the terms of the GNU General Public License version 2, available at
* http://www.broadcom.com/licenses/GPLv2.php (the "GPL").
*
* Notwithstanding the above, under no circumstances may you combine this
* software in any way with any other Broadcom software provided under a
* license other than the GPL, without Broadcom's express prior written
* consent.
*****************************************************************************/



#include <linux/init.h>
#include <linux/device.h>
#include <linux/dma-mapping.h>
#include <linux/kernel.h>
#include <linux/version.h>

#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/errno.h>
#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/interrupt.h>
#include <linux/bitops.h>
#include <linux/err.h>
#include <asm/mach/irq.h>
//#include <mach/platform.h>
#include <linux/version.h>
#include <mach/irqs.h>
//#include <asm-arm/irq.h>
//#include <asm-arm/delay.h>
#include <asm/cache.h>
#include <asm/io.h>
#include <asm/processor.h>	/* Processor type for cache alignment. */
#include <asm/uaccess.h>	/* for copy_from_user */
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/pwm.h>

#include <mach/socregs-cygnus.h>
#include "iproc_lcd_pl111.h"

/**********************************************************************
 *  Module Parameters
 **********************************************************************/
static int lcdwidth = IPROC_LCD_RES_DEFAULT_X;
module_param(lcdwidth, int, 0);
MODULE_PARM_DESC(lcdwidth, "LCD panel width connected to IPROC. LCD panel valid sizes: 800x480, 720x480, 640x480, 480x272");

static int lcdheight = IPROC_LCD_RES_DEFAULT_Y;
module_param(lcdheight, int, 0);
MODULE_PARM_DESC(lcdheight, "LCD panel height connected to IPROC. LCD panel valid sizes: 800x480, 720x480, 640x480, 480x272");

static int clockdivisor = IPROC_LCD_INVALID_CLKDIV;
module_param(clockdivisor, int, 0);
MODULE_PARM_DESC(clockdivisor, "LCD panel clock divisor: 0-4095");

/**********************************************************************
 *  Function Prototypes
 **********************************************************************/
	int __init iproc_lcd_probe(struct platform_device *pldev);
	int __init iproc_lcd_remove(struct platform_device *pldev);
	static irqreturn_t iproc_lcd_irq(int irq, void *dev_id);
static int iproc_lcd_fb_check_var(struct fb_var_screeninfo *var, struct fb_info *info);
static int iproc_lcd_fb_setcmap(struct fb_cmap *cmap, struct fb_info *info);
static int iproc_lcd_fb_setcolreg(unsigned regno, unsigned red, unsigned green, unsigned blue, unsigned transp, struct fb_info *info);

	static int iproc_lcd_fb_mmap(struct fb_info *info, struct vm_area_struct *vma);
static int iproc_lcd_fb_set_par(struct fb_info *info);
static int iproc_lcd_blank(int blank_mode, struct fb_info *info);
static int iproc_lcd_fb_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg);
static void iproc_lcd_poweron(struct iproc_lcd_ctx *pCTX);
static void iproc_lcd_poweroff(struct iproc_lcd_ctx *pCTX);

static ssize_t iproc_lcd_store(struct kobject *kobj, struct kobj_attribute *attr,
				const char * buf, size_t n);
static ssize_t iproc_lcd_show(struct kobject *kobj, struct kobj_attribute *attr,
			       char *buf);

/* either let gpio do it, or we do gpio */
#define USE_IPROC_GPIO   1


/*Intialization & structures*/
char const iproc_lcd_name[] = "iproc-lcd";

static struct resource iproc_lcd_resources[] = {
	[0] = {
	.flags  = IORESOURCE_MEM,
	.start	 = LCDTiming0, //START_LCD_CFG, /* defined in include/asm-arm/arch/iproc_reg.h */
	.end 	 = LCDTiming0 + 0xfff//END_LCD_CFG,
	},
	[1] = {
	.flags  = IORESOURCE_IRQ,
	.start  = 183 ,/*ASIU_LCD_INTR bit[26...22] */
	//IRQ_OLCDC,	/* open mode??? */ /* defined in include/asm-arm/arch/irqs.h */
	},

	[2] = {
	.flags  = IORESOURCE_IRQ,
	.start  = 184 ,/*ASIU_LCD_INTR bit[26...22] */
	//IRQ_OLCDC,   /* open mode??? */ /* defined in include/asm-arm/arch/irqs.h */
	},
	[3] = {
	.flags  = IORESOURCE_IRQ,
	.start  = 185 ,/*ASIU_LCD_INTR bit[26...22] */
	//IRQ_OLCDC,   /* open mode??? */ /* defined in include/asm-arm/arch/irqs.h */
	},
	[4] = {
	.flags  = IORESOURCE_IRQ,
	.start  = 186 ,/*ASIU_LCD_INTR bit[26...22] */
	//IRQ_OLCDC,   /* open mode??? */ /* defined in include/asm-arm/arch/irqs.h */
	},
	[5] = {
	.flags  = IORESOURCE_IRQ,
	.start  = 187 ,/*ASIU_LCD_INTR bit[26...22], */
	//IRQ_OLCDC,   /* open mode??? */ /* defined in include/asm-arm/arch/irqs.h */
	},

};


static struct kobj_attribute iproc_lcd_attr =
	__ATTR(lcd, 0644, iproc_lcd_show, iproc_lcd_store);

/* cached LCD CTX for sysfs hooks */
static struct iproc_lcd_ctx *iproc_lcd_ctx = NULL;

unsigned int iproc_lcd_confgure_pwm(struct iproc_lcd_ctx *pCTX, int flag)
{
	struct pwm_device *pwm;
	unsigned int i = 0;

	if(pCTX->lpwm == NULL) 
	{
	  pwm = pwm_request(0, "iproc-lcd");
	  pCTX->lpwm = pwm;
	}
	i=  pwm_config(pwm , 10000, 1000000);
	if(flag)pwm_enable(pwm);
}

/**********************************************************************
 *   This function calls the device structure.
 *   Input Parameter:
 *		dev - pointer to the struct device
 **********************************************************************/
static void iproc_lcd_release (struct device *dev) {};

/**********************************************************************
 *  This structure defines the methods to be called by a platform device
 *  during the lifecycle of a device
**********************************************************************/
static u64 iproc_lcd_dmamask = ~(u64)0;
static struct platform_device iproc_lcd_pdev = {

	.name   = iproc_lcd_name,
	.id	    = -1,
	.dev    = {
			.dma_mask           = &iproc_lcd_dmamask,
			.coherent_dma_mask  = 0xffffffff, /* if not set dma_alloc will have error "coherent DMA mask is unset" */
			.release            = iproc_lcd_release,
		},

	.resource        = iproc_lcd_resources,
 	.num_resources   = ARRAY_SIZE(iproc_lcd_resources),

};
#ifdef CONFIG_SUSPEND
static int iproc_lcd_suspend(struct platform_device *pdev, pm_message_t state)
{	
	struct pwm_device *pwm;
	unsigned int i = 0;
	struct iproc_lcd_ctx  *pCTX = iproc_lcd_ctx;

	if(pCTX->lpwm == NULL) 
	{
	  return 0;
	}
	pwm = pCTX->lpwm;
	pwm_disable(pwm);
	return 0;
}

static int iproc_lcd_resume(struct platform_device *pdev)
{
	struct pwm_device *pwm;
	unsigned int i = 0;
	struct iproc_lcd_ctx  *pCTX = iproc_lcd_ctx;

	if(pCTX->lpwm == NULL) 
	{
	  return 0;
	}
	pwm = pCTX->lpwm;
	pwm_config(pwm , 10000, 1000000);
	pwm_enable(pwm);	
	return 0;
}

#endif
/**********************************************************************
 * This structure defines the methods to be called by a bus driver
 * during the lifecycle of a device on that bus.
**********************************************************************/

	static struct platform_driver iproc_lcd_driver =
	{
		.probe = iproc_lcd_probe,
		.remove = iproc_lcd_remove,
		.driver =
		{
			.name = iproc_lcd_name,
		},
#ifdef CONFIG_SUSPEND
		.suspend = iproc_lcd_suspend,
		.resume = iproc_lcd_resume,
#endif
	};

static struct fb_ops iproc_lcd_ops = {
	.owner		    = THIS_MODULE,
	.fb_check_var	= iproc_lcd_fb_check_var,
	.fb_setcmap		= iproc_lcd_fb_setcmap,
	.fb_setcolreg	= iproc_lcd_fb_setcolreg,
	.fb_set_par	    = iproc_lcd_fb_set_par,
	.fb_mmap	    = iproc_lcd_fb_mmap,
	.fb_ioctl       = iproc_lcd_fb_ioctl,
	.fb_blank	    = iproc_lcd_blank,
};

static struct fb_var_screeninfo lcd_var_data __initdata = {
	.xres  =  IPROC_LCD_800x480_PPL,			/* visible resolution		*/
	.yres  =  IPROC_LCD_800x480_LPP,
	.xres_virtual =   IPROC_LCD_800x480_PPL,		/* virtual resolution		*/
	.yres_virtual  =  IPROC_LCD_800x480_LPP,
	.xoffset =  0,			/* offset from virtual to visible */
	.yoffset =  0,			/* resolution			*/

	.bits_per_pixel  = IPROC_LCD_RES_BBP_DEF,		/* guess what			*/
//	.grayscale =  0,		/* != 0 Graylevels instead of colors */

	.red =		{ 0, 8, 0 }, /* struct fb_bitfield: offset, length, msb_right(msb is at right) */
	.green =	{ 0, 8, 0 },
	.blue =		{ 0, 8, 0 },

	.nonstd =  0,			/* != 0 Non standard pixel format */

	.activate = FB_ACTIVATE_NOW,			/* see FB_ACTIVATE_*		*/

	.height = 928,			/* height of picture in mm    */
	.width = 525,		/* width of picture in mm     */


	/* Timing: All values in pixclocks, except pixclock (of course) */
	.pixclock = 25000000,		/* pixel clock in ps (pico seconds) */
	.left_margin = 88,		/* time from sync to picture	*/
	.right_margin = 40,	/* time from picture to sync	*/
	.upper_margin = 32,	/* time from sync to picture	*/
	.lower_margin = 13,
	.hsync_len = 48,		/* length of horizontal sync	*/
	.vsync_len = 3,		/* length of vertical sync	*/
	.sync = -1,			/* see FB_SYNC_*		*/
	.vmode = FB_VMODE_NONINTERLACED, 

};

static struct fb_fix_screeninfo lcd_fix_data __initdata = {
	.id =		"IPROC LCD",
	.type =		FB_TYPE_PACKED_PIXELS,
	.type_aux = 0,
	.xpanstep =	0,
	.ypanstep =	0,
	.ywrapstep = 0,
	.accel =	FB_ACCEL_NONE,
};



/**********************************************************************
 *	iproc_lcd_allocate_frame_buffer():
 *	Allocate frame buffer space for LCD
 *
 *  Input parameters:
 *         pCTX: struct iproc_lcd_ctx *
 *
 *  Return value:
 *	       0 : success
 *
 **********************************************************************/
static int iproc_lcd_allocate_frame_buffer(struct iproc_lcd_ctx *pCTX, u32 nFrameBufLen)
{
	nFrameBufLen = IPROC_LCD_FRAME_ALIGN(nFrameBufLen);

	// use dma_alloc_writecombine to allocate. kmalloc() fails with large size allocation
	pCTX->pFrameBuf_CPU = (u32 *)dma_alloc_writecombine(pCTX->dev, nFrameBufLen, &pCTX->pFrameBuf_DMA, GFP_KERNEL | GFP_DMA);

	if (pCTX->pFrameBuf_CPU) {
		//memset(fbi->pFrameBuf_CPU, 0xf0, fbi->nFrameBufLen);

		pCTX->fb->fix.smem_start = pCTX->pFrameBuf_DMA;
		pCTX->nFrameBufLen = nFrameBufLen;
		pCTX->fb->fix.smem_len = pCTX->nFrameBufLen;
		pCTX->pFrameBuf_CPU_2 = (u32 *)((uint32_t)pCTX->pFrameBuf_CPU + nFrameBufLen/2) ;
		pCTX->pFrameBuf_DMA_2 = (dma_addr_t)((uint32_t)pCTX->pFrameBuf_DMA + nFrameBufLen/2);

		return 0;
	} else {
		printk(KERN_ERR "dma_alloc_writecombine failed for nFrameBufLen = %d\n",nFrameBufLen);
		return -ENOMEM;
	}
}

static inline void iproc_lcd_release_frame_buffer(struct iproc_lcd_ctx *pCTX)
{
	dma_free_writecombine(pCTX->dev, pCTX->nFrameBufLen, pCTX->pFrameBuf_CPU, pCTX->pFrameBuf_DMA);
}


/**********************************************************************
 *	iproc_lcd_fb_mmap():
 *	memory map the frame buffer to user space
 *
 *  Input parameters:
 *
 *
 *  Return value:
 *	       0: success
 *
 **********************************************************************/
static int iproc_lcd_fb_mmap(struct fb_info *info, struct vm_area_struct *vma)
{
	struct iproc_lcd_ctx *pCTX = info->par;
	unsigned long off = vma->vm_pgoff << PAGE_SHIFT;
	size_t size = vma->vm_end - vma->vm_start;
	int ret = 0;

	/*
	 * printk("mmap: %x %x %x %x %x", (unsigned int)pCTX->dev, (unsigned int)vma, (unsigned int)pCTX->pFrameBuf_CPU,
	 * 	(unsigned int)pCTX->pFrameBuf_DMA, (unsigned int)pCTX->nFrameBufLen);
	 */

	if (size <= info->fix.smem_len) 
	{

		/* Re-Assign frame buffer address to LCD base register.
		   This is because other program (like vdec) might have set the register to vdec buffer,
		   since every image operation needs mmap(), we set the lcd base register here to make sure image can be showed
		 */

		/* Now do the map */
		ret =  dma_mmap_writecombine(pCTX->dev, vma, pCTX->pFrameBuf_CPU, pCTX->pFrameBuf_DMA, size);		
		__raw_writel((unsigned int)pCTX->pFrameBuf_DMA, LCDREG_UPBASE);
		return ret;
	}

	PDEBUG("iproc_lcd_fb_mmap() fail\n");

	return -EINVAL;
}


/**********************************************************************
 *	iproc_lcd_init_registers():
 *	Initialize LCD registers for timing related values.
 *
 *  Input parameters:
 *         pCTX: struct iproc_lcd_ctx *
 *
 *  Return value:
 *	       0
 *
 **********************************************************************/
static int iproc_lcd_init_registers(struct iproc_lcd_ctx *pCTX)
{
    u32 data,temp;

    /* CTRL */
    data = __raw_readl(LCDREG_CONTROL);

    if (pCTX->panel.bTFT) data |= LCD_F_LcdTFT_MASK;
    else                  data &= ~LCD_F_LcdTFT_MASK;

    data &= ~LCD_F_LcdBpp_MASK;
    data |= 1 << LCD_F_BGR_R;
    data |= LCD_BPP2REGVAL(IPROC_LCD_RES_BBP_DEF);
    __raw_writel(data, LCDREG_CONTROL); // always set to little endian

    /* TIMING0 */
    temp = (pCTX->panel.nPPL)/16;
    data = ((pCTX->panel.nHBP -1)<<LCD_F_HBP_R)  | ((pCTX->panel.nHFP - 1)<<LCD_F_HFP_R) |
           ((pCTX->panel.nHSW - 1)<<LCD_F_HSW_R) |  (temp<<LCD_F_PPL_R);
    __raw_writel(data, LCDREG_TIMING0);

    /* TIMING1 */
    data = ((pCTX->panel.nVBP -1)<<LCD_F_VBP_R) | ((pCTX->panel.nVFP - 1)<<LCD_F_VFP_R)|
           ((pCTX->panel.nVSW -1)<<LCD_F_VSW_R) |  (pCTX->panel.nLPP - 1);
    __raw_writel(data, LCDREG_TIMING1);

    /* TIMING2 */
    data = (clockdivisor & LCD_F_PCD_LO_MASK); /* this is the LO part */
    temp = (clockdivisor - data) >> LCD_F_PCD_LO_SIZE; /* temp is the left high part */
    data |= (temp<<LCD_F_PCD_HI_R) | ((pCTX->panel.nPPL - 1)<<LCD_F_CPL_R);
    __raw_writel(data, LCDREG_TIMING2);
	
    /* init GPIO   */
    iproc_lcd_confgure_pwm(pCTX, 1);

    /* TIMING3 (LINE END CONTROL) */
    data = LCD_F_LEE_MASK | (pCTX->panel.nLED - 1);
    __raw_writel(data, LCDREG_TIMING3);

    /* FRAME BASE ADDRESS */
    __raw_writel((unsigned int)pCTX->pFrameBuf_DMA, LCDREG_UPBASE);	

    /* Enable all LCD interrupts */
    /* Note: can't enable FIFO underrun int, it will be too much of them. Or enable and then increase the clock divider if them come */
    /* We do not enable any int for now */
    __raw_writel( ( /*0fixme */ LCD_F_MBERRORRIS_MASK | LCD_F_VcompRIS_MASK | LCD_F_LNBURIS_MASK | LCD_F_FUFRIS_MASK ), LCDREG_INTMASK);
	return 0;
}

/**********************************************************************
 *	bcm5892lcd_poweron():
 *	Power on the LCD panel
 *
 *  Input parameters:
 *         struct iproc_lcd_ctx *pCTX
 *
 *  Return value:
 *	       void
 *
 **********************************************************************/
static void iproc_lcd_poweron(struct iproc_lcd_ctx *pCTX)
{
	uint32_t data;

	/*this is the power on sequence */
	data = __raw_readl(LCDREG_CONTROL);
	data |= LCD_F_LcdEn_MASK;
	__raw_writel(data, LCDREG_CONTROL);
	mdelay(10);
	data |= LCD_F_LcdPwr_MASK;
	__raw_writel(data, LCDREG_CONTROL);
}


/**********************************************************************
 *	iproc_lcd_poweroff():
 *	Power off the LCD panel
 *
 *  Input parameters:
 *         struct iproc_lcd_ctx *pCTX
 *
 *  Return value:
 *	       void
 *
 **********************************************************************/
static void iproc_lcd_poweroff(struct iproc_lcd_ctx *pCTX)
{
	uint32_t data;

	/* this is the power off sequence */
	data = __raw_readl(LCDREG_CONTROL);
	data &= ~LCD_F_LcdPwr_MASK;
	__raw_writel(data, LCDREG_CONTROL);
	mdelay(10);
	data &= ~LCD_F_LcdEn_MASK;
	__raw_writel(data, LCDREG_CONTROL);
}


/**********************************************************************
 *	iproc_lcd_fb_check_var():
 *	Get the video params out of 'var'. If a value doesn't fit, round it up,
 *
 *  Input parameters:
 *         var:  struct fb_var_screeninfo
 *         info: struct fb_info
 *
 *  Return value:
 *	       0
 *
 **********************************************************************/
static int iproc_lcd_fb_check_var(struct fb_var_screeninfo *var, struct fb_info *info)
{
	//struct iproc_lcd_ctx *pCTX = info->par;

	PDEBUG("lcd_check_var, bpp=%d\n", var->bits_per_pixel);

	/* validate x/y resolution */

	if (var->yres > lcdheight)
		var->yres = lcdheight;

	if (var->xres > lcdwidth)
		var->xres = lcdwidth;

	/* validate bpp */

	if (var->bits_per_pixel > IPROC_LCD_RES_BBP_MAX)
		var->bits_per_pixel = IPROC_LCD_RES_BBP_DEF;

	/* set r/g/b positions */

	switch (var->bits_per_pixel) {
		case 12: /* 4:4:4 */
			var->red.offset		= 0;
			var->green.offset	= 4;
			var->blue.offset	= 8;
			var->transp.offset	= 0;
			var->red.length		= 4;
			var->green.length	= 4;
			var->blue.length	= 4;
			var->transp.length	= 0;
			break;
		case 16: /* 5:6:5 */
			var->red.offset		= 0;
			var->green.offset	= 5;
			var->blue.offset	= 11;
			var->transp.offset	= 0;
			var->red.length		= 5;
			var->green.length	= 6;
			var->blue.length	= 5;
			var->transp.length	= 0;
			break;
		case 15: /* 1:5:5:5 */
			var->red.offset		= 0;
			var->green.offset	= 5;
			var->blue.offset	= 10;
			var->transp.offset	= 15;
			var->red.length		= 5;
			var->green.length	= 5;
			var->blue.length	= 5;
			var->transp.length	= 1;
			break;
		case 24: /* 8:8:8 */ /* there will be one byte wasted so effectively = 32 */
		case 32: /* 8:8:8 */
			var->red.offset		= 0;
			var->green.offset	= 8;
			var->blue.offset	= 16;
			var->transp.offset	= 0;
			var->red.length		= 8;
			var->green.length	= 8;
			var->blue.length	= 8;
			var->transp.length	= 0;
			break;
		case 1: /* indexed color */
		case 2:
		case 4:
		case 8:
			var->red.length		= var->bits_per_pixel;
			var->red.offset		= 0;
			var->green.length	= var->bits_per_pixel;
			var->green.offset	= 0;
			var->blue.length	= var->bits_per_pixel;
			var->blue.offset	= 0;
			var->transp.length	= 0;
			break;
		default:
			return -EINVAL;
	}

	return 0;
}


/**********************************************************************
 *	iproc_lcd_fb_set_par():
 *	Set var parameters
 *
 *  Input parameters:
 *         info: struct fb_info
 *
 *  Return value:
 *	       0
 *
 **********************************************************************/
static int iproc_lcd_fb_set_par(struct fb_info *info)
{
	struct iproc_lcd_ctx *pCTX = info->par;
	struct fb_var_screeninfo *var = &info->var;
	u32    data;

	PDEBUG("lcd_set_par, bpp=%d\n", var->bits_per_pixel);

	if (var->bits_per_pixel >= 16)
		pCTX->fb->fix.visual = FB_VISUAL_TRUECOLOR; /* no hw palette needed */
	else
		pCTX->fb->fix.visual = FB_VISUAL_PSEUDOCOLOR; /* use hw palette */

	pCTX->fb->fix.line_length = (var->xres * var->bits_per_pixel) / 8;

	/* activate this new color info, e.g. set the CTRL reg */
    data = __raw_readl(LCDREG_CONTROL);
    data &= ~LCD_F_LcdBpp_MASK;
    data |= LCD_BPP2REGVAL(var->bits_per_pixel);
    __raw_writel(data, LCDREG_CONTROL);

	return 0;
}

static void schedule_palette_update(struct iproc_lcd_ctx *pCTX)
{
	unsigned long flags;
	unsigned long irqen;

	spin_lock_irqsave(&(pCTX->palette_lock), flags);

	if (!pCTX->palette_ready) {
		pCTX->palette_ready = 1;

		/* enable VCOMP IRQ */
		irqen = __raw_readl(LCDREG_INTMASK);
		irqen |= LCD_F_VcompRIS_MASK;
		__raw_writel(irqen, LCDREG_INTMASK); // will be re-disable after it's done
	}

	spin_unlock_irqrestore(&(pCTX->palette_lock), flags);
}

static inline unsigned int chan_to_field(unsigned int chan, struct fb_bitfield *bf)
{
	chan &= 0xffff;
	chan >>= 16 - bf->length;
	return chan << bf->offset;
}

/**********************************************************************
 *	iproc_lcd_fb_setcmap():
 *	Set the whole palette
 *
 *  Input parameters:
 *
 *
 *  Return value:
 *	       0: success; 1: fail
 *
 **********************************************************************/
static int iproc_lcd_fb_setcmap(struct fb_cmap *cmap, struct fb_info *info)
{
	u16 *red, *green, *blue, *transp;
	int i, start, rc = 0;

	red = cmap->red;
	green = cmap->green;
	blue = cmap->blue;
	transp = cmap->transp;
	start = cmap->start;

	for (i = 0; i < cmap->len; i++) {
		u_int hred, hgreen, hblue, htransp = 0xffff;

		hred   = *red++;
		hgreen = *green++;
		hblue  = *blue++;
		if (transp)	htransp = *transp++;
		rc = iproc_lcd_fb_setcolreg (start++, hred, hgreen, hblue, htransp, info);
		if (rc)
			break;
	}


	return rc;
}

/**********************************************************************
 *	iproc_lcd_fb_setcolreg():
 *	Set one item in the palette
 *
 *  Input parameters:
 *
 *
 *  Return value:
 *	       0: success; 1: fail
 *
 **********************************************************************/
static int iproc_lcd_fb_setcolreg(unsigned regno,
			       unsigned red, unsigned green, unsigned blue,
			       unsigned transp, struct fb_info *info)
{
	struct iproc_lcd_ctx *pCTX = info->par;
	unsigned int val;

	//PDEBUG("setcol: regno=%d, rgb=%d,%d,%d\n", regno, red, green, blue);

	switch (pCTX->fb->fix.visual) {
	case FB_VISUAL_TRUECOLOR:
		/* true-colour, use pseuo-palette */

		if (regno < IPROC_LCD_PSEUDO_PALETTE_ENTRY) {
			u32 *pal = pCTX->fb->pseudo_palette;

			val  = chan_to_field(red,   &pCTX->fb->var.red);
			val |= chan_to_field(green, &pCTX->fb->var.green);
			val |= chan_to_field(blue,  &pCTX->fb->var.blue);

			pal[regno] = val;
		}
		break;

	case FB_VISUAL_PSEUDOCOLOR: /* using one hw palette, we set to our sw palette first */
		if (regno < IPROC_LCD_HW_PALETTE_ENTRY) {

			pCTX->palette_buffer[regno] = LCDPALETTE_RGBI(red, green, blue, transp);

			schedule_palette_update(pCTX);
		}

		break;

	default:
		return 1;   /* unknown type */
	}

	return 0;
}


/**********************************************************************
 *	iproc_lcd_write_palette():
 *	Update the hardware palette with the palette data from software context
 *
 *  Input parameters:
 *         pCTX: struct iproc_lcd_ctx *
 *
 *  Return value:
 *	       void
 *
 **********************************************************************/
static void iproc_lcd_write_palette(struct iproc_lcd_ctx *pCTX)
{
	u32 i;
	u32 nVal;

	for (i = 0; i < IPROC_LCD_HW_PALETTE_ENTRY; i++) {
		if (pCTX->palette_buffer[i] == PALETTE_BUFF_CLEAR)
			continue;

		nVal = __raw_readl(LCDREG_PALETTE_i (i/2));

		if (i & 0x01) /* change MSB */
			nVal = (pCTX->palette_buffer[i] << 16) | (nVal & 0x00FFFF);
		else /* change LSB */
			nVal = (pCTX->palette_buffer[i]) | (nVal & 0xFFFF0000);

		__raw_writel(nVal, LCDREG_PALETTE_i (i/2));

		pCTX->palette_buffer[i] = PALETTE_BUFF_CLEAR;
	}
}

/*
 *		iproc_lcd_blank
 *	@blank_mode: the blank mode we want.
 *	@info: frame buffer structure that represents a single frame buffer
 *
 *	Blank the screen if blank_mode != 0, else unblank. Return 0 if
 *	blanking succeeded, != 0 if un-/blanking failed due to e.g. a
 *	video mode which doesn't support it. Implements VESA suspend
 *	and powerdown modes on hardware that supports disabling hsync/vsync:
 *
 *	Returns negative errno on error, or zero on success.
 *
 */
static int iproc_lcd_blank(int blank_mode, struct fb_info *info)
{
	struct iproc_lcd_ctx *pCTX = info->par;

	if (blank_mode == FB_BLANK_UNBLANK)
		iproc_lcd_poweron(pCTX);
 	else 
		iproc_lcd_poweroff(pCTX);

	return 0;
}


/**********************************************************************
 *	iproc_lcd_fb_ioctl():
 *	Custom IOCTL handling. Currently is only used for setting of LCD buffer address
 *
 *  Input parameters:
 *         info: struct fb_info, ...
 *
 *  Return value:
 *	       0
 *
 **********************************************************************/
 static int iproc_lcd_fb_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg)
{
	int retval = 0;
	struct iproc_lcd_ctx *pCTX = info->par;
	struct fb_var_screeninfo vinfo;
	u32 addr = pCTX->pFrameBuf_DMA;
	switch (cmd) {
		case FBIO_SET_BUFFER_ADDR:
			if (copy_from_user(&vinfo, (void *)arg, sizeof(struct fb_var_screeninfo)))
				return -EFAULT;
			sleep_on(&pCTX->done);

		/* update frame buffer base address if it's changed */
		if( vinfo.yoffset == lcdheight ) __raw_writel((unsigned int)pCTX->pFrameBuf_DMA_2, LCDREG_UPBASE);
		else __raw_writel((unsigned int)pCTX->pFrameBuf_DMA, LCDREG_UPBASE);

			/* PDEBUG("ioctl set buf addr = %x\n", addr); */

			/* change of frame buffer address */
			/* more elegant way is to wait for the interrupt then change it 
			__raw_writel((unsigned int)pCTX->pFrameBuf_DMA, LCDREG_UPBASE);*/


			break;

		default:
			break;
	}

	return retval;
}

/**********************************************************************
 *  iproc_lcd_irq
 *
 *  The interrupt handling routine
 *
 *  Input parameters:
 *     irq - The Interrupt Number dedicated to us.
 *     drv_ctx - The driver context
 *     r -
 *
 *  Return value:
 *     IRQ_RETVAL(1): Interrupt handling is Succesful
 *     IRQ_RETVAL(0): Interrupt handling is Not Succesful
 **********************************************************************/
static irqreturn_t iproc_lcd_irq(int irq, void *drv_ctx)
{
	struct iproc_lcd_ctx  *pCTX = drv_ctx;
	unsigned long flags;
	unsigned long irqen;
	u32 lcdirq;

	lcdirq = __raw_readl(LCDREG_INTSTATUS);

	/* Master Bus Error Interrupt */
	if (lcdirq & LCD_F_MBERRORRIS_MASK) {

		__raw_writel(LCD_F_MBERRORRIS_MASK, LCDREG_INTCLEAR); // clear interrupt
	}

	/* Vertical Compare Interrupt */
	if (lcdirq & LCD_F_VcompRIS_MASK) {
//		PDEBUG("IRQ Vcomp\n");

		spin_lock_irqsave(&(pCTX->palette_lock), flags);

		/* update palette if there's one */
		if (pCTX->palette_ready)
			iproc_lcd_write_palette(pCTX);

		pCTX->palette_ready = 0;

		/* disable VCOMP IRQ */
		irqen = __raw_readl(LCDREG_INTMASK);
		irqen &= ~LCD_F_VcompRIS_MASK;
		__raw_writel(irqen, LCDREG_INTMASK);
		

		spin_unlock_irqrestore(&(pCTX->palette_lock), flags);
	
		__raw_writel(LCD_F_VcompRIS_MASK, LCDREG_INTCLEAR); // clear interrupt
	}

	/* Next Base Address Update Interrupt */
	if (lcdirq & LCD_F_LNBURIS_MASK) {

		/* update frame buffer base address if it's changed */
		wake_up(&pCTX->done);
		__raw_writel(LCD_F_LNBURIS_MASK, LCDREG_INTCLEAR); // clear interrupt
	}

	/* FIFO Underflow interrupt */
	if (lcdirq & LCD_F_FUFRIS_MASK) {

		__raw_writel(LCD_F_FUFRIS_MASK, LCDREG_INTCLEAR); // clear interrupt
	}
	return IRQ_HANDLED;
}


/**********************************************************************
 *  iproc_lcd_store
 *
 *  Set the power state of the LCD panel
 *
 **********************************************************************/

static ssize_t iproc_lcd_store(struct kobject *kobj, struct kobj_attribute *attr,
				const char * buf, size_t n)
{
	int state;

	if (sscanf(buf, "%d", &state) != 1) {
		printk(KERN_ERR "%s: Invalid value\n", __func__);
		return -EINVAL;
	}
	if (state)
		iproc_lcd_poweron(iproc_lcd_ctx);
	else
		iproc_lcd_poweroff(iproc_lcd_ctx);
	return n;
}

/**********************************************************************
 *  iproc_lcd_show
 *
 *  Show the power state of the LCD panel
 *
 **********************************************************************/

static ssize_t iproc_lcd_show(struct kobject *kobj, struct kobj_attribute *attr,
			       char *buf)
{
	struct iproc_lcd_ctx  *pCTX = iproc_lcd_ctx;

	if (__raw_readl(LCDREG_CONTROL) & LCD_F_LcdPwr_MASK)
		return sprintf(buf, "1\n");
	return sprintf(buf, "0\n");
}

/**********************************************************************
 *  iproc_lcd_probe
 *
 *  The Platform Driver Probe function.
 *
 *  Input parameters:
 *         device: The Device Context
 *
 *  Return value:
 *		    0: Driver Probe is Succesful
 *		not 0: ERROR
 **********************************************************************/

int __init iproc_lcd_probe(struct platform_device *pldev)
{
	struct device *dev = &(pldev->dev);


	struct iproc_lcd_ctx  *pCTX;
	struct fb_info	       *fbinfo;
	struct resource        *res   = NULL;
	u32 nFrameBufLen;
	int ret;
	int irq;
	int i;

	void __iomem *reg_addr=0;
	unsigned int reg_val;

	reg_addr = ioremap_nocache(CRMU_IOMUX_CTRL4_OFFSET,0x4);  	
	reg_val = __raw_readl(reg_addr);  
	reg_val &= ~(0x7 << CRMU_IOMUX_CTRL4__CORE_TO_IOMUX_LCD_SEL_R);	
	__raw_writel( reg_val, (reg_addr));
    iounmap(reg_addr);
	reg_addr = ioremap_nocache(ASIU_TOP_SW_RESET_CTRL,0x10);  	
	reg_val = __raw_readl(reg_addr);  
	reg_val |= (1<< ASIU_TOP_SW_RESET_CTRL__LCD_SW_RESET_N);	
	__raw_writel( reg_val, (reg_addr));
	reg_val = __raw_readl(reg_addr+ 4);  
	reg_val |= (1<< ASIU_TOP_CLK_GATING_CTRL__LCD_CLK_GATE_EN);
	__raw_writel( reg_val, (reg_addr + 4));
	reg_val = __raw_readl(reg_addr+ 0xc);  
	reg_val &= 0xfeffffff;
	__raw_writel( reg_val, (reg_addr + 0xc));
    iounmap(reg_addr);

	/* Validation of platform device structure */
	if(!pldev)
	{
		PDEBUG("WRONG INPUT\nplatfrom_device ppointer should not be NULL.\n");
		return -EINVAL;
	}

	/* Validate LCD panel sizes */
	if (((lcdwidth == IPROC_LCD_800x480_PPL) && (lcdheight == IPROC_LCD_800x480_LPP)) ||
		((lcdwidth == IPROC_LCD_720x480_PPL) && (lcdheight == IPROC_LCD_720x480_LPP)) ||
		((lcdwidth == IPROC_LCD_640x480_PPL) && (lcdheight == IPROC_LCD_640x480_LPP)) ||
		((lcdwidth == IPROC_LCD_480x272_PPL) && (lcdheight == IPROC_LCD_480x272_LPP)))
	{
		lcd_var_data.xres =	lcdwidth;
		lcd_var_data.yres =	lcdheight;
		lcd_var_data.xres_virtual =	lcdwidth;
		lcd_var_data.yres_virtual =	lcdheight;

		lcd_fix_data.line_length  = (lcd_var_data.xres * lcd_var_data.bits_per_pixel) / 8;
	}
	else
	{
		printk(KERN_ERR "WRONG INPUT\nLCD Panel size not valid: %d, %d.\n", lcdwidth, lcdheight);
		return -EINVAL;
	}

	/* if a 640x480 display is in use, require the clock divisor parameter */
	if ((lcdwidth == IPROC_LCD_640x480_PPL) && (clockdivisor == IPROC_LCD_INVALID_CLKDIV)) {
		printk(KERN_ERR "640x480 display requires the clockdivisor module parameter\n");
		printk(KERN_ERR "Densitron 84-0159: Suggest 10\n");
		clockdivisor = IPROC_LCD_DEF_CLKDIV;
		return -EINVAL;
	} else if (clockdivisor == IPROC_LCD_INVALID_CLKDIV) {
		/* remain backwards compatable otherwise */
		clockdivisor = IPROC_LCD_DEF_CLKDIV;
	}

	/* this catches an invalid clockdivisor supplied */
	if (clockdivisor > 4095) {
		printk(KERN_ERR "Valid clock divisor is between 0 and 4095\n");
		return -EINVAL;
	}

	/* Reset the LCD module: block 1 bit 18 
	if (call_secure_api(CLS_DMU_BLOCK_RESET, 1, DMU_F_dmu_rst_lcd_R)) {
		PDEBUG("LCD reset failed\n");
	}*/

	/*---------- Driver Context -----------*/
	fbinfo = framebuffer_alloc(sizeof(struct iproc_lcd_ctx), dev); // this will allocate context memory
	if (!fbinfo) {
		return -ENOMEM;
	}

	platform_set_drvdata(pldev, fbinfo);

	pCTX = fbinfo->par;
	memset(pCTX, 0, sizeof(pCTX));

	pCTX->fb = fbinfo;
	pCTX->dev = dev;
	pCTX->lcdmode = 2;

	spin_lock_init(&(pCTX->palette_lock));
	for (i = 0; i < IPROC_LCD_HW_PALETTE_ENTRY; i++)
		pCTX->palette_buffer[i] = PALETTE_BUFF_CLEAR;

	pCTX->panel.bTFT = 1;  /* if this is a TFT panel */

	if ((lcdwidth == IPROC_LCD_800x480_PPL) && (lcdheight == IPROC_LCD_800x480_LPP))
	{
		pCTX->panel.nPPL = IPROC_LCD_800x480_PPL; /* Pixels Per Line */
		pCTX->panel.nHBP = IPROC_LCD_800x480_HBP; /* Horizontal Back Porch */
		pCTX->panel.nHFP = IPROC_LCD_800x480_HFP; /* Horizontal Front Porch */
		pCTX->panel.nHSW = IPROC_LCD_800x480_HSW; /* Horizontal Sync Width */
		pCTX->panel.nLPP = IPROC_LCD_800x480_LPP; /* Lines Per Panel */
		pCTX->panel.nVSW = IPROC_LCD_800x480_VSW; /* Vertical Sync Width */
		pCTX->panel.nVBP = IPROC_LCD_800x480_VBP; /* Vertical Back Porch */
		pCTX->panel.nVFP = IPROC_LCD_800x480_VFP; /* Vertical Front Porch */
		pCTX->panel.nLED = IPROC_LCD_800x480_LED; /* Line End Delay */
	}
	else if ((lcdwidth == IPROC_LCD_720x480_PPL) && (lcdheight == IPROC_LCD_720x480_LPP))
	{
		pCTX->panel.nPPL = IPROC_LCD_720x480_PPL; /* Pixels Per Line */
		pCTX->panel.nHBP = IPROC_LCD_720x480_HBP; /* Horizontal Back Porch */
		pCTX->panel.nHFP = IPROC_LCD_720x480_HFP; /* Horizontal Front Porch */
		pCTX->panel.nHSW = IPROC_LCD_720x480_HSW; /* Horizontal Sync Width */
		pCTX->panel.nLPP = IPROC_LCD_720x480_LPP; /* Lines Per Panel */
		pCTX->panel.nVSW = IPROC_LCD_720x480_VSW; /* Vertical Sync Width */
		pCTX->panel.nVBP = IPROC_LCD_720x480_VBP; /* Vertical Back Porch */
		pCTX->panel.nVFP = IPROC_LCD_720x480_VFP; /* Vertical Front Porch */
		pCTX->panel.nLED = IPROC_LCD_720x480_LED; /* Line End Delay */
	}
	else if ((lcdwidth == IPROC_LCD_640x480_PPL) && (lcdheight == IPROC_LCD_640x480_LPP))
	{
		pCTX->panel.nPPL = IPROC_LCD_640x480_PPL; /* Pixels Per Line */
		pCTX->panel.nHBP = IPROC_LCD_640x480_HBP; /* Horizontal Back Porch */
		pCTX->panel.nHFP = IPROC_LCD_640x480_HFP; /* Horizontal Front Porch */
		pCTX->panel.nHSW = IPROC_LCD_640x480_HSW; /* Horizontal Sync Width */
		pCTX->panel.nLPP = IPROC_LCD_640x480_LPP; /* Lines Per Panel */
		pCTX->panel.nVSW = IPROC_LCD_640x480_VSW; /* Vertical Sync Width */
		pCTX->panel.nVBP = IPROC_LCD_640x480_VBP; /* Vertical Back Porch */
		pCTX->panel.nVFP = IPROC_LCD_640x480_VFP; /* Vertical Front Porch */
		pCTX->panel.nLED = IPROC_LCD_640x480_LED; /* Line End Delay */
	}
	else if ((lcdwidth == IPROC_LCD_480x272_PPL) && (lcdheight == IPROC_LCD_480x272_LPP))
	{
		pCTX->panel.nPPL = IPROC_LCD_480x272_PPL; /* Pixels Per Line */
		pCTX->panel.nHBP = IPROC_LCD_480x272_HBP; /* Horizontal Back Porch */
		pCTX->panel.nHFP = IPROC_LCD_480x272_HFP; /* Horizontal Front Porch */
		pCTX->panel.nHSW = IPROC_LCD_480x272_HSW; /* Horizontal Sync Width */
		pCTX->panel.nLPP = IPROC_LCD_480x272_LPP; /* Lines Per Panel */
		pCTX->panel.nVSW = IPROC_LCD_480x272_VSW; /* Vertical Sync Width */
		pCTX->panel.nVBP = IPROC_LCD_480x272_VBP; /* Vertical Back Porch */
		pCTX->panel.nVFP = IPROC_LCD_480x272_VFP; /* Vertical Front Porch */
		pCTX->panel.nLED = IPROC_LCD_480x272_LED; /* Line End Delay */
	}


	/*---------- Register Address -------------*/

	/* get LCD register address */
	res = platform_get_resource(pldev, IORESOURCE_MEM, 0 /* the first IORESOURCE_MEM is what we want */ );
	if(res == NULL)
	{
		PDEBUG("ERROR: Could not get Platform Resource LCD Register Memory Resource\n");
		ret = -ENXIO;
		goto err_return;
	}

	/* request memory region */
	pCTX->lcd_reg_mem = request_mem_region(res->start, (res->end - res->start + 1), pldev->name);
	if (pCTX->lcd_reg_mem == NULL)
	{
		PDEBUG("ERROR: Could not request mem region, for LCD Register Memory Locations\n");
		ret = -ENOENT;
		goto err_return;
	}

	/* map */
	pCTX->lcd_reg_base = ioremap_nocache(res->start, res->end - res->start + 1);
	if (pCTX->lcd_reg_base == NULL) {
		PDEBUG("Unable to mape device registers\n");
		ret = -ENXIO;
		goto err_return;
	}

	/*---------------- IRQ -----------------*/
	for(i = 0; i<5 ; i++)
	{
		irq = platform_get_irq(pldev, i);
		if (irq < 0) {
			PDEBUG("no irq for device\n");
			ret = -ENOENT;
			goto err_return;
		}

		ret = request_irq(irq, iproc_lcd_irq,
			IRQF_DISABLED,
			pldev->name, pCTX); /* use pCTX as data pointer for IRQ routine */
		if (ret) {
			PDEBUG("cannot get irq %d - err %d\n", irq, ret);
			ret = -EBUSY;
			goto err_return;
		}

		pCTX->irq[i] = irq;
    }

	/*---------- Frame buffer driver related -----------*/
	fbinfo->fix     = lcd_fix_data;
	fbinfo->var     = lcd_var_data;
	fbinfo->fbops   = &iproc_lcd_ops;
	fbinfo->flags   = FBINFO_FLAG_DEFAULT;
	fbinfo->pseudo_palette  = &pCTX->pseudo_pal;

	printk(KERN_ERR "%s: Allocate vid ram for %dx%d display\n",__func__,lcdwidth,lcdheight);
	nFrameBufLen = lcdwidth * lcdheight * IPROC_LCD_RES_BBP_MAX /8;
	ret = iproc_lcd_allocate_frame_buffer(pCTX, 2*nFrameBufLen);
	if (ret) {
		PDEBUG("Failed to allocate video RAM\n");
		ret = -ENOMEM;
		goto err_return;
	}


	ret = iproc_lcd_fb_check_var(&fbinfo->var, fbinfo);

	ret = register_framebuffer(fbinfo);
	if (ret < 0) {
		PDEBUG("Failed to register framebuffer device: %d\n", ret);
		ret = -ENOENT;
		goto err_return;
	}

	iproc_lcd_ctx = pCTX;
	init_waitqueue_head(&pCTX->done);
	pCTX->lpwm = NULL;
	/*ret = bcm5892_sysfs_create_power_file(&iproc_lcd_attr.attr);
	if (ret)
		PDEBUG("sysfs_create_file failed: %d\n", ret);*/

	ret = iproc_lcd_init_registers(pCTX);
	iproc_lcd_poweron(pCTX);
	return 0;

err_return:
	if (pCTX->pFrameBuf_CPU)
		iproc_lcd_release_frame_buffer(pCTX);

 	if (pCTX->irq)
 		free_irq(pCTX->irq, pCTX);

 	if (pCTX->lcd_reg_base)
		iounmap(pCTX->lcd_reg_base);

 	if (pCTX->lcd_reg_mem) {
		release_resource(pCTX->lcd_reg_mem);
		kfree(pCTX->lcd_reg_mem);
	}

	if (pCTX->fb) {
		unregister_framebuffer(pCTX->fb);
		framebuffer_release(pCTX->fb);
	}

	return ret;
}


/**********************************************************************
 *  iproc_lcd_remove
 *
 *  The Platform Driver Remove function.
 *
 *  Input parameters:
 *         device: The Device Context
 *
 *  Return value:
 *		    0: Driver Remove is Succesful
 *		not 0: ERROR
 **********************************************************************/

int __exit iproc_lcd_remove(struct platform_device *pldev)
{
    int i =0;
	struct iproc_lcd_ctx  *pCTX = iproc_lcd_ctx;

	/* Remove sysfs file before iproc_lcd_ctx is destroyed 
	bcm5892_sysfs_remove_power_file(&iproc_lcd_attr.attr);*/

	/* Disable interrupts */
	__raw_writel(0, LCDREG_INTMASK);

	/* Reset the LCD module: block 1 bit 18 
	if (call_secure_api(CLS_DMU_BLOCK_RESET, 1, DMU_F_dmu_rst_lcd_R)) {
		PDEBUG("LCD reset failed\n");
	}*/

	/* Free up resources */
	if (iproc_lcd_ctx) {
		iproc_lcd_ctx = NULL;

		/* shut down LCD display */
		iproc_lcd_poweroff(pCTX);

		if (pCTX->pFrameBuf_CPU)
			iproc_lcd_release_frame_buffer(pCTX);
		for(i=0;i<5;i++) if(pCTX->irq[i])  free_irq(pCTX->irq[i], pCTX);
		if (pCTX->lcd_reg_base)
			iounmap(pCTX->lcd_reg_base);
		if (pCTX->lcd_reg_mem) {
			release_resource(pCTX->lcd_reg_mem);
			kfree(pCTX->lcd_reg_mem);
		}
		if (pCTX->fb) {
			unregister_framebuffer(pCTX->fb);
			framebuffer_release(pCTX->fb);
		}
		if(pCTX->lpwm){
			pwm_free(pCTX->lpwm);			
		}
       	}
	return 0;
}



/**********************************************************************
 *  iproc_lcd_init
 *  The Driver Entry Function
 *
 *  Input parameters:
 *         None
 *
 *  Return value:
 *		    0: Driver Entry is Succesful
 *		not 0: ERROR
 **********************************************************************/
static int __init iproc_lcd_init(void)
{
	int err = -1;

//	PDEBUG("Entering Module Init\n");


	err = platform_driver_register(&iproc_lcd_driver);
	if (err)
	{
		PDEBUG("ERROR IN module_init. Could not do driver_register\n");
		PDEBUG("Error Code = 0x%08x\n", err);
		return err;
	}

	err = platform_device_register(&iproc_lcd_pdev);
	if(err)
	{

		platform_driver_unregister(&iproc_lcd_driver);
		PDEBUG("ERROR IN module_init. Could not do platform_device_register\n");
		PDEBUG("Error Code = 0x%08x\n", err);
		return err;
	}

//	PDEBUG("Exiting Module Init\n");
	return err;
}

/**********************************************************************
 *  iproc_lcd_cleanup
 *  The Driver Exit Function
 *
 *  Input parameters:
 *         None
 *
 *  Return value:
 *		    Nothing
 **********************************************************************/
static void __exit iproc_lcd_cleanup(void)
{
//	PDEBUG("Entering Module Exit\n");

	/* Unregister the Platform Device */
	platform_device_unregister(&iproc_lcd_pdev);

	platform_driver_unregister(&iproc_lcd_driver);

//	PDEBUG("Exiting Module Exit\n");
}

module_init(iproc_lcd_init);
module_exit(iproc_lcd_cleanup);

MODULE_AUTHOR("Feng Ye <fye@broadcom.com>," "Broadcom Corporation");
MODULE_DESCRIPTION("Broadcom LCD Framebuffer driver for IPROC");
//MODULE_LICENSE("GPL");
MODULE_LICENSE("GPL");
