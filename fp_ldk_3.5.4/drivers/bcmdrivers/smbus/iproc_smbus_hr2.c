/*
 * Copyright (C) 2015, Broadcom Corporation. All Rights Reserved.
 * Dante Su <dantesu@broadcom.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/platform_device.h>

#define DRV_NAME "iproc-smb"

#ifndef setbits_le32
#define setbits_le32(reg, val)  writel(readl(reg) | (val), reg)
#endif

#ifndef clrbits_le32
#define clrbits_le32(reg, val)  writel(readl(reg) & ~(val), reg)
#endif

struct iproc_i2c_regs {
	uint32_t cfg;     /* configuration register */
	uint32_t tcfg;    /* timing configuration register */
	uint32_t sacr;    /* slave address control register */
	uint32_t mfcr;    /* master fifo control register */

	uint32_t sfcr;    /* slave fifo control register */
	uint32_t bbcr;    /* bit-bang control register */
	uint32_t rsvd[6];

	uint32_t mcr;     /* master command register */
	uint32_t scr;     /* slave command register */
	uint32_t ier;     /* interrupt enable register */
	uint32_t isr;     /* interrupt status register */

	uint32_t mdwr;    /* master data write register */
	uint32_t mdrr;    /* master data read register */
	uint32_t sdwr;    /* slave data write register */
	uint32_t sdrr;    /* slave data read register */
};

/* Configuration register */
#define CFG_RESET               0x80000000
#define CFG_ENABLE              0x40000000
#define CFG_BITBANG             0x20000000

/* Timing configuration register */
#define TCFG_400KHZ             0x80000000
#define TCFG_IDLE(x)            (((x) & 0xff) << 8)

/* Master fifo control register */
#define MFCR_RXCLR              0x80000000
#define MFCR_TXCLR              0x40000000
#define MFCR_RXPKT(x)           (((x) >> 16) & 0x7f)
#define MFCR_RXTHR(x)           (((x) & 0x3f) << 8)

/* Bit-Bang control register */
#define BBCR_SCLIN              0x80000000
#define BBCR_SCLOUT             0x40000000
#define BBCR_SDAIN              0x20000000
#define BBCR_SDAOUT             0x10000000

/* Master command register */
#define MCR_START               0x80000000 /* Send START (busy) */
#define MCR_ABORT               0x40000000
#define MCR_PROTO(x)            (((x) & 0x0f) << 9) /* SMBus protocol */
#define MCR_PEC                 0x00000100 /* Packet Error Check */
#define MCR_RXLEN(x)            ((x) & 0xff)

/* Master command status */
#define MCR_STATUS(x)           (((x) >> 25) & 7)
#define MCR_STATUS_SUCCESS      0
#define MCR_STATUS_LOST_ARB     1
#define MCR_STATUS_NACK_FB      2 /* NACK on the 1st byte */
#define MCR_STATUS_NACK_NFB     3 /* NACK on the non-1st byte */
#define MCR_STATUS_TIMEOUT      4
#define MCR_STATUS_TX_TLOW_MEXT 5
#define MCR_STATUS_RX_TLOW_MEXT 6

/* Master SMBus protocol type */
#define MCR_PROT_QUICK_CMD               0
#define MCR_PROT_SEND_BYTE               1
#define MCR_PROT_RECV_BYTE               2
#define MCR_PROT_WR_BYTE                 3
#define MCR_PROT_RD_BYTE                 4
#define MCR_PROT_WR_WORD                 5
#define MCR_PROT_RD_WORD                 6
#define MCR_PROT_BLK_WR                  7
#define MCR_PROT_BLK_RD                  8
#define MCR_PROT_PROC_CALL               9
#define MCR_PROT_BLK_WR_BLK_RD_PROC_CALL 10

/* Interrupt types */
#define IRQ_RXFF      0x80000000 /* rx fifo full */
#define IRQ_RXFT      0x40000000 /* rx fifo threshold */
#define IRQ_RX        0x20000000
#define IRQ_BUSY      0x10000000
#define IRQ_TX        0x08000000

/* Tx/Rx data control */
#define MDWR_END      0x80000000 /* end of transfer */
#define MDRR_END      0x80000000 /* end of transfer */
#define MDRR_NACK     0x40000000

struct iproc_i2c_chip {
	struct device        *dev;
	void __iomem         *base;
	struct resource      *res;
	int                   irq;
	struct i2c_adapter    adap;
	spinlock_t            lock;
	wait_queue_head_t     wait;
	bool                  busy;
	int                   err;
	struct {
		int timeout;
		int bus_error;
		int sda_failure;
		int sda_success;
	} recovery;
};

static int iproc_i2c_bus_reset(struct iproc_i2c_chip *chip)
{
	struct iproc_i2c_regs *regs = chip->base;
	int ret, i;
	uint32_t cfg;

	/* save a backup for CFG register */
	cfg = readl(&regs->cfg);

	/* enable bit-bang */
	setbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);

	/* set SDA=H, SCL=H */
	setbits_le32(&regs->bbcr, BBCR_SCLOUT | BBCR_SDAOUT);
	udelay(5);

	/* generate 9 dummy clocks (8-bits data + 1-bit NACK) */
	for (i = 0; i < 18; ++i) {
		if (i & 1)
			setbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: high */
		else
			clrbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: low */
		udelay(5);
	}

	/* generate a STOP signal */
	clrbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: low */
	udelay(2);
	clrbits_le32(&regs->bbcr, BBCR_SDAOUT); /* SDA: low */
	udelay(3);
	setbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: high */
	udelay(2);
	setbits_le32(&regs->bbcr, BBCR_SDAOUT); /* SDA: high */
	udelay(5);

	/* disable bit-bang control */
	clrbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);

	/* restore CFG register */
	writel(cfg, &regs->cfg);

	/* Now SDA should be at level high */
	if (readl(&regs->bbcr) & BBCR_SDAIN) {
		ret = 0;
		++chip->recovery.sda_success;
	} else {
		ret = -1;
		++chip->recovery.sda_failure;
		dev_info(chip->dev, "bus reset failed\n");
	}

	return ret;
}

static int iproc_i2c_reset(struct iproc_i2c_chip *chip)
{
	struct iproc_i2c_regs *regs = chip->base;
	unsigned long val, timeout;

	/* chip reset */
	writel(CFG_RESET, &regs->cfg);
	for (timeout = jiffies + HZ; time_is_after_jiffies(timeout); ) {
		if (!(readl(&regs->cfg) & CFG_RESET))
			break;
	}
	if (!time_is_after_jiffies(timeout))
		dev_warn(chip->dev, "reset timed out\n");

	/* clock setup */
	val = readl(&regs->tcfg);
#ifdef CONFIG_IPROC_I2C_400K
	val |= TCFG_400KHZ;
#else  /* 100k Hz */
	val &= ~TCFG_400KHZ;
#endif
	writel(val, &regs->tcfg);

	/* chip enable */
	writel(CFG_ENABLE, &regs->cfg);

	/* 50us delay as per HW spec. */
	udelay(50);

	return 0;
}

static int iproc_i2c_poll(struct iproc_i2c_chip *chip)
{
	struct iproc_i2c_regs *regs = chip->base;
	uint32_t val;
	int ret = 0;

	val = readl(&regs->mcr);
	if (val & MCR_START) {
		ret = -EAGAIN;
	} else {
		chip->err = MCR_STATUS(val);
		if (chip->err != MCR_STATUS_SUCCESS) {
			writel(0, &regs->mcr);
			ret = -EREMOTEIO;
		}
	}

	return ret;
}

static irqreturn_t iproc_i2c_isr(int irq, void *dev_id)
{
	struct iproc_i2c_chip *chip = dev_id;
	struct iproc_i2c_regs *regs = chip->base;
	uint32_t st;

	spin_lock(&chip->lock);

	st = readl(&regs->isr);
	writel(st, &regs->isr);

	if (chip->busy && !iproc_i2c_poll(chip)) {
		chip->busy = false;
		wake_up(&chip->wait);
	}

	spin_unlock(&chip->lock);
	return IRQ_HANDLED;
}

static int iproc_i2c_wait(struct iproc_i2c_chip *chip)
{
	unsigned long timeout = msecs_to_jiffies(20);
	struct iproc_i2c_regs *regs = chip->base;
	int ret = 0;

	if (chip->irq < 0) {
		for (timeout += jiffies; time_is_after_jiffies(timeout); ) {
			ret = iproc_i2c_poll(chip);
			if (ret != -EAGAIN)
				break;
			schedule();
		}
		if (!time_is_after_jiffies(timeout))
			ret = -ETIMEDOUT;
	} else {
		wait_event_timeout(chip->wait, !chip->busy, timeout);
		if (chip->busy)
			ret = -ETIMEDOUT;
	}

	switch(ret) {
	case -ETIMEDOUT:
		dev_dbg(chip->dev, "timed out\n");
		++chip->recovery.timeout;
		break;
	case -EREMOTEIO:
		dev_dbg(chip->dev, "bus error, err=%d\n", chip->err);
		if (chip->err == MCR_STATUS_TIMEOUT)
			++chip->recovery.timeout;
		else
			++chip->recovery.bus_error;
		break;
	default:
		break;
	}

	if (ret == -ETIMEDOUT) {
		/* only activate bus reset upon SDA low */
		if (!(readl(&regs->bbcr) & BBCR_SDAIN))
			iproc_i2c_bus_reset(chip);
		/* only activate chip reset upon busy/start=1 */
		if (readl(&regs->mcr) & MCR_START)
			iproc_i2c_reset(chip);
	}

	if (ret) {
		dev_dbg(chip->dev, "recovery statistics:\n");
		dev_dbg(chip->dev, "    timeout      = %d\n", 
			chip->recovery.timeout);
		dev_dbg(chip->dev, "    bus error    = %d\n", 
			chip->recovery.bus_error);
		dev_dbg(chip->dev, "    sda recovery = %d (%d failed)\n",
			chip->recovery.sda_success + chip->recovery.sda_failure,
			chip->recovery.sda_failure);
	}

	chip->busy = 0;
	return ret;
}

static int iproc_i2c_xfer(struct i2c_adapter *adap, u16 addr,
			unsigned short flags, char read_write,
			u8 command, int size, union i2c_smbus_data *data)
{
	struct iproc_i2c_chip *chip = adap->algo_data;
	struct iproc_i2c_regs *regs = chip->base;
	int len, ret, prot, retry;
	u8 *buf;

	chip->err = 0;

	/*
	 * In Hurricane2 platforms, the I2C module is inside CMIC and
	 * it would be reset upon SDK initialization in user mode.
	 * So we'll have to re-enable the I2C controller.
	 */
	if (!(readl(&regs->cfg) & CFG_ENABLE))
		iproc_i2c_reset(chip);

	retry = 10;

xfer_retry:

	ret = 0;
	len = 0;
	buf = &data->byte;

	if (readl(&regs->mcr) & MCR_START)
		BUG();

	/* fifo clear */
	writel(MFCR_TXCLR | MFCR_RXCLR, &regs->mfcr);
	while (readl(&regs->mfcr) & (MFCR_TXCLR | MFCR_RXCLR))
		schedule();

	switch (size) {
	case I2C_SMBUS_BYTE:
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_RECV_BYTE;
			writel((addr << 1) | 1, &regs->mdwr);
			len = 1;
		} else {
			prot = MCR_PROT_SEND_BYTE;
			writel(addr << 1, &regs->mdwr);
			writel(buf[0] | MDWR_END, &regs->mdwr);
		}
		break;

	case I2C_SMBUS_BYTE_DATA:
		writel(addr << 1, &regs->mdwr);
		writel(command, &regs->mdwr);
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_RD_BYTE;
			writel((addr << 1) | 1, &regs->mdwr);
			len = 1;
		} else {
			prot = MCR_PROT_WR_BYTE;
			writel(buf[0] | MDWR_END, &regs->mdwr);
		}
		break;

	case I2C_SMBUS_WORD_DATA:
		writel(addr << 1, &regs->mdwr);
		writel(command, &regs->mdwr);
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_RD_WORD;
			writel((addr << 1) | 1, &regs->mdwr);
			len = 2;
		} else {
			prot = MCR_PROT_WR_WORD;
			writel(buf[0], &regs->mdwr);
			writel(buf[1] | MDWR_END, &regs->mdwr);
		}
		break;

	case I2C_SMBUS_BLOCK_DATA:
		writel(addr << 1, &regs->mdwr);
		writel(command, &regs->mdwr);
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_BLK_RD;
			writel((addr << 1) | 1, &regs->mdwr);
			len = I2C_SMBUS_BLOCK_MAX + 1;
		} else {
			prot = MCR_PROT_BLK_WR;
			len = *buf + 1;
			++buf;
			if (len > I2C_SMBUS_BLOCK_MAX)
				len = I2C_SMBUS_BLOCK_MAX;
			writel(len, &regs->mdwr);
			while (len-- > 0) {
				if (!len)
					writel(*buf | MDWR_END, &regs->mdwr);
				else
					writel(*buf, &regs->mdwr);
				++buf;
			}
		}
		break;

	default:
		dev_err(chip->dev,
			"SMBus protocol %d is not yet implemented\n", size);
		ret = -1;
		goto xfer_done;
	}

	chip->busy = true;
	writel(MCR_START | MCR_PROTO(prot) | MCR_RXLEN(len), &regs->mcr);
	ret = iproc_i2c_wait(chip);

	if (ret) {
		if (retry--) {
			goto xfer_retry;
		} else {
			struct timespec uptime;
			int hh, mm, ss;

			do_posix_clock_monotonic_gettime(&uptime);
			monotonic_to_bootbased(&uptime);
			hh = uptime.tv_sec / 3600;
			uptime.tv_sec %= 3600;
			mm = uptime.tv_sec / 60;
			uptime.tv_sec %= 60;
			ss = uptime.tv_sec;
			dev_err(chip->dev, "[%02d:%02d:%02d] cmd failed (dev=0x%x, err=%d)\n", 
				hh, mm, ss, addr, chip->err);
			goto xfer_done;
		}
	}

	while (len-- > 0) {
		*buf = (u8)(readl(&regs->mdrr) & 0xff);
		++buf;
	}

xfer_done:
	return ret;
}

static u32 iproc_i2c_functionality(struct i2c_adapter *adap)
{
	return I2C_FUNC_SMBUS_BYTE |
	       I2C_FUNC_SMBUS_BYTE_DATA |
	       I2C_FUNC_SMBUS_WORD_DATA |
	       I2C_FUNC_SMBUS_BLOCK_DATA;
}

static struct i2c_algorithm iproc_i2c_algorithm = {
	.smbus_xfer    = iproc_i2c_xfer,
	.functionality = iproc_i2c_functionality,
};

static int __devinit iproc_i2c_probe(struct platform_device *dev)
{
	struct iproc_i2c_chip *chip;
	struct iproc_i2c_regs *regs;
	struct i2c_adapter *adap;
	struct resource *res;
	int ret = 0;

	res = platform_get_resource(dev, IORESOURCE_MEM, 0);
	if (!res) {
		ret = -EINVAL;
		goto err_out;
	}

	if (!request_mem_region(res->start, resource_size(res), DRV_NAME)) {
		ret = -EBUSY;
		goto err_out;
	}

	chip = kzalloc(sizeof(*chip), GFP_KERNEL);
	if (!chip) {
		ret = -ENOMEM;
		goto err_release;
	}

	spin_lock_init(&chip->lock);
	init_waitqueue_head(&chip->wait);

	regs = ioremap(res->start, resource_size(res));
	if (!regs) {
		ret = -ENOMEM;
		goto err_free;
	}
	chip->dev = &dev->dev;
	chip->irq = platform_get_irq(dev, 0);
	chip->res = res;
	chip->base = regs;

	adap = &chip->adap;
	strlcpy(adap->name, dev->name, sizeof(adap->name));
	adap->algo = &iproc_i2c_algorithm;
	adap->algo_data = chip;
	adap->nr = dev->id;
	adap->class = 0;
	adap->dev.parent = &dev->dev;

	/* chip reset */
	iproc_i2c_reset(chip);

	if (chip->irq > 0) {
		writel(IRQ_BUSY, &regs->ier);
		writel(IRQ_BUSY, &regs->isr);
		ret = request_irq(chip->irq, iproc_i2c_isr, IRQF_SHARED, dev->name, chip);
		if (ret) {
			dev_err(&dev->dev, "unable to request IRQ %d\n", chip->irq);
			goto err_unmap;
		}
	}

	platform_set_drvdata(dev, chip);
	ret = i2c_add_numbered_adapter(adap);
	if (ret < 0) {
		dev_err(&dev->dev, "unable to register I2C adapter\n");
		goto err_free_irq;
	}

	return 0;

err_free_irq:
	if (chip->irq > 0)
		free_irq(chip->irq, chip);
err_unmap:
	iounmap(chip->base);
err_free:
	kfree(chip);
err_release:
	release_mem_region(res->start, resource_size(res));
err_out:
	return ret;
}

static int __devexit iproc_i2c_remove(struct platform_device *pdev)
{
	struct iproc_i2c_chip *chip = platform_get_drvdata(pdev);
	struct iproc_i2c_regs *regs = chip->base;

	writel(0, &regs->cfg);

	if (chip->irq > 0)
		free_irq(chip->irq, chip);

	if (chip->base) {
		iounmap(chip->base);
		release_mem_region(chip->res->start, resource_size(chip->res));
	}

	platform_set_drvdata(pdev, NULL);
	i2c_del_adapter(&chip->adap);

	return 0;
}

static struct platform_driver iproc_i2c_driver = {
	.probe		= iproc_i2c_probe,
	.remove		= __devexit_p(iproc_i2c_remove),
	.driver		= {
		.name	= DRV_NAME,
		.owner	= THIS_MODULE,
	},
};

static int __init iproc_i2c_init(void)
{
	pr_info("I2C: Broadcom CMICm-I2C driver\n");
	return platform_driver_register(&iproc_i2c_driver);
}

static void __exit iproc_i2c_exit(void)
{
	platform_driver_unregister(&iproc_i2c_driver);
}

MODULE_DESCRIPTION("I2C-Bus adapter routines for Broadcom CMICm-I2C");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" DRV_NAME);

subsys_initcall(iproc_i2c_init);
module_exit(iproc_i2c_exit);
