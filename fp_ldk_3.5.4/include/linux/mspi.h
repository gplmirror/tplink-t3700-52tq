/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <linux/io.h>

/* SPI device number :
 *  - Supports only one device.
 */
#define CCA_SPI_NUM_DEV 1

/* MSPI Register Addressess */

#define CMIC_BASE	0x48000000
#define MSPI_CDRAM_BASE	0x1640
#define MSPI_ENDQP	0x1514
#define MSPI_NEWQP	0x1510
#define MSPI_RXRAM_BASE	0x15c0
#define MSPI_SPCR0_LSB	0x1500
#define MSPI_SPCR0_MSB	0x1504
#define MSPI_SPCR1_LSB	0x1508
#define MSPI_SPCR1_MSB	0x150c
#define MSPI_SPCR2	0x1518
#define MSPI_STATUS	0x1520
#define MSPI_TXRAM_BASE	0x1540
#define CMIC_OVERRIDE_STRAP	0x10234
#define CMICM_BSPI_MAST_N_BOOT	0x1680
#define IPROC_CCA_REG_BASE	0x18000000
#define GPIO_OUTEN	0x2008//0x68
#define GPIO_OUT	0x2004//0x64
#define SPIF 51924

typedef unsigned char BYTE;     // these save typing

extern int soc_mspi_init(void);
extern int soc_mspi_read8(uint8_t *buf, int len);
extern int soc_mspi_write8(uint8_t *buf, int len);
extern void max_spi_ss_low(void);
extern void max_spi_ss_high(void);

extern void max_reg_write(BYTE reg, BYTE val);
extern void max_data_write(BYTE reg, BYTE N, BYTE *p);
extern BYTE max_reg_read(BYTE reg);
extern BYTE *max_data_read(BYTE reg, BYTE N, BYTE *p);
