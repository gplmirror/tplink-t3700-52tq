/*
 * drivers/char/spi_flash_m25p40.h
 * ST M25P40 Flash driver for MPC836x SPI driver
 *
 * Copyright (C) 2006 Freescale Semiconductor, Inc
 *
 * Porting to linux by tanya.jiang@freescale.com
 * Based on MPC8360 bare board code of Freescale Ishral by amirh nire and yoramsh.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __SPI_FLASH_M25P40_H__
#define __SPI_FLASH_M25P40_H__

#include <asm/ioctl.h>		/* For _IO* macros */

#ifdef  CONFIG_MPC832XE_MDS
#define QE_CLK  200
#endif

#define FALSE   0
#define TRUE    1
#define E_OK    0
#define E_ERR   1

#define SPI_M25P40_MAGIC			0xFE /* defined randomly*/

#define SPI_M25P40_BULK_ERASE_IOCTL		_IO(SPI_M25P40_MAGIC, 1)
#define SPI_M25P40_WRITE_IOCTL			_IOW(SPI_M25P40_MAGIC, 2, unsigned char)	
#define SPI_M25P40_READ_IOCTL			_IOR(SPI_M25P40_MAGIC, 3, unsigned char)	
#define SPI_M25P40_MAX_IOCTL			_IO(SPI_M25P40_MAGIC, 12)


#define M25P40_SIZE				0x80000	/* bytes */
#define M25P40_PAGE_SIZE			256
#define M25P40_SECTOR_NUM			8
#define M25P40_PAGES_PER_SECTOR			256

#define RETRY					250000
#define FSL_836X_SPI_ID				1	
#define FSL_SPI_CPU_MODE
#define MAX_TIME_TO_RECEIVE_ZERO_BYTES		0x1000
#define WAITS_BETWEEN_RECEIVES			0x1000

/* M25P40 instruction list refer spec chapter INSTRUCTION table 4*/
#define M25P40_WRITE_STATUS_CMD			0x01
#define M25P40_PAGE_PROGRAM_CMD			0x02
#define M25P40_READ_DATA_BYTES_CMD		0x03
#define M25P40_WRITE_DISABLE_CMD		0x04
#define M25P40_READ_STATUS_CMD			0x05
#define M25P40_WRITE_ENABLE_CMD			0x06
#define M25P40_FAST_READ_CMD			0x0B
#define M25P40_RELEASE_DEEP_POWER_DOWN_CMD	0xAB
#define M25P40_DEEP_POWER_DOWD_CMD          	0xB9
#define M25P40_BULK_ERASE_CMD               	0xC7
#define M25P40_SECTOR_ERASE_CMD             	0xD8

/* status register format */
#define M25P40_STATUS_SRWD  			0x80
#define M25P40_STATUS_BP2   			0x10
#define M25P40_STATUS_BP1   			0x08
#define M25P40_STATUS_BP0   			0x04
#define M25P40_STATUS_WEL   			0x02
#define M25P40_STATUS_WIP   			0x01

#define MAX_CMD_LENGTH              		4
#define NO_ADDRESS_CYCLE        		0xFFFFFFFF

#define SPI_M25P40_WRITE_MODE			1
#define SPI_M25P40_READ_MODE			2 /* Use read cmd */
#define SPI_M25P40_FAST_READ_MODE  		3 /* Use fast read cmd  */


#define SpiFlashM25P40_MRBLR    		2
#define SpiFlashM25P40_CHAR_LEN 		8

/* used for ioctl with user application */
struct spi_m25p40_transfer
{
	unsigned int	addr;
	unsigned int	size;
	unsigned int	mode;	/* write, read or fast_read */
	unsigned char	*data;
};

	
#endif /* __SPI_FLASH_M25P40_H__ */
