/*
 * drivers/char/ppc83xx_spi.c
 * This is SPI driver for MPC8360 MPC8325 and MPC8349
 * for MPC8360 and MPC8325, SPI1 in CPU mode
 *
 * Copyright (C) 2006 Freescale Semiconductor, Inc
 *
 * Porting to linux by tanya.jiang@freescale.com
 * Based on MPC8360 bare board code of Freescale Ishral by amirh nire and yoramsh.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/device.h>
#include <linux/config.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <asm/mmu.h>
#include <asm/types.h>
#include <asm/system.h>
#include <asm/io.h>
#include <asm/atomic.h>
#include <asm/uaccess.h>
#include <asm/string.h>
#include <asm/byteorder.h>


#include <linux/fsl_devices.h>
#include <asm/mpc83xx.h>
#include "ppc83xx_spi.h"

#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
static char driver_name[] = "fsl-qe-spi1";  /* same as mpc83xx_device.c */
#endif
#ifdef CONFIG_MPC834x_SYS
static char driver_name[] = "fsl-spi" ; /* same as mpc83xx_device.c */
#endif
static char spi_slave_name[] = "flash m25p40";
/* Structure for a device driver */
unsigned int	spi_m25p40_txconf_char_nums;
EXPORT_SYMBOL(spi_m25p40_txconf_char_nums);

struct spi_836x	 	*gp_spi;

#ifdef FSL_SPI_CPU_MODE
static unsigned char spi_mode_str[10] = "CPU";
#else
static unsigned char spi_mode_str[10] = "QE";
#endif

#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
extern int par_io_config_pin(u8 port,u8 pin,int dir,int open_drain,int assignment,int has_irq);
extern int par_io_data_set(u8 port, u8 pin, u8 val);
#endif

/* called by spi_cpu_tx_data() or interrupt_handler() */
static void cpu_tx(void )
{
	struct spi_836x *p_spi = gp_spi;
	struct spi_txqueue  *tmp_txqueue;
	unsigned int tmp_loc;
	unsigned char char_mem;

	_SPI_DBG("START ");
	/* search for next char to be transmitted */
	tmp_txqueue = p_spi->p_txqueue_curr;
	if(tmp_txqueue->curr_location == tmp_txqueue->char_nums) {
		/* FIXME: whether tmp_queue->curr_location isnot 0 */
		if (tmp_txqueue->char_nums == 0) return;
	
		/* here: all the data in current tx entry has been sent out */

		/* update the globle transmited byte num */
    		spi_m25p40_txconf_char_nums = tmp_txqueue->char_nums;
		tmp_txqueue->char_nums = 0;
		tmp_txqueue->curr_location = 0;
		tmp_txqueue->data = NULL;
		
		if(tmp_txqueue->last == TRUE) /* wrap */ {
			_SPI_DBG(" ");
			p_spi->p_txqueue_curr = p_spi->p_txqueue_start;
		}
		else
			p_spi->p_txqueue_curr++;
	
		/* FIXME: if there is data at the next queue-entry to be transmitted */
		tmp_txqueue = p_spi->p_txqueue_curr;
		if(tmp_txqueue->char_nums == 0) {
			_SPI_DBG("set txdoing FALSE");
			p_spi->tx_doing = FALSE;

			return;
		}
		
	}
	
	tmp_loc = tmp_txqueue->curr_location;
	/* write next characters to be transmitted  */
	char_mem = (unsigned char)((p_spi->char_len + 7)/8);
	switch (char_mem)
	{
	case 4:
		p_spi->p_reg->spitd = ((unsigned int *)(tmp_txqueue->data))[tmp_loc];
		tmp_txqueue->curr_location++;
		break;
	case 2:
		/* FIXME: */
		p_spi->p_reg->spitd =
			 ((unsigned short *)(tmp_txqueue->data))[tmp_loc];
		tmp_txqueue->curr_location++;
		break;
	case 1:
#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
		/* FIXME: */
		p_spi->p_reg->spitd =
			(((unsigned char *)(tmp_txqueue->data))[tmp_loc]) << 24;
		_SPI_DBG("txqueue_curr char_num 0x%x loc 0x%x data addr 0x%x, value 0x%x",
			tmp_txqueue->char_nums, tmp_loc, (unsigned int)tmp_txqueue->data,
			(((unsigned char *)(tmp_txqueue->data))[tmp_loc])<<24);
		tmp_txqueue->curr_location++;
#endif
#ifdef	CONFIG_MPC834x_SYS
		p_spi->p_reg->spitd =
			(((unsigned char *)(tmp_txqueue->data))[tmp_loc]);
		_SPI_DBG("txqueue_curr char_num 0x%x loc 0x%x data addr 0x%x, value 0x%x",
			tmp_txqueue->char_nums, tmp_loc, (unsigned int)tmp_txqueue->data,
			(((unsigned char *)(tmp_txqueue->data))[tmp_loc]));
		tmp_txqueue->curr_location++;
#endif
		break;
	default:
		printk(KERN_ERR "wrong p_spi->char_len");
	}
	_SPI_DBG("END ");
	return;
}

/* called by interrupt_handler(), data from spird => internal p_spi->rxbuff*/
static void cpu_rx(void)
{
	struct spi_836x        *p_spi = gp_spi;
	unsigned int	tmp_empty;
	unsigned int 	tmp_spmode, tmp_rxdata;
	unsigned char 	char_mem ;
	
	_SPI_DBG("START: rx pointer start 0x%x data 0x%x empty 0x%x",
		p_spi->rxbuff_start, p_spi->rxbuff_data, p_spi->rxbuff_empty);

	tmp_spmode = p_spi->p_reg->spmode;
	tmp_rxdata = p_spi->p_reg->spird;
	char_mem = (unsigned char)((p_spi->char_len + 7)/8);
	
	/* check: p_spi->char_len should only 4~16 or 32 */
	if( char_mem == 4 ) {
		*(unsigned int *)p_spi->rxbuff_empty = tmp_rxdata ;
	}
	else {
		_SPI_DBG("spimode #0x%x", tmp_spmode);
		/* refer to chapter 21.9.7 examples */
		if( tmp_spmode | SPMODE_REV ) { /* normal mode */
#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
 			if(char_mem == 2) {
				/* tanya: the content in the rxbuff_empty storing an addr is rxdata */
				*(unsigned short *)(p_spi->rxbuff_empty) = (unsigned short)(tmp_rxdata >> 16);
				_SPI_DBG("data 0x%x addr 0x%x\n",
					*(unsigned short *)(p_spi->rxbuff_empty), p_spi->rxbuff_empty);
			}
			if( char_mem == 1 ) {
				*(unsigned char *)(p_spi->rxbuff_empty) = (unsigned char)(tmp_rxdata >> 16);
				_SPI_DBG("spird 0x%x data 0x%x addr 0x%x\n",tmp_rxdata,
					*(unsigned char *)(p_spi->rxbuff_empty), p_spi->rxbuff_empty);
            		}
#endif
#ifdef	CONFIG_MPC834x_SYS
            		/* the spird register is different with 836x and 832x */
            		if(char_mem == 2) {
			*(unsigned short *)(p_spi->rxbuff_empty) = (unsigned short)(tmp_rxdata);
				_SPI_DBG("data 0x%x addr 0x%x\n",
					*(unsigned short *)(p_spi->rxbuff_empty), p_spi->rxbuff_empty);
            		}
			if( char_mem == 1 ) {
				*(unsigned char *)(p_spi->rxbuff_empty) = (unsigned char)(tmp_rxdata);
				_SPI_DBG("spird 0x%x data 0x%x addr 0x%x\n",tmp_rxdata,
					*(unsigned char *)(p_spi->rxbuff_empty), p_spi->rxbuff_empty);
            		}
#endif
		}//end of SPMODE_REV mode
        	else {
			tmp_rxdata &= 0x0000FFFF;
			if(char_mem == 2) {
				*(unsigned short *)(p_spi->rxbuff_empty) =
						(unsigned short)(tmp_rxdata >> (16 - p_spi->char_len));
				_SPI_DBG("data 0x%x addr 0x%x\n",
					*(unsigned short *)(p_spi->rxbuff_empty), p_spi->rxbuff_empty);
			}
			if(char_mem == 1) {
				*(unsigned char *)(p_spi->rxbuff_empty) =
						 (unsigned char)(tmp_rxdata >> (16 - p_spi->char_len));
				_SPI_DBG("value #0x%x ==> 0x%x \n",
					*(unsigned char *)(p_spi->rxbuff_empty), p_spi->rxbuff_empty);
			}
		} //end of else SPMODE_REV
	} //end of else char_mem = 4
	
	tmp_empty = p_spi->rxbuff_empty + char_mem;
	if (tmp_empty == (p_spi->rxbuff_start + DEFAULT_RXBUFF_SIZE)) {/* wrap */
		tmp_empty =  p_spi->rxbuff_start;
	}
	if(tmp_empty == p_spi->rxbuff_data) { /* overrun */
		printk(KERN_ERR "spi rx buff full");
		p_spi->rxbuff_full = TRUE;
	}

	p_spi->rxbuff_empty =  tmp_empty;
	_SPI_DBG("END: rx pointer start 0x%x data 0x%x empty 0x%x",
		p_spi->rxbuff_start, p_spi->rxbuff_data, p_spi->rxbuff_empty);
}

/********************************************************************
 data - pointer to the data to be transmited.
 In case of 8,16 and 32bit char size,
 the transmit function expect an array of the char size.
 In case of 4-8bits the ransmit function expect
 an array of bytes, in each location there is one char.
 In case of 9-16bits the ransmit function expect
 an array of unsignt short in each location there is one char.
 num - (in) The NUMBER OF CHARACTERS to be transmitted.
 *******************************************************************/
int spi_cpu_tx( void *data, unsigned int num)
{
	struct spi_836x	*p_spi = gp_spi;
 	unsigned long flags;

	spin_lock_irqsave(&p_spi->lock_tx, flags);
	_SPI_DBG("START :data p 0x%p, num 0x%x", data, num);
	if(p_spi->p_txqueue_empty->char_nums != 0) {
		/* no queue-entry ready */
		printk(KERN_ERR "no empty tx queue ");
		spin_unlock_irqrestore(&p_spi->lock_tx, flags);
		return -ENOSPC;
	}
	/* insert the data into the Tx queue */
	p_spi->p_txqueue_empty->curr_location = 0;
	p_spi->p_txqueue_empty->data = data;
	p_spi->p_txqueue_empty->char_nums = num;
	
	/* update the next empty location */
	if(p_spi->p_txqueue_empty->last == TRUE)
		p_spi->p_txqueue_empty = p_spi->p_txqueue_start;
	else
		p_spi->p_txqueue_empty++;
	/* reactivate the transmiter if needed */
	if(p_spi->tx_doing == FALSE) {
		_SPI_DBG("set txdoing TRUE ");
		p_spi->tx_doing = TRUE;
		p_spi->p_reg->spim |= SPI_CPU_NF;
		cpu_tx();
	}
	_SPI_DBG("END");
	spin_unlock_irqrestore(&p_spi->lock_tx, flags);
	return E_OK;
}

int spi_cpu_rx( void *dest, unsigned int num)
{
	struct spi_836x		*p_spi = gp_spi;
	unsigned int 		avialable_num = 0, copied_num = 0;
	unsigned int 		tmp_empty, tmp_data, tmp_start;
	unsigned char 		char_mem;
	unsigned long 		flags;
	void *tmp_dest ;
			
	if (num == 0) return 0;

	/* for disable interrupt */
	spin_lock_irqsave(&p_spi->lock_rx, flags);
	
	tmp_dest = dest;
	/* checked */
	tmp_start = p_spi->rxbuff_start;
	tmp_data = p_spi->rxbuff_data;
	tmp_empty = p_spi->rxbuff_empty;
	
	char_mem = (unsigned char)((p_spi->char_len + 7)/8);

	_SPI_DBG("START: rx pointer start 0x%x data 0x%x empty 0x%x",
		p_spi->rxbuff_start, p_spi->rxbuff_data, p_spi->rxbuff_empty);

	/* no data to pass to upper layer user */
	if((tmp_data == tmp_empty) && (p_spi->rxbuff_full == FALSE)){
		spin_unlock_irqrestore(&p_spi->lock_rx, flags);
		return E_OK;
	}
	if((tmp_data > tmp_empty) || (p_spi->rxbuff_full == TRUE)) {
		
		/* tanya: the real avaialbe num is
		 * DEFAULT_RXBUFF_SIZE - tmp_data + tmp_empty ) / p_spi->char_len
		 * however we only deal with the data in bottom in this if-block
		 * ( between rxbuff+MAX_SIZE to rxdata )
		 */
        	avialable_num = (tmp_start + DEFAULT_RXBUFF_SIZE - tmp_data)/char_mem;
		
		if(num <= avialable_num) /* we have enough data */ {
			memcpy(dest, (void *)tmp_data, num * char_mem);
			p_spi->rxbuff_data = tmp_data + num * char_mem;
			if( p_spi->rxbuff_data == tmp_start + DEFAULT_RXBUFF_SIZE ) /* wrap */
				p_spi->rxbuff_data = tmp_start;
			copied_num = num;
			_SPI_DBG(" ");
        	}
	        else {
			memcpy(dest, (void *)tmp_data, avialable_num * char_mem);
			tmp_data = tmp_start; /* wrap */
			p_spi->rxbuff_data = tmp_start;
			copied_num = avialable_num; /* save the number of characters that were copied */
			/* changed by tanya
 			 * update tmp_dest and num, they will be used by next if-block
			 */
			tmp_dest = (void *)(((unsigned int)dest) + (avialable_num * char_mem));
			num -= avialable_num;
			_SPI_DBG(" ");
		}
	}
	
	/* this if-block can also be entered after the previous one */
	if(tmp_data < tmp_empty) {
		avialable_num = ( tmp_empty - tmp_data )/char_mem;
		if(num <= avialable_num) {
			/* checked */
			memcpy(tmp_dest, (void *)tmp_data, num * char_mem);
			p_spi->rxbuff_data = tmp_data + num * char_mem;
			copied_num = num + copied_num;
			_SPI_DBG("value 0x%x ", *(unsigned int *)tmp_dest);
		}
		else /* we didnot have enough data for upper layer */ {
			/* checked */
			memcpy(tmp_dest, (void *)tmp_data, avialable_num * char_mem);
			p_spi->rxbuff_data = tmp_empty;
			copied_num = avialable_num + copied_num;
			_SPI_DBG(" ");
		}
	}
	
	if((p_spi->rxbuff_full == TRUE) && (copied_num != 0)) {
		p_spi->rxbuff_full = FALSE;
			_SPI_DBG(" ");
	}
	_SPI_DBG("END: rx pointer start 0x%x data 0x%x empty 0x%x",
		p_spi->rxbuff_start, p_spi->rxbuff_data, p_spi->rxbuff_empty);
	spin_unlock_irqrestore(&p_spi->lock_rx, flags);
	return copied_num;
}
EXPORT_SYMBOL( spi_cpu_tx);
EXPORT_SYMBOL( spi_cpu_rx);

static void exceptions_handle(unsigned int intr_type)
{
	unsigned char err_msg[100];
	switch(intr_type)
	{
	case(SPI_CPU_OV):
		strcpy(err_msg, "Overrun\n");
		break;
	case(SPI_CPU_UN):
		strcpy(err_msg, "Underrun\n");
		break;
#ifdef	CONFIG_MPC834x_SYS
	case(SPI_CPU_DNR):
		strcpy(err_msg, "DateNotReady\n");
		break;
#endif
	case(SPI_CPU_MME):
#ifdef FSL_SPI_QE_MODE
	case(SPI_QE_MME):
#endif
		strcpy(err_msg, "Multi-Master\n");
		break;
	case(SPI_CPU_LT):
		strcpy(err_msg, "Last character was transmitted\n");
		break;
#ifdef FSL_SPI_QE_MODE
	case(SPI_QE_TXE):
		strcy(err_msg, "Tx Error\n");
		break;
	case(SPI_QE_BSY):
		strcy(err_msg, "Busy\n");
		break;
#endif
	default:
		strcpy(err_msg, "Unknown interrupt type\n");
		break;
	}
	printk(KERN_ERR "%s %s", spi_slave_name, err_msg);
}

static irqreturn_t fsl_83xx_spi_intr(int irq, void *_spi, struct pt_regs *r)
{
	struct spi_836x	*p_spi = (struct spi_836x *)_spi;
	int	event, mask;
#ifdef	CONFIG_MPC834x_SYS
	int rx_repeat = 2, i, new_event;
#endif
	_SPI_DBG(" START ");

	event = p_spi->p_reg->spie;
	mask  = p_spi->p_reg->spim;
	event &= mask;

#ifdef	CONFIG_MPC834x_SYS
	while ( (event & SPI_CPU_NE) && rx_repeat ) {
		_SPI_DBG(" rx ");
		cpu_rx();
		/* the following is the workaround for mpc834x SPI errata */
		rx_repeat--;
		for (i = 0; i < WAIT_BETWEEN_RX; i++);
		new_event = p_spi->p_reg->spie;
		mask = p_spi->p_reg->spim;
        	new_event &= mask;
        	/* clear the event register */
        	p_spi->p_reg->spie = new_event & SPI_CPU_ALL_EVENTS;
		event &= (~SPI_CPU_NE);
		event |= new_event;
	}
#endif

#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
	/* NOT EMPTY, CPU can read the SPIRD */
	if(event & SPI_CPU_NE){
		_SPI_DBG(" rx ");
		cpu_rx();
	}
#endif
	/* NOT FULL, CPU can write to SPITD */
	if(event & SPI_CPU_NF) {
		_SPI_DBG(" tx ");
		cpu_tx();
		if(p_spi->tx_doing == FALSE) {	
			/* no data waiting for tx,
			 * the SPI_CPU_NF mask bit will be set at spi_cpu_tx()
			 * that means only upper layer user has somthing to tx, we open tx interrupt
			 * but the rx interrupt is always enabled*/
			p_spi->p_reg->spim = mask & (~SPI_CPU_NF);
		}


	}
#ifdef	CONFIG_MPC834x_SYS
	for(i = 0; i < WAIT_BETWEEN_RX; i++) ;
	new_event = p_spi->p_reg->spie;
	mask = p_spi->p_reg->spim;
    	new_event &= mask;
    	/* clear the event register */
        p_spi->p_reg->spie = new_event & SPI_CPU_ALL_EVENTS;
    	event &= (~SPI_CPU_NE);
	event |= new_event;
	if(event & SPI_CPU_NE)
		cpu_rx();
#endif	

	/* this is last character */
	if(event & SPI_CPU_LT)
		exceptions_handle(SPI_CPU_LT);
	if(event & SPI_CPU_MME)
		exceptions_handle(SPI_CPU_MME);
	if(event & SPI_CPU_UN)
		exceptions_handle(SPI_CPU_UN);
	if(event & SPI_CPU_OV)
		exceptions_handle(SPI_CPU_OV);
#ifdef	CONFIG_MPC834x_SYS
	if(event & SPI_CPU_DNR)
		exceptions_handle(SPI_CPU_DNR);

#endif
	/* clear the event register */
#ifdef FSL_SPI_CPU_MODE
	p_spi->p_reg->spie = event & SPI_CPU_ALL_EVENTS;
#endif
	_SPI_DBG(" END ");
	return IRQ_HANDLED;
}

/* routines about intialization */
#ifdef CONFIG_MPC8360E_PB
static void fsl_836x_spi_config_pin(void)
{
	par_io_config_pin(PE, 28, IO, 0, FUNC3 , 0);	/* SPI1MOSI */
	par_io_config_pin(PE, 29, IO, 0, FUNC3 , 0);	/* SPI1MISO*/
	par_io_config_pin(PE, 30, IO, 0, FUNC3 , 0);	/* SPI1CLK*/
	par_io_config_pin(PE, 31, OUT,0, FUNC0 , 0);	/* fix an error in spec */
}
#endif

#ifdef CONFIG_MPC832XE_MDS
static void fsl_832x_spi_config_pin(void)
{
	par_io_config_pin(PD, 0,  IO, 0, FUNC1 , 0);	/* SPI1MOSI */
	par_io_config_pin(PD, 1,  IO, 0, FUNC1 , 0);	/* SPI1MISO*/
	par_io_config_pin(PD, 2,  IO, 0, FUNC1 , 0);	/* SPI1CLK*/
	par_io_config_pin(PD, 9,  OUT,0, FUNC0 , 0);	/* SELECT fix an error in spec */
}
#endif

#ifdef CONFIG_MPC834x_SYS
static void fsl_834x_spi_config_pin(void)
{
	static volatile struct gpio_reg   *gpio_regs;
        gpio_regs = (struct gpio_reg *)(VIRT_IMMRBAR|GPIO_OFFSET);

        /* Configure the SPISEL  -- GPIO1[0] */
	gpio_regs->gpio_dir |= 0x80000000;
        gpio_regs->gpio_odr  &= ~0x80000000;
        gpio_regs->gpio_data |= 0x80000000;
        gpio_regs->gpio_mask &= ~0x80000000;

}
#endif

int fsl_83xx_spi_char_len(unsigned int char_len)
{
	unsigned int tmp_spmode;
	unsigned char code;
	if(! (char_len == 32 || (char_len >= 4 && char_len <= 16))) {
		printk(KERN_ERR "config char len error");
		return -EINVAL ;
	}
	gp_spi->char_len = char_len;
	/* set the char length code */
	if(char_len == 32)
		code = 0;
	else
        	code = char_len - 1;
 	tmp_spmode = gp_spi->p_reg->spmode;
    	gp_spi->p_reg->spmode = (tmp_spmode & (~SPMODE_LEN_MASK))
			 | ((code << SPMODE_LEN_SHIFT) & SPMODE_LEN_MASK);
	return E_OK;
}
/* qe_freq: current system qe freq
 * desire_freq: the desired spi frequency we want to set
 * divider:  DIV16 bit of spmode reg is set or not, TRUE, FALSE
 * return the value of  the actually spi frequency we set */
int fsl_83xx_spi_clk(unsigned int qe_clk, unsigned int desired_clk, unsigned char  divider)
{
	unsigned int tmp_clk;
	unsigned char tmp_prescale, tmp_div;
	unsigned int tmp_spmode;
	unsigned int tmp_actual;

	if (qe_clk == 0 || desired_clk == 0) {
		printk(KERN_ERR "config spi clk err");
		return -EINVAL;
	}
	if (divider == FALSE)
		tmp_clk = qe_clk / desired_clk;
	else
        	tmp_clk = qe_clk * desired_clk;
	if (tmp_clk > 1024)
		tmp_clk = 1024;
	else if( tmp_clk < 4 )
		tmp_clk = 4;
	
	/* clock is divided at least by 4 when (spmode PM bit is 0)
	 * the divide at 16 must be done only at clock dividers bigger than 64*/
	if(tmp_clk > 64) {
		/* get to the closest 64 multiplication */
		tmp_clk  = ((tmp_clk + 32) / 64) * 64;
		tmp_div = TRUE;
		tmp_clk /= 16;
	}
	 else {
		/* In the range of 4 - 64 it is in multiplication of 4. */
		tmp_clk = ((tmp_clk + 2) / 4) * 4;
		tmp_div = FALSE;
	}
	/* calculate the PM bits value for spmode reg */
	tmp_prescale = (unsigned char)((tmp_clk / 4) - 1);
	tmp_actual = qe_clk / (4 * (tmp_prescale + 1));

	if(tmp_div)	tmp_actual /= 16;

	tmp_spmode = gp_spi->p_reg->spmode;
	tmp_spmode = (tmp_spmode & (~(SPMODE_DIV16|SPMODE_PM_MASK))) |
        	(tmp_div ? SPMODE_DIV16 : 0) |
		 ((tmp_prescale << SPMODE_PM_SHIFT) & SPMODE_PM_MASK);
	gp_spi->p_reg->spmode = tmp_spmode;

	_SPI_DBG("qe_clk or csb_clk %d desire_clk %d actual_clk %d spmode 0x%x ",qe_clk, desired_clk, tmp_actual,tmp_spmode);
	
	return tmp_actual;
}

static void fsl_83xx_spi_tx_flush(void)
{
#ifdef FSL_SPI_CPU_MODE
	unsigned int tmp_nums;

	/* confirm the successful tx nums o the up layer */
	while(gp_spi->p_txqueue_curr->char_nums != 0) {
		
		tmp_nums = gp_spi->p_txqueue_curr->char_nums -
			   gp_spi->p_txqueue_curr->curr_location;
		/* origanlly m25p40.c tx_conf( tmp_nums ); */
	
    		spi_m25p40_txconf_char_nums = tmp_nums;
        	gp_spi->p_txqueue_curr->char_nums = 0;
		if(gp_spi->p_txqueue_curr->last == TRUE)
			gp_spi->p_txqueue_curr = gp_spi->p_txqueue_start;
		else
			gp_spi->p_txqueue_curr++;
	}
	return;
#endif
}

static void fsl_83xx_spi_rx_flush(void)
{	
	unsigned int  tmp_evt, i, tmp_val;
#ifdef FSL_SPI_CPU_MODE
	/* if there is data in spird reg, read it and discard untill no data */
        tmp_evt = gp_spi->p_reg->spie;

	for( i = 0; i < 0x100; i++) {}
	
	/* read all the data left and discard*/
	while(tmp_evt & SPI_CPU_NE) {
        	gp_spi->p_reg->spie |= SPI_CPU_NE;
                tmp_val = gp_spi->p_reg->spird;
                tmp_evt = gp_spi->p_reg->spie;
		for( i = 0; i < 0x100; i++) {}
	}
#endif
	
	gp_spi->rxbuff_data = gp_spi->rxbuff_start;
	gp_spi->rxbuff_empty = gp_spi->rxbuff_start;
	gp_spi->rxbuff_full = FALSE;
	return ;
}

void fsl_83xx_spi_enable(void)
{
	gp_spi->p_reg->spmode |= SPMODE_EN;
	return ;
}

void fsl_83xx_spi_disable(void)
{
	unsigned long flags;

	spin_lock_irqsave(&gp_spi->lock_tx, flags);
	gp_spi->p_reg->spmode &= ~SPMODE_EN;
	gp_spi->tx_doing  = FALSE;
	_SPI_DBG("flush tx and rx");
#ifdef FSL_SPI_CPU_MODE
	fsl_83xx_spi_tx_flush();
#endif
	fsl_83xx_spi_rx_flush();
	spin_unlock_irqrestore(&gp_spi->lock_tx, flags);
	return;
}
int fsl_83xx_spi_device_sel(unsigned char select)
{
#ifdef CONFIG_MPC8360E_PB
/* select = TRUE, set cpdata_e reg bit 31 as 0 to select m25p40*/
        return (par_io_data_set(PE, 31, (1 - select)));
#endif
#ifdef CONFIG_MPC832XE_MDS
        return (par_io_data_set(PD, 9, (1 - select)));
#endif
#ifdef CONFIG_MPC834x_SYS
	static volatile struct gpio_reg   *gpio_regs;
        gpio_regs = (struct gpio_reg *)(VIRT_IMMRBAR|GPIO_OFFSET);

	gpio_regs->gpio_dir |= 0x80000000;
	if (select == TRUE)
		gpio_regs->gpio_data &= ~0x80000000;
	else
		gpio_regs->gpio_data |=  0x80000000;
	return E_OK;
#endif
}
EXPORT_SYMBOL(fsl_83xx_spi_device_sel);

EXPORT_SYMBOL(fsl_83xx_spi_char_len);
EXPORT_SYMBOL(fsl_83xx_spi_clk);
EXPORT_SYMBOL(fsl_83xx_spi_enable);
EXPORT_SYMBOL(fsl_83xx_spi_disable);

/* when initialize failed, free resource */
static int __init fsl_83xx_spi_probe(struct device *dev)
{
	struct platform_device	*pdev = to_platform_device(dev);
    	struct spi_txqueue	*tmp_txqueue;
	unsigned int		tmp_size, i, tmp_offset;
	unsigned int		tmp_spmode = 0;
	int			rc;

#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
	if (strcmp(pdev->name, "fsl-qe-spi1")
		&& strcmp(pdev->name, "fsl-qe-spi2")) {
		printk(KERN_ERR "Wrong platform device\n");
		return -ENODEV;
	}
#endif
#ifdef CONFIG_MPC834x_SYS
	if (strcmp(pdev->name, "fsl-spi")) {
		printk(KERN_ERR "Wrong platform device\n");
		return -ENODEV;
	}
#endif
	
#ifdef CONFIG_MPC8360E_PB
	fsl_836x_spi_config_pin();
#endif
#ifdef CONFIG_MPC832XE_MDS
	fsl_832x_spi_config_pin();
#endif
#ifdef CONFIG_MPC834x_SYS
	fsl_834x_spi_config_pin();
#endif

	/* Initialize internal data */
	if (pdev->resource[0].flags != IORESOURCE_MEM
			|| pdev->resource[1].flags != IORESOURCE_IRQ) {
		printk(KERN_ERR "error resource ");
		return -ENODEV;
	}
	if (!request_mem_region(pdev->resource[0].start,
				pdev->resource[0].end -
				pdev->resource[0].start + 1,
				driver_name)) {
		printk(KERN_ERR "request mem region for %s failed \n", pdev->name);
		return -EBUSY;
	}
	gp_spi = (struct spi_836x *)kmalloc(sizeof(struct spi_836x), GFP_KERNEL);
	if (gp_spi == NULL) {
		printk(KERN_ERR "malloc gp_spi failed\n");
		return -ENOSPC;
	}
	/* Zero out the internal USB state structure */
	memset(gp_spi, 0, sizeof(struct spi_836x));

	gp_spi->p_reg = (struct spi_reg *)ioremap(pdev->resource[0].start, sizeof(struct spi_reg));
	gp_spi->tx_doing = 0;

#ifdef FSL_SPI_CPU_MODE
	/* initialize TX buff and queue */
	tmp_size = MAX_TXBUFF_NUM * sizeof(struct spi_txqueue);
        gp_spi->p_txqueue_start = (struct spi_txqueue *)
				kmalloc(tmp_size, GFP_KERNEL);
        gp_spi->p_txqueue_empty = gp_spi->p_txqueue_start;
        gp_spi->p_txqueue_curr = gp_spi->p_txqueue_start;
        if(gp_spi->p_txqueue_start == NULL)
        {
		printk(KERN_ERR "malloc txqueue failed");
		goto fail1;
	}
	memset(gp_spi->p_txqueue_start, 0, tmp_size);

	for(i = 0; i < MAX_TXBUFF_NUM; i++) {
		tmp_offset = i * sizeof(struct spi_txqueue);
		tmp_txqueue = (struct spi_txqueue *)
			(((unsigned int)gp_spi->p_txqueue_start) + tmp_offset);
		tmp_txqueue->data = NULL;
		tmp_txqueue->char_nums = 0;
		tmp_txqueue->curr_location = 0;
		tmp_txqueue->last = FALSE;
	}
        tmp_txqueue->last = TRUE;
#endif
	/* Assigned PRAM for SPI in QE mode (use QE_IssueCmd(assigned page) */

	/* initlaize the RX buff and queue */
	gp_spi->rxbuff_start = (unsigned int)kmalloc(DEFAULT_RXBUFF_SIZE, GFP_KERNEL);
	if((void *)gp_spi->rxbuff_start == NULL) {
		printk(KERN_ERR "malloc rxbuf failed");
		goto fail2;
	}
	gp_spi->rxbuff_data = gp_spi->rxbuff_start;
	gp_spi->rxbuff_empty = gp_spi->rxbuff_start;
	gp_spi->rxbuff_full  = FALSE;
 	
	/* intialize share lock */
	spin_lock_init(&gp_spi->lock_rx);
	spin_lock_init(&gp_spi->lock_tx);

	/* initialize QE SPI reg */

	gp_spi->p_reg->spmode = 0;
#ifdef FSL_SPI_CPU_MODE
	tmp_spmode |= SPMODE_MS | SPMODE_DIV16 | SPMODE_REV |
		 ((DEFAULT_PRESCALE_MODE_IN_USE << SPMODE_PM_SHIFT) & SPMODE_PM_MASK);
#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
	tmp_spmode |= SPMODE_OPMODE ;
#endif	
	gp_spi->p_reg->spmode |= tmp_spmode;
	
#if defined CONFIG_MPC8360E_PB || defined CONFIG_MPC832XE_MDS
        gp_spi->p_reg->spie |= SPI_CPU_ALL_EVENTS | SPI_CPU_NF;
        gp_spi->p_reg->spim |= SPI_CPU_ALL_EVENTS |SPI_CPU_NF ;
#endif
#ifdef CONFIG_MPC834x_SYS
        gp_spi->p_reg->spie |= SPI_CPU_ALL_EVENTS | SPI_CPU_NF | SPI_CPU_NE;
        gp_spi->p_reg->spim |= SPI_CPU_ALL_EVENTS |SPI_CPU_NF | SPI_CPU_NE;
#endif
#endif //FSL_SPI_CPU_MODE
	gp_spi->irq = pdev->resource[1].start;
	_SPI_DBG("gp_spi->p_reg addr 0x%p spmode 0x%x irq %d",
		gp_spi->p_reg, gp_spi->p_reg->spmode, (int)gp_spi->irq);
	/* request irq for SPI */
	rc = request_irq(gp_spi->irq ,fsl_83xx_spi_intr, 0, driver_name, gp_spi);
	if (rc != 0)
	{
		printk(KERN_ERR "request IRQ 0x%lx in %s failed with rc 0x%x", gp_spi->irq, driver_name, rc);
		goto fail3;
	}
	/* enable interrupt for spi1*/
#ifdef CONFIG_MPC8360E_PB
	printk(KERN_INFO "FSL 836x SPI %d in %s Mode init\n", FSL_836X_SPI_ID, spi_mode_str);
#endif
#ifdef CONFIG_MPC832XE_MDS
	printk(KERN_INFO "FSL 832x SPI %d in %s Mode init\n", FSL_836X_SPI_ID, spi_mode_str);
#endif
#ifdef CONFIG_MPC834x_SYS
	printk(KERN_INFO "FSL 834x SPI in %s Mode init\n", spi_mode_str);
#endif
	return E_OK;

fail3:
	kfree((void*)gp_spi->rxbuff_start);
fail2:
	kfree(gp_spi->p_txqueue_start);
fail1:
	kfree(gp_spi);
	release_mem_region(pdev->resource[0].start,
				pdev->resource[0].end -
				pdev->resource[0].start + 1);
	return E_ERR;
}

static int __exit fsl_83xx_spi_remove(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);

	if (!gp_spi)
		return -ENODEV;
	free_irq(gp_spi->irq, gp_spi);
	kfree((void*)gp_spi->rxbuff_start);
#ifdef FSL_SPI_CPU_MODE
	kfree(gp_spi->p_txqueue_start);
#endif
	kfree(gp_spi);
	release_mem_region(pdev->resource[0].start,
				pdev->resource[0].end -
				pdev->resource[0].start + 1);
	return E_OK;

}
static struct device_driver fsl_83xx_spi_driver = {
	.name = (char *) driver_name,
	.bus    = &platform_bus_type,
	.probe  = fsl_83xx_spi_probe,
	.remove = fsl_83xx_spi_remove,
};

static int __init ppc83xx_spi_init(void)
{
	return driver_register(&fsl_83xx_spi_driver);
}


static void __exit ppc83xx_spi_exit(void)
{
	driver_unregister(&fsl_83xx_spi_driver);
}

module_init(ppc83xx_spi_init);
module_exit(ppc83xx_spi_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Freescale 83xx SPI driver");
MODULE_AUTHOR("Tanya Jiang");

