/*
 * drivers/char/ppc83xx_spi.h
 * This is SPI driver for MPC8360 MPC8325 and MPC8349
 * for MPC8360 and MPC8325, SPI1 in CPU mode
 *
 * Copyright (C) 2006 Freescale Semiconductor, Inc
 *
 * Porting to linux by tanya.jiang@freescale.com
 * Based on MPC8360 bare board code of Freescale Ishral by amirh nire and yoramsh.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __PPC83XX_SPI_H
#define __PPC83XX_SPI_H

#include <asm/mpc83xx.h>
#include <linux/spinlock.h>

#define FSL_83XX_SPI_ID		1	
#define FSL_SPI_CPU_MODE

/* the pin configuration */
#ifdef CONFIG_MPC834x_SYS
#define GPIO_OFFSET 0x00000C00

struct gpio_reg {
	unsigned int gpio_dir;    /* gpio direction reg*/
	unsigned int gpio_odr;    /* gpio open drain reg*/
	unsigned int gpio_data;   /* gpio data reg */
	unsigned int gpio_int;    /* gpio interrupt event reg */
	unsigned int gpio_mask;   /* gpio interrupt mask reg */
	unsigned int gpio_icr;    /* gpio external interrupt control reg */
};

#define WAIT_BETWEEN_RX	100
#endif

#if defined  CONFIG_MPC832XE_MDS || defined CONFIG_MPC8360E_PB
/* paralell io functin */
#define	PA	0
#define	PB	1
#define	PC	2
#define	PD	3
#define	PE	4
#define	PF	5
#define	PG	6

#define	DISABLE	0
#define	OUT	1
#define	IN	2
#define	IO	3

#define	FUNC0	0
#define	FUNC1	1
#define	FUNC2	2
#define	FUNC3	3
#endif

/* SPI mode register description */
#define SPMODE_EM		0x80000000  /* Emergency request to QE RISC 		*/
#define SPMODE_LOOP		0x40000000  /* Loop mode enable, 0-normal, 1-loopback   */
#define SPMODE_CI		0x20000000  /* Clock invert, 0-SPICLK is low, 1-high    */
#define SPMODE_CP		0x10000000  /* Clock phase, 0-SPICLK toggle in middle of data, 1-at start of data */
#define SPMODE_DIV16  		0x08000000  /* Divide by 16 				*/
#define SPMODE_REV    		0x04000000  /* Reverse mode, 0-LSB first, 1-MSB first   */
#define SPMODE_MS     		0x02000000  /* Master/Slave, 0-Slave, 1-Master 		*/
#define SPMODE_EN     		0x01000000  /* Enable SPI, 0-disable, 1-enable 		*/
#define SPMODE_LEN_MASK 	0x00F00000  /* Character length, 0011(4bit) to 1111(16bit) or 0000(32bit) */
#define SPMODE_LEN_SHIFT  	20          /* Shift to Character length  		*/
#define SPMODE_PM_MASK    	0x000F0000  /* prescale modulus select    		*/
#define SPMODE_PM_SHIFT   	16          /* Shift to prescale modulus select 	*/
#define SPMODE_OPMODE   	0x00004000  /* Operation mode, 0-QE, 1-CPU 		*/
#define SPMODE_MII      	0x00002000  /* MIIMCOM mode, 0-regular SPI, 1-MIIMCOM mode */
#define SPMODE_CG_MASK  	0x00000F80  /* Clock gap (master mode or MIIMCOM only)	*/
#define SPMODE_CG_SHIFT 	7           /* Shift to clock-gap select 		*/

/* SPI - CPU mode and mask register description*/
#define SPI_CPU_LT		0x00004000  /* Last character was transmitted */
#define SPI_CPU_OV		0x00001000  /* Over run           	*/
#define SPI_CPU_UN		0x00000800  /* Slave under run    	*/
#define SPI_CPU_MME		0x00000400  /* Multimaster errors 	*/
#define SPI_CPU_NE		0x00000200  /* Not empty          	*/
#define SPI_CPU_NF		0x00000100  /* Not full           	*/

/* pay attention, we deal with SPI_CPU_NF separately */
#if defined  CONFIG_MPC832XE_MDS || defined CONFIG_MPC8360E_PB
#define SPI_CPU_ALL_EVENTS  	(SPI_CPU_LT | SPI_CPU_OV | SPI_CPU_UN | SPI_CPU_MME | SPI_CPU_NE)
#endif
#ifdef CONFIG_MPC834x_SYS
#define SPI_CPU_DNR		0x00002000
#define SPI_CPU_ALL_EVENTS  	(SPI_CPU_DNR| SPI_CPU_OV | SPI_CPU_UN | SPI_CPU_MME)
#endif

#if defined  CONFIG_MPC832XE_MDS || defined CONFIG_MPC8360E_PB
/* SPI - QE mode - event register description */
#define SPI_QE_MME		0x00002000  /* Multimaster error 		 */
#define SPI_QE_TXE   		0x00001000  /* Tx Error           		 */
#define SPI_QE_BSY   		0x00000400  /* Busy (no Rx buffer available) 	 */
#define SPI_QE_TXB   		0x00000200  /* Tx buffer: last char written to Tx FIFO		   */
#define SPI_QE_RXB   		0x00000100  /* Rx buffer: last char written to Rx buffer, BD closed */
#define SPI_QE_ALL_EVENTS  (SPI_QE_MME | SPI_QE_TXE | SPI_QE_BSY | SPI_QE_TXB | SPI_QE_RXB)

/* SPI - QE mode - mask register description (not available for MPC834x) */
#define SPI_QE_MME   		0x00002000  /**< Multimaster error  */
#define SPI_QE_TXE   		0x00001000  /**< Tx Error           */
#define SPI_QE_BSY   		0x00000400  /**< Busy (no Rx buffer available) */
#define SPI_QE_TXB   		0x00000200  /**< Tx buffer: last char written to Tx FIFO */
#define SPI_QE_RXB   		0x00000100  /**< Rx buffer: last char written to Rx buffer, BD closed */
#endif

#define DEFAULT_PRESCALE_MODE_IN_USE	0xf
#define MAX_TXBUFF_NUM			32

/* notice: if user app try to tx too many byte at one time,
 * the dummy rx byte may fill the rx buff full
 * and when read, zero bytes may exceed the MAX_ZERO_NUM
 * so we recommend read/write less bytes at one time
 * and through addr parameter of read/write, go through the whole flash
 */
#define DEFAULT_RXBUFF_SIZE		(1024*4) /* Size of bytes for Rx buffer */

#define FALSE	0
#define TRUE	1
#define E_OK	0
#define E_ERR	1

struct spi_reg {
	unsigned char		res0[0x20];
	unsigned int 		spmode;  	/* SPI mode register */
	unsigned int 		spie;     	/* SPI event register */
	unsigned int 		spim;	     	/* SPI mask register */
	unsigned int 		spcom;  	/* SPI command register  */
	unsigned int 		spitd;    	/* SPI transmit data register (cpu mode) */
	unsigned int 		spird;   	/* SPI receive data register (cpu mode) */
	unsigned char  		res7[0x8];
};

#if defined  CONFIG_MPC832XE_MDS || defined CONFIG_MPC8360E_PB
struct spi_parm {
	unsigned short  	rbase;		/* Rx BD table base address in multi-user RAM	*/
	unsigned short  	tbase;  	/* Tx BD table base address in multi-user RAM	*/
	unsigned char   	rx_bus_mode;	/* Rx bus mode register                      	*/
	unsigned char   	tx_bus_mode;	/* Tx bus mode register                       	*/
	unsigned short  	mrblr;		/* Maximum receive buffer length             	*/
	unsigned int    	qe_reserved_0[2];/* Reserved for QE use                        	*/
	unsigned short 		rbptr;		 /* RxBD pointer - current (next if idle) RxBD 	*/
	unsigned short 		qe_reserved_1[7];/* Reserved for QE use                        	*/
	unsigned short 		tbptr;		 /* TxBD pointer - current (next if idle) TxBD 	*/
	unsigned short 		qe_reserved_2[21];/* Reserved for QE use                        */
};
#endif

struct spi_txqueue {
	void			*data;
	unsigned int		char_nums;		/* number of characters to transmit		*/
	unsigned int		curr_location;		/* current charecter in the buffer to transmit  */
	unsigned char		last;			/* specify the last queue-entry.                */
};

struct spi_836x
{
	struct spi_reg		*p_reg;     	/* SPI control register space */
	unsigned long		irq;		/* the interrupt number for spi */
	unsigned char		char_len;	/* The length of a SPI character in bits, 4~16 or 32 */
	unsigned char		tx_doing;  	/* tx transcation is doing */
	unsigned int		rxbuff_start;	/* Start Address of the Rx data buffer */
	unsigned int		rxbuff_data;	/* Addr of the first received data that was not passed to the user */
	unsigned int		rxbuff_empty;	/* Address of the next empty location to save received data */
	unsigned char		rxbuff_full;	/* flag */
	unsigned char		loop_mode;	/* 1 loop mode; 0 normal mode */
	struct spi_txqueue	*p_txqueue_start;/* pointer to the start of Tx buffers block */
	struct spi_txqueue	*p_txqueue_empty;/* pointer to the next empty entry to fill  */
	struct spi_txqueue	*p_txqueue_curr; /* pointer to the first full entry */

	spinlock_t		lock_rx;	/* lock for rxbuff_data, rxbuff_empty, rxbuff_full */
	spinlock_t		lock_tx;	/* lock for txqueue_curr and tx_doing */
	spinlock_t		lock_reg;	/* lock for mask and event registers  */
};

//#define FSL_83XX_SPI_DEBUG
#ifdef FSL_83XX_SPI_DEBUG
#define _SPI_DBG(fmt, args...) 	printk(KERN_DEBUG "\n[%s]:[line%d]----" fmt, __FUNCTION__, __LINE__, ## args)
#else
#define _SPI_DBG(fmt, args...)	do{}while(0)
#endif

#endif //PPC_83XX_SPI
