/*
 * drivers/char/spi_flash_m25p40.c
 * ST M25P40 Flash driver for MPC836x SPI driver
 *
 * Copyright (C) 2006 Freescale Semiconductor, Inc
 *
 * Porting to linux by tanya.jiang@freescale.com
 * Based on MPC8360 bare board code of Freescale Ishral by amirh nire and yoramsh.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 /* notice:
 * notice: if user app try to tx too many byte at one time,
 * the dummy rx byte may fill the rx buff full
 * and when read, zero bytes may exceed the MAX_ZERO_NUM
 * so we recommend read/write less bytes at one time
 * and through addr parameter of read/write, go through the whole flash
 */

#include <linux/module.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <asm/uaccess.h>
#include <asm/page.h>

#include "spi_flash_m25p40.h"

/*****************************************************************
 * Global Variables
 ****************************************************************/

extern unsigned int  spi_m25p40_txconf_char_nums;
/**************************************************************
 * Internal Variables and Functions
 **************************************************************/
extern int fsl_83xx_spi_char_len(unsigned int char_len);
extern int fsl_83xx_spi_clk(unsigned int qe_clk,
		unsigned int desired_clk, unsigned char  divider);
extern void fsl_83xx_spi_enable(void);
extern void fsl_83xx_spi_disable(void);
extern void fsl_83xx_spi_device_sel(unsigned char select);
extern int spi_cpu_rx( void *dest, unsigned int num);
extern int spi_cpu_tx( void *data, unsigned int num);

static int spi_m25p40_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);
static int spi_m25p40_open (struct inode *inode, struct file *file);
static int spi_m25p40_close (struct inode *inode, struct file *file);

static struct file_operations spi_m25p40_fops = {
	.owner		= THIS_MODULE,
	.ioctl		= spi_m25p40_ioctl,
	.open		= spi_m25p40_open,
	.release	= spi_m25p40_close,
};
static const char driver_name[]= "spi_m25p40";
static const char driver_ver[]= "1.0";
static int m25p40_major = -EINVAL  ;
static unsigned int spi_m25p40_open_flag = 0;

static void spi_m25p40_waiting(void)
{
	unsigned int retry =  RETRY;
	while(--retry) {}
	return;
}

static void spi_m25p40_enable(void)
{
	unsigned int  i;
 	
	fsl_83xx_spi_char_len(8);
	fsl_83xx_spi_clk(QE_CLK, 20, FALSE);
	fsl_83xx_spi_enable();
	fsl_83xx_spi_device_sel(TRUE);
	for(i = 0; i < 0x100; i++){}

	return ;
}

static void spi_m25p40_disable(void)
{
	int i;
	fsl_83xx_spi_disable();
	fsl_83xx_spi_device_sel(FALSE);
	for(i = 0; i < 0x100; i++){}
	return ;
}
/* return E_OK or E_ERR */
static unsigned int spi_m25p40_cmd(unsigned char cmd, unsigned int addr, unsigned int dummy_num)
{
	unsigned int	len;
	unsigned char	tx_data[MAX_CMD_LENGTH];
	
	
	len = dummy_num + 1 ;
	tx_data[0] = cmd;
	_SPI_DBG("cmd 0x%x at 0x%p dummny 0x%x ", tx_data[0], tx_data, dummy_num );
	if (len == 2)
		_SPI_DBG("dummy is 0x%x",tx_data[1]);
	
	/* for read, fast_read, page_program, sector_erase command */
	if(addr != NO_ADDRESS_CYCLE)
	{
		len += 3;
		/* it is 24 bits address */
		addr &= 0x00ffffff;
		tx_data[1] = (unsigned char)(addr >> 16); /* The 8 MSB bits of 24bit address */
		tx_data[2] = (unsigned char)(addr >> 8);  /* The 8 middle bits */
		tx_data[3] = (unsigned char)(addr);       /* The 8 LSB bits of 24bit address */
	}
#ifdef FSL_SPI_CPU_MODE
	if (spi_cpu_tx(tx_data, len) != E_OK) {
		printk(KERN_ERR "m25p40 cmd tx failed\n");
		return E_ERR;
	}
#endif
    return E_OK;
}

/* return E_OK or E_ERR */
unsigned int spi_m25p40_read_status( unsigned char *status)
{
	unsigned int rc;
	unsigned int retry = RETRY;
	
	spi_m25p40_enable();
	rc = spi_m25p40_cmd(M25P40_READ_STATUS_CMD, NO_ADDRESS_CYCLE, 1);
	if( rc != E_OK) {
		printk(KERN_ERR "m25p40 read status cmd failed\n");
		return E_ERR;
	}
 	
	/* wait the transmission is done */
	spi_m25p40_waiting();

	/* read the first dummy data */
	retry = RETRY;
#ifdef FSL_SPI_CPU_MODE
	while(spi_cpu_rx(status, 1) == 0 && --retry) {}
	
	if(!retry) {/* device is busy */
		printk(KERN_ERR "m25p40 read dummy timeout\n");
		return E_ERR;
	}
#endif	
	/* read the real status data */
	retry = RETRY;
#ifdef FSL_SPI_CPU_MODE
	while(spi_cpu_rx(status, 1) == 0 && --retry) {}
	if(!retry) {/* device is busy */
		printk(KERN_ERR "m25p40 read status data timeout\n");
		return E_ERR;
	}
#endif
	spi_m25p40_disable();
 	
	return E_OK;
}

/* return E_OK or E_ERR */
static unsigned int spi_m25p40_write_enable(void)
{
	unsigned int   rc;
	unsigned int  retry = RETRY;
	unsigned char   status;
 	
	/* Check that the flash device is not write protected */
	rc = spi_m25p40_read_status (&status) ;
	_SPI_DBG(" status 0x%x", status);	
	if ((rc != E_OK) || (status == 0xFF)) {
		printk(KERN_ERR "m25p40 isnot connect correctly\n");
		return E_ERR;
	}
	if (( status & M25P40_STATUS_SRWD) == M25P40_STATUS_SRWD ) {
		printk(KERN_ERR "m25p40 is write protected\n");
		return E_ERR;
	}
	/* wait if device is write-in-progress  */
	while( (rc == E_OK ) && --retry &&
		((status & M25P40_STATUS_WIP) == M25P40_STATUS_WIP) ) {
		 rc = spi_m25p40_read_status(&status);
	}
	if(!retry || rc != E_OK) {
		printk(KERN_ERR "m25p40 is busy\n");
		return E_ERR;
	}
	spi_m25p40_enable();
 	
	rc = spi_m25p40_cmd(M25P40_WRITE_ENABLE_CMD, NO_ADDRESS_CYCLE, 0);
	if( rc != E_OK) {
		printk(KERN_ERR "m25p40 write enable cmd failed\n");
		return E_ERR;
	}
	spi_m25p40_waiting();

	spi_m25p40_disable();
 	
	/* Check that the flash device is write enable */
	rc = spi_m25p40_read_status( &status);
	_SPI_DBG("status 0x%x", status);
	if(rc != E_OK || (status & M25P40_STATUS_WEL) != M25P40_STATUS_WEL) {
		printk(KERN_ERR "m25p40 write enable status failed\n");
		return E_ERR;
	}
	return E_OK;
}

/* addr : should 0~ M25P40_SIZE
 * size : should 0~ M25P40_PAGE_SIZE
 * return value: the successful tx number
 */
static unsigned int spi_m25p40_page_program(unsigned int addr,
			unsigned char *data, unsigned int size)
{
	unsigned int   rc;

	_SPI_DBG("START addr 0x%x data 0x%x size 0x%x",addr, (int)data, size);
	if (addr >= M25P40_SIZE) {
		printk(KERN_ERR "m25p40 page program addr illegal\n");
		return 0;
	}
 	if(size == 0)	return 0;
 	if(size > M25P40_PAGE_SIZE)
		size = M25P40_PAGE_SIZE;
 	rc = spi_m25p40_write_enable();
	if(rc != E_OK) {
		printk(KERN_ERR "m25p40 write enable failed\n");
		return 0;
	}
	spi_m25p40_enable();

 	rc = spi_m25p40_cmd(M25P40_PAGE_PROGRAM_CMD, addr, 0);
	if( rc != E_OK) {
		spi_m25p40_disable();
		printk(KERN_ERR "m25p40 page program cmd failed\n");
		return 0;
	}
	_SPI_DBG(" START TX ALL DATA");
#ifdef FSL_SPI_CPU_MODE
 	rc = spi_cpu_tx(data, size);
	if( rc != E_OK) {
		spi_m25p40_disable();
		printk(KERN_ERR "m25p40 tx page program failed\n");
		return 0;
	}
#endif
	spi_m25p40_waiting();
	
	spi_m25p40_disable();

	return spi_m25p40_txconf_char_nums;
}

/* addr : should 0~ M25P40_SIZE
 * return total write byte successfully
 */
unsigned int spi_m25p40_write(unsigned int addr, unsigned char *data, unsigned int size)
{
	unsigned int  cycle_size , tx_bytes , total_bytes;

	_SPI_DBG("START");
	cycle_size = tx_bytes = total_bytes = 0;
	
	if (addr >= M25P40_SIZE) {
		printk(KERN_ERR "m25p40 write addr illegal\n");
		_SPI_DBG("END");
		return 0;
	}
	if(size == 0) {
		_SPI_DBG("END");
		return 0;
 	}
	/* make sure that we do not exceeds address range */
	if((addr + size) > M25P40_SIZE)
		size = (unsigned int)(M25P40_SIZE - addr);
	while(size > 0) {
		/* first write the rest, then page by page */
		cycle_size = (unsigned int)(M25P40_PAGE_SIZE - (addr & (M25P40_PAGE_SIZE - 1)));
		if(size < cycle_size)
			cycle_size = size;
		_SPI_DBG("cycle_size 0x%x", cycle_size);
		tx_bytes =  spi_m25p40_page_program(addr, data, cycle_size);
		if(tx_bytes == 0) {
			_SPI_DBG("END");
			return total_bytes;
		}
		addr += tx_bytes;
		data += tx_bytes;
		size -= tx_bytes;
		total_bytes += tx_bytes;
	}
	_SPI_DBG("END");
	return total_bytes;
}
/* addr : should 0~ M25P40_SIZE
* return value: total received data num successfully */
unsigned int spi_m25p40_read( unsigned int addr, unsigned char *data,
				unsigned int size, unsigned char read_mode)
{
    unsigned int  total_rx_num, rx_num, zero_cnt;
    unsigned int  rc, i, retry;
    unsigned char cmd_size, status;

	_SPI_DBG("START");
	total_rx_num = rx_num = zero_cnt = 0;
	
	if (addr >= M25P40_SIZE) {
		printk(KERN_ERR "m25p40 read addr illegal\n");
		return 0;
	}
	if(size == 0)	return 0;
	
	/* wait until the device is not busy */
	rc = spi_m25p40_read_status( &status);
	retry = RETRY;
	while((rc == E_OK) && ((status & M25P40_STATUS_WIP) == M25P40_STATUS_WIP) && --retry){
 	       rc = spi_m25p40_read_status( &status);
	}
	/* device is busy */
	if(!retry || rc != E_OK) {
		printk(KERN_ERR "m25p40 read failed \n");
		_SPI_DBG("END");
		return 0;
	}
	
	/* check the address range */
	if((addr + size) > M25P40_SIZE)
		size = (unsigned int)(M25P40_SIZE - addr);
	
	spi_m25p40_enable();
 	
	if(read_mode == SPI_M25P40_FAST_READ_MODE) {
        	rc = spi_m25p40_cmd( M25P40_FAST_READ_CMD, addr, 1);
		cmd_size = 5;
	}
	 else {
	        rc = spi_m25p40_cmd( M25P40_READ_DATA_BYTES_CMD, addr, 0);
		cmd_size = 4;
	}
	if( rc != E_OK) {
		printk(KERN_ERR "m25p40 read tx cmd failed\n");
		spi_m25p40_disable();
		_SPI_DBG("END");
		return 0;
	}
 	
	/* to generate CLK to m25p40, issue dummy*/
#ifdef FSL_SPI_CPU_MODE
	rc = spi_cpu_tx(data, size);
#endif
	if((read_mode == SPI_M25P40_FAST_READ_MODE) && (rc == E_OK))
		/* transmit some more clocks */
		rc = spi_cpu_tx(data, size);
	if(rc != E_OK) {
       		printk(KERN_ERR "m25p40 read tx dummy failed\n");
		spi_m25p40_disable();
		return 0;
	}
	
	spi_m25p40_waiting();

	/* ignore the bytes received because of issuing cmd */
	while(total_rx_num < cmd_size) {
#ifdef FSL_SPI_CPU_MODE
		rx_num = spi_cpu_rx( &data[total_rx_num], cmd_size - total_rx_num);
#endif
		total_rx_num += rx_num;

	        if(rx_num == 0)	{ /* stuck checking */
			zero_cnt++;
			if(zero_cnt > MAX_TIME_TO_RECEIVE_ZERO_BYTES) {
				printk(KERN_ERR "m25p40 read zero too many times in cmd\n");
				spi_m25p40_disable();
				_SPI_DBG("END");
				return 0;
			}
		} else
        		zero_cnt = 0;

        	for(i = 0; i < WAITS_BETWEEN_RECEIVES; i++){}
	}//end of while(total_rx_num < cmd_size)
	
	total_rx_num = zero_cnt = 0;

	while(total_rx_num < size) {
#ifdef FSL_SPI_CPU_MODE
		rx_num = spi_cpu_rx( &data[total_rx_num], size - total_rx_num);
#endif
		total_rx_num += rx_num;
		if(rx_num == 0) { /* stuck checking */
	        	zero_cnt++;
			if(zero_cnt > MAX_TIME_TO_RECEIVE_ZERO_BYTES) {
				printk(KERN_ERR "m25p40 read zero too many times in data\n");
				spi_m25p40_disable();
				_SPI_DBG("END");
				return total_rx_num;
			}
		} else
			zero_cnt = 0;
			/* wait to the next buffer */
		for(i = 0; i < WAITS_BETWEEN_RECEIVES; i++){}
	}//end of while(total_rx_num < size)
	
	spi_m25p40_disable();

	_SPI_DBG("END");
	 return total_rx_num;
}

unsigned int spi_m25p40_sector_erase( unsigned int addr)
{
	unsigned int	rc;
	
	rc = spi_m25p40_write_enable();
	if(rc != E_OK) {
		printk(KERN_ERR "m25p40 sector erase failed in enable write\n");
		return E_ERR;
	}
	spi_m25p40_enable();
	rc = spi_m25p40_cmd(M25P40_SECTOR_ERASE_CMD, addr, 0);
	if( rc != E_OK) {
		printk(KERN_ERR "m25p40 sector erase failed in tx cmd\n");
		spi_m25p40_disable();
		return E_ERR;
	}
	spi_m25p40_waiting();

	/* FIXED by tanya */
	spi_m25p40_disable();

	return E_OK;
}

unsigned int spi_m25p40_bulk_erase(void)
{
	unsigned int	rc;
	_SPI_DBG(" ");
	rc = spi_m25p40_write_enable();
	if(rc != E_OK) {
		printk(KERN_ERR "m25p40 bulk erase failed in enable write\n");
		return E_ERR;
	}
 	spi_m25p40_enable();
	rc = spi_m25p40_cmd( M25P40_BULK_ERASE_CMD, NO_ADDRESS_CYCLE, 0);
	if( rc != E_OK) {
		printk(KERN_ERR "m25p40 bulk erase failed in tx cmd\n");
		spi_m25p40_disable();
		return E_ERR;
	}

	spi_m25p40_waiting();
 	
	spi_m25p40_disable();

	return E_OK;
}

/*******************************************************************
 * Globle Functions
**********************************************************************/

/*****************************************************************
 * Linux Standard Driver API
 ****************************************************************/
static int spi_m25p40_open (struct inode *inode, struct file *file)
{
	if(spi_m25p40_open_flag == 1){
		printk(KERN_ERR  "open %s failed\n", driver_name);
		return -EBUSY;
	}
	spi_m25p40_open_flag = 1 ;

	return E_OK;
}
static int spi_m25p40_close (struct inode *inode, struct file *file)
{
	if ( spi_m25p40_open_flag == 1)
		spi_m25p40_open_flag = 0;

	return E_OK;
}
static int spi_m25p40_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
	int rc = 0;

	/* check the command validatation */
	if ((_IOC_TYPE(cmd) != SPI_M25P40_MAGIC)
	    || (_IOC_NR(cmd) > SPI_M25P40_MAX_IOCTL))
	 	return -ENOTTY;
	if (_IOC_DIR(cmd) & _IOC_READ)
		rc = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
	else if (_IOC_DIR(cmd) & _IOC_WRITE)
		rc =  !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));

	if (rc) return -EFAULT;
	
	if (cmd == SPI_M25P40_BULK_ERASE_IOCTL)	{
		rc = spi_m25p40_bulk_erase();
		return rc; /* E_OK or E_ERR */
	}
	
	if (cmd == SPI_M25P40_READ_IOCTL) {
    		struct spi_m25p40_transfer r_transfer;
		unsigned int r_addr, r_size, r_mode;
		unsigned char *r_data;
		
		_SPI_DBG("IOCTL READ");
		
		if (copy_from_user(&r_transfer,
				  (struct spi_m25p40_transfer __user *)arg,
				   sizeof(struct spi_m25p40_transfer)))
			return -EFAULT;

		r_addr = r_transfer.addr;
		r_size = r_transfer.size;
		r_mode = r_transfer.mode;
		r_data = r_transfer.data;

		if (r_data == NULL) return -EFAULT;
		if ( (r_mode != SPI_M25P40_READ_MODE) &&
			 (r_mode != SPI_M25P40_FAST_READ_MODE))
			return -EFAULT;
		_SPI_DBG("addr 0x%x data 0x%p size 0x%x mode 0x%x",r_addr, r_data, r_size, r_mode);
		rc = spi_m25p40_read(r_addr, r_data, r_size, r_mode);
		copy_to_user((void __user *)arg, &r_transfer, sizeof(r_transfer));
		return rc; /* successfully rx bytes */
	}

	if (cmd == SPI_M25P40_WRITE_IOCTL) {
    		struct spi_m25p40_transfer w_transfer;
		unsigned int w_addr, w_size, w_mode;
		unsigned char *w_data;

		_SPI_DBG("IOCTL WRITE");
		if (copy_from_user(&w_transfer,
				  (struct spi_m25p40_transfer __user *)arg,
				   sizeof(struct spi_m25p40_transfer)))
			return -EFAULT;

		w_addr = w_transfer.addr;
		w_size = w_transfer.size;
		w_mode = w_transfer.mode;
		w_data = w_transfer.data;
		if (( w_mode != SPI_M25P40_WRITE_MODE ) || (w_data == NULL))
			return -EFAULT;
			
		_SPI_DBG("addr 0x%x data 0x%p size 0x%x",w_addr, w_data, w_size);
		rc = spi_m25p40_write(w_addr, w_data, w_size);
		_SPI_DBG("write return 0x%x", rc);
		return rc; /* total tx successfully num */
	}
	
	printk(KERN_CRIT "DEMO: set command WRONG \n");
	return 0;
}

static int __init spi_m25p40_init(void)
{
	printk(KERN_ERR "%s version %s init\n", driver_name, driver_ver);

	 m25p40_major = register_chrdev(0, driver_name, &spi_m25p40_fops);
	if ( m25p40_major < 0 ) {
		printk(KERN_ERR "%s init failed \n", driver_name);
		return E_ERR;
	}
	printk("register char dev with major %d\n", m25p40_major);
	return 0;
}

static void __exit spi_m25p40_exit (void)
{
	unregister_chrdev(m25p40_major, driver_name);
}

module_init(spi_m25p40_init);
module_exit(spi_m25p40_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("SPI M25P40 FLASH DRIVER");
MODULE_AUTHOR("Tanya Jiang");

