/*
 * arch/ppc/platforms/bmw.c
 *
 * (c) 2006 LVL7 Systems. Based on sandpint.c, (c) 2000-2003 MontaVista
 * Software, Inc. Original author Mark A. Greer, mgreer@mvista.com. 
 * This file is licensed under the terms of the GNU General Public License 
 * version 2.  This program is licensed "as is" without any warranty of any 
 * kind, whether express or implied.
 */



#include <linux/slab.h>
#include <linux/pci.h>
#include <linux/autoconf.h>
#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/reboot.h>
#include <linux/initrd.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/irq.h>
#include <linux/seq_file.h>
#include <linux/root_dev.h>
#include <linux/serial.h>
#include <linux/tty.h>	/* for linux/serial_core.h */
#include <linux/serial_core.h>

#include <asm/system.h>
#include <asm/pgtable.h>
#include <asm/page.h>
#include <asm/time.h>
#include <asm/dma.h>
#include <asm/io.h>
#include <asm/machdep.h>
#include <asm/prom.h>
#include <asm/smp.h>
/*#include <asm/vga.h>*/
#include <asm/open_pic.h>
/*#include <asm/i8259.h>*/
#include <asm/todc.h>
#include <asm/bootinfo.h>
#include <asm/mpc10x.h>
#include <asm/pci-bridge.h>
#include <asm/byteorder.h>
#include <asm/uaccess.h>

#include "bmw.h"

unsigned char __res[sizeof(bd_t)];

static u_char bmw_openpic_initsenses[] __initdata = {
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQs 0-15*/
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* I2C */
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* DMA 0-1 */
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* MSG */
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* UART 1-2 */
  (IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE)       
};


/*
 * Broadcom BMW interrupt routes.
 */
static inline int
bmw_map_irq(struct pci_dev *dev, unsigned char idsel, unsigned char pin)
{
        static char pci_irq_table[][4] =
        /*
         *      PCI IDSEL/INTPIN->INTLINE
         *         A   B   C   D
         */
        {
                { 1, 1, 1, 1 },     /* IDSEL 13/d  - BCM5703 */
                { 2, 2, 2, 2 },     /* IDSEL 14/e  - unused */
                { 2, 2, 2, 2 },     /* IDSEL 15/f  - unused */
                { 2, 2, 2, 2 },     /* IDSEL 16/10 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 17/11 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 18/12 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 19/13 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 20/14 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 21/15 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 22/16 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 23/17 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 24/18 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 25/19 - unused */
                { 2, 2, 2, 2 },     /* IDSEL 26/1a - unused */
                { 2, 2, 2, 2 },     /* IDSEL 27/1b - unused */
                { 2, 2, 2, 2 },     /* IDSEL 28/1c - unused */
                { 2, 2, 2, 2 },     /* IDSEL 29/1d - unused */
                { 2, 2, 2, 2 },     /* IDSEL 30/1e - CompactPCIA */
                { 2, 2, 2, 2 },     /* IDSEL 31/1f - unused */
        };

        const long min_idsel = 13, max_idsel = 31, irqs_per_slot = 1;
        return PCI_IRQ_TABLE_LOOKUP;
}

void __init
bmw_find_bridges(void)
{
	struct pci_controller	*hose;

	hose = pcibios_alloc_controller();

	if (!hose)
		return;

	hose->first_busno = 0;
	hose->last_busno = 0xff;

	if (mpc10x_bridge_init(hose,
			       MPC10X_MEM_MAP_B,
			       MPC10X_MEM_MAP_B,
			       BMW_EUMB_BASE) == 0) {

		hose->mem_resources[0].end = 0xffffffff;
	
		/* scan PCI bus */
		ppc_md.pci_exclude_device = NULL;
		hose->last_busno = pciauto_bus_scan(hose, hose->first_busno);

		ppc_md.pcibios_fixup = NULL;
		ppc_md.pcibios_fixup_bus = NULL;
		ppc_md.pci_swizzle = common_swizzle;
		ppc_md.pci_map_irq = bmw_map_irq;
	}
	else {
		if (ppc_md.progress)
			ppc_md.progress("Bridge init failed", 0x100);
		printk("Host bridge init failed\n");
	}

	return;
}

static int
bmw_show_cpuinfo(struct seq_file *m)
{
	bd_t *bp = (bd_t *)__res;

	seq_printf(m, "vendor\t\t: Broadcom\n");
	seq_printf(m, "machine\t\t: BMW CPU card\n");
        seq_printf(m, "cpu speed\t: %ldMhz\n",
		   bp->bi_intfreq/1000000);
        seq_printf(m, "bus speed\t: %ldMhz\n",
		   bp->bi_busfreq/1000000);
        seq_printf(m, "System RAM\t: %ldMB\n",
		   bp->bi_memsize/1048576);
        seq_printf(m, "System FLASH\t: %ldMB\n",
		   bp->bi_flashsize/1048576);
        seq_printf(m, "Static RAM\t: %ld bytes\n", bp->bi_sramsize);
	
	return 0;
}

static void __init
bmw_init_IRQ(void)
{
	OpenPIC_InitSenses = bmw_openpic_initsenses;
	OpenPIC_NumInitSenses = sizeof(bmw_openpic_initsenses);

        /* Map serial interrupts 0-15 */
        openpic_set_sources(0, 16, OpenPIC_Addr + 0x10200);
        /* Skip reserved space and map i2c and DMA Ch[01] */
        openpic_set_sources(16, 3, OpenPIC_Addr + 0x11020);
        /* Skip reserved space and map Message Unit Interrupt (I2O) */
        openpic_set_sources(19, 1, OpenPIC_Addr + 0x110C0);
        /* Skip reserved space and map DUART */
        openpic_set_sources(20, 2, OpenPIC_Addr + 0x11120);

        openpic_init(0);
}

static void __init
bmw_setup_arch(void)
{
	loops_per_jiffy = 500000000 / HZ;

#ifdef CONFIG_BLK_DEV_INITRD
	if (initrd_start)
		ROOT_DEV = Root_RAM0;
	else
#endif
#ifdef	CONFIG_ROOT_NFS
		ROOT_DEV = Root_NFS;
#else
		ROOT_DEV = Root_RAM0;
#endif

	printk(KERN_INFO "Broadcom BMW CPU card\n");
	printk(KERN_INFO "Port by LVL7 Systems, Inc.\n");
	/* Lookup PCI host bridges */
	bmw_find_bridges();
}

static void
bmw_restart(char *cmd)
{
        volatile unsigned char *reset_addr = 
          (volatile unsigned char *)BMW_CPLD_RESET_ADDR;
	local_irq_disable();

	/* Reset system via CPLD */
	*reset_addr = (BMW_CPLD_RESET_ALL & 0xff);
	*reset_addr = 0;
	for(;;);	/* Spin until reset happens */
        panic("Reset failed - reached code after end of infinite loop.\n");
}

static void
bmw_power_off(void)
{
	local_irq_disable();
	for(;;);	/* No way to shut power off with software */
        panic("Poweroff failed - reached code after end of infinite loop.\n");
}

static void
bmw_halt(void)
{
	bmw_power_off();
	/* NOTREACHED */
}

static void __init
bmw_map_io(void)
{
        io_block_mapping(0xfc000000, 0xfc000000, 0x00100000, _PAGE_IO);
        io_block_mapping(0xff000000, 0xff000000, 0x01000000, _PAGE_IO);
        io_block_mapping(BMW_CPLD_VBASE, BMW_CPLD_PBASE, BMW_NV_CPLD_SIZE, _PAGE_IO);
}

static unsigned long __init
bmw_find_end_of_memory(void)
{
	bd_t *bp = (bd_t *)__res;

	if (bp->bi_memsize)
		return bp->bi_memsize;
	printk(KERN_ERR "*** Could not determine memory size, assuming 64 MB!\n\n");
	return 64*1024*1024;
}

static __inline__ void
bmw_set_bat(void)
{
        static int recursion_guard = 0;
	unsigned long bat3u, bat3l;

	if (!recursion_guard) {
	  __asm__ __volatile__(
			" lis %0,0xf000\n	\
			ori %1,%0,0x002a\n	\
			ori %0,%0,0x0ffe\n	\
			mtspr 0x21e,%0\n	\
			mtspr 0x21f,%1\n	\
			isync\n			\
			sync "
			: "=r" (bat3u), "=r" (bat3l));
	  recursion_guard = 1;
	}
}

#ifdef  CONFIG_SERIAL_TEXT_DEBUG
#include <linux/serialP.h>
#include <linux/serial_reg.h>
#include <asm/serial.h>

static struct serial_state rs_table[RS_TABLE_SIZE] = {
  SERIAL_PORT_DFNS        /* Defined in <asm/serial.h> */
};

void
bmw_progress(char *s, unsigned short hex)
{
  volatile char c;
  volatile unsigned long com_port;
  u16 shift;

  com_port = rs_table[0].port;
  shift = rs_table[0].iomem_reg_shift;

  while ((c = *s++) != 0) {
    while ((*((volatile unsigned char *)com_port +
	      (UART_LSR << shift)) & UART_LSR_THRE) == 0)
      ;
    *(volatile unsigned char *)com_port = c;

    if (c == '\n') {
      while ((*((volatile unsigned char *)com_port +
		(UART_LSR << shift)) & UART_LSR_THRE) == 0)
	;
      *(volatile unsigned char *)com_port = '\r';
    }
  }
  s = "\n\r";
  while ((c = *s++) != 0) {
    while ((*((volatile unsigned char *)com_port +
	      (UART_LSR << shift)) & UART_LSR_THRE) == 0)
      ;
    *(volatile unsigned char *)com_port = c;

    if (c == '\n') {
      while ((*((volatile unsigned char *)com_port +
		(UART_LSR << shift)) & UART_LSR_THRE) == 0)
	;
      *(volatile unsigned char *)com_port = '\r';
    }
  }
}
#endif  /* CONFIG_SERIAL_TEXT_DEBUG */

static void __init
bmw_calibrate_decr(void)
{
  bd_t    *binfo = (bd_t *)__res;
  int freq;

  /* determine processor bus speed */
  freq = (binfo->bi_busfreq / 4);
  tb_ticks_per_jiffy = freq / HZ;
  tb_to_us = mulhwu_scale_factor(freq, 1000000);
}

void bmw_get_mac(unsigned char *ethaddr)
{
        bd_t *binfo = (bd_t *)__res;

        memcpy((void *)ethaddr, (void *)binfo->bi_enetaddr,
               sizeof(binfo->bi_enetaddr));
}

TODC_ALLOC();

void __init
platform_init(unsigned long r3, unsigned long r4, unsigned long r5,
		unsigned long r6, unsigned long r7)
{
        bd_t *binfo = (bd_t *)__res;

	parse_bootinfo(find_bootinfo());

	/* ASSUMPTION:  If both r3 (bd_t pointer) and r6 (cmdline pointer)
	 * are non-zero, then we should use the board info from the bd_t
	 * structure and the cmdline pointed to by r6 instead of the
	 * information from birecs, if any.  Otherwise, use the information
	 * from birecs as discovered by the preceeding call to
	 * parse_bootinfo().  This rule should work with both PPCBoot, which
	 * uses a bd_t board info structure, and the kernel boot wrapper,
	 * which uses birecs.
	 */
	if (r3 && r6) {
		/* copy board info structure */
		memcpy( (void *)__res,(void *)(r3+KERNELBASE), sizeof(bd_t) );
		/* copy command line */
		*(char *)(r7+KERNELBASE) = 0;
		strcpy(cmd_line, (char *)(r6+KERNELBASE));
	}

	/* nothing but serial consoles... */
	if (!strstr((char *)(r6+KERNELBASE), "console="))
	  sprintf((cmd_line+strlen(cmd_line)),
		  " console=ttyS0,%ld", binfo->bi_baudrate);

#ifdef CONFIG_BLK_DEV_INITRD
	/* take care of initrd if we have one */
	if (r4) {
		initrd_start = r4 + KERNELBASE;
		initrd_end = r5 + KERNELBASE;
	}
#endif /* CONFIG_BLK_DEV_INITRD */

	/* Map in board regs, etc. */
	bmw_set_bat();

	isa_io_base = MPC10X_MAPB_ISA_IO_BASE;
	isa_mem_base = MPC10X_MAPB_ISA_MEM_BASE;
	pci_dram_offset = MPC10X_MAPB_DRAM_OFFSET;
	ISA_DMA_THRESHOLD = 0x00ffffff;
	DMA_MODE_READ = 0x44;
	DMA_MODE_WRITE = 0x48;

	ppc_md.setup_arch = bmw_setup_arch;
	ppc_md.show_cpuinfo = bmw_show_cpuinfo;
	ppc_md.irq_canonicalize = NULL;
	ppc_md.init_IRQ = bmw_init_IRQ;
	ppc_md.get_irq = openpic_get_irq;

	ppc_md.restart = bmw_restart;
	ppc_md.power_off = bmw_power_off;
	ppc_md.halt = bmw_halt;

	ppc_md.find_end_of_memory = bmw_find_end_of_memory;
	ppc_md.setup_io_mappings = bmw_map_io;

	TODC_INIT(TODC_TYPE_DS1743, 0, 0, BMW_NVRAM_VBASE, 0);
	ppc_md.time_init = todc_time_init;
	ppc_md.set_rtc_time = todc_set_rtc_time;
	ppc_md.get_rtc_time = todc_get_rtc_time;
	ppc_md.calibrate_decr = bmw_calibrate_decr;

	ppc_md.nvram_read_val = todc_direct_read_val;
	ppc_md.nvram_write_val = todc_direct_write_val;

#ifdef CONFIG_SERIAL_TEXT_DEBUG
	ppc_md.progress = bmw_progress;
#endif
}
