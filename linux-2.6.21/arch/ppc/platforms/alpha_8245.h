/* * arch/ppc/platforms/alpha_8245.h
 * 
 * Board declarations for Alpha 8245 XGS3 board.
 *
 * Author: John W. Linville
 *         linville@lvl7.com
 *         Neil Horman
 *         nhorman@lvl7.com
 *         Andy Gospodarek
 *         andyg@lvl7.com
 *
 * Copyright 2002 LVL7 Systems Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */
#ifndef	__PPC_PLATFORMS_ALPHA_8245_H_
#define	__PPC_PLATFORMS_ALPHA_8245_H_

/*
 * Use PPCBoot's bd_t definition...
 */
#include <asm/ppcboot.h>

#define ALPHA_8245_EUMB_BASE	0xFC000000

/*
 * CPLD register addresses and related definitions...
 */
#define ALPHA_8245_CPLD_PBASE           0xff6f0000
#define ALPHA_8245_CPLD_VBASE           0xfd000000

#define ALPHA_8245_CPLD_RESET_ADDR               ALPHA_8245_CPLD_VBASE+3
#define ALPHA_8245_CPLD_RESET_ENABLE   	         0x04

#define ALPHA_8245_CPLD_RESET_COUNT_ADDR         ALPHA_8245_CPLD_VBASE+2
#define ALPHA_8245_CPLD_RESET_COUNT              0x80



#define ALPHA_8245_NVRAM_PBASE          0x7c004000
#define ALPHA_8245_NVRAM_VBASE          0xfd004000
#define ALPHA_8245_NV_CPLD_SIZE         ((uint)32*1024)

void alpha_8245_find_bridges(void);

#endif	/* __PPC_PLATFORMS_ALPHA_8245_H_ */
