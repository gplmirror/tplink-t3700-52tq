/*
 * arch/ppc/platforms/alpha_8245_pci.c
 * 
 * PCI setup routines for Alpha 8245 XGS3 boards.
 *
 * Author: John W. Linville
 *         linville@lvl7.com
 *         Andy Gospodarek
 *         andyg@lvl7.com
 *
 * Copyright 2002 LVL7 Systems Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/slab.h>

#include <asm/byteorder.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/machdep.h>
#include <asm/mpc10x.h>
#include <asm/pci-bridge.h>

#include "alpha_8245.h"

/*
 * Broadcom ALPHA_8245 interrupt routes.
 */
static inline int
alpha_8245_map_irq(struct pci_dev *dev, unsigned char idsel, unsigned char pin)
{
  /* This board has only 1 PCI device, assign IRQ 0 to it. */
  int irq = 0;
  if (idsel == 0x10)
  {
    irq =  1;
  }
  return irq;
}

void __init
alpha_8245_find_bridges(void)
{
	struct pci_controller	*hose;

	hose = pcibios_alloc_controller();

	if (!hose)
		return;

	hose->first_busno = 0;
	hose->last_busno = 0xff;

	if (mpc10x_bridge_init(hose,
			       MPC10X_MEM_MAP_B,
			       MPC10X_MEM_MAP_B,
			       ALPHA_8245_EUMB_BASE) == 0) {

		hose->mem_resources[0].end = 0xffffffff;
	
		/* scan PCI bus */
		ppc_md.pci_exclude_device = NULL;
		hose->last_busno = pciauto_bus_scan(hose, hose->first_busno);

		ppc_md.pcibios_fixup = NULL;
		ppc_md.pcibios_fixup_bus = NULL;
		ppc_md.pci_swizzle = common_swizzle;
		ppc_md.pci_map_irq = alpha_8245_map_irq;
	}
	else {
		if (ppc_md.progress)
			ppc_md.progress("Bridge init failed", 0x100);
		printk("Host bridge init failed\n");
	}

	return;
}
