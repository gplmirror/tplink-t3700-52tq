/*
 * arch/ppc/platforms/alpha_8245_setup.c
 *
 * Board setup routines for Alpha 8245 XGS3 board.
 *
 * Author: Neil Horman
 *         nhorman@lvl7.com
 *         Andy Gospodarek 
 *         andyg@lvl7.com
 *
 * Copyright 2003 LVL7 Systems Inc.
 *
 * Modified from setup for Znyx ZX4500 family of cPCI boards.
 * Copyright 2000, 2001 MontaVista Software Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/autoconf.h>
#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/reboot.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/types.h>
#include <linux/major.h>
/*#include <linux/blk.h>*/
#include <linux/initrd.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/irq.h>
#include <linux/seq_file.h>
#include <linux/root_dev.h>

#include <asm/system.h>
#include <asm/pgtable.h>
#include <asm/page.h>
#include <asm/dma.h>
#include <asm/io.h>
#include <asm/machdep.h>
#include <asm/prom.h>
#include <asm/time.h>
#include <asm/todc.h>
#include <asm/open_pic.h>
#include <asm/mpc10x.h>
#include <asm/pci-bridge.h>
#include <asm/bootinfo.h>

#include "alpha_8245.h"

unsigned char __res[sizeof(bd_t)];

static u_char alpha_8245_openpic_initsenses[] __initdata = {
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 0 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 1 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 2 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 3 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 4 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 5 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 6 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 7 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 8 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 9 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 10 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 11 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 12 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 13 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 14 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* IRQ 15 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* I2C */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* DMA 0 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* DMA 1 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* MSG Unit */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE),      /* DUART 1 */
	(IRQ_SENSE_LEVEL | IRQ_POLARITY_NEGATIVE)       /* DUART 2 */
};


static void __init
alpha_8245_setup_arch(void)
{
	loops_per_jiffy = 500000000 / HZ;

#ifdef CONFIG_BLK_DEV_INITRD
	if (initrd_start)
		ROOT_DEV = MKDEV(RAMDISK_MAJOR, 0);
#endif
#if defined(CONFIG_BLK_DEV_INITRD) && defined(CONFIG_ROOT_NFS)
	else
#endif
#ifdef CONFIG_ROOT_NFS
		ROOT_DEV = to_kdev_t(0x00FF);   /* /dev/nfs pseudo device */
#endif

	/* nothing but serial consoles... */
	printk("Alpha_8245 Fast Ethernet Routing Switch\n");
	printk("Alpha_8245 port (C) 2005 LVL7 Systems, Inc. (andyg@lvl7.com)\n");

	/* Lookup PCI host bridge */
	alpha_8245_find_bridges();

	return;
}

static ulong __init
alpha_8245_find_end_of_memory(void)
{
	bd_t *binfo;
	extern unsigned char __res[];

	binfo = (bd_t *)__res;

	return binfo->bi_memsize;
}

static void __init
alpha_8245_map_io(void)
{
	io_block_mapping(0xfc000000, 0xfc000000, 0x00100000, _PAGE_IO);
	io_block_mapping(0xff000000, 0xff000000, 0x01000000, _PAGE_IO); 
	io_block_mapping(ALPHA_8245_CPLD_VBASE, ALPHA_8245_CPLD_PBASE, ALPHA_8245_NV_CPLD_SIZE, _PAGE_IO); 
}


static void __init
alpha_8245_init_IRQ(void)
{
	OpenPIC_InitSenses = alpha_8245_openpic_initsenses;
	OpenPIC_NumInitSenses = sizeof(alpha_8245_openpic_initsenses);

	/*
	 * We need to tell openpic_set_sources where things actually are.
	 * mpc10x_common will setup OpenPIC_Addr at ioremap(EUMB phys base +
	 * EPIC offset (0x40000));  The EPIC IRQ Register Address Map -
	 * Interrupt Source Configuration Registers gives these numbers
	 * as offsets starting at 0x50200, we need to adjust occordinly.
	 */
	/* Map serial interrupts 0-15 */
#if 1 
	openpic_set_sources(0, 16, OpenPIC_Addr + 0x10200);
	/* Skip reserved space and map i2c and DMA Ch[01] */
	openpic_set_sources(16, 3, OpenPIC_Addr + 0x11020);
	/* Skip reserved space and map Message Unit Interrupt (I2O) */
	openpic_set_sources(19, 1, OpenPIC_Addr + 0x110C0);
	/* Skip reserved space and map DUART */
	openpic_set_sources(20, 2, OpenPIC_Addr + 0x11120);
#else
	openpic_set_sources(0,  15, OpenPIC_Addr + 0x10200); 
	openpic_set_sources(20, 2, OpenPIC_Addr + 0x11120);
#endif 
	openpic_init(0);

	return;
}

static void
alpha_8245_restart(char *cmd)
{
	local_irq_disable();

	out_8((volatile u_char *)ALPHA_8245_CPLD_RESET_ADDR, ALPHA_8245_CPLD_RESET_ENABLE);
	out_8((volatile u_char *)ALPHA_8245_CPLD_RESET_ADDR, 0);

	out_8((volatile u_char *)ALPHA_8245_CPLD_RESET_COUNT_ADDR, ALPHA_8245_CPLD_RESET_COUNT);
	out_8((volatile u_char *)ALPHA_8245_CPLD_RESET_COUNT_ADDR, 0);

	for (;;);

	panic("Restart failed.\n");
	/* NOTREACHED */
}

static void
alpha_8245_power_off(void)
{
	local_irq_disable();
	for(;;);  /* No way to shut power off with software */
	/* NOTREACHED */
}

static void
alpha_8245_halt(void)
{
	alpha_8245_power_off();
	/* NOTREACHED */
}

static int
alpha_8245_show_cpuinfo(struct seq_file *m)
{
	bd_t *bp;

	bp = (bd_t *)__res;

	seq_printf(m, "vendor\t\t: Broadcom\n");
	seq_printf(m, "machine\t\t: ALPHA_8245\n");
	seq_printf(m, "cpu speed\t: %ldMhz\n",
			bp->bi_intfreq/1000000);
	seq_printf(m, "bus speed\t: %ldMhz\n",
			bp->bi_busfreq/1000000);

	return 0;
}

static void __init
alpha_8245_calibrate_decr(void)
{
	bd_t    *binfo = (bd_t *)__res;
	ulong freq;

	freq = (binfo->bi_busfreq / 4);

	printk("time_init: decrementer frequency = %lu.%.6lu MHz\n",
	       freq/1000000, freq%1000000);

	tb_ticks_per_jiffy = freq / HZ;
	tb_to_us = mulhwu_scale_factor(freq, 1000000);
#ifdef CONFIG_ILATENCY
	{
		extern unsigned ticks_per_usec;
		ticks_per_usec = freq / 1000000;
	}
#endif

	return;
}

void alpha_8245_get_mac(unsigned char *ethaddr)
{
        bd_t *binfo = (bd_t *)__res;

        memcpy((void *)ethaddr, (void *)binfo->bi_enetaddr,
               sizeof(binfo->bi_enetaddr));
}


/*
 * Set BAT 3 to map 0xfc000000 to end of physical memory space 1-to-1.
 */
static __inline__ void
alpha_8245_set_bat(void)
{
	unsigned long   bat3u, bat3l; 
	static int	mapping_set = 0;

	if (!mapping_set) {
		__asm__ __volatile__(
		" lis %0,0xfc00\n \
		  ori %1,%0,0x002a\n \
		  ori %0,%0,0x07fe\n \
		  mtspr 0x21e,%0\n \
		  mtspr 0x21f,%1\n \
		  isync\n \
		  sync "
		  : "=r" (bat3u), "=r" (bat3l));

		mapping_set = 1;
	}

	return;
}

#ifdef	CONFIG_SERIAL_TEXT_DEBUG
#include <linux/serialP.h>
#include <linux/serial_reg.h>
#include <asm/serial.h>

static struct serial_state rs_table[RS_TABLE_SIZE] = {
	SERIAL_PORT_DFNS	/* Defined in <asm/serial.h> */
};

void
alpha_8245_progress(char *s, unsigned short hex)
{
	volatile char c;
	volatile unsigned long com_port;
	u16 shift;

	com_port = rs_table[0].port;
	shift = rs_table[0].iomem_reg_shift;

	while ((c = *s++) != 0) {
		while ((*((volatile unsigned char *)com_port +
				(UART_LSR << shift)) & UART_LSR_THRE) == 0)
		                ;
	        *(volatile unsigned char *)com_port = c;

		if (c == '\n') {
			while ((*((volatile unsigned char *)com_port +
				(UART_LSR << shift)) & UART_LSR_THRE) == 0)
					;
	        	*(volatile unsigned char *)com_port = '\r';
		}
	}
	s = "\n\r";
	while ((c = *s++) != 0) {
		while ((*((volatile unsigned char *)com_port +
				(UART_LSR << shift)) & UART_LSR_THRE) == 0)
		                ;
	        *(volatile unsigned char *)com_port = c;

		if (c == '\n') {
			while ((*((volatile unsigned char *)com_port +
				(UART_LSR << shift)) & UART_LSR_THRE) == 0)
					;
	        	*(volatile unsigned char *)com_port = '\r';
		}
	}
}
#endif	/* CONFIG_SERIAL_TEXT_DEBUG */

TODC_ALLOC();

void __init
platform_init(unsigned long r3, unsigned long r4, unsigned long r5,
	      unsigned long r6, unsigned long r7)
{
	bd_t *binfo;

	parse_bootinfo(find_bootinfo());

	if ( r3 )
		memcpy( (void *)__res,(void *)(r3+KERNELBASE), sizeof(bd_t) );

	binfo = (bd_t *)__res;

#ifdef CONFIG_BLK_DEV_INITRD
	/* take care of initrd if we have one */
	if ( r4 )
	{
		initrd_start = r4 + KERNELBASE;
		initrd_end = r5 + KERNELBASE;
	}
#endif /* CONFIG_BLK_DEV_INITRD */

	/* take care of cmd line */
	if ( r6 )
	{
		*(char *)(r7+KERNELBASE) = 0;

		sprintf(cmd_line, "%s", (char *)(r6+KERNELBASE));

		/* nothing but serial consoles... */
		if (!strstr((char *)(r6+KERNELBASE), "console="))
			sprintf((cmd_line+strlen(cmd_line)),
			        " console=ttyS0,%ld", binfo->bi_baudrate);
	}

	/* Map in board regs, etc. */
	alpha_8245_set_bat();

	isa_io_base = MPC10X_MAPB_ISA_IO_BASE;
	isa_mem_base = MPC10X_MAPB_ISA_MEM_BASE;
	pci_dram_offset = MPC10X_MAPB_DRAM_OFFSET;

	ppc_md.setup_arch = alpha_8245_setup_arch;
	ppc_md.show_cpuinfo = alpha_8245_show_cpuinfo;
	ppc_md.irq_canonicalize = NULL;
	ppc_md.init_IRQ = alpha_8245_init_IRQ;
	ppc_md.get_irq = openpic_get_irq;
	ppc_md.init = NULL;

	ppc_md.restart = alpha_8245_restart;
	ppc_md.power_off = alpha_8245_power_off;
	ppc_md.halt = alpha_8245_halt;

	ppc_md.find_end_of_memory = alpha_8245_find_end_of_memory;
	ppc_md.setup_io_mappings = alpha_8245_map_io;

	ppc_md.calibrate_decr = alpha_8245_calibrate_decr;

#if 0 
	TODC_INIT(TODC_TYPE_DS1743, 0, 0, ALPHA_8245_NVRAM_VBASE, 0);
	ppc_md.time_init = todc_time_init;
	ppc_md.set_rtc_time = todc_set_rtc_time;
	ppc_md.get_rtc_time = todc_get_rtc_time;

	ppc_md.nvram_read_val = todc_direct_read_val;
	ppc_md.nvram_write_val = todc_direct_write_val;
#else
	ppc_md.time_init = NULL;
	ppc_md.set_rtc_time = NULL;
	ppc_md.get_rtc_time = NULL;

	ppc_md.nvram_read_val = NULL;
	ppc_md.nvram_write_val = NULL;

#endif 

#ifdef	CONFIG_SERIAL_TEXT_DEBUG
	ppc_md.progress = alpha_8245_progress;
#else	/* !CONFIG_SERIAL_TEXT_DEBUG */
	ppc_md.progress = NULL;
#endif	/* CONFIG_SERIAL_TEXT_DEBUG */

	return;
}
