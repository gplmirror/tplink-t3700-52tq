/*
 * arch/ppc/platforms/85xx/mpc8540_ads.h
 *
 * MPC8540ADS board definitions
 *
 * Maintainer: Kumar Gala <kumar.gala@freescale.com>
 *
 * Copyright 2004 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 */

#ifndef __MACH_MPC8540ADS_H__
#define __MACH_MPC8540ADS_H__

#include <linux/config.h>
#include <linux/initrd.h>
#include <syslib/ppc85xx_setup.h>
#include <platforms/85xx/mpc85xx_ads_common.h>

#define SYSTEM_CPLD_PBASE             0xD0000000
#define SYSTEM_CPLD_VBASE             0xFD000000
#define SYSTEM_CPLD_SIZE              0x00005000

#define MODULE_CPLD_PBASE             0xE0000000
#define MODULE_CPLD_VBASE             0xFD010000
#define MODULE_CPLD_SIZE              0x00008000

#define SYSTEM_RESET_ADDR           (SYSTEM_CPLD_VBASE + 0x3000)
#define SYSTEM_RESET_CMD            0x1f

#endif /* __MACH_MPC8540ADS_H__ */
