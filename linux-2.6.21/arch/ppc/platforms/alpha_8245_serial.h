/*
 * arch/ppc/platforms/alpha_8245_serial.h
 * 
 * Definitions for Alpha 8245 XGS3 Platform
 *
 * Author: John W. Linville
 *         linville@lvl7.com
 *         Andy Gospodarek
 *         andyg@lvl7.com
 *
 * Copyright 2002 LVL7 Systems Inc.
 *
 * Modified from Motorola SPS Sandpoint Test Platform
 * Copyright 2001 MontaVista Software Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#ifndef __ASMPPC_ALPHA_8245_SERIAL_H
#define __ASMPPC_ALPHA_8245_SERIAL_H

#include <linux/autoconf.h>

#define ALPHA_8245_SERIAL_0		0xfc004500
#define ALPHA_8245_SERIAL_1		0xfc004600

#ifdef CONFIG_SERIAL_MANY_PORTS
#define RS_TABLE_SIZE  64
#else
#define RS_TABLE_SIZE  2
#endif

/* Rate for the 133 Mhz (SDRAM) clock for the onboard serial chip */

#define BASE_BAUD (133333333 / 16)

#ifdef CONFIG_SERIAL_DETECT_IRQ
#define STD_COM_FLAGS (ASYNC_BOOT_AUTOCONF|ASYNC_SKIP_TEST|ASYNC_AUTO_IRQ)
#else
#define STD_COM_FLAGS (ASYNC_BOOT_AUTOCONF|ASYNC_SKIP_TEST)
#endif

#define STD_SERIAL_PORT_DFNS \
        { 0, BASE_BAUD, ALPHA_8245_SERIAL_0, 20, STD_COM_FLAGS, /* ttyS0 */ \
		iomem_base: (u8 *)ALPHA_8245_SERIAL_0,			  \
		io_type: SERIAL_IO_MEM },				  \
        { 0, BASE_BAUD, ALPHA_8245_SERIAL_1, 21, STD_COM_FLAGS, /* ttyS1 */ \
		iomem_base: (u8 *)ALPHA_8245_SERIAL_1,			  \
		io_type: SERIAL_IO_MEM },

#define SERIAL_PORT_DFNS \
        STD_SERIAL_PORT_DFNS

#endif /* __ASMPPC_ALPHA_8245_SERIAL_H */
