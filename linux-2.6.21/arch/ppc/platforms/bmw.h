/* * arch/ppc/platforms/bmw.h
 * 
 * Board declarations for Broadcom BMW board.
 *
 * Author: John W. Linville
 *         linville@lvl7.com
 *         Neil Horman
 *         nhorman@lvl7.com
 *
 * Copyright 2002 LVL7 Systems Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */
#ifndef	__PPC_PLATFORMS_BMW_H_
#define	__PPC_PLATFORMS_BMW_H_

/*
 * Use PPCBoot's bd_t definition...
 */
#include <asm/ppcboot.h>

#define BMW_EUMB_BASE	0xFC000000

/*
 * CPLD register addresses and related definitions...
 */
#define BMW_CPLD_PBASE           0x7c000000
#define BMW_CPLD_VBASE           0xfd000000

#define BMW_CPLD_RESET_ADDR	 BMW_CPLD_VBASE
#define BMW_CPLD_RESET_ALL   	 0xffffffff	

#define BMW_NVRAM_PBASE          0x7c004000
#define BMW_NVRAM_VBASE          0xfd004000
#define BMW_NV_CPLD_SIZE         ((uint)32*1024)

/*
 * Serial port definitions...
 */
#define BMW_SERIAL_0		0xfc004500
#define BMW_SERIAL_1		0xfc004600

#ifdef CONFIG_SERIAL_MANY_PORTS
#define RS_TABLE_SIZE  64
#else
#define RS_TABLE_SIZE  2
#endif

/* Rate for the 100 Mhz (SDRAM) clock for the onboard serial chip */
#define BASE_BAUD ( 100000000 / 16 )

#ifdef CONFIG_SERIAL_DETECT_IRQ
#define STD_COM_FLAGS (ASYNC_BOOT_AUTOCONF|ASYNC_SKIP_TEST|ASYNC_AUTO_IRQ)
#else
#define STD_COM_FLAGS (ASYNC_BOOT_AUTOCONF|ASYNC_SKIP_TEST)
#endif

#define STD_SERIAL_PORT_DFNS \
        { 0, BASE_BAUD, BMW_SERIAL_0, 20, STD_COM_FLAGS, /* ttyS0 */ \
		iomem_base: (u8 *)BMW_SERIAL_0,			  \
		io_type: SERIAL_IO_MEM },				  \
        { 0, BASE_BAUD, BMW_SERIAL_1, 21, STD_COM_FLAGS, /* ttyS1 */ \
		iomem_base: (u8 *)BMW_SERIAL_1,			  \
		io_type: SERIAL_IO_MEM },

#define SERIAL_PORT_DFNS \
        STD_SERIAL_PORT_DFNS

void bmw_find_bridges(void);

#endif	/* __PPC_PLATFORMS_BMW_H_ */
