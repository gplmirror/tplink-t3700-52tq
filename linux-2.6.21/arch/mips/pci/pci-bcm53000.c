/*
 * Low-Level PCI and SB support for BCM53000 (Linux support code)
 *
 * Copyright (C) 2004 Broadcom Corporation
 * Copyright (C) 2005 MontaVista Inc. ( 2.6 kernel modification )
 * Copyright (C) 2007 Wind River Systems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/pci.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/paccess.h>

#include <asm/bcmsi/typedefs.h>
#include <asm/bcmsi/bcmutils.h>
#include <asm/bcmsi/hndsoc.h>
#include <asm/bcmsi/siutils.h>
#include <asm/bcmsi/hndpci.h>
#include <asm/bcmsi/pcicfg.h>
#include <asm/bcmsi/bcmdevs.h>
#include <asm/bcmsi/bcmnvram.h>

/* Convenience */
extern si_t *bcm53000_sih;
extern spinlock_t bcm53000_sih_lock;

#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock

static int
bcm53000_pci_read_config(struct pci_bus *bus, unsigned int devfn,
             int where, int size, u32 * value)
{
    unsigned long flags;
    int ret;
    u8 val8;
    u16 val16;
    u32 *pval;

    spin_lock_irqsave(&sih_lock, flags);

    if (size == 1) {
        pval = &val8;
    } else if (size == 2) {
        pval = &val16;
    } else {
        pval = value;
    } 
    ret =
        hndpci_read_config(sih, bus->number, PCI_SLOT(devfn),
                  PCI_FUNC(devfn), where, pval, size);

    if (size == 1) {
        *value = (u32)val8;
    } else if (size == 2) {
        *value = (u32)val16;
    } 
    spin_unlock_irqrestore(&sih_lock, flags);
    return ret ? PCIBIOS_DEVICE_NOT_FOUND : PCIBIOS_SUCCESSFUL;

}

static int
bcm53000_pci_write_config(struct pci_bus *bus, unsigned int devfn,
              int where, int size, u32 value)
{
    unsigned long flags;
    int ret;
    u8 val8;
    u16 val16;
    u32 *pval;

    spin_lock_irqsave(&sih_lock, flags);
    if (size == 1) {
        val8 = value;
        pval = &val8;
    } else if (size == 2) {
        val16 = value;
        pval = &val16;
    } else {
        pval = &value;
    } 
    ret =
        hndpci_write_config(sih, bus->number, PCI_SLOT(devfn),
                   PCI_FUNC(devfn), where, pval, size);
    spin_unlock_irqrestore(&sih_lock, flags);
    return ret ? PCIBIOS_DEVICE_NOT_FOUND : PCIBIOS_SUCCESSFUL;
}

static struct pci_ops pcibios_ops = {
    .read = bcm53000_pci_read_config,
    .write = bcm53000_pci_write_config
};

static struct resource bcm53000_mem_resource_port0 = {
    .name = "bcm53000 mem port 0",
    .start = SI_PCI0_MEM,
    .end = SI_PCI0_MEM + 0x04000000,
    .flags = IORESOURCE_MEM,
};

static struct resource bcm53000_io_resource_port0 = {
    .name = "bcm53000 io port 0",
    .start = 0,
    .end = 0,
    .flags = IORESOURCE_IO,
};

static struct pci_controller bcm53000_controller_port0 = {
    .pci_ops = &pcibios_ops,
    .mem_resource = &bcm53000_mem_resource_port0,
    .io_resource = &bcm53000_io_resource_port0,
};
static struct resource bcm53000_mem_resource_port1 = {
    .name = "bcm53000 mem port1",
    .start = SI_PCI1_MEM,
    .end = SI_PCI1_MEM + 0x04000000,
    .flags = IORESOURCE_MEM,
};

static struct resource bcm53000_io_resource_port1 = {
    .name = "bcm53000 io port 1",
    .start = 1,
    .end = 1,
    .flags = IORESOURCE_IO,
};
static int bcm53000_pcibios_get_busno(void)
{
	return PCIE_PORT1_BUS_START;
}
static struct pci_controller bcm53000_controller_port1 = {
    .pci_ops = &pcibios_ops,
    .mem_resource = &bcm53000_mem_resource_port1,
    .io_resource = &bcm53000_io_resource_port1,
    .get_busno =bcm53000_pcibios_get_busno,
};

static int __init bcm53000_pcibios_init(void)
{
    ulong flags;
    extern int pci_probe_only;
        
    pci_probe_only = 1;
    if (!(sih = si_kattach(SI_OSH)))
        panic("sb_kattach failed");
    spin_lock_init(&sih_lock);
    spin_lock_irqsave(&sih_lock, flags);

    hndpci_init(sih);
    spin_unlock_irqrestore(&sih_lock, flags);

    register_pci_controller(&bcm53000_controller_port0);

    register_pci_controller(&bcm53000_controller_port1);

    return 0;
}

arch_initcall(bcm53000_pcibios_init);
