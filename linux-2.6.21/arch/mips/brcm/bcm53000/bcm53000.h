/*
 * Broadcom HND MIPS boards configuration
 *
 * $Copyright Open Broadcom Corporation$
 *
 * $Id: bcm53000.h,v 1.1 2008/09/19 11:27:30 ako Exp $
 */

#ifndef _bcm53000_h_
#define _bcm53000_h_

/* Virtual IRQ base, after last hw IRQ */
#define SBMIPS_VIRTIRQ_BASE	6

/* # IRQs, hw and sw IRQs */
#define SBMIPS_NUMIRQS	8

#endif	/* _bcm53000_h_ */ 
