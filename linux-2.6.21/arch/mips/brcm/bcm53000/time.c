/*
 * Copyright (C) 2004 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/serial_reg.h>
#include <linux/interrupt.h>
#include <asm/addrspace.h>
#include <asm/io.h>
#include <asm/time.h>

#include <typedefs.h>
#include <osl.h>
#include <bcmnvram.h>
#include <sbconfig.h>
#include <sbchipc.h>
#include <siutils.h>
#include <hndmips.h>
#include <hndcpu.h>

#ifdef CONFIG_HWTIMER_HOOKS
#include <linux/hwtimer.h>

static int bcm53000_timer_get_freq(void)
{
	return HZ;
}

static struct hwtimer_data bcm53000_timer_data = {
	name: "bcm53000 timer",
	desc: "bcm53000 Kernel jiffy timer",
	def_freq: HZ,
	min_freq: HZ,
	max_freq: HZ
};

static DECLARE_HWTIMER_LOCK(bcm53000_timer_lock);

static struct hwtimer bcm53000_timer = {
	data: &bcm53000_timer_data,
	set_freq: NULL,
	get_freq: bcm53000_timer_get_freq,
	start: NULL,
	stop: NULL,
	lock: &bcm53000_timer_lock,
	hook: NULL,
	hook_data: NULL
};
#endif	/* CONFIG_HWTIMER_HOOKS */

/* Global SI handle */
extern si_t *bcm53000_sih;
extern spinlock_t bcm53000_sih_lock;

/* Convenience */
#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock
#define WATCHDOG_MIN	3000	/* milliseconds */
#define WATCHDOG_MAX	5592	/* milliseconds */


extern int panic_timeout;
static int watchdog = 0;


void __init bcm53000_time_init(void)
{
        unsigned int hz;

        /*
         * Use deterministic values for initial counter interrupt
         * so that calibrate delay avoids encountering a counter wrap.
         */
        write_c0_count(0);
        write_c0_compare(0xffff);

        if (!(hz = si_cpu_clock(sih)))
        	hz = 100000000;
        printk("CPU: BCM%04x rev %d at %d MHz\n", sih->chip, sih->chiprev,
           (hz + 500000) / 1000000);

	/* Set MIPS counter frequency for fixed_rate_gettimeoffset() */
	mips_hpt_frequency = hz / 2;

	/* Set watchdog interval in ms */
	watchdog = simple_strtoul(nvram_safe_get("watchdog"), NULL, 0);

	/* Ensure at least WATCHDOG_MIN
	 * Values above WATCHDOG_MAX overflow counter
	 */
	if (watchdog > 0) {
		if (watchdog < WATCHDOG_MIN)
			watchdog = WATCHDOG_MIN;

		if (watchdog > WATCHDOG_MAX)
			watchdog = WATCHDOG_MAX;
	}

	/* Set panic timeout in seconds */
	panic_timeout = watchdog / 1000;

}

static irqreturn_t
bcm53000_timer_interrupt(int irq, void *dev_id)
{

#ifdef CONFIG_HWTIMER_HOOKS
	spin_lock(bcm53000_timer.lock);
	if (bcm53000_timer.hook != NULL) {
		(bcm53000_timer.hook) (bcm53000_timer.hook_data);
	}
	spin_unlock(bcm53000_timer.lock);
#endif	/* CONFIG_HWTIMER_HOOKS */
	/* Generic MIPS timer code */
	timer_interrupt(irq, dev_id);

	/* Set the watchdog timer to reset after the specified number of ms */
	if (watchdog > 0)
		si_watchdog(sih, WATCHDOG_CLOCK / 1000 * watchdog);

	return IRQ_HANDLED;
}

static struct irqaction bcm53000_timer_irqaction = {
	bcm53000_timer_interrupt,
	SA_INTERRUPT,	/* no timer interrupt threading */
	CPU_MASK_NONE,
	"timer",
	NULL,
	NULL
};

/* void __init bcm947xx_timer_setup(struct irqaction *irq) */
void __init plat_timer_setup(struct irqaction *irq)
{
#ifdef CONFIG_HWTIMER_HOOKS
	register_hwtimer(&bcm53000_timer);
#endif /* CONFIG_HWTIMER_HOOKS */

	/* Enable the timer interrupt */
	setup_irq(7, &bcm53000_timer_irqaction);
}
