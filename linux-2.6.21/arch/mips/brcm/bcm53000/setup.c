/*
 * Generic setup routines for Broadcom MIPS boards
 *
 * Copyright (C) 2004 Broadcom Corporation
 * Copyright (C) 2005 MontaVista Inc. ( 2.6 kernel modification )
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <asm/bootinfo.h>
#include <asm/time.h>
#include <asm/reboot.h>

#ifdef CONFIG_MTD_PARTITIONS
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/minix_fs.h>
#include <linux/ext2_fs.h>
#include <linux/romfs_fs.h>
#include <linux/cramfs_fs.h>
#endif

#include <typedefs.h>
#include <bcmdevs.h>
#include <bcmutils.h>
#include <bcmnvram.h>
#include <siutils.h>
#include <hndmips.h>
#include <sbchipc.h>
#include <hndchipc.h>
#include <asm/bcmsi/cfe_api.h> 
#include <asm/bcmsi/cfe_error.h> 

extern void bcm53000_time_init(void);
extern int sysResetgpio;
extern int cpuResetgpio;

/* Global SB handle */
si_t *bcm53000_sih = NULL;
spinlock_t bcm53000_sih_lock = SPIN_LOCK_UNLOCKED;
EXPORT_SYMBOL(bcm53000_sih);
EXPORT_SYMBOL(bcm53000_sih_lock);

/* Convenience */
#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock


void bcm53000_machine_restart(char *command)
{
    int gpioFlag = 0x0;
#if 0
    char name[128];
#endif
    printk("Please stand by while rebooting the system...\n");

    local_irq_disable();
    /* Using the GPIO pins wired to CPU/board reset to achieve system reboot */
    /* BCM953003C using GPIO 11 and 12 */
    /* BCM953001R24M using GPIO 7 */
#if 0    
/*TODO:For multiple boards*/
    cfe_getenv("boardtype",name,128); 
    if(!strncmp(name,"bcm953003c",strlen("bcm953003c"))){ 
        *(volatile uint32 *)(0xb8000064) = 0x00001800;
        *(volatile uint32 *)(0xb8000068) = 0x0000f800;
        *(volatile uint32 *)(0xb8000064) = 0x00000000;
    } else if(!strncmp(name,"bcm953001r",strlen("bcm953001r"))){ 
        *(volatile uint32 *)(0xb8000064) = 0x00000080; 
        *(volatile uint32 *)(0xb8000068) = 0x00000080;
        *(volatile uint32 *)(0xb8000064) = 0x00000000;
    } else {
        /* The boards bcm953003rsp and bcm953003rwap use */
        /* GPIO 6 and 7 for the system reset */
        *(volatile uint32 *)(0xb8000064) = 0x000000c0; 
        *(volatile uint32 *)(0xb8000068) = 0x000000c0;
        *(volatile uint32 *)(0xb8000064) = 0x00000000;
    /* Set the watchdog timer to reset immediately */
    si_watchdog(sih, 1);
    }
#else

#if 0
*(volatile uint32 *)(0xb8000064) = 0x00001800;
*(volatile uint32 *)(0xb8000068) = 0x0000f800;
*(volatile uint32 *)(0xb8000064) = 0x00000000;
#endif
if (sysResetgpio != -1) {
   gpioFlag |=  (1 << sysResetgpio);
}

if (cpuResetgpio != -1) {
   gpioFlag |=  (1 << cpuResetgpio);
}

/* bcm53001 processor can use GPIO 6/7 for UART1  */
if ((sysResetgpio == 7 || sysResetgpio == 6) ||
    (cpuResetgpio == 7 || cpuResetgpio == 6))
{
  /* Select GPIO mode for GPIO6/7 */
   *(volatile uint32 *)(0xb8000008) &= 0x7F;
}
*(volatile uint32 *)(0xb8000064) = gpioFlag;
*(volatile uint32 *)(0xb8000068) = gpioFlag;
*(volatile uint32 *)(0xb8000064) = 0x00000000;

#endif

    while (1);
}

#ifdef CONFIG_CHEETAH_MINIKS
void miniks_machine_restart(char *command)
{
  void * pointer;
  chipcregs_t *chipcommon;

  local_irq_disable();
  pointer = ioremap_nocache(0x18000000, PAGE_SIZE);
  chipcommon = (chipcregs_t *)pointer;
  if (chipcommon == NULL)
  {
    printk("Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
  else
  {
    chipcommon->gpiocontrol = 0x00;
    chipcommon->gpioout = 0xfeff;
    chipcommon->gpioouten = 0x100;
    /*System should reset here. Use cpu reset if not */
    printk("\n      Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
}
#endif

#ifdef CONFIG_CHEETAH_KS72XXP
void ks72xxp_machine_restart(char *command)
{
  char *cpldbase;
  cpldbase = (char *)ioremap_nocache(0x1E000000, 0x16);
  local_irq_disable();
  if (cpldbase == NULL)
  {
    printk(" Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
  else
  {
    cpldbase[8] = 0x20;
    /*System should reset here. Use cpu reset if not */
    printk("\n        Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);

  }
}
#endif
void bcm53000_machine_halt(void)
{
    printk("System halted\n");

    /* Disable interrupts and watchdog and spin forever */
    local_irq_disable();
    si_watchdog(sih, 0);
    while (1);
}


#ifdef CONFIG_SERIAL_8250
static void __init serial_setup(si_t *sih)
{
  /* 
   * Since 8250 UART driver will control divider of UART module to achieve 
   * baud rate specified in kernel arguments, we simply let UART clock 
   * to be divided by 2 (so the pseudo baud rate is clock / 16 / 2). 
  */ 
  si_serial_init(sih, NULL, ALP_CLOCK / 16 / 2); 

}
#endif				/* CONFIG_SERIAL_8250 */

/* Virtual IRQ base, after last hw IRQ */
#define SBMIPS_VIRTIRQ_BASE	6

void __init plat_mem_setup(void)
{
    /* Get global SB handle */
    sih = si_kattach(SI_OSH);
    
    /* Initialize clocks and interrupts */
    si_mips_init(sih, SBMIPS_VIRTIRQ_BASE);

#ifdef CONFIG_SERIAL_8250
    /* Initialize UARTs */
    serial_setup(sih);
#endif

    /* Generic setup */
    _machine_restart = bcm53000_machine_restart;
#ifdef CONFIG_CHEETAH_KS72XXP
    _machine_restart = ks72xxp_machine_restart;
#endif
#ifdef CONFIG_CHEETAH_MINIKS
    _machine_restart = miniks_machine_restart;
#endif
    _machine_halt = bcm53000_machine_halt;
    /* _machine_power_off = bcm53000_machine_halt;*/
    pm_power_off = bcm53000_machine_halt;

    board_time_init = bcm53000_time_init;
    /* board_timer_setup = bcm53000_timer_setup; */
}

const char *get_system_type(void)
{
	static char s[32];

	if (bcm53000_sih) {
		sprintf(s, "Broadcom BCM%X chip rev %d", bcm53000_sih->chip,
			bcm53000_sih->chiprev);
		return s;
	}
	else
		return "Broadcom bcm53000";
}

void __init bus_error_init(void)
{
}


