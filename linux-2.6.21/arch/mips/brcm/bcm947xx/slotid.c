/*
 * Slot ID proc file for Broadcom MIPS boards
 *
 * Copyright (C) 2004 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/ctype.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

#define BCM95836LM_FPGA_ADDR            0X1A000000

#define BCM95836LM_RESET                0x0000
#define BCM95836LM_FPGA_REV             0x2000
#define BCM95836LM_SLOTID               0x2001
#define BCM95836LM_CPUSUBSYSID          0x4000
#define BCM95836LM_BOARDID              0x8000

#define CPLD_BASE                       (KSEG1ADDR(BCM95836LM_FPGA_ADDR))

#define CPLD_REG_READ(r)                *((volatile uint8_t *)((CPLD_BASE + r)^3))
#define CPLD_REG_WRITE(r,v)             *((volatile uint8_t *)((CPLD_BASE + r)^3)) = v

#define CPLD_GET_CPUSUBSYS_ID()         CPLD_REG_READ(BCM95836LM_CPUSUBSYSID)
#define CPLD_GET_BOARD_ID()             CPLD_REG_READ(BCM95836LM_BOARDID)
#define CPLD_GET_SLOT_ID()              CPLD_REG_READ(BCM95836LM_SLOTID)
#define CPLD_GET_CPLD_REV()             CPLD_REG_READ(BCM95836LM_FPGA_REV)

/* CPU subsystem IDs */
#define CPUSUBSYS_BCM95695P48LM         0x13
#define CPUSUBSYS_BCM95674P6CX4LM       0x16
#define CPUSUBSYS_BCM956504P48LM        0x17


#ifdef CONFIG_PROC_FS

static int slotid;

static int slotid_proc_read(char *page, char **start, off_t off, int count, 
        int *eof, void *data)
{
        char *out = page;
        int len;

        out += sprintf(out, "%d\n", slotid);

        len = out - page - off;
        if (len < count) {
                *eof = 1;
                if (len <= 0) return 0;
        } else {
                len = count;
        }
        *start = page + off;
        return len;
}

static int slotid_proc_write(struct file *file, const char *buf, 
        unsigned long count, void *data)
{
        char *cur, lbuf[8];

        memset(lbuf, 0, sizeof(lbuf));

        copy_from_user(lbuf, buf, sizeof(lbuf)-1);
        cur = lbuf;

        /* skip initial spaces */
        while (*cur && isspace(*cur))
        {
                cur++;
        }

        slotid = *cur - '0';

        return count;
}

static int __init slotid_create_procfs(void)
{
        struct proc_dir_entry *ent;

        ent = create_proc_entry("slotid", S_IFREG|S_IRUGO, NULL);
        if (!ent) return -1;
        ent->nlink = 1;
        ent->read_proc = slotid_proc_read;
        ent->write_proc = slotid_proc_write;
        ent->owner = THIS_MODULE;

#ifdef CONFIG_BCM95836_CPCI
        switch (CPLD_GET_CPUSUBSYS_ID()) {
         case CPUSUBSYS_BCM95695P48LM:
         case CPUSUBSYS_BCM95674P6CX4LM:
         case CPUSUBSYS_BCM956504P48LM:
                slotid = CPLD_GET_SLOT_ID();
                break;
        }
#endif

        return 0;
}

module_init(slotid_create_procfs)

#endif /* CONFIG_PROC_FS */
