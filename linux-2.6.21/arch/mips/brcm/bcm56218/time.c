/*
 * Copyright (C) 2004 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/serial_reg.h>
#include <linux/interrupt.h>
#include <asm/addrspace.h>
#include <asm/io.h>
#include <asm/time.h>

#include <asm/brcm/typedefs.h>
#include <asm/brcm/sbconfig.h>
#include <asm/brcm/sbextif.h>
#include <asm/brcm/sbutils.h>
#include <asm/brcm/sbmips.h>
#include <asm/brcm/bcmtime.h>

extern void *bcm956218_sbh;
extern spinlock_t bcm956218_sbh_lock;

/* Convenience */
#define sbh bcm956218_sbh
#define sbh_lock bcm956218_sbh_lock

extern int panic_timeout;
static int watchdog = 0;

void __init bcm956218_time_init(void)
{
	unsigned int hz;

	/*
	 * Use deterministic values for initial counter interrupt
	 * so that calibrate delay avoids encountering a counter wrap.
	 */
	write_c0_count(0);
	write_c0_compare(0xffff);

	if (!(hz = sb_mips_clock(sbh)))
		hz = 100000000;

	printk("CPU: BCM%05d rev %d at %d MHz\n", sb_chip(sbh),
	       sb_chiprev(sbh), (hz + 500000) / 1000000);

	/* Set MIPS counter frequency for fixed_rate_gettimeoffset() */
	mips_hpt_frequency = hz / 2;

	/* Set watchdog interval in ms */
	watchdog = 0xffffffff;

	/* Set panic timeout in seconds */
	panic_timeout = watchdog / 1000;
}

void __init plat_timer_setup(struct irqaction *irq)
{
	bcm956218_timer_setup(irq);
	board_time_init = bcm956218_time_init;
}


static irqreturn_t
bcm956218_timer_interrupt(int irq, void *dev_id)
{

	/* Generic MIPS timer code */
	timer_interrupt(irq, dev_id);

	/* Set the watchdog timer to reset after the specified number of ms */
	if (watchdog > 0)
		sb_watchdog(sbh, WATCHDOG_CLOCK / 1000 * watchdog);

	return IRQ_HANDLED;
}

static struct irqaction bcm956218_timer_irqaction = {
	bcm956218_timer_interrupt,
	SA_INTERRUPT,	/* no timer interrupt threading */
	CPU_MASK_NONE,
	"timer",
	NULL,
	NULL
};

void __init bcm956218_timer_setup(struct irqaction *irq)
{
	/* Enable the timer interrupt */
	setup_irq(7, &bcm956218_timer_irqaction);
}

