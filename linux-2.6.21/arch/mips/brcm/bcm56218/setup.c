/*
 * Generic setup routines for Broadcom MIPS boards
 *
 * Copyright (C) 2004 Broadcom Corporation
 * Copyright (C) 2005 MontaVista Inc. ( 2.6 kernel modification )
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <asm/bootinfo.h>
#include <asm/time.h>
#include <asm/reboot.h>
#include <linux/initrd.h>
#include <linux/major.h>
#include <linux/kdev_t.h>
#include <linux/root_dev.h>
#include <asm/bootinfo.h>

#include <asm/brcm/typedefs.h>
#include <asm/brcm/bcmdevs.h>
#include <asm/brcm/bcmutils.h>
#include <asm/brcm/sbmips.h>
#include <asm/brcm/sbutils.h>
#include <asm/brcm/bcmtime.h>

/* Global SB handle */
void *bcm956218_sbh = NULL;
spinlock_t bcm956218_sbh_lock = SPIN_LOCK_UNLOCKED;
EXPORT_SYMBOL(bcm956218_sbh);
EXPORT_SYMBOL(bcm956218_sbh_lock);

#ifdef CONFIG_EMBEDDED_RAMDISK
extern void * __rd_start, * __rd_end;
#endif

/* Convenience */
#define sbh bcm956218_sbh
#define sbh_lock bcm956218_sbh_lock

void bcm956218_machine_restart(char *command)
{
	printk("Please stand by while rebooting the system...\n");

	/* Set the watchdog timer to reset immediately */
	local_irq_disable();
	sb_watchdog(sbh, 1);
	while (1);
}

void bcm956218_machine_halt(void)
{
	printk("System halted\n");

	/* Disable interrupts and watchdog and spin forever */
	local_irq_disable();
	sb_watchdog(sbh, 0);
	while (1);
}

#ifdef CONFIG_SERIAL_8250
static void __init serial_setup(void *sbh)
{
	sb_serial_init(sbh, NULL /*serial_add */ );

}
#endif				/* CONFIG_SERIAL_8250 */

void __init plat_mem_setup(void)
{
	/* Get global SB handle */
	sbh = sb_kattach();

	/* Initialize clocks and interrupts */
	sb_mips_init(sbh);

#ifdef CONFIG_SERIAL_8250
	/* Initialize UARTs */
	serial_setup(sbh);
#endif

	board_time_init = bcm956218_time_init;

}

const char *get_system_type(void)
{
	return "Broadcom BCM956218";
}

void __init bus_error_init(void)
{
}

