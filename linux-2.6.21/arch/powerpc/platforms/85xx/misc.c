/*
 * MPC85xx generic code.
 *
 * Maintained by Kumar Gala (see MAINTAINERS for contact information)
 *
 * Copyright 2005 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */
#include <linux/irq.h>
#include <linux/module.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <sysdev/fsl_soc.h>
#include <linux/autoconf.h>

static __be32 __iomem *rstcr;
#ifdef CONFIG_BCM98548
static __be32 __iomem *rstdr;
#endif

extern void abort(void);

static int __init mpc85xx_rstcr(void)
{
	/* map reset control register */
#ifdef CONFIG_BCM98548
        rstdr = ioremap(get_immrbase() + 0xE0040, 0xff);
        rstcr = ioremap(get_immrbase() + 0xE0030, 0xff);
#else
	rstcr = ioremap(get_immrbase() + 0xE00B0, 0xff);
#endif
	return 0;
}

arch_initcall(mpc85xx_rstcr);

void mpc85xx_restart(char *cmd)
{
	local_irq_disable();
	if (rstcr) {
		/* set reset control register */
#ifdef CONFIG_BCM98548
	  out_be32(rstdr, ~0x0);  /* HRESET_REQ */
	  out_be32(rstcr, 0x200); /* HRESET_REQ */
	  out_be32(rstdr, ~0x1);  /* HRESET_REQ */
	  out_be32(rstdr, ~0x3);  /* HRESET_REQ */
#else
	  out_be32(rstcr, 0x2);	/* HRESET_REQ */
#endif
	} else
		printk (KERN_EMERG "Error: reset control register not mapped, spinning!\n");
	abort();
}
