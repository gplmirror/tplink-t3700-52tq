#include <linux/types.h>
#include <linux/errno.h>
#include <asm/uaccess.h>

#include "spe.h"
#include "soft-fp.h"
#include "single.h"

int
efsctsi(u32 *rD, void *rB)
{
	FP_DECL_S(B);
	unsigned int r;

	__FP_UNPACK_S(B, rB);
	_FP_ROUND(1, B);
	FP_TO_INT_S(r, B, 32, 1);
	rD[0] = r;

#ifdef DEBUG
	printk("%s: D %p, B %p: ", __FUNCTION__, rD, rB);
	printk("\n");
#endif

	return 0;
}
