#include <linux/types.h>
#include <linux/errno.h>
#include <asm/uaccess.h>

int
efsabs(u32 *rD, u32 *rA)
{
	rD[0] = rA[0] & 0x7fffffff;

#ifdef DEBUG
	printk("%s: D %p, A %p: ", __FUNCTION__, rD, rA);
	printk("\n");
#endif

	return 0;
}
