#include <linux/types.h>
#include <linux/errno.h>
#include <asm/uaccess.h>

#include "spe.h"
#include "soft-fp.h"
#include "double.h"

int
efdctsi(u32 *rD, void *rB)
{
	FP_DECL_D(B);
	unsigned int r;

	__FP_UNPACK_D(B, rB);
	_FP_ROUND(2, B);
	FP_TO_INT_D(r, B, 32, 1);
	rD[1] = r;

#ifdef DEBUG
	printk("%s: D %p, B %p: ", __FUNCTION__, rD, rB);
	dump_double(rD);
	printk("\n");
#endif

	return 0;
}
