/* marker.c
 *
 * Powerpc optimized marker enabling/disabling.
 *
 * Mathieu Desnoyers <mathieu.desnoyers@polymtl.ca>
 */

#include <linux/module.h>
#include <linux/marker.h>
#include <linux/string.h>
#include <asm/cacheflush.h>

int marker_optimized_set_enable(void *address, char enable)
{
	char newi[MARK_OPTIMIZED_ENABLE_IMMEDIATE_OFFSET+1];
	int size = MARK_OPTIMIZED_ENABLE_IMMEDIATE_OFFSET
			+ sizeof(MARK_OPTIMIZED_ENABLE_TYPE);

	memcpy(newi, address, size);
	MARK_OPTIMIZED_ENABLE(&newi[0]) = enable;
	memcpy(address, newi, size);
	flush_icache_range((unsigned long)address, size);
	return 0;
}
EXPORT_SYMBOL_GPL(marker_optimized_set_enable);
