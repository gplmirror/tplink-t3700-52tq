/* marker.c
 *
 * Erratum 49 fix for Intel PIII and higher.
 *
 * Permits marker activation by XMC with correct serialization.
 *
 * Reentrant for NMI and trap handler instrumentation. Permits XMC to a
 * location that has preemption enabled because it involves no temporary or
 * reused data structure.
 *
 * Mathieu Desnoyers <mathieu.desnoyers@polymtl.ca>
 */

#include <linux/notifier.h>
#include <linux/mutex.h>
#include <linux/preempt.h>
#include <linux/smp.h>
#include <linux/notifier.h>
#include <linux/module.h>
#include <linux/marker.h>
#include <linux/kdebug.h>

#include <asm/cacheflush.h>

#define BREAKPOINT_INSTRUCTION  0xcc
#define BREAKPOINT_INS_LEN 1

static DEFINE_MUTEX(mark_mutex);
static long target_eip = 0;

static void mark_synchronize_core(void *info)
{
	sync_core();	/* use cpuid to stop speculative execution */
}

/* We simply skip the 2 bytes load immediate here, leaving the register in an
 * undefined state. We don't care about the content (0 or !0), because we are
 * changing the value 0->1 or 1->0. This small window of undefined value
 * doesn't matter.
 */
static int mark_notifier(struct notifier_block *nb,
	unsigned long val, void *data)
{
	enum die_val die_val = (enum die_val) val;
	struct die_args *args = (struct die_args *)data;

	if (!args->regs || user_mode_vm(args->regs))
		return NOTIFY_DONE;

	if (die_val == DIE_INT3	&& args->regs->eip == target_eip) {
		args->regs->eip += 1; /* Skip the next byte of load immediate */
		return NOTIFY_STOP;
	}
	return NOTIFY_DONE;
}

static struct notifier_block mark_notify = {
	.notifier_call = mark_notifier,
	.priority = 0x7fffffff,	/* we need to be notified first */
};

int marker_optimized_set_enable(void *address, char enable)
{
	char saved_byte;
	int ret;
	char *dest = address;

	if (!(enable ^ dest[1])) /* Must be a state change 0<->1 to execute */
		return 0;

	mutex_lock(&mark_mutex);
	target_eip = (long)address + BREAKPOINT_INS_LEN;
	/* register_die_notifier has memory barriers */
	register_die_notifier(&mark_notify);
	saved_byte = *dest;
	*dest = BREAKPOINT_INSTRUCTION;
	wmb();
	/* Execute serializing instruction on each CPU.
	 * Acts as a memory barrier. */
	ret = on_each_cpu(mark_synchronize_core, NULL, 1, 1);
	BUG_ON(ret != 0);

	dest[1] = enable;
	wmb();
	*dest = saved_byte;
		/* Wait for all int3 handlers to end
		   (interrupts are disabled in int3).
		   This CPU is clearly not in a int3 handler
		   (not preemptible).
		   synchronize_sched has memory barriers */
	synchronize_sched();
	unregister_die_notifier(&mark_notify);
	/* unregister_die_notifier has memory barriers */
	target_eip = 0;
	mutex_unlock(&mark_mutex);
	flush_icache_range(address, size);
	return 0;
}
EXPORT_SYMBOL_GPL(marker_optimized_set_enable);
