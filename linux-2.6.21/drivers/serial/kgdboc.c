/*
 * drivers/serial/kgdboc.c
 *
 * Based on the same principle as kgdboe using the NETPOLL api, this
 * driver uses a serial polling api to implement a gdb serial inteface
 * which is multiplexed on a console port.
 *
 * Maintainer: Jason Wessel <jason.wessel@windriver.com>
 *
 * 2007 (c) Jason Wessel - Wind River Systems, Inc.
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2. This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */
#include <linux/kernel.h>
#include <linux/kgdb.h>
#include <linux/tty.h>

#define NOT_CONFIGURED_STRING "not_configured"
#define MAX_KGDBOC_CONFIG_STR 40

static struct kgdb_io kgdboc_io_ops;

/* -1 = init not run yet, 0 = unconfigured, 1 = configured. */
static int configured = -1;

MODULE_DESCRIPTION("KGDB Console TTY Driver");
MODULE_LICENSE("GPL");
static char config[MAX_KGDBOC_CONFIG_STR] = NOT_CONFIGURED_STRING;
static struct kparam_string kps = {
	.string = config,
	.maxlen = MAX_KGDBOC_CONFIG_STR,
};

static struct tty_driver *kgdb_tty_driver;
static int kgdb_tty_line;

static int kgdboc_option_setup(char *opt)
{
	if (strlen(opt) > MAX_KGDBOC_CONFIG_STR) {
		printk(KERN_ERR "kgdboc: config string too long\n");
		strcpy(config, NOT_CONFIGURED_STRING);
		return 1;
	}

	/* If we're being given a new configuration, copy it in. */
	if (opt != config)
		strcpy(config, opt);
	return 0;
}
__setup("kgdboc=", kgdboc_option_setup);

static int configure_kgdboc(void)
{
	struct tty_driver *p;
	int ret = 1;
	char *str;
	int tty_line = 0;

	kgdboc_option_setup(config);
	if (strcmp(config, NOT_CONFIGURED_STRING) == 0)
		goto err;

	/* Search through the tty devices to look for a match */
	list_for_each_entry(p, &tty_drivers, tty_drivers) {
		if (!(p->type == TTY_DRIVER_TYPE_SERIAL &&
			  strncmp(config, p->name, strlen(p->name)) == 0))
			continue;
		str = config + strlen(p->name);
		tty_line = simple_strtoul(str, &str, 10);
		if (*str == ',')
			str++;
		if (*str == '\0')
			str = 0;

		if (tty_line >= 0 && tty_line <= p->num &&
			p->poll_init && !p->poll_init(p, tty_line, str)) {
			kgdb_tty_driver = p;
			kgdb_tty_line = tty_line;
			ret = 0;
			break;
		}
	}
	if (ret) {
		printk(KERN_ERR "kgdboc: invalid parmater: %s\n", config);
		printk(KERN_ERR "Usage kgdboc=<serial_device>[,baud]\n");
		goto err;
	}

	if (kgdb_register_io_module(&kgdboc_io_ops)) {
		printk(KERN_ERR "KGDB IO registration failed\n");
		goto err;
	}
	configured = 1;

	printk(KERN_INFO "kgdboc: Debugging enabled\n");
	return 0;

err:
	strcpy(config, NOT_CONFIGURED_STRING);
	configured = 0;
	return -EINVAL;

}

static int init_kgdboc(void)
{
	/* Already configured? */
	if (configured == 1)
		return 0;

	if (configure_kgdboc())
		printk(KERN_INFO "kgdboc: Driver loaded in"
		" not_configured state\n");

	return 0;
}

static void cleanup_kgdboc(void)
{
	kgdb_unregister_io_module(&kgdboc_io_ops);
}

static int kgdboc_get_char(void)
{
	return kgdb_tty_driver->poll_get_char(kgdb_tty_driver,
			kgdb_tty_line);
}

static void kgdboc_put_char(u8 chr)
{
	kgdb_tty_driver->poll_put_char(kgdb_tty_driver,
			kgdb_tty_line, chr);
}

static int param_set_kgdboc_var(const char *kmessage,
		struct kernel_param *kp)
{
	char kmessage_save[MAX_KGDBOC_CONFIG_STR];
	int msg_len = strlen(kmessage);

	if (msg_len + 1 > MAX_KGDBOC_CONFIG_STR) {
		printk(KERN_ERR "%s: string doesn't fit in %u chars.\n",
		       kp->name, MAX_KGDBOC_CONFIG_STR - 1);
		return -ENOSPC;
	}

	/* Only copy in the string if the init function has not run yet */
	if (configured < 0) {
		strncpy(config, kmessage, sizeof(config));
		return 0;
	}

	if (kgdb_connected) {
		printk(KERN_ERR "kgdboc: Cannot reconfigure while KGDB is "
				"connected.\n");
		return 0;
	}

	/* Start the reconfiguration process by saving the old string */
	strncpy(kmessage_save, config, sizeof(kmessage_save));


	/* Copy in the new param and strip out invalid characters so we
	 * can optionally specify the MAC.
	 */
	strncpy(config, kmessage, sizeof(config));
	msg_len--;
	while (msg_len > 0 &&
			(config[msg_len] < ',' || config[msg_len] > 'f')) {
		config[msg_len] = '\0';
		msg_len--;
	}

	/* Check to see if we are unconfiguring the io module and that it
	 * was in a fully configured state, as this is the only time that
	 * netpoll_cleanup should get called
	 */
	if (configured == 1 && strcmp(config, NOT_CONFIGURED_STRING) == 0) {
		printk(KERN_INFO "kgdboc: reverting to unconfigured state\n");
		cleanup_kgdboc();
		return 0;
	} else
		/* Go and configure with the new params. */
		configure_kgdboc();

	if (configured == 1)
		return 0;

	/* If the new string was invalid, revert to the previous state, which
	 * is at a minimum not_configured. */
	strncpy(config, kmessage_save, sizeof(config));
	if (strcmp(kmessage_save, NOT_CONFIGURED_STRING) != 0) {
		printk(KERN_INFO "kgdboc: reverting to prior configuration\n");
		/* revert back to the original config */
		strncpy(config, kmessage_save, sizeof(config));
		configure_kgdboc();
	}
	return 0;
}

static void kgdboc_pre_exp_handler(void)
{
	/* Increment the module count when the debugger is active */
	if (!kgdb_connected)
		try_module_get(THIS_MODULE);
}

static void kgdboc_post_exp_handler(void)
{
	/* decrement the module count when the debugger detaches */
	if (!kgdb_connected)
		module_put(THIS_MODULE);
}

static struct kgdb_io kgdboc_io_ops = {
	.read_char = kgdboc_get_char,
	.write_char = kgdboc_put_char,
	.init = init_kgdboc,
	.pre_exception = kgdboc_pre_exp_handler,
	.post_exception = kgdboc_post_exp_handler,
};

module_init(init_kgdboc);
module_exit(cleanup_kgdboc);
module_param_call(kgdboc, param_set_kgdboc_var, param_get_string, &kps, 0644);
MODULE_PARM_DESC(kgdboc, " kgdboc=<serial_device>[,baud]\n");
