/*
    i2c-mpc824x.c - Adapter driver for I2C support on Motorola MPC824x

    Copyright (c) 2002 NetBotz Inc <primm@netbotz.com>

    Derived from i2c-via.c

    Copyright (c) 1998, 1999 Kysti M�kki <kmalkki@cc.hut.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <asm/io.h>
#include <linux/types.h>
#include <linux/i2c.h>
#include <linux/i2c-algo-mpc824x.h>
#include <linux/init.h>

#define DRV_VERSION	"1.1"

/* PCI device */
#define PCI_DEVICE_ID_BRIDGE_MPC8240    0x0003
#define PCI_DEVICE_ID_BRIDGE_MPC8245    0x0006

#define PCI_CONFIG_EUMB_BASE_ADDR       0x78
#define I2C_REGISTER_BASE_OFFSET        0x03000

MODULE_LICENSE("GPL");

extern u32 *eumb_base;
/* ------------------------------------------------------------------------ */

static struct i2c_algo_mpc824x_data i2c_mpc824x_data = { 0 };

static struct i2c_adapter i2c_mpc824x_ops = {
        .owner     = THIS_MODULE,
        .name      = "Motorola mpc824X i2c",
        .id        = I2C_HW_MPC824X,
        .class     = I2C_CLASS_HWMON,
	.algo_data = &i2c_mpc824x_data,
        .timeout   = 100,
        .retries   = 3
};

static struct pci_device_id mpc824x_ids[] __devinitdata = {
        { PCI_DEVICE(PCI_VENDOR_ID_MOTOROLA, PCI_DEVICE_ID_BRIDGE_MPC8240) },
        { PCI_DEVICE(PCI_VENDOR_ID_MOTOROLA, PCI_DEVICE_ID_BRIDGE_MPC8245) },
        { 0, } 
};

MODULE_DEVICE_TABLE (pci, mpc824x_ids);

static int __devinit mpc824x_i2c_probe(struct pci_dev *dev, const struct pci_device_id *id)
{
        u32 lcl_eumb_base;

        /* Read EUMB base address                                   */
        if(pci_read_config_dword(dev, PCI_CONFIG_EUMB_BASE_ADDR,
                &lcl_eumb_base) == PCIBIOS_SUCCESSFUL) {
                eumb_base = (u32 *)(lcl_eumb_base + I2C_REGISTER_BASE_OFFSET);
        }

	printk("i2c-mpc824x.o version %s\n", DRV_VERSION);
	if (i2c_mpc824x_add_bus(&i2c_mpc824x_ops, dev) == 0) {
		printk("i2c-mpc824x.o: Module succesfully loaded\n");
		return 0;
	} else {
		printk("i2c-mpc824x.o: Algo-mpc824x error, couldn't register bus\n");
		return -ENODEV;
	}
}

static void __devexit mpc824x_i2c_remove(struct pci_dev *dev)
{
	i2c_mpc824x_del_bus(&i2c_mpc824x_ops);
        return;
}

/* Structure for a device driver */
static struct pci_driver mpc824x_i2c_driver = {
        .name = "mpc824x-i2c",
        .id_table = mpc824x_ids,
        .probe = mpc824x_i2c_probe,
        .remove = mpc824x_i2c_remove,
};

int __init i2c_mpc824x_init(void)
{
	return pci_register_driver(&mpc824x_i2c_driver);
}

static void __exit i2c_mpc824x_exit(void)
{
        pci_unregister_driver(&mpc824x_i2c_driver);
        return;
}

module_init(i2c_mpc824x_init);
module_exit(i2c_mpc824x_exit);

MODULE_AUTHOR("Michael Primm <primm@netbotz.com>");
MODULE_DESCRIPTION("i2c for Motorola mpc824x");

