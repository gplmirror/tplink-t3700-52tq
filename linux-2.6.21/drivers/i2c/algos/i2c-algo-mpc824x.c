/* ------------------------------------------------------------------------- */
/* i2c-algo-mpc824x.c i2c driver algorithms for mpc824x i2c interfaces	     */
/* ------------------------------------------------------------------------- */
/*   Copyright (C) 2002 NetBotz Inc <primm@netbotz.com>			     */
/* Derived from i2c-algo-bit.c						     */
/*   Copyright (C) 1995-2000 Simon G. Vogl

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		     */
/* ------------------------------------------------------------------------- */

/* $Id: i2c-algo-mpc824x.c,v 1.1 2005/05/20 20:36:26 andyg Exp $ */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/pci.h>
#include <linux/ioport.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/i2c.h>
#include <linux/i2c-algo-mpc824x.h>
#include <linux/smp_lock.h>
#include <linux/interrupt.h>

#define DRV_VERSION "1.1"

#define USE_ASYNC_FOR_XFER
#define USE_HW_INT_FOR_XFER

#ifdef USE_HW_INT_FOR_XFER
//#define	TIMER_PERIOD	(HZ/50)
#define	TIMER_PERIOD	(HZ/10)
#else
#define TIMER_PERIOD	(1)
#endif

/*#define HW_INT 129*/
#define HW_INT 16

enum i2c_mpc824x_regs {
	I2CADR, I2CFDR, I2CCR, I2CSR, I2CDR
};

typedef enum _i2c_mpc824x_status {
	ERROR = -1,
	NO_EVENT = -2,
	XMIT_ERROR = -3,
	RCV_ERROR = -4,
	BUS_BUSY = -5,
	ARB_LOST = -6,
	/* Pur errors before SUCCESS					*/
	SUCCESS = 0,
	ADDRESS_SENT,
	RECV_BUF_FULL,
	XMIT_BUF_EMPTY,
} i2c_mpc824x_status;

struct i2c_mpc824x_xfer_req {
	struct i2c_mpc824x_xfer_req *next;

	struct i2c_adapter *i2c_adap;
	struct i2c_msg *msgs;
	int num;
	void *usr_data;
	void (*callback)(struct i2c_adapter *, struct i2c_msg *,
		int num, int rc, void *usr_data);
	/* State variables						*/
	int cur_buf_num;	/* Current buffer index			*/
	int cur_do_xmit;	/* Current buffer is a transmit		*/
	unsigned long cur_wait_time;	/* Next timeout time (jiffies)	*/
};

static int	did_init = 0;
static spinlock_t	lock;
static struct i2c_mpc824x_xfer_req *req_head = NULL;
static struct i2c_mpc824x_xfer_req *req_tail = NULL;
static struct timer_list our_timer;
static int timer_added = 0;
static struct i2c_adapter *mpc_adap;

/* Default divisor of 1536 : gives us < 100KHz for 133MHz or less 	*/
//#define DEFAULT_FDR	(0x0A)
#define DEFAULT_FDR	(0x3C)
#define DEFAULT_DFFSR   (0x20)
/* Defines for i2c_mpc824x_get/setbits					*/
#define I2C_BITS(reg, off, mask) ((((reg) & 0xFF) << 24) | \
	(((off) & 0xFF) << 16) | ((mask) & 0xFFFF))
#define I2C_GETOFF(v) 		(((v) >> 16) & 0xFF)
#define I2C_GETMASK(v)		((v) & 0xFFFF)
#define I2C_GETREG(v) 		(((v) >> 24) & 0xFF)

#define I2C_MPC824X_ADR_ADDR	I2C_BITS(I2CADR, 1, 0x7F)
#define I2C_MPC824X_FDR_DFFAR	I2C_BITS(I2CFDR, 8, 0x3F)
#define I2C_MPC824X_FDR_FDR	I2C_BITS(I2CFDR, 0, 0x3F)
#define I2C_MPC824X_FDR		I2C_BITS(I2CFDR, 0, 0x3F3F)
#define I2C_MPC824X_CR_MEN	I2C_BITS(I2CCR, 7, 0x01)
#define I2C_MPC824X_CR_MIEN	I2C_BITS(I2CCR, 6, 0x01)
#define I2C_MPC824X_CR_MSTA	I2C_BITS(I2CCR, 5, 0x01)
#define I2C_MPC824X_CR_MTX	I2C_BITS(I2CCR, 4, 0x01)
#define I2C_MPC824X_CR_TXAK	I2C_BITS(I2CCR, 3, 0x01)
#define I2C_MPC824X_CR_RSTA	I2C_BITS(I2CCR, 2, 0x01)
#define I2C_MPC824X_CR_PCII	I2C_BITS(I2CCR, 1, 0x01)
#define I2C_MPC824X_CR_BCST	I2C_BITS(I2CCR, 0, 0x01)
#define I2C_MPC824X_CR		I2C_BITS(I2CCR, 0, 0xFF)
#define I2C_MPC824X_SR_MCF	I2C_BITS(I2CSR, 7, 0x01)
#define I2C_MPC824X_SR_MAAS	I2C_BITS(I2CSR, 6, 0x01)
#define I2C_MPC824X_SR_MBB	I2C_BITS(I2CSR, 5, 0x01)
#define I2C_MPC824X_SR_MAL	I2C_BITS(I2CSR, 4, 0x01)
#define I2C_MPC824X_SR_BCA	I2C_BITS(I2CSR, 3, 0x01)
#define I2C_MPC824X_SR_SRW	I2C_BITS(I2CSR, 2, 0x01)
#define I2C_MPC824X_SR_MIF	I2C_BITS(I2CSR, 1, 0x01)
#define I2C_MPC824X_SR_RXAK	I2C_BITS(I2CSR, 0, 0x01)
#define I2C_MPC824X_SR		I2C_BITS(I2CSR, 0, 0xFF)
#define I2C_MPC824X_DR_DATA	I2C_BITS(I2CDR, 0, 0xFF)

/* ----- global variables ---------------------------------------------	*/
static int debug_level = 0;

u32 *eumb_base = NULL;

#define DEB(x, arg...) if((x) <= debug_level) { printk(arg); }

/* --- other auxiliary functions --------------------------------------	*/
static u32 i2c_mpc824x_getbits(u32 field)
{
	u32 val;
	val = in_le32(eumb_base + I2C_GETREG(field));
	val >>= I2C_GETOFF(field);	/* Shift field into place	*/
	val &= I2C_GETMASK(field);	/* And mask off other data	*/
	DEB(3, "i2c_mpc824x_getbits(0x%.8x)=0x%.8x\n",
		field, val);
	return val;
}

static void i2c_mpc824x_setbits(u32 field, u32 val)
{
	u32 oldval;
	u32 mask;
	u32 newval;

	DEB(3, "i2c_mpc824x_setbits(0x%.8x, 0x%.8x)\n",
		field, val);
	/* Get current value						*/
	oldval = in_le32(eumb_base + I2C_GETREG(field));
	mask = I2C_GETMASK(field);	/* Get mask			*/
	/* Mask off old values to keep					*/
	newval = oldval & (~(mask << I2C_GETOFF(field)));
	/* Now, add in the new value					*/
	newval |= (val & mask) << I2C_GETOFF(field);
	/* Now write out the modified register				*/
	out_le32(eumb_base + I2C_GETREG(field), newval);
}

static void i2c_init(void)
{
        unsigned int i2ccr_reg, i2cdr_reg;

	DEB(2, "i2c_init()\n");
	/* Clear control register, for starters				*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR, 0);
	/* Set FDR to a reasonable value (assume SDRAM clk <= 133MHz)	*/
	i2c_mpc824x_setbits(I2C_MPC824X_FDR_FDR,
		DEFAULT_FDR);
	i2c_mpc824x_setbits(I2C_MPC824X_FDR_DFFAR,
		DEFAULT_DFFSR);
	/* Set slave address to invalid address (we don't do slave)     */
	i2c_mpc824x_setbits(I2C_MPC824X_ADR_ADDR, 0x76);
#ifdef USE_HW_INT_FOR_XFER
	/* Initialize to interrupts enabled				*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_MIEN, 1);
#else
	/* Initialize to interrupts disabled (for now)			*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_MIEN, 0);
#endif
	/* We're all set : enable module				*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_MEN, 1);

        /*
         *implement the i2c bus reset fix
         *algorithm from the 8245 manual in section 10.4.6
         */
        i2ccr_reg = i2c_mpc824x_getbits(I2C_MPC824X_CR);
        i2c_mpc824x_setbits(I2C_MPC824X_CR,0x20);
        i2c_mpc824x_setbits(I2C_MPC824X_CR,0xA0);
        i2cdr_reg = i2c_mpc824x_getbits(I2C_MPC824X_DR_DATA);
        i2c_mpc824x_setbits(I2C_MPC824X_CR, i2ccr_reg);
}

static void i2c_term(void)
{
	DEB(2, "i2c_term()\n");
	/* Clear control register					*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR, 0);
}

static i2c_mpc824x_status i2c_start(struct i2c_algo_mpc824x_data *adap)
{
	DEB(2, "i2c_start()\n");
	/* Complain if not initialized					*/
	if(i2c_mpc824x_getbits(I2C_MPC824X_CR_MEN) == 0) {
		DEB(1, "i2c-algo-mpc824x: not initialized\n");
		return ERROR;
	}
	/* Check to see if bus is free (only if we're not restarting)	*/
	if(i2c_mpc824x_getbits(I2C_MPC824X_SR_MBB) &&
		(adap->is_restart == 0)) {
		i2c_init();	/* Reinitialize				*/
                /* Don't return bus error, as we should be able to 
                 * send our request after i2c_init has reset the bus    */ 
	}
	/* Set master mode						*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_MSTA, 1);
	/* Set MTC to indicate transmit					*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_MTX, 1);
	/* Clear txak							*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_TXAK, 0);
	/* Set restart appropriately					*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_RSTA,
		adap->is_restart?1:0);
	/* Write slave address being called into the data register	*/
	i2c_mpc824x_setbits(I2C_MPC824X_DR_DATA,
		(((u32)adap->slaveaddr & 0x7F) << 1) |
		(adap->is_xmit?0x00:0x01));
	/* If receive, then we're in the address phase			*/
	if(adap->is_xmit) {
		adap->is_rcv_addr = 0;
	} else {
		adap->is_rcv_addr = 1;
	}
	return SUCCESS;
}

static void i2c_stop(void)
{
	DEB(2, "i2c_stop()\n");
	/* Clear the master mode bit					*/
	i2c_mpc824x_setbits(I2C_MPC824X_CR_MSTA, 0);
}

/* Transmit next pending byte : return 1 if no more bytes, 0 if sent,
 * -1 if error								*/
static i2c_mpc824x_status i2c_xmit_next_byte(struct
	i2c_algo_mpc824x_data *adap)
{
	DEB(2, "i2c_xmit_next_byte(), bytes remaining = %d\n", adap->buflen - adap->bufindex);
	/* If any bytes to send						*/
	if(adap->bufindex >= 0) {
		/* If we've sent the last byte				*/
		if(adap->bufindex == adap->buflen) {
			adap->bufindex = -1;	/* Mark we're done	*/
			/* If we want to stop, do it			*/
			if(adap->stop_when_done) {
				i2c_stop();
			}
			return XMIT_BUF_EMPTY;
		}
		/* Send next byte					*/
		i2c_mpc824x_setbits(I2C_MPC824X_DR_DATA,
			adap->buffer[adap->bufindex]);
		/* Increment index					*/
		adap->bufindex++;
		return SUCCESS;
	}
	return XMIT_BUF_EMPTY;
}

/* Receive next byte into buffer : return 1 if full, 0 if byte read,
 * -1 if error								*/
 static i2c_mpc824x_status i2c_recv_next_byte(struct
 	i2c_algo_mpc824x_data *adap)
 {
 	DEB(2, "i2c_recv_next_byte()\n");
 	/* If we're still receiving bytes				*/
	if(adap->bufindex >= 0) {
		/* If we're up to the next to last byte, we need to
		 * set the TXAK bit to tell the sender to stop		*/
		if((adap->buflen - adap->bufindex) == 2) {
			i2c_mpc824x_setbits(I2C_MPC824X_CR_TXAK, 1);
		}
		/* If we're about to receive the last one, and we want
		 * to stop after it, do so				*/
		if(((adap->buflen - adap->bufindex) == 1) &&
			adap->stop_when_done) {
			i2c_stop();
		}
		/* Now, read the data register				*/
		adap->buffer[adap->bufindex] = (u8)i2c_mpc824x_getbits(
			I2C_MPC824X_DR_DATA);
		adap->bufindex++;
		/* If that was the last one, return appropriately	*/
		if(adap->bufindex == adap->buflen) {
			adap->bufindex = -1;
			return RECV_BUF_FULL;
		}
		return SUCCESS;
	}
	return RECV_BUF_FULL;
}

/* Master ISR for I2C state machine					*/
static i2c_mpc824x_status i2c_isr(struct i2c_algo_mpc824x_data *adap)
{
	DEB(3, "i2c_isr: i2ccr=0x%.4x, i2csr=0x%.4x\n",
		i2c_mpc824x_getbits(I2C_MPC824X_CR),
		i2c_mpc824x_getbits(I2C_MPC824X_SR));
	/* Fist, clear the MIF flag					*/
	i2c_mpc824x_setbits(I2C_MPC824X_SR_MIF, 0);
	/* If we've completed a byte transfer				*/
	if(i2c_mpc824x_getbits(I2C_MPC824X_SR_MCF)) {
		/* If we're in master mode				*/
		if(i2c_mpc824x_getbits(I2C_MPC824X_CR_MSTA)) {
			/* If transmit mode				*/
			if(i2c_mpc824x_getbits(I2C_MPC824X_CR_MTX)) {
				/* If receive address mode		*/
				if(adap->is_rcv_addr) {
					/* Switch to receiving		*/
					i2c_mpc824x_setbits(
						I2C_MPC824X_CR_MTX, 0);
					/* If only getting 1 byte, and
					 * want to stop after: set ak	*/
					if((adap->buflen == 1) &&
						(adap->stop_when_done)) {
						i2c_mpc824x_setbits(
							I2C_MPC824X_CR_TXAK, 1);
					}
					/* Do fake read first		*/
					i2c_mpc824x_getbits(
						I2C_MPC824X_DR_DATA);
					adap->is_rcv_addr = 0;
					/* Return (in address state)	*/
					return ADDRESS_SENT;
				}
				/* Master xmit : has slave acknowledged */
				if(i2c_mpc824x_getbits(
					I2C_MPC824X_SR_RXAK) == 0) {
					/* If so, send next byte	*/
					return i2c_xmit_next_byte(adap);
				}
	DEB(1, "i2c_isr: i2ccr=0x%.4x, i2csr=0x%.4x\n",
		i2c_mpc824x_getbits(I2C_MPC824X_CR),
		i2c_mpc824x_getbits(I2C_MPC824X_SR));
				/* Else, no ACK : do stop if needed	*/
				if(adap->stop_when_done) {
					i2c_stop();
				}
				return SUCCESS;
			}
			else {	/* Else, we're receiving		*/
				return i2c_recv_next_byte(adap);
			}
		}
		else {	/* Else, slave mode				*/
			/* We don't do this....shouldn't happen		*/
			printk("i2c-algo-mpc824x: slave mode - not supported!!!\n");
			return ERROR;
		}
	}
	/* Else if received call from master - not supported		*/
	else if(i2c_mpc824x_getbits(I2C_MPC824X_SR_MAAS)) {
		printk("i2c-algo-mpc824x: received call from master - "
			"slave mode not supported!!!\n");
		return ERROR;
	}
	/* Else, has to be arbitration lost				*/
	else {
		/* Clear MAL flag					*/
		i2c_mpc824x_setbits(I2C_MPC824X_SR_MAL, 0);
		/* Return to receive mode				*/
		i2c_stop();
	}
	return SUCCESS;
}

/* I2C timer function : used if periodic polling is used instead if ints*/
static i2c_mpc824x_status i2c_timer_event(struct
	i2c_algo_mpc824x_data *adap)
{
	/* If interrupt flag set, call ISR				*/
	if(i2c_mpc824x_getbits(I2C_MPC824X_SR_MIF)) {
		//printk("MIF\n");
		return i2c_isr(adap);
	}
	else {	/* Else, no event					*/
		//printk("NO_EVENT\n");
		return NO_EVENT;
	}
}

/* Function to initialize a xmit/recv operation				*/
static i2c_mpc824x_status i2c_init_xmit_recv(struct
	i2c_algo_mpc824x_data *adap, int address, u8 *buffer,
	int buflen, int stop_when_done, int is_restart, int is_xmit)
{
	if((is_xmit == 0) && ((buffer == NULL) || (buflen == 0))) {
		DEB(1, "i2c-algo-mpc824x: zero length recv unsupported!\n");
		return ERROR;
	}
	/* Initialize all state variables for transfer			*/
	adap->buffer = buffer;
	adap->buflen = buflen;
	adap->bufindex = 0;
	adap->slaveaddr = address;
	adap->is_xmit = is_xmit;
	adap->stop_when_done = stop_when_done;
	adap->is_restart = is_restart;
	adap->is_rcv_addr = 0;
	/* Now, start the transfer					*/
	return i2c_start(adap);
}

#ifndef USE_ASYNC_FOR_XFER
/* Function to do polling mode processing of one buffer			*/
static i2c_mpc824x_status i2c_do_buffer(struct i2c_algo_mpc824x_data *
	adap, int do_xmit, int address, u8 *buffer, int buflen,
	int stop_when_done, int do_restart)
{
	i2c_mpc824x_status rc;
	u8 minbuf[1];
	/* Initialize the transfer itself				*/
	if((do_xmit == 0) && (buflen == 0)) {
		buffer = minbuf;
		buflen = 1;
	}
	rc = i2c_init_xmit_recv(adap, address, buffer, buflen,
		stop_when_done, do_restart, do_xmit);
	/* If not started successfully, we're done			*/
	if(rc != SUCCESS) {
		DEB(1, "i2c-algo-mpc824x: start failed: i2ccr=0x%.4x, "
			"i2csr=0%.4x\n", i2c_mpc824x_getbits(
			I2C_MPC824X_CR), i2c_mpc824x_getbits(
			I2C_MPC824X_SR));
		i2c_stop();
		return ERROR;
	}
	/* Now, wait for it to get done					*/
	while((rc == SUCCESS) || (rc == ADDRESS_SENT)) {
		unsigned long wait_time = jiffies + 2*HZ;
		unsigned long wait_cnt = 0;

		/* Poll the device until something happens		*/
		do {
			rc = i2c_timer_event(adap);
			wait_cnt++;
		} while ((rc == NO_EVENT) && (jiffies < wait_time) &&
			(wait_cnt < 1000000));
		/* If error, quit					*/
		if(rc < SUCCESS) {
			DEB(1, "i2c-algo-mpc824x: do_buffer failed(%d) - "
				"i2ccr=0x%.4x, i2csr=0x%.4x\n", rc,
				i2c_mpc824x_getbits(
				I2C_MPC824X_CR), i2c_mpc824x_getbits(
				I2C_MPC824X_SR));
			return ERROR;
		}
	}
	return SUCCESS;
}
#endif

/* IRQ function : used for timer and for hw irq				*/
static void i2c_mpc824x_irq_func(unsigned long data)
{
	int rc = 0;
	int cur_cmd_done = 0;
	int cur_req_done = 0;
	unsigned long flags;
	struct i2c_msg *pmsg;
	static u8 minbuf[1];
	int len;
	u8 *buf;
	struct i2c_mpc824x_xfer_req *req;

	req = req_head;
	/* If active request, process it			*/
	if(req) {
		/* If buffer is in progress			*/
		if(req->cur_buf_num >= 0) {
			/* Process tick to advance it		*/
			rc = i2c_timer_event(
				req->i2c_adap->algo_data);
			/* If nothing happened, consider timeout*/
			if(rc == NO_EVENT) {
				/* If we're out of time		*/
				if(jiffies >
					req->cur_wait_time) {
					rc = -EREMOTEIO;
					cur_req_done = 1;
				}
			}
			/* If error, we're done			*/
			else if(rc < SUCCESS) {
				rc = -EREMOTEIO;
				cur_req_done = 1;
			}
			/* Else, reset wait timer		*/
			else if((rc == SUCCESS) || (rc == ADDRESS_SENT)) {
				req->cur_wait_time =
					jiffies + 2*HZ;
			}
			/* Else, we're done!			*/
			else {
				rc = 0;
				cur_cmd_done = 1;
			}
		}
		else {	/* If not running yet, we need to start */
			cur_cmd_done = 1;
		}
		/* If we finished one (or need to start)	*/
		if(cur_cmd_done) {
			req->cur_buf_num++;
			/* See if there are more buffers	*/
			if(req->cur_buf_num >= req->num) {
				/* If not, we're done		*/
				cur_req_done = 1;
				/* Set return code		*/
				rc = req->num;
			}
			/* Else, start next one			*/
			else {
				pmsg = &(req->msgs[
					req->cur_buf_num]);

				if(pmsg->flags & I2C_M_RD)
					req->cur_do_xmit = 0;
				else
					req->cur_do_xmit = 1;
				len = pmsg->len;
				buf = pmsg->buf;
				/* Make sure its minimum read   */
				if((req->cur_do_xmit == 0) &&
					(len == 0)) {
					buf = minbuf;
					len = 1;
				}
				/* Start the new request	*/
				rc = i2c_init_xmit_recv(
					req->i2c_adap->algo_data,
					pmsg->addr, buf, len,
					(req->cur_buf_num ==
					 (req->num - 1)),
					req->cur_buf_num > 0,
					req->cur_do_xmit);
				if(rc != SUCCESS) {
					rc = -EREMOTEIO;
					cur_req_done = 1;
					printk(KERN_ERR
						"i2c-algo-mpc824x: xfer init failed\n");
				}
			}
		}
		/* If the request is complete			*/
		if(cur_req_done) {
			/* If not, request is complete		*/
			if(req->callback) {
				/* Do callback			*/
				req->callback(req->i2c_adap,
					req->msgs, req->num,
					rc, req->usr_data);
			}
			/* Dequueue from list		        */
			spin_lock_irqsave(&lock, flags);
			req_head = req->next;
			if(req_head == NULL) req_tail = NULL;
			spin_unlock_irqrestore(&lock, flags);
			/* Free the record			*/
			kfree(req);
		}
	}
}

/* Timer function : called every tick when active	 	    */
static void i2c_mpc824x_timer_func(unsigned long data)
{
	unsigned long flags;

	spin_lock_irqsave(&lock, flags);
	timer_added = 0;
	spin_unlock_irqrestore(&lock, flags);

	//printk("timer\n");

	i2c_mpc824x_irq_func(data);

	spin_lock_irqsave(&lock, flags);
	/* Requeue the timer function IF a request is active    */
	if(req_head && (!timer_added)) {
		our_timer.expires = jiffies + TIMER_PERIOD;
		our_timer.function = i2c_mpc824x_timer_func;
		add_timer(&our_timer);
		timer_added = 1;
	}
	spin_unlock_irqrestore(&lock, flags);
}

static irqreturn_t i2c_mpc824x_interrupt_func(int this_irq, void *dev_id)
{
	if(i2c_mpc824x_getbits(I2C_MPC824X_SR_MIF)) {
		//printk("irq:MIF\n");
		i2c_mpc824x_irq_func(0);
		i2c_mpc824x_setbits(I2C_MPC824X_SR_MIF, 0);
	}
	else {
		//printk("irq:!MIF\n");
	}
	return IRQ_HANDLED;
}

/* Submit a non-blocking request to issue I2C commands			*/
int i2c_mpc824x_xfer_async(struct i2c_adapter *i2c_adap,
	struct i2c_msg *msgs, int num, void (*callback)
	(struct i2c_adapter *, struct i2c_msg *, int num, int rc,
	void *usr_data), void *usr_data)
{
	unsigned long flags;
	struct i2c_mpc824x_xfer_req *req;
	int did_first_add = 0;

	/* Allocate request block					*/
	req = kmalloc(sizeof(*req), GFP_ATOMIC);
	if(!req) {
		printk("i2c_algo_mpc824x: kmalloc failed!\n");
		return -ENOMEM;
	}
	req->i2c_adap = i2c_adap;
	req->msgs = &msgs[0];
	req->num = num;
	req->usr_data = usr_data;
	req->callback = callback;
	req->cur_buf_num = -1;
	req->cur_do_xmit = 0;
	req->next = NULL;
	req->cur_wait_time = 0;
	/* Add to list							*/
	spin_lock_irqsave(&lock, flags);
	if(req_tail == NULL) {	/* Empty list				*/
		req_tail = req_head = req;
		did_first_add = 1;
	}
	else {
		req_tail->next = req;
		req_tail = req;
	}
	/* If first on list, start timer back up			*/
	if(did_first_add && (!timer_added)) {
		our_timer.expires = jiffies;
		our_timer.function = i2c_mpc824x_timer_func;
		timer_added = 1;
		add_timer(&our_timer);
	}
	spin_unlock_irqrestore(&lock, flags);

	return 0;
}

#ifndef USE_ASYNC_FOR_XFER
static int i2c_mpc824x_xfer_sync(struct i2c_adapter *i2c_adap,
	struct i2c_msg *msgs, int num)
{
	struct i2c_msg *pmsg;
	struct i2c_algo_mpc824x_data *adap = i2c_adap->algo_data;
	int i;

	for (i=0;i<num;i++) {
		pmsg = &msgs[i];	/* Get pointer to message	*/
		/* If message is not requesting a no-start		*/
		if (!(pmsg->flags & I2C_M_NOSTART)) {
			i2c_mpc824x_status rc;
			int do_xmit = 1;

			if(pmsg->flags & I2C_M_RD)
				do_xmit = 0;

			rc = i2c_do_buffer(adap, do_xmit,
				pmsg->addr, pmsg->buf ,pmsg->len,
				(i == (num-1)),
				(i > 0));
			if (rc < SUCCESS) {
				DEB(1, "i2c-algo-mpc824x.o:"
					"error(%d) from device adr %#2x "
					"msg #%d\n", rc, msgs[i].addr,
					i);
				return -EREMOTEIO;
			}
		}
	}
	return num;
}
#endif

#ifdef USE_ASYNC_FOR_XFER
struct i2c_wait_rslt {
	wait_queue_head_t update_wait_q;
	int rc;
};

static void i2c_mpc824x_xfer_callback(struct i2c_adapter *adap,
	struct i2c_msg *msgs, int num, int rc, void *usr_data)
{
	struct i2c_wait_rslt *rslt = (struct i2c_wait_rslt *)usr_data;
	rslt->rc = rc;
	wake_up(&(rslt->update_wait_q));
}
#endif


static int i2c_mpc824x_xfer(struct i2c_adapter *i2c_adap,
	struct i2c_msg msgs[], int num)
{
	int rc;
#ifdef USE_ASYNC_FOR_XFER
	struct i2c_wait_rslt rslt;

	init_waitqueue_head(&(rslt.update_wait_q));
	rslt.rc = -EREMOTEIO;
	rc = i2c_mpc824x_xfer_async(i2c_adap, msgs, num,
		i2c_mpc824x_xfer_callback, &rslt);
	if(rc == 0) {
		sleep_on(&(rslt.update_wait_q));
		rc = rslt.rc;
	}
#else
	rc = i2c_mpc824x_xfer_sync(i2c_adap, &msgs[0], num);
#endif
	//printk("i2c_mpc824x_xfer() - rc=%d\n", rc);
	return rc;
}

static int algo_control(struct i2c_adapter *adapter,
	unsigned int cmd, unsigned long arg)
{
	return 0;
}

static u32 i2c_mpc824x_func(struct i2c_adapter *adap)
{
	return I2C_FUNC_SMBUS_EMUL |
	       I2C_FUNC_PROTOCOL_MANGLING;
}

#ifdef CONFIG_RAPTOR
/* Special entry point used for I2C commands issued for system hard reset
 * from raptor_setup.c - needs to be interrupt time callable		*/
void i2c_mpc824x_do_cmds(void *eumb_addr, struct i2c_msg msgs[],
	int num)
{
	struct i2c_algo_mpc824x_data algo;
	struct i2c_adapter adap;
	unsigned long flags;
	int was_up = 0;

	/* Make bogus adapter structure ... enough to work with calls	*/
	memset(&adap, 0, sizeof(adap));
	adap.algo_data = &algo;
	memset(&algo, 0, sizeof(algo));

	eumb_base = (u32 *)((unsigned long)eumb_addr +
		I2C_REGISTER_BASE_OFFSET);

	if(!did_init)	/* If not initialized, init the lock		*/
		lock = (spinlock_t)SPIN_LOCK_UNLOCKED;

	/* Disable interrupts if we're initialized			*/
	spin_lock_irqsave(&lock, flags);

	/* Now do a stop (if we were already active)			*/
	if(i2c_mpc824x_getbits(I2C_MPC824X_CR_MEN)) {
		i2c_stop();
		i2c_term();
		was_up = 1;
	}
	/* Reinitialize if needed					*/
	i2c_init();
	/* Now send the messages					*/
	i2c_mpc824x_xfer_sync(&adap, msgs, num);
	/* Now terminate if we weren't active before			*/
	if(was_up == 0) {
		i2c_term();
	}
	/* Drop lock							*/
	spin_unlock_irqrestore(&lock, flags);
}

/* Special entry point used for I2C commands issued for modules		*/
void i2c_mpc824x_do_cmds_async(struct i2c_msg *msgs,
	int num, void (*callback)(struct i2c_adapter *, struct i2c_msg *, 
	int num, int rc, void *usr_data), void *usr_data)
{
	if(mpc_adap) {
		i2c_mpc824x_xfer_async(mpc_adap, msgs, num,
			callback, usr_data);
	}
	else {
		callback(NULL, msgs, num, -1, usr_data);
	}
}

#endif
/* -----exported algorithm data: -------------------------------------	*/

static struct i2c_algorithm i2c_bit_algo = {
        .master_xfer    = i2c_mpc824x_xfer,
        .algo_control   = algo_control, /* ioctl */
        .functionality  = i2c_mpc824x_func,
};

/*
 * registering functions to load algorithms at runtime
 */
int i2c_mpc824x_add_bus(struct i2c_adapter *i2c_adap, struct pci_dev *dev)
{
	DEB(2, "i2c-algo-mpc824x.o: hw routines for %s registered.\n",
	            i2c_adap->name);

	/* Initialize the hardware				*/
	i2c_init();
	init_timer(&our_timer);

	/* register new adapter to i2c module... */
	i2c_adap->algo = &i2c_bit_algo;

	i2c_adap->timeout = 100;/* default values, should	*/
	i2c_adap->retries = 3;	/* be replaced by defines	*/


	i2c_adap->dev.parent = &dev->dev;
	i2c_add_adapter(i2c_adap);

	mpc_adap = i2c_adap;
 
	printk(KERN_INFO "i2c-algo-mpc824x.o: i2c mpc824x algorithm module version %s\n", DRV_VERSION);

	did_init = 1;

	lock = (spinlock_t)SPIN_LOCK_UNLOCKED;

#ifdef USE_HW_INT_FOR_XFER
	if (request_irq(HW_INT, i2c_mpc824x_interrupt_func, SA_INTERRUPT, "i2c mpc824x", NULL) < 0) {
		printk(KERN_ERR "i2c-algo-mpc824x.o: error registering irq \n");
		return -ENODEV;
	}
#endif
	return 0;
}

int i2c_mpc824x_del_bus(struct i2c_adapter *adap)
{
	int res;

	if ((res = i2c_del_adapter(adap)) < 0) {
		return res;
	}

	DEB(2, "i2c-algo-mpc824x.o: adapter unregistered: %s\n",adap->name);

	i2c_term();		/* Shutdown hardware		 */

	mpc_adap = NULL;
 
#ifdef USE_HW_INT_FOR_XFER
	free_irq(HW_INT, NULL);
#endif
	return 0;
}

EXPORT_SYMBOL(i2c_mpc824x_add_bus);
EXPORT_SYMBOL(i2c_mpc824x_del_bus);
#ifdef CONFIG_RAPTOR
EXPORT_SYMBOL(i2c_mpc824x_do_cmds);
EXPORT_SYMBOL(i2c_mpc824x_do_cmds_async);
#endif


MODULE_AUTHOR("Michael Primm <primm@netbotz.com>");
MODULE_DESCRIPTION("I2C-Bus mpc824x algorithm");
MODULE_LICENSE("GPL");

module_param(debug_level,int,0);
MODULE_PARM_DESC(debug_level,
            "debug level - 0 off; 1 normal; 2,3 more verbose");

