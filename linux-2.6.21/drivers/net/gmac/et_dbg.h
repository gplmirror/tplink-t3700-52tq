/*
 * Minimal debug/trace/assert driver definitions for
 * Broadcom Home Networking Division 10/100 Mbit/s Ethernet
 * Device Driver.
 *
 * $Copyright (C) 2003 Broadcom Corporation$
 * $Id: et_dbg.h,v 1.1 2009/01/06 06:13:03 ako Exp $
 */

#ifndef _et_dbg_
#define _et_dbg_

struct ether_header;
extern void etc_prhdr(char *msg, struct ether_header *eh, uint len, int unit);
extern void etc_prhex(char *msg, uchar *buf, uint nbytes, int unit);

#define	ET_ERROR(args) 
#define	ET_TRACE(args) 
#define	ET_PRHDR(msg, eh, len, unit) 
#define	ET_PRPKT(msg, buf, len, unit) 

extern uint32 et_msg_level;

#define	ET_LOG(fmt, a1, a2)

#include <et_linux.h>

#endif /* _et_dbg_ */
