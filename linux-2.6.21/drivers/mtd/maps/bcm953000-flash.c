/*
 * Flash mapping for BCM953000 boards
 *
 * $Copyright Open Broadcom Corporation$
 *
 * $Id: bcm953000-flash.c,v 1.4 2009/03/26 10:47:55 cchao Exp $
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <asm/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>

#include <asm/bcmsi/typedefs.h>
#include <asm/bcmsi/bcmnvram.h>
#include <asm/bcmsi/bcmutils.h>
#include <asm/bcmsi/hndsoc.h>
#include <asm/bcmsi/sbchipc.h>
#include <asm/bcmsi/siutils.h>
#include <asm/bcmsi/bcmendian.h>

/* Global SI handle */
extern void *bcm53000_sih;
extern spinlock_t bcm53000_sih_lock;

/* Convenience */
#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock

#ifdef CONFIG_MTD_PARTITIONS

#define PTABLE_MAGIC		0x5054424C /* 'PTBL' (big-endian) */
#define PTABLE_MAGIC_LE		0x4C425450 /* 'PTBL' (little-endian) */
#define PTABLE_PNAME_MAX	16
#define PTABLE_MAX_PARTITIONS	8
#define FLASH_BOOT_OFFSET 0x0
#define BCM953000_DEFAULT_PARTS_COUNT (sizeof(bcm953000_default_parts)/sizeof(struct mtd_partition))

#define KB                   1024
#define MB                   (1024 * 1024)
#define MAX_FLASH_SIZE       (128 * MB)
#define BOOTLOADER_SIZE      (1 * MB)
#define NVRAM_SIZE           (128 * KB) 
#define UIMAGE_SIZE          (3 * MB)
#define FP_VXWORK_SIZE       (62 * MB)
#define VXBOOT_SIZE          (1 * MB)

typedef struct partition_s {
	char		name[PTABLE_PNAME_MAX];
	uint32_t	offset;
	uint32_t	size;
	uint32_t	type;
	uint32_t	flags;
} partition_t;

typedef struct ptable_s {
	uint32_t	magic;
	uint32_t	version;
	uint32_t	chksum;
	uint32_t	reserved;
	partition_t part[PTABLE_MAX_PARTITIONS];
} ptable_t;

static struct mtd_partition bcm953000_mtd_ptable_parts[PTABLE_MAX_PARTITIONS];
static ptable_t ptable;
static int num_parts;

static struct mtd_partition bcm953000_default_parts[] = {
    { name: "cfe", offset: 0, size: BOOTLOADER_SIZE, mask_flags: MTD_WRITEABLE, },
    { name: "nvram", offset: MTDPART_OFS_NXTBLK, size: NVRAM_SIZE,  },
    { name: "vxboot", offset: MTDPART_OFS_NXTBLK, size: VXBOOT_SIZE, },	
    { name: "vxworks1", offset: MTDPART_OFS_NXTBLK, size: FP_VXWORK_SIZE, },	
    { name: "vxworks2", offset: MTDPART_OFS_NXTBLK, size: FP_VXWORK_SIZE, },	
    { name: "lboot", offset: MTDPART_OFS_NXTBLK, size: UIMAGE_SIZE, },	
    { name: "Linux", offset: MTDPART_OFS_NXTBLK, size: MTDPART_SIZ_FULL, },
    { name: NULL, },
};

static int bcm53000_check_ptable(ptable_t *ptbl,struct mtd_partition *part, ssize_t max_parts, ssize_t *num_parts)
{
  uint32_t chksum, *p32;
  int i, swapped = 0;

  if (ptbl->magic == PTABLE_MAGIC_LE)
    swapped = 1;
  else if (ptbl->magic != PTABLE_MAGIC)
  {
    return -1;
  }
  
  chksum = 0;
  p32 = (uint32_t*)ptbl;

  for (i = 0; i < sizeof(ptable_t)/4; i++) 
  {
    chksum ^= p32[i];
    if (swapped)
      p32[i] = swab32(p32[i]);
  }

  if (chksum != 0)
  {
    return -1;
  }
  for (i = 0; i < max_parts && ptbl->part[i].size; i++) 
  {
    part[i].name = ptbl->part[i].name;
    part[i].size = ptbl->part[i].size;
    part[i].offset = ptbl->part[i].offset;
  }
  *num_parts = i;
  return 0;
}

static int bcm953000_find_partitions(struct mtd_info *mi, 
				     struct mtd_partition *part, 
				     ssize_t max_parts, ssize_t *num_parts)
{
  int retlen, boot_offset;

  if (mi == NULL || mi->read == NULL)
    return -1;

  for(boot_offset = FLASH_BOOT_OFFSET; boot_offset < mi->size;boot_offset+=0x8000)
  {	
    if (mi->read(mi, boot_offset, sizeof(ptable), &retlen, 
      (u_char *)&ptable) < 0) 
    {
      return -1;
    }

    if (retlen != sizeof(ptable))
    {
      return -1;
    }
    if (bcm53000_check_ptable(&ptable, part, max_parts, num_parts) == 0) 
    {
      if (*num_parts > 0) 
      {
        printk("Found flash partition table at offset 0x%08x\n", boot_offset);
        return 0;
      }
    }
  }
	
  printk("No valid flash partition table found - using default mapping\n");
  return -1;
}

#endif
struct mtd_partition * __init
init_mtd_partitions(struct mtd_info *mtd, size_t size)
{
    return bcm953000_default_parts;
}

#if 0
EXPORT_SYMBOL(init_mtd_partitions);


#endif

#define WINDOW_ADDR 0x1fc00000
#define WINDOW_SIZE 0x400000
#define BANKWIDTH 2

static struct mtd_info *bcm953000_mtd;

static map_word bcm953000_map_read(struct map_info *map, unsigned long offs)
{
  map_word r;
    
  if (map_bankwidth_is_1(map)) {
    u16 val;
    if (map->map_priv_2 == 1)
      r.x[0] = readb((void *) (map->map_priv_1 + offs));
    else {
       val = readw((void *) (map->map_priv_1 + (offs & ~1)));
       if (offs & 1)
         r.x[0] = ((val >> 8) & 0xff);
       else
         r.x[0] = (val & 0xff);
    }
  }else if (map_bankwidth_is_2(map)){
     r.x[0] = ltoh16(readw((void *) (map->map_priv_1 + offs)));
  }
  else if (map_bankwidth_is_4(map))
    r.x[0] = ltoh32(readl((void *) (map->map_priv_1 + offs)));
  else if (map_bankwidth_is_large(map))
    memcpy_fromio(r.x, (void *)(map->map_priv_1) + offs, map->bankwidth);
  return r;
}

static void bcm953000_map_copy_from(struct map_info *map, void *to, unsigned long from, ssize_t len)
{
  from += map->map_priv_1;
  while (len --)
    *(char*)to ++ = readb((void *)(from ++));
}

static void bcm953000_map_write(struct map_info *map, const map_word datum,
			       unsigned long offs)
{
    if (map_bankwidth_is_1(map))
        writeb(datum.x[0], (void *) (map->map_priv_1 + offs));
    else if (map_bankwidth_is_2(map)){
    #if 1
    writew(datum.x[0], (void *) (map->map_priv_1 + offs));
    #else
			writew(ltoh16(datum.x[0]), (void *) (map->map_priv_1 + offs));
		#endif	
    }
    else if (map_bankwidth_is_4(map)){
     writel(datum.x[0], (void *) (map->map_priv_1 + offs));
    }
    else if (map_bankwidth_is_large(map))
        memcpy_toio((void *)(map->map_priv_1 + offs), (void *) datum.x[0],
            map->bankwidth);
    mb();
}

static void bcm953000_map_copy_to(struct map_info *map, unsigned long to, const void *from, ssize_t len)
{
  to += map->map_priv_1;
   while (len --)
     writeb(*(char*)from ++, (void *)(to ++));
}

struct map_info bcm953000_map = {
    .name = "Physically mapped flash",
    .size = WINDOW_SIZE,
    .bankwidth = BANKWIDTH,
    .read = bcm953000_map_read,
    .write = bcm953000_map_write,
    .copy_from = bcm953000_map_copy_from,
    .copy_to = bcm953000_map_copy_to
};

#if LINUX_VERSION_CODE < 0x20212 && defined(MODULE)
#define init_bcm953000_map init_module
#define cleanup_bcm953000_map cleanup_module
#endif

int __init init_bcm953000_map(void)
{
    ulong flags;
    uint coreidx;
    chipcregs_t *cc;
    uint32 fltype = 0;
    uint window_addr = 0, window_size = 0;
    size_t size;
    int ret = 0;
   
    spin_lock_irqsave(&sih_lock, flags);
    coreidx = si_coreidx(sih);
    /* Check strapping option if chipcommon exists */
    if ((cc = si_setcoreidx(sih, SI_CC_IDX))) {
        fltype = readl(&cc->capabilities) & CC_CAP_FLASH_MASK;
         if (fltype == PFLASH) {
             /* map_priv_2 indicates no workaround needed */
             bcm953000_map.map_priv_2 = 1;
             window_addr = 0x20000000;
             bcm953000_map.size = window_size = MAX_FLASH_SIZE;
             if ((readl(&cc->pflash_config) & CC_CFG_DS) == 0)
                 bcm953000_map.bankwidth = 1;
        }
    } else {
        fltype = PFLASH;
        bcm953000_map.map_priv_2 = 0;
        window_addr = WINDOW_ADDR;
        window_size = WINDOW_SIZE;
    }
    si_setcoreidx(sih, coreidx);
    spin_unlock_irqrestore(&sih_lock, flags);
    
    if (fltype != PFLASH) {
        printk(KERN_ERR "pflash: found no supported devices\n");
        ret = -ENODEV;
        goto fail;
    }
    bcm953000_map.map_priv_1 = (unsigned long) ioremap(window_addr, window_size);
    if (!bcm953000_map.map_priv_1) {
        printk(KERN_ERR "pflash: ioremap failed\n");
        ret = -EIO;
        goto fail;
    }
    
    if (!(bcm953000_mtd = do_map_probe("cfi_probe", &bcm953000_map))) {
        printk(KERN_ERR "pflash: cfi_probe failed\n");
        ret = -ENXIO;
        goto fail;
    }
    
    bcm953000_mtd->owner = THIS_MODULE;
    
    size = bcm953000_mtd->size;
    
    printk(KERN_NOTICE "Flash device: 0x%x at 0x%x\n", size, window_addr);
    
#ifdef CONFIG_MTD_PARTITIONS
    if (bcm953000_find_partitions(bcm953000_mtd, bcm953000_mtd_ptable_parts, 
				      PTABLE_MAX_PARTITIONS, &num_parts) == 0) 
    {
        ret = add_mtd_partitions(bcm953000_mtd, bcm953000_mtd_ptable_parts, num_parts);
    }
    else 
    {
      ret = add_mtd_partitions(bcm953000_mtd, bcm953000_default_parts, BCM953000_DEFAULT_PARTS_COUNT);
    }
    if (ret) {
        printk(KERN_ERR "pflash: add_mtd_partitions failed\n");
        goto fail;
    }
#endif
    
    return 0;
    
fail:
    if (bcm953000_mtd)
        map_destroy(bcm953000_mtd);
    if (bcm953000_map.map_priv_1)
        iounmap((void *) bcm953000_map.map_priv_1);
    bcm953000_map.map_priv_1 = 0;
    return ret;
}

static void __exit cleanup_bcm953000_map(void)
{
#ifdef CONFIG_MTD_PARTITIONS
    del_mtd_partitions(bcm953000_mtd);
#endif
    map_destroy(bcm953000_mtd);
    iounmap((void *) bcm953000_map.map_priv_1);
    bcm953000_map.map_priv_1 = 0;
}

module_init(init_bcm953000_map);
module_exit(cleanup_bcm953000_map);
