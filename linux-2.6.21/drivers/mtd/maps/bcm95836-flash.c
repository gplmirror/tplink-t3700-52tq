/*
 * Flash mapping for BCM95836 SDK boards
 *
 * Copyright (C) 2004 Broadcom Corporation
 * Copyright (C) 2007 Wind River Systems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/config.h>

#include <asm/brcm/typedefs.h>
#include <asm/brcm/bcmnvram.h>
#include <asm/brcm/bcmutils.h>
#include <asm/brcm/sbconfig.h>
#include <asm/brcm/sbchipc.h>
#include <asm/brcm/sbutils.h>

/* Global SB handle */
extern void *bcm947xx_sbh;
extern spinlock_t bcm947xx_sbh_lock;

/* Convenience */
#define sbh bcm947xx_sbh
#define sbh_lock bcm947xx_sbh_lock

#define WINDOW_ADDR 0x1fc00000
#define WINDOW_SIZE 0x400000
#define BUSWIDTH 2

static struct mtd_info *bcm95836_mtd;

static map_word bcm95836_map_read(struct map_info *map, unsigned long offs)
{
	map_word r;

	if (map_bankwidth_is_1(map)) {
		u16 val;
		if (map->map_priv_2 == 1)
			r.x[0] = readb((void *) (map->map_priv_1 + offs));
		else {
			val =
			    readw((void *) (map->map_priv_1 +
					    (offs & ~1)));
			if (offs & 1)
				r.x[0] = ((val >> 8) & 0xff);
			else
				r.x[0] = (val & 0xff);
		}
	} else if (map_bankwidth_is_2(map))
		r.x[0] = readw((void *) (map->map_priv_1 + offs));
	else if (map_bankwidth_is_4(map))
		r.x[0] = readl((void *) (map->map_priv_1 + offs));
	else if (map_bankwidth_is_large(map))
		memcpy_fromio(r.x, map->map_priv_1 + offs, map->bankwidth);
	return r;
}

static void bcm95836_map_copy_from(struct map_info *map, void *to, unsigned long from, ssize_t len)
{
	from += map->map_priv_1;
	while (len --)
		*(char*)to ++ = readb((void *)(from ++));
}

static void bcm95836_map_write(struct map_info *map, const map_word datum,
			       unsigned long offs)
{
	if (map_bankwidth_is_1(map))
		writeb(datum.x[0], (void *) (map->map_priv_1 + offs));
	else if (map_bankwidth_is_2(map))
		writew(datum.x[0], (void *) (map->map_priv_1 + offs));
	else if (map_bankwidth_is_4(map))
		writel(datum.x[0], (void *) (map->map_priv_1 + offs));
	else if (map_bankwidth_is_large(map))
		memcpy_toio(map->map_priv_1 + offs, (void *) datum.x[0],
			    map->bankwidth);
	mb();
}

static void bcm95836_map_copy_to(struct map_info *map, unsigned long to, const void *from, ssize_t len)
{
	to += map->map_priv_1;
	while (len --)
		writeb(*(char*)from ++, (void *)(to ++));
}

struct map_info bcm95836_map = {
	.name = "Physically mapped flash",
	.size = WINDOW_SIZE,
	.bankwidth = BUSWIDTH,
	.read = bcm95836_map_read,
	.write = bcm95836_map_write,
	.copy_from = bcm95836_map_copy_from,
	.copy_to = bcm95836_map_copy_to
};

#ifdef CONFIG_MTD_PARTITIONS

/*
 * CFE 1.2.6 SDK flash map for BCM95836:
 *
 * flash1.boot   offset 00000000 size 896 KB
 * flash1.nvram  offset 000E0000 size 128 KB
 * flash1.os     offset 00100000 size   7 MB
 * flash1.user   offset 00800000 size   8 MB
 */

static struct mtd_partition bcm95836_mtd_parts[] __initdata = {
	{
		.name = "boot",
		.size = 0x0e0000,
		.offset = 0x000000,
		.mask_flags = MTD_WRITEABLE
	}, {
		.name = "nvram",
		.size = 0x020000,
		.offset = 0x0e0000
	}, {
		.name = "os",
		.size = 0x700000,
		.offset = 0x100000
	}, {
		.name = "user",
		.size = 0x800000,
		.offset = 0x800000
	}
};

#define BCM95836_PARTS (sizeof(bcm95836_mtd_parts)/sizeof(struct mtd_partition))

/*
 * CFE 1.1.0 SDK flash map for BCM94704:
 *
 * flash2.boot   offset 00000000 size  512 KB
 * flash2.trx    offset 00080000 size 3456 KB
 * flash2.nvram  offset 003E0000 size  128 KB (note: must be whole flash page)
 */

static struct mtd_partition bcm94704_mtd_parts[] __initdata = {
	{
		.name = "cfe",
		.size = 0x080000,
		.offset = 0x000000,
		.mask_flags = MTD_WRITEABLE
	}, {
		.name = "User FS",
		.size = 0x360000,
		.offset = 0x080000
	}, {
		.name = "nvram",
		.size = 0x020000,
		.offset = 0x3e0000
	}
};

#define BCM94704_PARTS (sizeof(bcm94704_mtd_parts)/sizeof(struct mtd_partition))

#define PTABLE_MAGIC            0x5054424C /* 'PTBL' (big-endian) */
#define PTABLE_MAGIC_LE         0x4C425450 /* 'PTBL' (little-endian) */
#define PTABLE_PNAME_MAX        16
#define PTABLE_MAX_PARTITIONS   8

typedef struct partition_s {
    char        name[PTABLE_PNAME_MAX];
    uint32_t    offset;
    uint32_t    size;
    uint32_t    type;
    uint32_t    flags;
} partition_t;

typedef struct ptable_s {
    uint32_t    magic;
    uint32_t    version;
    uint32_t    chksum;
    uint32_t    reserved;
    partition_t part[PTABLE_MAX_PARTITIONS];
} ptable_t;

static struct mtd_partition bcm95836_mtd_user_parts[PTABLE_MAX_PARTITIONS];
static ptable_t ptable;
static int num_parts;

static int bcm95836_check_ptable(ptable_t *ptbl,
                                 struct mtd_partition *part, 
                                 ssize_t max_parts, ssize_t *num_parts)
{
        uint32_t chksum, *p32;
        int i, swapped = 0;

        if (ptbl->magic == PTABLE_MAGIC_LE) {
                swapped = 1;
        }
        else if (ptbl->magic != PTABLE_MAGIC) {
                return -1;
        }
        chksum = 0;
        p32 = (uint32_t*)ptbl;
        for (i = 0; i < sizeof(ptable_t)/4; i++) {
                chksum ^= p32[i];
                if (swapped) {
                        p32[i] = swab32(p32[i]);
                }
        }
        if (chksum != 0) {
                return -1;
        }
        for (i = 0; i < max_parts && ptbl->part[i].size; i++) {
                part[i].name = ptbl->part[i].name;
                part[i].size = ptbl->part[i].size;
                part[i].offset = ptbl->part[i].offset;
        }
        *num_parts = i;
        return 0;
}

static int bcm95836_find_partitions(struct mtd_info *mi, 
                                     struct mtd_partition *part, 
                                     ssize_t max_parts, ssize_t *num_parts)
{
        int retlen, offset;

        if (mi == NULL || mi->read == NULL) {
                return -1;
        }
        for (offset = 0; offset < mi->size; offset += 0x8000) {
                if (mi->read(mi, offset, sizeof(ptable), &retlen, 
                             (u_char *)&ptable) < 0) {
                        return -1;
                }
                if (retlen != sizeof(ptable)) {
                        return -1;
                }
                if (bcm95836_check_ptable(&ptable, part, max_parts, 
                                          num_parts) == 0) {
                        if (*num_parts > 0) {
                                printk("Found flash partition table "
                                       "at offset 0x%08x\n", offset);
                                return 0;
                        }
                }
        }
        printk("No valid flash partition table found - using default mapping\n");
        return -1;
}

#endif

#if LINUX_VERSION_CODE < 0x20212 && defined(MODULE)
#define init_bcm95836_map init_module
#define cleanup_bcm95836_map cleanup_module
#endif

int __init init_bcm95836_map(void)
{
	ulong flags;
 	uint coreidx;
	chipcregs_t *cc;
	uint32 fltype;
	uint window_addr = 0, window_size = 0;
	int ret = 0;

	spin_lock_irqsave(&sbh_lock, flags);
	coreidx = sb_coreidx(sbh);

	/* Check strapping option if Chipcommon exists */
	if ((cc = sb_setcore(sbh, SB_CC, 0))) {
		fltype = readl(&cc->capabilities) & CAP_FLASH_MASK;
		if (fltype == PFLASH) {
			bcm95836_map.map_priv_2 = 1;
			window_addr = 0x1c000000;
			bcm95836_map.size = window_size = 16 * 1024 * 1024;
			if ((readl(&cc->parallelflashconfig) & CC_CFG_DS) == 0)
				bcm95836_map.bankwidth = 1;
		}
	} else {
		fltype = PFLASH;
		bcm95836_map.map_priv_2 = 0;
		window_addr = WINDOW_ADDR;
		window_size = WINDOW_SIZE;
	}

	sb_setcoreidx(sbh, coreidx);
	spin_unlock_irqrestore(&sbh_lock, flags);

	if (fltype != PFLASH) {
		printk(KERN_ERR "pflash: found no supported devices\n");
		ret = -ENODEV;
		goto fail;
	}

	bcm95836_map.map_priv_1 = (unsigned long) ioremap(window_addr, window_size);
	if (!bcm95836_map.map_priv_1) {
		printk(KERN_ERR "pflash: ioremap failed\n");
		ret = -EIO;
		goto fail;
	}

	if (!(bcm95836_mtd = do_map_probe("cfi_probe", &bcm95836_map))) {
		printk(KERN_ERR "pflash: cfi_probe failed\n");
		ret = -ENXIO;
		goto fail;
	}

	bcm95836_mtd->owner = THIS_MODULE;

	printk(KERN_NOTICE "Flash device: 0x%x at 0x%x\n", bcm95836_mtd->size, window_addr);

#ifdef CONFIG_MTD_PARTITIONS
        if (bcm95836_find_partitions(bcm95836_mtd, bcm95836_mtd_user_parts, 
                                     PTABLE_MAX_PARTITIONS, &num_parts) == 0) {
                ret = add_mtd_partitions(bcm95836_mtd, bcm95836_mtd_user_parts, num_parts);
        }
        else if (bcm95836_mtd->size == 0x400000) {
                ret = add_mtd_partitions(bcm95836_mtd, bcm94704_mtd_parts, BCM94704_PARTS);
        }
        else {
                ret = add_mtd_partitions(bcm95836_mtd, bcm95836_mtd_parts, BCM95836_PARTS);
        }
	if (ret) {
		printk(KERN_ERR "pflash: add_mtd_partitions failed\n");
		goto fail;
	}
#endif

	return 0;

 fail:
	if (bcm95836_mtd)
		map_destroy(bcm95836_mtd);
	if (bcm95836_map.map_priv_1)
		iounmap((void *) bcm95836_map.map_priv_1);
	bcm95836_map.map_priv_1 = 0;
	return ret;
}

static void __exit cleanup_bcm95836_map(void)
{
#ifdef CONFIG_MTD_PARTITIONS
	del_mtd_partitions(bcm95836_mtd);
#endif
	map_destroy(bcm95836_mtd);
	iounmap((void *) bcm95836_map.map_priv_1);
	bcm95836_map.map_priv_1 = 0;
}

module_init(init_bcm95836_map);
module_exit(cleanup_bcm95836_map);
