/*
 * drivers/mtd/maps/dni8541.c
 * 
 * Mapping for DNI8541 user flash derived from driver for MPC 85xx ADS driver
 *
 * 2004 (c) MontaVista Software, Inc. This file is licensed under
 * the terms of the GNU General Public License version 2. This program
 * is licensed "as is" without any warranty of any kind, whether express
 * or implied.
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <asm/io.h>

#define FLASH_BASE_ADDR          0xfe000000

#define APP_FS_START             0x00000000
#define APP_FS_LEN               0x01c40000

#define USYSTEM_START            0x01c40000 
#define USYSTEM_LEN              0x00280000

#define VPD_START                0x01ec0000
#define VPD_LEN                  0x00040000

#define RESERVE_START            0x01f00000
#define RESERVE_LEN              0x00040000

#define UBOOT_ENV_START          0x01f40000
#define UBOOT_ENV_LEN            0x00040000

#define UBOOT_START              0x01f80000
#define UBOOT_LEN                0x00080000

static struct mtd_info *flash;

static struct map_info dni8541_map = {
	.name =		"DNI8541 flash",
	.size =		0x2000000,
	.bankwidth =	4,
	.phys = 	FLASH_BASE_ADDR,
};

static struct mtd_partition dni8541_partitions[] = {
	{
		.name =   "application fs",
		.offset = APP_FS_START,
		.size =   APP_FS_LEN,
	},
	{
		.name =   "uSystem",
		.offset = USYSTEM_START,
		.size =   USYSTEM_LEN,
        },
        {
		.name =   "u-boot env",
		.offset = UBOOT_ENV_START,
		.size =   UBOOT_ENV_LEN,
	},
	{
		.name =   "u-boot",
		.offset = UBOOT_START,
		.size =   UBOOT_LEN,
	},
   	{
		.name =   "reserve",
		.offset = RESERVE_START,
		.size =   RESERVE_LEN,
	},
   	{
		.name =   "vpd",
		.offset = VPD_START,
		.size =   VPD_LEN,
	}
};

void print_flash_info(struct mtd_info *flash)
{
  printk("FLASH Info for DNI8541\n");
}

int __init init_dni8541(void)
{
	dni8541_map.virt =
		(unsigned long)ioremap(FLASH_BASE_ADDR, dni8541_map.size);
  
	if (!dni8541_map.virt) {
		printk(KERN_NOTICE "Failed to ioremap flash\n");
		return -EIO;
	}

	simple_map_init(&dni8541_map);

	flash = do_map_probe("cfi_probe", &dni8541_map);
	if (flash) {
		flash->owner = THIS_MODULE;
		add_mtd_partitions(flash, dni8541_partitions,
					ARRAY_SIZE(dni8541_partitions));
	} else {
		printk(KERN_NOTICE "map probe failed for flash\n");
		return -ENXIO;
	}

	return 0;
}

static void __exit cleanup_dni8541(void)
{
	if (flash) {
		del_mtd_partitions(flash);
		map_destroy(flash);
	}

	if (dni8541_map.virt) {
		iounmap((void *)dni8541_map.virt);
		dni8541_map.virt = 0;
	}
}

module_init(init_dni8541);
module_exit(cleanup_dni8541);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("MTD map and partitions for the DNI8541 board.");
