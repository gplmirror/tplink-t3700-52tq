/*
 * Handle mapping of the flash on MPC83xx board
 *
 * Copyright (C) 2005 Freescale semicondutor
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <asm/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>


/* MPC8349MDS and MPC8349EMITX flash layout
 * 0 : 0xFE00 0000 - 0xFE01 FFFF : HRCW (128KB)
 * 1 : 0xFE02 0000 - 0xFE5F FFFF : JFFS2 file system (5.875MB)
 * 2 : 0xFE60 0000 - 0xFE6F FFFF : Kernel (1MB)
 * 3 : 0xFE70 0000 - 0xFE7F FFFF : U-Boot (1MB)
 */
#ifdef CONFIG_MPC834x
#define WINDOW_ADDR 0xFE000000
#define WINDOW_SIZE 0x00800000
#endif

/* MPC8360EPB flash layout
 * 0 : 0xFE00 0000 - 0xFE01 FFFF : HRCW (128KB)
 * 1 : 0xFE02 0000 - 0xFE8F FFFF : JFFS2 file system (8.875MB)
 * 2 : 0xFE90 0000 - 0xFECF FFFF : Ramdisk file system (4MB)
 * 3 : 0xFED0 0000 - 0xFEEF FFFF : Kernel (2MB)
 * 4 : 0xFEF0 0000 - 0xFEFF FFFF : U-Boot (1MB)
 */
#ifdef CONFIG_MPC8360
#define WINDOW_ADDR 0xFE000000
#define WINDOW_SIZE 0x01000000
#endif

/* MPC832XEPB flash layout
 * 0 : 0xFE00 0000 - 0xFE01 FFFF : HRCW (128KB)
 *   : 0xFE02 0000 - 0xFE0F FFFF : Reserved for BIT (896KB)
 * 1 : 0xFE10 0000 - 0xFE8F FFFF : JFFS2 file system (8MB)
 * 2 : 0xFE90 0000 - 0xFECF FFFF : Ramdisk file system (4MB)
 * 3 : 0xFED0 0000 - 0xFEEF FFFF : Kernel (2MB)
 * 4 : 0xFEF0 0000 - 0xFEFF FFFF : U-Boot (1MB)
 */
#ifdef CONFIG_MPC832X
#define WINDOW_ADDR 0xFE000000
#define WINDOW_SIZE 0x01000000
#endif

/* partition_info gives details on the logical partitions that the split the
 * single flash device into. If the size if zero we use up to the end of the
 * device. */
static struct mtd_partition partition_info[]={
	{
		.name		= "HRCW",
		.offset 	= 0,
		.size		= 0x020000,
		.mask_flags	= MTD_WRITEABLE
	},
	{
		.name		= "JFFS2",
#ifdef CONFIG_MPC832X
		.offset         = 0x100000,
#endif
#if defined(CONFIG_MPC834x) || defined(CONFIG_MPC8360)
		.offset         = 0x020000,
#endif
#ifdef CONFIG_MPC834x
		.size		= 0x5E0000
#endif
#ifdef CONFIG_MPC8360
		.size		= 0x8E0000
#endif
#ifdef CONFIG_MPC832X
		.size		= 0x800000
#endif
	},
#ifdef CONFIG_MPC8360
	{
		.name           = "Ramdisk",
		.offset         = MTDPART_OFS_APPEND,
		.size           = 0x400000
	},
#endif
#ifdef CONFIG_MPC832X
	{
		.name           = "Ramdisk",
		.offset         = MTDPART_OFS_APPEND,
		.size           = 0x400000
	},
#endif
	{
		.name		= "Kernel",
		.offset		= MTDPART_OFS_APPEND,
#ifdef CONFIG_MPC834x
		.size		= 0x100000
#endif
#ifdef CONFIG_MPC8360
		.size		= 0x200000
#endif
#ifdef CONFIG_MPC832X
		.size		= 0x200000
#endif
	},
	{
		.name		= "U-Boot",
		.offset		= MTDPART_OFS_APPEND,
		.size		= 0x100000,
		.mask_flags	= MTD_WRITEABLE
    }
};

#define PARTITION_NUM (sizeof(partition_info)/sizeof(struct mtd_partition))

static struct mtd_info *mymtd;


struct map_info mpc83xx_map = {
#ifdef CONFIG_MPC834x
	.name		= "MPC8349MDS Flash Map Info",
#endif
#ifdef CONFIG_MPC8360
	.name		= "MPC8360E PB Flash Map Info",
#endif
#ifdef CONFIG_MPC832X
	.name		= "MPC832XE PB Flash Map Info",
#endif
	.size		= WINDOW_SIZE,
	.phys		= WINDOW_ADDR,
	.bankwidth	= 2,
};

int __init init_mpc83xx(void)
{
	char *board;
#ifdef CONFIG_MPC834x
	board = "MPC8349MDS";
#endif
#ifdef CONFIG_MPC8360
	board = "MPC8360E PB";
#endif
#ifdef CONFIG_MPC832X
	board = "MPC832XE MDS";
#endif
	printk(KERN_NOTICE "%s flash device: %x at %x Partition number %d\n",
			board,WINDOW_SIZE, WINDOW_ADDR, PARTITION_NUM);
	mpc83xx_map.virt = ioremap(WINDOW_ADDR, WINDOW_SIZE);

	if (!mpc83xx_map.virt) {
		printk("Failed to ioremap\n");
		return -EIO;
	}
	simple_map_init(&mpc83xx_map);

	mymtd = do_map_probe("cfi_probe", &mpc83xx_map);
	if (mymtd) {
		mymtd->owner = THIS_MODULE;
                add_mtd_partitions(mymtd, partition_info, PARTITION_NUM);
		printk(KERN_NOTICE "%s flash device initialized\n", board);
		return 0;
	}

	iounmap((void *)mpc83xx_map.virt);
	return -ENXIO;
}

static void __exit cleanup_mpc83xx(void)
{
	if (mymtd) {
		del_mtd_device(mymtd);
		map_destroy(mymtd);
	}
	if (mpc83xx_map.virt) {
		iounmap((void *)mpc83xx_map.virt);
		mpc83xx_map.virt = 0;
	}
}

module_init(init_mpc83xx);
module_exit(cleanup_mpc83xx);

MODULE_AUTHOR("FSL");
MODULE_DESCRIPTION("MTD map driver for Freescale MPC83xx board");
MODULE_LICENSE("GPL");
