/*
 * $Id: bmw.c,v 1.1 2004/05/06 15:15:58 linville Exp $
 *
 * Normal mappings of chips in physical memory
 *
 * Modified for Broadcom BMW partitions... -- JWL
 */

/* partition support */
#define HAVE_PARTITIONS

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/autoconf.h>
#ifdef HAVE_PARTITIONS
#include <linux/mtd/partitions.h>
#endif

#define WINDOW_ADDR	0xffe00000
#define WINDOW_SIZE	0x00200000
#define BUSWIDTH	1

#define SYSTEM_START	0x00000000
#define SYSTEM_LEN	0x00100000

#define PPCBOOT_START	0x00100000
#define PPCBOOT_LEN	0x00030000

#define SYSTEM_2_START	0x00130000
#define SYSTEM_2_LEN	0x000d0000

#ifdef HAVE_PARTITIONS
static struct mtd_partition bmw_partitions[] =
{
	/* system (kernel + ramdisk) */
	{
		      .name = "System (Kernel + Ramdisk)",
		    .offset = SYSTEM_START,
		      .size = SYSTEM_LEN,
		.mask_flags = 0
	},
	/* PPCBoot */
	{
		      .name = "PPCBoot",
		    .offset = PPCBOOT_START,
		      .size = PPCBOOT_LEN,
		.mask_flags = 0
	},
	/* system (part 2) */
	{
		      .name = "System (part 2)",
		    .offset = SYSTEM_2_START,
		      .size = SYSTEM_2_LEN,
		.mask_flags = 0
	}
};

#define NUM_PARTITIONS	(sizeof(bmw_partitions) \
                       / sizeof(struct mtd_partition))
#endif

static struct mtd_info *mymtd;

struct map_info bmw_map = {
	.name = "Physically mapped flash",
	.phys = WINDOW_ADDR,
	.size = WINDOW_SIZE,
	.bankwidth = BUSWIDTH
};

int __init init_bmw(void)
{
       	printk(KERN_NOTICE "bmw flash device: %x at %x\n", WINDOW_SIZE, WINDOW_ADDR);
	bmw_map.virt = (void *)ioremap(WINDOW_ADDR, WINDOW_SIZE);

	if (!bmw_map.virt) {
		printk("Failed to ioremap\n");
		return -EIO;
	}

	simple_map_init(&bmw_map);

	mymtd = do_map_probe("cfi_probe", &bmw_map);
	if (mymtd) {
		mymtd->owner = THIS_MODULE;

#if defined(HAVE_PARTITIONS)
		add_mtd_partitions(mymtd, bmw_partitions,
		                   NUM_PARTITIONS);
#else
		add_mtd_device(mymtd);
#endif
		return 0;
	}

	iounmap((void *)bmw_map.virt);
	return -ENXIO;
}

static void __exit cleanup_bmw(void)
{
	if (mymtd) {
#if defined(HAVE_PARTITIONS)
		del_mtd_device(mymtd);
#else
		del_mtd_partitions(mymtd);
#endif
		map_destroy(mymtd);
	}
	if (bmw_map.virt) {
		iounmap((void *)bmw_map.virt);
		bmw_map.virt = 0;
	}
}

module_init(init_bmw);
module_exit(cleanup_bmw);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("John W. Linville <linville@lvl7.com>");
MODULE_DESCRIPTION("MTD map driver for Broadcom BMW");
