/*
 * Handle mapping of the flash on the LVL7 Alpha 8245
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/ioport.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>


static struct mtd_info *boot_mtd;
static struct mtd_info *main_mtd;

/* Boot Flash - SST-39VF040, 512K (128 uniform 4K sectors) */
#define BOOT_FLASH_ADDR 0xfff00000
#define BOOT_FLASH_SIZE 0x00080000

static struct map_info alpha_8245_boot_map = {
	.name = "ALPHA8245_BOOT",
	.size = BOOT_FLASH_SIZE,
	.bankwidth = 1,
	.phys = BOOT_FLASH_ADDR,
};

#define BOOT_PARTITION_SIZE_KiB    192	
#define DATA_PARTITION_SIZE_KiB     64
#define NUM_BOOT_PARTITIONS 2



/* Main Flash -  2 x Intel-28F128J3, 32MB (128 uniform 128K sectors) */
#define MAIN_FLASH_ADDR 0x70000000
#define MAIN_FLASH_SIZE 0x02000000
#define NUM_MAIN_PARTITIONS 1

static struct map_info alpha_8245_main_map = {
	.name = "Alpha 8245_MAIN",
	.size = MAIN_FLASH_SIZE,
	.bankwidth = 1,
	.phys = MAIN_FLASH_ADDR,
};


/*
 * MTD 'PARTITIONING' STUFF 
 */
static struct mtd_partition boot_partitions[NUM_BOOT_PARTITIONS] = {
    { .name =   "Alpha 8245 uboot partition", 
      .offset = 0, 
      .size =   BOOT_PARTITION_SIZE_KiB*1024 },
    { .name =   "Alpha 8245 data partition", 
      .offset = BOOT_PARTITION_SIZE_KiB * 1024, 
      .size =   DATA_PARTITION_SIZE_KiB*1024 }
};

static struct mtd_partition main_partitions[NUM_MAIN_PARTITIONS] = {
    { .name =   "Alpha 8245 application partition", 
      .offset = 0, 
      .size =   MAIN_FLASH_SIZE },
};


int __init init_alpha_8245(void)
{
	int retval = 0;

	printk(KERN_NOTICE "Alpha 8245 boot flash device: %x at %x\n", BOOT_FLASH_SIZE*4, BOOT_FLASH_ADDR);
	alpha_8245_boot_map.virt = (unsigned long)ioremap(BOOT_FLASH_ADDR, BOOT_FLASH_SIZE * 4);

	if (!alpha_8245_boot_map.virt) {
		printk("Failed to ioremap boot flash\n");
		retval = -EIO;
	} else {
		simple_map_init(&alpha_8245_boot_map);
		boot_mtd = do_map_probe("jedec_probe", &alpha_8245_boot_map);
		if (boot_mtd) {
			boot_mtd->owner = THIS_MODULE;
			add_mtd_partitions(boot_mtd, boot_partitions, NUM_BOOT_PARTITIONS );
		} else {
			iounmap((void *)alpha_8245_boot_map.virt);
			retval =  -ENXIO;
		}
	}

	printk(KERN_NOTICE "Alpha 8245 main flash device: %x at %x\n", MAIN_FLASH_SIZE*4, MAIN_FLASH_ADDR);


	alpha_8245_main_map.virt = (unsigned long)ioremap(MAIN_FLASH_ADDR, MAIN_FLASH_SIZE * 4);

	if (!alpha_8245_main_map.virt) {
		printk("Failed to ioremap\n");
		return -EIO;
	}
	simple_map_init(&alpha_8245_main_map);
	main_mtd = do_map_probe("cfi_probe", &alpha_8245_main_map);
	if (main_mtd) {
		main_mtd->owner = THIS_MODULE;
		add_mtd_partitions(main_mtd, main_partitions, NUM_MAIN_PARTITIONS );
		return retval;
	}

	iounmap((void *)alpha_8245_main_map.virt);
	return -ENXIO;
}

static void __exit cleanup_alpha_8245(void)
{
	if (boot_mtd) {
		del_mtd_device(boot_mtd);
		map_destroy(boot_mtd);
	}
	if (alpha_8245_boot_map.virt) {
		iounmap((void *)alpha_8245_boot_map.virt);
		alpha_8245_boot_map.virt = 0;
	}
	if (main_mtd) {
		del_mtd_device(main_mtd);
		map_destroy(main_mtd);
	}
	if (alpha_8245_main_map.virt) {
		iounmap((void *)alpha_8245_main_map.virt);
		alpha_8245_main_map.virt = 0;
	}
}

module_init(init_alpha_8245);
module_exit(cleanup_alpha_8245);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("LVL7 Systems.");
MODULE_DESCRIPTION("MTD map driver Alpha 8245 boards");
