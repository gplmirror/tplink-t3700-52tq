#ifndef _NET_RAWV6_H
#define _NET_RAWV6_H
#include <linux/autoconf.h>
#ifdef __KERNEL__

#define RAWV6_HTABLE_SIZE	MAX_INET_PROTOS
extern struct hlist_head raw_v6_htable[RAWV6_HTABLE_SIZE];
extern rwlock_t raw_v6_lock;

extern int ipv6_raw_deliver(struct sk_buff *skb, int nexthdr);

extern struct sock *__raw_v6_lookup(struct sock *sk, unsigned short num,
				    struct in6_addr *loc_addr, struct in6_addr *rmt_addr,
				    int dif);

extern int			rawv6_rcv(struct sock *sk,
					  struct sk_buff *skb);


extern void			rawv6_err(struct sock *sk,
					  struct sk_buff *skb,
					  struct inet6_skb_parm *opt,
					  int type, int code, 
					  int offset, __be32 info);

#ifdef CONFIG_IPV6_LVL7_MROUTE
extern void rawv6_mroute_sock_deliver(struct sk_buff *skb);
extern int rawv6_mroute_mifi_add(unsigned short mifi, unsigned short pifi);
extern int rawv6_mroute_mifi_del(unsigned short mifi);
extern int ravw6_mroute_pifi_del(unsigned short pifi);
#define MRT6_BASE       200
#define MRT6_INIT       (MRT6_BASE)     /* Enable the socket upon which this is called to 
                                            send and receive multicast packets without 
                                            regard to group membership */
#define MRT6_DONE       (MRT6_BASE+1)   /* Cancel the above */
#define MRT6_ADD_MIF    (MRT6_BASE+2)   /* Enable an interface's participation in the above */
#define MRT6_DEL_MIF    (MRT6_BASE+3)   /* Disable an interface's participation in the above*/

/* Structures Passed to the stack during MRT6_ADD_MIF / MRT6_DEL_MIF */
 typedef unsigned short mifi_t;

struct mif6ctl {
        mifi_t  mif6c_mifi;             /* Userspace-provided index value for DEL_MIF */
        unsigned char mif6c_flags;      /* unused */
        unsigned char vifc_threshold;   /* unused */
        unsigned int vifc_rate_limit;   /* unused */
        u_short  mif6c_pifi;            /* ifindex */
};
#endif

#endif

#endif
