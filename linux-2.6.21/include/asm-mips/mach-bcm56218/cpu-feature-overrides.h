/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2005 MontaVista Software Inc.
 * Author: source@mvista.com
 * Copyright (C) 2003 Ralf Baechle
 */
#ifndef __ASM_MACH_BCM56218_CPU_FEATURE_OVERRIDES_H
#define __ASM_MACH_BCM56218_CPU_FEATURE_OVERRIDES_H

#define cpu_has_llsc		1
/* #define cpu_has_dc_aliases	0 */

#endif /* __ASM_MACH_BCM56218_CPU_FEATURE_OVERRIDES_H */
