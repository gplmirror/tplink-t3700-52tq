/*
 * HND SiliconBackplane MIPS core software interface.
 *
 * $Copyright Open Broadcom Corporation$
 *
 * $Id: hndmips.h,v 1.1 2009/01/05 07:19:05 cchao Exp $
 */

#ifndef _hndmips_h_
#define _hndmips_h_

extern void si_mips_init(si_t *sih, uint shirq_map_base);
extern bool si_mips_setclock(si_t *sih, uint32 mipsclock, uint32 sbclock, uint32 pciclock);
extern void enable_pfc(uint32 mode);
extern uint32 si_memc_get_ncdl(si_t *sih);

#endif /* _hndmips_h_ */
