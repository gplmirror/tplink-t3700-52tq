#ifndef _ASM_I386_MARKER_H
#define _ASM_I386_MARKER_H

/*
 * marker.h
 *
 * Code markup for dynamic and static tracing. i386 architecture optimisations.
 *
 * (C) Copyright 2006 Mathieu Desnoyers <mathieu.desnoyers@polymtl.ca>
 *
 * This file is released under the GPLv2.
 * See the file COPYING for more details.
 */


#ifdef CONFIG_MARKERS

#define MF_DEFAULT (MF_OPTIMIZED | MF_LOCKDEP | MF_PRINTK)

/* Optimized version of the markers */
#define MARK_OPTIMIZED(flags, name, format, args...) \
	do { \
		static const char __mstrtab_name_##name[] \
		__attribute__((section("__markers_strings"))) \
		= #name; \
		static const char __mstrtab_format_##name[] \
		__attribute__((section("__markers_strings"))) \
		= format; \
		static const char __mstrtab_args_##name[] \
		__attribute__((section("__markers_strings"))) \
		= #args; \
		static struct __mark_marker_data __mark_data_##name \
		__attribute__((section("__markers_data"))) = \
		{ __mstrtab_name_##name,  __mstrtab_format_##name, \
		__mstrtab_args_##name, \
		(flags) | MF_OPTIMIZED, __mark_empty_function, NULL }; \
		char condition; \
		asm volatile(	".section __markers, \"a\", @progbits;\n\t" \
					".long %1, 0f;\n\t" \
					".previous;\n\t" \
					".align 2\n\t" \
					"0:\n\t" \
					"movb $0,%0;\n\t" \
				: "=r" (condition) \
				: "m" (__mark_data_##name)); \
		__mark_check_format(format, ## args); \
		if (likely(!condition)) { \
		} else { \
			preempt_disable(); \
			(*__mark_data_##name.call)(&__mark_data_##name, \
						format, ## args); \
			preempt_enable(); \
		} \
	} while (0)

/* Marker macro selecting the generic or optimized version of marker, depending
 * on the flags specified. */
#define _MARK(flags, format, args...) \
do { \
	if (((flags) & MF_LOCKDEP) && ((flags) & MF_OPTIMIZED)) \
		MARK_OPTIMIZED(flags, format, ## args); \
	else \
		MARK_GENERIC(flags, format, ## args); \
} while (0)

/* Marker with default behavior */
#define MARK(format, args...) _MARK(MF_DEFAULT, format, ## args)

/* Architecture dependant marker information, used internally for marker
 * activation. */

/* Offset of the immediate value from the start of the movb instruction, in
 * bytes. */
#define MARK_OPTIMIZED_ENABLE_IMMEDIATE_OFFSET 1
#define MARK_OPTIMIZED_ENABLE_TYPE char
/* Dereference enable as lvalue from a pointer to its instruction */
#define MARK_OPTIMIZED_ENABLE(a) \
	*(MARK_OPTIMIZED_ENABLE_TYPE*) \
		((char*)a+MARK_OPTIMIZED_ENABLE_IMMEDIATE_OFFSET)

extern int marker_optimized_set_enable(void *address, char enable);

#endif
#endif //_ASM_I386_MARKER_H
