/* ------------------------------------------------------------------------- */
/* i2c-algo-mpc824x.h i2c driver algorithms for Motorola mpc824x processors  */
/* ------------------------------------------------------------------------- */
/*   Copyright (C) 2002 NetBotz Inc <primm@netbotz.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                */
/* ------------------------------------------------------------------------- */

/* $Id: i2c-algo-mpc824x.h,v 1.1 2005/05/20 20:36:26 andyg Exp $ */

#ifndef I2C_ALGO_MPC824X_H
#define I2C_ALGO_MPC824X_H 1

#include <linux/i2c.h>

/* --- Defines for mpc824x-adapters ----------------------------------	*/

/* Structures for I2C registers					        */

/* I2C Address Register							*/
union i2c_mpc824x_i2cadr {
	struct {
		u32	reserved : 24;
		/* Slave address					*/
		u32	addr : 7;
		u32	reserved2 : 1;
	} bits;
	u32	reg;
};

/* I2C Frequency Divider Register					*/
union i2c_mpc824x_i2cfdr {
	struct {
		u32	reserved : 18;
		/* Digital filter frequency sampling rate		*/
		u32	dffsr : 6;
		u32	reserved2 : 2;
		/* Frequency divider ratio				*/
		u32	fdr : 6;
	} bits;
	u32	reg;
};

/* I2C Control Register							*/
union i2c_mpc824x_i2ccr {
	struct {
		u32	reserved : 24;
		/* Module enable					*/
		u32	men : 1;
		/* Module interrupt enable				*/
		u32	mien : 1;
		/* Master/slave mode START				*/
		u32	msta : 1;
		/* Transmit/receive mode select				*/
		u32	mtx : 1;
		/* Transfer acknowledge					*/
		u32	txak : 1;
		/* Repeat START						*/
		u32	rsta : 1;
		/* PCI interrupt enable					*/
		u32	pcii : 1;
		/* Broadcast enable					*/
		u32	bcst : 1;
	} bits;
	u32 reg;
};
	
/* I2C Status Register							*/
union i2c_mpc824x_i2csr {
	struct {
		u32	reserved : 24;
		/* Data transferring					*/
		u32	mcf : 1;
		/* Addressed as a slave					*/
		u32	maas : 1;
		/* Bus busy						*/
		u32	mbb : 1;
		/* Arbitration lost					*/
		u32	mal : 1;
		/* Broadcast address detection				*/
		u32	bca : 1;
		/* Slave read/write					*/
		u32	srw : 1;
		/* Module interrupt					*/
		u32	mif : 1;
		/* Received acknowledge					*/
		u32	rxak : 1;
	} bits;
	u32	reg;
};

/* I2C Data Register							*/
union i2c_mpc824x_i2cdr {
	struct {
		u32	reserved : 24;
		/* Data register					*/
		u32	data : 8;
	} bits;
	u32	reg;
};

/*
 * This struct contains the hardware specific information for the port.
 */
struct i2c_algo_mpc824x_data {
	/* State of current transfer					*/
	int	is_xmit;	/* 1 = XMIT, 0 = RECV			*/
	u8 *	buffer;		/* xmit/recv buffer for data		*/
	int	buflen;		/* Length of buffer recv/xmit		*/
	int	bufindex;	/* Current index into buffer		*/
	u8	slaveaddr;	/* Current slave address		*/
	u8	stop_when_done;	/* 1=do STOP when completed		*/
	u8	is_restart;	/* Are we doing a repeat start		*/
	u8	is_rcv_addr;	/* Is receive address phase		*/
};

#define I2C_BIT_ADAP_MAX	16

int i2c_mpc824x_add_bus(struct i2c_adapter *i2c_adap, struct pci_dev *dev);
int i2c_mpc824x_del_bus(struct i2c_adapter *i2c_adap);

#ifdef CONFIG_RAPTOR
void i2c_mpc824x_do_cmds(void *eumb_addr, struct i2c_msg msgs[],
	int num);
/* Special entry point used for I2C commands issued for modules		*/
void i2c_mpc824x_do_cmds_async(struct i2c_msg *msgs,
	int num, void (*callback)(struct i2c_adapter *, struct i2c_msg *, 
	int num, int rc, void *usr_data), void *usr_data);
#endif

#endif /* I2C_ALGO_MPC824X_H */
