/* marker-example.c
 *
 * Executes a marker when /proc/marker-example is opened.
 *
 * (C) Copyright 2007 Mathieu Desnoyers <mathieu.desnoyers@polymtl.ca>
 *
 * This file is released under the GPLv2.
 * See the file COPYING for more details.
 */

#include <linux/module.h>
#include <linux/marker.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>

struct proc_dir_entry *pentry_example = NULL;

static int my_open(struct inode *inode, struct file *file)
{
	int i;
	int j = 1;

	for (i = 0; i < 32; i++)
	  {
	    MARK(kernel_generic_int, "%d", j);
	    j <<= 1;
	  }

	MARK(kernel_generic_string, "%s", "example string");
	MARK(kernel_generic_file_line_msg, "%s %d %s",
	     __FILE__,
	     __LINE__,
	     __FUNCTION__);

	MARK(kernel_generic_int, "%d", INT_MIN);
	MARK(kernel_generic_int, "%d", INT_MAX);
	MARK(kernel_generic_uint, "%u", 0);
	MARK(kernel_generic_uint, "%u", UINT_MAX);
	MARK(kernel_generic_int64, "%lld", LLONG_MAX);
	MARK(kernel_generic_int64, "%lld", LLONG_MIN);
	MARK(kernel_generic_uint64, "%llu", 0);
	MARK(kernel_generic_uint64, "%llu", ULLONG_MAX);
	MARK(kernel_generic_pointer, "%p", &i);
	MARK(kernel_generic_size_t, "%zd", sizeof (1LL));
	MARK(kernel_generic_int_x4, "%d %d %d %d", 1, 2, 3, 4);

	MARK(kernel_generic_int64_x4, "%lld %lld %lld %lld",
	     LLONG_MAX / 1,
	     LLONG_MAX / 2,
	     LLONG_MAX / 4,
	     LLONG_MAX / 8);

	MARK(kernel_generic_string_int_x4, "%s %d %d %d %d",
	     __FUNCTION__,
	     1, 2, 3, 4);

	MARK(kernel_generic_string_int64_x4, "%s %lld %lld %lld %lld",
	     __FUNCTION__,
	     LLONG_MAX / 1,
	     LLONG_MAX / 2,
	     LLONG_MAX / 4,
	     LLONG_MAX / 8);
	
	return -EPERM;
}

static struct file_operations mark_ops = {
	.open = my_open,
};

static int example_init(void)
{
	printk(KERN_ALERT "example init\n");
	pentry_example = create_proc_entry("marker-example", 0444, NULL);
	if (pentry_example)
		pentry_example->proc_fops = &mark_ops;
	else
		return -EPERM;
	return 0;
}

static void example_exit(void)
{
	printk(KERN_ALERT "example exit\n");
	remove_proc_entry("marker-example", NULL);
}

module_init(example_init)
module_exit(example_exit)

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mathieu Desnoyers");
MODULE_DESCRIPTION("Linux Trace Toolkit example");
