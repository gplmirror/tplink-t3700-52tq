/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */

/*  Create device with:
 *  mknod /dev/watchdog c 10 130
 *  An test application is kept at linux/Documentation/watchdog/src/nlm_wdt_test.c
*/
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/watchdog.h>
#include <linux/init.h>
#include <linux/bitops.h>
#include <linux/jiffies.h>
#include <linux/reboot.h>
#include <linux/interrupt.h>

#include <asm/irq.h>
#include <asm/uaccess.h>
#include <asm/netlogic/xlp_irq.h>
#include <linux/smp_lock.h>

static unsigned int heartbeat_0 = 60, heartbeat_1 = 60;	/* (secs) Default is 1 minute */
static unsigned long xlp_wdt_status;

#define	WDT_IN_USE		0
#define NLM_IOC_WDT_ENABLE (0x1003 + 1)
#define NLM_IOC_WDT_DISABLE (0x1003 + 2)

static unsigned long cpu_mask_0 = 0, cpu_mask_1;

struct nlm_wdt{
	int wd_id;
	int heartbeat;
	uint64_t cpu_mask;
	};

static void
xlp_wdt_keepalive(u64 cpu_mask, int wd_id)
{
	int i;
	u8 node;

	node = hard_smp_processor_id() / NLM_MAX_CPU_PER_NODE;
	/* ack the watchdog on the cpu */
	i = node *4;
	while(cpu_mask)
	{
		if(cpu_mask & 0x1)
			nlh_pic_w64r(node, XLP_PIC_WD_BEATCMD(wd_id), i);
		i++;
		cpu_mask = cpu_mask >> 1;
	}

}

static irqreturn_t xlp_wdt_handler(int irq, void *p)
{
	u64 val;
	int wd_id;
	u8 node;

	node = hard_smp_processor_id() / NLM_MAX_CPU_PER_NODE;
	/* Read PIC status to know which watchdog interrupt occured*/
        val = nlh_pic_r64r(node, XLP_PIC_STATUS);

	wd_id = (int)(val & 0x3);

	/* Writing 1 to PIC status [0:1] cleares inteppupt for watchdog [0:1] */
	val = (val | wd_id);
	nlh_pic_w64r(node, XLP_PIC_STATUS, val);

	/* Once interrupt is occured then keep the CPU alive to write on hearbeat register*/
	if(wd_id & 0x1)
		xlp_wdt_keepalive(cpu_mask_0, 0);
	if(wd_id & 0x2)
		xlp_wdt_keepalive(cpu_mask_1, 1);

        return IRQ_HANDLED;
}

int
xlp_wdt_enable(int wd_id, int heartbeat, u64 cpu_mask)
{
	u8 node;
	u64 val, max_val;
	int i;
	int ret;

	if(wd_id == 1)
	{
		cpu_mask_1 = cpu_mask;
		heartbeat_1 = heartbeat;
	}
	if(wd_id == 0)
	{
		cpu_mask_0 = cpu_mask;
		heartbeat_0 = heartbeat;
	}

	max_val = heartbeat * XLP_PIT_TICK_RATE;

	/* Register a handler*/
	node = hard_smp_processor_id() / NLM_MAX_CPU_PER_NODE;
        ret = request_irq(XLP_WD_IRQ(node, wd_id), xlp_wdt_handler, 0, "xlp_wtd", NULL);
        if (ret < 0) {
                return -EINVAL;
        }

        val = nlh_pic_r64r(node, XLP_PIC_CTRL);

	/* Set WATCHDOG[01]_MAXVALUE register for the max value of count donw register*/
	nlh_pic_w64r(node, XLP_PIC_WD_MAXVAL(wd_id), max_val);

	/* Set the (WATCHDOG[01]_ENABLE[01]) CPU masks of therad which need to be monitered*/
	if(node < 2)
		nlh_pic_w64r(node, XLP_PIC_WD_THREN0(wd_id), cpu_mask);
	else
		nlh_pic_w64r(node, XLP_PIC_WD_THREN1(wd_id), cpu_mask);


	/* Keep the thread active to write on WATCHDOG[01]_BEATCMD register */
	i = node *4;
	while(cpu_mask)
	{
		if(cpu_mask & 0x1)
			nlh_pic_w64r(node, XLP_PIC_WD_BEATCMD(wd_id), i);
		i++;
		cpu_mask = cpu_mask >> 1;
	}

	/* There are two watchdog timer 0 and 1. Enable them*/
	val = (val|(1<<wd_id));
	nlh_pic_w64r(node, XLP_PIC_CTRL, val);

}

void
xlp_wdt_disable(int wd_id)
{
	u64 val, tmp = 0;
	u8 node;

	node = hard_smp_processor_id() / NLM_MAX_CPU_PER_NODE;
	tmp = 1 << wd_id;
	val = val & ~tmp;

	nlh_pic_w64r(node, XLP_PIC_CTRL, val);
}

EXPORT_SYMBOL(xlp_wdt_enable);
EXPORT_SYMBOL(xlp_wdt_disable);


static int
xlp_wdt_open(struct inode *inode, struct file *file)
{
	if (test_and_set_bit(WDT_IN_USE, &xlp_wdt_status))
		return -EBUSY;


	return nonseekable_open(inode, file);
}


static struct watchdog_info ident = {
	.options	= WDIOF_MAGICCLOSE | WDIOF_SETTIMEOUT |
				WDIOF_KEEPALIVEPING,
	.identity	= "XLP Watchdog",
};

int
xlp_wdt_ioctl(struct inode *inode, struct file *file, unsigned int cmd,
			unsigned long arg)
{
	int ret = -ENOTTY;
	u8 node;
	struct nlm_wdt *wdt;
	unsigned char temp[32];

	ret = copy_from_user(temp, (unsigned char*)arg, sizeof(struct nlm_wdt));
	if(ret > -1)
		wdt = (struct nlm_wdt*)temp;

	node = hard_smp_processor_id() / NLM_MAX_CPU_PER_NODE;

	switch (cmd) {
	case WDIOC_GETSUPPORT:
		ret = copy_to_user((struct watchdog_info *)arg, &ident,
				   sizeof(ident)) ? -EFAULT : 0;
		break;

	case WDIOC_GETSTATUS:
		ret = put_user(0, (int *)arg);
		break;

	case WDIOC_GETBOOTSTATUS:
		ret = put_user(0, (int *)arg);
		break;

	case WDIOC_SETTIMEOUT:

		break;
	case WDIOC_GETTIMEOUT:
		if(wdt->wd_id == 1)
			ret = put_user(heartbeat_1, (int *)arg);
		else
			ret = put_user(heartbeat_0, (int *)arg);
		break;

	case WDIOC_KEEPALIVE:
		xlp_wdt_keepalive(wdt->cpu_mask, wdt->wd_id);
		break;

	case NLM_IOC_WDT_ENABLE:
		xlp_wdt_enable(wdt->wd_id, wdt->heartbeat, wdt->cpu_mask);
		break;

	case NLM_IOC_WDT_DISABLE:
		xlp_wdt_disable(wdt->wd_id);
	}

	return ret;
}

long xlp_wdt_compat_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	unsigned long ret = -1;
  	lock_kernel();
	ret = xlp_wdt_ioctl(NULL, filp, cmd, arg);
	unlock_kernel();
	if(ret){
		printk("%s: ioctl error\n", __FUNCTION__);
		return -EINVAL;
	}
	return ret;
}

static int
xlp_wdt_release(struct inode *inode, struct file *file)
{
	clear_bit(WDT_IN_USE, &xlp_wdt_status);

	return 0;
}

static int xlp_notify_sys(struct notifier_block *this, unsigned long code, void *unused)
{
        return NOTIFY_DONE;
}

static const struct file_operations xlp_wdt_fops =
{
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.ioctl		= xlp_wdt_ioctl,
	.compat_ioctl   = xlp_wdt_compat_ioctl,
	.open		= xlp_wdt_open,
	.release	= xlp_wdt_release,
};

static struct miscdevice xlp_wdt_miscdev =
{
	.minor		= WATCHDOG_MINOR,
	.name		= "watchdog",
	.fops		= &xlp_wdt_fops,
};

static struct notifier_block xlp_notifier = {
        .notifier_call = xlp_notify_sys,
};


static int __init xlp_wdt_init(void)
{
	int ret;
	u8 node;

	node = hard_smp_processor_id() / NLM_MAX_CPU_PER_NODE;
        ret = register_reboot_notifier(&xlp_notifier);
        if (ret) {
                printk(KERN_ERR "cannot register reboot notifier (err=%d)\n",
                        ret);
                return ret;
        }
	return misc_register(&xlp_wdt_miscdev);
}

static void __exit xlp_wdt_exit(void)
{
	misc_deregister(&xlp_wdt_miscdev);
	unregister_reboot_notifier(&xlp_notifier);
}

module_init(xlp_wdt_init);
module_exit(xlp_wdt_exit);

MODULE_AUTHOR("Alok Agrawat");
MODULE_DESCRIPTION("Broadcom Processors Watchdog");

MODULE_LICENSE("GPL");
MODULE_ALIAS_MISCDEV(WATCHDOG_MINOR);

