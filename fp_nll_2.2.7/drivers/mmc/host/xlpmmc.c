/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */


/*
 * linux/drivers/mmc/host/xlpmmc.c - XLP MMC driver
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/mm.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/scatterlist.h>
#include <linux/leds.h>
#include <linux/mmc/host.h>
#include <linux/delay.h>

#include <asm/io.h>
#include <asm/gpio.h>
#include <asm/netlogic/hal/nlm_hal.h>
#include <hal/nlm_hal_xlp_dev.h>

#include "xlpmmc.h"

#undef XLP_MMC_DEBUG 
#undef DEBUG 

struct xlpmmc_host {
	struct mmc_host *mmc;
	struct mmc_request *mrq;

	u32 flags;
	void    __iomem         *base;
	void* ioarea;
	u32 clock;
	u32 bus_width;
	u32 power_mode;
	spinlock_t		irq_lock; /* Prevent races with irq handler */
	int status;
        int present;
        int slot;

	struct {
		int len;
		int dir;
	} dma;

	struct {
		int index;
		int offset;
		int len;
	} pio;

        unsigned long workaround;
	int irq;
        struct timer_list       timer;
        struct tasklet_struct   card_tasklet;   /* Tasklet structures */

	struct xlpmmc_platform_data *platdata;
	struct platform_device *pdev;
        struct xlpmmc_host **host_array;
};

#define XLPMMC_DESCRIPTOR_SIZE (64<<10)
#define XLPMMC_DESCRIPTOR_COUNT  1	

/* Status flags used by the host structure */
#define HOST_F_XMIT	0x0001
#define HOST_F_RECV	0x0002
#define HOST_F_DMA	0x0010
#define HOST_F_ACTIVE	0x0100
#define HOST_F_STOP	0x1000

#define HOST_S_IDLE	0x0001
#define HOST_S_CMD	0x0002
#define HOST_S_DATA	0x0003

#define PCIE_HDR_OFFSET 0x100
#define GPIO_MMC_DETECT 29

#define XLP_SLOT_SIZE   0x100
#define XLP_2XX_NUM_SDMMC_SLOT 2
#define XLP_3XX_NUM_SDMMC_SLOT 2
#define XLP_8XX_NUM_SDMMC_SLOT 2

__inline__ int32_t gpio_regread(int node, int regidx) 
{
        volatile uint64_t mmio;
        mmio = nlm_hal_get_dev_base(node, 0, XLP_PCIE_GIO_DEV, XLP_GIO_GPIO_FUNC);
        return nlm_hal_read_32bit_reg(mmio, regidx);
}

static __inline__ void gpio_regwrite(int node, int regidx, int32_t val)
{
        volatile uint64_t mmio;
        mmio = nlm_hal_get_dev_base(node, 0, XLP_PCIE_GIO_DEV, XLP_GIO_GPIO_FUNC);
        nlm_hal_write_32bit_reg(mmio, regidx, val);
}

/* Low-level write-routines*/
static inline void hc_wr32(void *iobase, int offset, u32 data, int slot) {

        int reg_offset = PCIE_HDR_OFFSET + (slot*XLP_SLOT_SIZE);
        volatile u32 *mmc_base = (u32 *)( (u8*)iobase + reg_offset );
        mmc_base[offset>>2] = data;
        return;
}

static inline void hc_wr16(void *iobase, int offset, u16 data, int slot) {

        int reg_offset = PCIE_HDR_OFFSET + (slot*XLP_SLOT_SIZE);
        volatile u16 *mmc_base = (u16 *)( (u8*)iobase + reg_offset );

        mmc_base[offset>>1] = data;
        return;
}

/* Low-level read-routines
 */
static inline u32 hc_rd32(void *iobase,int offset, int slot) {

        int reg_offset = PCIE_HDR_OFFSET + (slot*XLP_SLOT_SIZE);
        volatile u32 *mmc_base = (u32 *)( (u8*)iobase + reg_offset );

        u32 data = mmc_base[offset>>2];
        return data;
}

static inline u32 hc_rd16(void *iobase, int offset, int slot) {

        int reg_offset = PCIE_HDR_OFFSET + (slot*XLP_SLOT_SIZE);
        volatile u16 *mmc_base = (u16 *)( (u8*)iobase + reg_offset );

        u32 data = mmc_base[offset>>1];
        return data;
}
static int findmaxslots() {
        if(is_nlm_xlp8xx())
                return XLP_8XX_NUM_SDMMC_SLOT;
        else if (is_nlm_xlp3xx())
                return XLP_3XX_NUM_SDMMC_SLOT;
        else if(is_nlm_xlp2xx())
                return XLP_2XX_NUM_SDMMC_SLOT;
        else
                return 1;
}

static void dump_hc_regs(struct xlpmmc_host *host, int slot)
{
#ifdef XLP_MMC_DEBUG	
	printk ("SLOT:%d MMC_PRESENT STATE = 0x%x\n", slot, hc_rd32((host->base), HC_PRESENT_STATE_LO, slot));
	printk ("SLOT:%d MMC_POWER_CTL = 0x%x\n", slot, hc_rd16((host->base), HC_PC_HC, slot));
	printk ("SLOT:%d MMC_CLOK_CTL = 0x%x\n", slot, hc_rd16((host->base), HC_CLOCK_CTRL, slot));
	printk ("SLOT:%d MMC_CAP0 = 0x%x\n", slot, hc_rd32((host->base), 0x40, slot));
#endif
}

static void xlpmmc_set_power(struct xlpmmc_host *host, int state, int slot)
{	
	if(state)
		hc_wr16(host->base, HC_PC_HC, 0x1f00, slot);
	else
		hc_wr16(host->base, HC_PC_HC, 0x1e00, slot);
}		

static int xlpmmc_find_slot(struct xlpmmc_host *host)
{
        return host->slot;
}

static int xlpmmc_card_inserted(struct mmc_host *mmc)
{
        struct xlpmmc_host *host = mmc_priv(mmc);
        int slot = xlpmmc_find_slot(host);
        host->present = !gpio_get_value(GPIO_MMC_DETECT+slot);
        return host->present;
}

static int xlpmmc_card_readonly(struct mmc_host *mmc)
{
	//struct xlpmmc_host *host = mmc_priv(mmc);
	return -ENOSYS;
}

static irqreturn_t xlpmmc_det_irq(int irq, void *dev_id)
{
        struct xlpmmc_host **xlpmmc_host_data = dev_id;
        int present, count; 
        int maxslots = findmaxslots();
#ifdef XLP_MMC_DEBUG
        //printk("Entered xlpmmc_det_irq\n");
#endif
        /*
        * we expect this irq on both insert and remove,
        * and use a short delay to debounce.
        */
        for (count=0; count<maxslots; count ++) {
	        struct xlpmmc_host *host = xlpmmc_host_data[count];
                present = !gpio_get_value(GPIO_MMC_DETECT+count); 
                gpio_regwrite(0, XLP_8XX_GPIO_INT_STAT0, gpio_regread(0, XLP_8XX_GPIO_INT_STAT0) 
                        & 0x1<<(GPIO_MMC_DETECT+count));
                        
                if(present)
                        gpio_regwrite(0, XLP_8XX_GPIO_INT_POLAR0, gpio_regread(0, XLP_8XX_GPIO_INT_POLAR0) 
                                & ~(0x1<<(GPIO_MMC_DETECT+count)));
                else
                        gpio_regwrite(0, XLP_8XX_GPIO_INT_POLAR0, gpio_regread(0, XLP_8XX_GPIO_INT_POLAR0) 
                                | 0x1<<(GPIO_MMC_DETECT+count));

                if (present != host->present) {
                        host->present = present;
                        pr_debug("%s: card %s\n", mmc_hostname(host->mmc),
                                present ? "insert" : "remove");
                        //tasklet_schedule(&host->card_tasklet);
                        mmc_detect_change(host->mmc, msecs_to_jiffies(100));
                }
        }
        return IRQ_HANDLED;
}

static void xlpmmc_finish_request(struct xlpmmc_host *host)
{
	struct mmc_request *mrq = host->mrq;
#ifdef XLP_MMC_DEBUG	
	printk("xlpmmc_finish_request mrq= %p host=%p slot %d\n", mrq, host, host->slot);
#endif
	host->mrq = NULL;
	host->flags &= HOST_F_ACTIVE | HOST_F_DMA;

	host->dma.len = 0;
	host->dma.dir = 0;

	host->pio.index  = 0;
	host->pio.offset = 0;
	host->pio.len = 0;

	host->status = HOST_S_IDLE;

	mmc_request_done(host->mmc, mrq);
}

static int xlpmmc_send_command(struct xlpmmc_host *host, int wait,
				struct mmc_command *cmd, struct mmc_data *data, int slot)
{
	int rsp_type;
	u32 mmccmd = 0;
	volatile u32 hcstate = 0;
	
	/* Parse the command and set the CMD and XFER reg accordingly */
	mmccmd = (cmd->opcode << CMD_IDX_SHT);

	/*Command type*/
	rsp_type = mmc_resp_type(cmd);
	switch (rsp_type) {
	case MMC_RSP_NONE:
		mmccmd |= RSP_TYPE_NONE;
		break;
	case MMC_RSP_R1:
		mmccmd |= RSP_TYPE_R1;
		break;
	case MMC_RSP_R1B:
		mmccmd |= RSP_TYPE_R1B;
		break;
	case MMC_RSP_R2:
		mmccmd |= RSP_TYPE_R2;
		break;
	case MMC_RSP_R3:
		mmccmd |= RSP_TYPE_R3;
		break;
	default:
		printk(KERN_INFO "xlpmmc: unhandled response type %02x\n",
			mmc_resp_type(cmd));
		return -EINVAL;
	}

#ifdef XLP_MMC_DEBUG
	printk("xlpmmc_send_command : cmd-opcode=%d mmc_data= %p rep= 0x%x cm-arg=0x%x slot%d \n", cmd->opcode, data, rsp_type,cmd->arg, slot);
#endif
	
	/*Data direction and count*/
	if (data) {
		//if((rsp_type != RSP_TYPE_R1B) && (rsp_type != RSP_TYPE_R5B) )/*do we ever come here?*/
		if((rsp_type != RSP_TYPE_R1B) )/*do we ever come here?*/
			mmccmd |= DP_DATA;

		/*enable block count*/	
		mmccmd |= (1<<1);
		/*enable dma*/
		if(host->flags & HOST_F_DMA)
			mmccmd |= DMA_EN;
		
		if (data->blocks == 1){
			mmccmd |= MBS_SINGLE;
		}
		else{
			mmccmd |= MBS_MTPLE; 
			mmccmd |= AUTO_CMD12_EN;  
		}

		if (data->flags & MMC_DATA_READ) {
			mmccmd |= DDIR_READ;
		
		} else if (data->flags & MMC_DATA_WRITE) {
			mmccmd |= DDIR_WRITE;
		}
	}else{
		mmccmd |= DP_NO_DATA; 
	}

	/*Wait for CMD line to free. Safe to poll 
	as HOST CMD line could have been resetted.*/
	hcstate = hc_rd32(host->base, 0x24, slot);
	do{
		/*Should never stuck here*/
		hcstate = hc_rd32(host->base, 0x24, slot);
	}while(hcstate & 0x1);
        mod_timer(&host->timer, jiffies + 1 * HZ); 

	hc_wr32(host->base, HC_ARG1_LO, cmd->arg, slot);
	hc_wr32(host->base, HC_COMMAND, mmccmd, slot);	
	return 0;
}

static void xlpmmc_data_complete(struct xlpmmc_host *host)
{
	struct mmc_request *mrq = host->mrq;
	struct mmc_data *data;

	WARN_ON((host->status != HOST_S_DATA));

	if (host->mrq == NULL)
		return;
	
	data = mrq->cmd->data;

	dma_unmap_sg(mmc_dev(host->mmc), data->sg, data->sg_len, host->dma.dir);
	
	data->bytes_xfered = 0;

	if (!data->error) {
		if(host->flags & HOST_F_DMA){	
			data->bytes_xfered = (data->blocks * data->blksz); 
		}else{
			data->bytes_xfered = (data->blocks * data->blksz) - host->pio.len;
		}
	}
	xlpmmc_finish_request(host);
}


static void xlpmmc_send_pio(struct xlpmmc_host *host, int slot)
{
	struct mmc_data *data;
	int sg_len, max, count;
	unsigned char *sg_ptr;
	struct scatterlist *sg;

	data = host->mrq->data;

	if (!(host->flags & HOST_F_XMIT))
		return;
	
	/* This is the pointer to the data buffer */
	sg = &data->sg[host->pio.index];
	sg_ptr = sg_virt(sg) + host->pio.offset;

	/* This is the space left inside the buffer */
	sg_len = data->sg[host->pio.index].length - host->pio.offset;

	/* Check if we need less than the size of the sg_buffer */
	max = (sg_len > host->pio.len) ? host->pio.len : sg_len;
	if (max > MMCSD_SECTOR_SIZE)
		max = MMCSD_SECTOR_SIZE;

	for (count = 0; count < max/4; count++) {
		u8 write_data[4];
		
		write_data[3] = *sg_ptr++ & 0xff; 
		write_data[2] = *sg_ptr++ & 0xff; 
		write_data[1] = *sg_ptr++ & 0xff; 
		write_data[0] = *sg_ptr++ & 0xff;
		hc_wr32(host->base, HC_BUFF_DATA_PORT0, *(u32*)write_data, slot);
	}
	count=count<<2;

	host->pio.len -= count;
	host->pio.offset += count;

	if (count == sg_len) {
		host->pio.index++;
		host->pio.offset = 0;
	}
}

static void xlpmmc_receive_pio(struct xlpmmc_host *host, int slot)
{
	struct mmc_data *data;
	int max, count, sg_len = 0;
	unsigned char *sg_ptr = NULL;
	struct scatterlist *sg;

	data = host->mrq->data;

	if (!(host->flags & HOST_F_RECV))
		return;

	max = host->pio.len;

	if (host->pio.index < host->dma.len) {
		sg = &data->sg[host->pio.index];
		sg_ptr = sg_virt(sg) + host->pio.offset;

		/* This is the space left inside the buffer */
		sg_len = sg_dma_len(&data->sg[host->pio.index]) - host->pio.offset;

		/* Check if we need less than the size of the sg_buffer */
		if (sg_len < max)
			max = sg_len;
	}
	if (max > MMCSD_SECTOR_SIZE){
		max = MMCSD_SECTOR_SIZE;
	}

	for (count = 0; count < max/4; count++) {
		volatile uint32_t read_data;
		read_data = hc_rd32(host->base, HC_BUFF_DATA_PORT0, slot);
			*sg_ptr++ = (unsigned char)((read_data >>  0) & 0xFF);
			*sg_ptr++ = (unsigned char)((read_data >>  8) & 0xFF);
			*sg_ptr++ = (unsigned char)((read_data >>  16) & 0xFF);
			*sg_ptr++ = (unsigned char)((read_data >>  24) & 0xFF);
	}
	count = count<<2;

	host->pio.len -= count;
	host->pio.offset += count;

	if (sg_len && count == sg_len) {
		host->pio.index++;
		host->pio.offset = 0;
	}

}

/*
  1)This is called when a command has been completed - grab the response
     and check for errors. Then start the data transfer if it is indicated.
  2) Notify the core about command completion 
*/
static void xlpmmc_cmd_complete(struct xlpmmc_host *host, u32 status, int slot)
{
	struct mmc_request *mrq = host->mrq;
	struct mmc_command *cmd;
	u32 r[4];
	int i, trans;

	if (!host->mrq)
		return;

	cmd = mrq->cmd;

	if (cmd->flags & MMC_RSP_PRESENT) {
		if (cmd->flags & MMC_RSP_136) {
			r[0] = hc_rd32(host->base, HC_RESPONSE6, slot);
			r[1] = hc_rd32(host->base, HC_RESPONSE4, slot);
			r[2] = hc_rd32(host->base, HC_RESPONSE2, slot);
			r[3] = hc_rd32(host->base, HC_RESPONSE0, slot);
			for (i = 0; i < 4; i++) {
				cmd->resp[i] = (r[i] & 0x00FFFFFF) << 8;
				if (i != 3)
					cmd->resp[i] |= (r[i + 1] & 0xFF000000) >> 24;
			}
#ifdef XLP_MMC_DEBUG
			printk("RESP0 =0x%x RESP1 =0x%x RESP2 = 0x%x RESP3=0x%x\n ",  cmd->resp[0], cmd->resp[1], cmd->resp[2], cmd->resp[3]);
#endif
		}else{ 
			cmd->resp[0] = hc_rd32(host->base, HC_RESPONSE0,slot);
		}
	}

	trans = host->flags & (HOST_F_XMIT | HOST_F_RECV);

	if (!trans || cmd->error) {
		xlpmmc_finish_request(host); 
		return;
	}
	host->status = HOST_S_DATA;
}

static int cal_range(int base_clk, int rate)
{
	if(rate<=(base_clk/256)) 
		return 128;	
	if(rate<=(base_clk/128) && (rate>(base_clk/256)))
		return 64;	
	if(rate<=(base_clk/64) && (rate>(base_clk/128)))
		return 32;	
	if(rate<=(base_clk/32) &&  (rate>(base_clk/64)))
		return 16;	
	if(rate<=(base_clk/16) &&  (rate>(base_clk/32)))
		return 8;	
	if(rate<=(base_clk/8) &&  (rate>(base_clk/16)))
		return 4;	
	if(rate<=(base_clk/4) &&  (rate>(base_clk/8)))
		return 2;	
	if(rate<=(base_clk/2) &&  (rate>(base_clk/4)))
		return 1;	

	return 0;
}

static void xlpmmc_set_clock(struct xlpmmc_host *host, int rate , int slot)
{
	u32 sd_bclk_fq;
	u16 sd_ck_fs = 0;
	u16 hc_clk_ctl=0;
	/* NOTE: 10 bit mode is not set. CAP1 doesn't say that*/
	/* i) Read the base clock.
	   ii) find the divisior. */

	sd_bclk_fq = (0x85)*1000; /* reset value 133 MHz */ 
	rate= rate/1000;
	sd_ck_fs = cal_range(sd_bclk_fq, rate);
	
	hc_clk_ctl  = (sd_ck_fs<<SD_CLK_FRQ_SHT) |  HCC_INT_CLK_EN | HCC_SD_CLK_EN;
	hc_wr16(host->base, HC_CLOCK_CTRL, hc_clk_ctl, slot);

	/* Wait for stable clock */
	while ((hc_rd16(host->base, HC_CLOCK_CTRL, slot) & HCC_INT_CLK_STABLE) == 0) {};
	return;	
}

static int xlpmmc_prepare_data(struct xlpmmc_host *host,
				struct mmc_data *data, int slot)
{
	int datalen = data->blocks * data->blksz;
	u32 mmc_blk_ctl = 0;
#ifdef XLP_MMC_DEBUG
	printk("xlpmmc_prepare_data datablks=%d block sz= 0x%x\n", data->blocks,  data->blksz);
#endif
	if (data->flags & MMC_DATA_READ)
		host->flags |= HOST_F_RECV;
	else
		host->flags |= HOST_F_XMIT;

	if (host->mrq->stop)
		host->flags |= HOST_F_STOP;

	host->dma.dir = DMA_BIDIRECTIONAL;

	host->dma.len = dma_map_sg(mmc_dev(host->mmc), data->sg,
				   data->sg_len, host->dma.dir);

	if(host->flags & HOST_F_DMA){
		struct scatterlist *sg = &data->sg[0];
#if 0
		if(sg_phys(sg) >= (4ULL<<30)){
			extern void dump_stack(void);
			printk(">4G ADDR: sg_phys(sg) = %#lx\n",sg_phys(sg));
			dump_stack();
		}
#endif
		hc_wr32(host->base, HC_SDMA_SA_OR_ARG2_LO, (unsigned long)sg_phys(sg), slot);
                host->workaround = (unsigned long)sg_phys(sg);
	}else{	
		host->pio.index = 0;
		host->pio.offset = 0;
		host->pio.len = datalen;
	}

	if (host->dma.len == 0)
		return -ETIMEDOUT;

	if(data->blocks > 1)	/*Multiple block transfer?*/
		mmc_blk_ctl |=  data->blocks<<BLK_CNT_SHT;
	else
		mmc_blk_ctl &= 0xffff;
	
	/*Lets slways set 64KB Host bufsize*/
	mmc_blk_ctl |= HOST_BUF_SZ_64;
	
	/*block size bit Xsz[0:11]=[0:11] and Xsz[12]=[15]*/
	mmc_blk_ctl |= (data->blksz<< 0);
	if(data->blksz & BLK_SZ_LOW_BITS)
		mmc_blk_ctl |= BLK_SZ_HGH_BIT;
	else
		mmc_blk_ctl &= ~BLK_SZ_HGH_BIT;
	hc_wr32(host->base, HC_BLOCK_SIZE, mmc_blk_ctl, slot);
#ifdef XLP_MMC_DEBUG
	printk("xlpmmc_prepare_data datablks=%d block sz= 0x%x\n", data->blocks,  data->blksz);
#endif
	
	return 0;
}

/* This actually starts a command or data transaction */
static void xlpmmc_request(struct mmc_host* mmc, struct mmc_request* mrq)
{
	struct xlpmmc_host *host = mmc_priv(mmc);
	int ret  = 0;
        int slot = xlpmmc_find_slot(host);

	WARN_ON(irqs_disabled());
	WARN_ON(host->status != HOST_S_IDLE);
        
        while(host->host_array[0]->status != HOST_S_IDLE || host->host_array[1]->status != HOST_S_IDLE)
        {
                printk("Serializing commands to mutliple MMC Slots\n");
        }

	host->mrq = mrq;
	host->status = HOST_S_CMD;

	/* fail request immediately if no card is present */
#if 0	
	if (0 == xlpmmc_card_inserted(mmc)) {
		mrq->cmd->error = -ENOMEDIUM;
		xlpmmc_finish_request(host);
		return;
	}
	/* No platform support to know card detection */
#endif
	if (mrq->data) {
		ret = xlpmmc_prepare_data(host, mrq->data, slot);
	}

	if (!ret)
		ret = xlpmmc_send_command(host, 0, mrq->cmd, mrq->data, slot);

        /* Work around for slot0, when using two SD/MMCslots */
        hc_wr32(host->base, HC_SDMA_SA_OR_ARG2_LO, host->workaround, slot);

	if (ret) {
		mrq->cmd->error = ret;
		xlpmmc_finish_request(host);
	}
}

static void xlpmmc_reset_controller(struct xlpmmc_host *host, int slot)
{
        /* S1: Clear any set INT Bits */
        hc_wr32(host->base, HC_NORMAL_INT_STS, hc_rd32(host->base, HC_NORMAL_INT_STS, slot), slot);

        /* S2: Enable Interrupts */
        hc_wr32(host->base, HC_NORMAL_INT_STS_EN, 0x37ff7fff, slot);

        /* S3: Enable Interrupt Signals */
        hc_wr32(host->base, HC_NORMAL_INT_SIGNAL_EN, 0x37ff7fff, slot);

        /* S4: Send HW Reset to eMMC-4.4 Card */
        hc_wr16(host->base, HC_PC_HC, 0x1e00, slot);
        hc_wr16(host->base, HC_PC_HC, 0x1f00, slot);

        /* S4: Remove HW Reset to eMMC-4.4 Card */
        hc_wr16(host->base, HC_PC_HC, 0x0f00, slot);

        hc_wr16(host->base, HC_SWRST_TIMEOUT_CTRL,0xe, slot) ;
        printk("Initialized eMMC Host Controller\n");

        return;
}

static void xlpmmc_set_ios(struct mmc_host *mmc, struct mmc_ios *ios)
{
	struct xlpmmc_host *host = mmc_priv(mmc);
	u16 hc_pc_hc;
        int slot = xlpmmc_find_slot(host);
#ifdef XLP_MMC_DEBUG
	printk("xlpmmc_set_ios  host %lx, ios = %p power = 0x%x clock = 0x%x bus-width=0x%x, slot %d\n", host, ios, ios->power_mode, ios->clock, ios->bus_width, slot);	
#endif

	/*Power */
	if (ios->power_mode == MMC_POWER_OFF)
		xlpmmc_set_power(host, 0, slot);
	else if (ios->power_mode == MMC_POWER_ON) {
		xlpmmc_set_power(host, 1, slot);
	}
	msleep(5);
	/* Clock */
	if (ios->clock && ios->clock != host->clock) {
		xlpmmc_set_clock(host, ios->clock, slot);
		host->clock = ios->clock;
	}
	
	/*BUS width*/
	hc_pc_hc = hc_rd16(host->base, HC_PC_HC, slot);
	
	switch (ios->bus_width) {
	case MMC_BUS_WIDTH_4:
		hc_pc_hc |= HC_HCR_4BIT_MODE;
		break;
	case MMC_BUS_WIDTH_1:
		hc_pc_hc &= ~HC_HCR_4BIT_MODE;
		break;
	}
	hc_wr16 (host->base, HC_PC_HC, hc_pc_hc, slot);
	msleep(5);
	dump_hc_regs(host, slot);
}

static void xlpmmc_timeout_timer(long unsigned int data)
{  
        struct xlpmmc_host *host = (struct xlpmmc_host *) data;
        volatile short interrsts;
        int slot = xlpmmc_find_slot(host);

	spin_lock(&host->irq_lock);
        if (host->mrq) {
                    if(host->mrq->cmd) {                
                        if (host->mrq->cmd->data) {
                                host->mrq->data->error = -ETIMEDOUT; 
                                xlpmmc_data_complete(host);
                        } else {                              
			        /*Reset the CMD line*/
			        interrsts =  hc_rd16(host->base, HC_SWRST_TIMEOUT_CTRL, slot);
			        interrsts |= SW_RST_CMD; 
			        hc_wr16(host->base, HC_SWRST_TIMEOUT_CTRL, interrsts, slot);	
                                host->mrq->cmd->error = -ETIMEDOUT; 
                                xlpmmc_cmd_complete(host, 0, slot);
                        }                       
                    }
        }
	spin_unlock(&host->irq_lock);
}

static irqreturn_t xlpmmc_irq(int irq, void *dev_id)
{
        struct xlpmmc_host **xlpmmc_host_data = dev_id;
        volatile short intstatus;
        int count;
        int maxslots = findmaxslots();

        for (count=0; count<maxslots; count ++) { 
	        struct xlpmmc_host *host = xlpmmc_host_data[count];

	        intstatus = hc_rd16(host->base, HC_NORMAL_INT_STS, count);
#ifdef XLP_MMC_DEBUG
	printk("got xlpmmc_irq status = 0x%x\n", intstatus);	
#endif
	        hc_wr16(host->base, HC_NORMAL_INT_STS, intstatus, count);	
	        spin_lock(&host->irq_lock);

	        /*Error Interrupt*/
	        if(intstatus & HNIS_ERR){
		        volatile short interrsts;
		        u16 handle_err=0;
		        handle_err = 0x1; /*TODO : Add error codes here*/
		        interrsts = hc_rd16(host->base, HC_ERROR_INT_STS, count);	  
#ifdef XLP_MMC_DEBUG
        printk("INT ERR: error status  = 0x%x\n", interrsts);
#endif
		        hc_wr16(host->base, HC_ERROR_INT_STS, interrsts, count);
		        if(interrsts & handle_err){	
			        /*Reset the CMD line*/
			        interrsts =  hc_rd16(host->base, HC_SWRST_TIMEOUT_CTRL, count);
			        interrsts |= SW_RST_CMD; 
			        hc_wr16(host->base, HC_SWRST_TIMEOUT_CTRL, interrsts, count);	
			        host->mrq->cmd->error = -ETIMEDOUT;
		        } 
		        xlpmmc_cmd_complete(host, intstatus, count);
		        spin_unlock(&host->irq_lock);
		        return IRQ_HANDLED;
	        }

	        if(intstatus & HNIS_CMD_CMPL){	
		        if (host->status == HOST_S_CMD)
			        xlpmmc_cmd_complete(host, intstatus, count);

	        } 

                if(intstatus & (HNIS_CINS | HNIS_CREM)){
                        tasklet_schedule(&host->card_tasklet);
                }
	        if (!(host->flags & HOST_F_DMA)) {
		        if ((host->flags & HOST_F_XMIT) && (intstatus & HNIS_BUFF_WR_RDY))
			        xlpmmc_send_pio(host, count);
		        else if ((host->flags & HOST_F_RECV) && (intstatus & HNIS_BUFF_RD_RDY))
			        xlpmmc_receive_pio(host, count);
		        else if( ((host->flags & HOST_F_RECV) || (host->flags & HOST_F_XMIT))
                                                                &&(intstatus & HNIS_TC_CMPL))
			        xlpmmc_data_complete(host);
	        }
        
	        if((host->flags & HOST_F_DMA) && (intstatus & HNIS_TC_CMPL)){
		        xlpmmc_data_complete(host);
	        }

	        if(!(intstatus &= (HNIS_ERR 
		        	| HNIS_CMD_CMPL
			        | HNIS_BUFF_WR_RDY | HNIS_BUFF_RD_RDY
			        | HNIS_TC_CMPL | HNIS_DMA ))){
			        //printk("Unhandled status 0x%x\n", intstatus);
	        }
	
	        spin_unlock(&host->irq_lock);
        }
        return IRQ_HANDLED;
}
//static void xlpmmc_tasklet_card(unsigned long param)
static void xlpmmc_tasklet_card(long unsigned int param)
{
        struct xlpmmc_host *host;
        host = (struct xlpmmc_host*)param;
        mmc_detect_change(host->mmc, msecs_to_jiffies(100));
}

static const struct mmc_host_ops xlpmmc_ops = {
	.request	= xlpmmc_request,
	.set_ios	= xlpmmc_set_ios,
	.get_ro		= xlpmmc_card_readonly,
	.get_cd		= xlpmmc_card_inserted,
	//.enable_sdio_irq = xlpmmc_enable_sdio_irq,
};

static int __devinit xlpmmc_probe(struct platform_device *pdev)
{
	struct mmc_host *mmc=NULL;
        struct xlpmmc_host **xlpmmc_host_data;
	struct xlpmmc_host *host=NULL;
	struct resource *r;
	int count, irq, ret=0;
        void  __iomem         *base;
        void* ioarea;
        int maxslots = findmaxslots();

        xlpmmc_host_data = kmalloc(sizeof(struct xlpmmc_host*) *maxslots, GFP_KERNEL);

	r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!r) {
		dev_err(&pdev->dev, "no mmio defined\n");
		goto out1;
	}

	ioarea = request_mem_region(r->start, r->end - r->start + 1,
					   pdev->name);
	if (!ioarea) {
		dev_err(&pdev->dev, "mmio already in use\n");
		goto out1;
	}

	base = (void *)ioremap_nocache(r->start, PAGE_SIZE);
	if (!base) {
		dev_err(&pdev->dev, "cannot remap mmio\n");
		goto out2;
	}

	r = platform_get_resource(pdev, IORESOURCE_IRQ, 0);
	if (!r) {
		dev_err(&pdev->dev, "no IRQ defined\n");
		goto out3;
	}


	hc_wr32(base, HC_SYSCTRL,0x1C, 0);
        msleep(5);

        for (count=0; count<maxslots; count ++) {                                                            
	        mmc = mmc_alloc_host(sizeof(struct xlpmmc_host), &pdev->dev);
	        if (!mmc) {
		        dev_err(&pdev->dev, "no memory for mmc_host\n");
		        ret = -ENOMEM;
		        goto out0;
	        }

	        host = mmc_priv(mmc);
	        host->mmc = mmc;
                host->base = base;
	        spin_lock_init(&host->irq_lock);
                xlpmmc_host_data[count] = host;
                host->host_array = xlpmmc_host_data;
                /* S1: Clear any set INT Bits */
                hc_wr32(base, HC_NORMAL_INT_STS, hc_rd32(base, HC_NORMAL_INT_STS, count), count);
        }

	irq = r->start;
	ret = request_irq(irq, xlpmmc_irq, IRQF_SHARED,
			  "xlp-mmc", xlpmmc_host_data);
	if (ret) {
		dev_err(&pdev->dev, "cannot grab IRQ\n");
		goto out3;
	}
        
        ret = request_irq(xlp_irt_to_irq(0, XLP_GPIO_INT0_IRT), xlpmmc_det_irq,
                IRQF_SHARED, "mmc-gpio", xlpmmc_host_data);
        if (ret) {
                dev_warn(&pdev->dev, "request MMC detect irq failed\n");
                free_irq(xlp_irt_to_irq(0, XLP_GPIO_INT0_IRT), xlpmmc_host_data);
        }
        
        for (count=0; count<maxslots; count ++) {
                host = xlpmmc_host_data[count];
                mmc  = host->mmc;
	        host->platdata = pdev->dev.platform_data;
	        host->pdev = pdev;
                host->slot=count;
                host->irq = irq;
                host->base = base;
                host->ioarea = ioarea;
                tasklet_init(&host->card_tasklet,
                        xlpmmc_tasklet_card, (unsigned long)host);
                setup_timer(&host->timer, xlpmmc_timeout_timer, (unsigned long)host);

	        ret = -ENODEV;
                        
                if (gpio_is_valid(GPIO_MMC_DETECT + count)) {
                        if (gpio_request(GPIO_MMC_DETECT+count, "mmc_detect")) {
                                pr_debug("no detect pin available\n");
                        }
                }
        
                gpio_regwrite(0, XLP_GPIO_INTEN00,   gpio_regread(0, XLP_GPIO_INTEN00) | 
                                0x1<<(GPIO_MMC_DETECT));
                gpio_regwrite(0, XLP_8XX_GPIO_INT_POLAR0, gpio_regread(0, XLP_8XX_GPIO_INT_POLAR0) | 
                                0x1<<(GPIO_MMC_DETECT+count));
                gpio_regwrite(0, XLP_8XX_GPIO_INT_TYPE0, gpio_regread(0, XLP_8XX_GPIO_INT_TYPE0) | 
                                0x1<<(GPIO_MMC_DETECT+count));

	        mmc->ops = &xlpmmc_ops;

	        mmc->f_min =     1039000;
	        mmc->f_max =   133000000;  

	        mmc->max_blk_size = 512;
	        mmc->max_blk_count = 2048;

	        mmc->max_seg_size = XLPMMC_DESCRIPTOR_SIZE;
	        mmc->max_phys_segs = XLPMMC_DESCRIPTOR_COUNT;	
	
	        /* Enable DMA mode. Defualt is PIO*/
	        host->flags |= HOST_F_DMA;

	        mmc->ocr_avail = XLPMMC_OCR; /* volt 2.70 ~ 3.60 */
	
	        mmc->caps = MMC_CAP_4_BIT_DATA;

	        host->status = HOST_S_IDLE;

	        xlpmmc_reset_controller(host, count);
                msleep(5);

	        ret = mmc_add_host(mmc);
	        if (ret) {
		        dev_err(&pdev->dev, "cannot add mmc host\n");
		        goto out6;
	        }

	        printk(KERN_INFO "xlp-mmc " ": MMC Controller %d set up at %p"
		        " (mode=%s)\n", pdev->id, host->base,
		        host->flags & HOST_F_DMA ? "dma" : "pio");
                //mdelay(5000);
        }
	platform_set_drvdata(pdev, xlpmmc_host_data);

	return 0;	/*Everything is OK */

out6:
	/*Disable the host if init fails*/
	mmc_remove_host(host->mmc);
        
        for (count=0; count<maxslots; count ++) { 
	        xlpmmc_set_power(host, 0, count);
                if (gpio_is_valid(GPIO_MMC_DETECT+count)) {
                        gpio_free(GPIO_MMC_DETECT+count);
                }
        }
	free_irq(xlp_irt_to_irq(0, XLP_GPIO_INT0_IRT), host);
	free_irq(host->irq, host);
out3:
	iounmap((void *)host->base);
out2:
	release_resource(host->ioarea);
	kfree(host->ioarea);
out1:
	mmc_free_host(mmc);
out0:
	return ret;
}

static int __devexit xlpmmc_remove(struct platform_device *pdev)
{
	void   *data = platform_get_drvdata(pdev);
        struct xlpmmc_host **xlpmmc_host_data = data;
        int count;
        int maxslots = findmaxslots();
	
        for (count=0; count<maxslots; count ++) { 
            struct xlpmmc_host *host = xlpmmc_host_data[count];
	    if (host) {
		mmc_remove_host(host->mmc);
		
		xlpmmc_set_power(host, 0, count);
                if (gpio_is_valid(GPIO_MMC_DETECT+count)) {
                        gpio_free(GPIO_MMC_DETECT+count);
                }
                del_timer_sync(&host->timer);
                tasklet_kill(&host->card_tasklet);
		
		free_irq(xlp_irt_to_irq(0, XLP_GPIO_INT0_IRT), host);
		free_irq(host->irq, host);
		iounmap((void *)host->base);
		release_resource(host->ioarea);
		kfree(host->ioarea);

		mmc_free_host(host->mmc);
		platform_set_drvdata(pdev, NULL);
	
            }
        }
	return 0;
}

#ifdef CONFIG_PM
static int xlpmmc_suspend(struct platform_device *pdev, pm_message_t state)
{
	//TODO: Implement if required. 
	return 0;
}

static int xlpmmc_resume(struct platform_device *pdev)
{
	//TODO: Implement if required. 
	return 0;
}
#else
#define xlpmmc_suspend NULL
#define xlpmmc_resume NULL
#endif

static struct platform_driver xlpmmc_driver = {
	.probe         = xlpmmc_probe,
	.remove        = xlpmmc_remove,
	.suspend       = xlpmmc_suspend,
	.resume        = xlpmmc_resume,
	.driver        = {
		.name  = "mmc-xlp",
		.owner = THIS_MODULE,
	},
};

static int __init xlpmmc_init(void)
{
	return platform_driver_register(&xlpmmc_driver);
}

static void __exit xlpmmc_exit(void)
{
	platform_driver_unregister(&xlpmmc_driver);
}

module_init(xlpmmc_init);
module_exit(xlpmmc_exit);

MODULE_AUTHOR("Netlogic Microsystems");
MODULE_DESCRIPTION("MMC/SD driver for the XLP");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:xlp-mmc");
