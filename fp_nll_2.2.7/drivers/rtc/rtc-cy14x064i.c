/*
 * An I2C driver for the Cypress Semiconductor CY14X064I RTC
 *
 * File   : drivers/rtc/rtc-cy14x064i.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/rtc.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/bcd.h>
#include <linux/delay.h>

#define DRV_VERSION "0.1"

#define CY14X064I_REG_RTC_FLG   0x00  /* Ctrl & Status */
#define CY14X064I_REG_RTC_CEN   0x01  /* Century */

#define CY14X064I_REG_RTC_ASC   0x02  /* Alarm */
#define CY14X064I_REG_RTC_AMN   0x03
#define CY14X064I_REG_RTC_AHR   0x04
#define CY14X064I_REG_RTC_ADM   0x05

#define CY14X064I_REG_RTC_INT   0x06  /* Interrupts */
#define CY14X064I_REG_RTC_WDG   0x07  /* Watchdog */
#define CY14X064I_REG_RTC_CLB   0x08  /* Calibration */

#define CY14X064I_REG_RTC_SC    0x09  /* Datetime : Seconds */
#define CY14X064I_REG_RTC_MN    0x0A  /* Datetime : Minutes */
#define CY14X064I_REG_RTC_HR    0x0B  /* Datetime : Hours (24 HRS) */
#define CY14X064I_REG_RTC_DW    0x0C  /* Datetime : Day of Week */
#define CY14X064I_REG_RTC_DM    0x0D  /* Datetime : Day of Month */
#define CY14X064I_REG_RTC_MO    0x0E  /* Datetime : Month of Year */
#define CY14X064I_REG_RTC_YR    0x0F  /* Datetime : Year */

#define CY14X064I_FLAG_RTC_OSC  0x10
#define CY14X064I_FLAG_RTC_BPF  0x08
#define CY14X064I_FLAG_RTC_CAL  0x04
#define CY14X064I_FLAG_RTC_WR   0x02
#define CY14X064I_FLAG_RTC_RD   0x01

#define CY14X064I_REG_CTRL_CMD   0xAA  /* Command Reigster Address */
#define CY14X064I_CTRL_CMD_STOR  0x3C  /* STORE SRAM data to nonvolatile memory */
#define CY14X064I_CTRL_CMD_RECL  0x60  /* RECALL data from nonvolatile memory to SRAM */
#define CY14X064I_CTRL_CMD_ASEN  0x59  /* Enable AutoStore */
#define CY14X064I_CTRL_CMD_ASDS  0x19  /* Disable AutoStore */
#define CY14X064I_CTRL_CMD_SLEP  0xB9  /* Enter Sleep Mode for low power consumption */

#define CY14X064I_SW_STORE_DELAY 8 /* CY14X064I Spec: 8 msec */

struct cy14x064i {
  struct rtc_device *rtc;
};

static struct i2c_driver cy14x064i_driver;


static __inline__ int cy14x064i_rtc_set_regi(struct i2c_client *client, unsigned char regi, unsigned char *value)
{
  unsigned char buf[2] = {regi, *value};

  return ARRAY_SIZE(buf) == i2c_master_send(client, (char *)buf, ARRAY_SIZE(buf)) ? 0 : -EIO;
}

static __inline__ int cy14x064i_rtc_get_regi(struct i2c_client *client, unsigned char regi, unsigned char *value)
{
  unsigned char addr[1] = { regi };
  struct i2c_msg msgs[2] = {
    {
      .addr = client->addr,
      .flags = 0, /* write: setup read ptr */
      .len = 1,
      .buf = addr,
    }, {
      .addr = client->addr,
      .flags = I2C_M_RD, /* read date & time */
      .len = 1,
      .buf = value,
    }
  };

  return ARRAY_SIZE(msgs) == i2c_transfer(client->adapter, msgs, ARRAY_SIZE(msgs)) ? 0 : -EIO;
}

static int cy14x064i_rtc_sw_store(struct i2c_client *client)
{
	unsigned char i2c_sw_store_buf[2] = { CY14X064I_REG_CTRL_CMD, CY14X064I_CTRL_CMD_STOR };
	struct i2c_msg msgs[1] = {
		{
			.addr	  = 0x18, /* Control Register Slave Address */
			.flags	= 0,    /* write */
			.len	  = sizeof(i2c_sw_store_buf),
			.buf	  = i2c_sw_store_buf
		}
	};

  dev_dbg(&client->dev, "%s: Software Store Sequence: Control Slave addr_buf# 0x%02X Cmd RegAddr# 0x%02X Cmd# 0x%02X\n",
          __FUNCTION__, msgs[0].addr, i2c_sw_store_buf[0], i2c_sw_store_buf[1]);

	msleep(1);
	if (ARRAY_SIZE(msgs) != i2c_transfer(client->adapter, msgs, ARRAY_SIZE(msgs)))
  {
		dev_err(&client->dev, "%s: Software Store Sequence Error!\n", __FUNCTION__);
		return -EIO;
	}

	msleep(CY14X064I_SW_STORE_DELAY);

  return 0;
}
static __inline__ int cy14x064i_get_ctrl_sts(struct i2c_client *client, unsigned char *ctrl_sts)
{
  unsigned char addr[1] = { CY14X064I_REG_RTC_FLG };
  struct i2c_msg msgs[2] = {
    {
      .addr = client->addr,
      .flags = 0, /* write: setup read ptr */
      .len = 1,
      .buf = addr,
    }, {
      .addr = client->addr,
      .flags = I2C_M_RD, /* read date & time */
      .len = 1,
      .buf = ctrl_sts,
    }
  };
  if (ARRAY_SIZE(msgs) != i2c_transfer(client->adapter, msgs, ARRAY_SIZE(msgs))){
		dev_err(&client->dev, "%s: Ctrl-Sts Regi Read Error!\n", __FUNCTION__);
		return -EIO;
	}

  if (*ctrl_sts & 0xFF)
  {
    dev_dbg(&client->dev, "%s : Ctrl-Sts Value# 0x%X\n", __FUNCTION__, *ctrl_sts);

		if (*ctrl_sts & CY14X064I_FLAG_RTC_OSC)
      dev_warn(&client->dev, "Oscillator is enabled and not running in the first 5 ms of operation.\n"
            "This indicates that RTC backup power failed and clock value is no longer valid!!!\n"
            "Clear Bit OSCEN @ Regi 0x08 and OSC @ Regi 0x00 to enable RTC Function with Osc Running.\n");
		if (*ctrl_sts & CY14X064I_FLAG_RTC_RD)
      dev_warn(&client->dev, "RTC Read is Disabled!!! "
            "Clear Bit R @ Regi 0x00 to enable RTC Read Function.\n");
		if (*ctrl_sts & CY14X064I_FLAG_RTC_WR)
      dev_warn(&client->dev, "RTC Write is Disabled!!! "
            "Clear Bit W @ Regi 0x00 to enable RTC Write Function.\n");
		if (*ctrl_sts & CY14X064I_FLAG_RTC_CAL)
      dev_warn(&client->dev, "RTC Calibration Mode is SET with a 512 Hz square wave is output on the INT pin.!!!\n"
            "Clear Bit CAL @ Regi 0x00 to disable Calibration Mode.\n");
		if (*ctrl_sts & CY14X064I_FLAG_RTC_BPF)
      dev_warn(&client->dev, "RTC Backup Power Failed!!! "
            "Clear Bit BPF @ Regi 0x00 to enable RTC Write Function.\n");
  }
	return 0;
}

static __inline__ int cy14x064i_set_ctrl_sts(struct i2c_client *client, unsigned char *ctrl_sts)
{
  unsigned char buf[2] = {CY14X064I_REG_RTC_FLG, *ctrl_sts};

  return ARRAY_SIZE(buf) == i2c_master_send(client, (char *)buf, ARRAY_SIZE(buf)) ? 0 : -EIO;
}


static int cy14x064i_get_datetime(struct i2c_client *client, struct rtc_time *dt)
{
  unsigned char buf[7] = {0}, addr[1] = { CY14X064I_REG_RTC_SC },  century = 0;
  struct i2c_msg msgs[2] = {
    {
      .addr = client->addr ,
      .flags = 0, /* write: setup read ptr */
      .len = 1,
      .buf = addr,
    }, {
      .addr = client->addr,
      .flags = I2C_M_RD, /* read date & time */
      .len = 7,
      .buf = buf,
    }
  };

  dev_dbg(&client->dev, "%s : I2C Adapter Name# %s I2C Client Name# %s Client Addr# 0x%X: Recived GET Req\n",
            __FUNCTION__, client->adapter->name, client->name, client->addr);
  dev_dbg(&client->dev, "Start Reading from Address# addr = 0x%02X msg.buf = 0x%02X\n", addr[0], *msgs[0].buf);

  if (ARRAY_SIZE(msgs) != i2c_transfer(client->adapter, msgs, ARRAY_SIZE(msgs)))
  {
		dev_err(&client->dev, "%s: RTC Date-Time Regi Read Error!\n", __FUNCTION__);
		return -EIO;
	}

  cy14x064i_rtc_get_regi(client, CY14X064I_REG_RTC_CEN, &century);
	msleep(1);

  dt->tm_sec  = bcd2bin(buf[0] & 0x7F);
  dt->tm_min  = bcd2bin(buf[1] & 0x7F);
  dt->tm_hour = bcd2bin(buf[2] & 0x3F);
  dt->tm_wday = bcd2bin(buf[3] & 0x07) - 1;
  dt->tm_mday = bcd2bin(buf[4] & 0x3F);
  dt->tm_mon  = bcd2bin(buf[5] & 0x1F) - 1;
  dt->tm_year = bcd2bin(buf[6] & 0xFF) + century * 100;

  dev_dbg(&client->dev, "%s secs=%d, mins=%d, "
        "hours=%d, mday=%d, mon=%d, year=%d, wday=%d\n",
        __FUNCTION__, dt->tm_sec, dt->tm_min,
        dt->tm_hour, dt->tm_mday,
        dt->tm_mon, dt->tm_year, dt->tm_wday);

  dev_dbg(&client->dev, "%s: Regi[0x09] = 0x%02X Regi[0x0A] = 0x%02X Regi[0x0B] = 0x%02X "
        "Regi[0x0C] = 0x%02X Regi[0x0D] = 0x%02X Regi[0x0E] = 0x%02X Regi[0x0F] = 0x%02X\n",
        __FUNCTION__, buf[0], buf[1], buf[2], buf[3],
        buf[4], buf[5], buf[6]);

  return 0;
}

static int cy14x064i_set_datetime(struct i2c_client *client, struct rtc_time *dt)
{
  unsigned char ctrl_sts, buf[8];

  dev_dbg(&client->dev, "%s : I2C Adapter Name# %s I2C Client Name# %s Client Add# 0x%X: Recived SET Req\n",
          __FUNCTION__, client->adapter->name, client->name, client->addr);

  buf[0] = CY14X064I_REG_RTC_SC;
  buf[1] = bin2bcd(dt->tm_sec) & 0x7F;
  buf[2] = bin2bcd(dt->tm_min) & 0x7F;
  buf[3] = bin2bcd(dt->tm_hour) & 0x3F;
  buf[4] = bin2bcd(dt->tm_wday + 1) & 0x07;
  buf[5] = bin2bcd(dt->tm_mday) & 0x3F;
  buf[6] = bin2bcd(dt->tm_mon + 1) & 0x1F;
  buf[7] = bin2bcd(dt->tm_year % 100) & 0xFF;

  /* Disable RTC Regi Updates */
  ctrl_sts = 0x02;
  if (cy14x064i_set_ctrl_sts(client, &ctrl_sts))
  {
		dev_warn(&client->dev, "%s : Failed to Write i2c Ctrl-Sts Regi\n", __FUNCTION__);
    return -EIO;
  }
  if (ARRAY_SIZE(buf) != i2c_master_send(client, (char *)buf, ARRAY_SIZE(buf)))
  {
    dev_err(&client->dev, "%s : i2cWrite master_send Failed\n", __FUNCTION__);
    return -EIO;
  }

  /* Enable RTC Regi Updates */
  ctrl_sts = 0x00;
  if (cy14x064i_set_ctrl_sts(client, &ctrl_sts))
  {
		dev_err(&client->dev, "%s : Failed to Write i2c Ctrl-Sts Regi\n", __FUNCTION__);
    return -EIO;
  }
  /* Enable Software Store: Uncomment below line if Auto Store is Disabled or No Hardware Store is Supported */
  /* cy14x064i_rtc_sw_store(client); */
  return 0;
}

static int cy14x064i_rtc_read_time(struct device *dev, struct rtc_time *tm)
{
  return cy14x064i_get_datetime(to_i2c_client(dev), tm);
}

static int cy14x064i_rtc_set_time(struct device *dev, struct rtc_time *tm)
{
  struct i2c_client *client = to_i2c_client(dev);

  return cy14x064i_set_datetime(client, tm);
}

static const struct rtc_class_ops cy14x064i_rtc_ops = {
  .read_time  = cy14x064i_rtc_read_time,
  .set_time  = cy14x064i_rtc_set_time,
};
static int __devinit cy14x064i_probe(struct i2c_client *client,
                                    const struct i2c_device_id *id)
{
  struct cy14x064i *cy14x064i;
  int err;
  unsigned char ctrl_sts = 0;

  dev_dbg(&client->dev, "%s : Entered probing for RTC to i2c Functionality\n", __FUNCTION__);

  if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
  {
    dev_err(&client->dev, "%s : Failed to i2c Functionality\n", __FUNCTION__);
    return -ENODEV;
  }
  dev_dbg(&client->dev, "%s : i2c Adapter Name %s\n", __FUNCTION__, client->adapter->name);

  cy14x064i = kzalloc(sizeof(struct cy14x064i), GFP_KERNEL);
  if (!cy14x064i)
    return -ENOMEM;

  dev_dbg(&client->dev, "%s : I2C Adapter Name# %s\n", __FUNCTION__, client->adapter->name);
  cy14x064i->rtc = rtc_device_register(client->name, &client->dev,
                                        &cy14x064i_rtc_ops, THIS_MODULE);

  dev_dbg(&client->dev, "Registered the class device as %s with Adapter %s with I2C-%d\n",
          client->name, client->adapter->name, client->adapter->nr);
  if (IS_ERR(cy14x064i->rtc)) {
    err = PTR_ERR(cy14x064i->rtc);
    dev_err(&client->dev, "unable to register the class device\n");
    i2c_set_clientdata(client, NULL);
    kfree(cy14x064i);
    return err;
  }
  i2c_set_clientdata(client, cy14x064i);

  cy14x064i_rtc_get_regi(client, CY14X064I_REG_RTC_CLB, &ctrl_sts);
  if (ctrl_sts & 0x80)
  {
    dev_warn(&client->dev, "Oscillator not Running!!! "
            "Clear Bit OSCEN @ Regi 0x08 to enable Oscillator & RTC Function.\n");
    ctrl_sts = 0;
    cy14x064i_rtc_set_regi(client, CY14X064I_REG_RTC_CLB, &ctrl_sts);
    msleep(2000); /* Time Delay Required for Oscillator to Start on Power Cycle once Enabled. */
  }

  /* Read control-status register */
	cy14x064i_get_ctrl_sts(client, &ctrl_sts);

  if (ctrl_sts & 0xFF)
  {
    dev_dbg(&client->dev, "%s : Clearing Ctrl-Sts Regi 0x00\n", __FUNCTION__);
    ctrl_sts = 0x00;
    cy14x064i_set_ctrl_sts(client, &ctrl_sts);
  }

  return 0;
}

static int __devexit cy14x064i_remove(struct i2c_client *client)
{
  struct cy14x064i *cy14x064i = i2c_get_clientdata(client);

  if (cy14x064i->rtc)
    rtc_device_unregister(cy14x064i->rtc);
	i2c_set_clientdata(client, NULL);
  kfree(cy14x064i);
  return 0;
}

static const struct i2c_device_id cy14x064i_id[] = {
  { "cy14x064i", 0 },
  { }
};
MODULE_DEVICE_TABLE(i2c, cy14x064i_id);

static struct i2c_driver cy14x064i_driver = {
  .driver = {
    .name  = "cy14x064i",
    .owner  = THIS_MODULE,
  },
  .probe    = cy14x064i_probe,
  .remove    = __devexit_p(cy14x064i_remove),
  .id_table  = cy14x064i_id,
};

static __init int cy14x064i_init(void)
{
  return i2c_add_driver(&cy14x064i_driver);
}

static __exit void cy14x064i_exit(void)
{
  i2c_del_driver(&cy14x064i_driver);
}

module_init(cy14x064i_init);
module_exit(cy14x064i_exit);

MODULE_AUTHOR("Vinaykumar Masane <vinaykumar.masane@broadcom.com>");
MODULE_DESCRIPTION("CY14X064I I2C RTC driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
