/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */


#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>

#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>

#include <asm/io.h>
#ifdef CONFIG_NLM_XLP
#include <asm/netlogic/xlp8xx/cpu.h>
#include <asm/netlogic/hal/nlm_hal.h>
#include <asm/netlogic/xlp.h>
#endif

#undef NOR_DEBUG

#define XLP_NOR_BASEADDR	0x40
#define XLP_NOR_SIZE 		0x01000000 /* 16MB */
#define XLP_NOR_WIDTH		2 /* 16-bits */

struct xlp_nor_info {
        struct mtd_partition    *parts;
        struct mtd_info         *mtd;
        struct map_info         map;
};

static struct mtd_partition xlp_nor_partitions[] = {
        {
                .name   = "X-Loader(RO)",
                .offset = 0,
                .size   = 0x100000,     /* 1M */
                .mask_flags = MTD_WRITEABLE,
        },
        {
                .name   = "U-boot(RW)",
                .offset = 0x100000,
                .size   = 0x60000,      /* 384k */
        },
        {
                .name   = "DTB(RW)",
                .offset = 0x160000,
                .size   = 0x20000,      /* 128K */
        },
        {
                .name   = "Kernel(RW)",
                .offset = 0x180000,
                .size   = 0x580000,     /* 5.5M */
        },
        {
                .name   = "Rootfs(RW)",
                .offset = 0x700000,
                .size   = 0x800000,     /* 8M */
        },
        {
                .name   = "Env(RO)",
                .offset = 0xf00000,     /* 1M */
                .size   = MTDPART_SIZ_FULL,
                .mask_flags = MTD_WRITEABLE,
        },
};

static __inline__ int32_t nor_reg_read(int node,  int regidx)
{
        volatile uint64_t mmio;
        mmio = nlm_hal_get_dev_base(node, 0, XLP_PCIE_SPI_NOR_FLASH_DEV, XLP_PCIE_SPI_NOR);
        return nlm_hal_read_32bit_reg(mmio, regidx);
}
static __inline__ void nor_reg_write(int node, int regidx, int32_t val)
{
        volatile uint64_t mmio;
        mmio = nlm_hal_get_dev_base(node, 0, XLP_PCIE_SPI_NOR_FLASH_DEV, XLP_PCIE_SPI_NOR);
        nlm_hal_write_32bit_reg(mmio, regidx, val);
}

#ifdef NOR_DEBUG
static void nor_dump_reg(void)
{
        int i;

	printk("\nNor Flash memory interface chip select:\n");
        for(i = 0x0; i < 0x7; i++)
        {
                printk("nor flash:  0x%0x = 0x%8x\n", i, nor_reg_read(0, i));
        }

        for(i = 0x40; i < 0x47; i++)
        {
                printk("base addr for cs:%d  0x%0x = 0x%8x\n", i & 0x0f, i, nor_reg_read(0, i));
        }

        for(i = 0x48; i < 0x4f; i++)
        {
                printk("addr limit  cs:%d  0x%0x = 0x%8x\n", i & 0x0f, i, nor_reg_read(0, i));
        }

        for(i = 0x50; i < 0x57; i++)
        {
                printk("device parameter for cs:%d  0x%0x = 0x%8x\n", i & 0x0f, i, nor_reg_read(0, i));
        }

        printk("Nor flash system control 0x68= 0x%8x\n", nor_reg_read(0, 0x68));
}
#endif


int __init xlp_nor_probe(struct platform_device *pdev)
{
	struct xlp_nor_info *info;
	int nb_parts, err;

	info = kzalloc(sizeof(struct xlp_nor_info), GFP_KERNEL);
	if (!info)
		return -ENOMEM;

	info->map.name		= dev_name(&pdev->dev);
	info->map.bankwidth	= XLP_NOR_WIDTH;
	info->map.phys		= nor_reg_read(0, XLP_NOR_BASEADDR) << 8;
	info->map.size		= XLP_NOR_SIZE;
	info->map.virt		= ioremap(info->map.phys, XLP_NOR_SIZE);
	if(!info->map.virt) {
		err = -ENOMEM;
		goto out_info;
	}
	info->mtd 		= do_map_probe("cfi_probe", &info->map);
	if (!info->mtd) {
		err = -ENXIO;
		goto out_unmap;
	}
	info->mtd->owner	= THIS_MODULE;
	info->mtd->dev.parent	= &pdev->dev;
	
	if (mtd_has_partitions()) {
		if (mtd_has_cmdlinepart()) {
                        static const char *part_probes[]
                                        = { "cmdlinepart", NULL, };
			nb_parts = parse_mtd_partitions(info->mtd, part_probes, &info->parts, 0);
		}
		if(nb_parts <= 0)
		{
			nb_parts = ARRAY_SIZE(xlp_nor_partitions);
			if(!info->parts)
				info->parts = xlp_nor_partitions;
		}
		if(nb_parts > 0)
		{
			err = add_mtd_partitions(info->mtd, info->parts, nb_parts);
		}
	}
	else {
		err = add_mtd_device(info->mtd);
	}

	platform_set_drvdata(pdev, info);

	return 0;
out_unmap:
	iounmap(info->map.virt);
out_info:
	kfree(info);
	return err;
}

static int __exit xlp_nor_remove(struct platform_device *pdev)
{
	struct xlp_nor_info* info = platform_get_drvdata(pdev);

	platform_set_drvdata(pdev, NULL);

	if (info) {
		if(info->parts) {
			del_mtd_partitions(info->mtd);
			kfree(info->parts);
		}
		else
			del_mtd_device(info->mtd);
		map_destroy(info->mtd);
		iounmap(info->map.virt);
		kfree(info);
	}
	return 0;
}

static struct platform_driver xlp_nor_driver = {
        .probe          = xlp_nor_probe,
        .remove         = xlp_nor_remove,
        .driver         = {
                .name   = "nor-xlp",
                .owner  = THIS_MODULE,
        },
};

#define GBU_CS0_BASEADDRESS_REG         0
#define GBU_CS1_BASEADDRESS_REG         1
#define GBU_CS0_BASELIMIT_REG           8
#define GBU_CS1_BASELIMIT_REG           9

static int __init xlp_nor_init(void)
{
	uint32_t base, limit;
	u32 nandboot;
	volatile u32 *mmio = cpu_io_mmio(0, SYS);

	nandboot = (nlm_hal_read_32bit_reg((uint64_t)mmio,1) & 0xF) == 6 ? 1 : 0;

	mmio = cpu_io_mmio(0, GBU);
	if (nandboot)	{
		base = nlm_hal_read_32bit_reg((uint64_t)mmio,GBU_CS1_BASEADDRESS_REG);
		limit = nlm_hal_read_32bit_reg((uint64_t)mmio,GBU_CS1_BASELIMIT_REG); 
	}
	else	{
		base = nlm_hal_read_32bit_reg((uint64_t)mmio,GBU_CS0_BASEADDRESS_REG);
		limit = nlm_hal_read_32bit_reg((uint64_t)mmio,GBU_CS0_BASELIMIT_REG); 
	}

	if((base == 0xFFFFFFFF) && (limit == 0))	{
		return -1;
	}

        return platform_driver_register(&xlp_nor_driver);
}

static void __exit xlp_nor_exit(void)
{
        platform_driver_unregister(&xlp_nor_driver);
}


module_init(xlp_nor_init);
module_exit(xlp_nor_exit);

MODULE_AUTHOR("Netlogic Microsystems");
MODULE_DESCRIPTION("Netlogic xlp NOR MTD driver");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.1");
MODULE_ALIAS("platform:xlp-nor");
