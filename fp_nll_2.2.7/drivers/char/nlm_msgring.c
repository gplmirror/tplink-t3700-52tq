/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/smp_lock.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/vmalloc.h>
#include <linux/smp_lock.h>
#include <linux/mman.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <asm/netlogic/msgring.h>

#define MSGRING_WAIT_CHRDEV_NAME "nlm_msgring"
static int msgring_major;
static wait_queue_head_t msgring_wq[NR_CPUS];
static int msgring_status[NR_CPUS];
static int msgring_timeout[NR_CPUS];

#define NLM_MSGRING_WAIT_IOC 'm'
#define NLM_MSGRING_WAIT_VC   _IOWR(NLM_MSGRING_WAIT_IOC, 1, unsigned int *)
#define NLM_MSGRING_WAIT_TIMEOUT   _IOWR(NLM_MSGRING_WAIT_IOC, 2, unsigned int *)
extern unsigned int intr_vc_mask[];


extern int nlm_xlp_register_intr_vc_handler(int (*handler)(int vc));
extern int nlm_xlp_register_intr_vc(int cpu, int vc);

static int msgring_event(int vc)
{
	int cpu = hard_smp_processor_id();
	if(msgring_status[cpu] != 1)
		return 0;
	msgring_status[cpu] = 0;
	wake_up_interruptible(&msgring_wq[cpu]);
	return 0;
}

static ssize_t msgring_read (struct file *filp, char __user *buf, size_t count, loff_t *offset)
{
	int cpu = hard_smp_processor_id();
	unsigned int val;
	unsigned long flags;

#ifdef CONFIG_32BIT
	unsigned long mflags;
#endif

	local_irq_save(flags);
#ifdef CONFIG_32BIT
	msgrng_access_enable(mflags);
#endif
	
	msgring_status[cpu] = 1;

	/* Enable intr on the vcs */
	/* Need write vcmask into the register */
	val =  _read_32bit_cp2_register(XLP_MSG_STATUS1_REG);
	val |= (intr_vc_mask[cpu] << 16);
	_write_32bit_cp2_register(XLP_MSG_STATUS1_REG, val);

#ifdef CONFIG_32BIT
	msgrng_access_disable(mflags);
#endif
	local_irq_restore(flags);
	
	if(msgring_timeout[cpu] < 0)
		wait_event_interruptible(msgring_wq[cpu], (msgring_status[cpu] == 0));
	else
		wait_event_interruptible_timeout(msgring_wq[cpu], (msgring_status[cpu] == 0), msgring_timeout[cpu]);
	return 1;
}

static int msgring_open (struct inode *inode, struct file *filp)
{
	return 0;
}

static int msgring_release (struct inode *inode, struct file *filp)
{
	return 0;
}

static int msgring_ioctl(struct inode *inode, struct file *filp, unsigned int cmd,
		   unsigned long arg)
{
	int err = 0, rv, vc = -1;
	unsigned int  *ptr = (unsigned int *) arg;
	int cpu, timeout;

	switch (cmd) {
		case NLM_MSGRING_WAIT_VC:
			rv = copy_from_user(&vc, ptr, sizeof(*ptr));
			if(vc < 0 || vc > 3) {
				printk("Error in %s, Invald vc %d\n", __FUNCTION__, vc);
				err = -EINVAL;
				break;
			}
			preempt_disable();
			cpu = hard_smp_processor_id();
			nlm_xlp_register_intr_vc(cpu, vc);
			preempt_enable();
			break;

		case NLM_MSGRING_WAIT_TIMEOUT:
			rv = copy_from_user(&timeout, ptr, sizeof(*ptr));
			preempt_disable();
			cpu = hard_smp_processor_id();
			msgring_timeout[cpu] = timeout;
			preempt_enable();
			printk("timeout for cpu %d is %d\n", cpu, timeout);
			break;
		default:
			printk("Invalid cmd in %s\n", __FUNCTION__);
			err = -EINVAL;
	}	
	return err;
}

static long msgring_compat_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	unsigned long ret = -1;
	lock_kernel();
	ret = msgring_ioctl(NULL,filp,cmd,arg);
	unlock_kernel();
	if(ret){
		printk("msgring_ioctl returned with an error %lx", ret);
		return -ENOIOCTLCMD;
	}
	return ret;
}

static struct file_operations msgring_fops = {
	owner:		THIS_MODULE,
	read:		msgring_read,
	open:  		msgring_open,
	ioctl:		msgring_ioctl,
	compat_ioctl:   msgring_compat_ioctl,
	release:        msgring_release,
};

static int msgring_init(void)
{
	int ret, i;

	for(i = 0; i < NR_CPUS; i++) {
		init_waitqueue_head(&msgring_wq[i]);
		msgring_timeout[i] = -1;
	}

	nlm_xlp_register_intr_vc_handler(msgring_event);

	ret = register_chrdev (0, MSGRING_WAIT_CHRDEV_NAME, &msgring_fops);
	if (ret < 0) {
		printk("[%s] Failed to register msgring char device major=%d\n",
				__FUNCTION__, 0);
		return -EIO;
	}
	msgring_major = ret;
	printk("[%s] Registered nlm_msgring char device major=%d\n",
			__FUNCTION__, msgring_major);
	return 0;
}

static void msgring_exit(void)
{
	unregister_chrdev (msgring_major, MSGRING_WAIT_CHRDEV_NAME);
}

	


module_init(msgring_init);
module_exit(msgring_exit);
MODULE_AUTHOR("Netlogic");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Msgring driver");
