/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */



#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/i2c.h>
#include <linux/slab.h>

#include <asm/netlogic/hal/nlm_hal.h>
#include <asm/netlogic/xlp.h>

#define I2C_CLKFREQ_HZ                 133333333 /* 133.333 MHz */
#define I2C_TIMEOUT                    500000

static int i2c_speed=45; 		/* KHz */
module_param(i2c_speed,int,0);

struct i2c_xlp_data {
	int 	node;
	int	bus;
	int	func;
	int	speed;	/* KHz */
	int	xfer_timeout;
	int	ack_timeout;
	uint64_t* ioreg;
	struct i2c_adapter adap;
	struct resource *ioarea;
};

static __inline__ int32_t i2c_reg_read(int node, int func, int regidx)
{
        volatile uint64_t mmio;
        mmio = nlm_hal_get_dev_base(node, 0, XLP_PCIE_GIO_DEV, func)+0x100;
	if(is_nlm_xlp2xx())
                return nlm_hal_read_32bit_reg(mmio+0x20, regidx);
        else
                return nlm_hal_read_32bit_reg(mmio, regidx);
}

static __inline__ void i2c_reg_write(int node, int func, int regidx, int32_t val)
{
        volatile uint64_t mmio;
        mmio = nlm_hal_get_dev_base(node, 0, XLP_PCIE_GIO_DEV, func)+0x100;
	if(is_nlm_xlp2xx())
                nlm_hal_write_32bit_reg(mmio+0x20, regidx, val);
        else
                nlm_hal_write_32bit_reg(mmio, regidx, val);
}

#ifdef XLP_I2C_DEBUG
static void i2c_dump_reg()
{
        int i, j = 0;
	for(j = 0; j < 1; j++)
	{
		printk("dump i2c_%d register\n", j);
                for(i = 0; i < 6; i++) {
			printk("0x%0x = 0x%8x\n", i, i2c_reg_read( 0, XLP_GIO_I2C0_FUNC+j, i));
		}
                for(i = 0x3C; i < 0x42; i++) {
			printk("0x%0x = 0x%8x\n", i, i2c_reg_read( 0, XLP_GIO_I2C0_FUNC+j, i));
		}
	}
}
#endif

static inline uint8_t i2c_read_reg8(struct i2c_xlp_data *adap, int offset) {

        volatile uint32_t *i2c_mmio = (u32*)adap->ioreg;
        return ((uint8_t)i2c_mmio[offset]);
}

static inline void i2c_write_reg8(struct i2c_xlp_data *adap,int offset, uint8_t value) {
	
        volatile uint32_t *i2c_mmio = (u32*)adap->ioreg;
        i2c_mmio[offset] = value;
}

static int wait_xfer_done(struct i2c_xlp_data *adap) {

        volatile int timeout = I2C_TIMEOUT;
        int retval = 0;

        while ((i2c_read_reg8(adap, XLP_I2C_STATUS) & XLP_I2C_STATUS_TIP) && timeout) {
                timeout--;
        }
        if (timeout == 0) {
                printk("Timed Out Waiting for TIP to Clear.\n");
                retval = -1;
        }
        return retval;
}

static int bus_idle(struct i2c_xlp_data *adap) {

        volatile int timeout = I2C_TIMEOUT;
        int retval = 0;

        while ((i2c_read_reg8(adap, XLP_I2C_STATUS) & XLP_I2C_STATUS_BUSY) && timeout) {
                timeout--;
        }
        if (timeout == 0) {
                printk("Timed Out Waiting for Bus Busy to Clear.\n");
                retval = -1;
        }
        return retval;
}

static int wait_ack(struct i2c_xlp_data *adap) {

        if (i2c_read_reg8(adap, XLP_I2C_STATUS) & XLP_I2C_STATUS_NACK) {
               return -1;
        }
        return 0;
}

int xlp_i2c_read(struct i2c_xlp_data *adap, uint8_t slave_addr, uint32_t slave_offset,
              int alen, int len, uint8_t *data) {
        int i ;

        /* Verify the bus is idle */
        if (i2c_read_reg8(adap, XLP_I2C_STATUS) & XLP_I2C_STATUS_BUSY) {
                printk("I2C Bus BUSY (Not Available), Aborting.\n");
                goto i2c_rx_error;
        }

        i2c_write_reg8(adap, XLP_I2C_DATA, (slave_addr << 1) | XLP_WRITE_BIT);
        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_START);
        if (wait_xfer_done(adap) < 0) {
                goto i2c_rx_error;
        }
        if (wait_ack(adap) < 0) {
                goto i2c_rx_error;
        }

        /* Verify Arbitration is not Lost */
        if (i2c_read_reg8(adap, XLP_I2C_STATUS) & XLP_I2C_STATUS_AL) {
                printk("I2C Bus Arbitration Lost, Aborting.\n");
                goto i2c_rx_error;
        }
        for (i = 0; i<=alen; i++) {
                i2c_write_reg8(adap, XLP_I2C_DATA, ( (slave_offset >> (i*8) ) & 0xff) | XLP_WRITE_BIT);
                i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_WRITE);
                if (wait_xfer_done(adap) < 0) {
                        goto i2c_rx_error;
                }
                if (wait_ack(adap) < 0) {
                        goto i2c_rx_error;
                }
        }

        /* Address Phase Done, Data Phase begins
 	 */
        i2c_write_reg8(adap, XLP_I2C_DATA, (slave_addr << 1) | XLP_READ_BIT);
        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_START);
        if (wait_xfer_done(adap) < 0) {
                goto i2c_rx_error;
        }
        if (wait_ack(adap) < 0) {
                goto i2c_rx_error;
        }
        if (len > 1) {

                int bytenr = 0;

                for (bytenr = 0; bytenr < len-1; bytenr++) {
                        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_READ);
                        if (wait_xfer_done(adap) < 0) {
                                goto i2c_rx_error;
                        }
                        if (data != NULL) {
                                *data = i2c_read_reg8(adap, XLP_I2C_DATA);
                                data++;
                        }
                }
        }

        /* Last (or only) Byte: -
         * Set RD, NACK, STOP Bits
         */
        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_STOP | XLP_I2C_CMD_RDNACK);
        if (wait_xfer_done(adap) < 0) {
                goto i2c_rx_error;
        }
	if(data != NULL)
                *data = i2c_read_reg8(adap, XLP_I2C_DATA);
        return bus_idle(adap);

i2c_rx_error:
        /* Release Bus */
        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_STOP);
        bus_idle(adap);
        return -1;
}

int xlp_i2c_write(struct i2c_xlp_data *adap, uint8_t slave_addr, uint16_t slave_offset, int alen,
                int len, uint8_t *data) {
        int i ;

        /* Verify the bus is idle */
        if (i2c_read_reg8(adap, XLP_I2C_STATUS) & XLP_I2C_STATUS_BUSY) {
                printk("I2C Bus BUSY (Not Available), Aborting.\n");
                goto i2c_tx_error;
        }

        i2c_write_reg8(adap, XLP_I2C_DATA, (slave_addr << 1) | XLP_WRITE_BIT);
        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_START);
        if (wait_xfer_done(adap) < 0) {
                goto i2c_tx_error;
        }
        if (wait_ack(adap) < 0) {
                goto i2c_tx_error;
        }

        /* Verify Arbitration is not Lost */
        if (i2c_read_reg8(adap, XLP_I2C_STATUS) & XLP_I2C_STATUS_AL) {
                printk("I2C Bus Arbitration Lost, Aborting.\n");
                goto i2c_tx_error;
        }

        for (i = 0; i<=alen; i++) {
                i2c_write_reg8(adap, XLP_I2C_DATA, ( (slave_offset >> (i*8) ) & 0xff) | XLP_WRITE_BIT);
                i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_WRITE);
                if (wait_xfer_done(adap) < 0) {
                        goto i2c_tx_error;
                }
                if (wait_ack(adap) < 0) {
                        goto i2c_tx_error;
                }
        }

        if (len > 1) {

                int bytenr = 0;

                for (bytenr = 0; bytenr < len-1; bytenr++) {
                        i2c_write_reg8(adap, XLP_I2C_DATA, *data);
                        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_WRITE);
                        if (wait_xfer_done(adap) < 0) {
                                goto i2c_tx_error;
                        }
                        data++;
                }
        }
        i2c_write_reg8(adap, XLP_I2C_DATA, *data);
        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_STOP | XLP_I2C_CMD_WRITE);
        if (wait_xfer_done(adap) < 0) {
                goto i2c_tx_error;
        }
        if (wait_ack(adap) < 0) {
                goto i2c_tx_error;
        }

        return bus_idle(adap);

i2c_tx_error:
        /* Release Bus */
        i2c_write_reg8(adap, XLP_I2C_COMMAND, XLP_I2C_CMD_STOP);
        bus_idle(adap);
        return -1;
}

static int
xlp_i2c_xfer(struct i2c_adapter *i2c_adap, struct i2c_msg *msgs, int num)
{
	struct i2c_xlp_data *adap = i2c_adap->algo_data;
	struct i2c_msg *p;
	int err = 0, command, len;

	command = msgs[0].buf[0];
	if(num == 1) {
		p = &msgs[0];
		if(p && p->len == 0){
			len = 1;
		}
		else if (p && (p->flags & I2C_M_RD)){
			len = p->len;
		}
		else{
			len = p->len - 1;
		}
	}
	else if(num == 2) {
		p = &msgs[1];
		len = p->len;
	}
	else {
		printk("ERR: msg num =%d large than 2\n", num);
		return -1;
	}
	if (p->flags & I2C_M_RD)
		err = xlp_i2c_read(adap, p->addr, command, 0, len, &p->buf[0]);
	else
		err = xlp_i2c_write(adap, p->addr, command, 0, len, &p->buf[1]);

	/* Return the number of messages processed, or the error code.
	*/
	if (err == 0)
		err = num;


	return err;
}

static int
xlp_i2c_smbus_xfer(struct i2c_adapter *i2c_adap,
		u16 		addr,
		unsigned short	flags,
		char		read_write,
		u8 		command,
		int 		protocol,
		union i2c_smbus_data *data
		)
{
	struct i2c_xlp_data *adap = i2c_adap->algo_data;
	int err;
	int len;

	switch(protocol)
	{
	case I2C_SMBUS_BYTE:
		if (read_write == I2C_SMBUS_READ)
			err = xlp_i2c_read(adap, addr, command, 0, 1, &data->byte);
		else
			err = xlp_i2c_write(adap, addr, command, 0, 1, &command);

		break;
	case I2C_SMBUS_BYTE_DATA:
		if (read_write == I2C_SMBUS_READ)
			err = xlp_i2c_read(adap, addr, command, 0, 1, &data->byte);
		else
			err = xlp_i2c_write(adap, addr, command, 0, 1, &data->byte);
		break;

	case I2C_SMBUS_WORD_DATA:
	case I2C_SMBUS_PROC_CALL:
		if (read_write == I2C_SMBUS_READ)
			err = xlp_i2c_read(adap, addr, command, 0, 2, (u8 *)&data->word);
		else
			err = xlp_i2c_write(adap, addr, command, 0, 2, (u8 *)&data->word);

		break;
	case I2C_FUNC_SMBUS_BLOCK_DATA:
	case I2C_SMBUS_I2C_BLOCK_DATA:
		len = (data->block[0] > I2C_SMBUS_BLOCK_MAX) ? I2C_SMBUS_BLOCK_MAX: data->block[0];
		if (read_write == I2C_SMBUS_READ)
			err = xlp_i2c_read(adap, addr, command, 0, len, &data->block[1]);
		else
			err = xlp_i2c_write(adap, addr, command, 0, len, &data->block[1]);

		break;
	default:
		err = -1;

	}
	return err;
}

static uint32_t
xlp_i2c_func(struct i2c_adapter *adap)
{
	return I2C_FUNC_I2C | I2C_FUNC_SMBUS_EMUL;
}

static const struct i2c_algorithm xlp_i2c_algo = {
	.master_xfer	= xlp_i2c_xfer,
	.smbus_xfer	= xlp_i2c_smbus_xfer,
	.functionality	= xlp_i2c_func,
};

static void i2c_xlp_setup(struct i2c_xlp_data *priv)
{
	uint32_t val, prescaler;
	
	prescaler = (I2C_CLKFREQ_HZ/(5 * (priv->speed * 1000))) - 1;  /* (speed * 1000) - convert KHz to Hz */

	/* disable I2C before setting the prescaler values */
	val = i2c_reg_read(priv->node, priv->func, XLP_I2C_CONTROL);
	val &= ~(XLP_I2C_CTRL_EN | XLP_I2C_CTRL_IEN);
	i2c_reg_write(priv->node, priv->func, XLP_I2C_CONTROL, val);
	
	/* set prescaler values*/	
	i2c_reg_write(priv->node, priv->func, XLP_PRESCALE0, prescaler & 0xff);
	i2c_reg_write(priv->node, priv->func, XLP_PRESCALE1, prescaler >> 8);
	
	/* re-enable I2C */
	val &= ~0xFFFF;
	val |= XLP_I2C_CTRL_EN; 

	i2c_reg_write(priv->node, priv->func, XLP_I2C_CONTROL, val);	
}

static void i2c_xlp_disable(struct i2c_xlp_data *priv)
{
	int32_t val;
	
	val = i2c_reg_read(priv->node, priv->func, XLP_I2C_CONTROL);
	val &= ~XLP_I2C_CTRL_EN;
	i2c_reg_write(priv->node, priv->func, XLP_I2C_CONTROL, val);	
}

static int __devinit
i2c_xlp_probe(struct platform_device *pdev)
{
	struct i2c_xlp_data *priv;
	int ret;
	unsigned int phys;
#ifdef XLP_I2C_DEBUG
	i2c_dump_reg();	
#endif
	priv = kzalloc(sizeof(struct i2c_xlp_data), GFP_KERNEL);
	if (!priv) {
		ret = -ENOMEM;
		goto out;
	}

	priv->node = 0;
	priv->bus = pdev->id;
	priv->func = priv-> bus + 2;
	priv->xfer_timeout = 200;
	priv->ack_timeout = 200;
	priv->speed       = i2c_speed;	
	
	/* xlp2xx has different pcie function*/
	if(is_nlm_xlp2xx()) {
		priv->func = 7;
		phys = nlm_hal_get_dev_base(0, 0, XLP_PCIE_GIO_DEV, priv->func) + 0x100;
		priv->ioreg = ioremap_nocache(phys+0x20, PAGE_SIZE);
	}else
	{	phys = nlm_hal_get_dev_base(0, 0, XLP_PCIE_GIO_DEV, priv->func) + 0x100;
		priv->ioreg = ioremap_nocache(phys, PAGE_SIZE);
	}
	if(is_nlm_xlp2xx())
		priv->adap.nr = 1;
	else
		priv->adap.nr = pdev->id;
	
	priv->adap.algo = &xlp_i2c_algo;
	priv->adap.algo_data = priv;
	priv->adap.dev.parent = &pdev->dev;
	strlcpy(priv->adap.name, "i2c-xlp", sizeof(priv->adap.name));

	/* Now, set up the PSC for SMBus PIO mode.
	*/
	i2c_xlp_setup(priv);

	ret = i2c_add_numbered_adapter(&priv->adap);
	if (ret == 0) {
		platform_set_drvdata(pdev, priv);
		if(is_nlm_xlp2xx())
			printk("Initializing i2c-xlp host driver for i2c-xlp.%d\n",priv->adap.nr);
		else
			printk("Initializing i2c-xlp host driver for i2c-xlp.%d\n",pdev->id);
		return 0;
	}

	i2c_xlp_disable(priv);
out:
	return ret;
}

static int __devexit
i2c_xlp_remove(struct platform_device *pdev)
{
	struct i2c_xlp_data *priv = platform_get_drvdata(pdev);

	platform_set_drvdata(pdev, NULL);
	i2c_del_adapter(&priv->adap);
	i2c_xlp_disable(priv);
	release_resource(priv->ioarea);
	kfree(priv->ioarea);
	kfree(priv);
	return 0;
}

#ifdef CONFIG_PM
static int
i2c_xlp_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct i2c_xlp_data *priv = platform_get_drvdata(pdev);

	i2c_xlp_disable(priv);

	return 0;
}

static int
i2c_xlp_resume(struct platform_device *pdev)
{
	struct i2c_xlp_data *priv = platform_get_drvdata(pdev);

	i2c_xlp_setup(priv);

	return 0;
}
#else
#define i2c_xlp_suspend	NULL
#define i2c_xlp_resume	NULL
#endif

static struct platform_driver xlp_smbus_driver = {
	.driver = {
		.name	= "i2c-xlp",
		.owner	= THIS_MODULE,
	},
	.probe		= i2c_xlp_probe,
	.remove		= __devexit_p(i2c_xlp_remove),
	.suspend	= i2c_xlp_suspend,
	.resume		= i2c_xlp_resume,
};

static int __init
i2c_xlp_init(void)
{
	return platform_driver_register(&xlp_smbus_driver);
}

static void __exit
i2c_xlp_exit(void)
{
	platform_driver_unregister(&xlp_smbus_driver);
}

MODULE_AUTHOR("Netlogic MicroSystems");
MODULE_DESCRIPTION("I2C adapter for XLP soc");
MODULE_LICENSE("GPL");

module_init (i2c_xlp_init);
module_exit (i2c_xlp_exit);
