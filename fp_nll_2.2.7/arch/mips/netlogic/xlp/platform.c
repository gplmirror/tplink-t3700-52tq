/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */


#include <linux/dma-mapping.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/serial.h>
#include <linux/serial_8250.h>
#include <linux/pci.h>
#include <linux/serial_reg.h>
#include <linux/spinlock.h>

#include <asm/time.h>
#include <asm/netlogic/hal/nlm_hal.h>
#include <asm/netlogic/xlp_irq.h>
#include <asm/netlogic/xlp.h>
#include <asm/netlogic/xlp_usb.h>

#define XLP_SOC_PCI_DRIVER "XLP SoC Driver"
#define DEV_IRT_INFO		0x3D

#define XLP_MAX_DEVICE		8
#define XLP_MAX_FUNC		8
#define MAX_DEV2DRV		10
#define MAX_NUM_UARTS		4
#define UART_CLK_133MHz 133333333
#define UART_CLK_66MHz   66666666
#define XLP_UART_PORTIO_OFFSET	0x1000

static struct plat_serial8250_port xlp_uart_port[MAX_NUM_UARTS];

static u64 xlp_dev_dmamask = DMA_BIT_MASK(32);

int xlp_usb_dev;

enum driverType{
	PLAT_DRV = 0,
	PCI_DRV	 = 1
};

struct dev2drv {
	uint32_t 	devid;
	uint8_t 	drvname[16];
	uint8_t 	len;
	uint8_t 	id;
	uint8_t		drivetype;
};

unsigned int xlp_uart_in(struct uart_port *p, int offset) {

	nlm_reg_t *mmio;
	unsigned int value;

	/* XLP uart does not need any mapping of regs 
	 */
	offset = offset << p->regshift;
	mmio = (nlm_reg_t *)(p->membase + offset);
	value = netlogic_read_reg(mmio, 0);

	return value;
}

void xlp_uart_out(struct uart_port *p, int offset, int value)
{
	nlm_reg_t *mmio;

	offset = offset << p->regshift;
	mmio = (nlm_reg_t *)(p->membase + offset);
	netlogic_write_reg(mmio, 0, value);
}

static void xlp_init_uart(int port_id)
{
        xlp_uart_port[port_id].mapbase       = DEFAULT_NETLOGIC_IO_BASE 
						+ NETLOGIC_IO_UART_0_OFFSET + port_id * XLP_UART_PORTIO_OFFSET;
        xlp_uart_port[port_id].membase       = (void __iomem *)xlp_uart_port[port_id].mapbase;
        xlp_uart_port[port_id].irq           = XLP_UART_IRQ(0, port_id);

	if(nlm_hal_is_ref_clk_133MHz())
		xlp_uart_port[port_id].uartclk       = UART_CLK_133MHz;
	else
		xlp_uart_port[port_id].uartclk       = UART_CLK_66MHz;

        xlp_uart_port[port_id].iotype        = UPIO_NLM;
        xlp_uart_port[port_id].flags         = UPF_SKIP_TEST|UPF_FIXED_TYPE|UPF_BOOT_AUTOCONF;
        xlp_uart_port[port_id].type          = PORT_16550A;
        xlp_uart_port[port_id].regshift      = 2;
        xlp_uart_port[port_id].serial_in     = xlp_uart_in;
        xlp_uart_port[port_id].serial_out    = xlp_uart_out;
}

static void xlp_usb_hw_start_controller(int node, int ctrl_no)
{
	int val;
	/* Reset USB phy */
	val = usb_reg_read( node, ctrl_no, XLP_USB_PHY0);

	if(ctrl_no == 0)
		val &= ~(USBPHYRESET | USBPHYPORTRESET0 | USBPHYPORTRESET1);
	else if(ctrl_no == 3)
		val &= ~(USBPHYPORTRESET0 | USBPHYPORTRESET1);
	usb_reg_write(node, ctrl_no, XLP_USB_PHY0, val);

	udelay(2000);
	/* Bring usb controller out of reset
 *  	 */
	val = usb_reg_read( node, ctrl_no, XLP_USB_CTL0);
	val &= ~(USBCONTROLLERRESET );
	val |= 0x4;
	usb_reg_write(node, ctrl_no, XLP_USB_CTL0, val);

	return;
}

static void xlp_usb3_hw_start_controller(int node, int ctrl_no)
{
	int val;
	

        val = usb_reg_read(node, ctrl_no, XLP2XX_USB_PHY_LOS_LEV);
        val &= 0xfc0fffff;
        val |= (0x27 << 20);
        usb_reg_write(node, ctrl_no, XLP2XX_USB_PHY_LOS_LEV, val);

        val = usb_reg_read(node, ctrl_no, XLP2XX_USB_REF_CLK);
        val |= (1<<30);
        usb_reg_write(node, ctrl_no, XLP2XX_USB_REF_CLK, val);

        val = usb_reg_read(node, ctrl_no, XLP2XX_USB_PHY_TEST);
        val &= 0xfffffffe;
        usb_reg_write(node, ctrl_no, XLP2XX_USB_PHY_TEST, val);

        val = usb_reg_read(node, ctrl_no, XLP_USB3_CTL);
        usb_reg_write(node, ctrl_no, XLP_USB3_CTL, 0x2e02203);

	/* mask bits [7:0] -- these are core interrupts */
	usb_reg_write(node, ctrl_no, XLP_USB3_INT_MASK, 0x000fff01);

	/* clear all interrupts */
	usb_reg_write(node, ctrl_no, XLP_USB3_INT, 0xffffffff);

	udelay(2000);

	return;
}

static void xlp_usb_hw_start(void)
{
	int n, online;
	int total=num_possible_nodes();

	if (is_nlm_xlp2xx())
		xlp_usb_dev = XLP_PCIE_USB3_DEV;
	else
		xlp_usb_dev = XLP_PCIE_USB_DEV;

	for (n = 0; n < total; n++) {
		online = node_online(n);
		if(!online)
			continue;

		if (is_nlm_xlp2xx()) {
			xlp_usb3_hw_start_controller(n, 1);
			xlp_usb3_hw_start_controller(n, 2);
			xlp_usb3_hw_start_controller(n, 3);
		} else {
			xlp_usb_hw_start_controller(n, 0);
			xlp_usb_hw_start_controller(n, 3);
		}
	}
}

struct dev2drv dev2drv_table[MAX_DEV2DRV] = {
	{XLP_DEVID_UART, "serial8250",	11,	0,	PLAT_DRV},
	{XLP_DEVID_I2C,	 "i2c-xlp",	8,	0, 	PLAT_DRV},
	{XLP_DEVID_MMC,	 "mmc-xlp",	8,	0, 	PLAT_DRV},
	{XLP_DEVID_SPI,	 "spi-xlp",	8,	0, 	PLAT_DRV},
	{XLP_DEVID_NOR,	 "nor-xlp",	8,	0, 	PLAT_DRV},
	{XLP_DEVID_NAND, "nand-xlp",	9,	0, 	PLAT_DRV},
	{0x0, 			 "",	0,	0,	PLAT_DRV},
};

static int get_dev2drv(uint32_t x) 
{
	int i;

	for(i=0; i<MAX_DEV2DRV; i++) {	
		if(x == dev2drv_table[i].devid)
			return i;
	}
	return i;
}

static int xlp_find_pci_dev(void)
{
	uint16_t i, j, id, idx = 0, maxdevice=0,count=0;
	volatile uint64_t mmio;
	uint32_t val, devid, vid, irt, irq;
	struct platform_device* pplatdev;
	struct resource* pres;
	int total=num_possible_nodes();

	for(i=0; i<total; i++) {
		j=node_online(i);
		if(!j)     continue; 
		maxdevice += XLP_MAX_DEVICE;
	}	
	
	if(is_nlm_xlp2xx()){
		for(count=0; count<MAX_DEV2DRV; count++) {
                        if(XLP_DEVID_I2C == dev2drv_table[count].devid){
                                dev2drv_table[count].devid = XLP2XX_DEVID_I2C;
                        }
                }
        }
	

	pres = (struct resource*) kzalloc(sizeof(struct resource) * 2, GFP_KERNEL);

	if(!pres) {
		printk("kzalloc struct resource failedi!\n");
		return -ENOMEM;
	}

	for (i=0; i<maxdevice; i++) {

		for (j=0; j<XLP_MAX_FUNC; j++) {

			mmio = nlm_hal_get_dev_base(0, 0, i, j);
			val  = nlm_hal_read_32bit_reg(mmio, 0);

			if(val != 0xFFFFFFFF) {

				devid	= (val & 0xFFFF0000) >> 16;
				vid 	= (val & 0xFFFF);
				idx 	= get_dev2drv(devid);

				/* Register NAND only for other nodes.
				 * Remove if condition when other devices are supported on other nodes as well.
				 * */
				if(!((i>8 && (devid ==XLP_DEVID_NAND) ) || (i<8)))
					continue;

				if(idx >= 0 && idx < MAX_DEV2DRV) {

					if(dev2drv_table[idx].drivetype == PLAT_DRV) {

						id = dev2drv_table[idx].id;

						if (devid == XLP_DEVID_EHCI) {
							if(id == 1)
								id = 3;
						}

						if( devid == XLP_DEVID_OHCI) {
							if(id < 2)
								id = id + 1;
							else if (id >=2)
								id = id + 2;
						}

						if (devid == XLP_DEVID_UART)
							id += PLAT8250_DEV_PLATFORM;

						pplatdev =  platform_device_alloc(
								(const char*)dev2drv_table[idx].drvname, id);
						if (!pplatdev) {
							printk("platform_device_alloc  failed!\n");
							continue;
						}

						if(devid == XLP_DEVID_UART) {
							pplatdev->dev.platform_data = &xlp_uart_port[dev2drv_table[idx].id];
							xlp_init_uart(dev2drv_table[idx].id);
						}

						dev2drv_table[idx].id = dev2drv_table[idx].id + 1;

						pres[0].start	= mmio;
						pres[0].end	= mmio;
						pres[0].flags	= IORESOURCE_MEM;
						irt = (nlm_hal_read_32bit_reg(mmio, DEV_IRT_INFO) & 0xFFFF);
						irq = xlp_irt_to_irq(0, irt);

						pres[1].start = irq;
						pres[1].end = irq;
						pres[1].flags = IORESOURCE_IRQ;

						platform_device_add_resources(pplatdev, pres, 2);
						pplatdev->dev.dma_mask	= &xlp_dev_dmamask;
						pplatdev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
						platform_device_add(pplatdev);
					}
				}
			}
		}
	}
	kfree(pres);
	return 0;
}
static int __init platform_devinit(void)
{
	xlp_find_pci_dev();

#ifdef CONFIG_USB
	xlp_usb_hw_start();
#endif
	return 0;
}

#ifdef CONFIG_RAPIDIO
void platform_rio_init(void)
{
	extern int bcm_rio_module_init();
        bcm_rio_module_init();
}
#endif

static void __init platform_devexit(void)
{
	return;
}

module_init(platform_devinit);
module_exit(platform_devexit);
