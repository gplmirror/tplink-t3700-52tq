/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */


#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/bootmem.h>
#include <linux/init.h>
#include <linux/pm.h>

#include <asm/asm.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <asm/bootinfo.h>
#include <asm/addrspace.h>
#include <asm/reboot.h>
#include <asm/time.h>
#include <linux/interrupt.h>
#include <asm/atomic.h>
#include <asm/cacheflush.h>
#include <asm/netlogic/xlp8xx/cpu_control_macros.h>
#include <asm/netlogic/xlp.h>
#include <asm/netlogic/hal/nlm_hal.h>

#define XLP_ECFG_BASE           0x18000000
#define XLP_SYS_DEV_BASE        0x35000
#define PLL_REF_CLK_PS_133MHz	7500
#define PLL_REF_CLK_PS_66MHz	15000

/* temporary storage space for
 * stack pointers
 */
unsigned long linuxsp[NR_CPUS];

/* Externs
 */
extern unsigned char __stack[];
extern char boot_siblings_start[], boot_siblings_end[];
extern char reset_entry[], reset_entry_end[];
extern int  is_nlm_xlp2xx_compat;

static inline void jump_address(unsigned long entry)
{
	asm volatile (
			".set push   	\n"
			".set noreorder \n"
			"jalr  %0     	\n"
			"nop          	\n"
			".set pop       \n"
			:: "r"(entry));
}

/* Configure LSU */
static inline void config_lsu(void)
{
	uint32_t tmp0, tmp1, tmp2;

	if (xlp8xx_a01_workaround_needed) {
		__asm__ __volatile__ (
			".set push\n"
			".set noreorder\n"
			"li      %0, "STR(LSU_DEFEATURE)"\n"
			"mfcr    %1, %0\n"
			"lui     %2, 0x4080\n"
			"or      %1, %1, %2\n"
			"li	 %2, ~0xe\n"
			"and	 %1, %1, %2\n"
			"mtcr    %1, %0\n"
			"li      %0, "STR(SCHED_DEFEATURE)"\n"
			"lui     %1, 0x0100\n"
			"mtcr    %1, %0\n"
			".set pop\n"
			: "=r" (tmp0), "=r" (tmp1), "=r" (tmp2)
		);
	} else {
		__asm__ __volatile__ (
			".set push\n"
			".set noreorder\n"
			"li      %0, "STR(LSU_DEFEATURE)"\n"
			"mfcr    %1, %0\n"
			"lui     %2, 0x4080\n"
			"or      %1, %1, %2\n"
			"mtcr    %1, %0\n"
			"li      %0, "STR(SCHED_DEFEATURE)"\n"
			"lui     %1, 0x0100\n"
			"mtcr    %1, %0\n"
			".set pop\n"
			: "=r" (tmp0), "=r" (tmp1), "=r" (tmp2)
		);
	}
}

/* Configure ISU */
static inline void config_isu(void)
{
	uint32_t tmp0, tmp1, tmp2;
	/* set bits 31:27 of the ISU_DEFEAURE register (0x700) for unaligned access erratum */
	__asm__ __volatile__ (
		".set push\n"
		".set noreorder\n"
		"li      %0, "STR(SCHED_DEFEATURE)"\n"
		"mfcr    %1, %0\n"
		"lui     %2, 0xF800\n"
		"or      %1, %1, %2\n"
		"mtcr    %1, %0\n"
		".set pop\n"
		: "=r" (tmp0), "=r" (tmp1), "=r" (tmp2)
	);
}

static void enable_cores(unsigned int node, unsigned int cores_bitmap)
{
	uint32_t core, value;
	uint64_t sys_mmio;
	uint32_t cbitmap = cores_bitmap;

	/* if n0c0t0, don't pull yourself out of reset! */
	if(node == 0) cbitmap = cbitmap & 0xfe;

	printk("[%s] node@%d, cores_bitmap = 0x%08x cbitmap = 0x%08x\n",
	       __func__, node, cores_bitmap, cbitmap);

        sys_mmio = (uint64_t) (XLP_ECFG_BASE + XLP_SYS_DEV_BASE + 0x40000 * node );

	for (core = 0x1; core != (0x1 << 8); core <<= 1) {

		if ( (cbitmap & core) == 0) continue;

		if (!is_nlm_xlp2xx_compat) {
			/* Enable CPU clock: only needed for xlp8xx/xlp3xx */
			value = nlm_hal_read_32bit_reg(sys_mmio,0x4E) & ~core;
			nlm_hal_write_32bit_reg(sys_mmio, 0x4E, value);
		}

		/* Remove CPU Reset */
		value = nlm_hal_read_32bit_reg(sys_mmio, 0x4B) & ~core;
		nlm_hal_write_32bit_reg(sys_mmio, 0x4B, value);

		/* Poll for CPU to mark itself coherent */
		for(;;) {
			value = nlm_hal_read_32bit_reg(sys_mmio, 0x4D) & core;
			if (!value) break;
		}
	}
}

static inline int num_ones(unsigned long mask)
{
	int ret = 0;

	if (!mask) return 0;
	while ((mask &= (mask - 1))) ret++;
	return (ret + 1);
}

int threads_to_enable = 0;
/*
 * This function is called once for each node. However, it is executed
 * only on "master cpu", mostly on n0c0t0
 *
 * t0_bitmap: bitmap of thread@0 of those cores in the given node, which 
 *            are enabled in node_cpumask
 *
 * cores_bitmap: bitmap of cores (core 0's) which have at least one thread
 *               enabled (considering ALL nodes and cores)
 *
 * threads_to_enable: count bitmap of threads to enable in all enabled cores.
 *
 */
void enable_cpus(unsigned int node, unsigned int node_cpumask)
{
	uint32_t t0_bitmap = 0x0;
	uint32_t t0_positions = 0, index = 0;
	uint32_t cores_bitmap;

	if (hard_smp_processor_id() != 0) {
		printk("[%s]: Running on non n0c0t0 cpu??\n", __func__);
		return;
	}

	/* Setup Exception vectors only the first time around */
	if (!node) {

		config_lsu();
		config_isu();

		/* Linux runs out of KSEG2. Setup TLBs
		 * for other threads, by running from
		 * KSEG0. Then, jump back into KSEG2.
		 */
		memcpy((void *)(NMI_BASE + (2<<10)),
		       (void *)&boot_siblings_start,
		       (boot_siblings_end - boot_siblings_start));

		/* setup the reset vector */
		memcpy((void *)(NMI_BASE), (void *)&reset_entry, (reset_entry_end - reset_entry));

	}

	/* bitmap of thread@0 of every core in this node */
	t0_bitmap = node_cpumask & 0x11111111;

	cores_bitmap = 0;
	for (t0_positions = 0, index = 0; t0_positions < 32; t0_positions += 4, index++) {
		 if (t0_bitmap & (1 << t0_positions)){
			cores_bitmap |= (1 << index);
			threads_to_enable |= (num_ones((node_cpumask >> t0_positions) & 0xf) << t0_positions);
		 }
	}

	printk("node@%d: t0_bitmap = 0x%08x, cores_bitmap = 0x%08x threads_to_enable 0x%x\n", 
		node, t0_bitmap, cores_bitmap,threads_to_enable);

	enable_cores(node, cores_bitmap);

	if (!node) {
		/* Wakeup threads of n0c0 */
		jump_address(NMI_BASE + (2<<10));
	}

	return;
}

EXPORT_SYMBOL(enable_cpus);
/* Return period in femto-seconds.(1e-15 s)
 */
static u32 get_pll_period(int divf,int divr) {

	u32 vco_fs;		/* vco in femto-seconds */
	u32 pll_period_fs;	/* pll output in femto-seconds */
	u32 pll_ref_clk_ps;

	if (divr == 2) {
		printk (KERN_ERR "Error! Illegal divr value %d!\n", divr);
		return 0;
	}
	if ((divf < 11) || (divf > 91)) {
		printk (KERN_ERR "Error! Illegal divf value %d!\n", divf);
		return 0;
	}

	if(nlm_hal_is_ref_clk_133MHz())
		pll_ref_clk_ps = PLL_REF_CLK_PS_133MHz;
	else
		pll_ref_clk_ps = PLL_REF_CLK_PS_66MHz;

	vco_fs = (((pll_ref_clk_ps * 1000) * (divr+1))/(4 * (divf+1)));
	pll_period_fs  = vco_fs * 2; /* pll output is divided by 2 */

	return pll_period_fs;
}

/* Return frequency in Hz
 */
static uint64_t get_pll_freq(int divf,int divr)
{
	uint64_t hz_freq = 1000000000000000ULL; /* 1e15 */
	u32 pll_period_fs = get_pll_period(divf,divr);

	do_div(hz_freq, ((uint64_t) pll_period_fs));

	return hz_freq;
}

u32 get_core_dfs(int cpu_num)
{
	u32 core_dfs, dfs;
	volatile u32 *mmio;

	mmio = (volatile u32 *) cpu_io_mmio(cpu_num/32,SYS);
	core_dfs = nlm_hal_read_32bit_reg((uint64_t)mmio, SYS_COREDFSDIVCTRL);
	dfs  = SYS_CORE_DFS(core_dfs, (cpu_num >> 2));
	return dfs;
}

EXPORT_SYMBOL(get_core_dfs);

