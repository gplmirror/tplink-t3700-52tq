/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */


#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/spi/spi.h>
#include <asm/netlogic/hal/nlm_eeprom.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <mtd/mtd-abi.h>

struct eeprom_data nlm_eeprom;

extern int eeprom_read (u8 devaddr, uint addr, int alen, u8 * buf, int len);
extern int eeprom_write (u8 devaddr, uint addr, int alen, u8 * buf, int len);
extern int eeprom_init (int speed);
static struct i2c_board_info xlp_i2c1_device_info[] __initdata = {
  {
    I2C_BOARD_INFO("lm90", 0x4c),
  },
  {
    I2C_BOARD_INFO("at24c02", 0x57),
  }
};
#ifdef CONFIG_NLM_XLP_CAMARO_BOARD
static struct mtd_partition xlp_mtd_partitions[] = {
	{
   		.name = "X-Loader(RO)",
		.offset = 0,
		.size = 0x20000,  /* 128K */
	},
	{
		.name = "u-boot",
		.offset = 0x20000,
		.size = 0xa0000,  /* 640k */
	},
	{
		.name = "VPD",
		.offset = 0xc0000,
		.size = 0x20000,  /* 128K */
	},
	{
		.name = "u-boot env",
		.offset = 0xe0000,
		.size = 0x20000,  /* 128K */
	},
	{
		.name = "diagnostics",
		.offset = 0x100000,
		.size = 0x300000, /* 3M */
	},
	{
		.name = "jffs2",
		.offset = 0x400000,
		.size = 0x3c00000,  /* 60M */
	},
	{
		.name = "image1",
		.offset = 0x4000000,
		.size = 0x1800000,  /* 24M */
	},
	{
		.name = "image2",
		.offset = 0x6000000,
		.size = 0x1800000,  /* 24M */
	},
	{
		.name = "unused",
		.offset = 0x7800000,  /* 128M */
		.size = MTDPART_SIZ_FULL,
		.mask_flags = MTD_WRITEABLE,
	},
};

#define BOARD_FLASH_SIZE 0x10000000 /* 16MB */
#define BOARD_FLASH_BASE 0x0000000090000000ULL
#define BOARD_FLASH_WIDTH 2 /* 16-bits */
static struct physmap_flash_data xlp_flash_data = {
	.width    = BOARD_FLASH_WIDTH,
	.nr_parts = ARRAY_SIZE(xlp_mtd_partitions),
	.parts    = xlp_mtd_partitions,
};

static struct resource xlp_mtd_resource = {
	.start  = BOARD_FLASH_BASE,
	.end  = BOARD_FLASH_BASE + BOARD_FLASH_SIZE - 1,
	.flags  = IORESOURCE_MEM,
};


static struct platform_device xlp_mtd_device = {
	.name   = "physmap-flash",
	.dev    = {
		   .platform_data  = &xlp_flash_data,
		  },
		  .num_resources  = 1,
		  .resource = &xlp_mtd_resource,
};

static struct platform_device *xlp_mtd_devices[] __initdata = {
	&xlp_mtd_device,
};
#endif


static struct i2c_board_info xlp_i2c_device_info[] __initdata = {
#ifdef CONFIG_N511
        {"ds1374",          1, 0x68, 0, 0, 0},
        // max6657 old driver style is only working when omitted here
        /*{"max6657",         1, 0x4c, 0, 0, 0},*/
#elif defined(CONFIG_NLM_XLP_CAMARO_BOARD)
        {"cy14x064i",          0, 0x68, 0, 0, 0},
#else
        {"ds1374",          0, 0x68, 0, 0, 0},
        {"max6657",         0, 0x4c, 0, 0, 0},	
#endif
};

static int __init xlp_i2c_device_init(void)
{
#ifdef CONFIG_NLM_XMC_SUPPORT
	// XMC uses bus 0 for common devices
	return i2c_register_board_info(0, xlp_i2c_device_info, ARRAY_SIZE(xlp_i2c_device_info));
#else
	return i2c_register_board_info(1, xlp_i2c_device_info, ARRAY_SIZE(xlp_i2c_device_info));
#endif
}

arch_initcall(xlp_i2c_device_init);

#ifdef CONFIG_NLM_XMC_SUPPORT
	// XMC SPI bus goes to backplane and has no dedicated devices
static int __init xlp_spi_device_init(void) {;}
#else
static struct spi_board_info spsn_spi_board_info[] __initdata = {
#ifdef CONFIG_N511
        {
                .modalias = "m25p80",
                .max_speed_hz = 40000000,
                .bus_num = 0,
                .chip_select = 0
        },
#else
	{
		.modalias = "m25p80",
		.max_speed_hz = 40000000,
		.bus_num = 0,
		.chip_select = 1
	},
	{
		.modalias = "mt29f",
		.max_speed_hz = 50000000,
		.bus_num = 0,
		.chip_select = 2
	},
#endif
};

static int __init xlp_spi_device_init(void)
{
	return spi_register_board_info(spsn_spi_board_info, ARRAY_SIZE(spsn_spi_board_info));
}
#endif

arch_initcall(xlp_spi_device_init);

static int __init eeprom_device_init(void)
{
	nlm_eeprom.i2c_dev_addr=0x57;
	nlm_eeprom.eeprom_i2c_read_bytes = (void *)eeprom_read;
        nlm_eeprom.eeprom_i2c_write_bytes = (void *)eeprom_write;
        eeprom_init(45000);
#ifdef CONFIG_NLM_XLP_CAMARO_BOARD
	platform_add_devices(xlp_mtd_devices, ARRAY_SIZE(xlp_mtd_devices));
#endif
	return 0;
}
arch_initcall(eeprom_device_init);
struct eeprom_data *get_nlm_eeprom(void)
{
        return &nlm_eeprom;
}
EXPORT_SYMBOL(get_nlm_eeprom);
