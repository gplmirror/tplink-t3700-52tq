/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */


#include <linux/types.h>
#include <linux/init.h>
#include <linux/smp.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/cpumask.h>
#include <linux/nodemask.h>

#include <asm/netlogic/msgring.h>
#include <asm/netlogic/iomap.h>
#include <asm/netlogic/mips-exts.h>
#include <asm/netlogic/debug.h>
#include <asm/netlogic/xlp.h>

#include <asm/netlogic/hal/nlm_hal_fmn.h>
#include <asm/netlogic/xlp_irq.h>

#include <linux/netdevice.h>
#include <asm/netlogic/cpumask.h>

#define MAX_VC	4096

extern int xlp_rvec_from_irt(int);
extern int xlp_rvec_from_irq(int);
unsigned long netlogic_io_base = (unsigned long)(DEFAULT_NETLOGIC_IO_BASE);
EXPORT_SYMBOL(netlogic_io_base);

extern int hwemul;

extern void nlm_cpu_stat_update_msgring_int(void);
extern void nlm_cpu_stat_update_msgring_cycles(__u32 cycles);
extern void nlm_cpu_stat_update_msgring_pic_int(void);

uint32_t msgring_global_thread_mask = 0;
uint32_t nlm_cpu_vc_mask[NLM_MAX_CPU_NODE*NLM_MAX_CPU_PER_NODE] = {0};

uint32_t nlm_l1_lock[NR_CPUS/4] = {0};

typedef int (*vchandler)(int vc, int budget);
static vchandler xlp_napi_vc_handlers[NLM_MAX_VC_PER_THREAD];

typedef int (*intr_vchandler)(int vc);
static intr_vchandler xlp_intr_vc_handler;
unsigned int intr_vc_mask[NR_CPUS];

/* make this a read/write spinlock */
spinlock_t msgrng_lock;
static nlm_common_atomic_t msgring_registered;

struct msgstn_handler {
        void (*action)(uint32_t, uint32_t, uint32_t, uint32_t, uint64_t, uint64_t, uint64_t, uint64_t, void *);
        void *dev_id;
	void (*napi_final)(void *arg);
	void *napi_final_arg;
};

static int napi_final_needed[NR_CPUS][XLP_MSG_HANDLE_MAX];
struct net_device xlp_napi_fmn_dummy_dev;
DEFINE_PER_CPU(struct napi_struct, xlp_napi_fmn_poll_struct);
DEFINE_PER_CPU(unsigned long long, xlp_napi_fmn_rx_count);
static int xlp_napi_vc_count = 0;
static int xlp_fmn_init_done = 0;
extern unsigned int xlp_napi_vc_mask;

static uint16_t vc_to_handle_map[MAX_VC] = {
	[0 ... 15] = XLP_MSG_HANDLE_CPU0,
	[16 ... 31] = XLP_MSG_HANDLE_CPU1,
	[32 ... 47] = XLP_MSG_HANDLE_CPU2,
	[48 ... 63] = XLP_MSG_HANDLE_CPU3,
	[64 ... 79] = XLP_MSG_HANDLE_CPU4,
	[80 ... 95] = XLP_MSG_HANDLE_CPU5,
	[96 ... 111] = XLP_MSG_HANDLE_CPU6,
	[112 ... 127] = XLP_MSG_HANDLE_CPU7,
	[128 ... 143] = XLP_MSG_HANDLE_CPU0,
	[144 ... 159] = XLP_MSG_HANDLE_CPU1,
	[160 ... 175] = XLP_MSG_HANDLE_CPU2,
	[176 ... 191] = XLP_MSG_HANDLE_CPU3,
	[192 ... 207] = XLP_MSG_HANDLE_CPU4,
	[208 ... 223] = XLP_MSG_HANDLE_CPU5,
	[224 ... 239] = XLP_MSG_HANDLE_CPU6,
	[240 ... 255] = XLP_MSG_HANDLE_CPU7,
	[256 ... 257] = XLP_MSG_HANDLE_PCIE0,
	[258 ... 259] = XLP_MSG_HANDLE_PCIE1,
	[260 ... 261] = XLP_MSG_HANDLE_PCIE2,
	[262 ... 263] = XLP_MSG_HANDLE_PCIE3,
	[264 ... 267] = XLP_MSG_HANDLE_DTRE,
	[268 ... 271] = XLP_MSG_HANDLE_GDX,
	[272 ... 280] = XLP_MSG_HANDLE_RSA_ECC,
	[281 ... 296] = XLP_MSG_HANDLE_CRYPTO,
	[297 ... 304] = XLP_MSG_HANDLE_CMP,
	[305 ... 383] = XLP_MSG_HANDLE_INVALID,
	[384 ... 391] = XLP_MSG_HANDLE_NAE_0,
	[392 ... 475] = XLP_MSG_HANDLE_INVALID,
	[476 ... 1019] = XLP_MSG_HANDLE_NAE_0,
	[1020 ... 1023] = XLP_MSG_HANDLE_INVALID,
	//NODE-1
	[1024 ... 1039] = XLP_MSG_HANDLE_CPU0,
	[1040 ... 1055] = XLP_MSG_HANDLE_CPU1,
	[1056 ... 1071] = XLP_MSG_HANDLE_CPU2,
	[1072 ... 1087] = XLP_MSG_HANDLE_CPU3,
	[1088 ... 1103] = XLP_MSG_HANDLE_CPU4,
	[1104 ... 1119] = XLP_MSG_HANDLE_CPU5,
	[1120 ... 1135] = XLP_MSG_HANDLE_CPU6,
	[1136 ... 1151] = XLP_MSG_HANDLE_CPU7,
	[1158 ... 1279] = XLP_MSG_HANDLE_INVALID,
	[1280 ... 1281] = XLP_MSG_HANDLE_PCIE0,
	[1282 ... 1283] = XLP_MSG_HANDLE_PCIE1,
	[1284 ... 1285] = XLP_MSG_HANDLE_PCIE2,
	[1286 ... 1287] = XLP_MSG_HANDLE_PCIE3,
	[1288 ... 1291] = XLP_MSG_HANDLE_DTRE,
	[1292 ... 1295] = XLP_MSG_HANDLE_GDX,
	[1296 ... 1304] = XLP_MSG_HANDLE_RSA_ECC,
	[1305 ... 1320] = XLP_MSG_HANDLE_CRYPTO,
	[1321 ... 1328] = XLP_MSG_HANDLE_CMP,
	[1329 ... 1407] = XLP_MSG_HANDLE_INVALID,
	[1408 ... 1415] = XLP_MSG_HANDLE_NAE_0,
	[1416 ... 1499] = XLP_MSG_HANDLE_INVALID,
	[1500 ... 2043] = XLP_MSG_HANDLE_NAE_0,
	[2044 ... 2047] = XLP_MSG_HANDLE_INVALID,
	// NODE-2	
	[2048 ... 2063] = XLP_MSG_HANDLE_CPU0,
	[2064 ... 2079] = XLP_MSG_HANDLE_CPU1,
	[2080 ... 2095] = XLP_MSG_HANDLE_CPU2,
	[2096 ... 2111] = XLP_MSG_HANDLE_CPU3,
	[2112 ... 2127] = XLP_MSG_HANDLE_CPU4,
	[2128 ... 2143] = XLP_MSG_HANDLE_CPU5,
	[2144 ... 2159] = XLP_MSG_HANDLE_CPU6,
	[2160 ... 2175] = XLP_MSG_HANDLE_CPU7,
	[2176 ... 2303] = XLP_MSG_HANDLE_INVALID,
	[2304 ... 2305] = XLP_MSG_HANDLE_PCIE0,
	[2306 ... 2307] = XLP_MSG_HANDLE_PCIE1,
	[2308 ... 2309] = XLP_MSG_HANDLE_PCIE2,
	[2310 ... 2311] = XLP_MSG_HANDLE_PCIE3,
	[2312 ... 2315] = XLP_MSG_HANDLE_DTRE,
	[2316 ... 2319] = XLP_MSG_HANDLE_GDX,
	[2320 ... 2328] = XLP_MSG_HANDLE_RSA_ECC,
	[2329 ... 2344] = XLP_MSG_HANDLE_CRYPTO,
	[2345 ... 2352] = XLP_MSG_HANDLE_CMP, 
	[2353 ... 2431] = XLP_MSG_HANDLE_INVALID,
	[2432 ... 2439] = XLP_MSG_HANDLE_NAE_0,
	[2440 ... 2523] = XLP_MSG_HANDLE_INVALID,
	[2524 ... 3067] = XLP_MSG_HANDLE_NAE_0,
	[3068 ... 3071] = XLP_MSG_HANDLE_INVALID,

	// NODE-3
	[3072 ... 3087] = XLP_MSG_HANDLE_CPU0,
	[3088 ... 3103] = XLP_MSG_HANDLE_CPU1,
	[3104 ... 3119] = XLP_MSG_HANDLE_CPU2,
	[3120 ... 3135] = XLP_MSG_HANDLE_CPU3,
	[3136 ... 3151] = XLP_MSG_HANDLE_CPU4,
	[3152 ... 3167] = XLP_MSG_HANDLE_CPU5,
	[3168 ... 3183] = XLP_MSG_HANDLE_CPU6,
	[3184 ... 3199] = XLP_MSG_HANDLE_CPU7,
	[3200 ... 3327] = XLP_MSG_HANDLE_INVALID,
	[3328 ... 3329] = XLP_MSG_HANDLE_PCIE0,
	[3330 ... 3331] = XLP_MSG_HANDLE_PCIE1,
	[3332 ... 3333] = XLP_MSG_HANDLE_PCIE2,
	[3334 ... 3335] = XLP_MSG_HANDLE_PCIE3,
	[3336 ... 3339] = XLP_MSG_HANDLE_DTRE,
	[3340 ... 3343] = XLP_MSG_HANDLE_GDX,
	[3344 ... 3352] = XLP_MSG_HANDLE_RSA_ECC,
	[3353 ... 3368] = XLP_MSG_HANDLE_CRYPTO,
	[3369 ... 3376] = XLP_MSG_HANDLE_CMP,
	[3377 ... 3455] = XLP_MSG_HANDLE_INVALID,
	[3456 ... 3463] = XLP_MSG_HANDLE_NAE_0,
	[3464 ... 3547] = XLP_MSG_HANDLE_INVALID,
	[3548 ... 4091] = XLP_MSG_HANDLE_NAE_0,
	[4092 ... 4095] = XLP_MSG_HANDLE_INVALID,
};

static uint16_t xlp3xx_vc_to_handle_map[MAX_VC] = {
	[0 ... 15] = XLP_MSG_HANDLE_CPU0,
	[16 ... 31] = XLP_MSG_HANDLE_CPU1,
	[32 ... 47] = XLP_MSG_HANDLE_CPU2,
	[48 ... 63] = XLP_MSG_HANDLE_CPU3,
	[64 ... 79] = XLP_MSG_HANDLE_INVALID,
	[80 ... 95] = XLP_MSG_HANDLE_INVALID,
	[96 ... 111] = XLP_MSG_HANDLE_INVALID,
	[112 ... 127] = XLP_MSG_HANDLE_INVALID,
	[128 ... 143] = XLP_MSG_HANDLE_CPU0,
	[144 ... 159] = XLP_MSG_HANDLE_CPU1,
	[160 ... 175] = XLP_MSG_HANDLE_CPU2,
	[176 ... 191] = XLP_MSG_HANDLE_CPU3,
	[192 ... 207] = XLP_MSG_HANDLE_INVALID,
	[208 ... 223] = XLP_MSG_HANDLE_INVALID,
	[224 ... 239] = XLP_MSG_HANDLE_INVALID,
	[240 ... 255] = XLP_MSG_HANDLE_INVALID,
	[256 ... 257] = XLP_MSG_HANDLE_PCIE0,
	[258 ... 259] = XLP_MSG_HANDLE_PCIE1,
	[260 ... 261] = XLP_MSG_HANDLE_PCIE2,
	[262 ... 263] = XLP_MSG_HANDLE_PCIE3,
	[264 ... 267] = XLP_MSG_HANDLE_DTRE,
	[268 ... 271] = XLP_MSG_HANDLE_REGX,
	[272 ... 275] = XLP_MSG_HANDLE_RSA_ECC,
	[276 ... 279] = XLP_MSG_HANDLE_CRYPTO,
	[280 ... 288] = XLP_MSG_HANDLE_SRIO,
	[289 ... 383] = XLP_MSG_HANDLE_INVALID,
	[384 ... 391] = XLP_MSG_HANDLE_NAE_0,
	[392 ... 431] = XLP_MSG_HANDLE_INVALID,
	[432 ... 511] = XLP_MSG_HANDLE_NAE_0,
	[512 ... 4095]= XLP_MSG_HANDLE_INVALID
};

static uint16_t xlp2xx_vc_to_handle_map[MAX_VC] = {
	[0 ... 15] = XLP_MSG_HANDLE_CPU0,
	[16 ... 31] = XLP_MSG_HANDLE_CPU1,
	[32 ... 127] = XLP_MSG_HANDLE_INVALID,
	[128 ... 143] = XLP_MSG_HANDLE_CPU0,
	[144 ... 159] = XLP_MSG_HANDLE_CPU1,
	[160 ... 255] = XLP_MSG_HANDLE_INVALID,
	[256 ... 257] = XLP_MSG_HANDLE_PCIE0,
	[258 ... 259] = XLP_MSG_HANDLE_PCIE1,
	[260 ... 261] = XLP_MSG_HANDLE_PCIE2,
	[262 ... 263] = XLP_MSG_HANDLE_PCIE3,
	[264 ... 265] = XLP_MSG_HANDLE_DTRE,
	[266 ... 267] = XLP_MSG_HANDLE_CMP,
	[268 ... 271] = XLP_MSG_HANDLE_REGX,
	[272 ... 272] = XLP_MSG_HANDLE_RSA_ECC,
	[273 ... 275] = XLP_MSG_HANDLE_INVALID,
	[276 ... 276] = XLP_MSG_HANDLE_CRYPTO,
	[277 ... 383] = XLP_MSG_HANDLE_INVALID,
	[384 ... 391] = XLP_MSG_HANDLE_NAE_0,
	[392 ... 431] = XLP_MSG_HANDLE_INVALID,
	[432 ... 503] = XLP_MSG_HANDLE_NAE_0,
	[504 ... 4095]= XLP_MSG_HANDLE_INVALID

};

/******************************************************************************************
 *  dummy_handler 
 *
 *  @vc		cpu vc number
 *  @src_id	msg sender station vc
 *  @size	msg_size-1
 *  @code	software code nae or poe can put in
 *  @msg0	64 bit msg0 structure 
 *  @msg1	64 bit msg1 structure 
 *  @msg2	64 bit msg2 structure 
 *  @msg3	64 bit msg3 structure 
 *  @dev_id	driver write can save a device id here
 *
 ******************************************************************************************/
void dummy_handler(uint32_t vc, uint32_t src_id, uint32_t size, uint32_t code, 
		   uint64_t msg0, uint64_t msg1, uint64_t msg2, uint64_t msg3, void *dev_id)
{
#if 0
	printk("[%s]: No Handler for message from stn_id=%d, bucket=%d, "
	       "size=%d, msg0=%llx, msg1=%llx dropping message\n",
	       __FUNCTION__, src_id, vc, size,
	       (unsigned long long)msg0, (unsigned long long)msg1);
#endif
}

/******************************************************************************************
 *
 * intial msg_hander_map with dummy_handler, when real driver msgring handler registered
 * it will override the entry with hander driver writer provided
 *
 ******************************************************************************************/
struct msgstn_handler msg_handler_map[XLP_MSG_HANDLE_MAX] = {
	[0 ... (XLP_MSG_HANDLE_MAX-1)] = {dummy_handler, NULL, NULL, NULL},
};


int nlm_xlp_register_napi_vc_handler(int vc, int (*handler)(int vc, int budget))
{
	if(vc < 0 || vc >= NLM_MAX_VC_PER_THREAD) {
		printk("%s, Error, invalid VC Passed %d\n", __FUNCTION__, vc);
		return -1;
	}

	if(!((1 << vc) & xlp_napi_vc_mask)) {
		printk("%s , Error, VC is not specified in napi vc mask\n", __FUNCTION__);
		return -1;
	}

	if(!xlp_fmn_init_done)
		xlp_fmn_init_done = 1;
	
	xlp_napi_vc_handlers[vc] = handler;
	return 0;
}
EXPORT_SYMBOL(nlm_xlp_register_napi_vc_handler);


int nlm_xlp_unregister_napi_vc_handler(int vc)
{
	if(vc < 0 || vc >= NLM_MAX_VC_PER_THREAD) {
		printk("%s, Error, invalid VC Passed %d\n", __FUNCTION__, vc);
		return -1;
	}
	xlp_napi_vc_handlers[vc] = NULL;
	return 0;
}
EXPORT_SYMBOL(nlm_xlp_unregister_napi_vc_handler);

int nlm_xlp_register_intr_vc_handler(int (*handler)(int vc))
{
	xlp_intr_vc_handler = handler;
	return 0;
}
EXPORT_SYMBOL(nlm_xlp_register_intr_vc_handler);

int nlm_xlp_register_intr_vc(int cpu, int vc)
{
	int node;
	unsigned long flags;

	if(vc < 0 || vc >= NLM_MAX_VC_PER_THREAD) {
		printk("%s, Error, invalid VC Passed %d\n", __FUNCTION__, vc);
		return -1;
	}

	if(!xlp_fmn_init_done)
		xlp_fmn_init_done = 1;
	
	node = cpu / 32;
	nlm_hal_enable_vc_intr(node, (cpu*4 + vc) & 0x7f);

	spin_lock_irqsave(&msgrng_lock, flags);
	intr_vc_mask[cpu] |= (1 << vc);
	spin_unlock_irqrestore(&msgrng_lock, flags);
	
	/*printk("%s in, cpu %d intr_vc_mask %x\n", __FUNCTION__, cpu, intr_vc_mask[cpu]);*/
	return 0;
}
EXPORT_SYMBOL(nlm_xlp_register_intr_vc);

int nlm_xlp_unregister_intr_vc(int cpu, int vc)
{
	unsigned long flags;

	if(vc < 0 || vc >= NLM_MAX_VC_PER_THREAD) {
		printk("%s, Error, invalid VC Passed %d\n", __FUNCTION__, vc);
		return -1;
	}

	spin_lock_irqsave(&msgrng_lock, flags);
	intr_vc_mask[cpu] &= (~(1 << vc));
	spin_unlock_irqrestore(&msgrng_lock, flags);
	return 0;
}
EXPORT_SYMBOL(nlm_xlp_unregister_intr_vc);

/*********************************************************************
 * nlm_xlp_msgring_int_handler 
 *
 *  @irq	msgring irq number
 *  @regs	linux systems call back function provide struct pt_regs 
 *  
 ********************************************************************/
void nlm_xlp_msgring_int_handler(unsigned int irq, struct pt_regs *regs)
{
	int vc = 0;
	uint32_t size = 0, code = 0, src_id = 0, cycles = 0;
	struct msgstn_handler *handler = 0;
	unsigned int status = 0;
	uint64_t msg0 = 0, msg1 = 0, msg2 = 0, msg3 = 0;
	unsigned int msg_status1 = 0, vc_empty_status = 0;
	int cpu = hard_smp_processor_id();
	int pop_vc_mask = nlm_cpu_vc_mask[cpu];
	uint32_t napi_vc_mask = xlp_napi_vc_mask & pop_vc_mask;
	unsigned long mflags; /* Currently unused */
	unsigned int vcmask;

	if (irq == XLP_IRQ_MSGRING_RVEC) {
                /* normal message ring interrupt */
                /* xlr_inc_counter(MSGRNG_INT);  */
                nlm_cpu_stat_update_msgring_int();
        } else {
                nlm_cpu_stat_update_msgring_pic_int();
        }


	irq_enter();

        msgrng_access_enable(mflags);
	cycles = read_c0_count();

	for (;;) {

		/* Read latest VC empty mask */
		msg_status1 = xlp_read_status1();

		vcmask = (~(msg_status1>>24) & intr_vc_mask[cpu]);
		if(vcmask && xlp_intr_vc_handler) {
			for(vc = 0; vc < 4; vc++) {
				if(!(vcmask & (1<<vc)))
					continue;
				xlp_intr_vc_handler(vc);
			}
		}

		if((~(msg_status1>>24) & napi_vc_mask) && xlp_fmn_init_done) {
			struct napi_struct *napi;

			/*Schedule napi routine to process messages from napi vc*/
		        napi = &__get_cpu_var(xlp_napi_fmn_poll_struct);
		        napi_schedule(napi);
			pop_vc_mask = pop_vc_mask & ~napi_vc_mask;
		}

		vc_empty_status = (msg_status1 >> 24) & pop_vc_mask;
		if (vc_empty_status == pop_vc_mask) break;  

		for( vc = 0; vc < 4; vc++)
		{
			if(!(pop_vc_mask & (1<<vc)))
				continue;
			status = xlp_message_receive( vc, &src_id, &size, &code, &msg0, &msg1, &msg2, &msg3);
			if(status != 0)
				continue;

			if (is_nlm_xlp2xx()) {
				handler = &msg_handler_map[xlp2xx_vc_to_handle_map[src_id]];
			}
			else if (is_nlm_xlp3xx()) {
				handler = &msg_handler_map[xlp3xx_vc_to_handle_map[src_id]];
			}
			else {
				handler = &msg_handler_map[vc_to_handle_map[src_id]];
			}

			/* Execute device driver fmn handler */
			(handler->action)(vc, src_id, size, code,
					  msg0, msg1, msg2, msg3, handler->dev_id);

		}

		/* Simulator currently doesn't simulate vc_empty_status1 bits! */
		//if (!hwemul) break;
	}
	nlm_cpu_stat_update_msgring_cycles(read_c0_count() - cycles);

	/* Clear VC interrupt status by writing 1s */
	if (is_nlm_xlp2xx()) {
		xlp_write_msg_int((pop_vc_mask));
	} else {
		xlp_write_status1( (msg_status1 | (pop_vc_mask << 16)) );
	}

        msgrng_access_disable(mflags);

	irq_exit();
}

EXPORT_SYMBOL(nlm_xlp_msgring_int_handler);

static DEFINE_PER_CPU(struct timer_list, msg_int_bkup_timer);
static int msg_handler_timer_enabled=0;
static void msg_timer_handler(unsigned long data)
{
	int cpu = smp_processor_id();
	struct timer_list *timer = &per_cpu(msg_int_bkup_timer, cpu);


	nlm_xlp_msgring_int_handler(XLP_IRQ_MSGRING_RVEC, NULL);

	timer->expires = jiffies + (HZ/100);
	add_timer(timer);
}
void init_msg_bkp_timer(void *data)
{
	int cpu = smp_processor_id();
	struct timer_list *timer = &per_cpu(msg_int_bkup_timer, cpu);

	init_timer(timer);
	timer->expires = jiffies + 10;
	timer->data = 0;
	timer->function = msg_timer_handler;
	add_timer(timer);
}

void xlp_poll_vc0_messages(void)
{
        int vc = 0;
        uint32_t size = 0, code = 0, src_id = 0;
        struct msgstn_handler *handler = 0;
        unsigned int status = 0;
        uint64_t msg0, msg1, msg2, msg3;
        unsigned int msg_status1 = 0, vc_empty_status = 0;
        int loop = 0;
        int pop_vc_mask = 0x1;
#ifdef CONFIG_32BIT
	unsigned long mflags;
#endif

#if 0
	if (hard_smp_processor_id() != 0)
		printk("Called handler on cpu %d from %s msgstatus: 0x%x\n",
			       hard_smp_processor_id(),
			       __FUNCTION__,xlp_read_status1());
#endif
        msg0 = msg1 = msg2 = msg3 = 0;
        msgrng_access_enable(mflags);
        for (loop = 0; loop < 16; loop++) {
                /* Read latest VC empty mask */
                msg_status1 = xlp_read_status1();
                vc_empty_status = (msg_status1 >> 24) & pop_vc_mask;
                if (vc_empty_status == pop_vc_mask)
                        break;
                status = xlp_message_receive( vc, &src_id, &size, &code, &msg0, &msg1, &msg2, &msg3);
                if(status != 0)
                        continue;
                handler = &msg_handler_map[vc_to_handle_map[src_id]];
                /* Execute device driver fmn handler */
                (handler->action)(vc, src_id, size, code,
                                msg0, msg1, msg2, msg3, handler->dev_id);
        }
        msgrng_access_disable(mflags);
}
EXPORT_SYMBOL(xlp_poll_vc0_messages);

/*******************************************************************************************
 *  register_xlp_msgring_handler 
 *
 *  @major      handler id number, each type handler has an ID
 *  @action     handler callback function (see dummy_handler above for detail)
 *  @dev_id	optional dev_id paramter for driver write to save device id
 *******************************************************************************************/
int register_xlp_msgring_handler(int major,
			     void (*action) (uint32_t, uint32_t, uint32_t, uint32_t,
					     uint64_t, uint64_t, uint64_t, uint64_t, void *),
			     void *dev_id)
{
	int ret = 1;
	unsigned long flags = 0;

	if (major >= XLP_MSG_HANDLE_MAX || action == NULL) {
		printk(KERN_ALERT "%s:%d  Invalid parameter: major=%d, "
		       "XLP_MAX_TX_STN=%d action=%p",
		       __FUNCTION__, __LINE__, major, XLP_MAX_TX_STNS, action);
		return ret;
	}

	/* Check if the message station is valid, if not return error */
	spin_lock_irqsave(&msgrng_lock, flags);

	if(!xlp_fmn_init_done)
		xlp_fmn_init_done = 1;

	if (is_nlm_xlp8xx_ax()) {
		if(msg_handler_timer_enabled == 0) {
			msg_handler_timer_enabled = 1;
			spin_unlock_irqrestore(&msgrng_lock, flags);
			// init_msg_bkp_timer(0);	Not required, taken care by on_each_cpu()
			on_each_cpu(init_msg_bkp_timer, 0, 1);
			spin_lock_irqsave(&msgrng_lock, flags);
		}
	}

	msg_handler_map[major].action = action;
	msg_handler_map[major].dev_id = dev_id;

	ret = 0;
	msgring_registered.value = 1;

	spin_unlock_irqrestore(&msgrng_lock, flags);

	return ret;
}

EXPORT_SYMBOL(register_xlp_msgring_handler);

int unregister_xlp_msgring_handler(int major, void *dev_id)
{
	unsigned long flags;

	if(major >= XLP_MSG_HANDLE_MAX){
		printk(KERN_ALERT "%s:%d  Invalid parameter: major=%d, "
		       "XLP_MAX_TX_STN=%d", __FUNCTION__, __LINE__, major,
		       XLP_MAX_TX_STNS);
		return -1;
	}
	spin_lock_irqsave(&msgrng_lock, flags);
	if(msg_handler_map[major].dev_id == dev_id){
		msg_handler_map[major].action = dummy_handler;
		msg_handler_map[major].dev_id = NULL;
		msg_handler_map[major].napi_final = NULL;
		msg_handler_map[major].napi_final_arg = NULL;
	}
	spin_unlock_irqrestore(&msgrng_lock, flags);
	return 0;
}

EXPORT_SYMBOL(unregister_xlp_msgring_handler);

int nlm_xlp_register_napi_final_handler(int major, void (*napi_final)(void *arg), void *arg) 
{
	if(major >= XLP_MSG_HANDLE_MAX){
		printk(KERN_ALERT "%s:%d  Invalid parameter: major=%d, "
		       "XLP_MAX_TX_STN=%d", __FUNCTION__, __LINE__, major,
		       XLP_MAX_TX_STNS);
		return -1;
	}
	msg_handler_map[major].napi_final = napi_final;
	msg_handler_map[major].napi_final_arg = arg;
	return 0;
}

EXPORT_SYMBOL(nlm_xlp_register_napi_final_handler);

void nlm_nmi_cpus(unsigned int mask)
{
	uint32_t cpumask = cpumask_to_uint32(&cpu_present_map); /* doesn't handle non-n0 nodes */
	uint32_t cpumask_lo;
	uint32_t cpumask_hi;
	const int nmi = 1;

	cpumask = cpumask & mask;

	cpumask_hi = cpumask >> 16;;
	cpumask_lo = cpumask & 0xffff;

	/* Send IRQ_MSGRING vector in an IPI to all cpus but the current one */
	if (cpumask_lo)
		nlh_pic_w64r(0, XLP_PIC_IPI_CTL, (nmi << 31) | cpumask_lo );

	if (cpumask_hi)
		nlh_pic_w64r(0, XLP_PIC_IPI_CTL, (nmi << 31) | (1 << 16) | (cpumask_hi));
}


/*********************************************************************
 * enable_msgconfig_int 
 *
 ********************************************************************/
void enable_msgconfig_int(void)
{
	unsigned long flags;
	flags = 0; 		/* suppress compiler warning */
	/* Need write interrupt vector to cp2 msgconfig register */
	msgrng_access_enable(flags);
	nlm_hal_set_fmn_interrupt(XLP_IRQ_MSGRING_RVEC);
	msgrng_access_disable(flags);
}

atomic_t nlm_common_counters[NR_CPUS][NLM_MAX_COUNTERS] __cacheline_aligned;

/*********************************************************************
 *  nlm_usb_init
 *
 ********************************************************************/
static void nlm_usb_init (void) __attribute__((unused));
static void nlm_usb_init (void)
{
	volatile unsigned int value;

        nlm_reg_t * gpio_mmio = netlogic_io_mmio(NETLOGIC_IO_GPIO_OFFSET);
        nlm_reg_t * usb_mmio  = netlogic_io_mmio(NETLOGIC_IO_USB_1_OFFSET);

	netlogic_write_reg(usb_mmio, 49, 0x10000000); //Clear Rogue Phy INTs
	netlogic_write_reg(usb_mmio, 50, 0x1f000000);

        netlogic_write_reg(usb_mmio,  1, 0x07000500);

	value = gpio_mmio[21];
	if ((value >> 22) & 0x01) {
		printk("Detected USB Host mode..\n");
		netlogic_write_reg(usb_mmio,  0, 0x02000000);
	}
	else {
		printk("Detected USB Device mode..\n");
		netlogic_write_reg(usb_mmio,  0, 0x01000000);
	}
}


/*********************************************************************
 * nlm_enable_vc_intr
 *********************************************************************/
void nlm_enable_vc_intr(void)
{
	int cpu, node;
	int vc_index = 0;
	int i = 0;

	for(cpu=0; cpu<NR_CPUS; cpu++){
                if(!cpu_isset(cpu, phys_cpu_present_map))
                        continue;
		node = cpu / 32;
		for(i=0; i<NLM_MAX_VC_PER_THREAD; i++)
		{
			vc_index = (i + cpu*NLM_MAX_VC_PER_THREAD) & 0x7f;
			if(nlm_cpu_vc_mask[cpu] & (1<<i)){
				/*enable interrupts*/
				nlm_hal_enable_vc_intr(node, vc_index);
			}else{
				nlm_hal_disable_vc_intr(node, vc_index);
			}
		}
	}
}


int xlp_fmn_poll(struct napi_struct *napi, int budget)
{
	int vc = 0;
	uint32_t size = 0, code = 0, src_id = 0;
	struct msgstn_handler *handler = 0;
	unsigned int status = 0;
	uint64_t msg0, msg1, msg2, msg3;
	int cpu = hard_smp_processor_id();
	int count = 0;
	int no_msg = 0;
	uint32_t napi_vc_mask = xlp_napi_vc_mask & nlm_cpu_vc_mask[cpu];
#ifdef CONFIG_32BIT
	unsigned long mflags;
#endif
	unsigned long napi_final_hndlr[XLP_MSG_HANDLE_MAX];
	int hndlr_cnt = 0, hndlr_id, i, rv;

	while(count < budget){
		for( no_msg = 0, vc = 0; vc < 4; vc++)
		{
			
			if(!(napi_vc_mask & (1<<vc)))
				continue;
	
			/* Explicit per vc napi handlers. Here the vc handler does the polling of
			 all the packets */
			if(xlp_napi_vc_handlers[vc]) {
				rv = (xlp_napi_vc_handlers)[vc](vc, budget);
				count += rv;
				if(rv == 0)
					no_msg++;
				continue;
			}
			
#ifdef CONFIG_32BIT
			msgrng_access_enable(mflags);
#endif
			status = xlp_message_receive( vc, &src_id, &size, &code, &msg0, &msg1, &msg2, &msg3);
#ifdef CONFIG_32BIT
			msgrng_access_disable(mflags);
#endif
			if(status != 0){
				no_msg++;
				continue;
			}
			count++;
			if (is_nlm_xlp3xx()) {
				hndlr_id = xlp3xx_vc_to_handle_map[src_id];
			}
			else if (is_nlm_xlp2xx()) {
				hndlr_id = xlp2xx_vc_to_handle_map[src_id];
			}
			else {
				hndlr_id = vc_to_handle_map[src_id];
			}

			handler = &msg_handler_map[hndlr_id];

			/* Execute device driver fmn handler */
			(handler->action)(vc, src_id, size, code,
				  msg0, msg1, msg2, msg3, handler->dev_id);

			if(handler->napi_final && (napi_final_needed[cpu][hndlr_id] == 0)) {
				napi_final_needed[cpu][hndlr_id] = 1;
				napi_final_hndlr[hndlr_cnt] = (unsigned long)handler;
				hndlr_cnt++;
			}
		}
		if(no_msg == xlp_napi_vc_count)
			break;
	}

	for(i = 0; i < hndlr_cnt; i++) {
		handler = (struct msgstn_handler *)napi_final_hndlr[i];
		handler->napi_final(handler->napi_final_arg);
		hndlr_id = handler - &msg_handler_map[0];
		napi_final_needed[cpu][hndlr_id] = 0;
	}
	

	/*Ack fmn interrupts.*/
	if(count < budget) {
		uint32_t val;
		unsigned long flags;
		local_irq_save(flags);
#ifdef CONFIG_32BIT
		msgrng_access_enable(mflags);
#endif
                napi_complete(napi);
		/* Need write vc into the register */
                if (is_nlm_xlp2xx()) {
                        xlp_write_msg_int(napi_vc_mask);
                }
                else {
			val =  _read_32bit_cp2_register(XLP_MSG_STATUS1_REG);
			//val |= ((1 << vc) << 16);
			val |= (napi_vc_mask << 16);
			_write_32bit_cp2_register(XLP_MSG_STATUS1_REG, val);
		}
#ifdef CONFIG_32BIT
		msgrng_access_disable(mflags);
#endif
		local_irq_restore(flags);
		return count;
	}
	return budget;
}

static inline int num_ones(unsigned int mask)
{
	int ret = 0;

	if (!mask) return 0;
	while ((mask &= (mask - 1))) ret++;
	return (ret + 1);
}


static int xlp_napi_fmn_setup(void)
{
	int i, cpu_count;
	struct napi_struct *napi;
	int weight_p = 300;

	xlp_napi_vc_count = num_ones(xlp_napi_vc_mask);
	printk("MSGRING_NAPI: Initializing NLM NAPI subsystem\n");

	init_dummy_netdev(&xlp_napi_fmn_dummy_dev);

	for (cpu_count = 0; cpu_count < NR_CPUS; cpu_count++)
	{
		napi = &per_cpu(xlp_napi_fmn_poll_struct, cpu_count);
		memset(napi, 0, sizeof(*napi));
		netif_napi_add(&xlp_napi_fmn_dummy_dev, napi, xlp_fmn_poll, weight_p);
		napi_enable(napi);
	}

	for (i = 0; i < NR_CPUS; i++) {
		per_cpu(xlp_napi_fmn_rx_count, i) = 0;
	}
	return 0;
}



/*********************************************************************
 * on_chip_init
 *  
 ********************************************************************/
extern void xlp_pic_init(u8);
extern void xlp_ites_init(void);
void on_chip_init(void)
{
	int i = 0, j = 0, node;

	cpu_logical_map(0)  = hard_smp_processor_id();
	node = hard_smp_processor_id() / NLM_MAX_CPU_PER_NODE;
	/* Set netlogic_io_base to the run time value */
	spin_lock_init(&msgrng_lock);
	msgring_registered.value = 0;
	nlm_hal_init();
	xlp_pic_init(node);
	/* On XLP, MSGRING config register is per hw-thread */
	enable_msgconfig_int();
	for (i = 0; i < NR_CPUS; i++) {
		for (j = 0; j < NLM_MAX_COUNTERS; j++) {
			atomic_set(&nlm_common_counters[i][j], 0);
		}
	}
	if(xlp_napi_vc_mask) {
		xlp_napi_fmn_setup();
	}
}
