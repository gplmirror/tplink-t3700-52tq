/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */

#include "types.h"
#include "nlm_hal.h"
#include "cpu_control_macros.h"

#if defined(NLM_HAL_NETOS)
#include "boot_lib.h"
#else
#error "Unsupported platform for NL HAL"
#endif


/* Globals Section
 */
uint32_t cores_bitmap = 0x1;	/* Bitmap of cores to enable  */
int threads_to_enable; 			/* Threads to enable per core */

#define NR_CPUS 32
#define TOTAL_THREAD_SIZE       (NLM_HAL_THREAD_SIZE * NR_CPUS)

/* Used by master cpu to save it's sp before waking up
 * others
 */
unsigned long __master_sp_temp;

/* Struct for temp. allocation
 * of sp/gp for secondary CPUs
 */
struct stack_pages {
        unsigned long stack[(TOTAL_THREAD_SIZE)/sizeof(long)];
};

struct stack_pages stack_pages_temp
__attribute__((__section__(".data.init_task"),
               __aligned__(NLM_HAL_THREAD_SIZE)));

/* Externs
 */
extern char boot_siblings_master_start[], boot_siblings_master_end[];
extern char reset_entry[], reset_entry_end[];

unsigned int xlp8xx_a01_workaround_needed;

static inline void jump_address(unsigned long entry)
{
	__asm__ __volatile__ (
			".set push   	\n"
			".set noreorder \n"
			"jalr  %0     	\n"
			"nop          	\n"
			".set pop       \n"
			:: "r"(entry));
}

/* Configure LSU */
static inline void config_lsu(void)
{
	uint32_t tmp0, tmp1, tmp2;

	if (!xlp8xx_a01_workaround_needed) {
		__asm__ __volatile__ (
			".set push\n"
			".set noreorder\n"
			"li      %0, "STR(LSU_DEFEATURE)"\n"
			"mfcr    %1, %0\n"
			"lui     %2, 0x4080\n"  /* Enable Unaligned Access, L2HPE */
			"or      %1, %1, %2\n"
			"mtcr    %1, %0\n"
			"li      %0, "STR(SCHED_DEFEATURE)"\n"
			"lui     %1, 0x0100\n"  /* Experimental: Disable BRU accepting ALU ops */
			"mtcr    %1, %0\n"
			".set pop\n"
			: "=r" (tmp0), "=r" (tmp1), "=r" (tmp2)
		);
		return;
	}

	__asm__ __volatile__ (
		".set push\n"
		".set noreorder\n"
		"li      %0, "STR(LSU_DEFEATURE)"\n"
		"mfcr    %1, %0\n"

		"lui     %2, 0x4080\n"  /* Enable Unaligned Access, L2HPE */
		"or      %1, %1, %2\n"

		"li      %2, ~0xe\n"    /* S1RCM */
		"and     %1, %1, %2\n"

		"mtcr    %1, %0\n"

		"li      %0, "STR(SCHED_DEFEATURE)"\n"
		"lui     %1, 0x0100\n"  /* Experimental: Disable BRU accepting ALU ops */
		"mtcr    %1, %0\n"

		".set pop\n"
		: "=r" (tmp0), "=r" (tmp1), "=r" (tmp2)
	);
}

void enable_cores(unsigned int node, unsigned int cbitmap) {

	uint32_t core, value;

	if (node == 0)
		cbitmap &= 0xFE;

	for (core=0x2; core!=(0x1<<8); core<<=1) {

		if (cbitmap & core) {

			/* Enable CPU clock
			 */
			value = nlm_hal_read_sys_reg(node, SYS_COREDFSDISCTRL) & ~core;
			nlm_hal_write_sys_reg(node, SYS_COREDFSDISCTRL, value);

			/* Remove CPU Reset */
			value = (nlm_hal_read_sys_reg(node, SYS_CPURST) & ~core) & 0xff;
			nlm_hal_write_sys_reg(node, SYS_CPURST, value);
			nlm_print("%s wrote %lx to SYS_CPU_RESET\n", __func__, value);

			/* Poll for CPU to mark itself coherent */
			do {
				value = nlm_hal_read_sys_reg(node, SYS_CPUNONCOHERENTMODE) & core;
			} while (value);
#if 0
			/* hangs..? */
			udelay(50);
#endif
		}
	}
}

void (*cpu_park_func) (unsigned long);

/* Main routine
 */
void enable_cpus(unsigned int node, unsigned long thread_bitmask, unsigned long park_func)
{
	uint32_t cbitmap;
	uint32_t t0_bitmap = 0x0;
	uint32_t t0_positions = 0, index=3;

	/* Extract the bitmap of 'cores'
	 * from the complete input bitmask
	 */
	t0_bitmap = thread_bitmask & 0x11111111;

	/* FIXME: we should also remove it for xlp8xx a2, but we do not have interface function
	 * for it yet. Once A1 is phased out in the field, this workaround code should be removed.
	 */
	xlp8xx_a01_workaround_needed = is_nlm_xlp8xx_ax();

	cores_bitmap = 0x1;

	for (t0_positions = 4; t0_positions <=28; t0_positions+=4) {
		cores_bitmap |= ((t0_bitmap & (1 << t0_positions)) >> index);
		index+=3;
	}

	nlm_print("Cores Bitmap=0x%lx\n", cores_bitmap);

	/* Configure LSU on Core0. */
	config_lsu();

	/* As for the threads to be enabled
	 * per core, use Core0 as a reference
	 */
	thread_bitmask = thread_bitmask & 0xf;

	switch (thread_bitmask) {
		case 0x1:
			if (num_ones(cores_bitmap) == 1) {
				nlm_print("Core0/Thread0 is enabled.\n");
				return;
			}
		case 0x3:
		case 0xf:
			threads_to_enable = num_ones(thread_bitmask);
			nlm_print("Enabling (%d) cores, (%d) threads/core\n",
					num_ones(cores_bitmap), threads_to_enable);
			/* C0T0 should always be enabled.
			 * In the GPIO SW Reset Register,
			 * 	'0' => Core is enabled
			 */
			cbitmap = cores_bitmap;
			cores_bitmap = ~(cores_bitmap) & 0xfe;
			break;
		default:
			nlm_print("[Core 0] : Invalid Threads number!\n");
			return;
	}

	cpu_park_func = (void *)park_func;

	jump_address((unsigned long)boot_siblings_master_start);

	/* Bring the other cores online here
	 * Copy reset code into KSEG0 space
	 */
	if (cores_bitmap != 0xfe) {
		memcpy((void *)(NMI_BASE),
			   (void *)&reset_entry,
			   (reset_entry_end - reset_entry));
	}

	enable_cores(node, cbitmap);
	return;
}
