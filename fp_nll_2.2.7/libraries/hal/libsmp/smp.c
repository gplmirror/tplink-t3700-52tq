/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */

#include "nlm_hal_macros.h"

extern void (*cpu_park_func) (unsigned long);

void prom_boot_cpus_secondary(void *args)
{
	(*cpu_park_func)(0);
}

#ifdef CONFIG_NLM_XLP

extern void prom_pre_boot_secondary_cpus(void *);
extern uint32_t xlr_linux_cpu_mask;
 
#ifdef CONFIG_MAPPED_KERNEL
#define secondary_cpus_bootup_func \
       ((unsigned long)prom_pre_boot_secondary_cpus - \
        (unsigned long)LOADADDR + (unsigned long)PHYSADDR)
#else
#define secondary_cpus_bootup_func prom_pre_boot_secondary_cpus
#endif

int wakeup_secondary_cpus(void)
{
	nlm_print("Enabling CPU Mask [0x%x]\n", onlinemask);
	enable_cpus(0, onlinemask);	//FIXME J:
	return 0;
}
#endif /* #ifdef CONFIG_NLM_XLP */
