/*-
 * Copyright (c) 2003-2012 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */

#ifndef __CPUCONTROL_MACROS_H__
#define __CPUCONTROL_MACROS_H__

#include "nlm_hal_macros.h"

#define CHIP_PID_XLP    0x00
#define NMI_BASE    	KSEG1+0X1fc00000UL
#define NMI_BASE_ASM   	0xbfc00000

/* CPU Internal Blocks specific to XLP .
 * These are accessed using the mfcr/mtcr
 * instructions. Blocks [0-5] are same for
 * XLR and XLP
 */
#define CPU_BLOCKID_MAP                         0x0a
/* Offsets of interest from the 'MAP' Block */
#define BLKID_MAP_THREADMODE                    0x00 
#define BLKID_MAP_EXT_EBASE_ENABLE              0x04 
#define BLKID_MAP_CCDI_CONFIG                   0x08
#define BLKID_MAP_THRD0_CCDI_STATUS             0x0c    
#define BLKID_MAP_THRD1_CCDI_STATUS             0x10
#define BLKID_MAP_THRD2_CCDI_STATUS             0x14    
#define BLKID_MAP_THRD3_CCDI_STATUS             0x18
#define BLKID_MAP_THRD0_DEBUG_MODE              0x1c
#define BLKID_MAP_THRD1_DEBUG_MODE              0x20
#define BLKID_MAP_THRD2_DEBUG_MODE              0x24
#define BLKID_MAP_THRD3_DEBUG_MODE              0x28
#define BLKID_MAP_MISC_STATE                    0x60
#define BLKID_MAP_DEBUG_READ_CTL                0x64
#define BLKID_MAP_DEBUG_READ_REG0               0x68
#define BLKID_MAP_DEBUG_READ_REG1               0x6c

#define CPU_BLOCKID_SCH                         7
#define CPU_BLOCKID_SCU                         8
#define CPU_BLOCKID_FPU                         9

#define LSU_DEFEATURE 0x304
#define SCHED_DEFEATURE 0x700

/* ----------------------------------
 *   XLP RESET Physical Address Map
 * ----------------------------------
 * PCI ECFG : 0x18000000 - 0x1bffffff 
 * PCI CFG  : 0x1c000000 - 0x1cffffff 
 * FLASH    : 0x1fc00000 - 0x1fffffff 
 * ----------------------------------
 */

/* 
 * The DEFAULT_XLP_IO_BASE value is what is
 * programmed in the NBU's (NorthBridge Unit) 
 * ECFG_BAR register. The NBU itself is 
 * accessible as [BDF:0,0,0].
 */
#define DEFAULT_XLP_IO_BASE       KSEG1+0x18000000
#define DEFAULT_XLP_IO_BASE_VIRT  KSEG1+0x18000000      /* IO_BASE for Assembly macros */
#define DEFAULT_CPU_IO_BASE       DEFAULT_XLP_IO_BASE
#define DEFAULT_CPU_IO_BASE_VIRT  DEFAULT_XLP_IO_BASE_VIRT
#define CPU_IO_SIZE               (64<<20)        /* Size of the ECFG Space      */
#define HDR_OFFSET                0x100           /* Skip 256 bytes of cfg. hdrs */

/* The On-Chip functional blocks for XLP */

/* --------------------------------------------------------------*/
/* Accesses Based on Enhanced Configuration Mechanism            */
/* --------------------------------------------------------------*/
/* Interface | Bus          | Dev       |  Func                  */
/* --------------------------------------------------------------*/
#define        BRIDGE        (0x00<<20) | (0x00<<15) | (0x00<<12)
#define        PIC           (0x00<<20) | (0x00<<15) | (0x04<<12)
#define        CMS           (0x00<<20) | (0x04<<15) | (0x00<<12)
#define        UART0         (0x00<<20) | (0x06<<15) | (0x00<<12)
#define        UART1         (0x00<<20) | (0x06<<15) | (0x01<<12)
#define        I2C0          (0x00<<20) | (0x06<<15) | (0x02<<12)
#define        I2C1          (0x00<<20) | (0x06<<15) | (0x03<<12)
#define        GPIO          (0x00<<20) | (0x06<<15) | (0x04<<12)
#define        SYS           (0x00<<20) | (0x06<<15) | (0x05<<12)
#define        JTAG          (0x00<<20) | (0x06<<15) | (0x06<<12)
#define        NOR           (0x00<<20) | (0x07<<15) | (0x00<<12)
#define        NAND          (0x00<<20) | (0x07<<15) | (0x01<<12)
#define        SPI           (0x00<<20) | (0x07<<15) | (0x02<<12)
#define        MMC           (0x00<<20) | (0x07<<15) | (0x03<<12)

#define CPU_MMIO_OFFSET(x) (DEFAULT_CPU_IO_BASE_VIRT + (x) + HDR_OFFSET)


#define SYS_CHIPRST_REG                 0
#define SYS_PWRONRSTCFG0_REG            1
#define SYS_EFUSEDEV_CFG0_REG           2
#define SYS_EFUSEDEV_CFG1_REG           3
#define SYS_EFUSEDEV_CFG2_REG           4
#define SYS_EFUSEDEV_CFG3_REG           5
#define SYS_EFUSEDEV_CFG4_REG           6
#define SYS_EFUSEDEV_CFG5_REG           7
#define SYS_EFUSEDEV_CFG6_REG           8
#define SYS_EFUSEDEV_CFG7_REG           9 
#define SYS_PLLCTRL_REG                 10
#define SYS_CPURST_REG                  11
#define SYS_CPUTHREADEN_REG             12
#define SYS_CPUNONCOHERENTMODE_REG      13
#define SYS_COREDFSDISCTRL_REG          14
#define SYS_COREDFSRSTCTRL_REG          15
#define SYS_COREDFSBYPCTRL_REG          16
#define SYS_COREDFSPHACTRL_REG          17
#define SYS_COREDFSDIVCTRL_REG          18
#define SYS_SYSRST_REG                  19
#define SYS_SYSDFSDISCTRL_REG           20
#define SYS_SYSDFSRSTCTRL_REG           21
#define SYS_SYSDFSBYPCTRL_REG           22
#define SYS_SYSDFSDIVCTRL_REG           23
#define SYS_DMCDFSCTRL_REG              24

#ifndef __ASSEMBLY__

static inline int num_ones(unsigned long mask)
{
	int  nones;
	for (nones = 0; mask; mask >>= 1) {
		if (mask & 0x1)
			++nones;
	}
	return nones;
}

enum processor_sys
{
	SYS_CHIPRST                     = 0,
	SYS_PWRONRSTCFG0                = 1,
	SYS_EFUSEDEV_CFG0               = 2,
	SYS_EFUSEDEV_CFG1               = 3,
	SYS_EFUSEDEV_CFG2               = 4,
	SYS_EFUSEDEV_CFG3               = 5,
	SYS_EFUSEDEV_CFG4               = 6,
	SYS_EFUSEDEV_CFG5               = 7,
	SYS_EFUSEDEV_CFG6               = 8,
	SYS_EFUSEDEV_CFG7               = 9, 
	SYS_PLLCTRL                     = 10,
	SYS_CPURST                      = 11,
	SYS_CPUTHREADEN                 = 12,
	SYS_CPUNONCOHERENTMODE          = 13,
	SYS_COREDFSDISCTRL              = 14,
	SYS_COREDFSRSTCTRL              = 15,
	SYS_COREDFSBYPCTRL              = 16,
	SYS_COREDFSPHACTRL              = 17,
	SYS_COREDFSDIVCTRL              = 18,
	SYS_SYSRST                      = 19,
	SYS_SYSDFSDISCTRL               = 20,
	SYS_SYSDFSRSTCTRL               = 21,
	SYS_SYSDFSBYPCTRL               = 22,
	SYS_SYSDFSDIVCTRL               = 23,
	SYS_DMCDFSCTRL                  = 24
};
#endif
#endif /* __CPUCONTROL_MACROS_H__ */
