
/*-
 * Copyright (c) 2003-2013 Broadcom Corporation
 * All Rights Reserved
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * http://www.gnu.org/licenses/gpl-2.0.txt
 * or the Broadcom license below:

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_4# */
#ifdef NLM_HAL_LINUX_KERNEL
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <asm/netlogic/hal/nlm_eeprom.h>
#else
#include "nlm_eeprom.h"
#endif


int  eeprom_get_mac_addr(struct eeprom_data *nlm_eeprom, unsigned char *mac, int interface)
{
	unsigned char *eeprom_mac;
	unsigned int offset;
	unsigned char dev_addr;

	if(nlm_eeprom==NULL){
		return -1;
	}

	dev_addr = nlm_eeprom->i2c_dev_addr;
	eeprom_mac = interface?(nlm_eeprom->mac_addr1):(nlm_eeprom->mac_addr0);

	offset= (unsigned int) (interface?(unsigned long)(((struct eeprom_data*)0)->mac_addr1):(unsigned long)(((struct eeprom_data*)0)->mac_addr0));
	nlm_eeprom->eeprom_i2c_read_bytes(dev_addr, offset,0, eeprom_mac, MAC_LEN);
	memcpy(mac, eeprom_mac, MAC_LEN);
	return 0;
}

int  eeprom_set_mac_addr(struct eeprom_data *nlm_eeprom, unsigned char *mac, int interface)
{
	unsigned char *eeprom_mac;
	unsigned char dev_addr,i;
	unsigned int offset;
	if(nlm_eeprom==NULL){
		return -1;
	}
	dev_addr = nlm_eeprom->i2c_dev_addr;

	eeprom_mac = interface?(nlm_eeprom->mac_addr1):(nlm_eeprom->mac_addr0);
	for(i=0;i<6;i++){
                eeprom_mac[i]= mac[i];
        }
	offset= (unsigned int)(interface?(unsigned long)(((struct eeprom_data*)0)->mac_addr1):(unsigned long)(((struct eeprom_data*)0)->mac_addr0));
	nlm_eeprom->eeprom_i2c_write_bytes(dev_addr, offset,0, eeprom_mac, MAC_LEN);
	return 0;
}
int  eeprom_get_magic_bytes(struct eeprom_data *nlm_eeprom, unsigned char *mac, int interface)
{
        unsigned char *eeprom_mac;
        unsigned int offset;
        unsigned char dev_addr;

        if(nlm_eeprom==NULL){
                return -1;
        }

        dev_addr = nlm_eeprom->i2c_dev_addr;
        eeprom_mac = interface?(nlm_eeprom->magic_bytes1):(nlm_eeprom->magic_bytes0);

        offset= (unsigned int)(interface?(unsigned long)(((struct eeprom_data*)0)->magic_bytes1):(unsigned long)(((struct eeprom_data*)0)->magic_bytes0));
        nlm_eeprom->eeprom_i2c_read_bytes(dev_addr, offset,0, eeprom_mac, MAGIC_LEN);
        memcpy(mac, eeprom_mac, MAGIC_LEN);
        return 0;
}
int  eeprom_dump(struct eeprom_data *nlm_eeprom, unsigned char *mac, int offset, int len)
{

        unsigned char dev_addr;

        if(nlm_eeprom==NULL){
                return -1;
        }

        dev_addr   = nlm_eeprom->i2c_dev_addr;

        nlm_eeprom->eeprom_i2c_read_bytes(dev_addr, offset,0,mac,len);
        return 0;
}


#ifdef NLM_HAL_LINUX_KERNEL
/*Add  API here if any API  from above is needed*/
EXPORT_SYMBOL(eeprom_get_mac_addr);
EXPORT_SYMBOL(eeprom_set_mac_addr);
EXPORT_SYMBOL(eeprom_get_magic_bytes);
EXPORT_SYMBOL(eeprom_dump);
#endif

