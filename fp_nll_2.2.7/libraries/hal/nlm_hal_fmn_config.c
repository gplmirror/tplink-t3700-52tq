
/*-
 * Copyright (c) 2003-2013 Broadcom Corporation
 * All Rights Reserved
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * http://www.gnu.org/licenses/gpl-2.0.txt
 * or the Broadcom license below:

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_4# */
#ifdef NLM_HAL_LINUX_KERNEL
#include <linux/types.h>
#include <linux/kernel.h>
#include <asm/netlogic/hal/nlm_hal_fmn.h>
#else
#include "nlm_hal_fmn.h"
#endif
#include "libfdt.h"
#include "fdt_helper.h"
#include "nlm_hal_sys.h"

#define CPU		1
#define PCIE	2
#define GDX		3
#define CMP		4
#define CRYPTO	5
#define POE		6
#define NAE		7
#define RSA		8

#define INT_EN 			0x0800000000000000ULL
#define OUTQ_EN			0x8000000000000000ULL
#define SPILL_EN 		0x4000000000000000ULL
#define LOW40MASK       	0x000000ffffffffffULL
#define UP46MASK        	0xfffffffffffc0000ULL
#define UP52MASK        	0xfffffffffffff000ULL
#define LOW18MASK       	0x000000000003ffffULL
#define LOW4KMASK       	0x00000000000003ffULL
#define OUTQ_SIZE		32
#define NUM_SGMII_PORT		18
#define MAX_TXVC		524
#define FMN_OUTQ		1024
#define NUM_CORES		8
#define NUM_CPU_VC		128
#define NUM_CPU_POPQ		256
#define NUM_THREADS		4
#define NUM_VC_PER_THREAD	4


nlm_fmn_config_t xlp3xx_fmn_config[] = {
[XLP_MSG_HANDLE_CPU0] = {XLP_STNID_CPU0, XLP_CPU0_VC_LIMIT},
[XLP_MSG_HANDLE_CPU1] = {XLP_STNID_CPU1, XLP_CPU1_VC_LIMIT},
[XLP_MSG_HANDLE_CPU2] = {XLP_STNID_CPU2, XLP_CPU2_VC_LIMIT},
[XLP_MSG_HANDLE_CPU3] = {XLP_STNID_CPU3, XLP_CPU3_VC_LIMIT},

[XLP_MSG_HANDLE_CPU4] = {XLP_3XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_CPU5] = {XLP_3XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_CPU6] = {XLP_3XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_CPU7] = {XLP_3XX_INVALID_STATION, 0},

[XLP_MSG_HANDLE_PCIE0] = {XLP_PCIE0_VC_BASE, XLP_PCIE0_VC_LIMIT},
[XLP_MSG_HANDLE_PCIE1] = {XLP_PCIE1_VC_BASE, XLP_PCIE1_VC_LIMIT},
[XLP_MSG_HANDLE_PCIE2] = {XLP_PCIE2_VC_BASE, XLP_PCIE2_VC_LIMIT},
[XLP_MSG_HANDLE_PCIE3] = {XLP_PCIE3_VC_BASE, XLP_PCIE3_VC_LIMIT},

[XLP_MSG_HANDLE_DTRE] = {XLP_GDX_VC_BASE, XLP_GDX_VC_LIMIT},

[XLP_MSG_HANDLE_GDX]  = {XLP_GDX_VC_BASE, XLP_GDX_VC_LIMIT},
[XLP_MSG_HANDLE_REGX] = {XLP_3XX_REGEX_VC_BASE, XLP_3XX_REGEX_VC_LIMIT},
[XLP_MSG_HANDLE_RSA_ECC] = {XLP_3XX_RSA_ECC_VC_BASE, XLP_3XX_RSA_ECC_VC_LIMIT},
[XLP_MSG_HANDLE_CRYPTO] = {XLP_3XX_CRYPTO_VC_BASE, XLP_3XX_CRYPTO_VC_LIMIT},

[XLP_MSG_HANDLE_SRIO] = {XLP_3XX_SRIO_VC_BASE, XLP_3XX_SRIO_VC_LIMIT},

[XLP_MSG_HANDLE_CMP] = {XLP_3XX_INVALID_STATION, 0},

[XLP_MSG_HANDLE_POE] = {XLP_3XX_POE_VC_BASE, XLP_3XX_POE_VC_LIMIT},
[XLP_MSG_HANDLE_NAE_0] = {XLP_3XX_NET_TX_VC_BASE, XLP_3XX_NET_TX_VC_LIMIT},

[XLP_MSG_HANDLE_INVALID] = {XLP_3XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_MAX] = {XLP_3XX_INVALID_STATION, 0},
};

nlm_fmn_config_t xlp2xx_fmn_config[] = {
[XLP_MSG_HANDLE_CPU0] = {XLP_STNID_CPU0, XLP_CPU0_VC_LIMIT},
[XLP_MSG_HANDLE_CPU1] = {XLP_STNID_CPU1, XLP_CPU1_VC_LIMIT},

[XLP_MSG_HANDLE_CPU2] = {XLP_2XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_CPU3] = {XLP_2XX_INVALID_STATION, 0},

[XLP_MSG_HANDLE_CPU4] = {XLP_2XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_CPU5] = {XLP_2XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_CPU6] = {XLP_2XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_CPU7] = {XLP_2XX_INVALID_STATION, 0},

[XLP_MSG_HANDLE_PCIE0] = {XLP_PCIE0_VC_BASE, XLP_PCIE0_VC_LIMIT},
[XLP_MSG_HANDLE_PCIE1] = {XLP_PCIE1_VC_BASE, XLP_PCIE1_VC_LIMIT},
[XLP_MSG_HANDLE_PCIE2] = {XLP_PCIE2_VC_BASE, XLP_PCIE2_VC_LIMIT},
[XLP_MSG_HANDLE_PCIE3] = {XLP_PCIE3_VC_BASE, XLP_PCIE3_VC_LIMIT},

[XLP_MSG_HANDLE_DTRE] = {XLP_GDX_VC_BASE, XLP_GDX_VC_LIMIT},

[XLP_MSG_HANDLE_GDX]  = {XLP_GDX_VC_BASE, XLP_GDX_VC_LIMIT},
[XLP_MSG_HANDLE_REGX] = {XLP_2XX_REGEX_VC_BASE, XLP_2XX_REGEX_VC_LIMIT},
[XLP_MSG_HANDLE_RSA_ECC] = {XLP_2XX_RSA_ECC_VC_BASE, XLP_2XX_RSA_ECC_VC_LIMIT},
[XLP_MSG_HANDLE_CRYPTO] = {XLP_2XX_CRYPTO_VC_BASE, XLP_2XX_CRYPTO_VC_LIMIT},

[XLP_MSG_HANDLE_SRIO] = {XLP_2XX_INVALID_STATION,0},

[XLP_MSG_HANDLE_CMP] = {XLP_2XX_CDE_VC_BASE, XLP_2XX_CDE_VC_BASE},

[XLP_MSG_HANDLE_POE] = {XLP_2XX_POE_VC_BASE, XLP_2XX_POE_VC_LIMIT},
[XLP_MSG_HANDLE_NAE_0] = {XLP_2XX_NET_TX_VC_BASE, XLP_2XX_NET_VC_LIMIT},

[XLP_MSG_HANDLE_INVALID] = {XLP_2XX_INVALID_STATION, 0},
[XLP_MSG_HANDLE_MAX] = {XLP_2XX_INVALID_STATION, 0},
};
extern struct nlm_node_config nlm_node_cfg;

/* #define FMN_DEBUG 1 */

static unsigned int fmn_cfg_value[XLP_NET_VC_LIMIT + 1];
static unsigned int fmn_default_credits = XLP_FMN_DEFAULT_CREDITS;
static unsigned int fmn_default_qsize = XLP_FMN_DEFAULT_QUEUE_SIZE;
static unsigned long long fmn_spill_mem_addr = XLP_FMNQ_SPILL_DEFAULT_MEM_ADDR;
static unsigned long long fmn_spill_mem_size = XLP_FMNQ_SPILL_DEFAULT_MEM_SIZE;


static struct fmn_qsize_credit_config fmn_qsize_credit_cfg[XLP_MSG_BLK_MAX] = {
	[XLP_MSG_BLK_CPU] =     { "cpu",    XLP_CPU0_VC_BASE,      XLP_CPU7_VC_LIMIT,      8, 1 },
	[XLP_MSG_BLK_POPQ] =    { "popq",   XLP_POPQ_VC_BASE,      XLP_POPQ_VC_LIMIT,      0, 1 },
        [XLP_MSG_BLK_PCIE0] =    { "pcie0",   XLP_PCIE0_VC_BASE,     XLP_PCIE0_VC_LIMIT,     1, 1 },
        [XLP_MSG_BLK_PCIE1] =    { "pcie1",   XLP_PCIE1_VC_BASE,     XLP_PCIE1_VC_LIMIT,     1, 1 },
        [XLP_MSG_BLK_PCIE2] =    { "pcie2",   XLP_PCIE2_VC_BASE,     XLP_PCIE2_VC_LIMIT,     1, 1 },
        [XLP_MSG_BLK_PCIE3] =    { "pcie3",   XLP_PCIE3_VC_BASE,     XLP_PCIE3_VC_LIMIT,     1, 1 },
	[XLP_MSG_BLK_GDX]  =    { "gdx",    XLP_GDX_VC_BASE,       XLP_GDX_VC_LIMIT,       1, 1 },
	[XLP_MSG_BLK_RSA_ECC] = { "rsa",    XLP_RSA_ECC_VC_BASE,   XLP_RSA_ECC_VC_LIMIT,   1, 1 },
	[XLP_MSG_BLK_CRYPTO] =  { "crypto", XLP_CRYPTO_VC_BASE,    XLP_CRYPTO_VC_LIMIT,    1, 1 },
	[XLP_MSG_BLK_CMP] =     { "cmp",    XLP_CMP_VC_BASE,       XLP_CMP_VC_LIMIT,       1, 1 },
	[XLP_MSG_BLK_POE] =     { "poe",    XLP_POE_VC_BASE,       XLP_POE_VC_LIMIT,       1, 1 },
	[XLP_MSG_BLK_NAE] =     { "nae",    XLP_NET_VC_BASE,       XLP_NET_VC_LIMIT,       1, 1 },
	[XLP_MSG_BLK_REGX] =    { "regx",   XLP_3XX_REGEX_VC_BASE, XLP_3XX_REGEX_VC_LIMIT, 1, 1 },
	[XLP_MSG_BLK_SRIO] =    { "srio",   XLP_3XX_SRIO_VC_BASE,  XLP_3XX_SRIO_VC_LIMIT,  1, 1 },
};

int station_exist_in_2xx(int qid)
{
	int hndl;
	nlm_fmn_config_t *fmn_config = &xlp2xx_fmn_config[0];
	for(hndl = 0; hndl < XLP_MSG_HANDLE_MAX; hndl++) {
		if((qid >= fmn_config[hndl].base_vc) && (qid <= fmn_config[hndl].vc_limit))
			return 1;

	}
	/* check for popq. 
	TODO : We should add this also in to the array 
	 */
	if(qid >= 128 && qid <= 159)
		return 1;

	return 0;
}

/* called based on the chip type */
static void fmn_modify_qsize_credit_config(int node, int blk, int ntxstns, int b_stid, int e_stid)
{
	struct fmn_qsize_credit_config *fmn_q_config = nlm_node_cfg.fmn_cfg[node]->fmn_q_config;

	if(ntxstns >= 0)
		fmn_q_config[blk].n_txstns = ntxstns;
	if(b_stid >= 0)
		fmn_q_config[blk].b_stid = b_stid;
	if(e_stid >= 0)
		fmn_q_config[blk].e_stid = e_stid;
}

static void fmn_invalidate_blocks(int node, int blk)
{
	struct fmn_qsize_credit_config *fmn_q_config = nlm_node_cfg.fmn_cfg[node]->fmn_q_config;

	fmn_q_config[blk].valid = 0;
}


static void fmn_invalidate_block_on_allnodes(int block)
{
	int node;

	for(node=0; node < nlm_node_cfg.num_nodes; node++)
		fmn_invalidate_blocks(node, block); 
}

static void update_fmn_config(void)
{
	uint32_t pid, epid, config;

	pid = get_proc_id();

	switch(pid) {
                case CHIP_PROCESSOR_ID_XLP_8_4_XX:
			fmn_invalidate_block_on_allnodes(XLP_MSG_BLK_REGX);
			fmn_invalidate_block_on_allnodes(XLP_MSG_BLK_SRIO);
                        break;
		case CHIP_PROCESSOR_ID_XLP_3XX:
			xlp3xx_fmn_config[XLP_MSG_HANDLE_CMP].base_vc = XLP_INVALID_STATION;
			fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_CMP);
			epid = get_xlp3xx_epid();
			switch(epid) {
				case CPU_EXTPID_XLP_202a:
				case CPU_EXTPID_XLP_201a:
				case CPU_EXTPID_XLP_101a:
					xlp3xx_fmn_config[XLP_MSG_HANDLE_RSA_ECC].base_vc = XLP_INVALID_STATION;
					xlp3xx_fmn_config[XLP_MSG_HANDLE_CRYPTO].base_vc = XLP_INVALID_STATION;
					xlp3xx_fmn_config[XLP_MSG_HANDLE_SRIO].base_vc = XLP_INVALID_STATION;
					xlp3xx_fmn_config[XLP_MSG_HANDLE_REGX].base_vc = XLP_INVALID_STATION;

					fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_RSA_ECC);
					fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_CRYPTO);
					fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_SRIO);
					fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_REGX);
					break;
			}
			break;
		case CHIP_PROCESSOR_ID_XLP_2XX:
			xlp2xx_fmn_config[XLP_MSG_HANDLE_SRIO].base_vc =  XLP_INVALID_STATION;
			fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_SRIO);
			
			config = nlm_hal_read_32bit_reg(SYS_REG_BASE , (SYS_REG_INDEX(EFUSE_DEVICE_CFG2)));
			nlm_print("Efuse config 2 0x%x\n",config);

			if (config & (1 << XLP2XX_RSA_BLOCK_INDEX)) {
				xlp2xx_fmn_config[XLP_MSG_HANDLE_RSA_ECC].base_vc = XLP_INVALID_STATION;
				fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_RSA_ECC);
			}

			if (config & (1 << XLP2XX_REGX_BLOCK_INDEX)) {
				xlp2xx_fmn_config[XLP_MSG_HANDLE_REGX].base_vc = XLP_INVALID_STATION;
				fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_REGX);
			}

			config = nlm_hal_read_32bit_reg(SYS_REG_BASE , (SYS_REG_INDEX(EFUSE_DEVICE_CFG1)));
			nlm_print("Efuse config 1 0x%x\n",config);

			if (config & (1 << XLP2XX_CMP_BLOCK_INDEX)) {
				xlp2xx_fmn_config[XLP_MSG_HANDLE_CMP].base_vc = XLP_INVALID_STATION;
				fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_CMP);
			}

			if (config & (1 << XLP2XX_CRYPTO_BLOCK_INDEX)) {
				xlp2xx_fmn_config[XLP_MSG_HANDLE_CRYPTO].base_vc = XLP_INVALID_STATION;
				fmn_invalidate_block_on_allnodes( XLP_MSG_BLK_CRYPTO);
			}
			break;
	}
}

#if 0
static void fmn_qsize_credit_cfg_extract(void *fdt)
{
	char path[128];
	const void *pval;
	int s_stn = 0, d_stn = 0, nodeoffset, plen, len = 0;
	unsigned int qsize = XLP_FMN_DEFAULT_QUEUE_SIZE;
	unsigned int credits = XLP_FMN_DEFAULT_CREDITS;

	/* initialize with the default values, given the config file */
	if(fdt) {
		strcpy(path, "/fmn-config");
		nodeoffset = fdt_path_offset(fdt, path);
		if(nodeoffset >= 0)  {
			pval = fdt_getprop(fdt, nodeoffset, "default-queue-size", &plen);
			if(pval != NULL) {
				qsize = fdt32_to_cpu(*(unsigned int *)pval);
				fmn_default_qsize = qsize;
			}
			pval = fdt_getprop(fdt, nodeoffset, "default-credits", &plen);
			if(pval != NULL) {
				credits = fdt32_to_cpu(*(unsigned int *)pval);
				fmn_default_credits = credits;
			}
		}

		strcpy(path, "/extra-mem-config");
		nodeoffset = fdt_path_offset(fdt, path);
		if(nodeoffset >= 0)  {
			pval = fdt_getprop(fdt, nodeoffset, "fmn-spill-mem-range", &plen);
			if(pval != NULL) {
				fmn_spill_mem_addr = fdt64_to_cpu(*(unsigned long long *)pval);
				fmn_spill_mem_size = fdt64_to_cpu(*((unsigned long long *)pval + 1));
			}
		}
	}

	nlm_print("FMN Default queuesize %d credtis %d\n", qsize, credits);
	nlm_print("FMN Spill mem addr %lx mem size %lx\n", 
					(long)fmn_spill_mem_addr, (long)fmn_spill_mem_size);

	/* credits from this source station(s_stn) to the destination station(d_stn) */
	for(s_stn = 0; s_stn < XLP_MSG_BLK_MAX; s_stn++) {
		fmn_qsize_credit_cfg[s_stn].q_size = qsize;
		for(d_stn = 0; d_stn < XLP_MSG_BLK_MAX; d_stn++)
			fmn_qsize_credit_cfg[s_stn].credits[d_stn] = credits;
	}

	if(!fdt)
		return;
	
	for(s_stn = 0; s_stn < XLP_MSG_BLK_MAX; s_stn++) {
		len = sprintf(&path[0], "%s", "/fmn-config/");
		sprintf(&path[len], "%s", fmn_qsize_credit_cfg[s_stn].q_name);
		nodeoffset = fdt_path_offset(fdt, path);
		if(nodeoffset < 0) 
			continue;

		/* get queue size for this station */
		pval = fdt_getprop(fdt, nodeoffset, "queue-size", &plen);
		if(pval != NULL) {
			qsize = fdt32_to_cpu(*(unsigned int *)pval);
			fmn_qsize_credit_cfg[s_stn].q_size = qsize;
		}

		/* get credits from this station to other stations */
		for(d_stn = 0; d_stn < XLP_MSG_BLK_MAX; d_stn++) {
			pval = fdt_getprop(fdt, nodeoffset, fmn_qsize_credit_cfg[d_stn].q_name, &plen);
			if (pval != NULL) {
				credits = fdt32_to_cpu(*(unsigned int *)pval);
				fmn_qsize_credit_cfg[s_stn].credits[d_stn] = credits;
				/*nlm_print(" dst stn name %s credits %d\n", 
						fmn_qsize_credit_cfg[d_stn].q_name, credits);*/
			}
		}
	}
	
#ifdef FMN_DEBUG
	/* dump the table */
	for(s_stn = 0; s_stn < XLP_MSG_BLK_MAX; s_stn++) {
		len = 0;
		nlm_print("name %s bstid %d estid %d ntxstns %d qsize %d\n",
				fmn_qsize_credit_cfg[s_stn].q_name, fmn_qsize_credit_cfg[s_stn].b_stid,
				fmn_qsize_credit_cfg[s_stn].e_stid, fmn_qsize_credit_cfg[s_stn].n_txstns,
				fmn_qsize_credit_cfg[s_stn].q_size);
		nlm_print("  credits ");
		for(d_stn = 0; d_stn < XLP_MSG_BLK_MAX; d_stn++)
			nlm_print("<%s:%d> ",   fmn_qsize_credit_cfg[d_stn].q_name,
					 fmn_qsize_credit_cfg[s_stn].credits[d_stn]);
		nlm_print("\n");
	}
#endif
}
#endif

static void fmn_update_credit(int node, int b_stid, int dst_node)
{
	unsigned int *credits = NULL;
	int s_stn, d_stn, sid;
	struct fmn_qsize_credit_config *fmn_q_config = nlm_node_cfg.fmn_cfg[node]->fmn_q_config;

	/* configure with the default */
	for(sid = 0; sid <= XLP_NET_VC_LIMIT; sid++)
		fmn_cfg_value[sid] = nlm_node_cfg.fmn_cfg[node]->fmn_default_credits;

	/* Get credit config from the given source station to different destination station */
	for(s_stn = 0; s_stn < XLP_MSG_BLK_MAX; s_stn++) {
		if(!fmn_q_config[s_stn].valid)
			continue;
		if(b_stid >=  fmn_q_config[s_stn].b_stid && 
				b_stid <= fmn_q_config[s_stn].e_stid) {
			credits = fmn_q_config[s_stn].credits[dst_node];
			break;
		}
	}
	if(credits == NULL) {
		nlm_print("ERROR in Credit config: Station id not found, configuring default credit\n");
		return;
	}

	for(d_stn = 0; d_stn < XLP_MSG_BLK_MAX; d_stn++) {
		/* nlm_print("update credit s_stn %d d_stn %d credits %d\n",s_stn, d_stn, credits[d_stn]); */
		if(!fmn_q_config[d_stn].valid)
			continue;
		for(sid = fmn_q_config[d_stn].b_stid; sid <= fmn_q_config[d_stn].e_stid; sid++)
			fmn_cfg_value[sid] = credits[d_stn];
	}

	return;
}

static int fmn_update_qsize(int node)
{
	int sid, s_stn;
	unsigned long long qsize;
	struct fmn_qsize_credit_config *fmn_q_config = nlm_node_cfg.fmn_cfg[node]->fmn_q_config;

	
	/* qsize cannot be more than 256KB and it should be aligned to 4K */
	if((fmn_default_qsize % FMN_Q_PAGE_SIZE) != 0)
		fmn_default_qsize = (fmn_default_qsize + FMN_Q_PAGE_SIZE - 1) & (~(FMN_Q_PAGE_SIZE - 1));

	if(fmn_default_qsize > FMN_MAX_Q_SIZE) {
		nlm_print("ERROR: Default FMN Q size exceeds the limit\n");
		return -1;
	}
	
	for(sid = 0; sid <= XLP_NET_VC_LIMIT; sid++)
		fmn_cfg_value[sid] = fmn_default_qsize;

	for(s_stn = 0; s_stn < XLP_MSG_BLK_MAX; s_stn++) {
		if(!fmn_q_config[s_stn].valid)
			continue;
		for(sid = fmn_q_config[s_stn].b_stid; sid <= fmn_q_config[s_stn].e_stid; sid++) {
			qsize = fmn_q_config[s_stn].q_size;
			if(qsize > FMN_MAX_Q_SIZE) {
				nlm_print("ERROR: FMN Q size for stn %d exceeds the limit\n", s_stn);
				return -1;
			}
			if((qsize % FMN_Q_PAGE_SIZE) != 0)
				qsize = (qsize + FMN_Q_PAGE_SIZE - 1) & (~(FMN_Q_PAGE_SIZE - 1));
			fmn_cfg_value[sid] = qsize;
		}
	}
	return 0;
}


static void fmn_validate_credit(int node, int max_nodes)
{
	unsigned int credits, qsize;
	unsigned int s_stn, d_stn, src_node;
	struct fmn_qsize_credit_config *fmn_q_config = nlm_node_cfg.fmn_cfg[node]->fmn_q_config;

	
	/* credits from all the source stations to this station */
	for(d_stn = 0; d_stn < XLP_MSG_BLK_MAX; d_stn++) {
		if(!fmn_q_config[d_stn].valid)
			continue;
		qsize = fmn_q_config[d_stn].q_size;
		credits = 0;
	        for(src_node = 0; src_node < max_nodes; src_node++) {
  		    for(s_stn = 0; s_stn < XLP_MSG_BLK_MAX; s_stn++)  {
		 	if(!fmn_q_config[s_stn].valid)
				continue;
			credits += (fmn_q_config[s_stn].credits[src_node][d_stn] * fmn_q_config[s_stn].n_txstns);
                    }
		}

#ifdef FMN_DEBUG
		nlm_print("Credits check dst stn %s, reqd %d cfgrd %d\n", 
					fmn_q_config[d_stn].q_name, (credits * 12), qsize);
#endif

		/* considering single entry message only */
		if((credits * 12) >= qsize) 
			nlm_print("WARN ... Credits overflow.. dst stn %s, reqd %d cfgd %d\n", 
					fmn_q_config[d_stn].q_name, (credits * 12), qsize);
	}
}

/* 1024-bit bitmask as 16 64-bit longs.
 * '1' => station @ that bit position
 * is disabled.
 */
unsigned long long stids[16];

/*********************************************************************
 * nlm_hal_enable_vc_intr
 *
 * In xlp, there are 4 VC per cpu. Each vc can be configured to generate
 * an interrupt when message receive event happens.
 ********************************************************************/
void nlm_hal_enable_vc_intr(int node, int vc)
{
	uint64_t val = 0;
	val = nlm_hal_read_outq_config(node, vc);
	val &= ~((0x7ULL<<56) | (0x3ULL<<54) | (0x7ULL<<51) | (0x3ULL<<49));
	val |=  (0ULL<<56)|(0x2ULL<<54)|(0x0ULL<<51)|(0x1ULL<<49);
	nlm_hal_write_outq_config(node, vc, val);
}

/*********************************************************************
 * nlm_hal_disable_vc_intr
*********************************************************************/
void nlm_hal_disable_vc_intr(int node, int vc)
{
	uint64_t val = 0;
	val = nlm_hal_read_outq_config(node, vc);
	val = val & ~((0x3ULL<<54) | (0x3ULL<<49));
	nlm_hal_write_outq_config(node, vc, val);
}

/*********************************************************************
 * nlm_hal_set_fmn_interrupt
 *
 * setup cp2 msgconfig register for fmn interrup vector as 6
 *
 ********************************************************************/
void nlm_hal_set_fmn_interrupt(int irq)
{
	uint32_t val;
	/* Need write interrupt vector to cp2 msgconfig register */

	val =  _read_32bit_cp2_register(XLP_MSG_CONFIG_REG);
	val &= ~(0x1f << 16);
	val |= (irq << 16);
	_write_32bit_cp2_register(XLP_MSG_CONFIG_REG, val);

}

static __inline__ void nlm_hal_write_credit(int node, uint64_t src, uint64_t dst, uint64_t credits)
{
	uint64_t regaddr = nlh_qid_to_virt_addr(node, XLP_CREDIT_CONFIG_REG, 0); 
	uint64_t value = (((src) & 0x3ff) | (((dst) & 0xfff) << 12) | (((credits) & 0xffff) << 24));

	nlh_write_cfg_reg64(regaddr, value);
}

/* Clear a particular bit within the
 * 1024 bitmaks array stids.
 * Note: not a generic routine.
 */
static void clearbit(int bitnum) {

	int index = bitnum / 64;
	stids[index] &= ~(1ULL << (bitnum % 64));
}

void enable_interface(int interface, short value) {

	switch (interface) {
		case CPU: {
				int i;
				for (i=0; i<8; i++) {
					if ((value  & (1 << i)) == 0)
						clearbit(i << 4);
				}
			} break;
		case PCIE: {
				int i;
				for (i=0; i<4; i++) {
					if ((value  & (1 << i)) == 0)
						clearbit(XLP_STNID_PCIE0 + (i << 1));		
				}
			} break;
		case GDX: {
				if (value == 0)
					clearbit(XLP_STNID_GDX);		
			} break;
		case CMP: {
				int i;
#ifndef NLM_XLP_3XX
				for (i=0; i<4; i++) {
					if ((value  & (1 << i)) == 0)
						clearbit(XLP_STNID_CMP + i);		
				}
#endif
			} break;
		case CRYPTO: {
				int i;
				for (i=0; i<12; i++) {
					if ((value  & (1 << i)) == 0)
						clearbit(XLP_STNID_CRYPTO + i);		
				}
			} break;
		case POE: {
				if (value == 0)
					clearbit(XLP_STNID_POE);		
			} break;
		case NAE: {
				if (value == 0)
					clearbit(XLP_STNID_NAE_TX);		
			} break;
		case RSA: {
				int i;
				for (i=0; i<9; i++) {
					if ((value  & (1 << i)) == 0)
						clearbit(XLP_STNID_RSA_ECC + i);		
				}
			} break;
		default:
			nlm_print("Error! Configuring Credits for Unknown Interface.\n");
			break;
	}
}
	
/* Based on the (un)set bits from the EFUSE CFG Regs,
 * create a bitmask representing 0-1023 station IDs
 * which can then be used to check whether to send
 * credits to or not. This is a one-time operation.
 */
void stids_toskip(int node) {

	int i;

        if(is_nlm_xlp2xx()) {
		nlm_print("xlp2xx: please update %s\n", __FUNCTION__);
		return;
	}

	for (i=0; i<16; i++)
		stids[i] = ~0ULL;	/* init to all disabled */
	
	enable_interface(CPU,     nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG0) & 0xff);
	enable_interface(PCIE,   (nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG1) >> 3) & 0xf);
	enable_interface(GDX,    (nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG1) >> 8) & 0x1);
	enable_interface(CMP,    (nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG1) >> 9) & 0xf);
	enable_interface(CRYPTO, (nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG1) >> 14) & 0xfff);
	enable_interface(POE,    (nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG1) >> 27) & 0x1);
	enable_interface(NAE,    (nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG1) >> 28) & 0x1);
	enable_interface(RSA,    (nlm_hal_read_sys_reg(node, EFUSE_DEVICE_CFG2)) & 0x1ff);
}

static void nlm_hal_write_fmn_credit(int node, int max_nodes) 
{
	int src, qid, hndl = 0, dst_node;
	nlm_fmn_config_t *fmn_config = NULL;
	volatile int index;
	uint32_t credits, cfgrd;

        if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
		nlm_print(" XLP3XX/XLP2XX FMN configuration \n");
		if(is_nlm_xlp316()){
			nlm_print(" XLP316 FMN configuration \n");
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_CPU, 4, XLP_CPU0_VC_BASE, XLP_CPU3_VC_LIMIT);
		}
		else if(is_nlm_xlp312()){
			nlm_print(" XLP312 FMN configuration \n");
			fmn_config = &xlp3xx_fmn_config[0];
			fmn_config += XLP_MSG_HANDLE_CPU3;
			for(hndl = 0; hndl<1; hndl++){
				fmn_config->base_vc = XLP_3XX_INVALID_STATION,
				fmn_config->vc_limit =0;
				fmn_config++;
			}
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_CPU, 3, XLP_CPU0_VC_BASE, XLP_CPU2_VC_LIMIT);
		}
		else if(is_nlm_xlp308() || is_nlm_xlp208()){
			nlm_print(" XLP308/208 FMN configuration \n");
			fmn_config = (is_nlm_xlp308())? &xlp3xx_fmn_config[0] : &xlp2xx_fmn_config[0];
			fmn_config += XLP_MSG_HANDLE_CPU2;
			for(hndl = 0; hndl<2; hndl++){
				fmn_config->base_vc = XLP_3XX_INVALID_STATION,
				fmn_config->vc_limit =0;
				fmn_config++;
			}
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_CPU, 2, XLP_CPU0_VC_BASE, XLP_CPU1_VC_LIMIT);

		}
		else if(is_nlm_xlp304() || is_nlm_xlp204()){
			nlm_print(" XLP304/204 FMN configuration \n");
			fmn_config = (is_nlm_xlp304())? &xlp3xx_fmn_config[0] : &xlp2xx_fmn_config[0];
			fmn_config += XLP_MSG_HANDLE_CPU1;
			for(hndl = 0; hndl<3; hndl++){
				fmn_config->base_vc = XLP_3XX_INVALID_STATION,
				fmn_config->vc_limit =0;
				fmn_config++;
			}

			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_CPU, 1, XLP_CPU0_VC_BASE, XLP_CPU0_VC_LIMIT);
		}
		else {
		}

		if (is_nlm_xlp3xx()) {
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_RSA_ECC, 1, XLP_3XX_RSA_ECC_VC_BASE, XLP_3XX_RSA_ECC_VC_LIMIT);
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_CRYPTO, 1, XLP_3XX_CRYPTO_VC_BASE, XLP_3XX_CRYPTO_VC_LIMIT);
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_NAE, 1, XLP_3XX_NET_VC_BASE, XLP_3XX_NET_VC_LIMIT);
		}
		else if (is_nlm_xlp2xx()) {
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_RSA_ECC, 1, XLP_2XX_RSA_ECC_VC_BASE, XLP_2XX_RSA_ECC_VC_LIMIT);
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_CRYPTO, 1, XLP_2XX_CRYPTO_VC_BASE, XLP_2XX_CRYPTO_VC_LIMIT);
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_NAE, 1, XLP_2XX_NET_VC_BASE, XLP_2XX_NET_VC_LIMIT);
		}
			
		if (is_nlm_xlp3xx_ax() == 0)
			fmn_modify_qsize_credit_config(node, XLP_MSG_BLK_SRIO, 1, XLP_3XX_SRIO_VC_BASE, XLP_3XX_B0_SRIO_VC_LIMIT);

		fmn_config = (is_nlm_xlp3xx())? &xlp3xx_fmn_config[0] : &xlp2xx_fmn_config[0];
                for(hndl = 0; hndl < XLP_MSG_HANDLE_MAX; hndl++) {
                        if (fmn_config->base_vc != XLP_INVALID_STATION) {
				fmn_update_credit(node, fmn_config->base_vc, node);		
				credits = 0;
                                for (qid = 0; qid <= XLP_3XX_NET_VC_LIMIT; qid++) {
#ifdef FMN_DEBUG
					if(credits != fmn_cfg_value[qid]) {
						nlm_print("base %d qid %d credits %d\n", 
							fmn_config->base_vc, qid, fmn_cfg_value[qid]);
					}
#endif
					cfgrd = credits = fmn_cfg_value[qid];
					if ((fmn_config->base_vc >= XLP_STNID_CPU0) && (fmn_config->base_vc <= XLP_STNID_CPU7))
						cfgrd = credits - 1;

	                                nlm_hal_write_credit(node, fmn_config->base_vc, qid, cfgrd);	
                                }
                        }
                        fmn_config++;
                }
        }
	else {
	/* this populates the global array 'stids' 
	 * with a '1' representing a disabled station ID.
	 */
		stids_toskip(node);

		for (src = 0; src <= XLP_STNID_NAE_TX; src++) {
			/* check if bitposition src == 1 in 
			 * the dst_skip_bitmask. if so, continue
			 */
			index = src / 64;

			if ((stids[index] >> (src % 64)) & 0x1) {
				continue;
			}

			/* only enabled stations will reach here
			 */

			for(dst_node = 0 ;dst_node < max_nodes; dst_node++) {
				fmn_update_credit(node, src, dst_node);
				credits = 0;
				for (qid = 0; qid < 1024; qid++) {
#ifdef FMN_DEBUG
					if(credits != fmn_cfg_value[qid]) {
						nlm_print("src%d@%d node %d qid %d credits %d\n", 
							src, node, dst_node, qid, fmn_cfg_value[qid]);
					}
#endif
					cfgrd = credits = fmn_cfg_value[qid];
					if ((src >= XLP_STNID_CPU0) && (src <= XLP_STNID_CPU7))
						cfgrd = credits - 1;

					nlm_hal_write_credit(node, src, ((dst_node << 10) | qid), cfgrd);	
				}
			}
		}
	}

	/* Validae the credit config */
	fmn_validate_credit(node, max_nodes);
}
/*********************************************************************
 * nlm_hal_fmn_init
 *
 * setup 1024 outq, set credit from cpu to io,  io to io, and io to
 * cpu
 ********************************************************************/
#if defined(NLM_HAL_UBOOT) || defined(NLM_HAL_NETLBOOT) || defined(NLM_HAL_NETOS)

#define OUT_Q_INIT	0

#else

#define OUT_Q_INIT	((0x2ULL<<54) | (0x1ULL<<49))

#endif
/*********************************************************************
 * nlm_hal_setup_outq
 *
 * In xlp, there is 1024 receive message queues for fmn network. The
 * queue, allocated to cpu and high speed IO device, identified by
 * their vc number. When A send B a FMN message, receive VC is dest
 * number A need addressing. This function is to config each queue with
 * initial defaule value
 *
 * Total spill size for each Q is 16KB
 * This allows for 1024 q entries with 16B of entry size
 * This assumes credits across all sending agents to this queue is < 1024
 ********************************************************************/
int nlm_hal_setup_outq(int node, int max_nodes)
{
	uint32_t qid, max_qs = 0;
	uint64_t val;

	uint64_t spill_base = nlm_node_cfg.fmn_cfg[node]->fmn_spill_base; /* fmn_spill_mem_addr; */
	uint32_t spill_size = nlm_node_cfg.fmn_cfg[node]->fmn_spill_size;
	uint64_t q_spill_start_page = 0, q_spill_pages;

	const uint32_t ram_base = 0;
	uint32_t q_ram_base = 0;
	uint32_t q_ram_start_page = 0;
	const int q_ram_pages = 1;
	const uint32_t q_ram_page_entries = 32; /* entries, not bytes */
	int cnt =0;

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
		max_qs = XLP_3XX_NET_VC_LIMIT;
	}
	else {
		max_qs = XLP_NET_VC_LIMIT;
	}

        nlm_node_cfg.fmn_cfg[node]->spill_base_cur = spill_base;
        nlm_node_cfg.fmn_cfg[node]->q_ram_base = nlm_node_cfg.fmn_cfg[node]->q_ram_base_cur = q_ram_base;
        nlm_node_cfg.fmn_cfg[node]->q_ram_page_perq = 1;

	if(fmn_update_qsize(node) < 0)
		return -1;

	for( qid = 0; qid <= max_qs; qid++ )
	{
		/* Enable all output queues and spill on all queues.
		   Disable spill for u-boot as the spill memory will be enabled by the os 
		   loading time with the specified address */
#if defined(NLM_HAL_UBOOT) || defined(NLM_HAL_NETLBOOT)
		val = OUTQ_EN;
#else
		val = OUTQ_EN;
		if (nlm_node_cfg.fmn_cfg[node]->fmn_spill_base != 0ULL) 
			val |= SPILL_EN;
#endif

		/* Enable interrupts for cpu Queues */
		if ( (qid >= 0) && (qid < 128))
			val |= OUT_Q_INIT ;

			/* val |= INT_EN|(0ULL<<56)|(0x2ULL<<54)|(0x0ULL<<51)|(0x1ULL<<49); */

		/***************************************************************
		 * Configuration of on-chip RAM area
		 **************************************************************
		 */
		/* As 2xx has only 8k onchip memory skip all the invalid stations */
		if(is_nlm_xlp2xx()) {
			if(!station_exist_in_2xx(qid))
				continue;
			q_ram_base = ram_base + (cnt * q_ram_pages * q_ram_page_entries);
			cnt++;
		} else 
			q_ram_base = ram_base + (qid * q_ram_pages * q_ram_page_entries);

		val |= ( ((q_ram_base >> 10) & 0x1f) << 10); /* [14:10] of q_ram_base */

		q_ram_start_page = (q_ram_base >> 5) & 0x1f; /* [9:5] of q_ram_base */
		val |= (q_ram_start_page << 0);
		val |= ( (q_ram_start_page + q_ram_pages - 1) << 5) ;

		if (nlm_node_cfg.fmn_cfg[node]->fmn_spill_base != 0ULL) {

			/***************************************************************
			 * Configuration of spill area
			 **************************************************************
			 */
			/* pages in 4K units */
			q_spill_pages = fmn_cfg_value[qid] / (FMN_Q_PAGE_SIZE);
		
			/* if spill_start + qsize crosses 256MB boundary, configuration will be wrong as 
			 only 17-12 bits only considered for spill last */
			if(((spill_base & (FMN_MAX_Q_SIZE - 1)) +  fmn_cfg_value[qid]) > FMN_MAX_Q_SIZE)
				spill_base = (spill_base + FMN_MAX_Q_SIZE - 1) & (~(FMN_MAX_Q_SIZE - 1));
		
			val |= ( ((spill_base >> 18) & 0x3fffff) << 27); /* [39:18] of q_spill_base */

			q_spill_start_page = (spill_base >> 12) & 0x3f; /* [17:12] of q_spill_base */
			val |= (q_spill_start_page << 15);
			val |= ( (q_spill_start_page + q_spill_pages - 1) << 21);

#ifdef FMN_DEBUG
		nlm_print("Fmn q config %d sqbase %lx sqsize %d sqpages %d\n", 
				qid, (long)spill_base, fmn_cfg_value[qid], (int)q_spill_pages);
#endif
		
			spill_base +=  fmn_cfg_value[qid];
		}
		
		/* Write to the configuration register */
		nlm_hal_write_outq_config(node, qid, val);
	}

        nlm_node_cfg.fmn_cfg[node]->spill_base_cur = spill_base;
        nlm_node_cfg.fmn_cfg[node]->q_ram_base_cur = q_ram_base;
 
        nlm_print("spill_base_cur 0x%llx qram_base_cur 0x%llx \n",(unsigned long long)spill_base,(unsigned long long) q_ram_base);

/*	if((spill_base - fmn_spill_mem_addr) > fmn_spill_mem_size) { */
	if ((spill_base - nlm_node_cfg.fmn_cfg[node]->fmn_spill_base) > spill_size) {
		nlm_print("ERROR:  FMN Q total size exceeds the limit\n");
		return -1;
	}
	return 0;
}

int parse_queue_config(void *fdt, int max_nodes)
{
        int node = 0, src_node, s_stn, d_stn;
        char fmn_cfg_str[80];
        char prop_str[10];
        int plen, nodeoffset;
        uint32_t *pval;
	struct fmn_qsize_credit_config *fmn_q_config;
	uint32_t credits, qsize;

        for(node = 0 ;  node < max_nodes; node++) {
                fmn_q_config = nlm_node_cfg.fmn_cfg[node]->fmn_q_config;
                for(d_stn=0; d_stn < XLP_MSG_BLK_MAX; d_stn++) {

                        sprintf(fmn_cfg_str, "/soc/fmn@node-%d/q-config/%s", node, fmn_q_config[d_stn].q_name);
                        nodeoffset = fdt_path_offset(fdt, fmn_cfg_str);
                        if(nodeoffset < 0)
                                continue;
	                /* get queue size for this station */
        	        pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "queue-size", &plen);
                	if(pval != NULL) {
                        	qsize = fdt32_to_cpu(*(unsigned int *)pval);
	                        fmn_q_config[d_stn].q_size = qsize;
        	        }

                        sprintf(fmn_cfg_str, "/soc/fmn@node-%d/q-config/%s/credits-from", node, fmn_q_config[d_stn].q_name);
                        nodeoffset = fdt_path_offset(fdt, fmn_cfg_str);
                        if(nodeoffset < 0)
                                continue;

                	/* get credits from this station to other stations */

                        for(src_node = 0; src_node < max_nodes; src_node++) {
                                sprintf(prop_str, "node-%d", src_node);
                                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, prop_str, &plen);
                                if(pval != NULL) {
                                        if ((plen / sizeof(uint32_t)) != XLP_MSG_BLK_MAX) {
                                                nlm_print("Invalid credit configuration in fdt \n");
                                                while(1); 
                                        }
                                        for(s_stn = 0; s_stn < XLP_MSG_BLK_MAX; s_stn++) {
                                                credits = fdt32_to_cpu(*(unsigned int *)(pval + s_stn));
						if (credits != 0)
							nlm_node_cfg.fmn_cfg[src_node]->fmn_q_config[s_stn].credits[node][d_stn] = credits;
#ifdef FMN_DEBUG
                                                nlm_print("node %d station %s has %d credits to %d:Q %s\n", src_node, nlm_node_cfg.fmn_cfg[src_node]->fmn_q_config[s_stn].q_name, nlm_node_cfg.fmn_cfg[src_node]->fmn_q_config[s_stn].credits[node][d_stn], node, fmn_q_config[d_stn].q_name);
#endif
                                        }

                                }
                        }
                }
        }
	return 0;
}


int parse_fdt_fmn_config(void *fdt)
{
	char fmn_cfg_str[50];
	int nodeoffset = 0, plen, max_nodes = 1, node = 0;
  	uint32_t *pval;
	struct fmn_cfg *fmn_config;
#if !defined(NLM_HAL_UBOOT) && !defined(NLM_HAL_NETLBOOT)
	uint64_t spill_base = 0ULL;
#endif
	uint32_t qsize, credits, src_node, s_stn, d_stn;

	sprintf(fmn_cfg_str,"/soc/nodes");
	nodeoffset = fdt_path_offset(fdt, fmn_cfg_str);
        if(nodeoffset >= 0) {
		pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "num-nodes", &plen);
		if(pval != NULL) {
			max_nodes = fdt32_to_cpu(*(unsigned int *)pval);
		}
	}

	nlm_print("Number of nodes %d \n",max_nodes);

	nlm_node_cfg.num_nodes = max_nodes;
	
	for(node = 0; node < max_nodes; node++) {
		nlm_node_cfg.fmn_cfg[node] = nlm_malloc(sizeof(struct fmn_cfg));
		if (nlm_node_cfg.fmn_cfg[node] == NULL) {
			nlm_print("nlm_malloc failed for node %d\n", node);
			return -1;
		}
		memset(nlm_node_cfg.fmn_cfg[node], 0, sizeof(struct fmn_cfg));
		nlm_node_cfg.fmn_cfg[node]->fmn_default_credits = fmn_default_credits/max_nodes;
		nlm_node_cfg.fmn_cfg[node]->fmn_default_qsize = fmn_default_qsize;
		if (node == 0) {
			nlm_node_cfg.fmn_cfg[node]->fmn_spill_base = fmn_spill_mem_addr;
			nlm_node_cfg.fmn_cfg[node]->fmn_spill_size = fmn_spill_mem_size;
		}
		else {
			nlm_node_cfg.fmn_cfg[node]->fmn_spill_base = 0ULL;
			nlm_node_cfg.fmn_cfg[node]->fmn_spill_size = 0ULL;
		}
		
		sprintf(fmn_cfg_str,"/soc/fmn@node-%d",node);
                nodeoffset = fdt_path_offset(fdt, fmn_cfg_str);
                if(nodeoffset < 0) {
                        nlm_print("No 'fmn@node-%d' param in dtb \n",node);
			nlm_print("node %d default qsize %d credits %d\n",node, nlm_node_cfg.fmn_cfg[node]->fmn_default_qsize, nlm_node_cfg.fmn_cfg[node]->fmn_default_credits);
	                memcpy(nlm_node_cfg.fmn_cfg[node]->fmn_q_config, fmn_qsize_credit_cfg, sizeof(fmn_qsize_credit_cfg));
                        continue;
                }

#if !defined(NLM_HAL_UBOOT) && !defined(NLM_HAL_NETLBOOT)
		pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "fmn-spill-mem-size", &plen);
                if(pval != NULL) {
                        nlm_node_cfg.fmn_cfg[node]->fmn_spill_size = fdt64_to_cpu(*(unsigned long long *)pval) ;
		}

		pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "fmn-spill-mem-base", &plen);
		if(pval != NULL) {
			spill_base = fdt64_to_cpu(*(unsigned long long *)pval);
			if (spill_base != 0ULL) {
				nlm_node_cfg.fmn_cfg[node]->fmn_spill_base = spill_base;
			}
		}
#ifdef NLM_HAL_LINUX_KERNEL
		if ((pval == NULL) || (spill_base == 0ULL)){
			spill_base = nlm_spill_alloc(node, (nlm_node_cfg.fmn_cfg[node]->fmn_spill_size));
			if (spill_base == 0ULL) {
				nlm_print("Node %d FMN spill_mem alloc failed \n", node);
				nlm_node_cfg.fmn_cfg[node]->fmn_spill_size = 0;
			}
			else
				nlm_node_cfg.fmn_cfg[node]->fmn_spill_base = spill_base;
		}
#endif
#endif
		nlm_print("spillsize 0x%llx @ 0x%016llx \n", (unsigned long long)nlm_node_cfg.fmn_cfg[node]->fmn_spill_size,(unsigned long long)nlm_node_cfg.fmn_cfg[node]->fmn_spill_base);


                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "default-queue-size", &plen);
                if (pval != NULL) {
                        qsize = fdt32_to_cpu(*(unsigned int *)pval);
                        nlm_node_cfg.fmn_cfg[node]->fmn_default_qsize = qsize;
                }

                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "default-credits", &plen);
                if (pval != NULL) {
                        credits = fdt32_to_cpu(*(unsigned int *)pval);
                        nlm_node_cfg.fmn_cfg[node]->fmn_default_credits = credits / max_nodes;
                }

		nlm_print("node %d default qsize %d credits %d\n",node, nlm_node_cfg.fmn_cfg[node]->fmn_default_qsize, nlm_node_cfg.fmn_cfg[node]->fmn_default_credits);
                memcpy(nlm_node_cfg.fmn_cfg[node]->fmn_q_config, fmn_qsize_credit_cfg, sizeof(fmn_qsize_credit_cfg));
	}

        for(node=0; node < max_nodes; node++) {
                fmn_config = nlm_node_cfg.fmn_cfg[node];
                for(d_stn = 0; d_stn < XLP_MSG_BLK_MAX; d_stn++) {
                        fmn_config->fmn_q_config[d_stn].q_size = fmn_config->fmn_default_qsize;
                        for(src_node=0; src_node < max_nodes; src_node++) {
                                for(s_stn=0; s_stn < XLP_MSG_BLK_MAX; s_stn++) {
                                        fmn_config->fmn_q_config[d_stn].credits[src_node][s_stn] = fmn_config->fmn_default_credits;
#ifdef FMN_DEBUG
                                        nlm_print("%d:%s Q, credits given to node %d stn %s : %d\n", node, fmn_config->fmn_q_config[d_stn].q_name, src_node, fmn_config->fmn_q_config[s_stn].q_name, fmn_config->fmn_q_config[d_stn].credits[src_node][s_stn]);
#endif
                                }
                        }
                }
        }

	parse_queue_config(fdt, max_nodes);

	return max_nodes;
}

int get_dom_fmn_node_ownership(void *fdt, int dom_id)
{
	uint32_t owner_mask;

	owner_mask = get_dom_owner_mask(fdt, dom_id, "fmn");

	return owner_mask;
}

/**
* @brief nlm_hal_fmn_init function Initializes FMN (outpu Queues and Credit registers)
*
* @param [in]  spill_base    :Physical address where the spill base starts
* @param [in]  size          :Size of the spill/fill region in bytes
* @param [in]  credits       :Number of credits from any src to any dst. 
*
* @return
*  - Returns no value.
*
* @note
*    1. This funysconfig/dts/fmn-temp.dtstion must be the first to be called before any FMN HAL API's.
* @n
*    2. This function is typically called twice (once from U-boot and once from OS). This is due to
*       Netlogic's SDK convention that only 1 cpu is running while in U-boot and potentially more than 
*       one cpu running while in OS and also the requirement that Credit configuration can only
*       happen after the cpu is out of reset.
* @n
*    3. The credits between any source and any destination is chosen to be same for simplification.
* 
* @ingroup hal_fmn
*
*/
void nlm_hal_fmn_init(void *fdt, int node)
{
	int max_nodes;

	nlm_print("*** Firmware Configuration of FMN ***\n");

	max_nodes = parse_fdt_fmn_config(fdt);

	update_fmn_config();

	if (max_nodes < 0) {
		nlm_print("FMN Configuration failed\n");
		while(1);
	}

	if(!is_nlm_xlp2xx()) {
		nlm_hal_soc_clock_enable(node, DFS_DEVICE_RSA);
	}

	/* fmn_qsize_credit_cfg_extract(fdt); */
	/* verify out_q config 
	 */
	if(nlm_hal_setup_outq(node, max_nodes) < 0)
		while(1);
	nlm_hal_write_fmn_credit(node, max_nodes);
}

#ifdef NLM_HAL_LINUX_KERNEL 
#include <linux/types.h>
#include <linux/module.h>
EXPORT_SYMBOL(nlm_hal_disable_vc_intr);
EXPORT_SYMBOL(nlm_hal_enable_vc_intr);
EXPORT_SYMBOL(nlm_hal_fmn_init);
#endif
