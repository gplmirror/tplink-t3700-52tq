/*-
 * Copyright 2003-2012 Broadcom Corporation
 *
 * This is a derived work from software originally provided by the entity or
 * entities identified below. The licensing terms, warranty terms and other
 * terms specified in the header of the original work apply to this derived work
 *
 * #BRCM_1# */

/* Based on the code from Cortina */

#ifdef NLM_HAL_LINUX_KERNEL
#include <linux/netdevice.h>
#include <asm/netlogic/hal/nlm_evp_cpld.h>
#else
#include "nlm_evp_cpld.h"
#endif

#define XGE_MAC_STATS_ACCESS    0x0728c
#define XGE_MAC_STATS_DATA0     0x0728f
#define XGE_MAC_STATS_DATA1     0x0728e
#define XGE_MAC_STATS_DATA2     0x0728d

#define NLM_MAX_INTERLAKEN	2
#define CORTINA_MAX_PORTS	4

#define DEFAULT_FRAME_SIZE	1500

uint32_t logical_port_map[NLM_MAX_INTERLAKEN][CORTINA_MAX_PORTS] = {
{0, 1, 2, 3},
{0, 1, 2, 3}
};

void dump_cortina_regs(int cs)
{
	volatile uint16_t data;
   data = nlm_hal_cpld_read_16(cs,   0x3123 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_CXAUI_LAKE_FCTL1 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3182 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("LAKE_MAXBURST is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3183 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("LAKE_MINBURST is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3184 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_BURST is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3185 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_SYNC_ERR is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3186 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3188 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT2 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x318a );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT2 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x318c );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT2 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x318e );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT2 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3190 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT2 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3192 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT2 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x3194 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_LAKE_WORD_CRCCNT2 is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x317e );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_TXLANE_CRC_ERR is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x317d );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_FC_ERR is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x317f );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_BAD_ALIGN is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x3180 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_BAD_CODE is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x3181 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_ALIGN_FAIL is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x3105 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_INTR is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x3106 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_INTRENA is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x3107 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_INTSTAT is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x5000 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_TXUP_TXFCSCTRL is %x\n",data);
   data = nlm_hal_cpld_read_16(cs,   0x5001 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_MINPKTSIZE is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x5072 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_FCRERRFRM_CTRL is %x\n",data);

   data = nlm_hal_cpld_read_16(cs,   0x5073 );//(HIF SLICE INTSTATUS[0])
   nlm_print ("MON@_TXUP_PORT00_HDRCHKSUMERRFRM_CTR is %x\n",data);
}

#ifdef NLM_HAL_LINUX_KERNEL
void cortina_dump_mac_stats(int cs, int port)
{
        uint16_t counter = 0;
        volatile uint64_t data;

        for(counter = 0; counter < 5 ; counter++) { // 0x28
		data= 0ULL;
                nlm_hal_cpld_write_16 (cs,  0x0080 | (counter << 8), XGE_MAC_STATS_ACCESS + (0x1000 * port));//_XGIGE[0]_XGE_MAC_STATS_ACCESS)
                data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * port)));//_XGIGE[0]_XGE_MAC_STATS_DATA0)
                data |= (le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * port))) << 16);//_XGIGE[0]_XGE_MAC_STATS_DATA1)
#ifdef CONFIG_64BIT
                data |= (unsigned long long )(le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * port))) << 32);//_XGIGE[0]_XGE_MAC_STATS_DATA2)
#endif
		nlm_print("counter%d: %lld \n", counter, data);
        }

}
#endif

void cortina_cs34x7_init(int cs, int num_lanes, int lane_rate)
{
#if ((!defined(XLP_SIM) || defined(NLM_BOARD)) && (defined(NLM_CORTINA_SUPPORT)))
	 extern void cortina_cs34x7_init_p1(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p2(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p3(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p4(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p5(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p6(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p7(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p8(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p9(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p10(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p11(int cs, int num_lanes, int lane_rate);
	 extern void cortina_cs34x7_init_p12(int cs, int num_lanes, int lane_rate);

//	 volatile uint16_t data = 0;

// data = nlm_hal_cpld_read_16(cs, 0x03122 );//(MON2_CXAUI[0]_LAKE_FCTL0)
// //printf( " Inside write_read_seq_625.h : data is %0xh  \n", data );
// // data = nlm_hal_cpld_read_16(cs, 0x0000c );//(MON2_MPIF_CPLL_CONTROL2)
// //printf( " Inside write_read_seq_625.h : data is %0xh  \n", data );
// // nlm_hal_cpld_write_16(cs,   0x7059     ,0x0000c );//(MON2_MPIF_CPLL_CONTROL2)

//        MON2 device(0) unregistered!
//	**** MON2(Monza2) Driver unloaded ****
//INFO: Write 0xf200     to   MONZA_IM_SPAC dev 0 : 0x0009a (UNNAMED)
//INFO: Write 0xf018     to   MONZA_IM_SPAC dev 0 : 0x0009a (UNNAMED)
//INFO: Write 0xf100     to   MONZA_IM_SPAC dev 0 : 0x0009a (UNNAMED)
//INFO: Write 0xf280     to   MONZA_IM_SPAC dev 0 : 0x0009a (UNNAMED)
//	**** MON2(Monza2) Driver loaded ****
//	MON2 Driver - Lab Release.

	 cortina_cs34x7_init_p1(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p2(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p3(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p4(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p5(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p6(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p7(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p8(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p9(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p10(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p11(cs,num_lanes,lane_rate);
	 cortina_cs34x7_init_p12(cs,num_lanes,lane_rate);
#endif
}

void cortina_enable_padcrc(int cs)
{
	//nlm_print("FCS ctrl %04x\n",nlm_hal_cpld_read_16 (cs, 0x5000));
	nlm_hal_cpld_write_16(cs, 0x0800, 0x5000);
}

#ifdef NLM_HAL_LINUX_KERNEL
int cortina_get_mac_stats(int node, int hwport, int port, void *devstats)
{
	struct net_device_stats *stats = (struct net_device_stats *)devstats;
        int phys_port = 0;
        int cs;
	unsigned long long data;

	if (hwport == XLP_ILK_PORT_0)
                cs = XLP_ILK_PORT0_CS;
        else if (hwport == XLP_ILK_PORT_1)
                cs = XLP_ILK_PORT1_CS;
        else
                return -1;

        if (port > (CORTINA_MAX_PORTS - 1))
                return -1;

	phys_port = logical_port_map[hwport/8][port];

	nlm_hal_cpld_write_16 (cs,  0x0080 , XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
	stats->tx_packets = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (1<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->tx_bytes = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0xb<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->tx_errors = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0x3<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
	stats->rx_packets = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0x4<<8) , XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
	stats->rx_bytes = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0x6<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->rx_crc_errors = data;

	nlm_hal_cpld_write_16 (cs,  0x0080 | (0xf<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->rx_errors = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0x7<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
	stats->rx_frame_errors = data;
        
        nlm_hal_cpld_write_16 (cs,  0x0080 | (0x14<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->rx_length_errors = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0xd<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
	data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->multicast = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0x27<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
        data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->rx_dropped = data;

        nlm_hal_cpld_write_16 (cs,  0x0080 | (0x28<<8), XGE_MAC_STATS_ACCESS + (0x1000 * phys_port));
        data = le16_to_cpu(nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA0 + (0x1000 * phys_port)));
        data |= le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA1 + (0x1000 * phys_port)) )) << 16;
#ifdef CONFIG_64BIT
        data |= (unsigned long long )le16_to_cpu((nlm_hal_cpld_read_16 (cs,  XGE_MAC_STATS_DATA2 + (0x1000 * phys_port)) )) << 32;
#endif
        stats->rx_dropped += data;

	return 0;
}
#endif

int cortina_set_max_framesize(int hwport, int port, uint16_t framesize)
{
	int phys_port = 0;
	int cs;
	
	if (hwport == XLP_ILK_PORT_0)
		cs = XLP_ILK_PORT0_CS;
	else if (hwport == XLP_ILK_PORT_1)
		cs = XLP_ILK_PORT1_CS;
	else
		return -1;

	if (port > (CORTINA_MAX_PORTS - 1))
		return -1;

	phys_port = logical_port_map[hwport/8][port];
	framesize = ((framesize & 0x00FF) << 8) | ((framesize & 0xFF00) >> 8);
	nlm_hal_cpld_write_16(cs, framesize , 0x727c + (0x1000 * phys_port));
	return 0;
}

int nlm_hal_init_cs34x7(int hwport,int num_lanes, int lane_rate)
{
#if !defined(XLP_SIM) || defined(NLM_BOARD)
	int physport = 0;
	nlm_print("nlm_hal_init_cs34x7 port %d lanes %d\n",hwport, num_lanes);

	if (!is_nlm_xlp8xx()) {
		nlm_print("Interlaken is not supported\n");
		return -1;
	}

	nlm_print("Evp%d cpld version %04x \n",nlm_xlp_boardver(),nlm_xlp_cpldver());

	if (nlm_xlp_cpldver() < 0x030A) {
		nlm_print("Update cpld version to > 3.10\n");
		return -1;
	}

	if ((num_lanes > 4) && (is_xlp_evp2())) {
		nlm_print("Maximum lanes supported on EVP2 is 4\n");
		return -1;
	}

	if (!is_ilk_card_onslot(hwport/4)) {
		nlm_print("No interlaken card on slot %d\n",hwport/4);
		return -1;
	}

	switch(hwport) {
		case XLP_ILK_PORT_0:
			cortina_cs34x7_init(XLP_ILK_PORT0_CS, (XLP_ILK_MAX_LANES - num_lanes), lane_rate);
			//dump_cortina_regs(XLP_ILK_PORT0_CS);
			cortina_enable_padcrc(XLP_ILK_PORT0_CS);
			break;
		case XLP_ILK_PORT_1:
			cortina_cs34x7_init(XLP_ILK_PORT1_CS, (XLP_ILK_MAX_LANES - num_lanes), lane_rate);
			//dump_cortina_regs(XLP_ILK_PORT1_CS);
			cortina_enable_padcrc(XLP_ILK_PORT1_CS);
			break;
		default:
			nlm_print("Interlaken port id is invalid\n");
			return -1;
	}

	for(physport = 0; physport < CORTINA_MAX_PORTS; physport++) {
		cortina_set_max_framesize(hwport, physport, DEFAULT_FRAME_SIZE);
	}
#endif
	return 0;
}

#ifdef NLM_HAL_LINUX_KERNEL
EXPORT_SYMBOL(cortina_get_mac_stats);
EXPORT_SYMBOL(cortina_set_max_framesize);
EXPORT_SYMBOL(nlm_hal_init_cs34x7);
EXPORT_SYMBOL(cortina_dump_mac_stats);
#endif
