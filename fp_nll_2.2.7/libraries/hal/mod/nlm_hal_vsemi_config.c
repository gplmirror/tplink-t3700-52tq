/*-
 * Copyright (c) 2003-2013 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */
#include "nlm_hal_vsemi_data.h"

unsigned char vsemi_mem_sgmii_4page [256];
unsigned char vsemi_mem_sgmii [256];

unsigned char vsemi_mem_xaui_4page [256];
unsigned char vsemi_mem_xaui [256];

unsigned char vsemi_mem_12G_4page [256];
unsigned char vsemi_mem_12G [256];
unsigned char vsemi_mem_16G_4page [256];
unsigned char vsemi_mem_16G [256];

void nlm_hal_config_vsemi_mem_16G_4page(void)
{

	vsemi_mem_16G_4page[101] = 0xB7;
	vsemi_mem_16G_4page[102] = 0xB7;
	vsemi_mem_16G_4page[103] = 0xC;
	vsemi_mem_16G_4page[104] = 0xC;
	vsemi_mem_16G_4page[105] = 0x1A;
	vsemi_mem_16G_4page[106] = 0x1A;
	vsemi_mem_16G_4page[107] = 0x2;
	vsemi_mem_16G_4page[108] = 0x2;
	vsemi_mem_16G_4page[109] = 0x11;
	vsemi_mem_16G_4page[110] = 0x0;
	
	return;
}

void nlm_hal_config_vsemi_mem_16G_4page_125(void)
{
	vsemi_mem_16G_4page[101] = 0xBF;
	vsemi_mem_16G_4page[102] = 0xBF;
	vsemi_mem_16G_4page[103] = 0xA;
	vsemi_mem_16G_4page[104] = 0xA;
	vsemi_mem_16G_4page[105] = 0xE;
	vsemi_mem_16G_4page[106] = 0xE;
	vsemi_mem_16G_4page[107] = 0x1;
	vsemi_mem_16G_4page[108] = 0x1;
	vsemi_mem_16G_4page[109] = 0x11;
	vsemi_mem_16G_4page[110] = 0x0;

	return;
}


void nlm_hal_config_vsemi_mem_16G(void)
{
	
	vsemi_mem_16G[101] = 0x99;
	vsemi_mem_16G[102] = 0x0;
	vsemi_mem_16G[103] = 0x76;
	vsemi_mem_16G[104] = 0xB7;
	vsemi_mem_16G[105] = 0xB7;
	vsemi_mem_16G[106] = 0xC;
	vsemi_mem_16G[107] = 0xC;
	vsemi_mem_16G[108] = 0x1A;
	vsemi_mem_16G[109] = 0x1A;
	vsemi_mem_16G[110] = 0x6;
	vsemi_mem_16G[111] = 0x16;
	vsemi_mem_16G[112] = 0x8;
	vsemi_mem_16G[113] = 0x0;
	vsemi_mem_16G[114] = 0x8;
	vsemi_mem_16G[115] = 0x0;
	vsemi_mem_16G[116] = 0xFF;
	vsemi_mem_16G[117] = 0xB3;
	vsemi_mem_16G[118] = 0xF6;
	vsemi_mem_16G[119] = 0xD0;
	vsemi_mem_16G[120] = 0xEF;
	vsemi_mem_16G[121] = 0xFB;
	vsemi_mem_16G[122] = 0xFF;
	vsemi_mem_16G[123] = 0xFF;
	vsemi_mem_16G[124] = 0xFF;
	vsemi_mem_16G[125] = 0xFF;
	vsemi_mem_16G[126] = 0xFF;
	vsemi_mem_16G[127] = 0xD3;
	vsemi_mem_16G[128] = 0xD3;
	vsemi_mem_16G[129] = 0xE2;
	vsemi_mem_16G[130] = 0xEF;
	vsemi_mem_16G[131] = 0xFB;
	vsemi_mem_16G[132] = 0xFB;
	vsemi_mem_16G[133] = 0xFF;
	vsemi_mem_16G[134] = 0xEF;
	vsemi_mem_16G[135] = 0xFF;
	vsemi_mem_16G[136] = 0xFF;
	vsemi_mem_16G[137] = 0xD3;
	vsemi_mem_16G[138] = 0xD3;
	vsemi_mem_16G[139] = 0xE2;
	vsemi_mem_16G[140] = 0xEF;
	vsemi_mem_16G[141] = 0xFB;
	vsemi_mem_16G[142] = 0xFB;
	vsemi_mem_16G[143] = 0xFF;
	vsemi_mem_16G[144] = 0xEF;
	vsemi_mem_16G[145] = 0xFF;
	vsemi_mem_16G[146] = 0xFF;
	vsemi_mem_16G[147] = 0xFB;
	vsemi_mem_16G[148] = 0xFF;
	vsemi_mem_16G[149] = 0x3F;
	vsemi_mem_16G[150] = 0x0;
	vsemi_mem_16G[151] = 0x64;
	vsemi_mem_16G[152] = 0x0;
	vsemi_mem_16G[153] = 0x4;
	vsemi_mem_16G[154] = 0x2;
	vsemi_mem_16G[155] = 0x5;
	vsemi_mem_16G[156] = 0x5;
	vsemi_mem_16G[157] = 0x4;
	vsemi_mem_16G[158] = 0x0;
	vsemi_mem_16G[159] = 0x0;
	vsemi_mem_16G[160] = 0x8;
	vsemi_mem_16G[161] = 0x4;
	vsemi_mem_16G[162] = 0x0;
	vsemi_mem_16G[163] = 0x0;
	vsemi_mem_16G[164] = 0x4;

	return;
}

void nlm_hal_config_vsemi_mem_16G_125(void)
{

	vsemi_mem_16G[101] = 0xAA;
	vsemi_mem_16G[102] = 0x0;
	vsemi_mem_16G[103] = 0x57;
	vsemi_mem_16G[104] = 0xBF;
	vsemi_mem_16G[105] = 0xBF;
	vsemi_mem_16G[106] = 0xA;
	vsemi_mem_16G[107] = 0xA;
	vsemi_mem_16G[108] = 0xE;
	vsemi_mem_16G[109] = 0xE;
	vsemi_mem_16G[110] = 0x4;
	vsemi_mem_16G[111] = 0x17;
	vsemi_mem_16G[112] = 0x10;
	vsemi_mem_16G[113] = 0x0;
	vsemi_mem_16G[114] = 0x10;
	vsemi_mem_16G[115] = 0x0;
	vsemi_mem_16G[116] = 0xFF;
	vsemi_mem_16G[117] = 0xC3;
	vsemi_mem_16G[118] = 0xF5;
	vsemi_mem_16G[119] = 0xD9;
	vsemi_mem_16G[120] = 0xF2;
	vsemi_mem_16G[121] = 0xFC;
	vsemi_mem_16G[122] = 0xFE;
	vsemi_mem_16G[123] = 0xFF;
	vsemi_mem_16G[124] = 0xFF;
	vsemi_mem_16G[125] = 0xFF;
	vsemi_mem_16G[126] = 0xFF;
	vsemi_mem_16G[127] = 0xDB;
	vsemi_mem_16G[128] = 0xE9;
	vsemi_mem_16G[129] = 0xC2;
	vsemi_mem_16G[130] = 0xF2;
	vsemi_mem_16G[131] = 0xFC;
	vsemi_mem_16G[132] = 0xFE;
	vsemi_mem_16G[133] = 0xF2;
	vsemi_mem_16G[134] = 0xF2;
	vsemi_mem_16G[135] = 0xFF;
	vsemi_mem_16G[136] = 0xFF;
	vsemi_mem_16G[137] = 0xDB;
	vsemi_mem_16G[138] = 0xE9;
	vsemi_mem_16G[139] = 0xC2;
	vsemi_mem_16G[140] = 0xF2;
	vsemi_mem_16G[141] = 0xFC;
	vsemi_mem_16G[142] = 0xFE;
	vsemi_mem_16G[143] = 0xF2;
	vsemi_mem_16G[144] = 0xF2;
	vsemi_mem_16G[145] = 0xFF;
	vsemi_mem_16G[146] = 0xFF;
	vsemi_mem_16G[147] = 0xFF;
	vsemi_mem_16G[148] = 0xF2;
	vsemi_mem_16G[149] = 0x3F;
	vsemi_mem_16G[150] = 0x0;
	vsemi_mem_16G[151] = 0x64;
	vsemi_mem_16G[152] = 0x0;
	vsemi_mem_16G[153] = 0x2;
	vsemi_mem_16G[154] = 0x1;
	vsemi_mem_16G[155] = 0x5;
	vsemi_mem_16G[156] = 0x5;
	vsemi_mem_16G[157] = 0x4;
	vsemi_mem_16G[158] = 0x0;
	vsemi_mem_16G[159] = 0x0;
	vsemi_mem_16G[160] = 0x8;
	vsemi_mem_16G[161] = 0x4;
	vsemi_mem_16G[162] = 0x0;
	vsemi_mem_16G[163] = 0x0;
	vsemi_mem_16G[164] = 0x4;

}

void nlm_hal_config_vsemi_mem_12G_4page(void)
{
	vsemi_mem_12G_4page[101] = 0xC3;
	vsemi_mem_12G_4page[102] = 0xC3;
	vsemi_mem_12G_4page[103] = 0x11;
	vsemi_mem_12G_4page[104] = 0x11;
	vsemi_mem_12G_4page[105] = 0x12;
	vsemi_mem_12G_4page[106] = 0x12;
	vsemi_mem_12G_4page[107] = 0x2;
	vsemi_mem_12G_4page[108] = 0x2;
	vsemi_mem_12G_4page[109] = 0x0;
	vsemi_mem_12G_4page[110] = 0x0;

	return;
}

void nlm_hal_config_vsemi_mem_12G_4page_125(void)
{
	vsemi_mem_12G_4page[101] = 0xBA;
	vsemi_mem_12G_4page[102] = 0xBA;
	vsemi_mem_12G_4page[103] = 0xD;
	vsemi_mem_12G_4page[104] = 0xD;
	vsemi_mem_12G_4page[105] = 0x18;
	vsemi_mem_12G_4page[106] = 0x18;
	vsemi_mem_12G_4page[107] = 0x2;
	vsemi_mem_12G_4page[108] = 0x2;
	vsemi_mem_12G_4page[109] = 0x0;
	vsemi_mem_12G_4page[110] = 0x0;
	
	return;
}

void nlm_hal_config_vsemi_mem_12G(void)
{
	vsemi_mem_12G[101] = 0x99;
	vsemi_mem_12G[102] = 0x0;
	vsemi_mem_12G[103] = 0x76;
	vsemi_mem_12G[104] = 0xC3;
	vsemi_mem_12G[105] = 0xC3;
	vsemi_mem_12G[106] = 0x11;
	vsemi_mem_12G[107] = 0x11;
	vsemi_mem_12G[108] = 0x12;
	vsemi_mem_12G[109] = 0x12;
	vsemi_mem_12G[110] = 0x6;
	vsemi_mem_12G[111] = 0x16;
	vsemi_mem_12G[112] = 0x8;
	vsemi_mem_12G[113] = 0x0;
	vsemi_mem_12G[114] = 0x8;
	vsemi_mem_12G[115] = 0x0;
	vsemi_mem_12G[116] = 0xFF;
	vsemi_mem_12G[117] = 0xB3;
	vsemi_mem_12G[118] = 0xF6;
	vsemi_mem_12G[119] = 0xD0;
	vsemi_mem_12G[120] = 0xEF;
	vsemi_mem_12G[121] = 0xFB;
	vsemi_mem_12G[122] = 0xFF;
	vsemi_mem_12G[123] = 0xFF;
	vsemi_mem_12G[124] = 0xFF;
	vsemi_mem_12G[125] = 0xFF;
	vsemi_mem_12G[126] = 0xFF;
	vsemi_mem_12G[127] = 0xD3;
	vsemi_mem_12G[128] = 0xD3;
	vsemi_mem_12G[129] = 0xE2;
	vsemi_mem_12G[130] = 0xEF;
	vsemi_mem_12G[131] = 0xFB;
	vsemi_mem_12G[132] = 0xFB;
	vsemi_mem_12G[133] = 0xFF;
	vsemi_mem_12G[134] = 0xEF;
	vsemi_mem_12G[135] = 0xFF;
	vsemi_mem_12G[136] = 0xFF;
	vsemi_mem_12G[137] = 0xD3;
	vsemi_mem_12G[138] = 0xD3;
	vsemi_mem_12G[139] = 0xE2;
	vsemi_mem_12G[140] = 0xEF;
	vsemi_mem_12G[141] = 0xFB;
	vsemi_mem_12G[142] = 0xFB;
	vsemi_mem_12G[143] = 0xFF;
	vsemi_mem_12G[144] = 0xEF;
	vsemi_mem_12G[145] = 0xFF;
	vsemi_mem_12G[146] = 0xFF;
	vsemi_mem_12G[147] = 0xFB;
	vsemi_mem_12G[148] = 0xFF;
	vsemi_mem_12G[149] = 0x3F;
	vsemi_mem_12G[150] = 0x0;
	vsemi_mem_12G[151] = 0x64;
	vsemi_mem_12G[152] = 0x0;
	vsemi_mem_12G[153] = 0x4;
	vsemi_mem_12G[154] = 0x2;
	vsemi_mem_12G[155] = 0xA;
	vsemi_mem_12G[156] = 0x5;
	vsemi_mem_12G[157] = 0x4;
	vsemi_mem_12G[158] = 0x0;
	vsemi_mem_12G[159] = 0x0;
	vsemi_mem_12G[160] = 0x8;
	vsemi_mem_12G[161] = 0x4;
	vsemi_mem_12G[162] = 0x0;
	vsemi_mem_12G[163] = 0x0;
	vsemi_mem_12G[164] = 0x4;

	return;
}

void nlm_hal_config_vsemi_mem_12G_125(void)
{
	vsemi_mem_12G[101] = 0x99;
	vsemi_mem_12G[102] = 0x0;
	vsemi_mem_12G[103] = 0x57;
	vsemi_mem_12G[104] = 0xBA;
	vsemi_mem_12G[105] = 0xBA;
	vsemi_mem_12G[106] = 0xD;
	vsemi_mem_12G[107] = 0xD;
	vsemi_mem_12G[108] = 0x18;
	vsemi_mem_12G[109] = 0x18;
	vsemi_mem_12G[110] = 0x4;
	vsemi_mem_12G[111] = 0x17;
	vsemi_mem_12G[112] = 0x8;
	vsemi_mem_12G[113] = 0x0;
	vsemi_mem_12G[114] = 0x8;
	vsemi_mem_12G[115] = 0x0;
	vsemi_mem_12G[116] = 0xFF;
	vsemi_mem_12G[117] = 0xC3;
	vsemi_mem_12G[118] = 0xF5;
	vsemi_mem_12G[119] = 0xD9;
	vsemi_mem_12G[120] = 0xF2;
	vsemi_mem_12G[121] = 0xFC;
	vsemi_mem_12G[122] = 0xFE;
	vsemi_mem_12G[123] = 0xFF;
	vsemi_mem_12G[124] = 0xFF;
	vsemi_mem_12G[125] = 0xFF;
	vsemi_mem_12G[126] = 0xFF;
	vsemi_mem_12G[127] = 0xDB;
	vsemi_mem_12G[128] = 0xE9;
	vsemi_mem_12G[129] = 0xC2;
	vsemi_mem_12G[130] = 0xF2;
	vsemi_mem_12G[131] = 0xFC;
	vsemi_mem_12G[132] = 0xFE;
	vsemi_mem_12G[133] = 0xF2;
	vsemi_mem_12G[134] = 0xF2;
	vsemi_mem_12G[135] = 0xFF;
	vsemi_mem_12G[136] = 0xFF;
	vsemi_mem_12G[137] = 0xDB;
	vsemi_mem_12G[138] = 0xE9;
	vsemi_mem_12G[139] = 0xC2;
	vsemi_mem_12G[140] = 0xF2;
	vsemi_mem_12G[141] = 0xFC;
	vsemi_mem_12G[142] = 0xFE;
	vsemi_mem_12G[143] = 0xF2;
	vsemi_mem_12G[144] = 0xF2;
	vsemi_mem_12G[145] = 0xFF;
	vsemi_mem_12G[146] = 0xFF;
	vsemi_mem_12G[147] = 0xFF;
	vsemi_mem_12G[148] = 0xF2;
	vsemi_mem_12G[149] = 0x3F;
	vsemi_mem_12G[150] = 0x0;
	vsemi_mem_12G[151] = 0x64;
	vsemi_mem_12G[152] = 0x0;
	vsemi_mem_12G[153] = 0x4;
	vsemi_mem_12G[154] = 0x2;
	vsemi_mem_12G[155] = 0xA;
	vsemi_mem_12G[156] = 0x5;
	vsemi_mem_12G[157] = 0x4;
	vsemi_mem_12G[158] = 0x0;
	vsemi_mem_12G[159] = 0x0;
	vsemi_mem_12G[160] = 0x8;
	vsemi_mem_12G[161] = 0x4;
	vsemi_mem_12G[162] = 0x0;
	vsemi_mem_12G[163] = 0x0;
	vsemi_mem_12G[164] = 0x4;

	return;
}

void nlm_hal_config_vsemi_mem_xaui(void)
{
	vsemi_mem_xaui[101] = 0xAA;
	vsemi_mem_xaui[102] = 0x0;
	vsemi_mem_xaui[103] = 0x76;
	vsemi_mem_xaui[104] = 0xBF;
	vsemi_mem_xaui[105] = 0xBF;
	vsemi_mem_xaui[106] = 0xA;
	vsemi_mem_xaui[107] = 0xA;
	vsemi_mem_xaui[108] = 0xE;
	vsemi_mem_xaui[109] = 0xE;
	vsemi_mem_xaui[110] = 0x6;
	vsemi_mem_xaui[111] = 0x16;
	vsemi_mem_xaui[112] = 0x10;
	vsemi_mem_xaui[113] = 0x0;
	vsemi_mem_xaui[114] = 0x10;
	vsemi_mem_xaui[115] = 0x0;
	vsemi_mem_xaui[116] = 0xFF;
	vsemi_mem_xaui[117] = 0xB3;
	vsemi_mem_xaui[118] = 0xF6;
	vsemi_mem_xaui[119] = 0xD0;
	vsemi_mem_xaui[120] = 0xEF;
	vsemi_mem_xaui[121] = 0xFB;
	vsemi_mem_xaui[122] = 0xFF;
	vsemi_mem_xaui[123] = 0xFF;
	vsemi_mem_xaui[124] = 0xFF;
	vsemi_mem_xaui[125] = 0xFF;
	vsemi_mem_xaui[126] = 0xFF;
	vsemi_mem_xaui[127] = 0xD3;
	vsemi_mem_xaui[128] = 0xD3;
	vsemi_mem_xaui[129] = 0xE2;
	vsemi_mem_xaui[130] = 0xEF;
	vsemi_mem_xaui[131] = 0xFB;
	vsemi_mem_xaui[132] = 0xFB;
	vsemi_mem_xaui[133] = 0xFF;
	vsemi_mem_xaui[134] = 0xEF;
	vsemi_mem_xaui[135] = 0xFF;
	vsemi_mem_xaui[136] = 0xFF;
	vsemi_mem_xaui[137] = 0xD3;
	vsemi_mem_xaui[138] = 0xD3;
	vsemi_mem_xaui[139] = 0xE2;
	vsemi_mem_xaui[140] = 0xEF;
	vsemi_mem_xaui[141] = 0xFB;
	vsemi_mem_xaui[142] = 0xFB;
	vsemi_mem_xaui[143] = 0xFF;
	vsemi_mem_xaui[144] = 0xEF;
	vsemi_mem_xaui[145] = 0xFF;
	vsemi_mem_xaui[146] = 0xFF;
	vsemi_mem_xaui[147] = 0xFB;
	vsemi_mem_xaui[148] = 0xFF;
	vsemi_mem_xaui[149] = 0x3F;
	vsemi_mem_xaui[150] = 0x0;
	vsemi_mem_xaui[151] = 0x64;
	vsemi_mem_xaui[152] = 0x0;
	vsemi_mem_xaui[153] = 0x2;
	vsemi_mem_xaui[154] = 0x1;
	vsemi_mem_xaui[155] = 0x5;
	vsemi_mem_xaui[156] = 0x5;
	vsemi_mem_xaui[157] = 0x4;
	vsemi_mem_xaui[158] = 0x0;
	vsemi_mem_xaui[159] = 0x0;
	vsemi_mem_xaui[160] = 0x8;
	vsemi_mem_xaui[161] = 0x4;
	vsemi_mem_xaui[162] = 0x0;
	vsemi_mem_xaui[163] = 0x0;
	vsemi_mem_xaui[164] = 0x4;

	return;
}

void nlm_hal_config_vsemi_mem_xaui_125(void)
{
	vsemi_mem_xaui[101] = 0xAA;
	vsemi_mem_xaui[102] = 0x0;
	vsemi_mem_xaui[103] = 0x57;
	vsemi_mem_xaui[104] = 0xBF;
	vsemi_mem_xaui[105] = 0xBF;
	vsemi_mem_xaui[106] = 0x8;
	vsemi_mem_xaui[107] = 0x8;
	vsemi_mem_xaui[108] = 0x13;
	vsemi_mem_xaui[109] = 0x13;
	vsemi_mem_xaui[110] = 0x4;
	vsemi_mem_xaui[111] = 0x17;
	vsemi_mem_xaui[112] = 0x10;
	vsemi_mem_xaui[113] = 0x0;
	vsemi_mem_xaui[114] = 0x10;
	vsemi_mem_xaui[115] = 0x0;
	vsemi_mem_xaui[116] = 0xFF;
	vsemi_mem_xaui[117] = 0xC3;
	vsemi_mem_xaui[118] = 0xF5;
	vsemi_mem_xaui[119] = 0xD9;
	vsemi_mem_xaui[120] = 0xF2;
	vsemi_mem_xaui[121] = 0xFC;
	vsemi_mem_xaui[122] = 0xFE;
	vsemi_mem_xaui[123] = 0xFF;
	vsemi_mem_xaui[124] = 0xFF;
	vsemi_mem_xaui[125] = 0xFF;
	vsemi_mem_xaui[126] = 0xFF;
	vsemi_mem_xaui[127] = 0xDB;
	vsemi_mem_xaui[128] = 0xE9;
	vsemi_mem_xaui[129] = 0xC2;
	vsemi_mem_xaui[130] = 0xF2;
	vsemi_mem_xaui[131] = 0xFC;
	vsemi_mem_xaui[132] = 0xFE;
	vsemi_mem_xaui[133] = 0xF2;
	vsemi_mem_xaui[134] = 0xF2;
	vsemi_mem_xaui[135] = 0xFF;
	vsemi_mem_xaui[136] = 0xFF;
	vsemi_mem_xaui[137] = 0xDB;
	vsemi_mem_xaui[138] = 0xE9;
	vsemi_mem_xaui[139] = 0xC2;
	vsemi_mem_xaui[140] = 0xF2;
	vsemi_mem_xaui[141] = 0xFC;
	vsemi_mem_xaui[142] = 0xFE;
	vsemi_mem_xaui[143] = 0xF2;
	vsemi_mem_xaui[144] = 0xF2;
	vsemi_mem_xaui[145] = 0xFF;
	vsemi_mem_xaui[146] = 0xFF;
	vsemi_mem_xaui[147] = 0xFF;
	vsemi_mem_xaui[148] = 0xF2;
	vsemi_mem_xaui[149] = 0x3F;
	vsemi_mem_xaui[150] = 0x0;
	vsemi_mem_xaui[151] = 0x64;
	vsemi_mem_xaui[152] = 0x0;
	vsemi_mem_xaui[153] = 0x2;
	vsemi_mem_xaui[154] = 0x1;
	vsemi_mem_xaui[155] = 0x5;
	vsemi_mem_xaui[156] = 0x5;
	vsemi_mem_xaui[157] = 0x4;
	vsemi_mem_xaui[158] = 0x0;
	vsemi_mem_xaui[159] = 0x0;
	vsemi_mem_xaui[160] = 0x8;
	vsemi_mem_xaui[161] = 0x4;
	vsemi_mem_xaui[162] = 0x0;
	vsemi_mem_xaui[163] = 0x0;
	vsemi_mem_xaui[164] = 0x4;

}


void nlm_hal_config_vsemi_mem_xaui_4page(void)
{
	vsemi_mem_xaui_4page[101] = 0xBF;
	vsemi_mem_xaui_4page[102] = 0xBF;
	vsemi_mem_xaui_4page[103] = 0xA;
	vsemi_mem_xaui_4page[104] = 0xA;
	vsemi_mem_xaui_4page[105] = 0xE;
	vsemi_mem_xaui_4page[106] = 0xE;
	vsemi_mem_xaui_4page[107] = 0x1;
	vsemi_mem_xaui_4page[108] = 0x1;
	vsemi_mem_xaui_4page[109] = 0x22;
	vsemi_mem_xaui_4page[110] = 0xF;

	return;
}

void nlm_hal_config_vsemi_mem_xaui_4page_125(void)
{
	vsemi_mem_xaui_4page[101] = 0xBF;
	vsemi_mem_xaui_4page[102] = 0xBF;
	vsemi_mem_xaui_4page[103] = 0x8;
	vsemi_mem_xaui_4page[104] = 0x8;
	vsemi_mem_xaui_4page[105] = 0x13;
	vsemi_mem_xaui_4page[106] = 0x13;
	vsemi_mem_xaui_4page[107] = 0x1;
	vsemi_mem_xaui_4page[108] = 0x1;
	vsemi_mem_xaui_4page[109] = 0x22;
	vsemi_mem_xaui_4page[110] = 0xF;
	
	return;
}

void nlm_hal_config_vsemi_mem_sgmii(void)
{
	vsemi_mem_sgmii[101] = 0x99;
	vsemi_mem_sgmii[102] = 0x0;
	vsemi_mem_sgmii[103] = 0x76;
	vsemi_mem_sgmii[104] = 0xB7;
	vsemi_mem_sgmii[105] = 0xB7;
	vsemi_mem_sgmii[106] = 0xC;
	vsemi_mem_sgmii[107] = 0xC;
	vsemi_mem_sgmii[108] = 0x1A;
	vsemi_mem_sgmii[109] = 0x1A;
	vsemi_mem_sgmii[110] = 0x6;
	vsemi_mem_sgmii[111] = 0x16;
	vsemi_mem_sgmii[112] = 0x8;
	vsemi_mem_sgmii[113] = 0x0;
	vsemi_mem_sgmii[114] = 0x8;
	vsemi_mem_sgmii[115] = 0x0;
	vsemi_mem_sgmii[116] = 0xFF;
	vsemi_mem_sgmii[117] = 0xB3;
	vsemi_mem_sgmii[118] = 0xF6;
	vsemi_mem_sgmii[119] = 0xD0;
	vsemi_mem_sgmii[120] = 0xEF;
	vsemi_mem_sgmii[121] = 0xFB;
	vsemi_mem_sgmii[122] = 0xFF;
	vsemi_mem_sgmii[123] = 0xFF;
	vsemi_mem_sgmii[124] = 0xFF;
	vsemi_mem_sgmii[125] = 0xFF;
	vsemi_mem_sgmii[126] = 0xFF;
	vsemi_mem_sgmii[127] = 0xD3;
	vsemi_mem_sgmii[128] = 0xD3;
	vsemi_mem_sgmii[129] = 0xE2;
	vsemi_mem_sgmii[130] = 0xEF;
	vsemi_mem_sgmii[131] = 0xFB;
	vsemi_mem_sgmii[132] = 0xFB;
	vsemi_mem_sgmii[133] = 0xFF;
	vsemi_mem_sgmii[134] = 0xEF;
	vsemi_mem_sgmii[135] = 0xFF;
	vsemi_mem_sgmii[136] = 0xFF;
	vsemi_mem_sgmii[137] = 0xD3;
	vsemi_mem_sgmii[138] = 0xD3;
	vsemi_mem_sgmii[139] = 0xE2;
	vsemi_mem_sgmii[140] = 0xEF;
	vsemi_mem_sgmii[141] = 0xFB;
	vsemi_mem_sgmii[142] = 0xFB;
	vsemi_mem_sgmii[143] = 0xFF;
	vsemi_mem_sgmii[144] = 0xEF;
	vsemi_mem_sgmii[145] = 0xFF;
	vsemi_mem_sgmii[146] = 0xFF;
	vsemi_mem_sgmii[147] = 0xFB;
	vsemi_mem_sgmii[148] = 0xFF;
	vsemi_mem_sgmii[149] = 0x3F;
	vsemi_mem_sgmii[150] = 0x0;
	vsemi_mem_sgmii[151] = 0x64;
	vsemi_mem_sgmii[152] = 0x0;
	vsemi_mem_sgmii[153] = 0x4;
	vsemi_mem_sgmii[154] = 0x2;
	vsemi_mem_sgmii[155] = 0x5;
	vsemi_mem_sgmii[156] = 0x5;
	vsemi_mem_sgmii[157] = 0x4;
	vsemi_mem_sgmii[158] = 0x0;
	vsemi_mem_sgmii[159] = 0x0;
	vsemi_mem_sgmii[160] = 0x8;
	vsemi_mem_sgmii[161] = 0x4;
	vsemi_mem_sgmii[162] = 0x0;
	vsemi_mem_sgmii[163] = 0x0;
	vsemi_mem_sgmii[164] = 0x4;
}

void vsemi_mem_init_sgmii_125Mhzrefclk(void)
{
	vsemi_mem_sgmii[101] = 0xAA;
	vsemi_mem_sgmii[102] = 0x0;
	vsemi_mem_sgmii[103] = 0x57;
	vsemi_mem_sgmii[104] = 0xBF;
	vsemi_mem_sgmii[105] = 0xBF;
	vsemi_mem_sgmii[106] = 0xA;
	vsemi_mem_sgmii[107] = 0xA;
	vsemi_mem_sgmii[108] = 0xE;
	vsemi_mem_sgmii[109] = 0xE;
	vsemi_mem_sgmii[110] = 0x4;
	vsemi_mem_sgmii[111] = 0x17;
	vsemi_mem_sgmii[112] = 0x10;
	vsemi_mem_sgmii[113] = 0x0;
	vsemi_mem_sgmii[114] = 0x10;
	vsemi_mem_sgmii[115] = 0x0;
	vsemi_mem_sgmii[116] = 0xFF;
	vsemi_mem_sgmii[117] = 0xC3;
	vsemi_mem_sgmii[118] = 0xF5;
	vsemi_mem_sgmii[119] = 0xD9;
	vsemi_mem_sgmii[120] = 0xF2;
	vsemi_mem_sgmii[121] = 0xFC;
	vsemi_mem_sgmii[122] = 0xFE;
	vsemi_mem_sgmii[123] = 0xFF;
	vsemi_mem_sgmii[124] = 0xFF;
	vsemi_mem_sgmii[125] = 0xFF;
	vsemi_mem_sgmii[126] = 0xFF;
	vsemi_mem_sgmii[127] = 0xDB;
	vsemi_mem_sgmii[128] = 0xE9;
	vsemi_mem_sgmii[129] = 0xC2;
	vsemi_mem_sgmii[130] = 0xF2;
	vsemi_mem_sgmii[131] = 0xFC;
	vsemi_mem_sgmii[132] = 0xFE;
	vsemi_mem_sgmii[133] = 0xF2;
	vsemi_mem_sgmii[134] = 0xF2;
	vsemi_mem_sgmii[135] = 0xFF;
	vsemi_mem_sgmii[136] = 0xFF;
	vsemi_mem_sgmii[137] = 0xDB;
	vsemi_mem_sgmii[138] = 0xE9;
	vsemi_mem_sgmii[139] = 0xC2;
	vsemi_mem_sgmii[140] = 0xF2;
	vsemi_mem_sgmii[141] = 0xFC;
	vsemi_mem_sgmii[142] = 0xFE;
	vsemi_mem_sgmii[143] = 0xF2;
	vsemi_mem_sgmii[144] = 0xF2;
	vsemi_mem_sgmii[145] = 0xFF;
	vsemi_mem_sgmii[146] = 0xFF;
	vsemi_mem_sgmii[147] = 0xFF;
	vsemi_mem_sgmii[148] = 0xF2;
	vsemi_mem_sgmii[149] = 0x3F;
	vsemi_mem_sgmii[150] = 0x0;
	vsemi_mem_sgmii[151] = 0x64;
	vsemi_mem_sgmii[152] = 0x0;
	vsemi_mem_sgmii[153] = 0x2;
	vsemi_mem_sgmii[154] = 0x1;
	vsemi_mem_sgmii[155] = 0x5;
	vsemi_mem_sgmii[156] = 0x5;
	vsemi_mem_sgmii[157] = 0x4;
	vsemi_mem_sgmii[158] = 0x0;
	vsemi_mem_sgmii[159] = 0x0;
	vsemi_mem_sgmii[160] = 0x8;
	vsemi_mem_sgmii[161] = 0x4;
	vsemi_mem_sgmii[162] = 0x0;
	vsemi_mem_sgmii[163] = 0x0;
	vsemi_mem_sgmii[164] = 0x4;
}

void nlm_hal_config_vsemi_mem_sgmii_4page(void)
{    
	vsemi_mem_sgmii_4page[101] = 0xB7;
	vsemi_mem_sgmii_4page[102] = 0xB7;
	vsemi_mem_sgmii_4page[103] = 0xC;
	vsemi_mem_sgmii_4page[104] = 0xC;
	vsemi_mem_sgmii_4page[105] = 0x1A;
	vsemi_mem_sgmii_4page[106] = 0x1A;
	vsemi_mem_sgmii_4page[107] = 0x2;
	vsemi_mem_sgmii_4page[108] = 0x2;
	vsemi_mem_sgmii_4page[109] = 0x11;
	vsemi_mem_sgmii_4page[110] = 0xF;
}

void nlm_hal_config_vsemi_mem_sgmii_4page_125Mhz(void)
{
	vsemi_mem_sgmii_4page[101] = 0xBF;
	vsemi_mem_sgmii_4page[102] = 0xBF;
	vsemi_mem_sgmii_4page[103] = 0xA;
	vsemi_mem_sgmii_4page[104] = 0xA;
	vsemi_mem_sgmii_4page[105] = 0xE;
	vsemi_mem_sgmii_4page[106] = 0xE;
	vsemi_mem_sgmii_4page[107] = 0x1;
	vsemi_mem_sgmii_4page[108] = 0x1;
	vsemi_mem_sgmii_4page[109] = 0x11;
	vsemi_mem_sgmii_4page[110] = 0xF;
}

#ifdef NLM_HAL_LINUX_KERNEL
#include <linux/types.h>
#include <linux/module.h>
EXPORT_SYMBOL(vsemi_mem_sgmii_4page);
EXPORT_SYMBOL(vsemi_mem_sgmii);
EXPORT_SYMBOL(vsemi_mem_xaui_4page);
EXPORT_SYMBOL(vsemi_mem_xaui);
EXPORT_SYMBOL(vsemi_mem_12G_4page);
EXPORT_SYMBOL(vsemi_mem_12G);
EXPORT_SYMBOL(vsemi_mem_16G_4page);
EXPORT_SYMBOL(vsemi_mem_16G);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_16G_4page);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_16G_4page_125);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_16G);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_16G_125);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_12G_4page);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_12G_4page_125);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_12G);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_12G_125);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_xaui);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_xaui_125);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_xaui_4page);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_xaui_4page_125);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_sgmii);
EXPORT_SYMBOL(vsemi_mem_init_sgmii_125Mhzrefclk);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_sgmii_4page);
EXPORT_SYMBOL(nlm_hal_config_vsemi_mem_sgmii_4page_125Mhz);
#endif
