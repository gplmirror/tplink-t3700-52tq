/*-
 * Copyright (c) 2003-2013 Broadcom Corporation
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_2# */


#ifdef NLM_HAL_LINUX_KERNEL
#include <linux/netdevice.h>
#endif
#include "nlm_hal_fmn.h"
#include "nlm_hal_nae.h"
#include "nlm_hal_sys.h"
#include "libfdt.h"
#include "fdt_helper.h"
#include "nlm_evp_cpld.h"
#include "nlm_hal_vsemi_data.h"

#define VAL_UCORE_RESET(x) ( ( (x) &0xffff) << 8)

#define GET_PORT_PROP(prop, buf, len)                                   \
        copy_fdt_prop(fdt, nae_port_str, prop, PROP_CELL, buf, len)

#define GET_PORT_STR_PROP(prop, buf, len)                                       \
        copy_fdt_prop(fdt, nae_port_str, prop, PROP_STR, buf, len)

#define NLM_NAE_MAX_XONOFF_THR_GRPS 8

extern struct nlm_hal_ext_phy* get_phy_info(int inf);
extern void register_phy(int node, int inf, int* hw_portid);
extern uint32_t get_dom_owner_mask(void *fdt, int dom_id, char *module);
extern int nlm_hal_get_fdt_freq(void *fdt, int type);

static int nae_reset_done[NLM_MAX_NODES] = { 0 };
static void xlp_nae_config_interlaken(int node, int blk,int port, int num_lanes);
static void xlp_nae_config_xaui(int node, int block, int port, int vlan_pri_en,
                              int rxaui_scrambler, int mode, int higig_type);
static unsigned int ucore_shared_scratch[NLM_MAX_NODES][128];
static unsigned int ucore_shared_scratch_words[NLM_MAX_NODES];
static void xlp_nae_config_lane_gmac(int node, int cplx_mask);
static void xlp_nae_ilk_loopback(int node, int blk, int num_lanes);
static inline void nlm_hal_PMFF_ALL_workaround(int node, int block, int lane_ctrl);

uint32_t *cntx2port[NLM_MAX_NODES];

extern unsigned char vsemi_mem_sgmii_4page [256];
extern unsigned char vsemi_mem_sgmii [256];

extern unsigned char vsemi_mem_xaui_4page [256];
extern unsigned char vsemi_mem_xaui [256];

extern unsigned char vsemi_mem_12G_4page [256];
extern unsigned char vsemi_mem_12G [256];

extern unsigned char vsemi_mem_16G_4page [256];
extern unsigned char vsemi_mem_16G [256];
extern void vsemi_mem_init_sgmii_125Mhzrefclk(void);
extern void nlm_hal_config_vsemi_mem_sgmii_4page_125Mhz(void);

/* #define MACSEC_DEBUG 1 */
/* #define INCLUDE_NAE_DEBUG 1 */
#ifdef INCLUDE_NAE_DEBUG
#define NAE_DEBUG	nlm_print
#else
#define NAE_DEBUG(...)
#endif
#undef VSEMI_DEBUG
static uint32_t nae_vfbid_tbl[NLM_MAX_NODES][MAX_VFBID_ENTRIES];

/* #define NAE_SH_INFO_DBG 1 */
#ifdef NAE_SH_INFO_DBG
#define nlm_dbg_print nlm_print
#else
#define nlm_dbg_print(x, args...) { }
#endif

static uint32_t nlm_get_max_ports(void)
{
	uint32_t max_ports;
	if (is_nlm_xlp3xx()) {
		max_ports = XLP_3XX_MAX_PORTS;
	}
	else if (is_nlm_xlp2xx()) {
		max_ports = XLP_2XX_MAX_PORTS;
	}
	else {
		max_ports = XLP_MAX_PORTS;
	}
	return max_ports;
}


static int nlm_hal_vsemi_cmd_poll_pend(int node, int block)
{
	uint32_t vsemi_cmd=0; 
	uint32_t  retries=0;
	/*wait for command to clear*/	
	while(retries<100){
		vsemi_cmd = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CMD);
		if( vsemi_cmd & (1<<31)){
			retries++;
			nlm_mdelay(100);	
			continue;
		}else{
			/* nlm_print("VSEMI cmd pend clear\n");	 */
			break;
		}
	}
	
	if(retries==100){
		nlm_print("VSEMI cmd  clear NOT OK: VSEMI Configuration may not work\n");
		return 0;
	}

	return (vsemi_cmd&0xff);
}

void write_vsemi_reg(int node, int block, uint32_t lane_no, uint32_t addr, uint32_t data) {
    uint32_t tmp_data;
    tmp_data = (0x2 << 29) | (lane_no << 21) | ((addr & 0x1fff) << 8) | (data & 0xff);
    nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CMD, tmp_data);
    nlm_hal_vsemi_cmd_poll_pend(node, block);	
} 

static uint32_t read_vsemi_reg(int node, int block, uint32_t lane_no, uint32_t addr)
{
    uint32_t tmp_data, rd_data;
    tmp_data = (0x3 << 29) | (lane_no << 21) | ((addr & 0x1fff) << 8);
    nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CMD, tmp_data);
    rd_data = nlm_hal_vsemi_cmd_poll_pend(node, block);
    return (rd_data & 0xFF);
}

#ifdef VSEMI_DEBUG
static void display_vsemi_indirect_reg(int node, int block) 
{
     uint32_t rd_data, reg_addr, lane_no;
    for (reg_addr=101; reg_addr <111; reg_addr++) {
        for (lane_no=0; lane_no < 4; lane_no++) {
            rd_data = read_vsemi_reg(node, block, lane_no, reg_addr);
            nlm_print("VSEMI::  lane:%0d addr: 0x%x rd_data: 0x%02x\n",lane_no, reg_addr, rd_data);
        }
    }
    for (reg_addr=101; reg_addr <165; reg_addr++) {
        rd_data = read_vsemi_reg(node, block, 4, reg_addr);
        nlm_print("VSEMI:: common lane addr: 0x%x rd_data: 0x%02x\n", reg_addr, rd_data);
    }

	
}
#endif /* VSEMI_DEBUG */

void nlm_hal_xlp2xx_nae_program_vsemi(int node, int block, int xaui_speed, int phy_mode)
{
	uint32_t reg_addr, lane_no;
	uint32_t vsemi_cmd=0; 
	for (reg_addr=101; reg_addr <111; reg_addr++){
		for (lane_no=0; lane_no<4; lane_no++) { 
			vsemi_cmd=0; 
			if(xaui_speed==16){
				vsemi_cmd |= vsemi_mem_16G_4page[reg_addr];		
			}
			if(xaui_speed==12){
				vsemi_cmd |= vsemi_mem_12G_4page[reg_addr];		
			}
			if((xaui_speed==10)) {
				vsemi_cmd |= vsemi_mem_xaui_4page[reg_addr];		
				/* nlm_print("vsemi cmd: xaui 4page: [%X]:%X\n", reg_addr, vsemi_cmd); */
			}
			if(phy_mode==PHYMODE_SGMII){
				vsemi_cmd |= vsemi_mem_sgmii_4page[reg_addr]; 
			}
			write_vsemi_reg(node, block, lane_no, reg_addr, vsemi_cmd);
		}
	}
	
	/*configure only page 4*/
	for (reg_addr=101; reg_addr <165; reg_addr++){
		vsemi_cmd=0; 
		if(xaui_speed==16){
			vsemi_cmd |= vsemi_mem_16G[reg_addr];		
		}
		if(xaui_speed==12){
			vsemi_cmd |= vsemi_mem_12G[reg_addr];		
		}
		if(xaui_speed==10) {
			vsemi_cmd |= vsemi_mem_xaui[reg_addr];		
			/* nlm_print("vsemi cmd: mem xaui [%X]:%X\n", reg_addr, vsemi_cmd); */
		}
		if(phy_mode==PHYMODE_SGMII){
			vsemi_cmd |= vsemi_mem_sgmii[reg_addr]; 
		}
		write_vsemi_reg(node, block, 4, reg_addr, vsemi_cmd);
	}

	for (lane_no=0; lane_no<4; lane_no++) {
		write_vsemi_reg(node, block, lane_no, 21, 0xd9);
		write_vsemi_reg(node, block, lane_no, 22, 0x0);
		write_vsemi_reg(node, block, lane_no, 23, 0x0);
		write_vsemi_reg(node, block, lane_no, 87, 0x1f);
	}

	for (lane_no=0; lane_no<4; lane_no++) {
		write_vsemi_reg(node, block, lane_no, 24, 0x2);
		write_vsemi_reg(node, block, lane_no, 25, 0x0);
		write_vsemi_reg(node, block, lane_no, 26, 0x08);
		write_vsemi_reg(node, block, lane_no, 27, 0x78);
		write_vsemi_reg(node, block, lane_no, 28, 0x0);
		vsemi_cmd = read_vsemi_reg(node, block, lane_no, 86);
		vsemi_cmd &= ~(1<<3);
		write_vsemi_reg(node, block, lane_no, 86, vsemi_cmd);
	}

	nlm_print("VSEMI configuration is OK \n");
}

static uint32_t indirect_pma_register_read(int node, int block, int lane_ctrl, int pma_reg_addr)
{
	uint32_t val = 0; 
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	val |= (1<<30) | (1<<17) | (1<<16);
	val = val | (pma_reg_addr<<8);
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl, val);
	while(1){
		val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl); 
		if(val & (1<<18))
			break;
	}
	return (val&0xff);
}


static void indirect_pma_register_wite(int node, int block, int lane_ctrl, int pma_reg_addr, int reg_val)
{
	uint32_t val = 0,i; 
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	val |= (1<<30) | (1<<17);
	val &= ~(1<<16);
	val = (val & 0xffff0000) | ((pma_reg_addr & 0xff) << 8) | (reg_val & 0xff); 
  
	for (i=0; i<2; i++) {
		nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl, val);
		while(1) {
			val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl); 
			if(val & (1<<18))
			break;
		}
	}
}

void set_bit2_pm_fifo (int node, int block, int lane_ctrl)
{
	uint32_t val = 0;
	val = indirect_pma_register_read(node,  block, lane_ctrl, 0xE);
	val |= (1<<2);
	indirect_pma_register_wite(node,  block, lane_ctrl, 0xE, val);		
	val = indirect_pma_register_read(node,  block, lane_ctrl, 0xE);
}


/*  PLL  */
/**********************************************************************
 *  nae_lane_reset_txpll
 * * serdes lane progaming 
 ********************************************************************* */
/**
* @brief xlp2xx_nae_lane_reset_txpll function is used to reset the Tx PLL for XLP2XX products.
*
* @param [in] node Node number
* @param [in] block NAE Register Memory Map Block
* @param [in] lane_ctrl Lane to reset (0-3)
* @param [in] mode 0-unused, 1-XAUI/HS-SGMII, 2-SGMII, 3-INTERLAKEN, 6-PHYMODE_RXAUI
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/

void xlp2xx_nae_lane_reset_txpll(int node, int block, int lane_ctrl, int mode)
{
	uint32_t val = 0;
	NAE_DEBUG("%s: block %d lane_ctrl %x \n",__func__,block,lane_ctrl);

	/* rxaui mode only lanes 0 and 2 are used in each complex, 1/3 not used
	 * skip lane 1 & 3 by setting lane_inc = 2
	 */
	if(mode == PHYMODE_RXAUI && (lane_ctrl%2) )
		return;
	
	val = (1<<20) | (1<<29);
	val = (mode << PHY_LANE_CTRL_PHYMODE_POS) | (val&0xe1ffffff);
        nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl, val);
	nlm_mdelay(1);
	
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	NAE_DEBUG("xlp2xx_nae_lane_reset_txpll value before power down =0x%x\n", val);
	
	val &= ~(1<<29);
	val = val & 0xfff80000;
        nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl, val);
	nlm_mdelay(1);
	
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	NAE_DEBUG("xlp2xx_nae_lane_reset_txpll value after power up =0x%x\n", val);
	val |=(1<<30);
        nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl, val);
	nlm_mdelay(1);
	while(!((val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl-PHY_LANE_0_CTRL)) & PHY_LANE_STAT_PCR));


	NAE_DEBUG("Before register reset de-assertion PMA value=0x%x\n", nlm_hal_read_mac_reg( node, block, PHY, lane_ctrl));
}

/**
* @brief xlp3xx_8xxb0_nae_lane_reset_txpll function is used to reset the Tx PLL for XLP3XX and XLP8XX-B0 products.
*
* @param [in] node Node number
* @param [in] block NAE Register Memory Map Block
* @param [in] lane_ctrl Lane to reset (0-3)
* @param [in] mode 0-unused, 1-XAUI/HS-SGMII, 2-SGMII, 3-INTERLAKEN, 6-PHYMODE_RXAUI
*
* @return
* 	- 0 on success, non-zero on failure
* 
* @ingroup hal_nae
*
*/
int xlp3xx_8xxb0_nae_lane_reset_txpll(int node, int block, int lane_ctrl, int mode)
{
	uint32_t val = 0;
	int rext_sel = 0;
	int count=0;
	int count_max=1000;
	uint32_t val_temp = 0;
	NAE_DEBUG("%s: node %d block %d lane_ctrl %x \n", __func__, node, block, lane_ctrl);

	/* rxaui mode only lanes 0 and 2 are used in each complex, 1/3 not used
	 * skip lane 1 & 3 by setting lane_inc = 2
	 */
	if(mode == PHYMODE_RXAUI && (lane_ctrl%2))
	{
		NAE_DEBUG("mode is %x so function is returning\n", mode);
		return 0;
	}

	if(lane_ctrl != 4)
		rext_sel = (1 << 23);

	NAE_DEBUG("Before register reset de-assertion PMA value=0x%x\n", nlm_hal_read_mac_reg( node, block, PHY, lane_ctrl));

	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	val |= (1<< 29);
	val &= ~((1<< 30)&0xFFFFFFFF);
	val &= ~((1<< 20) & 0xFFFFFFFF);
	val |= (mode << PHY_LANE_CTRL_PHYMODE_POS);
	val &= ((~(1<<23)) & 0xFFFFFFFF); // ensure rext_sel is 0 before bringing interfaces out of reset
	val &= ~(1 << 17); //setting indirect register request access to 0
	NAE_DEBUG("PHY LANE CTRL REG to be written with value: %x\n",val);
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,val);
	nlm_mdelay(1);
	val_temp = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	NAE_DEBUG("PHY LANE CTRL REG that was just written is :  %x\n", val_temp);
	nlm_mdelay(1);

	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);

	if(mode != PHYMODE_SGMII){
		val |= PHY_LANE_CTRL_BPC_XAUI; /*Set comma bypass for XAUI*/
		NAE_DEBUG("Just set comma bypass for XAUI\n");
	}

	/* Clear the Power Down bit */
	NAE_DEBUG("Clearing the Power Down Bit(Bit 29)\n");
	val &= ~( (1 << 29) | (0x7ffff));
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl, val);
	nlm_mdelay(1);
	val_temp = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	NAE_DEBUG("PHY LANE CTRL REG that was just written is :  %x\n", val_temp);

	NAE_DEBUG("Setting the PMA Register Bit(Bit 20)\n");
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	val |= 0x100000;  /* Bit20: serdes reg reset Storm & Eagle B0 */
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,val);
	nlm_mdelay(1);
	val_temp = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	NAE_DEBUG("PHY LANE CTRL REG that was just written is :  %x\n", val_temp);

	NAE_DEBUG("Setting PMA Controller reset bit(Bit 30)\n");
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	val |= 0x40000000; /* Unset the reset (inverse logic) : Bit30: epcs reset */
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,val);
	nlm_mdelay(1);
	val_temp = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	NAE_DEBUG("PHY LANE CTRL REG that was just written is :  %x\n", val_temp);

	/* Setting Rext_Sel*/
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	val |= rext_sel;
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,val);
	NAE_DEBUG("Setting the Rext_sel register\n");
	nlm_mdelay(1);

	while ((nlm_hal_read_mac_reg(node, block, PHY, (lane_ctrl - 4)) & (PHY_LANE_STAT_PCR)) != (PHY_LANE_STAT_PCR) && count!=count_max) 
	{
		NAE_DEBUG("Wait for PMA Ready ..........%d\n", count);
		count++;
	}
	if(count==count_max)
	{
		nlm_print("PMA controller of node %d complex %d and lane %d did not come out of reset!!\n", node, block, (lane_ctrl-4));
		return -1;
	}
	count=0;
	while ((nlm_hal_read_mac_reg(node, block, PHY, (lane_ctrl - 4)) & (LANE_TX_CLK)) != LANE_TX_CLK && count!=count_max) 
	{
		NAE_DEBUG("Wait for TX clock to become stable ..........%d\n", count);
		count++;
	}
	if(count==count_max)
	{
		nlm_print(" TX clock stable bit of node %d complex %d and lane %d not set!!\n", node, block, (lane_ctrl-4));
		return -2;
	}
	count=0;
	while ((nlm_hal_read_mac_reg(node, block, PHY, (lane_ctrl - 4)) & (LANE_RX_CLK)) != LANE_RX_CLK && count!=count_max) 
	{
		NAE_DEBUG("Wait for RX clock to become stable ..........%d\n", count);
		count++;
	}
	if(count==count_max)
	{
		nlm_print("RX clock stable bit of node %d complex %d and lane %d not set!!\n", node, block, (lane_ctrl-4));
		return -3;
	}
	NAE_DEBUG("Reset PMA for node %d complex %d and lane %d was successful!! \n", node, block, (lane_ctrl-4));

	nlm_print("Reset PLL done @node:%d block:%d lane:%d mode:%d\n", node, block, lane_ctrl, mode);
    return 0;
}


/**
* @brief xlp8xx_ax_nae_lane_reset_txpll function is used to reset the Tx PLL for XLP8XX-AX products.
*
* @param [in] node Node number
* @param [in] block NAE Register Memory Map Block
* @param [in] lane_ctrl Lane to reset (0-3)
* @param [in] mode 0-unused, 1-XAUI, 2-SGMII, 3-INTERLAKEN
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
void xlp8xx_ax_nae_lane_reset_txpll(int node, int block, int lane_ctrl, int mode)
{
        uint32_t val = 0, saved_data;
        int rext_sel = 0;
	
	val = PHY_LANE_CTRL_RST | PHY_LANE_CTRL_PWRDOWN | (mode << PHY_LANE_CTRL_PHYMODE_POS);

	if(mode != PHYMODE_SGMII){
                val |= PHY_LANE_CTRL_BPC_XAUI; /*Set comma bypass for XAUII*/
        }

	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,  val);

	if (lane_ctrl != 4) {
		int i;
		rext_sel = (1 << 23);

		if(mode != PHYMODE_SGMII){
			rext_sel |= PHY_LANE_CTRL_BPC_XAUI;
		}

		val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
		val &= ~PHY_LANE_CTRL_RST; /* Set the reset (inverse logic) */
		val |= rext_sel;

		/* Resetting PMA for non-zero lanes */
		nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,val);

		for (i=0; i< 0x100000; ++i)
			; /* empty loop */

		val |= PHY_LANE_CTRL_RST; /* Unset the reset (inverse logic) */
		nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,val);

		val = 0;
	}

	/* Come out of reset for TXPLL */
	saved_data = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl) & 0xFFC00000;

	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,         (0x66 << PHY_LANE_CTRL_ADDR_POS)
			       | PHY_LANE_CTRL_CMD_READ
			       | PHY_LANE_CTRL_RST
			       | rext_sel
			       | val );
	while ((nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING);

	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,         (0x66 << PHY_LANE_CTRL_ADDR_POS)
			       | PHY_LANE_CTRL_CMD_READ
			       | PHY_LANE_CTRL_CMD_START
			       | PHY_LANE_CTRL_RST
			       | rext_sel
			       | val );
	while (((val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING));

	val &= 0xFF;

	/* set bit[4] to 0
	 */
	val &= ~(1 << 4);
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,
			((0x66 << PHY_LANE_CTRL_ADDR_POS)
			 | (0x0 << 19) /* (0x4 << 19) */
			 | rext_sel
			 | saved_data
			 | val )
			& ~(PHY_LANE_CTRL_CMD_READ));

	while ((nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING);
	
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl,   
			((0x66 << PHY_LANE_CTRL_ADDR_POS)
			 | PHY_LANE_CTRL_CMD_START
			 | (0x0 << 19) /* (0x4 << 19) */
			 | rext_sel
			 | saved_data
			 | val )
			& ~(PHY_LANE_CTRL_CMD_READ));

	while ((nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING);

	/*  Read Phy lane Status to check if PMA controller is ready
	 *  PHY_LANE_0_STATUS = lane_ctrl-PHY_LANE_0_CTRL
	 */
	while(!((val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl-PHY_LANE_0_CTRL)) & PHY_LANE_STAT_PCR));

	/* Clear the Power Down bit */
	val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	val &= ~( (1 << 29) | (0x7ffff));
	nlm_hal_write_mac_reg(node, block, PHY, lane_ctrl, (rext_sel | val));
}


/*
  cplx configuration needed to be parsed from fdt tree and set at the time
  before xlp_nae_config_lane_gmac can be called
*/
/**********************************************************************
 *  nae_config_lane_gmac
 *
 ********************************************************************* */
/**
* @brief xlp_nae_config_lane_gmac function is used to configure lanes in SGMII mode.
*
* @param [in] node Node number
* @param [in] cplx_mask Mask of complexes to configure (possible in XLP832: 0x1F)
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
static void xlp_nae_config_lane_gmac(int node, int cplx_mask)
{
	int block, lane_ctrl;
	int cplx_lane_enable = LM_SGMII | (LM_SGMII << 4) | (LM_SGMII << 8) | (LM_SGMII << 12);
	int lane_enable = 0, vsemi_config=0;
	static int vsemi_por=0;

	/*  Lane mode progamming
	 */
	block = 7;
	/* Complexes 0, 1 */
	if (cplx_mask & 0x1) {
		lane_enable |= cplx_lane_enable;
	}
	if (cplx_mask & 0x2) {
		lane_enable |= (cplx_lane_enable << 16);
	}
	if (lane_enable) {
		nlm_hal_write_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_0_1,  lane_enable);
		lane_enable = 0;
	}
	
	NAE_DEBUG("xlp_nae_config_lane_gmac with cplx_mask=0x%x and config=0x%x\n",cplx_mask ,nlm_hal_read_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_0_1));
	/* Complexes 2 3 */
	if (cplx_mask & 0x4) {
		lane_enable |= cplx_lane_enable;
	}
	if (cplx_mask & 0x8) {
		lane_enable |= (cplx_lane_enable << 16);
	}
	nlm_hal_write_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_2_3, lane_enable);
	
	/*complex 4*/
	if(cplx_mask & 0x10){
		nlm_hal_write_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_4, LM_SGMII << 4 | LM_SGMII);	
		block = 4;
		for( lane_ctrl = PHY_LANE_0_CTRL; lane_ctrl <= PHY_LANE_1_CTRL; lane_ctrl++)
		{
			if (is_nlm_xlp8xx_bx()) {
				xlp3xx_8xxb0_nae_lane_reset_txpll(node, block, lane_ctrl, PHYMODE_SGMII);
				nlm_hal_PMFF_ALL_workaround(node, block, lane_ctrl);
			} else {
				xlp8xx_ax_nae_lane_reset_txpll(node, block, lane_ctrl, PHYMODE_SGMII);
			}
		}
	}	

	for( block = 0; block < 4; block++)
	{
		if ((cplx_mask & (1 << block)) == 0) {
			continue;
		}
		
		if(is_nlm_xlp2xx()){
			int delay;
			if(!vsemi_por){			
				vsemi_config = nlm_hal_read_mac_reg(node, block+1, PHY, VSEMI_CTL0);
				vsemi_config &= ~VSEMI_CTL_POR;
				nlm_hal_write_mac_reg(node, block+1, PHY, VSEMI_CTL0, vsemi_config);
				vsemi_por++;
			}
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL0);
			vsemi_config &= 0xFFFF01FF;
			vsemi_config |= VSEMI_CTL_POR | VSEMI_CTL_SYNTH_RST | VSEMI_CTL_RTHR;
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL0, vsemi_config);
    			
			for(delay=0; delay<1000000; delay++);
			
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL0);
			vsemi_config &= ~VSEMI_CTL_POR;
			vsemi_config &= 0xFFFFFE00;
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL0, vsemi_config);
    			
			for(delay=0; delay<1000000; delay++);
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL1);
			vsemi_config &= ~((0x7<<4) | 0x7);
			vsemi_config |= VSEMI_CTL_SGMII_DR | VSEMI_CTL_SGMII_DW;
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL1, vsemi_config);

			nlm_hal_xlp2xx_nae_program_vsemi(node, block, 0, PHYMODE_SGMII);
#ifdef VSEMI_DEBUG
			display_vsemi_indirect_reg(node, block);
#endif
			
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL0);
			vsemi_config &= ~(VSEMI_CTL_POR | VSEMI_CTL_SYNTH_RST | VSEMI_CTL_RTHR);
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL0, vsemi_config);
		}

		for( lane_ctrl = PHY_LANE_0_CTRL; lane_ctrl <= PHY_LANE_3_CTRL; lane_ctrl++){
			if ((is_nlm_xlp3xx()) || (is_nlm_xlp8xx_bx())) {
				xlp3xx_8xxb0_nae_lane_reset_txpll(node, block, lane_ctrl, PHYMODE_SGMII);
				nlm_hal_PMFF_ALL_workaround(node, block, lane_ctrl);
			}else if(is_nlm_xlp2xx()){
				xlp2xx_nae_lane_reset_txpll(node, block, lane_ctrl, PHYMODE_SGMII);
			}else{
				xlp8xx_ax_nae_lane_reset_txpll(node, block, lane_ctrl, PHYMODE_SGMII);
			}	
		}
	}
	return;
}

void nlm_hal_mdio_init(int node)
{
        nlm_hal_mdio_reset(node, NLM_HAL_INT_MDIO, 0);
        nlm_hal_mdio_reset(node, NLM_HAL_EXT_MDIO, 0);
        nlm_hal_mdio_reset(node, NLM_HAL_EXT_MDIO, 1);
        nlm_hal_mdio_reset(node, NLM_HAL_EXT_MDIO_C45, 0);
        nlm_hal_mdio_reset(node, NLM_HAL_EXT_MDIO_C45, 1);
	nlm_mdelay(3);
}

/**
* @brief nlm_hal_sgmii_pcs_init function resets MDIOs, scans external PHYs, and configures lanes in SGMII mode.
*
* @param [in] node Node number
* @param [in] sgmii_cplx_mask Complex mask to configure in SGMII mode (possible in XLP832: 0x1F)
*
* @return
*       - none
*
* @ingroup hal_nae
*
*/
void nlm_hal_sgmii_pcs_init(int node, int sgmii_cplx_mask)
{
	if( is_nlm_xlp2xx() )
	{
		vsemi_mem_init_sgmii_125Mhzrefclk();
		nlm_hal_config_vsemi_mem_sgmii_4page_125Mhz();
	}

#if !defined(XLP_SIM) || defined(NLM_BOARD)
        sgmii_scan_phys(node);
#endif
        xlp_nae_config_lane_gmac(node, sgmii_cplx_mask);
        nlm_print("Net:   Completed PCS Configuration\n");
}

/**
* @brief nlm_hal_config_sgmii_if function configures an SGMII interface to the correct speed and mode.
*
* @param [in] node Node number
* @param [in] inf Interface number
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
void nlm_hal_config_sgmii_if(int node, int inf)
{
	unsigned int mac_cfg1 = 0, mac_cfg2 = 0;
	unsigned int netwk_inf = 0;
	unsigned int ifmode, speed, duplex;
	struct nlm_hal_ext_phy *this_phy=NULL;
	struct nlm_hal_mii_info mii_info;

	/* Disable TX , Rx for now */
	mac_cfg1 = read_gmac_reg(node, inf, MAC_CONF1);
	write_gmac_reg(node, inf , MAC_CONF1, mac_cfg1 & ~(0x5));
	netwk_inf = read_gmac_reg(node, inf, NETWK_INF_CTRL_REG);
	write_gmac_reg(node, inf ,NETWK_INF_CTRL_REG, netwk_inf &  (~(1<<2)));

	/*Verify whether inf is registered intf*/
	this_phy = get_phy_info(inf);
	if(!this_phy){
		nlm_print("Interface is not registered. Interface PHY-addr mismatch\n");
		return;
	}
#if !defined(XLP_SIM) || defined(NLM_BOARD)
	/* Enable auto negotiation on PHY side */
	nlm_hal_ext_phy_an(node, inf);

	/* Read PHY status from extended status */
	nlm_hal_status_ext_phy(node, inf, &mii_info);
        speed= mii_info.speed;
        duplex= mii_info.duplex;
#else
	speed = SPEED_1000M;
	duplex = 1;
#endif
	ifmode = ((speed == SPEED_1000M) ? INF_BYTE_MODE : INF_NIBBLE_MODE);

	/* Configure negotiated speed and mode */
        netwk_inf  = read_gmac_reg(node, inf, NETWK_INF_CTRL_REG);
        netwk_inf &= (~(0x3));
	write_gmac_reg(node, inf , NETWK_INF_CTRL_REG, netwk_inf | SPEED(speed));

        mac_cfg2 = read_gmac_reg(node, inf, MAC_CONF2);
        mac_cfg2 &= (~(0x3 << 8));
	write_gmac_reg(node, inf , MAC_CONF2,
			       mac_cfg2            |
			       INF_IFMODE(ifmode)     |
			       INF_FULLDUP(duplex));

	/* Clear statistics counters */
        netwk_inf  = read_gmac_reg(node, inf, NETWK_INF_CTRL_REG);
        write_gmac_reg(node, inf , NETWK_INF_CTRL_REG, netwk_inf | (1<<15));

	/* Enable statistics counters */
	netwk_inf  = read_gmac_reg(node, inf, NETWK_INF_CTRL_REG);
	write_gmac_reg(node, inf , NETWK_INF_CTRL_REG, (netwk_inf & (~(1<<15))) | (1 << 16));

        mac_cfg1 = read_gmac_reg(node, inf, MAC_CONF1);
        write_gmac_reg(node, inf , MAC_CONF1, mac_cfg1 | (0x3 << 4));
}

/*
 *  Ucore support
 */
/**
* @brief nlm_hal_load_ucore function is used to program NAE uCores.
*
* @param [in] node Node number
* @param [in] ucore_mask Mask of uCores to program
* @param [in] opcodes Pointer to array of opcode values
* @param [in] num_opcodes Number of opcodes
*
* @return
* 	- 0 on success
* 
* @ingroup hal_nae
*
*/
int nlm_hal_load_ucore(int node, int ucore_mask, unsigned int *opcodes, int num_opcodes)
{
	int mask = ucore_mask & NAE_UCORE_MASK;
	unsigned int id = 0;
	int i;

	while (id < MAX_NAE_UCORES) {

		if ((mask & (1 << id)) == 0) {
			id++;
			continue;
		}

		for (i=0; i < num_opcodes; ++i) {
			nlm_hal_write_ucode(node, id, (i * 4), opcodes[i]);
		}
		id++;
	}
	return 0;
}

/**
* @brief nlm_hal_write_ucore_shared_mem function is used to write the shared memory of the NAE uCores.
*
* @param [in] data Pointer to the 32-bit data
* @param [in] words Number of words of data to write
*
* @return
*  - 0 on success
* 
* @ingroup hal_nae
*
*/
int nlm_hal_write_ucore_shared_mem(int node, unsigned int *data, int words)
{
	int i = 0;
	int end =  ucore_shared_scratch_words[node] + words;
	if(end > 128)
		return -1;
	for (i=ucore_shared_scratch_words[node]; i<end; ++i) {
		ucore_shared_scratch[node][i] = data[i];
	}
	ucore_shared_scratch_words[node] += words;
	return 0;
}

/*
 * Temporary direct ucore configuration. HAL needs to support stop and restart
 * of ucores before reloading code
 */
/**
* @brief local_load_ucore function is used to load the instruction memory of the NAE uCores.
*
* @param [in] node Node number
* @param [in] ucore_mask Mask of uCores to load (possible in XLP832: 0xffff)
* @param [in] opcodes Pointer to opcodes
* @param [in] num_opcodes Number of opcodes to write
*
* @return
*  - 0 on success
* 
* @ingroup hal_nae
*
*/
static int local_load_ucore(int node, int ucore_mask, unsigned int *opcodes, int num_opcodes)
{
	int mask = ucore_mask & NAE_UCORE_MASK;
	unsigned int id = 0;
	int i, max_ucore;
	volatile uint32_t ucore_cfg = 0;

	NAE_DEBUG("node %d: ucore_mask 0x%x num_opcodes %d\n",node, ucore_mask, num_opcodes);
	/* Stop all ucores */
#ifndef UCORE_POE_BYPASS
	if (nae_reset_done[node] == 0) { /* Skip the Ucore reset if NAE reset is done */
#else
	if (is_nlm_xlp8xx_bx()) {
#endif
		nlm_print("Stopping and Resetting all ucore...\n");

		ucore_cfg = nlm_hal_read_nae_reg(node, RX_UCORE_CFG);
#ifndef	UCORE_POE_BYPASS
		nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg | (1 << 24));
#else
		ucore_cfg &= (~VAL_UCORE_RESET(ucore_mask));
		nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg | (1 << 24) | (1<<28));
#endif

		/* poll for ucore to get in to a wait state */
		for(;;) {
			ucore_cfg = nlm_hal_read_nae_reg(node, RX_UCORE_CFG);
			if (ucore_cfg & (1 << 25)) break;
		}
		nlm_print("Ucore Reset Complete\n");
	}

	nlm_print("Loading ucores (mask = 0x%04x)\n", mask);

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
                max_ucore = XLP_3XX_MAX_NAE_UCORES;
        }
        else {
                max_ucore = MAX_NAE_UCORES;
        }

	while (id < max_ucore) {

		if ((mask & (1 << id)) == 0) {
			id++;
			continue;
		}

		for (i=0; i < num_opcodes; ++i) {
			nlm_hal_write_ucode(node, id, (i * 4), opcodes[i]);
		}
		if(num_opcodes & 0x1){
			/* Add 'nop' if total number of instructions are odd */
			nlm_hal_write_ucode(node, id, (i * 4), 0x0);	
		}
		id++;
	}
	/* Download u-core shared memory data*/
	if(ucore_shared_scratch_words[node]){
		ucore_cfg = nlm_hal_read_nae_reg(node, RX_UCORE_CFG);	
		/*set iram to 0*/
		nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg & ~(1<<7));
		for (i=0; i<ucore_shared_scratch_words[node]; ++i) {
			nlm_hal_write_ucode(node, 0, (i * 4), ucore_shared_scratch[node][i]);
		}
		/*restore ucore_cfg*/
		nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg);
	}

	/* Enable per-domain ucores */
        ucore_cfg = nlm_hal_read_nae_reg(node, RX_UCORE_CFG);

	/* write one to reset bits to put the ucores in reset */
	ucore_cfg = ucore_cfg | (VAL_UCORE_RESET(ucore_mask));
        nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg);

	/* write zero to reset bits to pull them out of reset */
	ucore_cfg = ucore_cfg & (~VAL_UCORE_RESET(ucore_mask)) & ~(1 << 24);
        nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg);

	return 0;
}

#define UCORE_SRC_NODE(id) "/soc/nae-cfg/ucore/src@"#id

/* Currently only one microcore cfile (given under src@1 node is supported */
#define GET_UCORE_PROP(path_str, prop, buf, len)					\
	copy_fdt_prop(fdt, path_str, prop, PROP_CELL, buf, len)

/* Currently only suports only one Ucore file across all ucores */
/**
* @brief parse_ucore_config function is used to initailize and program the NAE uCores based on the configuration in FDT.
*
* @param [in] fdt Pointer to the FDT
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void parse_ucore_config(void *fdt, int node)
{
	int size = 0;
	unsigned int *uc_opcodes;
	uint32_t uc_mask = 0, num_opcodes = 0;
	char path_str[50];
	int count = 1;
	int ret = 0;

	while(count <= 16) {
		sprintf(path_str, "/soc/nae@node-%d/ucore/src@%d",node, count);
		/* Domain specific ucore mask */
		ret = GET_UCORE_PROP(path_str, "mask", &uc_mask, sizeof(uint32_t));
		if (ret < 0)
			return;
		nlm_print("UCORE MASK 0x%x\n",uc_mask);
		GET_UCORE_PROP(path_str, "num-opcodes", &num_opcodes, sizeof(uint32_t));

		size = sizeof(uint32_t) * num_opcodes;
		uc_opcodes = nlm_malloc(size);
		if (!uc_opcodes) {
			nlm_print("[%s] Unable to allocate temporary memory\n", __func__);
			return;
		}

		GET_UCORE_PROP(path_str, "opcodes", uc_opcodes, size);

		local_load_ucore(node, uc_mask, (unsigned int *)uc_opcodes, num_opcodes);

		nlm_free(uc_opcodes);
		count++;
	}
}

#define POE_NODE "/soc/nae-cfg/poe/regs"
#define GET_POE_PROP(path_str, prop, buf, len)					\
	copy_fdt_prop(fdt, path_str, prop, PROP_CELL, buf, len)

#define POE_MAX_LOC_32BIT_CHUNKS 192


#define NUM_VCS_PER_CPU 4

#define NUM_DISTVEC_CELLS 	16
#define MIN_DIST_VEC 0
#define MAX_DIST_VEC 16

#define NUM_DISTVEC_CPUMASKS 4

#define POE_DIST_VEC0 0x100

/**
* @brief nlm_hal_init_poe_distvec function can be used to set the POE distribution vectors.
*
* @param [in] node Node number
* @param [in] vec Distribution vector number to program
* @param [in] cm0 CPU mask for CPUs 0-7
* @param [in] cm1 CPU mask for CPUs 8-15
* @param [in] cm2 CPU mask for CPUs 16-23
* @param [in] cm3 CPU mask for CPUs 24-31
* @param [in] vcmask VC mask for all CPUs
*
* @return
* 	- 0 on success
* 	- -1 on invalid input
* 
* @ingroup hal_nae
*
*/
int nlm_hal_init_poe_distvec(int node,int vec, uint32_t cm0, uint32_t cm1,
	uint32_t cm2, uint32_t cm3, uint32_t vcmask)
{
	uint32_t cpumasks[NUM_DISTVEC_CPUMASKS];
	uint32_t distvec[NUM_DISTVEC_CELLS];
	int i;
	int cpu;

	if (vec < MIN_DIST_VEC || vec >= MAX_DIST_VEC)
		return -1;

	cpumasks[0] = cm0;
	cpumasks[1] = cm1;
	cpumasks[2] = cm2;
	cpumasks[3] = cm3;

	vcmask &= 0xf;
	if (!vcmask)
		return -1;

	/* Initialize distribution vector cells */
	for (i = 0; i < NUM_DISTVEC_CELLS; i++) {
		distvec[i] = 0;
	}

	for (i = 0; i < NUM_DISTVEC_CPUMASKS; i++) {
		uint32_t cpumask = cpumasks[i];

		for (cpu = 0; cpu < 32; cpu++) {
			int cell, offset;
			uint32_t value;
			int vc = 0;
			int gcpu = 0;

			if (((1 << cpu) & cpumask) == 0)
				continue;

			/* Use global cpu id */
			gcpu = cpu + (i * 32);
			vc = (gcpu * NUM_VCS_PER_CPU) % (NUM_DISTVEC_CELLS * 32);

			cell = vc / 32;
			offset = vc % 32;

			value = vcmask << offset;

			distvec[cell] |= value;
		}
	}

	nlm_disable_distribution(node);

	/* Write distribution vector cells */
	for (i = 0; i < NUM_DISTVEC_CELLS; i++) {
		int reg_index;
		uint32_t value;

		reg_index = POE_DIST_VEC0 + (vec * NUM_DISTVEC_CELLS)
		            + (NUM_DISTVEC_CELLS - 1 - i);
		value = distvec[i];

		nlm_print("Node %d POE DistVec[%d]: reg=%d value=%08x\n",
		          node, vec, (NUM_DISTVEC_CELLS - 1 - i), value);
		nlm_hal_write_poe_pcim_reg(node, reg_index, value);
	}

	nlm_enable_distribution(node);

	return 0;
}

static void init_poe_enq_deq_spill(int node, uint64_t val, int len)
{
	uint64_t enq_val, deq_val;
	uint32_t enq_low, enq_high, deq_low, deq_high, each_reg_len;
	uint32_t enq_reg_high, enq_reg_low, enq_reg_len, deq_reg_low, deq_reg_high, deq_reg_len;
	int i;

	each_reg_len = len/16;
	enq_val = val;
	deq_val = val + each_reg_len;

	enq_reg_low = POE_CL0_ENQ_SPILL_BASE_L;
	enq_reg_high = POE_CL0_ENQ_SPILL_BASE_H;
	enq_reg_len = POE_CL0_ENQ_SPILL_MAXLINE;

	deq_reg_low = POE_CL0_DEQ_SPILL_BASE_L;
	deq_reg_high = POE_CL0_DEQ_SPILL_BASE_H;
	deq_reg_len = POE_CL0_DEQ_SPILL_MAXLINE;

	for(i = 0 ;i < 8; i++) {
		enq_low = (enq_val & 0xffffffff);
		enq_high = ((enq_val >> 32) & 0xff);
		deq_low = (deq_val & 0xffffffff);
		deq_high = ((deq_val >> 32) & 0xff);

		nlm_hal_write_poe_pcie_reg(node, enq_reg_low, enq_low);
		nlm_hal_write_poe_pcie_reg(node, enq_reg_high, enq_high);	
		nlm_hal_write_poe_pcie_reg(node, enq_reg_len, each_reg_len);

		nlm_hal_write_poe_pcie_reg(node, deq_reg_low, enq_low);	
		nlm_hal_write_poe_pcie_reg(node, deq_reg_high, enq_high);	
		nlm_hal_write_poe_pcie_reg(node, deq_reg_len, each_reg_len);

		enq_reg_low++;
		enq_reg_high++;
		deq_reg_low++;
		deq_reg_high++;
		enq_val += (2 * each_reg_len);
		deq_val += (2 * each_reg_len);
	}
}
/**
* @brief init_poe_loc_msg_storage function is used to initailize the POE local message storage.
*
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void init_poe_loc_msg_storage(int node)
{
	int i;
	int reg_id = LOCAL_FBP_BASE;
	for (i = 0; i < POE_MAX_LOC_32BIT_CHUNKS; ++i) {
		/* Selects 32 entries via bit mask */
		nlm_hal_write_poe_pcim_reg(node, reg_id, 0xffffffff);
		reg_id++;
	}
}

int nlm_clear_poe_stats(int node)
{
	nlm_hal_write_poe_pcie_reg(node, OO_MSG_CNT_LO, 0);
	nlm_hal_write_poe_pcie_reg(node, IN_ORDER_MSG_CNT_LO, 0);
	nlm_hal_write_poe_pcie_reg(node, LOC_BUF_STOR_CNT_LO, 0);
	nlm_hal_write_poe_pcie_reg(node, EXT_BUF_STOR_CNT_LO, 0);
	nlm_hal_write_poe_pcie_reg(node, LOC_BUF_ALLOC_CNT_LO, 0);
	nlm_hal_write_poe_pcie_reg(node, EXT_BUF_ALLOC_CNT_LO, 0);
	return 0;
}

int nlm_enable_poe_statistics(int node) 
{
	uint32_t val;
	nlm_clear_poe_stats(node);
	
	val = nlm_hal_read_poe_pcie_reg(node, POE_STATISTICS_EN);
	val |= (1 << 1);
	nlm_hal_write_poe_pcie_reg(node, POE_STATISTICS_EN, val);
	return 0;
}

int nlm_disable_poe_statistics(int node)
{
        uint32_t val;

        val = nlm_hal_read_poe_pcie_reg(node, POE_STATISTICS_EN);
        val &= ~(1 << 1);
        nlm_hal_write_poe_pcie_reg(node, POE_STATISTICS_EN, val);
        return 0;
}

int nlm_read_poe_statistics(int node, struct poe_statistics *stats)
{
	if (stats == NULL)
		return -1;

	stats->ooo_msg_count = nlm_hal_read_poe_pcie_reg(node, OO_MSG_CNT_LO) |
				((uint64_t)nlm_hal_read_poe_pcie_reg(node, OO_MSG_CNT_HI) << 32ULL);
	stats->inorder_msg_count = nlm_hal_read_poe_pcie_reg(node, IN_ORDER_MSG_CNT_LO ) |
                                ((uint64_t)nlm_hal_read_poe_pcie_reg(node, IN_ORDER_MSG_CNT_HI) << 32ULL);
	stats->loc_stor_access_count = nlm_hal_read_poe_pcie_reg(node, LOC_BUF_STOR_CNT_LO ) |
                                ((uint64_t)nlm_hal_read_poe_pcie_reg(node, LOC_BUF_STOR_CNT_HI) << 32ULL);
	stats->ext_stor_access_count = nlm_hal_read_poe_pcie_reg(node, EXT_BUF_STOR_CNT_LO) |
                                ((uint64_t)nlm_hal_read_poe_pcie_reg(node, EXT_BUF_STOR_CNT_HI) << 32ULL);
	stats->loc_stor_alloc_count = nlm_hal_read_poe_pcie_reg(node, LOC_BUF_ALLOC_CNT_LO) |
                                ((uint64_t)nlm_hal_read_poe_pcie_reg(node, LOC_BUF_ALLOC_CNT_HI) << 32ULL);
	stats->ext_stor_alloc_count = nlm_hal_read_poe_pcie_reg(node, EXT_BUF_ALLOC_CNT_LO) |
                                ((uint64_t)nlm_hal_read_poe_pcie_reg(node, EXT_BUF_ALLOC_CNT_HI) << 32ULL);

	return 0;
}

#define NUM_DIST_VEC 		16
#define NUM_WORDS_PER_DV 	16
#define MAX_DV_TBL_ENTRIES (NUM_DIST_VEC * NUM_WORDS_PER_DV)

/**
* @brief parse_poe_config function is used to initailize and program the POE based on the configuration in FDT.
*
* @param [in] fdt Pointer to the FDT
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void parse_poe_config(void *fdt,int node)
{
	int size = 0, nodeoffset, plen, bypass = 1, dist_en = 1, stats_en = 0;
	uint32_t *dist_vec;
	char mode_str[20];
	char path_str[50];
	char dv_str[10];
	uint32_t *pval;
	int reg_id, i, offset;
	uint32_t drop_timer = 0, dist_drop_enable = 0, class_drop_enable = 0, dest_threshold = POE_DIST_THRESHOLD_VAL;
	uint32_t dist_threshold[4] = {POE_DIST_THRESHOLD_VAL , POE_DIST_THRESHOLD_VAL, 
					POE_DIST_THRESHOLD_VAL , POE_DIST_THRESHOLD_VAL};
	uint64_t  poe_address_map;
	uint32_t *p_addr_map, *p_addr_len, poe_address_len;
	int addr_map_len;

	size = sizeof(uint32_t) * MAX_DV_TBL_ENTRIES;

	dist_vec = nlm_malloc(size);
	if (dist_vec == NULL) {
		nlm_print("nlm_alloc failed");
		return;
	}

	sprintf(path_str, "/soc/nae@node-%d/poe", node);

	nodeoffset = fdt_path_offset(fdt, path_str);
	if (nodeoffset >= 0) {
		pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "distribution-enable", &plen);
		if (pval != NULL) {
			dist_en = fdt32_to_cpu(*(uint32_t *)(pval));
		}
		else {
			dist_en = 1;
		}

                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "dist-drop-enable", &plen);
                if (pval != NULL) {
                        dist_drop_enable = fdt32_to_cpu(*(uint32_t *)(pval)) & 0xFFFF;
                }

                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "class-drop-enable", &plen);
                if (pval != NULL) {
                        class_drop_enable = fdt32_to_cpu(*(uint32_t *)(pval)) & 0xFF;
                }

                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "drop-timer", &plen);
                if (pval != NULL) {
                        drop_timer = fdt32_to_cpu(*(uint32_t *)(pval)) & 0xFFFF;
                }

		pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "dest-threshold", &plen);
                if (pval != NULL) {
                        dest_threshold = fdt32_to_cpu(*(uint32_t *)(pval)) & 0xFFFF;
                }

                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "statistics-enable", &plen);
                if (pval != NULL) {
                        stats_en = fdt32_to_cpu(*(uint32_t *)(pval));
                }
                else {
                        stats_en = 0;
                }

		p_addr_map = (uint32_t *)fdt_getprop(fdt, nodeoffset, "poe-spill-address", &addr_map_len);
		p_addr_len = (uint32_t *)fdt_getprop(fdt, nodeoffset, "poe-spill-length", &plen);

		if ((p_addr_map != NULL) && (p_addr_len != NULL)) {
			poe_address_map = fdt64_to_cpu(*(uint32_t *)(p_addr_map));
			poe_address_len = fdt64_to_cpu(*(uint32_t *)(p_addr_len));

			if (poe_address_map > 0 && poe_address_len > 0)
				init_poe_enq_deq_spill(node, poe_address_map, poe_address_len);
		}
	
		nlm_print("distribution %d dest_threshold 0x%x drop_timer 0x%x\n", dist_en, dest_threshold, drop_timer);	
		copy_fdt_prop(fdt, path_str, "mode", PROP_STR, mode_str, 20);
		if (strcmp(mode_str, "bypass") != 0) { /* Non bypass mode */
			bypass = 0;
			/* Local Message Storage (6K): Always Enabled! */
			init_poe_loc_msg_storage(node);
			NAE_DEBUG("Finished POE mode %s memory  carving\n", mode_str);
		} else {
			nlm_print("Configuring POE in bypass mode\n");
		}

		if (dist_en == 1) {

			pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "dist-threshold", &plen);
                        if (pval != NULL) {
                                for(i = 0; i < 4; i++)
                                        dist_threshold[i] = fdt32_to_cpu(*(uint32_t *)(pval + i)) & 0xFFFF;
                        }

			sprintf(path_str, "/soc/nae@node-%d/poe/distribution_vectors", node);
			nodeoffset = fdt_path_offset(fdt, path_str);
			if (nodeoffset >= 0) {
				for(i = 0; i < NUM_DIST_VEC; i++) {
					sprintf(dv_str,"dv%d",i);

					pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, dv_str, &plen);
					if (pval != NULL) {
						if ((plen / sizeof(uint32_t)) != NUM_WORDS_PER_DV) {
							nlm_print("Number of words in distribution vector%d is wrong\n",i);
						}	
						else {
							for(offset = 0; offset < NUM_WORDS_PER_DV; offset++) {
								*(dist_vec + (i * NUM_WORDS_PER_DV) + offset) = (fdt32_to_cpu(*(uint32_t *)(pval+ offset)));	
							}
						}
					}	
					else {
						nlm_print("Distribution vector configuration is wrong\n");
						while(1);
					}
				}		
			}
			else {
				/* set default DV */
			}
		}	
	}
	
	if (dist_en == 1) {
		nlm_disable_distribution(node);
		reg_id = POE_DIST_VEC0;
		for(i=0; i < NUM_DIST_VEC; i++) {
			for(offset = 0; offset < NUM_WORDS_PER_DV; offset++) {
				nlm_hal_write_poe_pcim_reg(node, reg_id, *(dist_vec + (i * NUM_WORDS_PER_DV) + offset));
				reg_id++;
			}
		}

        	for (i=0; i< 4; ++i) {
                	nlm_hal_write_poe_pcie_reg(node, (i + POE_DIST_THRESHOLD_0), dist_threshold[i]);
	        }

		nlm_hal_write_poe_pcie_reg(node, POE_DEST_THRESHOLD, dest_threshold);
		nlm_enable_distribution(node);

		if (dist_drop_enable)
			nlm_enable_distribution_vector_drop(node, dist_drop_enable);
		if (class_drop_enable)
			nlm_enable_distribution_class_drop(node, class_drop_enable);

		nlm_write_distvec_drop_timer(node, drop_timer);
	}

	if (stats_en == 1)
		nlm_enable_poe_statistics(node);

	nlm_hal_write_poe_pcie_reg(node, POE_LOC_ALLOC_EN, 1);

	if (!bypass) {
		nlm_hal_write_poe_pcie_reg(node, POE_FBP_SP_EN, 0x1);
		nlm_hal_write_poe_pcie_reg(node, POE_EXT_ALLOC_EN, 1);
	}
	nlm_hal_write_poe_pcie_reg(node, POE_TX_TIMER, 0x3);
	
	nlm_free(dist_vec);
}

#define GET_CPU_PROP(node_str, prop, buf, len)				\
	copy_fdt_prop(fdt, node_str, prop, PROP_CELL, buf, len)

/**
* @brief parse_fdt_cpu_config function is used to initailize local structures holding CPU-NAE related information.
*
* @param [in] fdt Pointer to the FDT
* @param [in] dom_id Domain number to parse
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void parse_fdt_cpu_config(void *fdt, int dom_id, nlm_nae_config_ptr nae_cfg)
{
	char node_str[32];
	sprintf(node_str, "/doms/dom@%d/cpu", dom_id);

	if (GET_CPU_PROP(node_str, "nae-rx-vc", &(nae_cfg->rx_vc), sizeof(uint32_t)) < 0) {
		nlm_print("Unable to parse nae_rx_vc, using defaults\n");
		goto out;
	}

	if (GET_CPU_PROP(node_str, "nae-fb-vc", &(nae_cfg->fb_vc), sizeof(uint32_t)) < 0) {
		nlm_print("Unable to parse nae_rx_vc, using defaults\n");
		goto out;
	}
 out:
	return;
}

int rely_on_firmware_config = 0;

#define NAE_NODE "/soc/nae-cfg"
#define NAE_GLOBAL_NODE "/soc/nae-cfg/global-nae-regs"

#define GET_NAE_PROP(path_str, prop, buf, len)					\
	copy_fdt_prop(fdt, path_str, prop, PROP_CELL, buf, len)

#define GET_NAE_STR_PROP(path_str, prop, buf, len)					\
	copy_fdt_prop(fdt, path_str, prop, PROP_STR, buf, len)

#define GET_NAE_GLOBAL_PROP(path_str, prop, buf, len)					\
	copy_fdt_prop(fdt, NAE_GLOBAL_NODE, prop, PROP_CELL, buf, len)

#define MAX_PROP_LEN 30

/**
* @brief config_egress_fifo_carvings function is used to carve the various egress FIFOs for the active contexts.
*
* @param [in] node Node number
* @param [in] start_ctxt Starting context number
* @param [in] num_ctxts Number of contexts to carve for
* @param [in] nae_cfg nlm_nae_config_ptr pointer to the internal HAL nae_cfg structure
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void config_egress_fifo_carvings(int node, int start_ctxt, int num_ctxts, nlm_nae_config_ptr nae_cfg)
{
	int i, limit = start_ctxt + num_ctxts;
	uint32_t data = 0;
	uint32_t start = 0, size, offset;

	/* Stage 2 FIFO */
	start = nae_cfg->stg2fifo_base;
	for(i=start_ctxt;i<limit;i++) {
		size   = nlm_stg2_fifo_sz() / nae_cfg->num_contexts;
		if(size) offset = size -1; else offset = size ;
		if(offset > max_stg2_offset())
			offset = max_stg2_offset();
		data =   offset << 23  | 
			start << 11 |	
			i << 1      |
			1;	
		NAE_DEBUG("program_nae_stg2_pmem: ctxt_num:%d start:%d size:%d\n", i, start, size);
		nlm_hal_write_nae_reg(node, STG2_PMEM_PROG, data);
		start+= size;
	}
	nae_cfg->stg2fifo_base = start;

	/* EH FIFO */
	start  = nae_cfg->ehfifo_base;	
	for(i=start_ctxt;i<limit;i++){
		size   = nlm_eh_fifo_sz() / nae_cfg->num_contexts;
		if(size) offset = size -1; else offset = size ;
		if(offset > max_eh_offset())
			offset = max_eh_offset();
		data =   offset << 23  | 
			start << 11 |	
			i << 1      |
			1;	
		NAE_DEBUG("program_nae_eh_pmem: ctxt_num:%d start:%d size:%d\n", i, start, size);
		nlm_hal_write_nae_reg(node, EH_PMEM_PROG, data);
		start+= size;
	}
	nae_cfg->ehfifo_base = start;

	/* FROUT FIFO */
	start  = nae_cfg->froutfifo_base;	
	for(i=start_ctxt;i<limit;i++){
		size   = nlm_frout_fifo_sz() / nae_cfg->num_contexts;
		if(size) offset = size -1; else offset = size ;
		if(offset > max_free_out_offset())
			offset = max_free_out_offset();
		data =   offset << 23  | 
			start << 11 |	
			i << 1      |
			1;	
		NAE_DEBUG("program_nae_frout_pmem: ctxt_num:%d start:%d size:%d\n", i, start, size);
		nlm_hal_write_nae_reg(node, FREE_PMEM_PROG, data);
		start+= size;
	}
	nae_cfg->froutfifo_base = start;

	/* MS FIFO */
	start  = nae_cfg->msfifo_base;	
	for(i=start_ctxt;i<limit;i++){
		size   = nlm_ms_fifo_sz() / nae_cfg->num_contexts;
		if(size) offset = size -1; else offset = size ;
		if(offset > max_ms_offset())
			offset = max_ms_offset();
		data =   offset << 22  | 	
			start << 11 |	
			i << 1      |
			1;	
		NAE_DEBUG("program_nae_ms_pmem: ctxt_num:%d start:%d size:%d\n", i, start, size);
		nlm_hal_write_nae_reg(node, STR_PMEM_CMD, data);
		start+= size;
	}
	nae_cfg->msfifo_base = start;

	/* PKT FIFO */
	start  = nae_cfg->pktfifo_base;	
	for(i=start_ctxt;i<limit;i++){
		size   = nlm_pkt_fifo_sz() / nae_cfg->num_contexts;
		if(size) offset = size -1; else offset = size ;
		if(offset > max_pmem_offset())
			offset = max_pmem_offset();
		nlm_hal_write_nae_reg(node, TX_PKT_PMEM_CMD1, offset);

		data =   start << 11 |	
			i << 1      |
			1;	
		NAE_DEBUG("program_nae_pkt_pmem: ctxt_num:%d size:%d\n", i, size);
		nlm_hal_write_nae_reg(node, TX_PKT_PMEM_CMD0, data);
		start+= size;
	}
	nae_cfg->pktfifo_base = start;

	/* PKT LEN FIFO */
	start  = nae_cfg->pktlenfifo_base;	
	for(i=start_ctxt;i<limit;i++){
		size   = nlm_pktlen_fifo_sz() / nae_cfg->num_contexts;
		if(size) offset = size -1; else offset = size ;
		data =   offset  << 22 |	
			start << 11 |	
			i << 1      |
			1;	
		NAE_DEBUG("program_nae_plen_pmem: ctxt_num:%d size:%d\n", i, size);
		nlm_hal_write_nae_reg(node, TX_PKTLEN_PMEM_CMD, data);
		start+= size;
	}
	nae_cfg->pktlenfifo_base = start;
}

/**
* @brief config_egress_fifo_credits function is used to assign credits to the various stages in the egress path.
*
* @param [in] node Node number
* @param [in] start_ctxt Starting context number
* @param [in] num_ctxts Number of contexts to assign for
* @param [in] max_context The maximum context number for the device
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void config_egress_fifo_credits(int node, int start_ctxt, int num_ctxts, int max_context)
{
	int i, limit = start_ctxt + num_ctxts;
	uint32_t data, credit, max_credit;

	/* Stage1 -> Stage2 */
	max_credit = max_stg2_offset() + 1;
	for(i=start_ctxt;i<limit;i++){
		credit = stg1_2_credit() / max_context;
		if (credit > max_credit)
			credit = max_credit;

		data =   credit << 16  | 
			i << 4      |
			1;	
		NAE_DEBUG("program_nae_stg1_2_stg2_credit: ctxt_num:%d credit:%d\n", i, credit);
		nlm_hal_write_nae_reg(node, STG1_STG2CRDT_CMD, data);
	}

	/* Stage2 -> EH */
	max_credit = max_eh_offset() + 1;
	for(i=start_ctxt;i<limit;i++){
		credit = stg2_eh_credit() / max_context;
		if (credit > max_credit)
			credit = max_credit;

		data =   credit << 16  | 
			i << 4      |
			1;	
		NAE_DEBUG("program_nae_stg2_2_eh_credit: ctxt_num:%d credit:%d\n", i, credit);
		nlm_hal_write_nae_reg(node, STG2_EHCRDT_CMD, data);
	}

	/* Stage2 -> Frout */
	max_credit = max_free_out_offset() + 1;
	for(i=start_ctxt;i<limit;i++){
		credit = stg2_frout_credit() / max_context;
		if (credit > max_credit)
			credit = max_credit;

		data =   credit << 16  | 
			i << 4      |
			1;	
		NAE_DEBUG("program_nae_stg2_2_frout_credit: ctxt_num:%d credit:%d\n", i, credit);
		nlm_hal_write_nae_reg(node, STG2_FREECRDT_CMD, data);
	}

	/* Stage2 -> MS */
	max_credit = max_ms_offset() + 1;
	for(i=start_ctxt;i<limit;i++){
		credit = stg2_ms_credit() / max_context;
		if (credit > max_credit)
			credit = max_credit;

		data =   credit << 16  | 
			i << 4      |
			1;	
		NAE_DEBUG("program_nae_stg2_2_ms_credit: ctxt_num:%d credit:%d\n", i, credit);
		nlm_hal_write_nae_reg(node, STG2_STRCRDT_CMD, data);
	}
}

#define NUM_INGRESS_PORTS 19
/**
* @brief context_to_port_channel function finds the interface number for a given context.
*
* @param [in] node Node number
* @param [in] context Context number
*
* @return
*  - Interface number
* 
* @ingroup hal_nae
*
*/
static uint32_t context_to_port_channel(int node, uint32_t context)
{
	uint32_t data, i;
	uint32_t rx_if_base_config, rx_if_base_config1, base, ctxt;

	if (is_nlm_xlp3xx()) {
		ctxt  = context - XLP_3XX_NET_TX_VC_BASE;
	}
	else if (is_nlm_xlp2xx()) {
		ctxt  = context - XLP_2XX_NET_TX_VC_BASE;
	}
	else {
		ctxt  = context - XLP_NET_TX_VC_BASE;
	}

        /* Set it to non-existent port */
	data = 19 << 10 ;             
	for(i=0;i<NUM_INGRESS_PORTS;i++){
		base = nlm_hal_read_nae_reg(node, RX_IF_BASE_CONFIG_0 + i/2);

		rx_if_base_config = base & 0xffff;
		rx_if_base_config1 = (base >> 16) & 0xffff;

		if( (i == 19) && (ctxt >= rx_if_base_config) ) {
			data = (rx_if_base_config << 10) |
				(ctxt - rx_if_base_config);
		} else if( (ctxt >= rx_if_base_config) && (ctxt < rx_if_base_config1) ) {
			data = (i << 10) |
				(ctxt - rx_if_base_config);
			break;
		}
	}
	return data;
}

#define NUM_EGRESS_PORTS 18
#define TX_IF_BURST_MAX  2048
#define DRR_QUANTA       2048
#define SP_EN            0
#define SP_NUM           0

/**
* @brief config_egress_drr function configures the egress DRR scheduler.
*
* @param [in] node Node number
* @param [in] start_ctxt Starting context number
* @param [in] end_ctxt Ending context number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
void config_egress_drr(int node, int start_ctxt, int end_ctxt)
{
	uint32_t data, context, port, channel;
	uint32_t limit = end_ctxt + 1;

	for (port=0; port<NUM_EGRESS_PORTS; port++) {
		data  = (TX_IF_BURST_MAX << 12) |
			(port    << 4) |
			1;
		nlm_hal_write_nae_reg(node, TX_IFACE_BURSTMAX_CMD, data);
	}

	for (context = start_ctxt; context<limit; context++){

		data = DRR_QUANTA;
		nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD1, data);

		data = context_to_port_channel(node, context);
		port = data >> 10;
		channel   = data & 0xff;

		data   = channel << 20 |
			port    << 15 |
			context << 5  |
			SP_EN << 4        |
			SP_NUM << 1    |
			1;
		NAE_DEBUG("context:%d port:%d channel:%d \n",
			       context, port, channel);
		nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD0, data);
	}
}

/**
* @brief config_egress function configures the egress NAE path (fifo carvings, fifo credits) per port
*
* @param [in] node Node number
* @param [in] context_base Starting context number for port
* @param [in] port Logical interface number (as opposed to hardware interface number)
* @param [in] nae_cfg nlm_nae_config_ptr pointer to the internal HAL nae_cfg structure
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/

static void config_egress(int node, int context_base, int port, nlm_nae_config_ptr nae_cfg)
{
	unsigned int offset =0, data = 0;

	data  = (TX_IF_BURST_MAX << 12) | (nae_cfg->ports[port].hw_port_id << 4) | 1;
        nlm_hal_write_nae_reg(node, TX_IFACE_BURSTMAX_CMD, data);

	data  = ((context_base + nae_cfg->ports[port].num_channels - 1) << 22) | (context_base << 12) |
		 (nae_cfg->ports[port].hw_port_id << 4) | 1 ;
	nlm_hal_write_nae_reg(node, TX_DRR_ACTVLIST_CMD, data);
	
	config_egress_fifo_carvings(node, context_base, nae_cfg->ports[port].num_channels, nae_cfg);
	config_egress_fifo_credits (node, context_base, nae_cfg->ports[port].num_channels, nae_cfg->num_contexts);

	data = nlm_hal_read_nae_reg(node, DMA_TX_CREDIT_TH);
	data |= (1 << 25) | (1 << 24);
	nlm_hal_write_nae_reg(node, DMA_TX_CREDIT_TH, data);

	switch (nae_cfg->ports[port].iftype)
	{
		case INTERLAKEN_IF:
                        data  = (64 << 12) | (nae_cfg->ports[port].hw_port_id << 4) | 1;
                        nlm_hal_write_nae_reg(node, TX_IFACE_BURSTMAX_CMD, data);

			for(offset = 0; offset < nae_cfg->ports[port].num_channels; offset++) {
				nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD1, DRR_QUANTA);
				data = (nae_cfg->ports[port].hw_port_id << 15) |
					 (offset << 20) | ((context_base + offset) << 5) ;
				nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD0, data | 1 );
				nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD0, data);
				NAE_DEBUG("txsched %x \n",nlm_hal_read_nae_reg(node, TX_SCHED_MAP_CMD0)); 
			}
			break;
		default:
                        for (offset=0;offset < nae_cfg->ports[port].num_channels; offset++) {
				NAE_DEBUG("txsched hwport %d ctxt %d \n",nae_cfg->ports[port].hw_port_id , (context_base + offset));
				nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD1, DRR_QUANTA);
				data =  (nae_cfg->ports[port].hw_port_id << 15) | ((context_base + offset) << 5) ;
				nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD0 , data | 1 );
				nlm_hal_write_nae_reg(node, TX_SCHED_MAP_CMD0 , data);
			       NAE_DEBUG("txsched %x \n",nlm_hal_read_nae_reg(node, TX_SCHED_MAP_CMD0));
                        }
			break;
	}
}


static int get_flow_mask(int num_ports)
{
        int i;
        int max_bits = 5; /* upto 32 ports */
        /* Compute the number of bits to needed to
         * represent all the ports */
        for (i = 0; i < max_bits; ++i) {
                if (num_ports <= (2 << i)) {
                        return i + 1; /* num bits is non zero and 'i' starts from 0 */
                }
        }
        return max_bits;
}

/* Flow Config */
static void nlm_config_flow_base(int node, nlm_nae_config_ptr nae_cfg)
{
        int port = 0, hw_port = 0;
        int flow_mask = get_flow_mask(nae_cfg->num_ports);
        uint32_t reg, cur_flow_base = 0, max_ports;
	uint32_t per_port_num_flows = XLP_MAX_FLOWS / nae_cfg->num_ports;

	max_ports = nlm_get_max_ports();
	for (port = 0, hw_port = 0 ; port < nae_cfg->num_ports; hw_port++) { 
            if (hw_port == nae_cfg->ports[port].hw_port_id) {
		reg = (cur_flow_base << 16) | hw_port;
		reg |= ((flow_mask & 0x1f) << 8);
	        nlm_hal_write_nae_reg(node, FLOW_BASE_MASK_CFG, reg);
        	cur_flow_base += per_port_num_flows;
		port++;
	    }	   
	}	
}


static void nlm_config_rx_calendar(int node, nlm_nae_config_ptr nae_cfg)
{
        int cal = 0, cal_len = 0, last_free = 0, port = 0;
        uint32_t val = 0, max_ports;

	max_ports = nlm_get_max_ports();
        cal_len = nae_cfg->rx_cal_slots - 1;

        NAE_DEBUG("Rx calendar length %d \n", cal_len);
        do {
                if (cal >= MAX_CAL_SLOTS)
                        break;
                last_free = cal;
                for(port = 0; port < max_ports; port++) {
                        if (nae_cfg->ports[port].rx_slots_reqd) {
                                val = (cal_len << 16) | (nae_cfg->ports[port].hw_port_id << 8) | cal;
                                nlm_hal_write_nae_reg(node, RX_IFACE_SLOT_CAL, val);
                                NAE_DEBUG("Rx cal: hwport %d cal %d rx val 0x%x\n",nae_cfg->ports[port].hw_port_id, cal,  val);
                                cal++;
                                nae_cfg->ports[port].rx_slots_reqd--;
                        }
                }
                if (last_free == cal)
                        break;
        } while(1);
}

static void nlm_config_tx_calendar(int node, nlm_nae_config_ptr nae_cfg)
{
        int cal = 0, cal_len = 0, last_free = 0, port = 0;
        uint32_t val = 0, max_ports;

	max_ports = nlm_get_max_ports();
	cal_len = nae_cfg->tx_cal_slots - 1;
	
	nlm_hal_write_nae_reg(node, EGR_NIOR_CAL_LEN_REG, cal_len);
        do {
                if (cal >= MAX_CAL_SLOTS)
                        break;
                last_free = cal;
                for(port = 0; port < max_ports; port++) {
                        if (nae_cfg->ports[port].tx_slots_reqd) {
                                val = (nae_cfg->ports[port].hw_port_id << 7) | (cal << 1) | 1;
                                nlm_hal_write_nae_reg(node, EGR_NIOR_CRDT_CAL_PROG, val);     /* map it to interface */
                                NAE_DEBUG("TX cal: hwport %d cal %d tx val 0x%x\n",nae_cfg->ports[port].hw_port_id, cal,  val);
                                cal++;
                                nae_cfg->ports[port].tx_slots_reqd--;
                        }
                }
                if (last_free == cal)
                        break;
        } while(1);

}

/*
Input:
node - nae node id 	
start - vfbid table index. 
num_entries - number of entries to be configured from start
vfbid_tbl - array of destination VCs
(Entries 126 and 127 can not be used by software)

Returns:
	0 on success
*/

int nlm_config_vfbid_table(int node, uint32_t start, uint32_t num_entries, uint32_t *vfbid_tbl)
{
	int vfbid;		
	volatile uint32_t val = 0;
	if (vfbid_tbl == NULL)
		return -1;
	
	if ((start + num_entries) > 128)
		return -1;

	for(vfbid = start; vfbid < (start + num_entries); vfbid++, vfbid_tbl++) {
		val = ((*vfbid_tbl) << 16) | (vfbid << 4) | 1;
		nlm_hal_write_nae_reg(node, VFBID_TO_DEST_MAP_CMD, val);		
		NAE_DEBUG("node %d vfbid %d *vfbid %d\n", node, vfbid, *vfbid_tbl);
	}
	return 0;	
}

static void nlm_config_poe_class(int node)
{
        int i;
        uint32_t val;
        int poe_cl_tbl[MAX_POE_CLASSES] = {0x0, 0x249249, 0x492492,
                                           0x6db6db, 0x924924, 0xb6db6d,
                                           0xdb6db6, 0xffffff};
        int max_poe_tbl_sz;

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
        	max_poe_tbl_sz = XLP3XX_MAX_POE_CLASS_CTXT_TBL_SZ;
        }
        else {
		max_poe_tbl_sz = MAX_POE_CLASS_CTXT_TBL_SZ;
	}

        NAE_DEBUG("max_poe_tbl_sz %d\n",max_poe_tbl_sz);
        for (i = 0; i <max_poe_tbl_sz; ++i) {
                val = (0x00 | i); /* clear the read bit 0x80 */
                val |= (poe_cl_tbl[(i/MAX_POE_CLASSES) & 0x7] << 8);

		/*  NAE_DEBUG("POE index %d val %x \n",i, val); */
                nlm_hal_write_nae_reg(node, POE_CLASS_SETUP_CFG, val);
        }
}

static inline void nlm_write_ucore_sprayvec(int node, int hw_port_id, uint32_t spray_vec)
{
	nlm_hal_write_nae_reg(node, UCORE_IFACE_MASK_CFG, ( 0x1ULL << 31) | ((spray_vec & 0xffff)  << 8) |
                                     (hw_port_id & 0x1f));
}

static inline void nlm_write_fifo_size(int node, int hw_port_id, uint32_t size)
{
	nlm_hal_write_nae_reg(node, FREE_IN_FIFO_UNIQ_SZ_CFG, ((size/XLP_CACHELINE_SIZE) << 8) | (hw_port_id & 0x1f) );
}

static inline void nlm_write_interface_fifo(int node, int hw_port_id, uint32_t start, uint32_t size, uint32_t xoff_thresh)
{
	volatile uint32_t val;
	
	val = ((xoff_thresh << 25) | ((size & 0x1ff) << 16) |
                                       ((start & 0xff) << 8) | hw_port_id);
        nlm_hal_write_nae_reg(node, IFACE_FIFO_CFG, val);

	NAE_DEBUG("iface fifo config port %d addr %d size %d\n",hw_port_id, start, size);
	NAE_DEBUG("reg %x 0x%x\n",IFACE_FIFO_CFG, val);
}
 
static inline void nlm_write_rxbase(int node, int hw_port_id, uint32_t base)
{
	volatile uint32_t val;
	int reg = RX_IF_BASE_CONFIG_0 + (hw_port_id / 2);
	
        if ((hw_port_id % 2) == 0) {
		val = nlm_hal_read_nae_reg(node, reg) & (0x3FF << 16);
                val |= base;
                nlm_hal_write_nae_reg(node, reg, val);
        }
        else {
                val = nlm_hal_read_nae_reg(node, reg) & 0x3FF;
                val |= (base << 16);
                nlm_hal_write_nae_reg(node, reg, val);
		NAE_DEBUG("reg %x 0x%x\n",reg, val);
        }
}

static inline void config_context_xoff_thr(int node, int thgrp, int maxbuf)
{
	/* xon & xoff threshold is based on the number of entries used 
	 send xon when rx_data_buffer_used <= xon threshold
	 send xoff when rx_data_buffer_used >= xoff threshold */
	int xoff = maxbuf / 2;
	int xon = maxbuf / 4 ;
	int val = (xoff << 3) | (xon << 17) | thgrp;
	static int done[NLM_MAX_NODES][NLM_NAE_MAX_XONOFF_THR_GRPS];
	if(thgrp >= NLM_NAE_MAX_XONOFF_THR_GRPS)
		return;
	if(done[node][thgrp])
		return;
	done[node][thgrp] = 1;
	nlm_hal_write_nae_reg(node, RX_BUFFER_XONOFF_THR, thgrp);
	nlm_hal_write_nae_reg(node, RX_BUFFER_XONOFF_THR, val);

	nlm_hal_write_nae_reg(node, RX_BUFFER_XONOFF_THR, (1 << 31) | thgrp);
	val = nlm_hal_read_nae_reg(node, RX_BUFFER_XONOFF_THR);
	NAE_DEBUG("thgrp %d xoff %d xon %d maxbuf %d\n", 
			thgrp, (val >> 3) & 0x3fff, (val >> 17) & 0x3fff, maxbuf);
}

static inline void nlm_configure_rxbuffer(int node, int context_base, int num_channels, uint32_t base, uint32_t size,int thrgrp)
{
	int offset;
	volatile uint32_t val;

	size /= num_channels;

	for(offset = 0; offset < num_channels; offset++) {
		nlm_hal_write_nae_reg(node, RX_BUFFER_BASE_DEPTH_ADDR_REG, (context_base + offset) | (thrgrp << 10));
                NAE_DEBUG("context %d base %x size %d \n",context_base + offset, base, size);
         	NAE_DEBUG("reg %x 0x%x\n",RX_BUFFER_BASE_DEPTH_ADDR_REG, context_base + offset);       

                NAE_DEBUG("rxbuffer context %d base %d size %d thrgrp %d\n",
				context_base + offset, base, size, thrgrp);
		val = 0x80000000 | ((base << 2) & 0x3fff); /* base */
                val |= (((size << 2)  & 0x3fff) << 16); /* size */
                nlm_hal_write_nae_reg(node, RX_BUFFER_BASE_DEPTH_REG, val);
		NAE_DEBUG("reg %x 0x%x\n",RX_BUFFER_BASE_DEPTH_REG, val);
                nlm_hal_write_nae_reg(node, RX_BUFFER_BASE_DEPTH_REG, 0x7fffffff & val);
		NAE_DEBUG("reg %x 0x%x\n",RX_BUFFER_BASE_DEPTH_REG, 0x7fffffff & val);
		base += size;
	}
}

static inline void nlm_configure_parserfifo(int node, int hw_port_id, uint32_t start, uint32_t size)
{
	volatile uint32_t val;
	uint32_t xon, xoff;

	val = ((size & 0x1fff) << 17) | ((start & 0xfff) << 5) | (hw_port_id & 0x1f);
        nlm_hal_write_nae_reg(node, PARSER_SEQ_FIFO_CFG, val);
	NAE_DEBUG("reg %x 0x%x\n",PARSER_SEQ_FIFO_CFG, val);

	/*
	   FIFO xoff threshold: high. When #entries used rises to this specified number.
	   FIFO xon threshold: low.  When #entries used fall to this specified number.
	   */
	if (size != 0)
	{
		xoff = size/2;
		xon = size/4;
		val = (xoff<<12) | (xon);
		nlm_hal_write_nae_reg(node, PARSER_SEQ_FIFOTH_CFG, val);

		NAE_DEBUG("reg %x 0x%x\n",PARSER_SEQ_FIFOTH_CFG, val);
	}
}

static void nlm_config_ingress_fifo(int node, nlm_nae_config_ptr nae_cfg)
{
        int fifo_xoff_thresh;
        int port, lane = 0, hw_port = 0, max_lanes, offset = 0;
	int cur_iface_start = 0, max_ports;
	uint32_t cur_parser_base = 0, context_base = 0, rx_buf_base = 0, size = 0;
	int thrgrp, rx_buf_size;

	max_ports = nlm_get_max_ports();
        NAE_DEBUG("Interface FIFO carving max_ports:%d \n", max_ports);
        for (port = 0, hw_port = 0 ; hw_port < max_ports; ) { 
	    if (hw_port == nae_cfg->ports[port].hw_port_id) {


		NAE_DEBUG("port:%d iftype:%d\n", port, nae_cfg->ports[port].iftype);
		thrgrp = 0;
		rx_buf_size = 0;
		switch(nae_cfg->ports[port].iftype) {
		   case RXAUI_IF:
			fifo_xoff_thresh = 12;
			max_lanes = 4;
			offset = 4;
			break;
		   case XAUI_IF:
			fifo_xoff_thresh = 12;
			max_lanes = 4;
			offset = 4;
			thrgrp = 1;
			rx_buf_size = nae_cfg->ports[port].rx_buf_size;
			break;
		   case SGMII_IF:
			fifo_xoff_thresh = 6;
			max_lanes = 1;
			offset = 1;
			break;
		   case INTERLAKEN_IF:
			fifo_xoff_thresh = 12;
			max_lanes = 8;
			offset = 8;
 			break;
		    default:
			fifo_xoff_thresh = 0;
                        max_lanes = 0;
                        offset = 0;
			break;
                }

		NAE_DEBUG("cfg rxbuffer n:%d port:%d ctx_base:%d ch:%d rx_buf_base:%X buf_sz:%d\n",
			node, hw_port, context_base, nae_cfg->ports[port].num_channels,
                                        rx_buf_base, nae_cfg->ports[port].rx_buf_size);

		if(nae_cfg->ports[port].vlan_pri_en && rx_buf_size)
			config_context_xoff_thr(node, thrgrp,  rx_buf_size / nae_cfg->ports[port].num_channels);
		else
			thrgrp = 0;

		nlm_configure_rxbuffer(node, context_base,nae_cfg->ports[port].num_channels,
                                        rx_buf_base, nae_cfg->ports[port].rx_buf_size, thrgrp);  
                rx_buf_base += nae_cfg->ports[port].rx_buf_size;


		for (lane = 0 ; lane < max_lanes; lane++) {
			/* carving interface fifo */
			NAE_DEBUG("carving intf fifo: lane:%d n:%d hw_port:%d\n", lane, node, hw_port);
			size = ((lane == 0) ? nae_cfg->ports[port].intf_fifo_size : 0);
			nlm_write_interface_fifo(node, nae_cfg->ports[port].hw_port_id + lane,
                                                         cur_iface_start, size, fifo_xoff_thresh );
			cur_iface_start += size;
			
			/* carving Rx base */
			NAE_DEBUG("carving rx buffer: lane:%d\n", lane);
			nlm_write_rxbase(node, nae_cfg->ports[port].hw_port_id + lane, context_base);
			if (lane == 0)
                        	context_base += nae_cfg->ports[port].num_channels;
			
			size = ((lane == 0) ? nae_cfg->ports[port].prsr_seq_fifo_size : 0);
			nlm_configure_parserfifo(node, nae_cfg->ports[port].hw_port_id + lane, cur_parser_base, size);
			cur_parser_base += size;
		}
		hw_port += offset;
		port++;
	    }
	    else {
		NAE_DEBUG("carving intf fifo;rx buffer: lane:%d n:%d port:%d\n", lane, node, hw_port);
		nlm_write_interface_fifo(node, hw_port, cur_iface_start, 0, 0 );
		nlm_write_rxbase(node, hw_port, context_base);
		nlm_configure_parserfifo(node, hw_port, cur_parser_base, 0);
		hw_port++;
	    }		
        }
}

static void nlm_config_nae_global(int node, nlm_nae_config_ptr nae_cfg) 
{
	uint32_t vfbid = 0, i = 0;
        volatile uint32_t val = 0;
        uint32_t dest;

	nlm_config_poe_class(node);

	if (nae_cfg->flags & FREEBACK_TO_NAE) {
                dest = nae_cfg->frin_queue_base;

                for (i = 0, vfbid = 32; i < NLM_MAX_NODES; vfbid++) {
                        val = (dest << 16) | (vfbid << 4) | 1;
                        nlm_hal_write_nae_reg(node, VFBID_TO_DEST_MAP_CMD, val);
          		/*  NAE_DEBUG("HW vfbid %d dest %d \n",vfbid, dest); */
                        dest++;
                        if (dest == (nae_cfg->frin_queue_base + nae_cfg->frin_total_queue)) {
                                i++;
                                dest = (i<<10) | nae_cfg->frin_queue_base;
                        }
                }
        }
	else {
		for (i = 0, vfbid = 0; i < NLM_MAX_NODES; i++) {
                        dest = nae_cfg->fb_vc;
                        for (;vfbid < ((i+1) * 32); vfbid++) { 
                                val = (((i<<10) | dest) << 16) | (vfbid << 4) | 1;
                                nlm_hal_write_nae_reg(node, VFBID_TO_DEST_MAP_CMD, val);
        			/*  NAE_DEBUG("vfbid %d dest %d \n",vfbid, ((i<<10) | dest)); */
                                dest += MAX_VC_PERTHREAD;
                        }
                }

	}

#if defined(NLM_HAL_UBOOT)
	if (0==nae_cfg->num_ports)
		return; /* if no interface ports, Uboot skip nae cfg */
	else
#endif
	nlm_hal_write_nae_reg(node, FLOW_CRC16_POLY_CFG, 0xFFFF);
	nlm_config_ingress_fifo(node, nae_cfg);
	nlm_config_rx_calendar(node, nae_cfg);
	if (!is_nlm_xlp8xx_ax())
		nlm_config_tx_calendar(node, nae_cfg);

	nlm_config_flow_base(node, nae_cfg); /* must be after one of the above functions. */
}

/**
* @brief set_nae_frequency function sets the frequency of the NAE block
*
* @param [in] node Node number
* @param [in] frequency Frequency in MHz
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void set_nae_frequency(int node, int frequency)
{
        const uint64_t mhz = 1000000;
        uint64_t set_freq, set_freq_d; 
	if(is_nlm_xlp2xx()){
		set_freq_d = nlm_hal_xlp2xx_set_clkdev_frq(node, XLP2XX_CLKDEVICE_NAE, frequency * mhz);
		NLM_HAL_DO_DIV(set_freq_d, mhz);
		nlm_print("NAE frequncy set to = %lluMhz", (unsigned long long)set_freq_d);
		return;
	}else{
        	/* Note that the DFS sets the NAE 2X frequency.
	         * To set the NAE frequency, multiply by 2
        	 */
		set_freq_d = nlm_hal_set_soc_freq(node, DFS_DEVICE_NAE_2X, frequency * 2 * mhz);
		NLM_HAL_DO_DIV(set_freq_d, mhz);
	        set_freq = set_freq_d;
		NLM_HAL_DO_DIV(set_freq, 2);
	        nlm_print("NAE 2X Frequency set to %lluMHz (NAE frequency %llu MHz)\n", 
					(unsigned long long)set_freq_d, (unsigned long long)set_freq);
	}
	return;

}

static int nae_freein_fifo_cfg(void *fdt, int node, nlm_nae_config_ptr nae_cfg)
{
	uint64_t tmp[2];
	uint64_t *pval;
	uint32_t reg, size, spillsz;
	int start = 0, i,  th_hi, th_lo;
	uint64_t spill_addr, spill_mem_addr, spill_mem_size; 
	int freein_fifo_total_queues = nae_cfg->frin_total_queue;
	char path_str[50];

	/* spill mem allocation is skipped if FREEIN_SPILL_DYNAMIC is set */
	
	if ((nae_cfg->flags & FREEIN_SPILL_DYNAMIC) == 0) {
	    sprintf(path_str,"/soc/nae@node-%d/freein-fifo-config", node);
	    if (copy_fdt_prop(fdt, path_str, "freein-fifo-spill-mem-range", PROP_STR, 
				tmp, sizeof(tmp)) == sizeof(tmp)) {
		pval = &tmp[0];
		spill_mem_addr = fdt64_to_cpu((*pval));
		spill_mem_size = fdt64_to_cpu((*(pval + 1)));
#ifdef NLM_HAL_LINUX_KERNEL
		if (spill_mem_addr == 0ULL) {
			if ((spill_mem_addr = nlm_spill_alloc(node, spill_mem_size)) == 0ULL) {
				nlm_print("free in spill mem allocation failed \n");
				spill_mem_size = 0ULL;
			}
			else {
				nae_cfg->flags |= FREEIN_SPILL_DYNAMIC;
			}
		}	
#endif	
	    } else {
		if (node == 0) {
			spill_mem_addr = XLP_FREEIN_SPILL_DEFAULT_MEM_ADDR;
			spill_mem_size = XLP_FREEIN_SPILL_DEFAULT_MEM_SIZE;
		}
		else {
			spill_mem_addr = 0ULL;
			spill_mem_size = 0ULL;
		}
	   }
	   nae_cfg->freein_spill_base = spill_mem_addr;
	   nae_cfg->freein_spill_size = spill_mem_size;	
	}
	else {
		spill_mem_addr = nae_cfg->freein_spill_base;
		spill_mem_size = nae_cfg->freein_spill_size;
	}

	nlm_print("NAE Freein-fifo, memaddr %lx memsize %lx onnchip-descs %d spill-descs %d\n",
			(long)spill_mem_addr, (long)spill_mem_size, 
			nae_cfg->freein_fifo_onchip_num_descs[0], nae_cfg->freein_fifo_spill_num_descs);

	nlm_print("onchip descs array:\n");
	for (i=0; i<MAX_NAE_FREEIN_DESCS_QUEUE; i++) {
		nlm_print(" %d", nae_cfg->freein_fifo_onchip_num_descs[i]);
	}
	nlm_print("\n");

	/* in cache addr */
	spill_addr = spill_mem_addr >> 6; 

	/* in cachelines, 1 cacheline can store 12 descs  and 
	get the number of cacheline required */
	if (spill_mem_size != 0) {
		spillsz = nae_cfg->freein_fifo_spill_num_descs / 12 ; 
		if((nae_cfg->freein_fifo_spill_num_descs % 12) != 0)
			spillsz++;
	}
	else
		spillsz = 0;	

	{
		for(i = 0; i < freein_fifo_total_queues; i++) {
			if(i < MAX_NAE_FREEIN_DESCS_QUEUE && (spillsz)) {

				reg = spill_addr & 0xffffffff;
				nlm_hal_write_nae_reg(node, FREE_SPILL0_MEM_CFG, reg);
				nlm_print("Freein fifo cfg %d spl_addr %lx reg0 %x\n", i, (long)spill_addr, reg);
	
				reg = (((spill_addr >> 32) & 0x3) << 30) | spillsz;
				nlm_hal_write_nae_reg(node, FREE_SPILL1_MEM_CFG, reg);

				nlm_print("Freein fifo cfg %d spl_addr %lx reg1 %x\n", i, (long)spill_addr, reg);
				spill_addr += spillsz;
				/* align to 32k, already >> 6 is done above, so align to 1k */
				spill_addr = (spill_addr + 1023) & (~1023);
			} else {
				nlm_hal_write_nae_reg(node, FREE_SPILL0_MEM_CFG, 0);
				nlm_hal_write_nae_reg(node, FREE_SPILL1_MEM_CFG, 0);
			}
		
			/* Onchip configuraton is based on locations. 1 location can store 2 descs */
			if(i < MAX_NAE_FREEIN_DESCS_QUEUE) {
				size = nae_cfg->freein_fifo_onchip_num_descs[i] / 2;

				reg = ((size  & 0x3ff ) << 20) | /* fcSize */
					((start & 0x1ff)  << 8) | /* fcStart */
					(i  & 0x1f);

				nlm_hal_write_nae_reg(node, FREE_IN_FIFO_CFG, reg);
				nlm_print("Freein fifo cfg %d fcstart %d size %d\n", i, start, size);
				start += size;
			}
		}
	}

	if(spillsz) {
		th_hi = 6; /* Defualt value */
		th_lo = 0xe; /* Default value */
		/* spill credits [27:24] has to be 2 */
		reg = (2 << 24) | th_lo | (th_hi << 12);
		nlm_hal_write_nae_reg(node, FREE_FIFO_THRESHOLD_CFG, reg); 

		if((spill_addr << 6) > (spill_mem_addr + spill_mem_size)) {
			nlm_print("ERROR : Spill address range overflow\n");
			return -1;
		}
	}
	
	return 0;
}


static int parse_vfbid_config(void *fdt, int node)
{
	char vfbid_path[50];
	int offset, dest, index;
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[node];
	uint32_t *pval;
	int plen = 0, nodeoffset = 0, hw_replenish;
	uint32_t *vfbid_tbl = nae_vfbid_tbl[node];

	sprintf(vfbid_path, "/soc/nae@node-%d/vfbid-config",node);

	if(GET_NAE_PROP(vfbid_path, "hw-replenish", &hw_replenish, sizeof(uint32_t)) < 0){
                /* nlm_print("fdt missing hw-replenish\n"); */
	}
	else {
		if (hw_replenish != 0)
			nae_cfg->flags |= FREEBACK_TO_NAE;
		else
			nae_cfg->flags &= ~FREEBACK_TO_NAE;
	}

	for(offset = 0; offset < MAX_VFBID_ENTRIES; offset++) {
		*(vfbid_tbl + offset) = 0;
	}
		
	nodeoffset = fdt_path_offset(fdt, vfbid_path);
        if(nodeoffset >= 0)  {
                pval = (uint32_t *)fdt_getprop(fdt, nodeoffset, "vfbid-map", &plen);
               	if(pval != NULL) {
			nae_cfg->flags |= VFBID_FROM_FDT;
			for(offset = 0; offset < (plen / 4); offset+=2) {
				index = fdt32_to_cpu(*(uint32_t *)(pval + offset));
				dest = fdt32_to_cpu(*(uint32_t *)(pval + offset + 1));
				*(vfbid_tbl + index) = dest;
			}
		}
        }
	
	return 0;
}

static inline int get_num_ports(int block, int intf_type)
{
	if (intf_type == SGMII_IF) {
		if (block < 4)
			return 4;
		else
			return 2;
	}
	else
		return 1;	
}

static inline int get_slots_required(int intf_type)
{
	if (intf_type == SGMII_IF)
		return SGMII_CAL_SLOTS;
	else if (intf_type == XAUI_IF)
		return XAUI_CAL_SLOTS;
	else if (intf_type == RXAUI_IF)
		return XAUI_CAL_SLOTS;
	else if (intf_type == INTERLAKEN_IF)	
		return ILK_CAL_SLOTS;
	else
		return 0;		
}

static inline int valid_context_number(uint32_t context)
{
	int max_context;

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx())
		max_context = XLP3XX_MAX_NAE_CONTEXTS;
	else
		max_context = MAX_NAE_CONTEXTS;
	return ((context > max_context) ? 0 : 1);
}

static inline int valid_calendar_slot(uint32_t slot)
{
        return ((slot > MAX_CAL_SLOTS) ? 0 : 1);
}

static int get_interface_type(void *fdt, char *nae_port_str, int block, int port, int node)
{
	int intf_type, offset;
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[node];
	char port_type_str[MAX_PROP_LEN];

	if (GET_PORT_STR_PROP("cpld", port_type_str, MAX_PROP_LEN) < 0) {
			nlm_print("FDT missing cpld param for complex. port detection could be supported %d\n", block);
	}
	if (!strcmp(port_type_str, "no")) {
		nlm_print("Detection from CPLD not supported. Reading configuration from sysconf for block %d\n", block);
		intf_type = DC_NOT_PRSNT;
        }else{
		intf_type = nlm_get_interface_type(node, block);
	}

#ifdef CONFIG_N511
	if (block == 4) {
		intf_type = DC_SGMII;
	}
	else {
		intf_type = DC_XAUI;
	}
#endif
	if (intf_type == DC_NOT_PRSNT) {
		if (GET_PORT_STR_PROP("mode", port_type_str, MAX_PROP_LEN) < 0) {
			nlm_print("FDT missing mode param for complex %d\n", block);
			intf_type = UNKNOWN_IF;
		}
		nlm_print("%s", port_type_str);
                if (!strcmp(port_type_str, "sgmii")) {
                        intf_type = DC_SGMII;
                }
                else if (!strcmp(port_type_str, "xaui")) {
                        intf_type = DC_XAUI;
                }
                else if (!strcmp(port_type_str, "rxaui")) {
                        intf_type = DC_RXAUI;
                }
                else if (!strcmp(port_type_str, "interlaken")) {
                        intf_type = DC_ILK;
                }
                else {
                        intf_type = UNKNOWN_IF;
                        return -1;
                }
        }
	else if (intf_type == DC_XAUI) {
		/* RXAUI & XAUI have the same intf_type value from the board CPLD */
		if (GET_PORT_STR_PROP("mode", port_type_str, MAX_PROP_LEN) < 0) {
			nlm_print("FDT missing mode param for complex %d\n", block);
			intf_type = UNKNOWN_IF;
			return -1;
		}
		if (!strcmp(port_type_str, "rxaui")) {
			intf_type = DC_RXAUI;
		}
	}

        switch(intf_type) {
                case DC_SGMII:
			nlm_print("Complex %d in SGMII mode\n", block);
			for(offset = 0; offset < MAX_PORTS_PERBLOCK; offset++) {
	                        nae_cfg->ports[port+offset].iftype = SGMII_IF;
			}
                        break;
#ifndef NLM_HAL_UBOOT /* Uboot only support SGMII interface only for management port */
        	case DC_ILK:/* DC_HIGIG */
			if (is_nlm_xlp2xx()) {
				nlm_print("Complex %d in XAUI_HIGIG mode\n", block);
                        	nae_cfg->ports[port].iftype = XAUI_IF;
                        	break;
			}	
			/* FIXME interlaken supported only on node-0 nae */
			if ((is_nlm_xlp3xx()) || (node != 0)) {
				nlm_print("Interlaken not supported \n");
				return -1;
			}
			nlm_print("Complex %d in interlaken mode\n", block);
                	if (is_nlm_xlp8xx()) {
                        	nae_cfg->ports[port].iftype = INTERLAKEN_IF;
			}
                        else {
                                nae_cfg->ports[port].iftype = UNKNOWN_IF;
                                nlm_print("Interlaken is not supported on this board \n");
                        }
			break;
                case DC_XAUI:
			nlm_print("Complex %d in xaui mode\n", block);
                        nae_cfg->ports[port].iftype = XAUI_IF;
                        break;
                case DC_RXAUI:
			nlm_print("Complex %d in rxaui mode\n", block);
                        nae_cfg->ports[port].iftype = RXAUI_IF;
                        break;
#endif
                default:
                        nae_cfg->ports[port].iftype = UNKNOWN_IF;
	}
	return nae_cfg->ports[port].iftype;
}

static void extract_complex_params(void *fdt, int intf_type, char *nae_port_str, struct nae_complex_config *cmplx)
{
	int i = 0;
	memset(cmplx, 0, sizeof(struct nae_complex_config));

	GET_PORT_PROP("num-channels", &cmplx->num_channels, sizeof(cmplx->num_channels));
	GET_PORT_PROP("num-free-descs", &cmplx->num_free_desc, sizeof(cmplx->num_free_desc));		
        GET_PORT_PROP("free-desc-size", &cmplx->free_desc_size, sizeof(cmplx->free_desc_size));
        GET_PORT_PROP("iface-fifo-size", &cmplx->intf_fifo_size, sizeof(cmplx->intf_fifo_size));
        GET_PORT_PROP("parser-sequence-fifo-size", &cmplx->prsr_seq_fifo_size, sizeof(cmplx->prsr_seq_fifo_size));
        GET_PORT_PROP("rx-buffer-size", &cmplx->rx_buf_size, sizeof(cmplx->rx_buf_size));
        GET_PORT_PROP("ucore-mask",&cmplx->ucore_mask, sizeof(cmplx->ucore_mask));
	GET_PORT_PROP("higig-mode",&cmplx->higig_mode, sizeof(cmplx->higig_mode));
	GET_PORT_PROP("xgmii-speed",&cmplx->xgmii_speed, sizeof(cmplx->xgmii_speed));
	GET_PORT_PROP("msec-port-enable",&cmplx->msec_port_enable, sizeof(cmplx->msec_port_enable));
	if (intf_type == SGMII_IF) {
		if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
		     for (i = 0 ; i < 4; i++) {
			if (cmplx->prsr_seq_fifo_size[i] > XLP3XX_SGMII_PARSERSEQ_FIFO_MAX)
				cmplx->prsr_seq_fifo_size[i] = XLP3XX_SGMII_PARSERSEQ_FIFO_MAX;
		     }	
		}
		GET_PORT_PROP("ext-phy-addr", &cmplx->ext_phy_addr, sizeof(cmplx->ext_phy_addr));
        	GET_PORT_PROP("ext-phy-bus", &cmplx->ext_phy_bus, sizeof(cmplx->ext_phy_bus));
	}
	else if (intf_type == INTERLAKEN_IF) {
		if(GET_PORT_PROP("num-lanes", &cmplx->num_lanes, sizeof(uint32_t)) < 0) {
                	nlm_print("fdt missing num-lanes, default 4\n");
                        cmplx->num_lanes = 8;
                }
                if(GET_PORT_PROP("lane-rate", &cmplx->lane_rate, sizeof(uint32_t)) < 0) {
                        nlm_print("fdt missing lane-rate, default 0\n");
                        cmplx->lane_rate = XLP_ILK_LANE_RATE_LOW;
                }
	}
	GET_PORT_PROP("mgmt-port", &cmplx->mgmt, sizeof(cmplx->mgmt)); 
        GET_PORT_PROP("loopback", &cmplx->loopback, sizeof(cmplx->loopback));
        GET_PORT_PROP("vlan-pri-en", &cmplx->vlan_pri_en, sizeof(cmplx->vlan_pri_en));
	
}

uint32_t nlm_hal_get_frin_total_queue(int node)
{
	uint32_t frin_total_queue = 0;

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
		frin_total_queue = XLP_3XX_NET_RX_VC_LIMIT - XLP_3XX_NET_RX_VC_BASE + 1;
	}else{
		frin_total_queue = XLP_NET_RX_VC_LIMIT - XLP_NET_RX_VC_BASE - 1; /*skip gdx port*/
	}
	return frin_total_queue;
}

uint32_t nlm_hal_get_frin_queue_base(int node)
{
	uint32_t frin_queue_base = 0;
	if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
		frin_queue_base = (node <<10) | XLP_3XX_NET_RX_VC_BASE;
	}else{
		frin_queue_base = (node << 10) | XLP_NET_RX_VC_BASE;
	}
	return frin_queue_base;
}

void dump_nae_cfg_info(int node, nlm_nae_config_ptr nae_cfg)
{
	int i, j;

	nlm_print("Node %d owned %d fb_vc = %d, rx_vc = %d\n", node, nae_cfg->owned, nae_cfg->fb_vc, nae_cfg->rx_vc);
	nlm_print(" frin_queue_base %d frin_total_queue %d \n", nae_cfg->frin_queue_base, nae_cfg->frin_total_queue);
	nlm_print(" number of ports %d max_channels %d\n", nae_cfg->num_ports, nae_cfg->num_contexts);
	for(i = 0; i < nae_cfg->num_ports; i++ ) {
		if(!nae_cfg->ports[i].valid)
			continue;
		nlm_print(" port %d type %d hwport %d num_channel %d num_free_desc %d \n", 
			i, nae_cfg->ports[i].iftype,
			nae_cfg->ports[i].hw_port_id, nae_cfg->ports[i].num_channels, nae_cfg->ports[i].num_free_desc);

		nlm_print(" port %d freedesc %d txq %d rxq %d num_channel %d \n",
			i, nae_cfg->ports[i].num_free_desc, nae_cfg->ports[i].txq, nae_cfg->ports[i].rxq, 
			nae_cfg->ports[i].num_channels);
	}
	nlm_print(" num-onchip-descs %d num-spill-descs %d\n",
			 nae_cfg->freein_fifo_onchip_num_descs[0],
			 nae_cfg->freein_fifo_spill_num_descs);

	nlm_print("onchip descs array:\n");
	for (j=0; j<MAX_NAE_FREEIN_DESCS_QUEUE; j++) {
		nlm_print(" %d", nae_cfg->freein_fifo_onchip_num_descs[j]);
	}
	nlm_print("\n");

	nlm_print(" frfifo-dom-mask %x vfbid-shared(sw %d:%d hw %d:%d) \n",
			nae_cfg->freein_fifo_dom_mask, nae_cfg->vfbtbl_sw_offset,
			nae_cfg->vfbtbl_sw_nentries, nae_cfg->vfbtbl_hw_offset, nae_cfg->vfbtbl_hw_nentries 
			);
	nlm_print("\n");
}


static int parse_port_config(void *fdt, int node, nlm_nae_config_ptr nae_cfg)
{
	char nae_port_str[80];
	struct nae_complex_config cmplx_cfg;
	int max_complex, block, intf_type, fdt_cmplx_offset ;
	int offset, port, num_ports;
	struct nlm_hal_nae_port *nae_port;
	uint32_t txq, max_context = 0, tx_slots = 0, rx_slots = 0;
	int submode = 0;
	int rxaui_en = 0;

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
		max_complex = XLP3XX_MAX_NAE_COMPLEX;
		nae_cfg->frin_queue_base = (node <<10) | XLP_3XX_NET_RX_VC_BASE;
		nae_cfg->frin_total_queue = XLP_3XX_NET_RX_VC_LIMIT - XLP_3XX_NET_RX_VC_BASE + 1;
		txq = (node <<10) | XLP_3XX_NET_TX_VC_BASE;
	}
	else {
		max_complex = XLP8XX_MAX_NAE_COMPLEX;
		nae_cfg->frin_queue_base = (node << 10) | XLP_NET_RX_VC_BASE;
                nae_cfg->frin_total_queue = XLP_NET_RX_VC_LIMIT - XLP_NET_RX_VC_BASE - 1; /*skip gdx port */
		txq = (node <<10) | XLP_NET_TX_VC_BASE;
	}
	
	for(block=0, port = 0; block < max_complex; block++) {
#ifndef NLM_HAL_UBOOT
		if (is_nlm_xlp8xx_ax() && is_xlp_evp1() && (block % 2))
			continue;
#else
#ifndef CONFIG_N511
	if (is_nlm_xlp8xx()) {
		if(block != 4) {
			nlm_print("Uboot Skip complex:%d\n", block);
			continue;
		}
	}
#endif
		nlm_print("Config complex:%d\n", block);
#endif
		sprintf(nae_port_str, "/soc/nae@node-%d/complex@%d",node, block);
		fdt_cmplx_offset = fdt_path_offset(fdt, nae_port_str);
		if (fdt_cmplx_offset < 0) {
			/*nlm_print("complex %d configuration is missing in FDT\n", block); */
			continue;
		}

		intf_type = get_interface_type(fdt, nae_port_str, block, port, node);
                nlm_print("Complex %d intf:%d\n", block, intf_type);
#ifdef NLM_HAL_UBOOT

		if (intf_type != SGMII_IF) {
			nlm_print("Uboot Skip Complex %d intf:%d\n", block, intf_type);
			continue;
		}
#endif
		if (intf_type == SGMII_IF) {
			sprintf(nae_port_str, "/soc/nae@node-%d/complex@%d/sgmii", node, block);
			fdt_cmplx_offset = fdt_path_offset(fdt, nae_port_str); 
	                if (fdt_cmplx_offset < 0) {
				nlm_print("Complex %d SGMII configuration missing in FDT \n", block);
				continue;
			}
			nae_cfg->sgmii_complex_map |= (1 << block);

		}
		else if (intf_type == XAUI_IF) {
			sprintf(nae_port_str, "/soc/nae@node-%d/complex@%d/xaui", node, block);
			fdt_cmplx_offset = fdt_path_offset(fdt, nae_port_str);
                        if (fdt_cmplx_offset < 0) {
                                nlm_print("Complex %d XAUI configuration missing in FDT \n", block);
                                continue;
                        }
			nae_cfg->xaui_complex_map |= (1 << block);
		}
		else if (intf_type == RXAUI_IF) {
			sprintf(nae_port_str, "/soc/nae@node-%d/complex@%d/rxaui", node, block);
			fdt_cmplx_offset = fdt_path_offset(fdt, nae_port_str);
                        if (fdt_cmplx_offset < 0) {
                                nlm_print("Complex %d RXAUI configuration missing in FDT \n", block);
                                continue;
                        }
			GET_PORT_PROP("scrambler", &(nae_cfg->ports[port].rxaui_scrambler),
				sizeof(nae_cfg->ports[port].rxaui_scrambler));
			GET_PORT_PROP("submode", &submode, sizeof(submode));
			nae_cfg->ports[port].rxaui_mode = submode;

			rxaui_en = ((submode == NLM_NAE_RXAUI_MODE_MARVELL) ||
				    (submode == NLM_NAE_RXAUI_MODE_BROADCOM)) ? 1 : 0;
			if(rxaui_en)
				nae_cfg->rxaui_complex_map |= (1 << block);
			else
				nae_cfg->xaui_complex_map  |= (1 << block);

			nlm_print("Complex:%d: rmap:0x%X map:0x%X submode:%d scrambler:%d\n", block,
				nae_cfg->rxaui_complex_map,
				nae_cfg->xaui_complex_map,
				nae_cfg->ports[port].rxaui_mode,
				nae_cfg->ports[port].rxaui_scrambler);
		}
		else if (intf_type == INTERLAKEN_IF) {
			sprintf(nae_port_str, "/soc/nae@node-%d/complex@%d/interlaken",node, block);
			fdt_cmplx_offset = fdt_path_offset(fdt, nae_port_str);
                        if (fdt_cmplx_offset < 0) {
                                nlm_print("Complex %d interlaken configuration missing in FDT \n", block);
                                continue;
                        }
			nae_cfg->ilk_complex_map |= (1 << block);
		}
		else {
			nlm_print("Complex %d interface type is unknown \n", block);
			continue;
		}

		num_ports = get_num_ports(block, intf_type);
		extract_complex_params(fdt, intf_type, nae_port_str, &cmplx_cfg);

		if(intf_type == XAUI_IF || intf_type == RXAUI_IF)
		{
			if(cmplx_cfg.msec_port_enable)
				nae_cfg->msec_port_enable |=  0xf << (4 * block);
		}
		else
			nae_cfg->msec_port_enable |= cmplx_cfg.msec_port_enable << (4 * block);
#ifdef MACSEC_DEBUG
		nlm_print(" nae_cfg->msec_port_enable = %x block = %d cmplx_cfg.msec_port_enable = %x\n", 
		nae_cfg->msec_port_enable, block, cmplx_cfg.msec_port_enable);
#endif
		for(offset = 0; offset < num_ports; offset++, port++) {
			nae_port = &nae_cfg->ports[port];
			nae_port->hw_port_id = (block * MAX_PORTS_PERBLOCK) + offset;
			nlm_print(" Printing off:%d hw_port:%d\n", offset, nae_port->hw_port_id);
			nae_port->txq = txq;
			nae_port->rxq = nae_cfg->frin_queue_base + nae_port->hw_port_id;
			nae_port->num_free_desc = cmplx_cfg.num_free_desc[offset];
			nae_port->free_desc_size = cmplx_cfg.free_desc_size[offset];
			nae_port->intf_fifo_size = cmplx_cfg.intf_fifo_size[offset];
			nae_port->prsr_seq_fifo_size = cmplx_cfg.prsr_seq_fifo_size[offset];
			nae_port->num_channels = cmplx_cfg.num_channels[offset];
			nae_port->rx_buf_size = cmplx_cfg.rx_buf_size[offset];	/* RX buf size is per context. The value given in dts is for the interface. This should be divided by num_channels  */
			nae_port->ucore_mask = cmplx_cfg.ucore_mask[offset];
			nae_port->ext_phy_addr = cmplx_cfg.ext_phy_addr[offset];
			nae_port->ext_phy_bus = cmplx_cfg.ext_phy_bus[offset];
			nae_port->loopback = cmplx_cfg.loopback[offset];
			nae_port->higig_mode = cmplx_cfg.higig_mode;
			nae_port->xgmii_speed = cmplx_cfg.xgmii_speed;
			nae_port->vlan_pri_en = cmplx_cfg.vlan_pri_en;

			if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
				nae_port->ucore_mask &= 0xFF;
				nae_port->ext_phy_bus = 0;
			}
			nae_port->rx_slots_reqd = get_slots_required(nae_port->iftype);
			nae_port->tx_slots_reqd = nae_port->rx_slots_reqd; 						
			nae_cfg->lane_rate[nae_port->hw_port_id / XLP_ILK_MAX_LANES] = cmplx_cfg.lane_rate;
			nae_cfg->num_lanes[nae_port->hw_port_id / XLP_ILK_MAX_LANES] = cmplx_cfg.num_lanes;
			nae_port->mgmt = cmplx_cfg.mgmt[offset];
			nae_port->valid = 1;
			max_context += nae_port->num_channels;
			if (!valid_context_number(max_context)) 
				return -1;
			tx_slots += nae_port->tx_slots_reqd;
			if (!valid_calendar_slot(tx_slots))
				return -1;
			rx_slots += nae_port->rx_slots_reqd;
			txq += nae_port->num_channels;		
		}		
		if (intf_type == INTERLAKEN_IF)
			block++;
	}
	nae_cfg->num_ports = port;
	nae_cfg->rx_cal_slots = rx_slots;
	nae_cfg->tx_cal_slots = tx_slots;
	nae_cfg->num_contexts = max_context;
	return nae_cfg->num_ports;
}


/**
* @brief parse_fdt_nae_config function is used to initailize and program the NAE and all interfaces, based on the configuration in FDT.
*
* @param [in] fdt Pointer to the FDT
* @param [in] node Node number
* @param [in] nae_cfg nlm_nae_config_ptr pointer to the internal HAL nae_cfg structure
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void parse_fdt_nae_config(void *fdt, int node, nlm_nae_config_ptr nae_cfg)
{
	int hw_port;
	int num_ports = 0, port = 0;
	uint32_t start_port, num_nae_regs, num_intf_regs;
	uint32_t rx_config = 0, tx_config = 0;
	int frequency, context = 0;

	/* Parse Nae Config */
	start_port = num_nae_regs = num_intf_regs = 0;

	frequency = nlm_hal_get_fdt_freq(fdt, NLM_NAE);
	set_nae_frequency(node, frequency);

	parse_vfbid_config(fdt, node);

	num_ports = parse_port_config(fdt, node, nae_cfg);  
	NAE_DEBUG("node %d num_ports:%d\n", node, num_ports);
	if (num_ports < 0) {
		nlm_print("Node %d NAE configuration failed. Check fdt params !!!", node);
		while(1);
	}

#if defined(NLM_HAL_UBOOT)
	if (num_ports == 0) {
		return;
	}
#endif

	NAE_DEBUG("node %d mdio init\n", node);
	nlm_hal_mdio_init(node);

	if (nae_cfg->sgmii_complex_map) {
		nlm_print("node %d SGMII PCS init 0x%x\n", node, nae_cfg->sgmii_complex_map);
	       	nlm_hal_sgmii_pcs_init(node, nae_cfg->sgmii_complex_map);
	}

	if (nae_cfg->xaui_complex_map) {
		nlm_print("node %d XAUI PCS init 0x%x \n", node, nae_cfg->xaui_complex_map);
		nlm_hal_xaui_pcs_init(node, nae_cfg);
	}

	if (nae_cfg->rxaui_complex_map) {
		nlm_print("node %d RXAUI PCS init 0x%x\n", node, nae_cfg->rxaui_complex_map);
		nlm_hal_xaui_pcs_init(node, nae_cfg);
		for(port = 0; port < num_ports; port++) {
			if(nae_cfg->ports[port].rxaui_mode)
				nlm_hal_rxaui_nlp1042c2_init(port, nae_cfg->ports[port].rxaui_mode);
		}
	}

	if (nae_cfg->ilk_complex_map) {
		nlm_print("node %d interlaken PCS init 0x%x\n", node, nae_cfg->ilk_complex_map);
		nlm_hal_ilk_pcs_init(node, nae_cfg->ilk_complex_map);
	}

        /* Clear NETIOR soft reset */
        nlm_hal_write_mac_reg(node, BLOCK_7, LANE_CFG, NETIOR_SOFTRESET, 0x0);
        NAE_DEBUG("%s Completed NETIOR soft reset\n", __func__);


	/* Disable RX enable bit in RX_CONFIG */
	rx_config = nlm_hal_read_nae_reg(node, RX_CONFIG);
	rx_config &= 0xfffffffe;
	nlm_hal_write_nae_reg(node, RX_CONFIG, rx_config);

	if (is_nlm_xlp8xx_ax() == 0) {
		tx_config = nlm_hal_read_nae_reg(node, TX_CONFIG);
		tx_config &= ~(1<<3);
		nlm_hal_write_nae_reg(node, TX_CONFIG, tx_config);
	}	
	
	nlm_config_nae_global(node, nae_cfg);

	if (nae_cfg->flags & VFBID_FROM_FDT) {
		nlm_config_vfbid_table(node, 0 , MAX_VFBID_ENTRIES, nae_vfbid_tbl[node]);	
	}

	if(nae_freein_fifo_cfg(fdt, node, nae_cfg) < 0)
		return;
	
	for(port = 0; port < num_ports; port++)
	{
		hw_port = nae_cfg->ports[port].hw_port_id;
                if (nae_cfg->ports[port].iftype == XAUI_IF) {
			/* NAE_DEBUG("Cfg XAUI mode with higig type = %d \n", nae_cfg->ports[port].higig_type); */
			nlm_print("Cfg XAUI mode for port %d with higig type = %d \n", port, nae_cfg->ports[port].higig_mode);
                        xlp_nae_config_xaui(node, (hw_port / 4), port, nae_cfg->ports[port].vlan_pri_en,
				nae_cfg->ports[port].rxaui_scrambler, NLM_NAE_XAUI_MODE_XAUI, nae_cfg->ports[port].higig_mode);
                }
                else if (nae_cfg->ports[port].iftype == RXAUI_IF) {
			NAE_DEBUG("Cfg RXAUI mode:%d scrambler:%d\n",
				nae_cfg->ports[port].rxaui_mode, nae_cfg->ports[port].rxaui_scrambler);
                        xlp_nae_config_xaui(node, (hw_port / 4), port, nae_cfg->ports[port].vlan_pri_en,
				nae_cfg->ports[port].rxaui_scrambler, nae_cfg->ports[port].rxaui_mode,  nae_cfg->ports[port].higig_mode);
                }
                else if (nae_cfg->ports[port].iftype == INTERLAKEN_IF) {
                    if(nae_cfg->ports[port].loopback)
                        xlp_nae_ilk_loopback(node, (hw_port / 4), nae_cfg->num_lanes[hw_port / XLP_ILK_MAX_LANES]);
                        xlp_nae_config_interlaken(node, (hw_port / 4), port, nae_cfg->num_lanes[hw_port / XLP_ILK_MAX_LANES]);
                }

		/* Egress Config */
		NAE_DEBUG("Egress context %d port %d num_channels %d \n",context,port, nae_cfg->ports[port].num_channels);
		config_egress(node, context, port, nae_cfg);

#if !defined(NLM_HAL_UBOOT) && defined(NLM_CORTINA_SUPPORT)
                if (nae_cfg->ports[port].iftype == INTERLAKEN_IF) {
                        if ((nlm_hal_init_cs34x7(hw_port, nae_cfg->num_lanes[hw_port / XLP_ILK_MAX_LANES],
			 nae_cfg->lane_rate[hw_port / XLP_ILK_MAX_LANES]) == 0) && 
				(is_xlp_ilk_lanealigned(node, hw_port/4)))
				nlm_print("Interlaken lanes on port %d are aligned\n", hw_port);
			else
				nlm_print("Interlaken initialization on port %d failed\n", hw_port);
                }
#endif
		context += nae_cfg->ports[port].num_channels;

		nlm_write_ucore_sprayvec(node, hw_port, nae_cfg->ports[port].ucore_mask);
		nlm_write_fifo_size(node, hw_port, nae_cfg->ports[port].free_desc_size);

		if (!rely_on_firmware_config) {
			if (nlm_hal_open_if(node, nae_cfg->ports[port].iftype, hw_port) < 0) {
				nlm_print("[%s] Unable to open port %d\n", __func__, port);
				continue;
			} 
		}

		NAE_DEBUG("Initialized port@%d\n", port);
	}
}

/**
* @brief drain_nae_frin_fifo_descs function is used to clear all FreeIn FIFOs in the NAE Ingress path.
*
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void drain_nae_frin_fifo_descs(int node)
{
        uint32_t value, fifo_mask;
	int timeout = 10;

	fifo_mask = nlm_get_max_ports();
	fifo_mask = ((1 << fifo_mask)-1);
	nlm_hal_write_nae_reg(node, RX_FREE_FIFO_POP, fifo_mask);
	for (; timeout >= 0; timeout--) {
		nlm_mdelay(1);
		value = nlm_hal_read_nae_reg(node, RX_FREE_FIFO_POP);
		if (value == fifo_mask) break;
	}
	nlm_hal_write_nae_reg(node, RX_FREE_FIFO_POP, 0);

	if (timeout) {
		nlm_print("Successfully zapped free in fifo!\n");
	} else {
		nlm_print("Unable to zap free in fifo!(value=0x%08x)\n", value);
	}
}

/**
  * @brief nlm_hal_nae_drain_frin_fifo_descs clears a specific Free-in FIFO
  * in the NAE ingress path.
  * @param[in] node: Node number
  * @param[in] inf : Interface number
  * @return
  * - 0 Free-in FIFO successfully emptied
  * - -1 Free-in FIFO failed to drain successfully
  * @ingroup hal_nae
  */
int nlm_hal_nae_drain_frin_fifo_descs(int node, int inf)
{
	uint32_t value, fifo_mask = 1 << inf;
	int timeout = 10;

	nlm_hal_write_nae_reg(node, RX_FREE_FIFO_POP, fifo_mask);
	for (; timeout >= 0; timeout--) {
		nlm_mdelay(1);
		value = nlm_hal_read_nae_reg(node, RX_FREE_FIFO_POP);
		if (value == fifo_mask) break;
	}
	nlm_hal_write_nae_reg(node, RX_FREE_FIFO_POP, 0);
	return timeout? 0 : -1;
}

/**
* @brief print_frin_desc_carving function prints the carving of the FreeIn FIFOs for the available interfaces.
*
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
#ifdef INCLUDE_NAE_DEBUG
static int debug = 1;
static void print_frin_desc_carving(int node)
{
	int intf, max_ports;

	if (!debug) return;

	max_ports = nlm_get_max_ports();
	for (intf = 0; intf < max_ports; intf++) {
		uint32_t value = 0;
		int start = 0, size = 0;

		nlm_hal_write_nae_reg(node, FREE_IN_FIFO_CFG, (0x80000000 | intf));

		value = nlm_hal_read_nae_reg(node, FREE_IN_FIFO_CFG);
		size = 2 * ((value >> 20) & 0x3ff);
		start = 2 * ((value >> 8) & 0x1ff);

		nlm_print("intf@%02d=0x%08x, start=%d, size=%d\n", intf, value, start, size);
	}
}
#endif /* INCLUDE_NAE_DEBUG */

/**
* @brief deflate_frin_fifo_carving function initializes the FreeIn FIFO carvings with minimum default values.
*
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void deflate_frin_fifo_carving(int node)
{
	int intf = 0, max_ports;
	const int minimum_size = 8; /* this represents entries, each entry holds 2 descriptors */
	int start = 0;
	uint32_t value = 0;

	max_ports = nlm_get_max_ports();
	for (intf = 0; intf < max_ports; intf++) {
		start = minimum_size * intf;
		value = (minimum_size << 20) | (start << 8) | (intf);
		nlm_hal_write_nae_reg(node, FREE_IN_FIFO_CFG, value);
	}
}

/**
* @brief check_header function is used to validate the FDT.
*
* @param [in] fdt Pointer to the FDT
*
* @return
*  - 0 for valid FDT
*  - -1 for invalid FDT
* 
* @ingroup hal_nae
*
*/
static int check_header(void *fdt)
{
        int fdt_err = fdt_check_header(fdt);
        if (fdt_err < 0) {
                nlm_print("FDT_ERROR: Invalid FDT Detected: %s\n",
                                fdt_strerror(fdt_err));
                return -1;
        }

	return 0;
}

/**
* @brief drain_nae_stray_packets function disables Rx for port 16 and 17, and sends 100 free descriptors to drain any packets in the ingress path.
*
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
void drain_nae_stray_packets(int node)
{
	int i, j;
	int mgmt_vc = 1016;
	int desc_size = 2048;
	uint64_t laddr = (255 << 20); /* 255 MB */


	for (j = 0; j < 2; ++j) {

		/* Turn off RX enable */
		write_gmac_reg(node, 16 + j , MAC_CONF1,0);


		/* Send Free descriptors */
		for (i = 0; i < 100; ++i) {
			if (nlm_hal_send_msg1( mgmt_vc + j,
					       0,
					       laddr)) {

				nlm_print("%s; Failed to send Free-in desc\n", __func__);
				break;
			}
			laddr += desc_size;
		}
	}
	nlm_mdelay(1);
}

/* 1588 timer related APIs */
uint64_t  nlm_hal_1588_ptp_get_counter(int node, int counter)
{
	int reg_hi = PTP_TMR1_HI + ((counter-1)<<1);
	int reg_lo = PTP_TMR1_LO +  ((counter-1)<<1);
	return (((uint64_t)nlm_hal_read_nae_reg(node, reg_hi) <<32) |nlm_hal_read_nae_reg(node,reg_lo));

}
void nlm_hal_1588_ptp_set_counter(int node, int counter, uint64_t cnt_val)
{
	int reg_hi = PTP_TMR1_HI + ((counter-1)<<1);
	int reg_lo = PTP_TMR1_LO +  ((counter-1)<<1);
	nlm_hal_write_nae_reg(node, reg_hi, cnt_val>>32);
	nlm_hal_write_nae_reg(node, reg_lo, cnt_val & 0xffffffff);
}

int  nlm_hal_is_intr_1588(int node)
{
	uint32_t val;
	int ptp_intr = (1<<6|1<<7|1<<8);
	val = nlm_hal_read_nae_reg(node, NET_COMMON0_INTR_STS);
	if(val & ptp_intr){
		return (val & ptp_intr);
	}
	return 0;
}

uint32_t nlm_hal_get_int_sts(int node)
{
	return nlm_hal_read_nae_reg(node, PTP_STATUS);
}

void nlm_hal_clear_1588_intr(int node, int timer)
{
	uint32_t val;
	val = nlm_hal_read_nae_reg(node, 0x7b3);
	nlm_hal_write_nae_reg(node, 0x7b3, val|(1<<(timer+19)));	
	val = nlm_hal_read_nae_reg(node, NET_COMMON0_INTR_STS);
	nlm_hal_write_nae_reg(node, NET_COMMON0_INTR_STS, val|(1<<(timer+5)));
}

void nlm_hal_enable_1588_intr(int node, int timer)
{
	uint32_t val;
	val = nlm_hal_read_nae_reg(node, 0x7b4);
	nlm_hal_write_nae_reg(node, 0x7b4, val|(1<<(timer+19)) );
	val = nlm_hal_read_nae_reg(node, PTP_CONTROL);	
	nlm_hal_write_nae_reg(node, PTP_CONTROL, val|(1<<(timer+2)));
} 

void nlm_hal_1588_ptp_clk_sel(int node, int clk_type)
{
	uint32_t val;
	val = nlm_hal_read_nae_reg(node, PTP_CONTROL);
	nlm_hal_write_nae_reg(node, PTP_CONTROL, val|(clk_type<<8));	
}

void nlm_hal_1588_ld_user_val(int node, uint32_t user_val_hi,  uint32_t user_val_lo)
{
	uint32_t val;
	nlm_hal_write_nae_reg(node, PTP_USER_VALUE_HI, user_val_hi);
	nlm_hal_write_nae_reg(node, PTP_USER_VALUE_LO, user_val_lo);
	val = nlm_hal_read_nae_reg(node, PTP_CONTROL);	
	nlm_hal_write_nae_reg(node, PTP_CONTROL, val|(1<<6)); 
}

void nlm_hal_1588_ld_offs(int node, uint32_t ptp_off_hi,  uint32_t ptp_off_lo)
{
	uint32_t val;
	nlm_hal_write_nae_reg(node, PTP_OFFSET_HI, ptp_off_hi);
	nlm_hal_write_nae_reg(node, PTP_OFFSET_LO, ptp_off_lo);
	val = nlm_hal_read_nae_reg(node, PTP_CONTROL);	
	nlm_hal_write_nae_reg(node, PTP_CONTROL, val|(1<<2)); 
}

void nlm_hal_1588_ld_freq_mul(int node, uint32_t ptp_inc_den, uint32_t ptp_inc_num, 
					uint32_t ptp_inc_intg)
{
	uint32_t val;
	nlm_hal_write_nae_reg(node, PTP_INC_DEN, ptp_inc_den);
	nlm_hal_write_nae_reg(node, PTP_INC_NUM, ptp_inc_num);
	nlm_hal_write_nae_reg(node, PTP_INC_INTG, ptp_inc_intg);
	val = nlm_hal_read_nae_reg(node, PTP_CONTROL);	
	nlm_hal_write_nae_reg(node, PTP_CONTROL, val|(1<<1)); 
}

void nlm_hal_reset_1588_accum(int node)
{
	uint32_t val;
	val = nlm_hal_read_nae_reg(node, PTP_CONTROL);	
	nlm_hal_write_nae_reg(node, PTP_CONTROL, val|0x1); 
}

void nlm_hal_prepad_enable(int node, int size)
{
	uint32_t val;
	val = nlm_hal_read_nae_reg(node, RX_CONFIG);
	val |= (1 << 13); /* prepad enable */
	val |= ((size & 0x3) << 22); /* prepad size */
	nlm_hal_write_nae_reg(node, RX_CONFIG, val);
}

uint32_t nlm_hal_get_rtc(int node, uint32_t* p_val_hi,  uint32_t* p_val_lo)
{
	uint32_t cmd = nlm_hal_read_nae_reg(node, IOSYS_RTC_CMD);
	*p_val_hi = nlm_hal_read_nae_reg(node, IOSYS_RTC_RDATA_HI);
	*p_val_lo = nlm_hal_read_nae_reg(node, IOSYS_RTC_RDATA_HI);
	/* nlm_print("RTC:%X %X.%X\n", cmd, *p_val_hi, *p_val_lo); */
	return cmd;
}

/**
* @brief reset_nae function resets the NAE.
*
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
static void reset_nae(int node)
{
	uint32_t rx_config = nlm_hal_read_nae_reg(node, RX_CONFIG);
	int reset_bit = 9;  
	/* Reset NAE */

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx())
		reset_bit = 6;
	else
		reset_bit = 9;
	
	nlm_hal_write_sys_reg(node, SYS_RESET, (1 << reset_bit));
	nlm_mdelay(1);

	nlm_hal_write_sys_reg(node, SYS_RESET, (0 << reset_bit));
	nlm_mdelay(1);

	rx_config = nlm_hal_read_nae_reg(node, RX_CONFIG);
	nae_reset_done[node] = 1;
}

#if !defined(NLM_HAL_UBOOT)
/**
 * @brief reset_poe function resets the POE.
 *
 * @param [in] node Node number
 *
 * @return
 *  - none
 * 
 * @ingroup hal_nae
 *
 */
static void reset_poe(int node)
{
	int reset_bit = 10;

	if (is_nlm_xlp3xx() || is_nlm_xlp2xx())
		reset_bit = 7;
	else
		reset_bit = 10;


	/* POE reset in the SYS_RESET register */
	nlm_hal_write_sys_reg(node, SYS_RESET, (1 << reset_bit));
	nlm_mdelay(1);

	nlm_hal_write_sys_reg(node, SYS_RESET, (0 << reset_bit));
	nlm_mdelay(1);

	return;
}
#endif

#if defined(__MIPSEL__)
static uint32_t membar_fixup(uint32_t l)
{
	unsigned char b;
	uint32_t fixup;

	if (!is_nlm_xlp8xx_ax())
		return l;

	b = (l >> 24) & 0xff;
	fixup = ((b << 24) | (b << 16) | (b << 8) | (b));

	return fixup;
}
#else
#define membar_fixup(x) (x)
#endif


/**
* @brief reset_nae_mgmt function deain the frin fifo followed by the reset_nae .
*
* @param [in] node Node number
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/

void  reset_nae_mgmt(int node)
{
	uint32_t bar0;
	nlm_print("Resetting NAE ..\n");
	bar0 = nlm_hal_read_32bit_reg(nlm_hal_get_dev_base(node, 0, XLP_NAE_DEVICE, XLP_NAE_FUNC), 0x4);
	bar0 = membar_fixup(bar0);
	drain_nae_frin_fifo_descs(node);
	reset_nae(node);
	nlm_hal_write_32bit_reg(nlm_hal_get_dev_base(node, 0, XLP_NAE_DEVICE, XLP_NAE_FUNC), 0x4, bar0);
}


#define CONFIG_RXAUI 1
#ifdef  CONFIG_RXAUI
#define NLM_C45_PRINTF(n) while(0) {nlm_print(n);} 

void nlm_hal_rxaui_nlp1042c2_rxaui_broadcom_init(int port)
{
	int i;
	if(port==0) {
		nlm_print("Broadcom RXAUI P0\n");
		#include "NLP1042C2_RXAUI_Dune.h"
	}
	else {
		nlm_print("Broadcom RXAUI P1\n");
		#include "NLP1042C2_RXAUI_Dune_4.h"
	}
}

void nlm_hal_rxaui_nlp1042c2_rxaui_marvell_init(int port)
{
	int i;
	if(port==0) {
		nlm_print("Marvell RXAUI P0\n");
		#include "NLP1042C2_RXAUI_Marvell.h"
	}
	else {
		nlm_print("Marvell RXAUI P1\n");
		#include "NLP1042C2_RXAUI_Marvell_4.h"
	}
}

void nlm_hal_rxaui_nlp1042c2_xaui_init(int port)
{
	int i;
	if(port==0) {
		nlm_print("NLP1042C2 XAUI P0\n");
		#include "NLP1042C2_XAUI.h"
	}
	else {
		nlm_print("NLP1042C2 XAUI P1\n");
		#include "NLP1042C2_XAUI_4.h"
	}
}

void nlm_hal_rxaui_nlp1042c2_init(int port, int rxaui_mode)
{
	uint32_t data;
	int phyaddr = port*4;
	nlm_print("NLP port:%d mode:%d\n", port, rxaui_mode);

	data = NLM_C45_READ(0,0,phyaddr,1,0xc205);
	nlm_print("NLP1042C2 p:%d 0xc205:%x \n", port, data);

	if(rxaui_mode == NLM_NAE_RXAUI_MODE_MARVELL)
		nlm_hal_rxaui_nlp1042c2_rxaui_marvell_init(port);
	else if(rxaui_mode == NLM_NAE_RXAUI_MODE_BROADCOM)
		nlm_hal_rxaui_nlp1042c2_rxaui_broadcom_init(port);
	else
		nlm_hal_rxaui_nlp1042c2_xaui_init(port);

	data = NLM_C45_READ(0,0,phyaddr,1,0xc241);
	nlm_print("NLP1042C2 p:%d 0xc241:%x \n", port, data);

	data = NLM_C45_READ(0,0,phyaddr,1,0xc243);
	nlm_print("NLP1042C2 p:%d 0xc243:%x \n", port, data);

	data = NLM_C45_READ(0,0,phyaddr,1,0xc246);
	nlm_print("NLP1042C2 p:%d 0xc246:%x \n", port, data);

	data = NLM_C45_READ(0,0,phyaddr,1,0xc20d);
	nlm_print("NLP1042C2 p:%d 0xc20d:%x \n", port, data);

	data = NLM_C45_READ(0,0,phyaddr,1,0xc2c3);
	nlm_print("NLP1042C2 p:%d 0xc2c3:%x \n", port, data);
}

#endif

/**********************************************************************
 *  nae_config_lane_gmac 
 *
 ********************************************************************* */
/**
* @brief nlm_hal_xaui_pcs_init initializes lane configuration and resets the Tx PLL for XAUI complexes.
*
* @param [in] node Node number
* @param [in] xaui_cplx_mask XAUI complex mask (possible in XLP832: 0x0F)
* @param [in] rxaui_en RXAUI enabled
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
void nlm_hal_xaui_pcs_init(int node, nlm_nae_config_ptr nae_cfg)
{
	/*TODO: include vsemi programming here for HIGIG */
	int block, lane_ctrl, lane_status, vsemi_config=0;
	int cplx_lane_enable = LM_XAUI | (LM_XAUI << 4) | (LM_XAUI << 8) | (LM_XAUI << 12);
	int lane_enable = 0;
	int phy_mode = (nae_cfg->rxaui_complex_map) ?  PHYMODE_RXAUI : PHYMODE_XAUI;
	static int vsemi_por=0;
	int xaui_cplx_mask = nae_cfg->xaui_complex_map | nae_cfg->rxaui_complex_map;
	

	if (xaui_cplx_mask == 0) {
		return;
	}

	/* write 0x2 to enable SGMII for all lane
	 */
	block = BLOCK_7;

	if (xaui_cplx_mask & 0x3) { /* Complexes 0, 1 */
		lane_enable = nlm_hal_read_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_0_1);	
		if (xaui_cplx_mask & 0x1) { /* Complex 0 */
			lane_enable &= ~(0xFFFF);
			lane_enable |= cplx_lane_enable;
		}
		if (xaui_cplx_mask & 0x2) {/* Complex 1 */
			lane_enable &= ~(0xFFFF<<16);
			lane_enable |= (cplx_lane_enable << 16);
		}
		nlm_hal_write_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_0_1,   lane_enable);
		nlm_mdelay(1);
	}
	lane_enable = 0;
	if (xaui_cplx_mask & 0xc) { /* Complexes 2, 3 */
		lane_enable = nlm_hal_read_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_2_3);
		if (xaui_cplx_mask & 0x4) { /* Complex 2 */
			lane_enable &= ~(0xFFFF);
			lane_enable |= cplx_lane_enable;
		}
		if (xaui_cplx_mask & 0x8) {/* Complex 3 */
			lane_enable &= ~(0xFFFF<<16);
			lane_enable |= (cplx_lane_enable << 16);
		}
		nlm_hal_write_mac_reg(node, block, LANE_CFG, LANE_CFG_CPLX_2_3,   lane_enable);
		nlm_mdelay(1);
	}


	/* Bring txpll out of reset */
	for( block = 0; block < 4; block++)
	{
		if ((xaui_cplx_mask & (1 << block)) == 0) {
			continue;
		}		
		
		if(is_nlm_xlp2xx()){
			int delay;
			int port;
			struct nlm_hal_nae_port *portcfg;
			int xaui_speed;	 	
			if(nae_cfg->num_ports>2)
				port = block<<2;
			else 
				port = block;	
			
			portcfg = &nae_cfg->ports[port];
			xaui_speed = portcfg->xgmii_speed;
			if(xaui_speed==16){
				nlm_print("VSEMI: config data for 16G XAUI \n");
				nlm_hal_config_vsemi_mem_16G_4page_125();
				nlm_hal_config_vsemi_mem_16G_125();
			}	
			else if(xaui_speed==12){
				nlm_print("VSEMI: config data for 12G XAUI \n");
				nlm_hal_config_vsemi_mem_12G_4page_125();
				nlm_hal_config_vsemi_mem_12G_125();
			}	
			else {
				nlm_print("VSEMI: config data for 10G XAUI/RXAUI \n");
				nlm_hal_config_vsemi_mem_xaui_4page_125();
				nlm_hal_config_vsemi_mem_xaui_125();	
			}
			
			
			if(!vsemi_por){			
				vsemi_config = nlm_hal_read_mac_reg(node, block+1, PHY, VSEMI_CTL0);
				vsemi_config &= ~VSEMI_CTL_POR;
				nlm_hal_write_mac_reg(node, block+1, PHY, VSEMI_CTL0, vsemi_config);
				vsemi_por++;
			}
			
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL0);
			vsemi_config &= 0xFFFF01FF;
			vsemi_config |= VSEMI_CTL_POR | VSEMI_CTL_SYNTH_RST | VSEMI_CTL_RTHR;
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL0, vsemi_config);
    			
			for(delay=0; delay<1000000; delay++);
			
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL0);
			vsemi_config &= ~VSEMI_CTL_POR;
			vsemi_config &= 0xFFFFFE00;
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL0, vsemi_config);

			
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL1);
			vsemi_config &= ~((0x7<<4) | 0x7);

			if(xaui_speed==16)
				 vsemi_config = VSEMI_CTL_XAUI_16G_DR | VSEMI_CTL_XAUI_16G_DW;
			else if(xaui_speed==12)
				 vsemi_config = VSEMI_CTL_XAUI_12G_DR | VSEMI_CTL_XAUI_12G_DW;
			else if((PHYMODE_RXAUI==phy_mode) && (xaui_speed==10)) {
				 vsemi_config = VSEMI_CTL_RXAUI_10G_DR | VSEMI_CTL_RXAUI_10G_DW;
			}
			else /* XAUII/10G */
				 vsemi_config = VSEMI_CTL_XAUI_DR | VSEMI_CTL_XAUI_DW;
			
			for(delay=0; delay<1000000; delay++);
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL1, vsemi_config);
				
			nlm_hal_xlp2xx_nae_program_vsemi(node, block, xaui_speed, 0);
			
#ifdef VSEMI_DEBUG
			display_vsemi_indirect_reg(node, block);
#endif
			vsemi_config = nlm_hal_read_mac_reg(node, block, PHY, VSEMI_CTL0);
			vsemi_config &= ~(VSEMI_CTL_POR | VSEMI_CTL_SYNTH_RST | VSEMI_CTL_RTHR);
			nlm_hal_write_mac_reg(node, block, PHY, VSEMI_CTL0, vsemi_config);
			
		}

		for( lane_ctrl = PHY_LANE_0_CTRL; lane_ctrl <= PHY_LANE_3_CTRL; lane_ctrl++) {

			if(phy_mode == PHYMODE_RXAUI && (lane_ctrl%2))
				continue;

			if((is_nlm_xlp3xx()) || (is_nlm_xlp8xx_bx())){
				xlp3xx_8xxb0_nae_lane_reset_txpll(node, block, lane_ctrl, phy_mode);
				nlm_hal_PMFF_ALL_workaround(node, block, lane_ctrl);
			}else if(is_nlm_xlp2xx()){
				xlp2xx_nae_lane_reset_txpll(node, block, lane_ctrl, phy_mode);
			}else{
				xlp8xx_ax_nae_lane_reset_txpll(node, block, lane_ctrl, phy_mode);
			}
		}
	}
	NAE_DEBUG("%s all blocks & lanes out of TXPLL\n", __func__);

	nlm_mdelay(10);
	/* Wait for Rx & TX clock stable */
	for( block = 0; block < 4; block++)
	{
		if ((xaui_cplx_mask & (1 << block)) == 0) {
			continue;
		}


		for( lane_status = PHY_LANE_0_STATUS; lane_status <= PHY_LANE_3_STATUS; lane_status++) {

			if(phy_mode == PHYMODE_RXAUI && (lane_status%2))
				continue;

			/* Wait for TX clock to be set */
			nlm_print("Blk:%d lane%d wait TX clock stable\n", block, lane_status);
			while ((nlm_hal_read_mac_reg(node, block, PHY, lane_status) & (PHY_LANE_STAT_STCS)) != PHY_LANE_STAT_STCS) {
			}

			/* Wait for RX clock to be set */
			nlm_print("Blk:%d lane%d wait RX clock stable\n", block, lane_status);
			while ((nlm_hal_read_mac_reg(node, block, PHY, lane_status) & (PHY_LANE_STAT_SRCS)) != PHY_LANE_STAT_SRCS) {
			}

			nlm_print("Blk:%d lane%d wait lane fault cleared\n", block, lane_status);
			while ((nlm_hal_read_mac_reg(node, block, PHY, lane_status) & (PHY_LANE_STAT_XLF)) != 0) {
				/* Wait for XAUI Lane fault to be cleared */
			}
		}
	}

#ifdef CONFIG_N511
        nlm_xaui_phy_scan();
#endif /*CONFIG_N511*/

}

/* XAUI port initialization */
/**
* @brief xlp_nae_config_xaui initializes a XAUI port at the MAC level.
*
* @param [in] node Node number
* @param [in] block NAE Register Memory Map Block
* @param [in] port Logical interface number (not hardware interface number)
* @param [in] vlan_pri_en MAC level filtering enable
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
static void xlp_nae_config_xaui(int node, int block, int port, int vlan_pri_en,
				int rxaui_scrambler, int mode, int higig_type)
{
	uint32_t val = 0;
	int rxaui_en = ((mode == NLM_NAE_RXAUI_MODE_MARVELL) ||
                        (mode == NLM_NAE_RXAUI_MODE_BROADCOM)) ? 1 : 0;

	if(higig_type){
		/*enable higig */
		uint32_t higig_cfg = 0;
		higig_cfg = (1<<0) | (1<<1) | (1<<4) | (1<<6) | (1<<3) | (1<<7)| (1<<12); 
		if(higig_type==HIGIG2){
			higig_cfg |= (1<<10);
		}
		nlm_hal_write_mac_reg(node, block, XGMAC, NETIOR_HIGIG2_CTRL0, higig_cfg);  
		
		higig_cfg =  nlm_hal_read_mac_reg(node, block, XGMAC, NETIOR_HIGIG2_CTRL1);
		higig_cfg |= (0x20) | (0x80<<8) | (0x10<<16) | (0x20<<24); /* RX/TX threshold */
		nlm_hal_write_mac_reg(node, block, XGMAC, NETIOR_HIGIG2_CTRL1, higig_cfg);  

		higig_cfg =  nlm_hal_read_mac_reg(node, block, XGMAC, NETIOR_HIGIG2_CTRL2);
		higig_cfg |= (0x3fff<<16);  /*set frame len*/
		nlm_hal_write_mac_reg(node, block, XGMAC, NETIOR_HIGIG2_CTRL2, higig_cfg);
	}

	val = 	nlm_hal_read_mac_reg(node, block, XGMAC, XGMAC_CTL_REG1);
	val &= ~(0x1 << 11); /* Reset xgmac soft reset(bit 11) xaui soft reset (bit 12) */
	nlm_hal_write_mac_reg(node, block, XGMAC, XGMAC_CTL_REG1, val);
	NAE_DEBUG("%s Cleared XGMAC soft reset\n", __func__);

	val = 	nlm_hal_read_mac_reg(node, block, XGMAC, XGMAC_CTL_REG1);
	val &= ~(0x3 << 11); /* Reset xgmac soft reset(bit 11) xaui soft reset (bit 12) */
	nlm_hal_write_mac_reg(node, block, XGMAC, XGMAC_CTL_REG1, val);
	NAE_DEBUG("%s Cleared XAUI reset\n", __func__);
	
	nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_CONFIG_0, 0xffffffff);
	nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_CONFIG_0, 0x0);

	/* Enable tx/rx frame */
	val = 0xA00010A8;
	val |= (XAUI_CONFIG_LENCHK | XAUI_CONFIG_GENFCS | XAUI_CONFIG_PAD_64);
	nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_CONFIG_1, val );
	/* write max frame len*/
	nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_MAX_FRAME_LEN , 0x01800600);

	/* set stats counter*/
	val = nlm_hal_read_mac_reg(node, block, XGMAC, NETIOR_XGMAC_CTRL1);
	val |= 1 << NETIOR_XGMAC_STATS_EN_POS;

	if (rxaui_en)
		val |= 1 << NETIOR_XGMAC_RXAUI_EN_POS;
	else
		val &= ~(1 << NETIOR_XGMAC_RXAUI_EN_POS);

	if (rxaui_en)
	{
		if(mode == NLM_NAE_RXAUI_MODE_MARVELL)
			val &= ~(1 << NETIOR_XGMAC_RXAUI_DC_POS);
		else if(mode == NLM_NAE_RXAUI_MODE_BROADCOM)
			val |= 1 << NETIOR_XGMAC_RXAUI_DC_POS;

		if(rxaui_scrambler)
			val |= 1 << NETIOR_XGMAC_RXAUI_SCRAMBLER_POS;
		else
			val &= ~(1 << NETIOR_XGMAC_RXAUI_SCRAMBLER_POS);
	}

	if (vlan_pri_en) {
		val |= 1 << NETIOR_XGMAC_TX_PFC_EN_POS; 
		val |= 1 << NETIOR_XGMAC_RX_PFC_EN_POS; 
		val |= 1 << NETIOR_XGMAC_TX_PAUSE_POS; 
		val |= 1 << NETIOR_XGMAC_VLAN_DC_POS;
	} else {
		val &= ~(1 << NETIOR_XGMAC_TX_PFC_EN_POS); 
		val &= ~(1<<NETIOR_XGMAC_RX_PFC_EN_POS);
		val |= (1 << NETIOR_XGMAC_TX_PAUSE_POS); 
		val &= ~(1 << NETIOR_XGMAC_VLAN_DC_POS);
	}
	nlm_hal_write_mac_reg(node, block, XGMAC, NETIOR_XGMAC_CTRL1, val);

		/*
		 * Configuring the OFF/ON timer
		 * 31:16  - In PFC mode is used as the Xoff value                                     
		 * 15:0   - In PFC mode is used as the Xon value                                      
		 *        - in Link level FC mode, is used as the Xoff value.                         
		 */
	if (vlan_pri_en) {
		val = 0xF1230000;          /* PFC mode:  OffTimer = 0xF123  OnTimer = 0x0000   */
	} else {
		val = 0x0000F123;          /* Link level FC: OffTimer = 0xF123     */
	}
	nlm_hal_write_mac_reg(node, block, XGMAC, NETIOR_XGMAC_CTRL2, val);


	/* set xaui tx threshold */
	val = nlm_hal_read_mac_reg(node, block, XGMAC, NETIOR_XGMAC_CTRL3);

	val &= ~(0x1f << 10);  
	val |= (15 << 10);  

	nlm_hal_write_mac_reg(node, block, XGMAC, NETIOR_XGMAC_CTRL3, val); NAE_DEBUG("%s XAUI Config Complete block %d swport %d \n", __func__,block, port); return;
}

int nlm_hal_set_xaui_framesize(int node, int block, uint32_t tx_size, uint32_t rx_size)
{
       nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_MAX_FRAME_LEN , ((tx_size/4) << 16) | rx_size);
       NAE_DEBUG("Max frame len set for RX= %d bytes\n", nlm_hal_read_mac_reg(node, block, XGMAC, XAUI_MAX_FRAME_LEN) & (0xffff));
       NAE_DEBUG("Max frame len set for TX= %d bytes\n",  4*((nlm_hal_read_mac_reg(node, block, XGMAC, XAUI_MAX_FRAME_LEN) >>16) & (0xffff)));
       return 0;
}

int nlm_hal_set_sgmii_framesize(int node, int block, int index, uint32_t size)
{
       nlm_hal_write_mac_reg(node, block, index, SGMII_MAX_FRAME_LEN, size);
       NAE_DEBUG("Max frame len set = %d bytes\n",  nlm_hal_read_mac_reg(node, block, index, SGMII_MAX_FRAME_LEN));
       return 0;
}

#if !defined(NLM_HAL_UBOOT) && defined(NLM_CORTINA_SUPPORT)
int nlm_hal_set_ilk_framesize(int node, int block, int port, uint32_t size)
{
	return cortina_set_max_framesize(block * 4, port, size);
}
#else
int nlm_hal_set_ilk_framesize(int node, int block, int port, uint32_t size)
{
	return 0;
}
#endif

#ifdef NLM_HAL_LINUX_KERNEL 
int nlm_hal_get_ilk_mac_stats(int node, int block, int port, void *data)
{
#ifdef NLM_CORTINA_SUPPORT
	struct net_device_stats *stats = (struct net_device_stats *)data;	
	return cortina_get_mac_stats(node, block * 4, port,(void *) stats);
#else
	return 0;
#endif
}	
#endif

/*
 *                   Interlaken Support
 *
 */

static uint8_t nlm_hal_read_pma_reg(int node, int block, int lane_ctrl, uint8_t serdes_reg)
{
        volatile uint32_t serdes_val, regval;

	regval = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	regval |= PHY_LANE_CTRL_RST;
	regval |= PHY_LANE_CTRL_CMD_READ;
	regval = (regval & 0xffff0000);
	nlm_hal_write_mac_reg(node, block , PHY, lane_ctrl, regval
			| (serdes_reg << PHY_LANE_CTRL_ADDR_POS));

	while (((serdes_val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING));

	regval |= PHY_LANE_CTRL_CMD_START;
	nlm_hal_write_mac_reg(node, block , PHY, lane_ctrl, regval
			| (serdes_reg << PHY_LANE_CTRL_ADDR_POS));

	while (((serdes_val = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING));

        return (serdes_val & 0xFF);
}

static void nlm_hal_write_pma_reg(int node, int block, int lane_ctrl, uint8_t serdes_reg, uint8_t serdes_val)
{
        volatile uint32_t regval = 0;

	regval = nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl);
	regval |= PHY_LANE_CTRL_RST;
	regval &= ~(PHY_LANE_CTRL_CMD_READ); 
	regval = (regval & 0xffff0000) ;
	nlm_hal_write_mac_reg(node, block , PHY, lane_ctrl, regval
			| (serdes_reg << PHY_LANE_CTRL_ADDR_POS)
			| (serdes_val << PHY_LANE_CTRL_DATA_POS));

	while((nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING);

	regval |= PHY_LANE_CTRL_CMD_START;
	nlm_hal_write_mac_reg(node, block , PHY, lane_ctrl, regval
			| (serdes_reg << PHY_LANE_CTRL_ADDR_POS)
			| (serdes_val << PHY_LANE_CTRL_DATA_POS));

	while((nlm_hal_read_mac_reg(node, block, PHY, lane_ctrl)) & PHY_LANE_CTRL_CMD_PENDING);
}

static inline void nlm_hal_PMFF_ALL_workaround(int node, int block, int lane_ctrl)
{
	volatile int val = 0;

	if (is_nlm_xlp3xx_ax() || is_nlm_xlp3xx_b0()) {
		val = nlm_hal_read_pma_reg(node, block, lane_ctrl, SER_GEN1_PWR_DOWN);
		val = val | SERDES_PMFF_ALL_SET;
		nlm_hal_write_pma_reg(node, block, lane_ctrl, SER_GEN1_PWR_DOWN, val);
	}
}

static void nlm_hal_set_PLL(int node, int block, int lane_ctrl, uint32_t F_val, uint32_t N_val, uint32_t M_val)
{
        uint32_t data;

        data = nlm_hal_read_pma_reg(node, block, lane_ctrl, 4);
        data = data & 0xf0;
        data = data | (F_val & 0xf);
        nlm_hal_write_pma_reg(node, block, lane_ctrl, 4, data);

        data = nlm_hal_read_pma_reg(node, block, lane_ctrl, 5);
        data = data & 0x80;
        data = data | (N_val & 0x1f) | ((M_val & 0x3) << 5);
        nlm_hal_write_pma_reg(node, block, lane_ctrl, 5, data);
}

/**
* @brief xlp8xx_ilk_reset_pll initializes lane configuration and resets the Tx PLL for INTERLAKEN.
*
* @param [in] node Node number
* @param [in] block NAE Register Memory Map Block
* @param [in] ilk_num_lanes Number of INTERLAKEN lanes to configure
* @param [in] ilk_rate PLL lane frequency
* @return
*       - none
*
* @ingroup hal_nae
*
*/
static void xlp8xx_ilk_reset_pll(int node, int ilk_block_base, int ilk_num_lanes, int ilk_rate)
{
        volatile uint32_t lane_mask = 0, val = 0;
        uint32_t ilk_lane_eable = 0;
        volatile uint32_t lane_config = 0, lane_cfg_reg =  ((ilk_block_base < 2) ? LANE_CFG_CPLX_0_1 : LANE_CFG_CPLX_2_3);
        volatile uint32_t phy_lane = PHY_LANE_CTRL_BPC_XAUI | PHY_LANE_CTRL_RST |
                                PHY_LANE_CTRL_PWRDOWN | (PHYMODE_IL << PHY_LANE_CTRL_PHYMODE_POS);
        uint32_t block, lane, max_block, rext_sel=0;
	int i;

        if (!((ilk_block_base == 0) || (ilk_block_base == 2)))
                return;
        /* Configure Lane mode for interlaken */

        block = max_block = ilk_block_base ;
        max_block += ((ilk_num_lanes > MAX_LANE_PER_CPLX ) ? 2 : 1);

        for(; block < max_block ; block++ ) {
                if (block % 2) {
                        ilk_lane_eable <<= 16;
                        lane_mask <<= 16;
                }
                else {
                        ilk_lane_eable = (LM_IL << LANE_CFG_LANE_3_POS) |
                                        (LM_IL << LANE_CFG_LANE_2_POS) |
                                        (LM_IL << LANE_CFG_LANE_1_POS) | LM_IL;
                        lane_mask = 0xFFFFFFFF;
                }
                lane_config = nlm_hal_read_mac_reg(node, BLOCK_7, LANE_CFG, lane_cfg_reg);
                nlm_hal_write_mac_reg(node, BLOCK_7, LANE_CFG, lane_cfg_reg, lane_config & (~lane_mask));

                lane_config = nlm_hal_read_mac_reg(node, BLOCK_7, LANE_CFG, lane_cfg_reg);
                lane_config |= ilk_lane_eable;
                nlm_hal_write_mac_reg(node, BLOCK_7, LANE_CFG, lane_cfg_reg, lane_config | ilk_lane_eable);
        }
 
        if (is_nlm_xlp8xx_bx()) {
                for(block = ilk_block_base; block < max_block ; block++) {
                        for (lane = PHY_LANE_0_CTRL; lane <= PHY_LANE_3_CTRL; lane++) {
                                if (lane != 4)
                                        rext_sel = (1 << PHY_LANE_CTRL_REXSEL_POS);
                                else
                                        rext_sel = 0;
                                val = PHY_LANE_CTRL_RESET_PMA | (PHYMODE_IL << PHY_LANE_CTRL_PHYMODE_POS) | 
						PHY_LANE_CTRL_BPC_XAUI | PHY_LANE_CTRL_PWRDOWN | rext_sel; 
                                val &= ~(PHY_LANE_CTRL_RST);
                                nlm_hal_write_mac_reg(node, block, PHY, lane,val);
			}
			
			for (i=0; i< 0x100000; ++i);

			for (lane = PHY_LANE_0_CTRL; lane <= PHY_LANE_3_CTRL; lane++) {
				nlm_hal_set_PLL(node, block, lane, 0, 19, 0);
			}

			for (lane = PHY_LANE_0_CTRL; lane <= PHY_LANE_3_CTRL; lane++) { 
                                val = nlm_hal_read_mac_reg(node, block, PHY, lane);
                                val |= PHY_LANE_CTRL_RST; /* Bit30: epcs reset */
                                nlm_hal_write_mac_reg(node, block, PHY, lane,val);
                                NAE_DEBUG(" After serdes  de-assertion PMA value=0x%x\n", val);
				/* Clear the Power Down bit */
                                val = nlm_hal_read_mac_reg(node, block, PHY, lane);
                                val &= ~(PHY_LANE_CTRL_PWRDOWN  | (0x7ffff)) ;
                                nlm_hal_write_mac_reg(node, block, PHY, lane,  val);
				NAE_DEBUG("Reset PLL done \n");
                        }
                }
        }
	else {       
		for(block = ilk_block_base; block < max_block ; block++) {
        	        for (lane = PHY_LANE_0_CTRL; lane <= PHY_LANE_3_CTRL; lane++) {
	        		/*  xlp8xx_ax_nae_lane_reset_txpll( block, lane, PHYMODE_IL); */
        	                nlm_hal_write_mac_reg(node, block, PHY, lane,  phy_lane);

                        	if (lane != 4) {
                	                int i;
                                	rext_sel = (1 << 23);

	                                val = nlm_hal_read_mac_reg(node, block, PHY, lane);
        	                        val &= ~PHY_LANE_CTRL_RST; /* Set the reset (inverse logic) */
                	                val |= rext_sel | PHY_LANE_CTRL_BPC_XAUI;
	
        	                        /* Resetting PMA for non-zero lanes */
                	                nlm_hal_write_mac_reg(node, block, PHY, lane,val);

                        	        for (i=0; i< 0x100000; ++i); /* empty loop */

                                	val |= PHY_LANE_CTRL_RST; /* Unset the reset (inverse logic) */
	                                nlm_hal_write_mac_reg(node, block, PHY, lane,val);
        	                        val = 0;
                	        }
                        	else
                                	rext_sel = 0;

        	                val = nlm_hal_read_pma_reg(node, block, lane, 0x66) & 0xFF;		
       	        	        nlm_hal_set_PLL(node, block, lane, 0, 19, 0);

               	        	val &= ~(1 << 4);
                        	nlm_hal_write_pma_reg(node, block, lane, 0x66, val);
	                        nlm_hal_write_pma_reg(node, block, lane, 0x66, val);

	                        val = nlm_hal_read_mac_reg(node, block, PHY, lane);
        	                val &= ~( (1 << 29) | (0x7ffff));
                	        nlm_hal_write_mac_reg(node, block, PHY, lane, (rext_sel | val));

	        		/* nlm_print("pll F %x \n", nlm_hal_read_pma_reg(block, lane, 4)); */
        			/* nlm_print("pll M %x \n", nlm_hal_read_pma_reg(block, lane, 5)); */

                	}
        	}
	}
}

#ifdef INTERLAKEN_DEBUG
static void dump_interlaken_regs(int node, int blk)
{
	nlm_print("Rxstats1 0x%x\n",nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_RX_STATUS1));
	nlm_print("Rxstats2 0x%x\n",nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_RX_STATUS2));
	nlm_print("Rxstats3 0x%x\n",nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_RX_STATUS3));
}

static void xlp_debug_interlaken(int node, int blk)
{
	uint32_t debug;
	
	debug = (0 << ILK_GEN_CTRL2_SCS0_POS) | (1 << ILK_GEN_CTRL2_SCS1_POS ) | 
		(2 << ILK_GEN_CTRL2_SCS2_POS);

        debug |= (3 << ILK_GEN_CTRL2_SCS4_POS) | (4 << ILK_GEN_CTRL2_SCS5_POS);
	nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_GENERAL_CTRL2, debug);

	debug = (1 << ILK_GEN_CTRL3_LCS1_POS) | (3 << ILK_GEN_CTRL3_LCS0_POS) |
		(1 << ILK_GEN_CTRL3_MCS1_POS) | (0 << ILK_GEN_CTRL3_MCS0_POS) |
		(13 << ILK_GEN_CTRL3_SCS6_POS) | (11 << ILK_GEN_CTRL3_SCS7_POS);
	nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_GENERAL_CTRL3, debug);
}
#endif
/**
* @brief xlp_nae_ilk_loopback. Enables serdes loopback
*
* @param [in] node Node number
* @param [in] block NAE Register Memory Map Block
* @param [in] num_lanes Number of INTERLAKEN lanes enabled
*
* @return
*       - none
*/
static void xlp_nae_ilk_loopback(int node, int blk, int num_lanes)
{
	int lane, lnsperblk;
	volatile uint8_t prbsctrl = 0;
	int i = 0;

	if (!((blk == 0) || (blk == 2)))
		return;

	lnsperblk = (num_lanes > MAX_LANE_PER_CPLX) ?
			MAX_LANE_PER_CPLX : num_lanes;
	NAE_DEBUG("\n%s: Enabling interlaken serdes loopback ",__func__);
	NAE_DEBUG("\n Loopback: Node:%d Block:%d",node,blk);

	do {
		for (lane = 0; lane < lnsperblk; lane++) {
			prbsctrl = nlm_hal_read_pma_reg(node,blk + i, lane + 4,
					SERDES_PRBS_CTRL);
			prbsctrl |= SERDES_LOOPBACK_EN;
			nlm_hal_write_pma_reg(node,blk + i, lane + 4,
				SERDES_PRBS_CTRL, prbsctrl);
			NAE_DEBUG(" Lane %d", lane);
		}
		lnsperblk = num_lanes - lnsperblk;
		if (!lnsperblk)
			break;
		i++;
	} while (i < 2);

	nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_RX_STATUS1,
		0xFFFFFFFF);
	nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_RX_STATUS2,
		0xFFFFFFFF);
	nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_RX_STATUS3,
		0x0004FFFF);
}

/**
* @brief xlp_nae_config_interlaken initializes an INTERLAKEN port at the MAC level.
*
* @param [in] node Node number
* @param [in] block NAE Register Memory Map Block
* @param [in] port Logical interface number (not hardware interface number)
* @param [in] num_lanes Number of INTERLAKEN lanes enabled
*
* @return
*       - none
*
* @ingroup hal_nae
*
*/
static void xlp_nae_config_interlaken(int node, int blk,int port, int num_lanes)
{
        volatile uint32_t rxctrl = 0;
        volatile uint32_t txctrl = 0, genctrl = 0;
        volatile uint32_t val = 0;

        if (!((blk == 0) || (blk == 2)))
                return;

        nlm_print("interlaken blk %d num_lanes %d \n",blk, num_lanes);
        
	nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_TX_META_CTRL, 0x00f700f7); /* cortina */

        /* Configure number of lanes , IL registers */
        if (num_lanes == 1) {
                rxctrl = ILK_RX_CTRL_BAD_LANE | (num_lanes << ILK_RX_CTRL_BLS_POS )
                         | (num_lanes << ILK_RX_CTRL_LLS_POS);
                txctrl = ILK_TX_CTRL_BAD_LANE | (num_lanes << ILK_TX_CTRL_BLS_POS)
                         | (num_lanes << ILK_TX_CTRL_LLS_POS);
        }
        else {
                rxctrl = (num_lanes - 1) << ILK_RX_CTRL_LLS_POS | ((num_lanes-1) << ILK_RX_CTRL_BLS_POS);
                txctrl = (num_lanes - 1) << ILK_TX_CTRL_LLS_POS | ((num_lanes-1) << ILK_TX_CTRL_BLS_POS) | (3 << ILK_TX_CTRL_BS_POS);
        }

        rxctrl |= (ILK_BURST_MAX << ILK_RX_CTRL_BMAX_POS) ;
        txctrl |= (ILK_BURST_MAX << ILK_TX_CTRL_BMAX_POS) | (0x1 << ILK_TX_CTRL_CAL_LEN_POS) | ILK_TX_CTRL_TX_EN ;
        /* Enable packet mode and ratelimit for cortina IL card */
        rxctrl |= ILK_RX_CTRL_PKT_MODE;
        txctrl |= ILK_TX_CTRL_RATELIM_EN;
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_RX_CONTROL, rxctrl);
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_TX_CONTROL, txctrl);

        genctrl = nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_GENERAL_CTRL1);
        genctrl &= (~(0xF << 8)); /* 16:0 32:1 48:2 64:3 */
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_GENERAL_CTRL1, genctrl);

        txctrl = nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_TX_CONTROL);
        txctrl |= ILK_TX_CTRL_RST_INF | ILK_TX_CTRL_RST_CORE;
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_TX_CONTROL, txctrl);

        rxctrl = nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_RX_CONTROL);
        rxctrl |= (ILK_BURST_MAX << ILK_RX_CTRL_BMAX_POS);
        rxctrl |= (0xFF << ILK_RX_CTRL_RST_LANE_POS) |
                                ILK_RX_CTRL_RST_CORE;
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_RX_CONTROL, rxctrl);

        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_TX_META_CTRL, 0x07ff07ff);

        genctrl = nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_GENERAL_CTRL1);
        genctrl &= ~(0xF << 8) | 0x0F;
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_GENERAL_CTRL1, genctrl);

        txctrl = nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_TX_CONTROL);
        /* txctrl &= ~(0xf << ILK_TX_CTRL_CAL_LEN_POS); */
        txctrl |= (0xf << ILK_TX_CTRL_CAL_LEN_POS);
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_TX_CONTROL, txctrl);

        val = nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_TX_RATE_LIMIT);
        val |= (0xfff << 12);
        nlm_hal_write_mac_reg(node, blk, INTERLAKEN, ILK_TX_RATE_LIMIT, val);
	
        val = nlm_hal_read_nae_reg(node, TX_SCHED_CTRL);
        val |= 1;
        nlm_hal_write_nae_reg(node, TX_SCHED_CTRL, val);

}

int is_xlp_ilk_lanealigned(int node, int blk)
{
        int i = 0, retval = 1;
	volatile uint32_t status = 0;

        /* check lanes are aligned */
        do {
                status = nlm_hal_read_mac_reg(node, blk, INTERLAKEN, ILK_RX_STATUS3);
    	        if (++i > 1000) {
			retval = 0;
			break;	
		}
        }while(!(status & ILK_RX_STAT3_RXL_ALIGN));
#ifdef INTERLAKEN_DEBUG
        xlp_debug_interlaken(node, blk);
        dump_interlaken_regs(node, blk);
#endif
        return retval;
}

/**
* @brief nlm_hal_ilk_pcs_init configures the lanes of INTERLAKEN complexes and waits for Tx/Rx clock stable.
*
* @param [in] node Node number
* @param [in] ilk_complex_map Bitmap of NAE complexes to configure for Interlaken PCS
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/

int nlm_hal_ilk_pcs_init(int node, uint32_t ilk_complex_map)
{
        int lane_status;
	int block;
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[node];
	
        if (!is_nlm_xlp8xx()) {
                nlm_print("Internlaken is not supported \n");
                return -1;
        }

	if (nae_cfg == NULL) {
		nlm_print("NAE configuration is invalid\n");
		return -1;
	}

	for(block=0; block < XLP8XX_MAX_NAE_COMPLEX; block+=2) {
		if (ilk_complex_map & (1<<block)) {				
	        	xlp8xx_ilk_reset_pll(node, block , nae_cfg->num_lanes[block/2], nae_cfg->lane_rate[block/2]);
		}
	}

	for(block=0; block < XLP8XX_MAX_NAE_COMPLEX; block++) {
		if (!(ilk_complex_map & (1<<block))) {
			block++;
			continue;
		}
                /* Wait for Rx & TX clock stable */
		for( lane_status = PHY_LANE_0_STATUS; lane_status <= PHY_LANE_3_STATUS; lane_status++) {
			/* Wait for TX clock to be set */
			while ((nlm_hal_read_mac_reg(node, block, PHY, lane_status) & (PHY_LANE_STAT_STCS)) != PHY_LANE_STAT_STCS) {
                        }
                        nlm_print("%s Blk%d lane%d got TX clock stable\n", __func__, block, lane_status);

			/* Wait for RX clock to be set */
			while ((nlm_hal_read_mac_reg(node, block, PHY, lane_status) & (PHY_LANE_STAT_SRCS)) != PHY_LANE_STAT_SRCS) {
                        }
                        nlm_print("%s Blk%d lane%d got RX clock stable\n", __func__, block, lane_status);
                }
		if (nae_cfg->num_lanes[block/2] <= 4)
			block++;
        }

        return 0;
}

/**
* @brief nlm_hal_get_hwport function finds the interface number for a given context.
*
* @param [in] node Node number
* @param [in] context Context number
*
* @return
*  - Interface number
* 
* @ingroup hal_nae
*
*/
uint16_t nlm_hal_get_hwport(int node, uint32_t context)
{
	uint32_t rxbase = 0, rxbase1=0;
	int i, port = 0;

	for(i=0; i < 10; i++) {
		rxbase = nlm_hal_read_nae_reg(node, RX_IF_BASE_CONFIG_0 + i);
		if ((context >= (rxbase & 0x3FF)) && ( context < ((rxbase >> 16) & 0x3FF)))
			return port;
		port++;
		rxbase1 = nlm_hal_read_nae_reg(node, RX_IF_BASE_CONFIG_0 + i + 1);
		if ((context >= ((rxbase >> 16) & 0x3FF)) && ( context <  (rxbase1 & 0x3FF)))
			return port;	
		port++;		
	}
	return MAX_GMAC_PORT;
}


int nlm_hal_init_if(int node, int intf_type, int  block, uint32_t *regs, int num_regs)
{
	int i;
	if (regs == NULL) {
		return -1;
	}
	if (intf_type == INTERLAKEN_IF) {
		return -1;
	}
	/* Initialize the regs */
	for (i = 0; i < num_regs; ++i) {
		/*		nlm_hal_write_nae_reg(block, regs[2*i], regs[2*i + 1]); */
	}
	return 0;
}

/**
* @brief init_netior function is used to initialize the network IO ring.
*
* @param [in] node Node number
* @param [in] type Interface type (SGMII, XAUI, Interlaken..)
*
* @return
* 	- 0 on success
* 
* @ingroup hal_nae
*
*/
static int init_netior(int node, int type)
{
	nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_SOFTRESET, 0);
	if (is_nlm_xlp3xx() || is_nlm_xlp2xx()) {
		nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_MISC_REG3_ADDR , 0x0 );
                nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_MISC_REG2_ADDR , 0x0707 );
                nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_MISC_REG1_ADDR , 0x00ff );
	}
	else {
		nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_MISC_REG3_ADDR , (0x0 | (0x0007 << 18) ) );
		nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_MISC_REG2_ADDR , 0x07070707 );
		nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_MISC_REG1_ADDR , 0x00fffff );
	}
	nlm_hal_write_nae_iface_reg(node, 0xf, NETIOR_MISC_REG1_ADDR , 0x0);

	return 0;
}

/* Ingress Config
 *      20 queue (1000 - 1019)
 *      RxConfig : set the Free in desc default
 *      Interface to context mapping(RX_IF_BASE_CONFIG0..8)
 *      set valid active interface
 *      parser configuration
 *      Free-Fifo pool to context (FREE_IN_FIFO_CFG)
 *      Parser se
 *	default value for desc_size is 5504 in 64-bit
 *
 *
 * */
/**
* @brief init_ingress function is used to initialize the NAE ingress path to default values.
*
* @param [in] node Node number
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
void nlm_hal_init_ingress(int node, int desc_size)
{
	unsigned int rx_cfg = 0;
	unsigned int parser_threshold = 384;

	if (!desc_size)
#ifdef CONFIG_64BIT
		desc_size = 5504; /* to support 16384(mtu)/(max 3 P2Ds) and cacheline aligned */
#else
		desc_size = 3204; /* to support 16384(mtu)/(max 3 P2Ds) and cacheline aligned */
#endif

	rx_cfg = nlm_hal_read_nae_reg(node, RX_CONFIG);

	/* log_dbg("nae rxcfg %x txcfg %x\n", rx_cfg, tx_cfg); */
#define NAE_MAX_MESSAGE_SIZE(x)                 (((x) & 0x3)<<1)
#define RESET_MAX_MESSAGE_SIZE                   ~(0x3<<1)
#define NAE_FRINDESCCLSIZE(x)                   (((x)  & 0xff)<< 4 )
#define RESET_FRINDESCCLSIZE                   ~((0xff)<< 4)
#define NAE_RX_STATUS_MASK(x)                   (((x) & 0x7f) << 24)
#define RESET_RX_STATUS_MASK                   ~((0x3f) << 24)

	nlm_hal_write_nae_reg(node,  RX_CONFIG,(rx_cfg &
					  RESET_MAX_MESSAGE_SIZE &
					  RESET_FRINDESCCLSIZE &
					  RESET_RX_STATUS_MASK
				       ) |
			       NAE_RX_ENABLE|
			       NAE_MAX_MESSAGE_SIZE(0x0)|
			       NAE_RX_STATUS_MASK(0x43)|
			       NAE_FRINDESCCLSIZE(desc_size/64)
		);

#define PARSER_THRESHOLD(x) ((x)  & 0x3ff)
#define PARSER_THRESHOLD_DIV_DESCSIZE(x) ( ((x) & 0xff) << 12)
#define PARSER_THRESHOLD_MOD_DESCSIZE_CL(x) ( ((x) & 0xff) << 20)

	nlm_hal_write_nae_reg(node, XLP_PARSER_CONFIG,
			       PARSER_THRESHOLD(parser_threshold) |
			       PARSER_THRESHOLD_DIV_DESCSIZE((parser_threshold/desc_size) + 1) |
			       PARSER_THRESHOLD_MOD_DESCSIZE_CL( (parser_threshold/64)%desc_size) );
}

/**
* @brief init_egress function is used to initialize the NAE egress path to default values.
*
* @param [in] node Node number
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
static void init_egress(int node)
{
	uint32_t tx_cfg =  nlm_hal_read_nae_reg(node, TX_CONFIG);

	if ((is_nlm_xlp3xx()) || is_nlm_xlp2xx() || (is_nlm_xlp8xx_bx())) {
		nlm_hal_write_nae_reg(node, TX_CONFIG, tx_cfg | NAE_TX_ENABLE | NAE_TX_ACE  | NAE_TX_COMPATIBLE | (1 << 3));
	}
	else {
		nlm_hal_write_nae_reg(node, TX_CONFIG, tx_cfg | NAE_TX_ENABLE | NAE_TX_ACE);
	}
}


/**
* @brief nlm_hal_open_if function onfigures the internal PCS and MAC for an interface.
*
* @param [in] node Node number
* @param [in] intf_type Interface type (SGMII, XAUI, Interlaken..)
* @param [in] block (Interface number)
*
* @return
* 	- 0 on success
* 
* @ingroup hal_nae
*
*/
int nlm_hal_open_if(int node, int intf_type, int block)
{
	unsigned int netwk_inf  = 0;
	unsigned int tx_config = 0;
	int tx_ior_credit = 0;
	uint32_t ifmask = 0;
	unsigned int mac_cfg1 = 0;
	unsigned int netior_ctrl3 = 0;

#if !defined(XLP_SIM) || defined(NLM_BOARD)
	int hw_portid ;
#endif

	NAE_DEBUG("%s: %d node %d ",__func__,block, node);
	/* Init Netior ... Need to fixed */
	init_netior(node, intf_type);

	NAE_DEBUG("intf_type:%d\n", intf_type);
	switch(intf_type) {
		case XAUI_IF:
		case RXAUI_IF:
			/* block is a complex number */
			netwk_inf = nlm_hal_read_mac_reg(node, block, XGMAC, NETIOR_XGMAC_CTRL1);
			netwk_inf |= (1 << NETIOR_XGMAC_STATS_CLR_POS);
			nlm_hal_write_mac_reg(node, block, XGMAC, NETIOR_XGMAC_CTRL1, netwk_inf);
			ifmask = 0xf << (block);
			tx_ior_credit = nlm_hal_read_nae_reg(node, TX_IORCRDT_INIT);
			nlm_hal_write_nae_reg(node, TX_IORCRDT_INIT, tx_ior_credit | ifmask);
		        tx_config = nlm_hal_read_nae_reg(node, TX_CONFIG);
			/* need to toggle these bits for credits to be loaded */
		 	nlm_hal_write_nae_reg(node, TX_CONFIG, tx_config | ( TXINITIORCR(ifmask)));
		        nlm_hal_write_nae_reg(node, TX_CONFIG, tx_config & ~( TXINITIORCR(ifmask)));
			break;
		case INTERLAKEN_IF:
			/* block is a complex number */
			ifmask = 0xff << (block);   /* based on number of lanes */
			tx_ior_credit = nlm_hal_read_nae_reg(node, TX_IORCRDT_INIT);
			nlm_hal_write_nae_reg(node, TX_IORCRDT_INIT, tx_ior_credit | (ifmask));
                        tx_config = nlm_hal_read_nae_reg(node, TX_CONFIG);
			/* need to toggle these bits for credits to be loaded */
	                nlm_hal_write_nae_reg(node, TX_CONFIG, tx_config | ( TXINITIORCR(ifmask)));
                        nlm_hal_write_nae_reg(node, TX_CONFIG, tx_config & ~( TXINITIORCR(ifmask)));
			break;
		case SGMII_IF:
			tx_ior_credit = nlm_hal_read_nae_reg(node, TX_IORCRDT_INIT);
			nlm_hal_write_nae_reg(node, TX_IORCRDT_INIT, tx_ior_credit & (~ (1<<block)));
			tx_config = nlm_hal_read_nae_reg(node, TX_CONFIG);
			/* need to toggle these bits for credits to be loaded */
			nlm_hal_write_nae_reg(node, TX_CONFIG, tx_config | ( TXINITIORCR(1<<block)));
			nlm_hal_write_nae_reg(node, TX_CONFIG, tx_config & (~( TXINITIORCR(1<<block))));
	
			/* init phy id to access internal PCS */
		        netwk_inf = read_gmac_reg(node, block, NETWK_INF_CTRL_REG);
        		netwk_inf &= 0x7ffffff;
		        netwk_inf |= ((block) << 27);
        		write_gmac_reg(node, block, NETWK_INF_CTRL_REG, netwk_inf);

			/* Sofreset set bit 11 to 0  */

			write_gmac_reg(node, block , NETWK_INF_CTRL_REG,  netwk_inf & 0xfffff7ff);

			/* Reset GMAC  */
			mac_cfg1 = read_gmac_reg(node, block, MAC_CONF1);
		        write_gmac_reg(node, block , MAC_CONF1, mac_cfg1 | INF_SOFTRESET(1) |
					           INF_RX_ENABLE(1) |
						   INF_TX_ENABLE(1));

			/* Default 1G */
		        write_gmac_reg(node, block , MAC_CONF2,  INF_PREMBL_LEN(0x7) |
                        		     	INF_IFMODE(2)  |
                	               		INF_FULLDUP(1) |
		  			        INF_PADCRCEN(1));

			/* Clear GMAC reset */
			mac_cfg1 = read_gmac_reg(node, block, MAC_CONF1);
		        write_gmac_reg(node, block , MAC_CONF1, mac_cfg1 & ~(INF_SOFTRESET(1)));

			/* Clear speed debug bit */
		        netior_ctrl3 = read_gmac_reg(node, block, NETWK_INF_CTRL3_REG);
		        write_gmac_reg(node, block, NETWK_INF_CTRL3_REG, netior_ctrl3 & (~(1<<6)));
			/* nlm_print("CLEARING speed bit =0x%x\n", read_gmac_reg(node, block, NETWK_INF_CTRL3_REG)); */

#if !defined(XLP_SIM) || defined(NLM_BOARD)
			register_phy(node, block, &hw_portid);
			if(hw_portid<0)	
				goto init_inf;
			/* init external phy, bypass SGMII auto negotiation */
			NAE_DEBUG("init ext phy:%d\n", block);
			nlm_hal_init_ext_phy(node, block);
#endif
			/* Configure speed and mode */
			nlm_hal_config_sgmii_if(node, block);
			break;
		default:
			nlm_print("Unknown interface type\n");	
			return 0;
	}
#if !defined(XLP_SIM) || defined(NLM_BOARD)
init_inf:
#endif

	nlm_hal_init_ingress (node, 0);
	init_egress(node);

	return 0;
}

int nlm_hal_close_if(int node, int intf_type, int  block)
{
	if (intf_type != SGMII_IF) {
		return -1;
	}
	/* Turn off TX, RX enables */
	return 0;
}


/**
* @brief nlm_hal_mac_disable function is used to disable an interface at the MAC level.
*
* @param [in] node Node number
* @param [in] block (Interface number)
* @param [in] intf_type SGMII_IF, XAUI_IF, INTERLAKEN_IF
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
void nlm_hal_mac_disable(int node, int block, int intf_type)
{
        unsigned int mac_cfg1 = 0, xaui_cfg = 0;
        unsigned int netwk_inf = 0;

	switch(intf_type) {
		case SGMII_IF:
			mac_cfg1 = read_gmac_reg(node, block, MAC_CONF1);
		        write_gmac_reg(node, block , MAC_CONF1, mac_cfg1 & ~(0x5));
              		netwk_inf = read_gmac_reg(node, block, NETWK_INF_CTRL_REG);
              		write_gmac_reg(node, block ,NETWK_INF_CTRL_REG, netwk_inf &  (~(TX_EN(1))));
			break;
		case XAUI_IF:
		case RXAUI_IF:
			xaui_cfg=nlm_hal_read_mac_reg(node, block, XGMAC, XAUI_CONFIG_1);
			nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_CONFIG_1, xaui_cfg &
                                                        (~(XAUI_CONFIG_TFEN | XAUI_CONFIG_RFEN)));
			break;
		case INTERLAKEN_IF:
			break;
	}
}

/**
* @brief nlm_hal_mac_enable function is used to enable an interface at the MAC level.
*
* @param [in] block (Interface number)
* @param [in] intf_type SGMII_IF, XAUI_IF, INTERLAKEN_IF
*
* @return
* 	- none
* 
* @ingroup hal_nae
*
*/
void nlm_hal_mac_enable(int node, int block, int intf_type)
{
        unsigned int mac_cfg1 = 0, xaui_cfg = 0;
        unsigned int netwk_inf = 0;

        switch(intf_type) {
                case SGMII_IF:
			netwk_inf  = read_gmac_reg(node, block, NETWK_INF_CTRL_REG);
		        write_gmac_reg(node, block , NETWK_INF_CTRL_REG, 
					netwk_inf | TX_EN(1)| STATS_EN(1));
        		mac_cfg1 = read_gmac_reg(node, block, MAC_CONF1);
        		write_gmac_reg(node, block , MAC_CONF1, mac_cfg1 | (0x5));
                        break;
                case XAUI_IF:
                case RXAUI_IF:
			xaui_cfg=nlm_hal_read_mac_reg(node, block, XGMAC, XAUI_CONFIG_1);
	                nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_CONFIG_1, xaui_cfg |
							XAUI_CONFIG_TFEN | XAUI_CONFIG_RFEN | XAUI_CONFIG_TCTLEN | XAUI_CONFIG_RCTLEN);                               

                        break;
                case INTERLAKEN_IF:
                        break;
        }
}

static void get_dom_nae_property(void *fdt, int dom_id, char *ppty, unsigned int *val, int valsz)
{
	char dom_node_str[32];
	int nodeoffset;
	unsigned int *pval;
	int plen, i;


	sprintf(dom_node_str, "/doms/dom@%d/nae", dom_id);
	nodeoffset = fdt_path_offset(fdt, dom_node_str);
	if(nodeoffset >= 0) {
		pval = (unsigned int *)fdt_getprop(fdt, nodeoffset, ppty, &plen);
		if(pval != NULL) {
			if(plen > valsz)
				plen = valsz;
			for(i = 0; i < plen / sizeof(unsigned int); i++)
				val[i] = fdt32_to_cpu(pval[i]);
		}
	}
}
/* Returns 1 if the domain is the nae owner, 
 Returns 0 otherwise */
static int get_dom_nae_node_ownership(void *fdt, int dom_id, int node, struct nlm_hal_nae_config *nae_cfg)
{
	unsigned int nae_owner[NLM_MAX_NODES];
	int i;
	uint32_t owner_mask;

	for(i = 0; i < NLM_MAX_NODES; i++)
		nae_owner[i] = 1; /* default is owned */	

	owner_mask = get_dom_owner_mask(fdt, dom_id, "nae");
	for (i=0; i<NLM_MAX_NODES; i++)
	{
		nae_owner[i] = ((owner_mask &(1<<i)) ? 1:0);
		/* nlm_print("nae owner for node %d, is %d.\n", i, nae_owner[i]); */
	}

	/* nlm_print("nae ownder for node %d is %d \n", node, nae_owner[node]); */
	nae_cfg->owned  = nae_owner[node];
	return nae_owner[node];
}



static void extract_dom_nae_node_freein_fifo_info(void *fdt, int dom_id, int node, struct nlm_hal_nae_config *nae_cfg)
{
	char dom_node_str[128];
	int i;
	unsigned int freein_fifo_mask[NLM_MAX_NODES]; 
	unsigned int freein_fifo_onchip_num_descs[MAX_NAE_FREEIN_DESCS_QUEUE];
	unsigned int freein_fifo_spill_num_descs = 0;
	unsigned int data[4];
	int frin_total_queue = nlm_hal_get_frin_total_queue(node);

	for (i=0; i<MAX_NAE_FREEIN_DESCS_QUEUE; i++)
		freein_fifo_onchip_num_descs[i] = 0;

	for(i = 0; i < NLM_MAX_NODES; i++) 
		freein_fifo_mask[i] = (1 << frin_total_queue) - 1;/* Ownes all fifos by default*/

	sprintf(dom_node_str, "/soc/nae@node-%d/freein-fifo-config", node);
	copy_fdt_prop(fdt, dom_node_str, "freein-fifo-onchip-num-descs", PROP_CELL, 
			freein_fifo_onchip_num_descs, 4*MAX_NAE_FREEIN_DESCS_QUEUE);
	copy_fdt_prop(fdt, dom_node_str, "freein-fifo-spill-num-descs", PROP_CELL,
			&freein_fifo_spill_num_descs, 4);

	nae_cfg->freein_fifo_spill_num_descs = freein_fifo_spill_num_descs;
	
	for (i=0; i<MAX_NAE_FREEIN_DESCS_QUEUE; i++) {
		nae_cfg->freein_fifo_onchip_num_descs[i] = freein_fifo_onchip_num_descs[i];
	}

		get_dom_nae_property(fdt, dom_id, "freein-fifo-mask", freein_fifo_mask, sizeof(freein_fifo_mask));
	
	nae_cfg->freein_fifo_dom_mask = freein_fifo_mask[node];

	data[node]  = 0;
	get_dom_nae_property(fdt, dom_id, "vfbtbl-sw-offset", data, sizeof(data));
	nae_cfg->vfbtbl_sw_offset = data[node];
	data[node]  = 0;
	get_dom_nae_property(fdt, dom_id, "vfbtbl-sw-nentries", data, sizeof(data));
	nae_cfg->vfbtbl_sw_nentries = data[node];

	data[node]  = 0;
	get_dom_nae_property(fdt, dom_id, "vfbtbl-hw-offset", data, sizeof(data));
	nae_cfg->vfbtbl_hw_offset = data[node];
	data[node]  = 0;
	get_dom_nae_property(fdt, dom_id, "vfbtbl-hw-nentries", data, sizeof(data));
	nae_cfg->vfbtbl_hw_nentries = data[node];

}

void nlm_hal_derive_cpu_to_freein_fifo_map(int node, 
		unsigned int phys_cpu_map, 
		unsigned int freein_fifo_mask, unsigned int *cpu_2_freein_fifo_map)
{
	int i=0, j =0, filled = 0, index = 0, pcpu, fifo;
	unsigned char lcpu_2_pcpu[NLM_NCPUS_PER_NODE], pcpu_2_fifo[NLM_NCPUS_PER_NODE];
	int frin_total_queue = nlm_hal_get_frin_total_queue(node);

	if((phys_cpu_map == 0) || (freein_fifo_mask == 0))
		return;
	
	memset(lcpu_2_pcpu, 0, sizeof(lcpu_2_pcpu));
	memset(pcpu_2_fifo, 0, sizeof(pcpu_2_fifo));

	while(i < NLM_NCPUS_PER_NODE){
		for(j=0; j < NLM_NCPUS_PER_NODE && i < NLM_NCPUS_PER_NODE; j++){
			if(!(phys_cpu_map & (1 << j)))
				continue;
			lcpu_2_pcpu[i] = j;
			i++;
		}
	}
	i = 0;
	while(filled < NLM_NCPUS_PER_NODE) {
		for(j = 0; j < frin_total_queue && filled < NLM_NCPUS_PER_NODE;) {
			if(!(freein_fifo_mask & (1 << j))) {
				j++;
				continue;
			}

			pcpu = lcpu_2_pcpu[i];
			if(pcpu_2_fifo[pcpu] & 0x80) {
				fifo = pcpu_2_fifo[pcpu] & 0x1f;
			} else {
				fifo = j;
				pcpu_2_fifo[pcpu] = 0x80 | fifo;
				j++;
			}
			cpu_2_freein_fifo_map[i] = fifo;	
			i += NLM_NUM_THREADS_PER_CORE;
			if(i >= NLM_NCPUS_PER_NODE)
				i = ++index;
			filled++;
		}
	}
}

static int prepare_nae_shared_info(void *fdt, int dom_id, int node, 
		struct nlm_hal_nae_config *nae_cfg)
{
	char dom_node_str[128];
	int nodeoffset;
	unsigned int *pval;
	int plen, i, j;
	int pos, bitoff, max;
	unsigned int map[NLM_MAX_NODES], fmask[NLM_MAX_NODES];
	struct nlm_nae_shinfo shinfo;
	static int shdomains[NLM_NAE_MAX_SHARED_DOMS], shdom_extracted = 0;
	int domid = dom_id, cnt = 0;
	int frin_total_queue = nlm_hal_get_frin_total_queue(node);
	unsigned int lcpu_2_pcpu[NLM_NCPUS_PER_NODE], cpu_2_fifo[NLM_NCPUS_PER_NODE];

	if(!shdom_extracted) {
		for(i = 0; i < NLM_NAE_MAX_SHARED_DOMS; i++)
			shdomains[i] = -1;
		get_dom_nae_property(fdt, dom_id, "sh-domains", (unsigned int *)shdomains, sizeof(shdomains));
		shdom_extracted = 1;
	}

		
start_for_dom:
	map[node] = 0;
	for(i = 0; i < NLM_MAX_NODES; i++) 
		fmask[i] = (1 << frin_total_queue) - 1;/* Ownes all fifos by default*/

	memset((char *)&shinfo, 0, sizeof(shinfo));
	memset(lcpu_2_pcpu, 0, sizeof(lcpu_2_pcpu));
	
	sprintf(dom_node_str, "/doms/dom@%d/cpu", domid);
	nodeoffset = fdt_path_offset(fdt, dom_node_str);
	if(nodeoffset >= 0) {
		pval = (unsigned int *)fdt_getprop(fdt, nodeoffset, "onlinemask", &plen);
		if(pval != NULL) {
			if(plen > sizeof(map))
				plen = sizeof(map);
			/* cpu mask is from msb to lsb, but we require in lsb to msb */
			max = (plen / sizeof(unsigned int)) - 1;
			for(i = 0; i < plen / sizeof(unsigned int); i++, max--)
				map[i] = fdt32_to_cpu(pval[max]);
		}
		pval = (unsigned int *)fdt_getprop(fdt, nodeoffset, "nae-rx-vc", &plen);
		if(pval)
			shinfo.rxvc = fdt32_to_cpu(pval[0]);
		else
			goto err;
	}

	/* Extract free in fifo mask mask */
		get_dom_nae_property(fdt, domid, "freein-fifo-mask", fmask, sizeof(fmask));

#ifdef NAE_SH_INFO_DBG
	nlm_print("%s domid %d node %d cpumask %x fmask %x\n", __FUNCTION__, domid, node,
			map[node], fmask[node]);
#endif

	if(!(map[node] && fmask[node]))
		goto err;

	if(map[node]) {
		i = 0;
		while(i < NLM_NCPUS_PER_NODE){
			for(j=0; j < NLM_NCPUS_PER_NODE && i < NLM_NCPUS_PER_NODE; j++){
				if(!(map[node] & (1 << j)))
					continue;
				pos = i / NLM_NAE_SH_LCPU_TO_MAP_SZ;
				bitoff = (i % NLM_NAE_SH_LCPU_TO_MAP_NVALS_PER_ENTRY) * 
					NLM_NAE_SH_LCPU_TO_MAP_SNG_VAL_SZ;
				shinfo.lcpu_2_pcpu_map[pos] |= (j << bitoff);
				lcpu_2_pcpu[i] = j;
				i++;
			}
		}

#ifdef NAE_SH_INFO_DBG
		for(i = 0; i < NLM_NCPUS_PER_NODE; i++) {
			pos = i / NLM_NAE_SH_LCPU_TO_MAP_SZ;
			bitoff = (i % NLM_NAE_SH_LCPU_TO_MAP_NVALS_PER_ENTRY) * 
				NLM_NAE_SH_LCPU_TO_MAP_SNG_VAL_SZ;
			nlm_print("domid %d node %d lcpu %d pcpu %d (%d)\n", domid,
					node, i, (shinfo.lcpu_2_pcpu_map[pos] >> bitoff) & 0x1f, lcpu_2_pcpu[i]);
		}
#endif
	}

	/* We need to avoid usage same fifo in different cores if possible. Also the filling should 
	be physical cpu id based, as the application knows only phys cpu id 
	 */
	if(fmask[node]) {
		nlm_hal_derive_cpu_to_freein_fifo_map(node, map[node], fmask[node], cpu_2_fifo);
		for(i = 0; i < NLM_NCPUS_PER_NODE; i++) {
			pos = i / NLM_NAE_SH_LCPU_TO_MAP_SZ;
			bitoff = (i % NLM_NAE_SH_LCPU_TO_MAP_NVALS_PER_ENTRY) * 
				NLM_NAE_SH_LCPU_TO_MAP_SNG_VAL_SZ;
			shinfo.cpu_2_freeinfifo_map[pos] |= (cpu_2_fifo[i] << bitoff);
		}
#ifdef NAE_SH_INFO_DBG
		for(i = 0; i < NLM_NCPUS_PER_NODE; i++) {
			pos = i / NLM_NAE_SH_LCPU_TO_MAP_SZ;
			bitoff = (i % NLM_NAE_SH_LCPU_TO_MAP_NVALS_PER_ENTRY) * 
				NLM_NAE_SH_LCPU_TO_MAP_SNG_VAL_SZ;
			nlm_print("domid %d node %d lcpu %d rxfifo %d\n", domid, 
					node, i, (shinfo.cpu_2_freeinfifo_map[pos] >> bitoff) & 0x1f);
		}
#endif

	}

	shinfo.domid = domid;
	shinfo.valid = 1;
	memcpy((char *)&nae_cfg->shinfo[cnt], (char *)&shinfo, sizeof(shinfo));
err:

	while(cnt < NLM_NAE_MAX_SHARED_DOMS) {
		if((domid = shdomains[cnt]) >= 0) 
			break;
		cnt++;
	}
	if(cnt < NLM_NAE_MAX_SHARED_DOMS)  {
		cnt++;
		goto start_for_dom;
	}
	return 0;
}

/**
* @brief nlm_hal_init_nae function is the main function for initializing and configuring the NAE and POE, based on the configuration in FDT.
*
* @param [in] fdt Pointer to the FDT
* @param [in] dom_id Domain number to parse
*
* @return
*  - none
* 
* @ingroup hal_nae
*
*/
int nlm_hal_init_nae(void *fdt, int dom_id)
{
	int i = 0, max_ports = 0;
	int context = 0, ctxsize = 0, offset=0;
	uint32_t bar0;
	int node, owned;
	struct nlm_hal_nae_config *nae_cfg;
	uint64_t base, size;

	if (check_header(fdt)) {
		nlm_print("Sanity check on FDT blob failed! Aborting\n");
		return -1;
	}

	nlm_print("number of nodes %d \n", nlm_node_cfg.num_nodes);
	for (node= 0; node<nlm_node_cfg.num_nodes; node++) {
		if (nlm_node_cfg.nae_cfg[node] == NULL) {
			nlm_node_cfg.nae_cfg[node] = nlm_malloc(sizeof(struct nlm_hal_nae_config));
		
			nae_cfg = nlm_node_cfg.nae_cfg[node];
			if (nae_cfg == NULL) {
				nlm_print("Memory allocation failed for nae_cfg \n");
				return -1;
			}
			memset(nae_cfg, 0, sizeof(struct nlm_hal_nae_config));
		}
		else {
			nae_cfg = nlm_node_cfg.nae_cfg[node];
			base = nae_cfg->freein_spill_base;
			size = nae_cfg->freein_spill_size;
			memset(nae_cfg, 0, sizeof(struct nlm_hal_nae_config));
			nae_cfg->freein_spill_base = base;
			nae_cfg->freein_spill_size = size;
		}

		cntx2port[node] = &nae_cfg->cntx2port[0];

		bar0 = nlm_hal_read_32bit_reg(nlm_hal_get_dev_base(node, 0, XLP_NAE_DEVICE, XLP_NAE_FUNC), 0x4);
		bar0 = membar_fixup(bar0);

		/* Initialize default configuration */
		for (i = 0; i < 18; i++) {
			nae_cfg->fb_vc = 1;
			nae_cfg->rx_vc = 0;
			nae_cfg->ports[i].valid = 0;
			nae_cfg->ports[i].mgmt = 0;
		}

		for (i = 0; i < MAX_NAE_CONTEXTS; i++) {
			/* 18 is an invalid port */
			nae_cfg->cntx2port[i] = 18;
		}

		owned = get_dom_nae_node_ownership(fdt, dom_id, node, nae_cfg);
		extract_dom_nae_node_freein_fifo_info(fdt, dom_id, node, nae_cfg);
		prepare_nae_shared_info(fdt, dom_id, node, nae_cfg);
	
		if(owned) {
			/* frin_fifo represents the 20 pools of free-in descriptor fifos */
			/* drain_nae_stray_packets(node); */
			drain_nae_frin_fifo_descs(node);
			deflate_frin_fifo_carving(node);

#if !defined(NLM_HAL_UBOOT)
		/* For U-Boot, reset_nae clears all IO BARs
		 * which messes up Flash/CPLD/GBU BARs etc
		 */
			reset_nae(node);
			nae_cfg->flags |= NAE_RESET_DONE;
			reset_poe(node);
#else
			nae_reset_done[node] = 1;
#endif

			nlm_hal_write_32bit_reg(nlm_hal_get_dev_base(node, 0, XLP_NAE_DEVICE, XLP_NAE_FUNC), 0x4, bar0);

			nlm_print("Configuring ucore...\n");
			parse_ucore_config(fdt, node);

		        nlm_print("Configuring CPU-NAE...\n");
		} else
			nlm_print("Extracting CPU-NAE configuration...\n");
		
        	parse_fdt_cpu_config(fdt, dom_id, nae_cfg);

		if(owned) {
			parse_fdt_nae_config(fdt, node, nae_cfg);
#if defined(NLM_HAL_UBOOT)
			if(nae_cfg->num_ports)
#endif
			{
				nlm_print("Configuring PoE...\n");
				parse_poe_config(fdt, node);
				nlm_print("NAE configuration done!\n");
			}
		} else
			parse_port_config(fdt, node, nae_cfg);  


		max_ports = nae_cfg->num_ports;

		for (i = 0, context = 0; i < max_ports; i++) {
			struct nlm_hal_nae_port *port = &nae_cfg->ports[i];
#ifdef NLM_HAL_LINUX_KERNEL
			int port_num, txq;
#endif

			if (!port->valid) continue;

			ctxsize = nae_cfg->ports[i].num_channels;
#ifdef NLM_HAL_LINUX_KERNEL
                	if (nae_cfg->ports[i].iftype == INTERLAKEN_IF) {
                        	nae_cfg->cntx2port[context] = i;
	                        nae_cfg->ports[i].num_free_desc /= ctxsize;
        	                nae_cfg->ports[i].num_channels = 1;
				nae_cfg->ports[i].ext_phy_addr = 0;		/* cortina logical port id */
                	        txq = nae_cfg->ports[i].txq;

	                        for(offset=1; offset < ctxsize; offset++) {
        	                        port_num = nae_cfg->num_ports;
                	                nae_cfg->cntx2port[context + offset] = port_num;
                        	        memcpy(&nae_cfg->ports[port_num], &nae_cfg->ports[i], sizeof(struct nlm_hal_nae_port));
                                	nae_cfg->ports[port_num].txq = ++txq;
					nae_cfg->ports[port_num].ext_phy_addr = offset;
	                                nae_cfg->num_ports++;
                	        }
	                }
        	        else {
#endif
				if(nae_cfg->ports[i].iftype == RXAUI_IF)
                                        nae_cfg->ports[i].iftype = XAUI_IF; /*initialization done for RXAUI. Now it`ll be considered as XAUI.*/

				for(offset=0; offset < ctxsize; offset++)
				nae_cfg->cntx2port[context + offset] = i; /* logical port */
				NAE_DEBUG("save ctx:%d port:%d\n", context+offset, i);
#ifdef NLM_HAL_LINUX_KERNEL
        	        }
#endif
	
			context += ctxsize;
	
		}

#ifdef INCLUDE_NAE_DEBUG
#if defined(NLM_HAL_UBOOT)
		if(max_ports)
#endif
		{
			dump_nae_cfg_info(node, nae_cfg);
			if(owned) {
				nlm_print("FRIN desc carving after HAL init.\n");
				print_frin_desc_carving(node);
			}
		}
#endif
		if(is_nlm_xlp2xx() && nae_cfg->msec_port_enable)
		{
			nlm_hal_msec_tx_default_config(node, nae_cfg->msec_port_enable,
			/*preamble_len*/ 0, /*packet_num*/ 0x1, /*pn_thrshld*/0x7fffffff);
			nlm_hal_msec_rx_default_config(node, nae_cfg->msec_port_enable,
			/*preamble_len*/ 0, /*packet_num*/ 0x1, /*replay_win_size*/0x64);
		}
		else
			nae_cfg->msec_port_enable = 0;
	}
	
	return 0;
}

int nlm_hal_restart_ucore(int node, void *fdt)
{
	if (nlm_node_cfg.nae_cfg[node] == NULL)
		return -1;
	if(nlm_node_cfg.nae_cfg[node]->owned)
		parse_ucore_config(fdt, node);
	return 0;
}

void nlm_hal_modify_nae_ucore_sram_mem(int node, int ucoreid, unsigned int *data, int off, int words)
{
	int i;
	unsigned int ucore_cfg = nlm_hal_read_nae_reg(node, RX_UCORE_CFG);	
	/*set iram to 0*/
	nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg & ~(1<<7));
	for (i = 0; i < words; ++i, off += 4) {
		nlm_hal_write_ucode(node, ucoreid, off, data[i]);
	}
}

void nlm_hal_read_nae_ucore_sram_mem(int node, int ucoreid, unsigned int *data, int off, int words)
{
	int i;
	unsigned int ucore_cfg = nlm_hal_read_nae_reg(node, RX_UCORE_CFG);	
	/*set iram to 0*/
	nlm_hal_write_nae_reg(node, RX_UCORE_CFG, ucore_cfg & ~(1<<7));
	for (i = 0; i < words; ++i, off += 4) {
		data[i] = nlm_hal_read_ucode(node, ucoreid, off);
	}
}

void nlm_hal_disable_xaui_flow_control(int node, int block)
{
	unsigned int xaui_cfg;
	xaui_cfg = nlm_hal_read_mac_reg(node, block, XGMAC, XAUI_CONFIG_1);
	xaui_cfg &= (~(XAUI_CONFIG_TCTLEN | XAUI_CONFIG_RCTLEN));
	nlm_hal_write_mac_reg(node, block, XGMAC, XAUI_CONFIG_1, xaui_cfg);
}



/* Port enable[7:0] = 0xff default all 8 ports are enables
   preamble _len[23:8] = 0x0000 all port has zero preamblebe len
   if AAD(preamble len is 12) 0x5555
   if AAD(preamble len is 16) 0xaaaa
   based on preamble len, sectag offset will change
*/

struct sec_mac{
	unsigned char mac[6];
};
struct secure_assoc{
	int 	port_num;
	int 	cntx;
	int 	tci;
	uint64_t src;
	uint64_t dst;
};

#define MACSEC_NUM_SA	8

struct secure_assoc sa[MACSEC_NUM_SA] = {

			{0, 0, 0x13, 0x1122334455670000, 0x1122334455660000},
			{1, 1, 0x13, 0x1122334455680000, 0x1122334455670000},
			{2, 2, 0x13, 0x1122334455690000, 0x1122334455680000},
			{3, 3, 0x13, 0x11223344556a0000, 0x1122334455690000},
			{4, 4, 0x13, 0x11223344556b0000, 0x11223344556a0000},
			{5, 5, 0x13, 0x11223344556c0000, 0x11223344556b0000},
			{6, 6, 0x13, 0x11223344556d0000, 0x11223344556c0000},
			{7, 7, 0x13, 0x11223344556e0000, 0x11223344556d0000},
			/*{0, 0, 0x13, 0x1122334455660000, 0x1122334455670000},
			{1, 1, 0x13, 0x1122334455670000, 0x1122334455680000},
			{2, 2, 0x13, 0x1122334455680000, 0x1122334455690000},
			{3, 3, 0x13, 0x1122334455690000, 0x11223344556b0000},
			{4, 4, 0x13, 0x11223344556a0000, 0x11223344556c0000},
			{5, 5, 0x13, 0x11223344556b0000, 0x11223344556d0000},
			{6, 6, 0x13, 0x11223344556c0000, 0x11223344556e0000},
			{7, 7, 0x13, 0x11223344556d0000, 0x11223344556d0000},*/
		/*	{0, 0, 0x13, 0x1122334455660000, 0x1011213141500000},
			{1, 1, 0x13, 0x1122334455770000, 0x1011213141520000},
			{2, 2, 0x13, 0x1122334455880000, 0x1011213141540000},
			{3, 3, 0x13, 0x1122334455990000, 0x1011213141560000},
			{4, 4, 0x13, 0x1122334455aa0000, 0x1011213141580000},
			{5, 5, 0x13, 0x1122334455bb0000, 0x10112131415a0000},
			{6, 6, 0x13, 0x1122334455cc0000, 0x10112131415c0000},
			{7, 7, 0x13, 0x1122334455dd0000, 0x10112131415e0000},
			{1, 1, 0x0b, {"\x11\x22\x33\x44\x55\x77"},  {"\x11\x11\x21\x31\x41\x52"}},
			{2, 2, 0x0b, {"\x11\x22\x33\x44\x55\x88"},  {"\x11\x11\x21\x31\x41\x54"}},
			{3, 3, 0x0b, {"\x11\x22\x33\x44\x55\x99"},  {"\x11\x11\x21\x31\x41\x56"}},
			{4, 4, 0x0b, {"\x11\x22\x33\x44\x55\xaa"},  {"\x11\x11\x21\x31\x41\x58"}},
			{5, 5, 0x0b, {"\x11\x22\x33\x44\x55\xbb"},  {"\x11\x11\x21\x31\x41\x5a"}},
			{6, 6, 0x0b, {"\x11\x22\x33\x44\x55\xcc"},  {"\x11\x11\x21\x31\x41\x5c"}},
			{7, 7, 0x0b, {"\x11\x22\x33\x44\x55\xdd"},  {"\x11\x11\x21\x31\x41\x5e"}},*/
	};

struct cipher_key{
	unsigned char key[16];
};

struct cipher_key tx_ci_key[16] = {
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	/*{"\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22"},
	{"\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33"},
	{"\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44"},
	{"\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55"},
	{"\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66"},
	{"\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77"},
	{"\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88"},
	{"\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99"},
	{"\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"},
	{"\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb"},
	{"\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"},
	{"\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"},
	{"\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee"},
	{"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"},*/
};

struct cipher_key rx_ci_key[16] = {
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
/*	{"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11"},
	{"\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22"},
	{"\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33"},
	{"\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44\x44"},
	{"\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55"},
	{"\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66\x66"},
	{"\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77\x77"},
	{"\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88\x88"},
	{"\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99"},
	{"\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"},
	{"\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb\xbb"},
	{"\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"},
	{"\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"},
	{"\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee\xee"},
	{"\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"},*/
};

#ifdef MACSEC_DEBUG

static void dump_buffer(unsigned char *buf, unsigned int len, unsigned char *msg)
{
    int k = 0;
    nlm_print("\n %s", msg);
    for(k = 0; k < len; k++)
    {
        nlm_print(" %.2x",buf[k]);
        if((k+1) % 16 == 0)
            nlm_print("\n");
    }
    nlm_print("\n");
}
#endif
/* CHANGE HE ORDER OF THE KEY */

void nlm_hal_msec_set_tx_key(int node, int index, unsigned char *key)
{
	unsigned int cntrl_reg_val = (1 << 22 | (index & 0x7ff)), local_key;
	
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_3, local_key);
	key = key+4;
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_2, local_key);
	key = key+4;
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_1, local_key);
	key = key+4;
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_0, local_key);

	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}
/*
void nlm_hal_msec_set_tx_key(int node, int index, unsigned char *key)
{
	unsigned int cntrl_reg_val = (1 << 22 | (index & 0x7ff));
	
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_0, *(unsigned int*)key);
	key = key+4;
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_1, *(unsigned int*)key);
	key = key+4;
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_2, *(unsigned int*)key);
	key = key+4;
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_3, *(unsigned int*)key);

	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}
*/

void nlm_hal_msec_set_tx_param(int node, int index, unsigned int param)
{
	unsigned int cntrl_reg_val = (1 << 22 | 1 << 11 | (index & 0x7ff));
	
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_0, param);
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}
void nlm_hal_msec_set_tx_cntx_to_sa(int node, int cntx, int sa)
{
	unsigned int cntrl_reg_val = (1 << 22 | 2 << 11 | (cntx & 0x7ff));
	
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_0, sa);
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}

void nlm_hal_msec_set_tx_sci(int node, int index, uint64_t sci)
{
	unsigned int cntrl_reg_val = (1 << 22 | 3 << 11 | (index & 0x7ff));
	
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_0, sci);
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}

void nlm_hal_msec_set_tx_tci(int node, int index, int tci)
{
	unsigned int cntrl_reg_val = (1 << 22 | 4 << 11 | (index & 0x7ff));
	
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_DATAREG_0, tci & 0x3f);
	nlm_hal_write_nae_reg(node, TX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}

void nlm_hal_msec_tx_mem_config(int node, int context, int tci, uint64_t sci, unsigned char *key)
{
	int port;
	port = *(cntx2port[node] + context);
#ifdef MACSEC_DEBUG
	nlm_print("%s node = %d, port = %d context = %d, tci = %x, sci = %llx\n",__FUNCTION__, node, port, context, tci, sci);
	dump_buffer(key, 16, "tx_key:");
#endif
	nlm_hal_msec_set_tx_cntx_to_sa(node, context, port);
	nlm_hal_msec_set_tx_sci(node, port, sci);
	nlm_hal_msec_set_tx_tci(node, port, tci);
	nlm_hal_msec_set_tx_key(node, (port << 1)/*index<<1 | AN */, key);/* AN = 0*/
	nlm_hal_msec_set_tx_key(node, ((port << 1)| 1)/*index<<1 | AN */, key);/* AN = 1 */
}


void nlm_hal_msec_tx_config(int node, unsigned int port_enable, unsigned int preamble_len, unsigned int packet_num, unsigned int pn_thrshld)
{
	unsigned int sec_tag_offset[2], i = 0, port = 0, param, icv_len = 16, sectag_len = 0x8;
	uint64_t sec_tag_preamble;
	struct nlm_hal_nae_config *nae_cfg;

	nae_cfg = nlm_node_cfg.nae_cfg[node];

	sec_tag_offset[0] = 0x0c0c0c0c;
	sec_tag_offset[1] = 0x0c0c0c0c;
#ifdef MACSEC_DEBUG
	nlm_print("%s node = %d port_enable = %x preamble_len = %x packet_num = %d pn_thrshld = %d\n", __FUNCTION__, node, port_enable, preamble_len, packet_num, pn_thrshld);
#endif
	nlm_hal_write_nae_reg(node, TX_MSEC_BYPASS, 0x0);
	nlm_hal_write_nae_reg(node, TX_MSEC_ETHER_TYPE, 0x88e5);
	/* Enable port and pramble len for each port */
	nlm_hal_write_nae_reg(node, TX_MSEC_PORT_EN, port_enable & 0xff);


	nlm_hal_write_nae_reg(node, TX_MSEC_INIT_PN, packet_num);
	nlm_hal_write_nae_reg(node, TX_MSEC_PN_THRESH, pn_thrshld);
	nlm_hal_write_nae_reg(node, TX_MSEC_PREAMBLE_LEN_CODE, preamble_len);

	while(preamble_len)
	{	
		if((preamble_len & 0x3) == 0x1)
			sec_tag_offset[i/4] += 0xc << ((i%4) * 8);
		if((preamble_len & 0x3) == 0x2)
			sec_tag_offset[i/4] += 0x10 << ((i%4) * 8);
		i++;
		preamble_len = preamble_len >> 2;
	}
	sec_tag_preamble = ((uint64_t)(sec_tag_offset[1]) << 32) | sec_tag_offset[0];

	for(i = 0; i < 8/*Num_ports*/;i++)
	{
		nae_cfg->sectag_offset[i] = (sec_tag_offset[i/4] >> (i *8)) &0xff;
		nae_cfg->icv_len[i] = icv_len;
		nae_cfg->sectag_len[i] = sectag_len;
	}
#ifdef MACSEC_DEBUG
        nlm_print(" %s sec_tag_offset = %x %x sec_tag_preamble = %llx\n", __FUNCTION__, sec_tag_offset[0], sec_tag_offset[1], sec_tag_preamble);
#endif
	nae_cfg->msec_port_enable = port_enable;

	/* ci offset = sectag_preamble + 16 (sectag header length)*/
	while(port_enable)
	{
		if(port_enable & 0x1)
		{
			param = ((sec_tag_preamble >> (port * 8)) & 0xff) << 16 | (((sec_tag_preamble >>(port * 8)) & 0xff) + sectag_len) << 8 | icv_len;
			nlm_hal_msec_set_tx_param(node, port, param);
#ifdef MACSEC_DEBUG
        		nlm_print("%s param = %x\n", __FUNCTION__, param);
#endif
		}
		port++;
		port_enable = port_enable >> 1;
	}
}

/* Config  ci key, param, cntxtoSAMap, sci adn tci memory */
/* For simplicity total 8 cntx, 8 sci, 8 tci and 8 param are programmed, one for each port */
void nlm_hal_msec_tx_default_config(int node, unsigned int port_enable, unsigned int preamble_len, unsigned int packet_num, unsigned int pn_thrshld)
{
	unsigned int num_sa, i;
	uint64_t sci;
	num_sa = MACSEC_NUM_SA;

	nlm_hal_msec_tx_config(node, port_enable, preamble_len, packet_num, pn_thrshld);
	for(i = 0; i < num_sa; i++)
	{
		/* sci = *((uint64_t*)(sa[j].src.mac)); */
		sci = sa[i].src;
		/* sci | port identifier. For each src mac thee are two sa for each dst mac */
		/* sci = sci | (i%2); */
		sci = sci | 1; /* Port identifier 1. 0 will be taken directly from SRC MAC */

#ifdef MACSEC_DEBUG
        	nlm_print("macsec port_num = %d cntx = %d, tci = %d src_mac = %llx dst_mac = %llx sci = %llx tci = %x \n",sa[i].port_num, sa[i].cntx, sa[i].tci, sa[i].src, sa[i].dst, sci, sa[i].tci); 
		dump_buffer(tx_ci_key[i].key, 16, "tx_default_key:");
#endif

		nlm_hal_msec_tx_mem_config(node, sa[i].cntx, sa[i].tci, sci, tx_ci_key[i].key);
	}
}

/* 
  These functions can be opened and used for debug purpose
*/
/*
int nlm_hal_msec_get_tx_param(int node, int index)
{
	unsigned int cntrl_reg_val =  (1 << 11 | index & 0x7ff);
	unsigned int param;

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);

	cntrl_reg_val = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_CTRL_REG);
	if(cntrl_reg_val & (1<< 23))
	{
		param = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_DATAREG_0);
	}
	return param;
}
int nlm_hal_msec_get_tx_cntx_to_sa(int node, int cntx)
{
	unsigned int cntrl_reg_val =  (2 << 11 | cntx & 0x7ff);
	unsigned int sa;

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);

	cntrl_reg_val = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_CTRL_REG);
	if(cntrl_reg_val & (1<< 23))
	{
		sa = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_DATAREG_0);
	}
	return sa;
}

uint64_t nlm_hal_msec_get_tx_sci(int node, int index)
{
	unsigned int data, cntrl_reg_val =  (3 << 11 | cntx & 0x7ff);
	uint64_t sci;

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);

	cntrl_reg_val = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_CTRL_REG);
	if(cntrl_reg_val & (1<< 23))
	{
		data = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_DATAREG_0);
		sci = data;
		data = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_DATAREG_1);
		sci = data << 32 | sci;
	}
	return sci;
}
usigned int nlm_hal_msec_get_tx_tci(int node, int cntx)
{
	unsigned int cntrl_reg_val =  (4 << 11 | cntx & 0x7ff);
	unsigned int tci;

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);

	cntrl_reg_val = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_CTRL_REG);
	if(cntrl_reg_val & (1<< 23))
	{
		tci = nlm_hal_read_nae_reg(node, TX_MSEC_MEM_DATAREG_0);
	}
	return tci;
}
*/



void nlm_hal_msec_set_rx_cam(int node, int index, int port_num, uint64_t sci, uint64_t sci_mask)
{
	/* | CTRL_REG_RD_DONE | CTRL_REG_WR_EN | CTRL_REG_MEM_SEL[10:0] | CTRL_REG_ADDRESS[10:0] |*/
	unsigned int cntrl_reg_val = (1 << 22 | (2 << 11)| (index & 0x7ff));
	sci = sci & sci_mask;
	
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_0, sci & 0xffffffff);
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_1, (sci>>32) & 0xffffffff);
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_2, (port_num & 0x7)|0x0000);

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
	nlm_hal_write_nae_reg(node, RX_MSEC_SCI_MASK_LO(port_num), sci_mask & 0xffffffff);
	nlm_hal_write_nae_reg(node, RX_MSEC_SCI_MASK_HI(port_num), (sci_mask >> 32) & 0xffffffff);
}

unsigned int nlm_hal_msec_get_rx_cam(int node, int index)
{
	unsigned int cntrl_reg_val =  (2 << 11)| (index & 0x7ff);
	unsigned int data = 0, data1 = 0, data2 = 0;

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);

	cntrl_reg_val = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_CTRL_REG);
	if(cntrl_reg_val & (1<< 23))
	{
		data = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_0);
		data1 = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_1);
		data2 = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_2);
	}
#ifdef MACSEC_DEBUG
	nlm_print("RX CAM index = %d, data0 = %x data_1 = %x data_2 = %x\n", index, data, data1, data2);
#endif
	return data;
}
/* Changed the order of bytes */
void nlm_hal_msec_set_rx_key(int node, int index, unsigned char *key)
{
	unsigned int cntrl_reg_val = (1 << 22 | (index & 0x7ff)), local_key;
	
#ifdef MACSEC_DEBUG
	nlm_print("Set RX key- index = %d \n", index);
#endif
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_3, local_key);
	key = key+4;
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_2, local_key);
	key = key+4;
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_1, local_key);
	key = key+4;
	local_key = (key[0] << 24) | (key[1] << 16) | (key[2] << 8) | key[3];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_0, local_key);

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}
/*
void nlm_hal_msec_set_rx_key(int node, int index, unsigned char *key)
{
	unsigned int cntrl_reg_val = (1 << 22 | (index & 0x7ff)), local_key;
	
	local_key = (key[3] << 24) | (key[2] << 16) | (key[1] << 8) | key[0];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_0, local_key);
	key = key+4;
	local_key = (key[3] << 24) | (key[2] << 16) | (key[1] << 8) | key[0];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_1, local_key);
	key = key+4;
	local_key = (key[3] << 24) | (key[2] << 16) | (key[1] << 8) | key[0];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_2, local_key);
	key = key+4;
	local_key = (key[3] << 24) | (key[2] << 16) | (key[1] << 8) | key[0];
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_3, local_key);

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}
*/
void nlm_hal_msec_get_rx_key(int node, int index, unsigned char *key)
{
	unsigned int cntrl_reg_val =  (index & 0x7ff);
	unsigned int data;
	unsigned char *temp = key;

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);

	cntrl_reg_val = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_CTRL_REG);
	if(cntrl_reg_val & (1<< 23))
	{
		data = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_0);
		*(unsigned int*)key = data;
		key = key+4;
		data = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_1);
		*(unsigned int*)key = data;
		key = key+4;
		data = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_2);
		*(unsigned int*)key = data;
		key = key+4;
		data = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_3);
		*(unsigned int*)key = data;
	}
	key = temp;
}


void nlm_hal_msec_set_rx_param(int node, int index, unsigned int param)
{
	unsigned int cntrl_reg_val = (1 << 22 | 1 << 11 |(index & 0x7ff));
	
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_DATAREG_0, param);
	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);
}

unsigned int nlm_hal_msec_get_rx_param(int node, int index)
{
	unsigned int cntrl_reg_val =  ((1 << 11) | (index & 0x7ff));
	unsigned int data = 0;

	nlm_hal_write_nae_reg(node, RX_MSEC_MEM_CTRL_REG, cntrl_reg_val);

	cntrl_reg_val = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_CTRL_REG);
	if(cntrl_reg_val & (1<< 23))
	{
		data = nlm_hal_read_nae_reg(node, RX_MSEC_MEM_DATAREG_0);
	}
#ifdef MACSEC_DEBUG
	nlm_print("RX- index = %d PARAM= %x\n", index, data);
#endif
	return data;
}

/* Config cam, ci key and param memory */
void nlm_hal_msec_rx_mem_config(int node, int port, int index, uint64_t sci, unsigned char *key, uint64_t sci_mask)
{
#ifdef MACSEC_DEBUG
        nlm_print("%s node= %d port = %d index = %d sci = %llx sci_mask = %llx\n",__FUNCTION__, node, port, index, sci, sci_mask);
#endif

	nlm_hal_msec_set_rx_cam(node, index, port, sci, sci_mask);

	index = index << 1; /* Writing at AN = 0. index << 1 | AN*/
	nlm_hal_msec_set_rx_key(node, index, key);

	index = index | 1; /* Writing at AN = 1. index << 1 | AN*/
	nlm_hal_msec_set_rx_key(node, index, key);
}

void dump_rx_mem_config(int node)
{
	unsigned int num_sa, i;
	num_sa = MACSEC_NUM_SA;
#ifdef MACSEC_DEBUG
	nlm_print("%s\n",__FUNCTION__);
#endif
	for(i = 0; i < num_sa; i++)
	{
		 nlm_hal_msec_get_rx_cam(node, i);
		 nlm_hal_msec_get_rx_param(node, i);

	}

}
 
void nlm_hal_msec_rx_config(int node, unsigned int port_enable, unsigned int preamble_len, unsigned int packet_num, unsigned int replay_win_size)
{
	unsigned int sec_tag_offset[2], i = 0, port, param;
	uint64_t sec_tag_preamble;

	sec_tag_offset[0] = 0x0c0c0c0c;
	sec_tag_offset[1] = 0x0c0c0c0c;
#ifdef MACSEC_DEBUG
        nlm_print("%s node = %d, port_enable = %x, preamble_len = %x, packet_num = %x, replay_win_size = %d\n", __FUNCTION__, node, port_enable, preamble_len, packet_num, replay_win_size);
#endif
	/* Anti reply mode is not working. Always bypass anti reply check*/
	nlm_hal_write_nae_reg(node, RX_MSEC_BYPASS, 0x2);
	/* Enable port and pramble len for each port */
	nlm_hal_write_nae_reg(node, RX_MSEC_PORT_EN, (preamble_len | port_enable) & 0xffffff);

	while(preamble_len)
	{	
		if((preamble_len & 0x3) == 0x1)
			sec_tag_offset[i/4] += 0xc << ((i%4) * 8);
		if((preamble_len & 0x3) == 0x2)
			sec_tag_offset[i/4] += 0x10 << ((i%4) * 8);
		i++;
		preamble_len = preamble_len >> 2;
	}
	/* Set setag offset. default is 12, it can be 24 pr 28 also depending on preamble length*/
	nlm_hal_write_nae_reg(node, RX_MSEC_SEC_TAG0, sec_tag_offset[0]);
	nlm_hal_write_nae_reg(node, RX_MSEC_SEC_TAG1, sec_tag_offset[1]);

	nlm_hal_write_nae_reg(node, RX_MSEC_INIT_PN, packet_num);
	nlm_hal_write_nae_reg(node, RX_MSEC_REPLAY_WIN_SIZE, replay_win_size);

	sec_tag_preamble = (((uint64_t)sec_tag_offset[1]) << 32) | sec_tag_offset[0];

	port = 0; 
	while(port_enable)
	{
		if(port_enable & 0x1)
		{
			/* ci offset = sectag_preamble + 16 (sectag header length)*/
			param = (((sec_tag_preamble >> (port * 8)) & 0xff) + 8/*16*/) << 8 | 0x10;
			
#ifdef MACSEC_DEBUG
			nlm_print("%s param = %x\n", __FUNCTION__, param);
#endif
			nlm_hal_msec_set_rx_param(node, port, param);
		}
		port++;
		port_enable = port_enable >> 1;
	}
}

void nlm_hal_msec_rx_default_config(int node, unsigned int port_enable, unsigned int preamble_len, unsigned int packet_num, unsigned int replay_win_size)
{
	unsigned int num_sa, i;
	uint64_t sci, sci_mask = 0xffffffffffff0000;/* Last 4 nibbles are don't care to handle port indentifier*/
	num_sa = MACSEC_NUM_SA;

	nlm_hal_msec_rx_config(node, port_enable, preamble_len, packet_num, replay_win_size);

	for(i = 0; i < num_sa; i++)
	{
		/* sci = *(uint64_t*)sa[i].dst.mac; */
		sci = sa[i].dst;
		/* sci | port identifier = 0001 as SCB bit is not set*/
		/*sci = sci | 1;*/ /* As sci mask is 0xffffffffffff0000 then port identifier is not required */
#ifdef MACSEC_DEBUG
        	nlm_print("macsec port_num = %d src = %llx dst = %llx sci = %llx\n",sa[i].port_num, sa[i].src, sa[i].dst, sci); 
		dump_buffer(rx_ci_key[i].key, 16, "rx_key:");
#endif
		nlm_hal_msec_rx_mem_config(node, sa[i].port_num, i, sci, rx_ci_key[i].key, sci_mask);
	}

#ifdef MACSEC_DEBUG
	dump_rx_mem_config(node);
#endif
}

/* called only if the shared domain are exists 
 Extracts the paddr and desc information */
int nlm_hal_retrieve_shared_freein_fifo_info(void *fdt, 
		int shared_dom_id, 
		int *owner_replenish, 
		char **paddr_info, int *paddr_info_len,
		char **desc_info, int *desc_info_len)
{
	char nae_node_str[64];
	char *pbuf;
	const char *paddrname =  "freein-fifo-replenish-addr-info";
	const char *pinfoname =  "freein-fifo-replenish-desc-info";
	const char *ownerrepl = "freein-fifo-replenish-by-owner";
	int plen, pnode;
	
	sprintf(nae_node_str, "/doms/dom@%d/nae", shared_dom_id);

	pnode = fdt_path_offset(fdt, nae_node_str);
	if (pnode < 0) {
		return -1;
	}

	pbuf = (char *)fdt_getprop(fdt, pnode, ownerrepl, &plen);
	if (pbuf == NULL) {
		return -1;
	}
	*owner_replenish = fdt32_to_cpu(*(unsigned int *)pbuf);

	pbuf = (char *)fdt_getprop(fdt, pnode, paddrname, &plen);
	if (pbuf == NULL) {
		return -1;
	}
	*paddr_info = pbuf;
	*paddr_info_len = plen;

	pbuf = (char *)fdt_getprop(fdt, pnode, pinfoname, &plen);
	if (pbuf == NULL) {
		return -1;
	}
	*desc_info = pbuf;
	*desc_info_len = plen;

	return 0;
}

unsigned int nlm_hal_retrieve_freein_fifo_mask(void *fdt, int node, int dom_id)
{
	unsigned int freein_fifo_mask[NLM_MAX_NODES];
	memset(freein_fifo_mask, 0, sizeof(freein_fifo_mask));

	get_dom_nae_property(fdt, dom_id, "freein-fifo-mask", 
			freein_fifo_mask, sizeof(freein_fifo_mask));

	return freein_fifo_mask[node];
}

#ifdef NLM_HAL_LINUX_KERNEL
#include <linux/types.h>
#include <linux/module.h>
EXPORT_SYMBOL(nlm_hal_get_ilk_mac_stats);
EXPORT_SYMBOL(nlm_hal_set_ilk_framesize);
EXPORT_SYMBOL(nlm_hal_set_xaui_framesize);
EXPORT_SYMBOL(nlm_hal_set_sgmii_framesize);
EXPORT_SYMBOL(nlm_hal_nae_drain_frin_fifo_descs);
EXPORT_SYMBOL(nlm_hal_write_ucore_shared_mem);
EXPORT_SYMBOL(nlm_hal_init_nae);
EXPORT_SYMBOL(rely_on_firmware_config);
EXPORT_SYMBOL(nlm_config_vfbid_table);
EXPORT_SYMBOL(cntx2port);
EXPORT_SYMBOL(nlm_enable_poe_statistics);
EXPORT_SYMBOL(nlm_read_poe_statistics);
EXPORT_SYMBOL(nlm_clear_poe_stats);
EXPORT_SYMBOL(nlm_disable_poe_statistics);
EXPORT_SYMBOL(nlm_hal_prepad_enable);
EXPORT_SYMBOL(nlm_hal_reset_1588_accum);
EXPORT_SYMBOL(nlm_hal_1588_ld_freq_mul);
EXPORT_SYMBOL(nlm_hal_1588_ld_offs);
EXPORT_SYMBOL(nlm_hal_get_int_sts);
EXPORT_SYMBOL(nlm_hal_1588_ld_user_val);
EXPORT_SYMBOL(nlm_hal_1588_ptp_clk_sel);
EXPORT_SYMBOL(nlm_hal_1588_ptp_get_counter);
EXPORT_SYMBOL(nlm_hal_1588_ptp_set_counter);
EXPORT_SYMBOL (nlm_hal_clear_1588_intr);
EXPORT_SYMBOL(nlm_hal_is_intr_1588);
EXPORT_SYMBOL(nlm_hal_enable_1588_intr);
EXPORT_SYMBOL(nlm_hal_open_if);
EXPORT_SYMBOL(nlm_hal_init_ingress);
EXPORT_SYMBOL(nlm_hal_load_ucore);
EXPORT_SYMBOL(nlm_hal_init_poe_distvec);
EXPORT_SYMBOL(nlm_hal_mac_enable);
EXPORT_SYMBOL(nlm_hal_mac_disable);
EXPORT_SYMBOL(nlm_hal_restart_ucore);
EXPORT_SYMBOL(nlm_hal_derive_cpu_to_freein_fifo_map);
EXPORT_SYMBOL(nlm_hal_modify_nae_ucore_sram_mem);
EXPORT_SYMBOL(nlm_hal_read_nae_ucore_sram_mem);
EXPORT_SYMBOL(nlm_hal_msec_tx_config);
EXPORT_SYMBOL(nlm_hal_msec_rx_config);
EXPORT_SYMBOL(nlm_hal_msec_tx_default_config);
EXPORT_SYMBOL(nlm_hal_msec_rx_default_config);
EXPORT_SYMBOL(nlm_hal_msec_tx_mem_config);
EXPORT_SYMBOL(nlm_hal_msec_rx_mem_config);
EXPORT_SYMBOL(nlm_hal_get_rtc);
EXPORT_SYMBOL(nlm_hal_retrieve_shared_freein_fifo_info);
EXPORT_SYMBOL(nlm_hal_retrieve_freein_fifo_mask);

MODULE_AUTHOR("Broadcom ");
MODULE_DESCRIPTION("NAE driver");
MODULE_LICENSE("Proprietary");
MODULE_VERSION("0.1");

#endif /* #ifdef NLM_HAL_LINUX_KERNEL */
