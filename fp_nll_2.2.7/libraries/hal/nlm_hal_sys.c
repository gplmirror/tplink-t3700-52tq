
/*-
 * Copyright (c) 2003-2013 Broadcom Corporation
 * All Rights Reserved
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * http://www.gnu.org/licenses/gpl-2.0.txt
 * or the Broadcom license below:

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_4# */
#if !defined(__KERNEL__) && !defined(NLM_HAL_UBOOT)
#include <stddef.h>
#endif
#ifdef NLM_HAL_LINUX_KERNEL
#include <asm/div64.h>
#endif
#include "nlm_hal.h"
#include "nlm_hal_sys.h"
#include "nlm_hal_xlp_dev.h"

uint64_t nlm_hal_xlp2xx_get_pllfreq_dyn(int node, uint8_t pll_type);
uint64_t nlm_hal_xlp2xx_set_pllfreq_dyn(int node, uint8_t pll_type, uint64_t freq);
uint64_t nlm_hal_xlp2xx_get_pll_out_frq(int node, uint8_t pll_type);

/**
 * Calculate the DFS divider value for the specified reference clock
 * and target output frequency.
 * @param [in] reference the reference clock frequency, in Hz.
 * @param [in] target the target output frequency, in Hz.
 * @returns the divider that produces the target frequency
 * (if the target frequency is within FREQ_RESOLUTION of the actual output).
 * Otherwise, it will round up to the nearest divider (rounding down to the
 * the lower output frequency).
 */
static uint8_t fuzzy_divider(uint64_t reference, uint64_t target)
{
	uint64_t divider = reference;
	uint64_t freq = reference;
	uint64_t delta;
	uint8_t result;
	NLM_HAL_DO_DIV(divider, target);
	NLM_HAL_DO_DIV(freq, divider);
	delta = freq - target;
	result = (uint8_t)divider;
	return (delta <= FREQ_RESOLUTION)? result : (result + 1);
}


/**
 * @returns the numerator for the reference clock frequency.
 */
static inline uint64_t ref_clk_num(void)
{
	return  (nlm_hal_is_ref_clk_133MHz()) ? REF_CLK_NUM_400 : REF_CLK_NUM_200;
}

/**
 * @returns the denominator for the reference clock frequency.
 */
static inline uint64_t ref_clk_den(void)
{
	return REF_CLK_DEN;
}
/**
 * @returns the current reference clock frequency, in Hz.
 */
uint64_t nlm_hal_get_ref_clk_freq(void)
{
        uint64_t ref_freq;
        uint64_t clk_num;
        uint32_t clk_den;

	if(is_nlm_xlp2xx()) {
		ref_freq = xlp2xx_get_ref_clk(0, &clk_num, &clk_den);
	} else {
		ref_freq = ref_clk_num() / ref_clk_den();
	}
	return ref_freq;
}

/**
 * Mapping of DFS indices to actual DFS divider values.
 */
static uint8_t DFS[] = {1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 15};

/**
 * @returns the maximum DFS divider value.
 */
static inline uint8_t max_dfs_val(void)
{
	return DFS[COUNT_OF(DFS) - 1];
}
/**
 * @returns the minimum DFS divider value.
 */
static inline uint8_t min_dfs_val(void)
{
	return DFS[0];
}

/**
 * Get the output frequency for the PLL, based on the
 * R-divider, F-divider, and PLL DFS divider.
 * @param [in] divr R-divider for the PLL.
 * @param [in] divf F-divider for the PLL.
 * @param [in] pll_dfs PLL DFS divider.
 * @return PLL frequency, in Hz.
 */
static inline uint64_t pll_freq(uint8_t divr, uint8_t divf, uint8_t pll_dfs)
{
	uint64_t num = ref_clk_num() * (divf + 1) * 4 / 2;
	uint64_t den = ref_clk_den() * (divr + 1) * (pll_dfs + 1);
	NLM_HALT_IF_XLPII();
	NLM_HAL_DO_DIV(num, den);
	return num;
}

/**
 * Get the DFS index for the DFS value (to be used with the stepping functions).
 * The DFS value is rounded up (producing the lower output frequency) if
 * the exact DFS value does not exist.
  @param [in] dfs DFS divider value.
 * @return the DFS index greater than or equal to the specified DFS divider value.
 */
static inline int8_t closest_dfs_index(uint8_t dfs)
{
	int i;
	NLM_HALT_IF_XLPII();
	if (dfs > max_dfs_val())
		return COUNT_OF(DFS)-1;

	for (i = (COUNT_OF(DFS) - 2); i >= 0; i--) {
		if ((DFS[i+1] >= dfs) && (dfs > DFS[i]))
			return i+1;
	}

	return 0;
}

/**
 * Determine whether the Core PLL DFS is bypassed.
 * In XLP8xx-4xx A-stepping, the Core PLL DFS does not exist.
 * @returns 0 if the Core PLL is not bypassed.
 * @returns 1 if the Core PLL is bypassed, or if the Core PLL does not exist.
 */
static inline uint8_t is_core_pll_dfs_bypassed(int node)
{
	NLM_HALT_IF_XLPII();
	/* no Core PLL DFS on XLP8XX/4XX A-stepping */
	if (is_nlm_xlp8xx_ax())
		return 1;
	return nlm_hal_read_sys_reg(node, PLL_DFS_BYP_CTRL) & 0x1;
}

/**
 * Determine whether the SoC PLL DFS is bypassed.
 * In XLP8xx-4xx A-stepping, the SoC PLL DFS does not exist.
 * @returns 0 if the SoC PLL is not bypassed.
 * @returns 1 if the SoC PLL is bypassed, or if the SoC PLL does not exist.
 */
static inline uint8_t is_soc_pll_dfs_bypassed(int node)
{
	NLM_HALT_IF_XLPII();
	/* no Core PLL DFS on XLP8XX/4XX A-stepping */
	if (is_nlm_xlp8xx_ax())
		return 1;
	return (nlm_hal_read_sys_reg(node, PLL_DFS_BYP_CTRL) >> 1) & 0x1;
}

/**
 * @return the Core PLL DFS divider value, if the chip family supports it.
 * @return 0 if the Core PLL DFS is not implemented.
 */
static inline uint64_t core_pll_dfs_val(int node)
{
	NLM_HALT_IF_XLPII();
	/* no Core PLL DFS on XLP8XX/4XX A-stepping */
	if (is_nlm_xlp8xx_ax())
		return 0;
	if (is_core_pll_dfs_bypassed(node))
		return 0;
	return nlm_hal_read_sys_reg(node, PLL_DFS_DIV_VALUE) & 0xf;
}

/**
 * @return the SoC PLL DFS divider value, if the chip family supports it.
 * @return 0 if the SoC PLL DFS is not implemented.
 */
static inline uint64_t soc_pll_dfs_val(int node)
{
	NLM_HALT_IF_XLPII();
	/* no SoC PLL DFS on XLP8XX/4XX A-stepping */
	if (is_nlm_xlp8xx_ax())
		return 0;
	if (is_soc_pll_dfs_bypassed(node))
		return 0;
	return (nlm_hal_read_sys_reg(node, PLL_DFS_DIV_VALUE) >> 4) & 0xf;
}

/**
 * Get the Core PLL frequency post PLL DFS.
 */
static inline uint64_t core_pll_freq(int node)
{
	uint32_t reg = nlm_hal_read_sys_reg(node, POWER_ON_RESET_CFG);
	NLM_HALT_IF_XLPII();
	uint8_t divr = (reg >> 8)  & 0x3;
	uint8_t divf = (reg >> 10) & 0x7f;
	return pll_freq(divr, divf, core_pll_dfs_val(node));
}

/**
 * Get the SoC PLL frequency post PLL DFS.
 */
static inline uint64_t soc_pll_freq(int node)
{
	uint32_t reg = nlm_hal_read_sys_reg(node, PLL_CTRL);
	NLM_HALT_IF_XLPII();
	uint8_t divf = (reg >> 3) & 0x7F;
	uint8_t divr = (reg >> 1) & 0x3;
	return pll_freq(divr, divf, soc_pll_dfs_val(node));
}

/**
 * Get the DDR PLL frequency.
 */
static inline uint64_t ddr_pll_freq(int node)
{
	uint32_t reg = nlm_hal_read_sys_reg(node, PLL_CTRL);
	NLM_HALT_IF_XLPII();
	uint8_t divf = (reg >> 19) & 0x7F;
	uint8_t divr = (reg >> 17) & 0x3;
	return pll_freq(divr, divf, 0);
}

/**
 * Get the DFS divider value for the specified SoC device.
 * @param [in] device the SoC device.
 */
static inline uint64_t soc_dfs_val(int node, soc_device_id_t device)
{
	uint8_t device_index = device;
	NLM_HALT_IF_XLPII();
	if (device_index >= 8)
	{
		device_index -= 8;
		return (nlm_hal_read_sys_reg(node, SYS_DFS_DIV_VALUE1) >> (device_index * 4)) & 0xF;
	}
	return (nlm_hal_read_sys_reg(node, SYS_DFS_DIV_VALUE0) >> (device_index * 4)) & 0xF;
}

/**
 * Get the DFS divider value for the specified Core.
 * @param [in] core CPU core index.
 */
static inline uint64_t core_dfs_val(int node, uint8_t core)
{
	NLM_HALT_IF_XLPII();
	return (nlm_hal_read_sys_reg(node, CORE_DFS_DIV_VALUE) >> (core * 4)) & 0xF; 
}

/**
 * Determine whether the SoC device's DFS is bypassed.
 * @param [in] device the SoC device.
 * @returns 1 if the SoC device's DFS is bypassed.
 * @returns 0 if the SoC device's DFS is not bypassed.
 */
static inline uint8_t is_soc_dfs_bypassed(int node, soc_device_id_t device)
{
	uint8_t device_index = device;
	NLM_HALT_IF_XLPII();
	return (nlm_hal_read_sys_reg(node, SYS_DFS_BYP_CTRL) >> device_index) & 1;
}

/**
 * Enable/disable the DFS bypass for the specified SoC device.
 * @param [in] device the SoC device.
 * @param [in] bypass 1: bypass the DFS. 0: do not bypass DFS.
 */
static inline void set_soc_dfs_bypass(int node, soc_device_id_t device, uint8_t bypass)
{
	uint8_t device_index = device;
	uint32_t val;
	NLM_HALT_IF_XLPII();
	val = nlm_hal_read_sys_reg(node, SYS_DFS_BYP_CTRL) & ~(1 << device_index);
	nlm_hal_write_sys_reg(node, SYS_DFS_BYP_CTRL, val | ((bypass? 0x1 : 0x0) << device_index));
}

/**
 * Determine whether the CPU core's DFS is bypassed.
 * @param [in] core CPU core index.
 * @returns 1 if the CPU core's DFS is bypassed.
 * @returns 0 if the CPU core's DFS is not bypassed.
 */
static inline uint8_t is_core_dfs_bypassed(int node, uint8_t core)
{
	NLM_HALT_IF_XLPII();
	return (nlm_hal_read_sys_reg(node, CORE_DFS_BYP_CTRL) >> core) & 1;
}

/**
 * Enable/disable the DFS bypass for the specified CPU core.
 * @param [in] core CPU core index.
 * @param [in] bypass 1: bypass the DFS. 0: do not bypass DFS.
 */
static inline void set_core_dfs_bypass(int node, uint8_t core, uint8_t bypass)
{
	uint32_t val;
	NLM_HALT_IF_XLPII();
	val = nlm_hal_read_sys_reg(node, CORE_DFS_BYP_CTRL) & ~(1 << core);
	nlm_hal_write_sys_reg(node, CORE_DFS_BYP_CTRL, val | ((bypass? 0x1 : 0x0) << core));
}

/**
 * Get the operating frequency for the specified CPU core.
 * @param [in] device the SoC device.
 * @returns The SoC device operating frequency, in Hz.
 */
uint64_t nlm_hal_get_soc_freq(int node, soc_device_id_t device)
{
	uint64_t freq, den;

	if(is_nlm_xlp2xx()) {
		freq = nlm_hal_xlp2xx_get_clkdev_frq(node, device);
		return freq;
	}

	switch (device) {
		case DFS_DEVICE_NAND:
		case DFS_DEVICE_NOR:
		case DFS_DEVICE_MMC:
			/* NOR, NAND and MMC devices are derived from the freq clock. */
			freq = nlm_hal_get_ref_clk_freq();
			break;
		case DFS_DEVICE_DMC:
			freq = ddr_pll_freq(node);
			break;
		case DFS_DEVICE_CORE:
			/* The Core DFS is derived from the Core PLL */
			freq = core_pll_freq(node);
			break;
		default:
			freq = soc_pll_freq(node);
			break;
	}

	den = soc_dfs_val(node, device) + 1;
	if (!is_soc_dfs_bypassed(node, device)) {
		NLM_HAL_DO_DIV(freq, den);
	}
	return freq;
}

/**
 * Step the DFS of the specified SoC device to the target DFS index.
 * @param [in] device the SoC device.
 * @param [in] dfs_index DFS index (**not** the DFS value).
 */
static void step_soc_dfs(int node, soc_device_id_t device, uint8_t dfs_index)
{
	uint8_t device_index;
	uint8_t cur;
	int8_t delta;

	NLM_HALT_IF_XLPII();
	device_index = device;
	cur = closest_dfs_index(soc_dfs_val(node, device));
	delta = cur - dfs_index;
	int i;

	if (delta >= 0) {
		/* positive delta, decrement dfs */
		for (i=0; i < delta; i++)
			nlm_hal_write_sys_reg(node, SYS_DFS_DIV_DEC_CTRL, 1 << device_index);
	} else {
		/* negative delta, increment dfs */
		for (i=0; i > delta; i--)
			nlm_hal_write_sys_reg(node, SYS_DFS_DIV_INC_CTRL, 1 << device_index);
	}
}

/**
 * Set the operating frequency for the specified SoC device.
 * This is achieved only by stepping the SoC device DFS.
 * @param [in] device the SoC device.
 * @param [in] freq target SoC device frequency, in Hz.
 * @returns the new SoC device operating frequency, in Hz.
 */
uint64_t nlm_hal_set_soc_freq(int node, soc_device_id_t device, uint64_t freq)
{
	uint64_t reference;
	uint8_t  target;

	if(is_nlm_xlp2xx()) {
		return nlm_hal_xlp2xx_set_clkdev_frq(node, device, freq);
	}
	switch (device) {
		case DFS_DEVICE_NAND:
		case DFS_DEVICE_NOR:
		case DFS_DEVICE_MMC:
			/* NOR, NAND and MMC devices are derived from the reference clock. */
			reference = nlm_hal_get_ref_clk_freq();
			break;
		case DFS_DEVICE_DMC:
			reference = ddr_pll_freq(node);
			break;
		case DFS_DEVICE_CORE:
			/* The Core DFS is derived from the Core PLL */
			reference = core_pll_freq(node);
			break;
		default:
			reference = soc_pll_freq(node);
			break;
	}

	target = closest_dfs_index(fuzzy_divider(reference, freq) - 1);
	if (freq >= (reference - FREQ_RESOLUTION)) {
		/* bypass DFS if freq is reference freq */
		set_soc_dfs_bypass(node, device, 1);
	} else {
		/* otherwise, step dfs and clear bypass */
		step_soc_dfs(node, device, target);
		set_soc_dfs_bypass(node, device, 0);
	}

	return nlm_hal_get_soc_freq(node, device);
}

/**
 * Get the operating frequency for the specified CPU core.
 * @param [in] core CPU core index.
 * @returns The CPU core operating frequency, in Hz.
 */
uint64_t nlm_hal_get_core_freq(int node, uint8_t core)
{
	if(is_nlm_xlp2xx()) {
		return nlm_hal_xlp2xx_get_pllfreq_dyn(node, core);
	}
	else {
		uint64_t den, reference = core_pll_freq(node);
		den = core_dfs_val(node, core) + 1;
		if (!is_core_dfs_bypassed(node, core))
		    NLM_HAL_DO_DIV(reference, den);
		return reference;
	}
}

/**
 * Step the DFS of the specified CPU core to the target DFS index.
 * @param [in] core CPU core index.
 * @param [in] dfs_index DFS index (**not** the DFS value).
 */
static void step_core_dfs(int node, uint8_t core, uint8_t dfs_index)
{
	uint8_t cur;
	int8_t delta;
	int i;

	NLM_HALT_IF_XLPII();
	cur = closest_dfs_index(core_dfs_val(node, core));
	delta = cur - dfs_index;

	if (delta >= 0) {
		/* positive delta, decrement dfs */
		for (i=0; i < delta; i++)
			nlm_hal_write_sys_reg(node, CORE_DFS_DIV_DEC_CTRL, 1 << core);
	} else {
		/* negative delta, increment dfs */
		for (i=0; i > delta; i--)
			nlm_hal_write_sys_reg(node, CORE_DFS_DIV_INC_CTRL, 1 << core);
	}
}

/**
 * Set the operating frequency for the specified CPU core.
 * This is achieved only by stepping the Core DFS.
 * @param [in] core CPU core index.
 * @param [in] freq target CPU core frequency, in Hz.
 * @returns the new CPU core operating frequency, in Hz.
 */
uint64_t nlm_hal_set_core_freq(int node, uint8_t core, uint64_t freq)
{
	if(is_nlm_xlp2xx()) {
		return nlm_hal_xlp2xx_set_pllfreq_dyn(node, core, freq);
	}
	else {
		uint64_t reference = core_pll_freq(node);
		uint8_t target = closest_dfs_index(fuzzy_divider(reference, freq) - 1);

		if (freq >= (reference - FREQ_RESOLUTION)) {
			/* bypass DFS if freq is reference freq */
			set_core_dfs_bypass(node, core, 1);
		} else {
			/* otherwise, step dfs and clear bypass */
			step_core_dfs(node, core, target);
			set_core_dfs_bypass(node, core, 0);
		}
		return nlm_hal_get_core_freq(node, core);
	}
}

/**
 * Get the operating frequency for the current core.
 * @returns The core operating frequency (in Hz).
 */
unsigned long long nlm_hal_cpu_freq(void)  
{
	uint8_t core = (nlm_cpu_id() >> 2) & 0x7;
	return nlm_hal_get_core_freq(nlm_node_id(), core);
}

uint32_t nlm_hal_get_biu_mask_by_soc_device_id(soc_device_id_t device)
{
	int biu_mask=0;
	switch(device) {
        case XLP2XX_CLKDEVICE_NAE    : biu_mask =
		 (1<<XLP2XX_IO_NET_BIU_NUMBER)|(1<<XLP2XX_IO_MSG_BIU_NUMBER)|(1<<XLP2XX_IO_POE_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_SAE    : biu_mask = (1<<XLP2XX_IO_SEC_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_RSA    : biu_mask = (1<<XLP2XX_IO_RSA_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_GDX    : biu_mask = (1<<XLP2XX_IO_GDX_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_CMP    : biu_mask = (1<<XLP2XX_IO_CMP_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_NAND   : biu_mask = (1<<XLP2XX_IO_GBU_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_MMC    : biu_mask = (1<<XLP2XX_IO_GBU_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_GBU    : biu_mask = (1<<XLP2XX_IO_GBU_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_RGXF   : biu_mask = (1<<XLP2XX_IO_REGX_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_RGXS   : biu_mask = (1<<XLP2XX_IO_REGX_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_USB    : biu_mask = (1<<XLP2XX_IO_USB_BIU_NUMBER); break;
        case XLP2XX_CLKDEVICE_PIC    : biu_mask = (1<<XLP2XX_IO_PIC_BIU_NUMBER); break;
		default: break;
	}
	return biu_mask;
}

uint8_t nlm_hal_get_soc_clock_state(int node, soc_device_id_t device)
{
	uint32_t biu_mask=0;
	if(is_nlm_xlp2xx()) {
		biu_mask = nlm_hal_get_biu_mask_by_soc_device_id(device);
		return (nlm_hal_read_sys_reg(node, XLP2XX_SYSDISABLE) & biu_mask) ?  XLP_DISABLE : XLP_ENABLE;
	} else {
		return (nlm_hal_read_sys_reg(node, SYS_DFS_DIS_CTRL) >> device);
	}
}

void nlm_hal_soc_clock_enable(int node, soc_device_id_t device)
{
	uint32_t d32;
	uint32_t biu_mask=0;
	if(is_nlm_xlp2xx()) {
		biu_mask = nlm_hal_get_biu_mask_by_soc_device_id(device);
		biu_mask = (nlm_hal_read_sys_reg(node, XLP2XX_SYSDISABLE) & ~biu_mask);
		nlm_hal_write_sys_reg(node, XLP2XX_SYSDISABLE, biu_mask);
	} else {
		d32 = nlm_hal_read_sys_reg(node, SYS_DFS_DIS_CTRL);
		d32 &= ~(1<<device);
		nlm_hal_write_sys_reg(node, SYS_DFS_DIS_CTRL, d32);
	}
}

void nlm_hal_soc_clock_disable(int node, soc_device_id_t device)
{
	uint32_t d32;
	uint32_t biu_mask=0;
	if(is_nlm_xlp2xx()) {
		biu_mask = nlm_hal_get_biu_mask_by_soc_device_id(device);
		biu_mask |= nlm_hal_read_sys_reg(node, XLP2XX_SYSDISABLE);
		nlm_hal_write_sys_reg(node, XLP2XX_SYSDISABLE, biu_mask);
	} else {
		d32 = nlm_hal_read_sys_reg(node, SYS_DFS_DIS_CTRL);
		d32 |= (1<<device);
		nlm_hal_write_sys_reg(node, SYS_DFS_DIS_CTRL, d32);
	}
}

void nlm_hal_soc_clock_reset(int node, soc_device_id_t device)
{
	uint32_t biu_mask=0;
	if(is_nlm_xlp2xx()) {
		biu_mask = nlm_hal_get_biu_mask_by_soc_device_id(device);
		nlm_hal_write_sys_reg(node, XLP2XX_SYSRESET, biu_mask);
	} else {
		nlm_hal_write_sys_reg(node, SYS_DFS_RST_CTRL, 1 << device);
	}
}

/*
 * XLP 2XX Clock Management
 */
/*  Reference Clock Select 00:66; 01:100; 10:125; 11:133 */
#define SYS_PWRON_RCS(x) (((x)>>18) & 0x3)

#define SYS_PWRON_PLF(x) (((x)>>17) & 0x1)

static int xlp2xx_get_plf(int node)
{
	uint32_t reg = nlm_hal_read_sys_reg(node, XLP2XX_POWER_ON_RESET_CFG);
	int plf = SYS_PWRON_PLF(reg);
	return plf;
}

static int xlp2xx_get_rcs(int node)
{
	uint32_t reg = nlm_hal_read_sys_reg(node, XLP2XX_POWER_ON_RESET_CFG);
	int rcs = SYS_PWRON_RCS(reg);
	return rcs;
}

static void nlm_hal_xlp2xx_dev_pll_cfg(int node, soc_device_id_t dev_type, int dev_pll_sel, int div)
{
	int dev_idx = dev_type - XLP2XX_CLKDEVICE_NAE;
	uint32_t rsel, rdiv, rchg;
	NLM_HALT_IF(dev_idx<0);
	rsel = nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_SEL_REG) & (~(3<<(dev_idx*2)));
	rdiv = nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_DIV_REG) & (~(3<<(dev_idx*2)));
	nlm_hal_write_sys_reg(node, XLP2XX_SYS_CLK_DEV_SEL, rsel | ((dev_pll_sel&3) << (dev_idx*2)));
	nlm_hal_write_sys_reg(node, XLP2XX_SYS_CLK_DEV_DIV, rdiv | ((div&3)<<(dev_idx*2)));
	rsel = nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_SEL_REG);
	rdiv = nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_DIV_REG);
	rchg = nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_CHG);
	nlm_hal_write_sys_reg(node, XLP2XX_SYS_CLK_DEV_CHG, rchg|(1<<dev_idx));
	while((nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_CHG) & (1<<dev_idx)));
	return;
}

/*
 * SYS_CLK_DEV_SEL
 *  00  400MHz
 *  01  Dev0 PLL
 *  10  Dev1 PLL
 *  11  Dev2 PLL
 */
static inline xlp2xx_clkdev_sel_t xlp2xx_get_clk_dev_sel(int node, soc_device_id_t dev_type)
{
	int dev_idx = dev_type - XLP2XX_CLKDEVICE_NAE;
	NLM_HALT_IF(dev_idx<0);
	return ( nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_SEL_REG) >> (dev_idx*2) ) & 0x3 ;
}	

/*
 * SYS_CLK_DEV_DIV
 * 00  1
 * 01  2
 * 10  4
 * 11  8
 */
static inline uint8_t xlp2xx_get_clkdev_div(int node, soc_device_id_t dev_type)
{
	int dev_idx = dev_type - XLP2XX_CLKDEVICE_NAE;
	NLM_HALT_IF(dev_idx<0);
	return  1<< (( nlm_hal_read_sys_reg(node, XLP2XX_SYS_CLK_DEV_DIV_REG) >> (dev_idx*2) ) & 0x3);
}	

uint64_t nlm_hal_xlp2xx_get_pllfreq_dyn(int node, uint8_t pll_type)
{
        uint32_t pll_mult = 0; /* [5:0] */
        uint64_t ref_clk_num;
	uint32_t ref_clk_den;
        uint32_t reg_ctrl;
	int plf = xlp2xx_get_plf(node)+1;
        switch(pll_type)
        {
                case CORE0_PLL:
                case CORE1_PLL:
                        reg_ctrl = XLP2XX_CORE0_PLL_CTRL1+pll_type*4;
                break;
                case SYS_PLL:
                case DMC_PLL:
                case DEV0_PLL:
                case DEV1_PLL:
                case DEV2_PLL:
                        reg_ctrl = XLP2XX_SYS_PLL_CTRL1+(pll_type-SYS_PLL)*4;
                break;
                default:
                        nlm_print("Unknown PLL type:%d\n", pll_type);
                        return 0;
        }
        pll_mult = nlm_hal_read_sys_reg(node, reg_ctrl) & (0x3f);
        ref_clk_num =  (REF_CLK_NUM_100 * pll_mult);
        ref_clk_den = REF_CLK_DEN;
        NLM_HAL_DO_DIV(ref_clk_num, ref_clk_den);
        ref_clk_num +=  REF_CLK_NUM_400;

        if(pll_type != DMC_PLL) {
		NLM_HAL_DO_DIV(ref_clk_num, plf);
	}

        return ref_clk_num;
}

uint64_t nlm_hal_xlp2xx_set_pllfreq_dyn(int node, uint8_t pll_type, uint64_t freq)
{
        /*Target PLL output frequency; 400 MHz + (33.333 MHz x [5:0]).*/
	uint32_t pll_mult = 0; /* [5:0] */
	uint64_t clk_base_freq_num = 100*1000000;
	uint32_t clk_base_freq_den = 3;
	uint32_t reg_ctrl,reg_chg,chg_mask;
	int plf = xlp2xx_get_plf(node)+1;

	if(pll_type!=DMC_PLL)
		freq*=plf;

	if(freq<400*1000000){
	        nlm_print("Freq for PLL cant be less than 400 Mhz\n");
	        return 0;
	}
	freq = clk_base_freq_den * (freq - 400*1000000);
	NLM_HAL_DO_DIV(freq, clk_base_freq_num);
	pll_mult = freq;
        switch(pll_type)
        {
                case CORE0_PLL:
                case CORE1_PLL:
                        reg_ctrl = XLP2XX_CORE0_PLL_CTRL1+pll_type*4;
                        reg_chg  = XLP2XX_CPU_PLL_CHG_CTRL;
                        chg_mask = 1<<(pll_type-CORE0_PLL);
                break;
                case SYS_PLL:
                case DMC_PLL:
                case DEV0_PLL:
                case DEV1_PLL:
                case DEV2_PLL:
                        reg_ctrl = XLP2XX_SYS_PLL_CTRL1+(pll_type-SYS_PLL)*4;
                        reg_chg  = XLP2XX_SYS_PLL_CHG_CTRL;
                        chg_mask = 1<<(pll_type-SYS_PLL);
                break;
                default:
                        nlm_print("Unknown PLL type:%d\n", pll_type);
                        return 0;
        }

        nlm_hal_write_sys_reg(node, reg_ctrl, pll_mult);
        nlm_hal_write_sys_reg(node, reg_chg,  chg_mask);
        while(nlm_hal_read_sys_reg(node, reg_chg) & chg_mask);

#if 0
	freq = nlm_hal_xlp2xx_get_pll_out_frq(node, pll_type);
	nlm_print("pll out freq:%dMHz\n", (uint32_t)freq/1000000);
#endif

	freq = nlm_hal_xlp2xx_get_pllfreq_dyn(node, pll_type);
	return freq;
}

uint64_t xlp2xx_get_ref_clk(int node, uint64_t* ref_clk_num, uint32_t* ref_clk_den)
{
	uint64_t frq_num;
	uint32_t frq_den;
	uint32_t rcs = xlp2xx_get_rcs(node);
	switch(rcs) {
		case 0x0:
			frq_num = REF_CLK_NUM_200;
			frq_den = REF_CLK_DEN3;
		break;	
		case 0x1:
			frq_num = REF_CLK_NUM_100;
			frq_den = REF_CLK_DEN1;
		break;	
		case 0x2:
			frq_num = REF_CLK_NUM_125;
			frq_den = REF_CLK_DEN1;
		break;	
		case 0x3:
		default:
			frq_num = REF_CLK_NUM_400;
			frq_den = REF_CLK_DEN3;
		break;	
	}
	*ref_clk_num = frq_num;
	*ref_clk_den = frq_den;
	NLM_HAL_DO_DIV(frq_num, frq_den);
	return frq_num;	
}

/* freq_out = ( ref_freq/2 * (6 + ctrl2[7:0]) + ctrl2[20:8]/2^13 ) / ((2^ctrl0[7:5]) * Table(ctrl0[26:24]))
 * Table.ctrl0[26:24]
 * ------------------
 *  0: 1
 *  1: 2
 *  3: 4
 *  7: 8
 *  6: 16
 */
uint64_t nlm_hal_xlp2xx_get_pll_out_frq(int node, uint8_t pll_type)
{
	uint32_t vco_po_div, pll_post_div, fdiv, mdiv;
	uint64_t ref_frq_num, pll_out_freq_num, two13;
	uint32_t ref_frq_den, pll_out_freq_den;
        uint32_t reg_ctrl0, reg_ctrl2;
        uint32_t ctrl0, ctrl2;
	
        switch(pll_type)
        {
                case CORE0_PLL:
                case CORE1_PLL:
                        reg_ctrl0 = XLP2XX_CORE0_PLL_CTRL0+pll_type*4;
                        reg_ctrl2 = XLP2XX_CORE0_PLL_CTRL2+pll_type*4;
                break;
                case SYS_PLL:
                case DMC_PLL:
                case DEV0_PLL:
                case DEV1_PLL:
                case DEV2_PLL:
                        reg_ctrl0 = XLP2XX_SYS_PLL_CTRL0+(pll_type-SYS_PLL)*4;
                        reg_ctrl2 = XLP2XX_SYS_PLL_CTRL2+(pll_type-SYS_PLL)*4;
                break;
                default:
                        nlm_print("Unknown PLL type:%d\n", pll_type);
                        return 0;
        }

	xlp2xx_get_ref_clk(node, &ref_frq_num, &ref_frq_den);
	ctrl0 = nlm_hal_read_sys_reg(node, reg_ctrl0);
	ctrl2 = nlm_hal_read_sys_reg(node, reg_ctrl2);

	vco_po_div = (ctrl0>>5) & 0x7;
	pll_post_div = (ctrl0>>24) & 0x7;
	mdiv = ctrl2 & 0xff;
	fdiv = (ctrl2>>8) & 0xfff;

	switch(pll_post_div) {
	    case 1: pll_post_div=2; break;
	    case 3: pll_post_div=4; break;
	    case 7: pll_post_div=8; break;
	    case 6: pll_post_div=16; break;
	    case 0:
	    default: 
		    pll_post_div=1;
		    break;
	}

	two13 = 1<<13;
	NLM_HAL_DO_DIV(fdiv, two13);
	pll_out_freq_num = ((ref_frq_num>>1) * (6 + mdiv) ) + fdiv;
	pll_out_freq_den = (1<<vco_po_div) * pll_post_div * ref_frq_den;

	if(pll_out_freq_den>0) {
		NLM_HAL_DO_DIV(pll_out_freq_num, pll_out_freq_den);
	}
	return pll_out_freq_num;
}

uint64_t nlm_hal_xlp2xx_get_clkdev_frq(int node, soc_device_id_t dev_type)
{
	uint64_t frq = 0, ref_clk_num;
	uint32_t ref_clk_den;
	uint8_t div = xlp2xx_get_clkdev_div(node, dev_type);
	xlp2xx_clkdev_sel_t pll_sel = xlp2xx_get_clk_dev_sel(node, dev_type);

	switch(pll_sel)
	{
		case SEL_REF_CLK:
			frq = xlp2xx_get_ref_clk(node, &ref_clk_num, &ref_clk_den);
		break;
		case SEL_DEV0PLL:
			frq = nlm_hal_xlp2xx_get_pllfreq_dyn(node, DEV0_PLL);
		break;
		case SEL_DEV1PLL:
			frq = nlm_hal_xlp2xx_get_pllfreq_dyn(node, DEV1_PLL);
		break;
		case SEL_DEV2PLL:
			frq = nlm_hal_xlp2xx_get_pllfreq_dyn(node, DEV2_PLL);
		break;
		default:
		break;
	}
	NLM_HAL_DO_DIV(frq, div);
	return frq;
}

uint64_t nlm_hal_xlp2xx_set_clkdev_frq(int node, soc_device_id_t dev_type, uint64_t new_frq)
{
	uint64_t frq;
	uint8_t new_div, div;
	xlp2xx_clkdev_sel_t pll_sel;

	pll_sel = xlp2xx_get_clk_dev_sel(node, dev_type);
	div = xlp2xx_get_clkdev_div(node, dev_type);
	frq = nlm_hal_xlp2xx_get_clkdev_frq(node, dev_type);

	/* new_frq*new_div=frq*div
	 *  new_div = frq*div/new_frq;
	 */
	new_div = fuzzy_divider(frq*div, new_frq);
	switch (new_div)
	{
		case 1:
			nlm_hal_xlp2xx_dev_pll_cfg(node, dev_type, pll_sel, DIV_BYPASS);
		break;
		case 2:
			nlm_hal_xlp2xx_dev_pll_cfg(node, dev_type, pll_sel, DIV_DIV2);
		break;
		case 4:
			nlm_hal_xlp2xx_dev_pll_cfg(node, dev_type, pll_sel, DIV_DIV4);
		break;
		case 8:
			nlm_hal_xlp2xx_dev_pll_cfg(node, dev_type, pll_sel, DIV_DIV8);
		break;
		default:
		break;
	}
	frq = nlm_hal_xlp2xx_get_clkdev_frq(node, dev_type);
	return frq;
}

/**
 * @returns true if reference clock is 133MHz
 */
int nlm_hal_is_ref_clk_133MHz(void)
{
	if(is_nlm_xlp2xx())
		return 3 == xlp2xx_get_rcs(0);
	else {
		uint32_t reg = nlm_hal_read_sys_reg(0, POWER_ON_RESET_CFG);
		uint8_t divr = (reg >> 8) & 0x3;
		return  (divr==3);
	}
}

const char* nlm_hal_xlp2xx_get_dev_name(soc_device_id_t dev) {
	static char* name[] = {
	"NAE",
	"SAE",
	"RSA",
	"GDX",
	"CMP",
	"NAND",
	"MMC",
	"GBU",
	"RGXF",
	"RGXS",
	"USB",
	"PIC",
	"NULL"
	};
	if(! ((dev>=XLP2XX_CLKDEVICE_NAE)&&(dev<=XLP2XX_CLKDEVICE_PIC)))
	{
		dev = XLP2XX_CLKDEVICE_NULL;
	}
	return name[dev-XLP2XX_CLKDEVICE_NAE];
}


xlp2xx_soc_freq_s xlp2xx_tbl_freq[2] = {
	/* slow freq for low voltage*/
      { .nae = 250,
	.sae = 250,
	.rsa = 250,
	.gdx = 333,
	.cmp = 333,
	.nand = 133,
	.mmc = 133,
	.gbu = 133,
	.rgxf = 250,
	.rgxs = 200,
	.usb = 167,
	.pic = 200 },
	/* high freq */
      { .nae = 500,
	.sae = 500,
	.rsa = 500,
	.gdx = 667,
	.cmp = 667,
	.nand = 133,
	.mmc = 133,
	.gbu = 133,
	.rgxf = 500,
	.rgxs = 450,
	.usb = 167,
	.pic = 400 },
};

void nlm_hal_adjust_soc_freqs(int node, int freq_sel)
{
	if(is_nlm_xlp2xx()) {
	xlp2xx_soc_freq_s* freq;
	freq = &xlp2xx_tbl_freq[freq_sel&0x1];
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_NAE, freq->nae*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_SAE, freq->sae*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_RSA, freq->rsa*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_GDX, freq->gdx*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_CMP, freq->cmp*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_NAND, freq->nand*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_MMC, freq->mmc*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_GBU, freq->gbu*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_RGXF, freq->rgxf*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_RGXS, freq->rgxs*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_USB, freq->usb*1000000);
        nlm_hal_set_soc_freq(node, XLP2XX_CLKDEVICE_PIC, freq->pic*1000000);
	}
}


#ifdef NLM_HAL_LINUX_KERNEL
#include <linux/types.h>
#include <linux/module.h>
EXPORT_SYMBOL(nlm_hal_get_soc_clock_state);
EXPORT_SYMBOL(nlm_hal_soc_clock_enable);
EXPORT_SYMBOL(nlm_hal_soc_clock_disable);
EXPORT_SYMBOL(nlm_hal_soc_clock_reset);
EXPORT_SYMBOL(nlm_hal_xlp2xx_set_clkdev_frq);
EXPORT_SYMBOL(nlm_hal_xlp2xx_get_clkdev_frq);
EXPORT_SYMBOL(nlm_hal_xlp2xx_get_pll_out_frq);
EXPORT_SYMBOL(nlm_hal_xlp2xx_get_dev_name);
EXPORT_SYMBOL(nlm_hal_get_soc_freq);
EXPORT_SYMBOL(nlm_hal_set_soc_freq);
EXPORT_SYMBOL(nlm_hal_get_core_freq);
EXPORT_SYMBOL(nlm_hal_set_core_freq);
EXPORT_SYMBOL(nlm_hal_cpu_freq);
EXPORT_SYMBOL(nlm_hal_is_ref_clk_133MHz);
EXPORT_SYMBOL(nlm_hal_get_ref_clk_freq);
EXPORT_SYMBOL(nlm_hal_adjust_soc_freqs);

#endif
