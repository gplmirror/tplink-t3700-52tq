
/*-
 * Copyright (c) 2003-2013 Broadcom Corporation
 * All Rights Reserved
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * http://www.gnu.org/licenses/gpl-2.0.txt
 * or the Broadcom license below:

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY BROADCOM ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL BROADCOM OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * #BRCM_4# */
#ifndef _NLM_EEPROM_H
#define _NLH_EEPROM_H

#include "nlm_hal.h"
/* MACID i2c memory definitions
 */
#define MAGIC_OFF   		0x00
#define MAGIC_LEN   		0x02
#define MAC_MAGIC_BYTE0 	0xAA
#define MAC_MAGIC_BYTE1 	0x55
#define MAC_OFF     		0x02
#define MAC_LEN     		0x06

#define NAME_OFF     		0x10
#define NAME_LEN     		0x08
#define REV_OFF     		0x18
#define REV_LEN     		0x03
#define SN_OFF      		0x1B
#define SN_LEN      		0x04
#define UPD_OFF     		0x1F
#define UPD_LEN     		0x02


struct eeprom_data{
	unsigned char magic_bytes0[MAGIC_LEN];
	unsigned char mac_addr0[MAC_LEN];
	unsigned char magic_bytes1[MAGIC_LEN];
	unsigned char mac_addr1[MAC_LEN];
	unsigned char name[NAME_LEN];
	unsigned char revision[REV_LEN];
	unsigned char srnum[SN_LEN];
	unsigned char upd[UPD_LEN];

	unsigned char i2c_dev_addr;
	void *priv;

	void(*eeprom_i2c_read_bytes)(unsigned char dev_addr, unsigned int addr, int alen, unsigned char * buf, int len);
	void(*eeprom_i2c_write_bytes)(unsigned char dev_addr, unsigned int addr, int alen, unsigned char * buf, int len);
};

extern int  eeprom_get_mac_addr(struct eeprom_data *nlm_eeprom, unsigned char *mac, int interface);
extern int  eeprom_set_mac_addr(struct eeprom_data *nlm_eeprom, unsigned char *mac, int interface);
extern int  eeprom_get_magic_bytes(struct eeprom_data *nlm_eeprom, unsigned char *mac, int interface);
extern int  eeprom_dump(struct eeprom_data *nlm_eeprom, unsigned char *mac, int offset, int len);
#endif
