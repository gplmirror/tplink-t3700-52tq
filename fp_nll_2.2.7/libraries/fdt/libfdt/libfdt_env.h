#ifndef _LIBFDT_ENV_H
#define _LIBFDT_ENV_H

#if defined(__XEN__)
#include <xen/types.h>
#include <xen/string.h>
#elif defined(NLM_HAL_NETLBOOT)
/* NetlBoot uses libfdt, but std includes
 * conflict with the ones in newlib
 */
#include <stddef.h>
#include <types.h>
#include <string.h>
#else
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#endif

#define _BYT(n)	((unsigned long long)((uint8_t *)&x)[n])
static inline uint32_t fdt32_to_cpu(uint32_t x)
{
	return (_BYT(0) << 24) | (_BYT(1) << 16) | (_BYT(2) << 8) | _BYT(3);
}
#define cpu_to_fdt32(x) fdt32_to_cpu(x)

static inline uint64_t fdt64_to_cpu(uint64_t x)
{
	return (_BYT(0) << 56) | (_BYT(1) << 48) | (_BYT(2) << 40) | (_BYT(3) << 32)
		| (_BYT(4) << 24) | (_BYT(5) << 16) | (_BYT(6) << 8) | _BYT(7);
}
#define cpu_to_fdt64(x) fdt64_to_cpu(x)
#undef _BYT

#endif /* _LIBFDT_ENV_H */
