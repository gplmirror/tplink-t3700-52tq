/********************************************************************

  Copyright 2003-2010 Netlogic Microsystem, Inc. ("Netlogic"). All rights
  reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in
  the documentation and/or other materials provided with the
  distribution.

  THIS SOFTWARE IS PROVIDED BY Netlogic Microsystems, Inc. ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RMI OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.

  *****************************#NLM_2#**********************************/

#include <linux/types.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>

#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/mman.h>
#include <linux/mm.h>
#include <linux/pci.h>
#include <linux/kthread.h>
#include <linux/inet_lro.h>
#include <linux/net_tstamp.h>
#include <linux/clocksource.h>
#include <linux/timecompare.h>
#include <linux/timer.h>
#include <linux/if.h>

#include <net/ip.h>

#include <asm/current.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <asm/netlogic/msgring.h>
#include <asm/netlogic/cpumask.h>

#include <asm/netlogic/hal/nlm_hal_fmn.h>
#include <asm/netlogic/hal/nlm_hal_nae.h>
#include <asm/netlogic/xlp.h>
#include <asm/netlogic/xlp_irq.h>
#include <asm/netlogic/hal/nlm_eeprom.h>

#include "net_common.h"
#include "xlp_nae.h"

#define TSO_ENABLED	1

/*Enable sanity checks while receiving or transmitting buffer */
#undef ENABLE_SANITY_CHECKS

/* Module Parameters */

static int perf_mode= NLM_TCP_MODE;
module_param(perf_mode, int, 0);

static int enable_lro =  0;
module_param(enable_lro, int, 0);

int num_descs_per_normalq = 64;
module_param(num_descs_per_normalq, int, 0);

int num_descs_per_jumboq = 32;
module_param(num_descs_per_jumboq, int, 0);

static int enable_jumbo = 0;
module_param(enable_jumbo, int, 0);

static int exclusive_vc = 0;
module_param(exclusive_vc, int, 0);

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
static int load_balance_timer_run = 1;
module_param(load_balance_timer_run, int, S_IRUGO|S_IWUSR);
#endif

/***************************************************************
 *
 * Below parameters are set during FDT file parsing
 */

extern uint32_t nae_rx_vc;
extern uint32_t nae_fb_vc;
/***************************************************************/

static int enable_napi =  1;
static int nlm_prepad_len = 0;
unsigned char eth_hw_addr[NLM_MAX_NODES][MAX_GMAC_PORT][6];
static unsigned int phys_cpu_map[NLM_MAX_NODES];
extern uint32_t cpu_2_normal_frfifo[NLM_MAX_NODES][NLM_NCPUS_PER_NODE];
extern uint32_t cpu_2_jumbo_frfifo[NLM_MAX_NODES][NLM_NCPUS_PER_NODE];

static uint64_t nlm_mode[NR_CPUS*8] ____cacheline_aligned;

#ifdef CONFIG_NLM_NET_OPTS
static struct sk_buff *last_rcvd_skb[NR_CPUS * 8] ____cacheline_aligned;
static uint32_t last_rcvd_len[NR_CPUS * 8] ____cacheline_aligned;
static uint32_t last_rcvd_node[NR_CPUS * 8] ____cacheline_aligned;
static uint64_t last_rcvd_skb_phys[NR_CPUS * 8] ____cacheline_aligned;
#endif

static uint64_t receive_count[NR_CPUS * 8] __cacheline_aligned;
static uint64_t fast_replenish_count[NR_CPUS * 8] __cacheline_aligned;
static uint64_t slow_replenish_count[NR_CPUS * 8] __cacheline_aligned;
static uint64_t err_replenish_count[NR_CPUS * 8] __cacheline_aligned;
static uint64_t p2p_dynamic_alloc_cnt[NR_CPUS * 8] __cacheline_aligned;

static struct net_device *per_cpu_netdev[NLM_MAX_NODES][NR_CPUS][24] __cacheline_aligned;
static struct pci_device_id soc_pci_table[] __devinitdata = {
        {PCI_NETL_VENDOR, PCI_DEVID_BASE + PCI_DEVID_OFF_NET,
         PCI_ANY_ID, PCI_ANY_ID, 0},
        {}
};
#ifdef CONFIG_INET_LRO
static int lro_flush_priv_cnt[NR_CPUS];
static int lro_flush_needed[NR_CPUS][20];
static struct dev_data *lro_flush_priv[NR_CPUS][20];
#endif
static uint64_t dummy_pktdata_addr[NLM_MAX_NODES];
static struct p2p_desc_mem p2p_desc_mem[NR_CPUS] __cacheline_aligned;
static struct net_device_stats *nlm_xlp_mac_get_stats(struct net_device *dev);
static struct net_device *dev_mac[NLM_MAX_NODES][MAX_GMAC_PORT];


extern void xlp_set_ethtool_ops(struct net_device *netdev);
extern void xlp_get_mac_stats(struct net_device* dev, struct net_device_stats* stats);
static void nlm_xlp_nae_init(void);
static int xlp_mac_proc_read(char *page, char **start, off_t off,int count, int *eof, void *data);
static int  nlm_xlp_nae_open (struct net_device *dev);
static int  nlm_xlp_nae_stop (struct net_device *dev);
static int  nlm_xlp_nae_start_xmit (struct sk_buff *skb, struct net_device *dev);
static void  nlm_xlp_set_multicast_list (struct net_device *dev);
static int  nlm_xlp_nae_ioctl (struct net_device *dev, struct ifreq *rq, int cmd);
static int  nlm_xlp_nae_change_mtu(struct net_device *dev, int new_mtu);
static void  nlm_xlp_nae_tx_timeout (struct net_device *dev);
static void xlp_mac_setup_hwaddr(struct dev_data *priv);
static int nlm_xlp_nae_set_hwaddr(struct net_device *dev, void *p);
extern void nlm_xlp_mac_set_enable(struct dev_data *priv, int flag);
extern struct proc_dir_entry *nlm_root_proc;
extern struct eeprom_data * get_nlm_eeprom(void);
#ifdef  ENABLE_NAE_PIC_INT
static irqreturn_t nlm_xlp_nae_int_handler(int irq, void * dev_id);
#endif
#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
static struct flow_meta_info *nlm_flow_meta_info;
static struct active_flow_list *nlm_active_flow_list;
static struct timer_list nlm_load_balance_timer[NR_CPUS];
static int nlm_load_balance_search_cpu[NUM_LOAD_BALANCE_CPU][NUM_LOAD_BALANCE_CPU-1];
static uint32_t nlm_pcpu_mask = 0;
static uint32_t *ucore_shared_data = NULL;
#endif

#ifdef CONFIG_NLM_XLP_CAMARO_BOARD
extern void bcm_power_phy(int inf, int powerup, int node);
#endif

#define Message(fmt, args...) { }
//#define Message(fmt, args...) printk(fmt, ##args)

//define MACSEC_DEBUG		1
#define MAC_HEADER_LEN		12
#define MAC_ADDR_LEN		6
#define MACSEC_ETHER_TYPE	0x88e5
#define PROTOCOL_TYPE_IP 	0x0800
#define MAC_SEC_PADDING		(12+16+16+16) /* !2 byte DA and SA, 16 byte preamble len, 16 byte sectag hear, 16 byte ICV len*/

void dump_buffer(unsigned char *buf, unsigned int len, unsigned char *msg)
{
    int k = 0;
    printk("\n%s\n",msg);
    for(k = 0; k < len; k++)
    {
        printk("%.2x ",buf[k]);
        if((k+1) % 16 == 0)
            printk("\n");
    }
    printk("\n");
}
static unsigned short  nlm_select_queue(struct net_device *dev, struct sk_buff *skb)
{
	        return (unsigned short)smp_processor_id();
}

static const struct net_device_ops nlm_xlp_nae_ops = {
	.ndo_open			= nlm_xlp_nae_open,
	.ndo_stop			= nlm_xlp_nae_stop,
	.ndo_start_xmit			= nlm_xlp_nae_start_xmit,
	.ndo_set_multicast_list		= nlm_xlp_set_multicast_list,
	.ndo_do_ioctl			= nlm_xlp_nae_ioctl,
	.ndo_tx_timeout 		= nlm_xlp_nae_tx_timeout,
	.ndo_change_mtu			= nlm_xlp_nae_change_mtu,
	.ndo_set_mac_address		= nlm_xlp_nae_set_hwaddr,
	.ndo_get_stats 			= nlm_xlp_mac_get_stats,
	.ndo_select_queue		= nlm_select_queue,
};

static __inline__ void cpu_halt(void)
{
	__asm__ volatile (".set push\n"
			  ".set noreorder\n"
			  "   wait\n"
			  "1: b    1b\n"
			  "   nop\n"
			  ".set pop\n"
		);
}
#ifdef CONFIG_NLM_XLP_CAMARO_BOARD
        extern unsigned char mac_cmdline[6];
#endif
static void gen_mac_address(void)
{
	struct eeprom_data *nlm_eeprom=NULL;
	unsigned char mac_base[6],temp,buf_write[2],buf0_read[2],buf1_read[2];
	int if_mac_set=0,mac0_set=0, mac1_set=0;
	int i,j;
	buf_write[0]= MAC_MAGIC_BYTE0;
	buf_write[1]= MAC_MAGIC_BYTE1;

	memset(mac_base, '0', 6);
	nlm_eeprom = get_nlm_eeprom();

	eeprom_get_magic_bytes(nlm_eeprom,buf0_read,0);/* signature*/
	eeprom_get_magic_bytes(nlm_eeprom,buf1_read,1);


	if((buf0_read[0]==buf_write[0]) && (buf0_read[1]==buf_write[1]))/*match the signature*/
	{
		mac0_set=1;
		eeprom_get_mac_addr(nlm_eeprom, mac_base,0);/* get the mac address*/
	}
	else if((buf1_read[0]==buf_write[0]) && (buf1_read[1]==buf_write[1]))
	{
		mac1_set=1;
		eeprom_get_mac_addr(nlm_eeprom, mac_base,1);/* get the mac address*/
	}

	for(temp=0;temp<6;temp++)
	{
		if(mac_base[temp]!=0)
		{
			if_mac_set=1;
		}
	}
	if( ((mac0_set | mac1_set) && if_mac_set) == 0){
#ifdef CONFIG_NLM_XLP_CAMARO_BOARD
                memcpy(mac_base, mac_cmdline, sizeof(mac_base));
#else
		random_ether_addr(mac_base);
#endif
	}

	for(i=0 ; i<NLM_MAX_NODES; i++){ /*poppulate the eth_hw_add array according to the get mac address*/
		for(j=0;j<18;j++){
			memcpy(eth_hw_addr[i][j], mac_base, 6);
			mac_base[5] += 1;
		}
	}
}



static __inline__ struct sk_buff *mac_get_skb_back_ptr(uint64_t addr)
{
        uint64_t *back_ptr = (uint64_t *)(unsigned long)(addr - SKB_BACK_PTR_SIZE);
        /* this function should be used only for newly allocated packets. It assumes
         * the first cacheline is for the back pointer related book keeping info
         */
        return (struct sk_buff *)(unsigned long)(*back_ptr);
}


#define CACHELINE_ALIGNED_ADDR(addr) (((unsigned long)(addr)) & ~(CACHELINE_SIZE-1))

/**********************************************************************
 * cacheline_aligned_kmalloc -  64 bits cache aligned kmalloc
 * return -  buffer address
 *
 **********************************************************************/
static __inline__ void *cacheline_aligned_kmalloc(int size, int gfp_mask)
{
        void *buf = kmalloc(size + CACHELINE_SIZE, gfp_mask);
        if (buf)
                buf =(void*)(unsigned long)(CACHELINE_ALIGNED_ADDR((unsigned long)buf +
						    CACHELINE_SIZE));
        return buf;
}

/*********************************************************************
  * set tso enable features in the dev list
 **********************************************************************/
static __inline__ int tso_enable(struct net_device *dev, u32 data)
{
	int rv;
	rv = ethtool_op_set_tso(dev, data);
	if(rv == 0)
		rv = ethtool_op_set_tx_csum(dev, data);
	if(rv == 0)
		rv = ethtool_op_set_sg(dev, data);
	dev->features |= NETIF_F_FRAGLIST | NETIF_F_HIGHDMA;
	return rv;
}

static int p2p_desc_mem_init(void)
{
	int cpu, cnt;
	int dsize, tsize;
	void *buf;
	/* MAX_SKB_FRAGS + 4.  Out of 4, 2 will be used for skb and freeback storage */
	dsize = ((((MAX_SKB_FRAGS + P2P_EXTRA_DESCS + MSEC_EXTRA_MEM) * sizeof(uint64_t)) + CACHELINE_SIZE - 1) & (~((CACHELINE_SIZE)-1)));
	tsize = dsize * MAX_TSO_SKB_PEND_REQS;

	printk("%s in, dsize %d tsize %d \n", __FUNCTION__, dsize, tsize);

	for(cpu = 0; cpu < NR_CPUS; cpu++) {
		buf = cacheline_aligned_kmalloc(tsize, GFP_KERNEL);
		if (!buf)
			return -ENOMEM;
		p2p_desc_mem[cpu].mem = buf;
		for(cnt = 1; cnt < MAX_TSO_SKB_PEND_REQS; cnt++) {
			*(unsigned long *)buf = (unsigned long)(buf + dsize);
			buf += dsize;
			*(unsigned long *)buf = 0;
		}
		p2p_desc_mem[cpu].dsize = dsize;
	}
	return 0;
}

static inline void *alloc_p2p_desc_mem(int cpu)
{
	void *buf;
	buf = p2p_desc_mem[cpu].mem;
	if(buf) {
		p2p_desc_mem[cpu].mem = (void *)*(unsigned long *)(buf);
	} else {
		buf = cacheline_aligned_kmalloc(p2p_desc_mem[cpu].dsize, GFP_KERNEL);
		p2p_dynamic_alloc_cnt[CPU_INDEX(cpu)]++;
	}
	return buf;
}

static inline void free_p2p_desc_mem(int cpu, void *buf)
{
	*(unsigned long *)buf = (unsigned long)p2p_desc_mem[cpu].mem;
	p2p_desc_mem[cpu].mem = buf;

}

static inline int create_p2p_desc(uint64_t paddr, uint64_t len, uint64_t *p2pmsg, int idx)
{
	int plen;
	do {
		plen = len >= MAX_PACKET_SZ_PER_MSG ? (MAX_PACKET_SZ_PER_MSG - 64): len;
		p2pmsg[idx] = cpu_to_be64(nae_tx_desc(P2D_NEOP, 0, NULL_VFBID, plen, paddr));
		len -= plen;
		paddr += plen;
		idx++;
	} while(len > 0);
	return idx;
}

static inline void create_last_p2p_desc(uint64_t *p2pmsg, struct sk_buff *skb, int idx)
{
	p2pmsg[idx -1 ] = cpu_to_be64(be64_to_cpu(p2pmsg[idx - 1]) | ((uint64_t)P2D_EOP << 62));
	p2pmsg[P2P_SKB_OFF] = (uint64_t)(unsigned long)skb;
}

uint16_t pseuodo_chksum(uint16_t *ipsrc, uint16_t proto)
{
	uint32_t sum = 0;
	sum += cpu_to_be16(ipsrc[0]);
	sum += cpu_to_be16(ipsrc[1]);
	sum += cpu_to_be16(ipsrc[2]);
	sum += cpu_to_be16(ipsrc[3]);
	sum += proto;
	while(sum >> 16)
		sum = (sum & 0xffff)  + (sum >> 16);
	//      sum = ~sum;
	return (uint16_t)sum;
}

static __inline__ uint64_t nae_tso_desc0(
		unsigned int type,
		unsigned int subtype,
		unsigned int opcode,
		unsigned int param_index,
		unsigned int l3hdroff,
		unsigned int l4hdroff,
		unsigned int l3chksumoff,
		unsigned int pseudohdrchksum,
		unsigned int l4chksumoff,
		unsigned int pyldoff)
{

	return ((uint64_t)(type & 0x3) << 62) |
		((uint64_t)(subtype & 3) << 60) |
		((uint64_t)(opcode & 0xf) << 56) |
		((uint64_t)(param_index & 0xf) << 49) |
		((uint64_t)(l3hdroff & 0x3f) << 43) |
		((uint64_t)(l4hdroff & 0x7f) << 36) |
		((uint64_t)(l3chksumoff & 0x1f) << 31) |
		((uint64_t)(pseudohdrchksum & 0xffff) << 15) |
		((uint64_t)(l4chksumoff & 0x7f) << 8) |
		((uint64_t)(pyldoff & 0xff));
}

static __inline__ uint64_t nae_tso_desc1(
		unsigned int type,
		unsigned int subtype,
		unsigned int poly,
		unsigned int mss,
		unsigned int crcstopoff,
		unsigned int crcinsoff)
{
	return ((uint64_t)(type & 0x3) << 62) |
		((uint64_t)(subtype & 3) << 60) |
		((uint64_t)(poly & 0x3) << 48) |
		((uint64_t)(mss & 0xffff) << 32) |
		((uint64_t)(crcstopoff & 0xffff) << 16) |
		((uint64_t)(crcinsoff & 0xffff));

}

#define NUM_VC_PER_THREAD 4
#define NUM_CPU_VC	  128
#define RX_PARSER_EN 	1

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
#define RX_PPAD_EN 	1
#define RX_PPAD_SZ	0
#else
#define RX_PPAD_EN 	0
#define RX_PPAD_SZ	3
#endif

static void nlm_enable_l3_l4_parser(int node)
{
	int l2proto = 1; //ethernet
	int port = 0, i, ipchk = 1;
	uint32_t val = 0;
	uint32_t naereg;

	//enabling hardware parser
	naereg = nlm_hal_read_nae_reg(node, RX_CONFIG);
	nlm_hal_write_nae_reg(node, RX_CONFIG, (naereg | RX_PARSER_EN << 12 | RX_PPAD_EN << 13 | RX_PPAD_SZ << 22));
	//printk("Enabling parser, reg content = %x\n", nlm_hal_read_nae_reg(node, RX_CONFIG));

	/* enabling extraction of data */
	for(i=0; i<16;i++)
		nlm_hal_write_nae_reg(node, L2_TYPE_0 + i, l2proto);

	nlm_hal_write_nae_reg(node, L3_CTABLE_MASK_0, port | 0 << 5 | 1 << 6); // l2proto and ethtype included

	val = ((0 << 26) | (9 << 20) | (ipchk << 18) | (1 << 16) | (0x800));
	nlm_hal_write_nae_reg(node, L3_CTABLE_0_0, val);
	val =   (12 << 26) | (4 << 21) | (16 << 15) | (4 << 10); /* extract sourceip and dstip */
	nlm_hal_write_nae_reg(node, L3_CTABLE_0_1, val);

	nlm_hal_write_nae_reg(node, L4_CTABLE_0_0, 1 << 17 | 0x6); /* ip_proto = tcp */
	val = ((0 << 21) | (2 << 17) | (2 << 11) | (2 << 7)); /* extract source and dst port*/
	nlm_hal_write_nae_reg(node, L4_CTABLE_0_1, val);

}

static int lro_get_skb_hdr(struct sk_buff *skb, void **iphdr, void **tcph,
		u64 *hdr_flags, void *priv)
{
	skb_reset_network_header(skb);
	skb_set_transport_header(skb, ip_hdrlen(skb));

	if(ip_hdr(skb)->protocol != 0x6)
		return -1;

	*iphdr = ip_hdr(skb);
	*tcph = tcp_hdr(skb);

	*hdr_flags = LRO_IPV4 | LRO_TCP;

	return 0;
}

void lro_init(struct net_device *dev)
{
	struct dev_data* priv;
	static int done = 0;
	int cpu;
	priv = netdev_priv(dev);

#ifdef CONFIG_INET_LRO
	if(enable_lro) {
		printk("LRO is enabled \n");
		dev->features |= NETIF_F_LRO;
		for (cpu=0; cpu<NR_CPUS; cpu++) {
			memset(&priv->lro_mgr[cpu], 0, sizeof(struct net_lro_mgr));
			priv->lro_mgr[cpu].max_aggr = 48;
			priv->lro_mgr[cpu].max_desc = LRO_MAX_DESCS;
			priv->lro_mgr[cpu].get_skb_header = lro_get_skb_hdr;
			priv->lro_mgr[cpu].features = LRO_F_NAPI;
			priv->lro_mgr[cpu].dev = dev;
			priv->lro_mgr[cpu].ip_summed = CHECKSUM_UNNECESSARY;
			priv->lro_mgr[cpu].ip_summed_aggr = CHECKSUM_UNNECESSARY;
			priv->lro_mgr[cpu].lro_arr = cacheline_aligned_kmalloc(
					sizeof(struct net_lro_desc) * LRO_MAX_DESCS, GFP_KERNEL);
			memset(priv->lro_mgr[cpu].lro_arr, 0, sizeof(struct net_lro_desc) * LRO_MAX_DESCS);
		}
	}
#endif
	if(!done) {
		done = 1;
		nlm_enable_l3_l4_parser(priv->node);
	}
}

#if 0
static void dump_skbuff (struct sk_buff *skb)
{
	int cpu = hard_smp_processor_id();
	char buf[512];
	int blen = 0, i, len = 64;
	unsigned char *data = skb->data;

	for(i = 0; i < len;) {
		if(i != 0 && (i % 16 == 0))
			blen += sprintf(&buf[blen], "\n");
		blen += sprintf(&buf[blen], "%02x ", data[i]);
		i++;
	}
	printk("data recived on cpu %d len %d = \n%s\n", cpu, len, buf);
}
#endif

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING

static void dump_cpu_active_flow_info(int cpu, struct seq_file *m, int weight)
{
	struct active_flow_list *afl = nlm_active_flow_list + cpu;
	unsigned long mflags;

	if(!nlm_active_flow_list || !nlm_flow_meta_info)
		return;

	spin_lock_irqsave(&afl->lock, mflags);
	seq_printf(m, "Cpu%d ==> WeightInUcore %d ActiveFlows %llu, FlowCreated %llu, FlowProcessed %llu\n", cpu, weight, (unsigned long long)afl->nr_active_flows, (unsigned long long)afl->nr_flow_created, (unsigned long long)afl->nr_flow_processed);
	afl->nr_flow_created = 0;
	afl->nr_flow_processed = 0;
	spin_unlock_irqrestore(&afl->lock, mflags);
	return;
}

static int nlm_dump_load_balance_stats(struct seq_file *m, void *v)
{
	int i = 0;
	uint32_t cpu_weight[32] = {0};

	if(!nlm_active_flow_list || !nlm_flow_meta_info){
		return 0;
	}

	for(i=0; i<NLM_UCORE_SHARED_TABLE_SIZE; i++){
		cpu_weight[*(ucore_shared_data + i)]++;
	}

	for(i=0; i<NUM_LOAD_BALANCE_CPU; i++){
		if(!cpu_isset(i, phys_cpu_present_map))
        	continue;
		dump_cpu_active_flow_info(i, m, cpu_weight[__cpu_number_map[i]]);
	}
	return 0;
}

static int nlm_load_balance_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, nlm_dump_load_balance_stats, NULL);
}

struct file_operations nlm_load_balance_proc_fops = 
{
		.owner = THIS_MODULE,
		.open = nlm_load_balance_proc_open,
		.read = seq_read,
		.llseek = seq_lseek,
		.release = seq_release,
};

static void nlm_remove_inactive_flow(int cpu)
{
	int i = 0;
	struct active_flow_list *afl = nlm_active_flow_list + cpu;
	uint64_t bytes_received;
	struct flow_meta_info *fmi;
	int index;
	unsigned long mflags;

	spin_lock_irqsave(&afl->lock, mflags);
	for(i=0; i<afl->nr_active_flows; i++){
		index = afl->index_to_flow_meta_info[i];
		fmi = nlm_flow_meta_info + index;
#ifdef LOAD_BALANCE_DEBUG_ENABLE
		if(index > NLM_UCORE_SHARED_TABLE_SIZE){
				printk("??? Index %d\n",index);
		}
#endif
		bytes_received = fmi->total_bytes_rcvd - fmi->last_sample_bytes;
		fmi->last_sample_bytes = fmi->total_bytes_rcvd;
		mb();
		if(bytes_received == 0){
#ifdef LOAD_BALANCE_DEBUG_ENABLE
			if(fmi->cpu_owner != cpu){
					printk("Error!! fmi->cpu_owner = %lld, cpu %d, entry %d, total_active_flows %lld\n", fmi->cpu_owner, cpu, i, afl->nr_active_flows);
					continue;
			}
#endif
			/*No Packet Received on this flow!!! Delete the entry..*/
			if(i+1 < afl->nr_active_flows){
				/*Copy the last valid map to here..*/
				afl->index_to_flow_meta_info[i] = afl->index_to_flow_meta_info[afl->nr_active_flows-1];
				afl->index_to_flow_meta_info[afl->nr_active_flows - 1] = 0;
			}else{
				afl->index_to_flow_meta_info[i] = 0;
			}
			i--;
			afl->nr_active_flows--;
			afl->nr_flow_processed++;
			fmi->cpu_owner = -1;
			mb();
			continue;
		}
		mb();
	}
	spin_unlock_irqrestore(&afl->lock, mflags);
}

static void setup_search_path(void)
{
	int i = 0;
	int thrds[4] = {0, 1, 2, 3};
	int core;
	int previous, next;
	int index = 0;
	int num_phy_cpu=0;
	unsigned int pcpu;
	int previous_core, next_core;
#ifdef LOAD_BALANCE_DEBUG_ENABLE
	int j;
	unsigned char buf[256];
#endif

	for(i=0; i<NUM_LOAD_BALANCE_CPU; i++){
		if(!cpu_isset(i, phys_cpu_present_map))
			continue;
		nlm_pcpu_mask |= (1UL<<i);
		num_phy_cpu++;
	}
#ifdef LOAD_BALANCE_DEBUG_ENABLE
	printk("num_phy_cpu %d, nlm_pcpu_mask %#x\n",num_phy_cpu, nlm_pcpu_mask);
#endif
	for(pcpu=0; pcpu<NUM_LOAD_BALANCE_CPU; pcpu++){

		if(!((1U<<pcpu) & nlm_pcpu_mask))
			continue;

		core = pcpu/4;
		index = 0;
		for(i=0; i<3; i++){
			nlm_load_balance_search_cpu[pcpu][index] = thrds[(pcpu+i+1)%4] + core*4;
			if(((1U<<nlm_load_balance_search_cpu[pcpu][index]) & nlm_pcpu_mask)){
				index++;
			}
		}
		if(core >= 1){
			previous = (core-1)*4;
		}else{
			previous = 28;
		}
		previous_core = previous/4;

		if(core < 7){
			next = (core+1)*4;
		}else{
			next = 0;
		}
		next_core = next/4;

		while(index<(num_phy_cpu-1)){
			if((0xfU<<previous) & nlm_pcpu_mask){
				for(i=0; i<4; i++){
					if((1UL<<(i+previous) & nlm_pcpu_mask)){
						nlm_load_balance_search_cpu[pcpu][index] = i+previous;
						index++;
					}
				}
			}
			if((previous_core) >= 1){
				previous = (previous_core-1)*4;
			}else{
				previous = 28;
			}
			previous_core = previous/4;
			if((0xfU<<next) & nlm_pcpu_mask){
				for(i=0; i<4; i++){
					if((1UL<<(i+next)) & nlm_pcpu_mask){
						nlm_load_balance_search_cpu[pcpu][index] = i+next;
						index++;
					}
				}
			}
			if(next_core < 7){
				next = (next_core+1)*4;
			}else{
				next = 0;
			}
			next_core = next/4;
		}
	}
#ifdef LOAD_BALANCE_DEBUG_ENABLE
	printk("\nSearchPath:\n");
	for(i=0; i<NUM_LOAD_BALANCE_CPU; i++){
		int len;
		sprintf(buf, "Cpu%d ==> ", i);
		len = strlen(buf);
		for(j=0; j<NUM_LOAD_BALANCE_CPU-1; j++){
			sprintf(buf+len, "%2d ", nlm_load_balance_search_cpu[i][j]);
			len = strlen(buf);
		}
		sprintf(buf+len,"\n");
		printk(buf);
	}
#endif
	return;
}

static void nlm_load_balance_timer_func(unsigned long arg)
{
	int cpu = hard_smp_processor_id();
	struct active_flow_list *myafl = nlm_active_flow_list + cpu;
	struct active_flow_list *afl = NULL;
	int i = 0, j;
	int idx_to_fmi;
	struct flow_meta_info *fmi;
	unsigned long mflags;
	int lcpu;
	/*Remove inactive flows.*/
	nlm_remove_inactive_flow(cpu);

	/*Check if there is a `threshold %` of diff in load between us and any other cpus. If so
	 borrow few flows from it.
	 */
	for(i=0; i<(NUM_LOAD_BALANCE_CPU-1); i++){
		if(!cpu_isset(i, phys_cpu_present_map))
			continue;
		afl = nlm_active_flow_list + nlm_load_balance_search_cpu[cpu][i];
		Message("Checking for CPU %d\n", nlm_load_balance_search_cpu[cpu][i]);

		if(afl->nr_active_flows <= 1)
			continue;

		if(myafl->nr_active_flows < (afl->nr_active_flows - 1)){
			/*Borrow a flow from this cpu*/
			Message("NR_ACTIVE_FLOWS %ld",afl->nr_active_flows);

restart:
			spin_lock_irqsave(&afl->lock, mflags);
			for(j=0; j<afl->nr_active_flows; j++){
					fmi = nlm_flow_meta_info + afl->index_to_flow_meta_info[j];

					if(myafl->nr_active_flows >= (afl->nr_active_flows - 1)){
						break;
					}
					if(fmi->cpu_owner != nlm_load_balance_search_cpu[cpu][i]){
							spin_unlock_irqrestore(&afl->lock, mflags);
							Message("Flow is borrowed by cpu %lld\n",fmi->cpu_owner);
							goto restart;
					}
					/*Borrow a flow...*/
					fmi->cpu_owner = cpu;
					idx_to_fmi = afl->index_to_flow_meta_info[j];
					/*Update the active flow list of the cpu from which we just
					  borrowed the flow.*/
					if(j+1 < afl->nr_active_flows){
							/*Copy the last valid map to here..*/
							afl->index_to_flow_meta_info[j] = 
									afl->index_to_flow_meta_info[afl->nr_active_flows - 1];
							afl->index_to_flow_meta_info[afl->nr_active_flows-1] = 0;
					}else{
							afl->index_to_flow_meta_info[j] = 0;
					}
					j--;
					afl->nr_active_flows--;
					spin_unlock_irqrestore(&afl->lock, mflags);

					spin_lock_irqsave(&myafl->lock, mflags);
					/*Create a new entry for this FMI in ACTIVE_FLOW_LIST 
					  Change the u-core shared memroy once all data structures
					  are in place.
					 */
					myafl->index_to_flow_meta_info[myafl->nr_active_flows] = idx_to_fmi;
					myafl->nr_active_flows++;
					mb();
					spin_unlock_irqrestore(&myafl->lock, mflags);

					/*Update ucore shared memory*/
					lcpu = __cpu_number_map[cpu];
					nlm_hal_modify_nae_ucore_sram_mem(0, 0, &lcpu, NLM_UCORE_SHMEM_OFFSET+(idx_to_fmi*4),1);
					*(ucore_shared_data + idx_to_fmi) = __cpu_number_map[cpu];

					spin_lock_irqsave(&afl->lock, mflags);
			}
			spin_unlock_irqrestore(&afl->lock, mflags);
		}
	}
	if(!myafl->nr_active_flows)
		mod_timer(&nlm_load_balance_timer[cpu], jiffies + 50 );
	else
		mod_timer(&nlm_load_balance_timer[cpu], jiffies + load_balance_timer_run*HZ );
}

void nlm_setup_load_balance_timer(void *data)
{
	int pcpu = hard_smp_processor_id();
	nlm_load_balance_timer[pcpu].expires = jiffies + 5*HZ;
	add_timer(&nlm_load_balance_timer[pcpu]);
}

static void nlm_init_load_balance(void)
{
	int i = 0;
	int j = 0;
	uint32_t signature;
	nlm_active_flow_list = vmalloc(sizeof(struct active_flow_list) * NUM_LOAD_BALANCE_CPU);

	if(!nlm_active_flow_list){
		printk("\nAllocation Failed!!! for size %lu bytes\n", sizeof(struct active_flow_list) * NUM_LOAD_BALANCE_CPU);
		return;
	}
	memset(nlm_active_flow_list, 0, sizeof(struct active_flow_list)*NUM_LOAD_BALANCE_CPU);
#ifdef LOAD_BALANCE_DEBUG_ENABLE
	printk("Allocated active_flow_list @ %#lx, size %lu bytes\n", (unsigned long)nlm_active_flow_list, sizeof(struct active_flow_list) * NUM_LOAD_BALANCE_CPU);
#endif	
	nlm_flow_meta_info = vmalloc(sizeof(struct flow_meta_info)*(NLM_UCORE_SHARED_TABLE_SIZE+1));

	if(!nlm_flow_meta_info){
		printk("\nAllocation Failed!!! for size %lu bytes\n", sizeof(struct flow_meta_info) * NUM_LOAD_BALANCE_CPU);
		return;
	}
	memset(nlm_flow_meta_info, 0, sizeof(struct flow_meta_info)*(NLM_UCORE_SHARED_TABLE_SIZE+1));
#ifdef LOAD_BALANCE_DEBUG_ENABLE
	printk("Allocated flow_meta_info @ %#lx, size %lu bytes\n",(unsigned long)nlm_flow_meta_info, sizeof(struct flow_meta_info)*(NLM_UCORE_SHARED_TABLE_SIZE+1));
#endif	

	/*Init per cpu flow lock*/
	for(i=0; i<NUM_LOAD_BALANCE_CPU; i++){
		spin_lock_init(&((nlm_active_flow_list + i)->lock));
	}

	/*Set owner field to -1*/
	for(i=0; i<NLM_UCORE_SHARED_TABLE_SIZE; i++){
		(nlm_flow_meta_info+i)->cpu_owner = -1;
	}

	for(i=0; i<NUM_LOAD_BALANCE_CPU; i++){
		if(!cpu_isset(i, phys_cpu_present_map))
			continue;
		/*Register a timer for each cpu to calculate the flow rate*/
		setup_timer(&nlm_load_balance_timer[i], nlm_load_balance_timer_func, i);
	}
	smp_call_function(nlm_setup_load_balance_timer, NULL, 1);

	/*Add timer on self*/
	preempt_disable();
	nlm_load_balance_timer[hard_smp_processor_id()].expires = jiffies + 5*HZ;
	add_timer(&nlm_load_balance_timer[hard_smp_processor_id()]);
	preempt_enable();

	setup_search_path();
	/*Update ucore shared memory with the table*/
	ucore_shared_data = vmalloc(NLM_UCORE_SHARED_TABLE_SIZE*sizeof(uint32_t));
	if(!ucore_shared_data){
			printk("Ucore updation failed!!\n");
			return;
	}

	for(i=0; i<NLM_UCORE_SHARED_TABLE_SIZE; ){
			for(j=0; j<NUM_LOAD_BALANCE_CPU && i<NLM_UCORE_SHARED_TABLE_SIZE; j++){
				if(nlm_pcpu_mask & (1U<<j)){
					*(ucore_shared_data + i) = __cpu_number_map[j];
					i++;
				}
			}
	}
	nlm_hal_modify_nae_ucore_sram_mem(0, 0, ucore_shared_data, NLM_UCORE_SHMEM_OFFSET, NLM_UCORE_SHARED_TABLE_SIZE);
	mb();
	signature = 0xdeadbeefU;
	nlm_hal_modify_nae_ucore_sram_mem(0, 0, &signature, NLM_UCORE_SHMEM_OFFSET-4, 1);
}

#ifdef LOAD_BALANCE_DEBUG_ENABLE
static void dump_packet(unsigned char *vaddr, int len)
{
	int i = 0;
	for(i=0; i<len;){
		printk("[%#x] [%#x] [%#x] [%#x] [%#x] [%#x] [%#x] [%#x]\n", *(vaddr + i + 0),
			*(vaddr + i + 1), *(vaddr + i + 2), *(vaddr + i + 3), *(vaddr + i + 4), *(vaddr + i + 5), *(vaddr + i + 6), *(vaddr + i + 7));
		i=i+8;
	}
}

static void dump_prepad(unsigned char *vaddr)
{
	int i = 0;
	unsigned int *tmp = (unsigned int *)vaddr;
	if(*tmp == 0xdeadbeef){
		for(i=0; i<4; i++){
				printk("[%d] ==> [%#x]\n",i, *(tmp+i));
		}
	}else{
#if 1
	for(i=0; i<16;){
		printk("[%#x] [%#x] [%#x] [%#x] [%#x] [%#x] [%#x] [%#x]\n", *(vaddr + i + 0),
			*(vaddr + i + 1), *(vaddr + i + 2), *(vaddr + i + 3), *(vaddr + i + 4), *(vaddr + i + 5), *(vaddr + i + 6), *(vaddr + i + 7));
		i=i+8;
	}
#endif
	}
}
#endif

static inline void nlm_update_flow_stats(unsigned int *prepad, uint32_t len, uint32_t context)
{
	int hash_index;
	int cpu = hard_smp_processor_id();
	unsigned long mflags;
	struct active_flow_list *afl = nlm_active_flow_list + cpu;
	struct flow_meta_info *fmi;
	uint64_t index;

	if(perf_mode != NLM_TCP_MODE)
		return;

	if(*prepad != NLM_LOAD_BALANCING_MAGIC){
		/*No extractions have happend...*/
#ifdef LOAD_BALANCE_DEBUG_ENABLE
		printk("Invalid Packet!!\n");
		dump_packet(prepad, 64+16);
#endif
		return;
	}

	hash_index = *(prepad + 1);
	fmi = nlm_flow_meta_info + hash_index;

	if(unlikely(fmi->cpu_owner == -1)){
		/*New flow, Create an entry in active flow list*/
		local_irq_save(mflags);
		if(!spin_trylock(&afl->lock)){
			local_irq_restore(mflags);
			return;
		}
		if(fmi->cpu_owner != -1){
			spin_unlock(&afl->lock);
			local_irq_restore(mflags);
			return; 
		}
		fmi->cpu_owner = cpu;
		index = afl->nr_active_flows;
#ifdef LOAD_BALANCE_DEBUG_ENABLE
		if(index >= NLM_UCORE_SHARED_TABLE_SIZE){
				printk("Index %lld Crossing Table size...\n", index);
		}
#endif
		afl->index_to_flow_meta_info[index] = hash_index;
		mb();
		afl->nr_active_flows++;
		afl->nr_flow_created++;
		spin_unlock(&afl->lock);
		local_irq_restore(mflags);
	}
	fmi->total_bytes_rcvd += len;
	mb();
	return;
}

#endif


#ifdef CONFIG_NLM_NET_OPTS
/* Get the hardware replenishment queue id */
static int get_hw_frfifo_queue_id(int rxnode, nlm_nae_config_ptr nae_cfg, int cpu, unsigned int truesize)
{
	/* We have to use the logical map here as the below arrays are indexed by logical cpu id
	 */
	int qid;
	int node_cpu = __cpu_number_map[cpu] % NLM_NCPUS_PER_NODE;

	qid = cpu_2_normal_frfifo[rxnode][node_cpu];
	if (enable_jumbo)
	{
		if(truesize > NLM_RX_JUMBO_BUF_SIZE)
			qid = cpu_2_jumbo_frfifo[rxnode][node_cpu];
	}
	/* all the nodes vfbtable should be filled with starting node of 0 to ending node
	 with 20 entries each */
	return nae_cfg->vfbtbl_hw_offset + (rxnode * NLM_NAE_MAX_FREEIN_FIFOS_PER_NODE) + qid;
}
#endif

static int mac_refill_frin_skb(int node, int cpu, uint64_t paddr, unsigned int bufsize)
{
	/* We have to use the logical map here as the below arrays are indexed by logical cpu id
	 */
	int ret, code, qid;
	nlm_nae_config_ptr nae_cfg;
	int node_cpu = __cpu_number_map[cpu] % NLM_NCPUS_PER_NODE;
	unsigned long __attribute__ ((unused)) mflags;


	qid = (bufsize >= NLM_RX_JUMBO_BUF_SIZE) ? cpu_2_jumbo_frfifo[node][node_cpu] : cpu_2_normal_frfifo[node][node_cpu];

	nae_cfg = nlm_node_cfg.nae_cfg[node];
	if(nae_cfg == NULL) {
		printk("%s Error, Invalid node id %d\n", __FUNCTION__, node);
		return -1;
	}
	Message("%s in cpu %d bufsize %d node %d qid %d qbase %d\n", __FUNCTION__, cpu, bufsize, node, qid,  nae_cfg->frin_queue_base);

	ret = 0;
	qid = nae_cfg->frin_queue_base + qid;

	/* Assumption: SKB is all set to go */
	/* Send the free Rx desc to the MAC */
	code = 0;

	/* Send the packet to nae rx  */
	msgrng_access_enable(mflags);
	for(;;) {
	  ret = nlm_hal_send_msg1(qid, code, (paddr & 0xffffffffffULL) );
	  if (!ret) break;
	}
	msgrng_access_disable(mflags);

	return ret;
}

static int mac_refill_frin_one_buffer(struct net_device *dev, int cpu, unsigned int truesize)
{
	struct dev_data* priv = netdev_priv(dev);
	struct sk_buff * skb;
	int buf_size = NLM_RX_ETH_BUF_SIZE;

	if (enable_jumbo)
	{
		if(truesize > NLM_RX_JUMBO_BUF_SIZE)
			buf_size = NLM_RX_JUMBO_BUF_SIZE;
	}

	skb = nlm_xlp_alloc_skb_atomic(buf_size, priv->node);
	if(!skb)
	{
		printk("[%s] alloc skb failed\n",__FUNCTION__);
		panic("panic...");
		return -ENOMEM;
	}

	skb->dev = dev;

	mac_put_skb_back_ptr(skb);

	return mac_refill_frin_skb(priv->node, cpu, (unsigned long long)virt_to_bus(skb->data), buf_size);
}

static int nae_proc_read(char *page, char **start, off_t off,
			     int count, int *eof, void *data)
{
	int len = 0;
	int i = 0;
	uint64_t total_err = 0, total_fast = 0, total_slow = 0, total_recv = 0;

	for(i=0; i<32; i++){
		printk("cpu%d, recv %ld fast_repl %ld, slow_repl %ld, err_repl %ld p2pdalloc %lld\n",i,
			(unsigned long)receive_count[CPU_INDEX(i)],
			(unsigned long)fast_replenish_count[CPU_INDEX(i)],
			(unsigned long)slow_replenish_count[CPU_INDEX(i)],
			(unsigned long)err_replenish_count[CPU_INDEX(i)],
			p2p_dynamic_alloc_cnt[CPU_INDEX(i)]);

		total_err += err_replenish_count[CPU_INDEX(i)];
		total_fast += fast_replenish_count[CPU_INDEX(i)];
		total_slow += slow_replenish_count[CPU_INDEX(i)];
		total_recv += receive_count[CPU_INDEX(i)];

		p2p_dynamic_alloc_cnt[CPU_INDEX(i)] = 0;
		slow_replenish_count[CPU_INDEX(i)] = 0;
		fast_replenish_count[CPU_INDEX(i)] = 0;
		err_replenish_count[CPU_INDEX(i)] = 0;
		receive_count[CPU_INDEX(i)] = 0;
	}
	/*check how many hash are empty...*/
	printk("TOTAL_FAST_REPL %ld, TOTAL_SLOW_REPL %ld, TOTAL_ERR_REPL %ld TOTAL_RECV %ld\n",
			(unsigned long)total_fast,
			(unsigned long)total_slow,
			(unsigned long)total_err,
			(unsigned long)total_recv);

	*eof = 1;
	return len;
}


static inline void process_tx_complete(int cpu, unsigned int src_id, unsigned long long msg0)
{
	struct sk_buff* skb;
#ifdef TSO_ENABLED
	uint64_t *p2pfbdesc;
#endif
	unsigned long long addr;
	unsigned int context, port, node;

	Message("%s cpu %d src_id %d\n", __FUNCTION__, cpu, src_id);

	/* Process Transmit Complete, addr is the skb pointer */
	addr = msg0 & 0xffffffffffULL;

	/* context field is currently unused */
	context = (msg0 >> 40) & 0x3fff;
	node = (src_id >> 10) & 0x3;
	port = *(cntx2port[node] + context);

	if (addr == dummy_pktdata_addr[node]){
		printk("Dropping firmware RX packet (addr=%llx)!\n", addr);
		return;
	}

#ifdef TSO_ENABLED
	if(nlm_mode[CPU_INDEX(cpu)] == NLM_TCP_MODE){
		p2pfbdesc = bus_to_virt(addr);
		skb = (struct sk_buff *)(unsigned long)(p2pfbdesc[P2P_SKB_OFF]);
		free_p2p_desc_mem(cpu, p2pfbdesc);
	} else
#endif
		skb = (struct sk_buff *)bus_to_virt(addr);

	if(skb)
	{

		dev_kfree_skb_any(skb);
	}
	else {
		printk("[%s]: [txc] Null skb? paddr = %llx (halting cpu!)\n", __func__, addr);
		cpu_halt();
	}
}

static inline void process_rx_packets(int cpu, unsigned int src_id,
		unsigned long long msg0, unsigned long long msg1)
{
	uint64_t addr;
	uint32_t len, context, truesize;
	int port, node, err;
	struct net_device *pdev;
	struct dev_data *priv = NULL;
	uint64_t vaddr;
	struct sk_buff* skb;
	uint32_t msec_port;
	nlm_nae_config_ptr nae_cfg;


	err = (msg1 >> 4) & 0x1;

	/* Rx packet */
	addr	= msg1 & 0xffffffffc0ULL;
	len	= (msg1 >> 40) & 0x3fff;
	context = (msg1 >> 54) & 0x3ff;
	node = (src_id >> 10) & 0x3;

	Message("%s in cpu %d src_id %d len %d context %d node %d err %d\n", __FUNCTION__,
			cpu, src_id, len, context, node, err);
	if (err) {

		vaddr = (uint64_t)(unsigned long)bus_to_virt(addr);
		skb = mac_get_skb_back_ptr(vaddr);
		mac_refill_frin_skb(node, cpu, addr, skb->truesize);
		/*
		   Commenting as priv is not available here
		   STATS_INC(priv->stats.rx_errors);
		   STATS_INC(priv->stats.rx_dropped);
		 */
		err_replenish_count[CPU_INDEX(cpu)]++;
		return;
	}

	if (addr == dummy_pktdata_addr[node]){
		printk("Dropping firmware RX packet (addr=%llx)!\n", addr);
		return;
	}
	port = *(cntx2port[node] + context);

#ifdef ENABLE_SANITY_CHECKS
	if(port >= MAX_GMAC_PORT)
	{
		printk("[%s]: bad port=%d, context=%d\n", __func__, port, context);
		/*TODO: Where to replenish this packet ???? Context is out of range!*/
		return;
	}
#endif
	pdev = per_cpu_netdev[node][cpu][port];
#ifdef ENABLE_SANITY_CHECKS
	if(!pdev) {
		printk("[%s]: [rx] wrong port=%d(context=%d)? pdev = NULL!\n", __func__, port, context);
		return;
	}
#endif
	priv = netdev_priv(pdev);

	vaddr = (uint64_t)(unsigned long)bus_to_virt(addr);

	len = len  - ETH_FCS_LEN - nlm_prepad_len;

	skb = mac_get_skb_back_ptr(vaddr);

#ifdef ENABLE_SANITY_CHECKS
	if (!skb) {
		STATS_INC(priv->stats.rx_dropped);
		printk("[%s] Null skb? addr=%llx, vaddr=%llx, dropping it and losing one buffer!\n",
				__func__, addr, vaddr);
		STATS_INC(priv->stats.rx_dropped);
		err_replenish_count[CPU_INDEX(cpu)]++;
		return;
	}
#endif

#if defined(CONFIG_NLM_ENABLE_LOAD_BALANCING)
	if(!priv->mgmt_port){
		nlm_update_flow_stats((unsigned int *)vaddr, len, context);
	}
	skb_reserve(skb, nlm_prepad_len);
#endif

	if(priv->index == XGMAC)
		msec_port = (priv->port | 0xf) << (4 * priv->block) ;
	else
		msec_port = 1 << port;
	nae_cfg = nlm_node_cfg.nae_cfg[node];

#ifdef MACSEC_DEBUG
	printk("%s nae_cfg->sectag_offset = %d sectag_len = %d icv_len = %d\n",
	__FUNCTION__, nae_cfg->sectag_offset[port], nae_cfg->sectag_len[port], nae_cfg->icv_len[port]);
	dump_buffer(skb->data, len, "RX skb pkt:");
	printk("msec_port = %x port = %d len = %d nae_cfg->msec_port_enable = %x\n",msec_port, port, len, nae_cfg->msec_port_enable);
#endif
	//if(1/*nae_cfg->msec_port_enable & msec_port*/)/* check if port is tx port is enabled for msec else bypass MACSec*/
	if(nae_cfg->msec_port_enable & msec_port)/* check if port is tx port is enabled for msec else bypass MACSec*/
	{
		short ether_type = *(short*)(((char*)skb->data) + MAC_HEADER_LEN);

		if((ether_type & 0xffff) == MACSEC_ETHER_TYPE)/*Enable MACSEc processing*/
		{
			memcpy((char*)(skb->data + MAC_HEADER_LEN + nae_cfg->sectag_len[port] - MAC_ADDR_LEN /*DST MAC LEN*/),(((char*)skb->data)+MAC_ADDR_LEN), MAC_ADDR_LEN);
			memcpy((char*)(skb->data + MAC_HEADER_LEN + nae_cfg->sectag_len[port] - (MAC_ADDR_LEN * 2) /*SRC MAC LEN*/),((char*)skb->data), MAC_ADDR_LEN);
			len = len - nae_cfg->sectag_len[port] - nae_cfg->icv_len[port];
			skb_reserve(skb, nae_cfg->sectag_len[port]);
#ifdef MACSEC_DEBUG
			dump_buffer(skb->data, len, "RX mod skb pkt:");
#endif
		}
	}

	skb->dev = pdev;
	skb_put(skb, len);
	skb->protocol = eth_type_trans(skb, pdev);

	/* We use jumbo rx buffers if the ethernet type is not ip, see perf_nae ucore file */
	truesize = skb->truesize;
	if(skb->protocol != htons(ETH_P_IP))
		truesize = NLM_RX_JUMBO_BUF_SIZE + sizeof(struct sk_buff);

#ifdef CONFIG_NLM_NET_OPTS
	/* Pass the packet to Network stack */
	last_rcvd_skb[CPU_INDEX(cpu)] = skb;
	last_rcvd_skb_phys[CPU_INDEX(cpu)] = addr;
	last_rcvd_len[CPU_INDEX(cpu)] = len;
	last_rcvd_node[CPU_INDEX(cpu)] = node;
#endif

#ifdef CONFIG_INET_LRO
	if((skb->dev->features & NETIF_F_LRO) &&
			(msg1 & RX_IP_CSUM_VALID) && (msg1 & RX_TCP_CSUM_VALID)) {

		skb->ip_summed = CHECKSUM_UNNECESSARY;
		lro_receive_skb(&priv->lro_mgr[cpu], skb, NULL);
		if(!lro_flush_needed[cpu][priv->port]) {
			lro_flush_priv[cpu][lro_flush_priv_cnt[cpu]] = priv;
			lro_flush_needed[cpu][priv->port] = 1;
			lro_flush_priv_cnt[cpu]++;
			Message("Adding to lro flush queue cpu %d port %d\n", cpu, priv->port);
		}
	} else
#endif
	{
		netif_receive_skb(skb);
	}

	/* Update Stats */
	STATS_ADD(priv->stats.rx_bytes, len);
	STATS_INC(priv->stats.rx_packets);
	receive_count[CPU_INDEX(cpu)]++;

#ifdef CONFIG_NLM_NET_OPTS
	if (last_rcvd_skb[CPU_INDEX(cpu)]) {
		slow_replenish_count[CPU_INDEX(cpu)]++;
		mac_refill_frin_one_buffer(pdev, cpu, truesize);
		last_rcvd_skb[CPU_INDEX(cpu)] = NULL;
		last_rcvd_len[CPU_INDEX(cpu)] = 0;
	}
#else
	slow_replenish_count[CPU_INDEX(cpu)]++;
	mac_refill_frin_one_buffer(pdev, cpu, truesize);
#endif
}

#ifdef CONFIG_INET_LRO
static inline void napi_lro_flush(int cpu)
{
	struct dev_data *priv = NULL;
	int i;
	for(i = 0; i < lro_flush_priv_cnt[cpu]; i++) {
		priv = lro_flush_priv[cpu][i];
		lro_flush_all(&priv->lro_mgr[cpu]);
		lro_flush_needed[cpu][priv->port] = 0;
		Message("Lro flush cpu %d port %d\n", cpu, priv->port);
	}
	lro_flush_priv_cnt[cpu] = 0;
}

static void xlp_napi_lro_flush(void *arg)
{
	int cpu = hard_smp_processor_id();
	napi_lro_flush(cpu);
}
#endif


/*
 * NAE poll function on freeback only if rx and freeback vcs are different
*/
static void xlp_poll_upper(int cpu)
{
	unsigned int status;
	uint64_t msg0 = 0;
	uint32_t src_id = 0, size, code;
	unsigned long __attribute__ ((unused)) mflags;

	/* In non-exlusivevc , this vc can be shared with some other moduels */
	if((nae_rx_vc == nae_fb_vc) || (!exclusive_vc))
		return;

	while (1) {
			msgrng_access_enable(mflags);
			status = xlp_message_receive_1(nae_fb_vc, &src_id, &size, &code, &msg0);
			msgrng_access_disable(mflags);

			if(status) break;
			__sync();

			Message("poll upper cpu %d src_id %d size %d\n", cpu, src_id, size);
			process_tx_complete(cpu, src_id, msg0);

	} /* closing while (1) */
}

/*
 * NAE poll function on lower four buckets
 */
static int xlp_poll_lower(int budget, int cpu)
{
	int status;
	uint64_t msg0 = 0, msg1 = 0;
	int no_rx_pkt_rcvd = 0;
	uint32_t src_id = 0, size = 0, code;
	unsigned long __attribute__ ((unused)) mflags;

	while (budget--) {
		msgrng_access_enable(mflags);
		status = xlp_message_receive_2(nae_rx_vc, &src_id, &size, &code, &msg0, &msg1);
		msgrng_access_disable(mflags);

		if(status) {
			if(enable_napi)
				break;
			continue;
		}

		no_rx_pkt_rcvd++;
#ifdef ENABLE_SANITY_CHECKS
		if((size != 2) && (size != 1)) {
			printk("Unexpected single entry packet in poll_lower\n");
			continue;
		}
#endif
		if(size == 2)
			process_rx_packets(cpu, src_id, msg0, msg1);
		else if(size == 1)
			process_tx_complete(cpu, src_id, msg0);
		else {
			printk("%s , Error invalid message, size %d\n", __FUNCTION__, size);
			continue;
		}
	}
#ifdef CONFIG_INET_LRO
	if(enable_lro)
		napi_lro_flush(cpu);
#endif
	return no_rx_pkt_rcvd;
}



/**********************************************************************
 * nlm_xlp_nae_msgring_handler -  message ring interrupt handler
 * @vc-  virtual channel number
 * @dev_id  -  this device
 *
 **********************************************************************/
static void nlm_xlp_nae_msgring_handler(uint32_t vc, uint32_t src_id,
					uint32_t size, uint32_t code,
					uint64_t msg0, uint64_t msg1,
					uint64_t msg2, uint64_t msg3, void* data)
{
	int cpu = hard_smp_processor_id();

	if( vc == nae_rx_vc && size == 2)
		 process_rx_packets(cpu, src_id, msg0, msg1);
	else if(vc == nae_fb_vc && size == 1)
		process_tx_complete(cpu, src_id, msg0);
	else {
		printk("%s , Error invalid message, vc %d size %d\n", __FUNCTION__, vc, size);
	}
}

/*
 * Main NAE napi poll loop for exclusive vc handler
 */

static int xlp_nae_napi_poll(int vc, int budget)
{
	int rx_pkts = 0;
	int cpu = hard_smp_processor_id();

	Message("%s in vc %d budget %d\n", __FUNCTION__, vc, budget);

	xlp_poll_upper(cpu);
	rx_pkts = xlp_poll_lower(budget, cpu);

	return rx_pkts;
}

/*
 * Main NAE  poll loop for kthread model
 */
static int xlp_nae_poll(void *buf)
{
	//unsigned int count=0;
	int rx_pkts = 0;
	int cpu = hard_smp_processor_id();
	int budget = 96;

	if(perf_mode == NLM_RT_MODE)
		budget = 300000;

	while (1) {

		local_bh_disable();
		xlp_poll_upper(cpu);
		rx_pkts = xlp_poll_lower(budget, cpu);
		local_bh_enable();


		schedule();
	}
	return 0;
}

void nlm_spawn_kthread(void)
{
    unsigned int i = 0, nr_cpus;
    char buf[20];
    static struct task_struct *task[NR_CPUS];

    nr_cpus = nlm_node_cfg.num_nodes * NLM_NCPUS_PER_NODE;
    /*Spawn kthread*/
    for(i=0; i<nr_cpus; i++){
	if(!cpu_isset(i, cpu_present_map))
		continue;
        sprintf(buf,"nae_task_%d",i);
        task[i] = kthread_create(xlp_nae_poll, (void *)(long)i, (void *)&buf);
        if(!task[i])
            break;
    }
    if(i == nr_cpus){
        for(i=0; i<nr_cpus; i++){
	    if(!cpu_isset(i, cpu_present_map))
		    continue;
            kthread_bind(task[i], i);
            wake_up_process(task[i]);
        }
    }

}


/*
 * Setup XLP NAPI subsystem
 */
extern int nlm_xlp_register_napi_vc_handler(int nae_rx_vc, int (*napi_msgint_handler)(int, int));
extern int nlm_xlp_register_napi_final_handler(int major, void (*napi_final)(void *arg), void *arg);

static int nlm_xlp_enable_napi(void)
{
	if(exclusive_vc) {
		printk("Registering exclusive napi vc handler\n");
		nlm_xlp_register_napi_vc_handler(nae_rx_vc, xlp_nae_napi_poll);
		nlm_xlp_register_napi_vc_handler(nae_fb_vc, xlp_nae_napi_poll);
	} else {
		printk("Registering nae msgring handler\n");
		if(register_xlp_msgring_handler(XLP_MSG_HANDLE_NAE_0 , nlm_xlp_nae_msgring_handler, NULL)) {
			printk("Fatal error! Can't register msgring handler for XLP_MSG_HANDLE_NAE_0");
			return -1;
		}
#ifdef CONFIG_INET_LRO
		if(enable_lro) {
			printk("Registering napi final handler\n");
			nlm_xlp_register_napi_final_handler(XLP_MSG_HANDLE_NAE_0, xlp_napi_lro_flush, NULL);
		}
#endif
	}

	return 0;
}

static int nlm_xlp_disable_napi(void)
{
	int node, i;
	for(i=0; i<NR_CPUS; i++){
		if(!cpu_isset(i, phys_cpu_present_map))
			continue;
		node = i / 32;
		nlm_hal_disable_vc_intr(node, (i*NLM_MAX_VC_PER_THREAD + nae_rx_vc) & 0x7f);
		nlm_hal_disable_vc_intr(node, (i*NLM_MAX_VC_PER_THREAD + nae_fb_vc) & 0x7f);
	}
	return 0;
}

/**********************************************************************
 * nlm_xlp_nae_init -  xlp_nae device driver init function
 * @dev  -  this is per device based function
 *
 **********************************************************************/

static void nlm_xlp_nae_init(void)
{
	struct net_device *dev = NULL;
	struct dev_data *priv;
	int i, node = 0, maxnae;
	struct proc_dir_entry *entry;
	int cpu = 0;
	unsigned char *mode_str[3] = {"INVALID","TCP_PERF","ROUTE_PERF"};
	nlm_nae_config_ptr nae_cfg;

	if(!(perf_mode == NLM_TCP_MODE || perf_mode == NLM_RT_MODE)){
		printk("Invalid perf mode passed -- Using TCP_PERF mode\n");
		perf_mode = NLM_TCP_MODE;
	}

	printk("======= Module Parameters =========\n");
	printk("num_descs_per_normalq=%d num_descs_per_jumboq=%d perf_mode=%s enable_lro=%d enable_jumbo=%d\n",
	       num_descs_per_normalq, num_descs_per_jumboq, mode_str[perf_mode], enable_lro, enable_jumbo);

#if 0
	/* commenting out the below code, as it should use the bkup timer */
	/* msgring intr may not work in [8421]xxAx parts, disabling the napi */
	if(is_nlm_xlp8xx_ax() && enable_napi) {
		printk("NAPI cannot be enabled on XLP Ax parts\n");
		enable_napi = 0;
	}
#endif

	for(i=0; i<NR_CPUS; i++)
		nlm_mode[CPU_INDEX(i)] = perf_mode;

	/*Disable interrupts for VC - 0-127*/
	for(i=0; i<NR_CPUS; i++){
	        if(!cpu_isset(i, phys_cpu_present_map))
                        continue;
		phys_cpu_map[i / NLM_NCPUS_PER_NODE] |= (1 << (i % NLM_NCPUS_PER_NODE));
	}

	if(perf_mode == NLM_TCP_MODE)
		p2p_desc_mem_init();

	gen_mac_address();

	if (initialize_nae(phys_cpu_map, perf_mode, &enable_jumbo))
		return;

	maxnae = nlm_node_cfg.num_nodes;
	for(node = 0; node < maxnae; node++) {
		nae_cfg = nlm_node_cfg.nae_cfg[node];
		if (nae_cfg == NULL)
			continue;

		for(i = 0; i < nae_cfg->num_ports; i++)
		{
			/* Register only valid ports which are management */
			if (!nae_cfg->ports[i].valid)
				continue;

			dev = alloc_etherdev_mq(sizeof(struct dev_data), maxnae * NLM_NCPUS_PER_NODE);
			if(!dev)
				return;

			ether_setup(dev);
			dev->tx_queue_len = 0;	/* routing gives good performance with tx_queue_len = 0; */

			priv = netdev_priv(dev);
			spin_lock_init(&priv->lock);
			priv->dev 	= dev;
			dev->netdev_ops = &nlm_xlp_nae_ops;

			/* set ethtool_ops which is inside xlp_ethtool.c file*/
			xlp_set_ethtool_ops(dev);

			dev->dev_addr 	= eth_hw_addr[node][i];
			priv->port	= i;
			priv->hw_port_id = nae_cfg->ports[i].hw_port_id;

			priv->inited	= 0;
			priv->node 	= node;
			priv->block	= nae_cfg->ports[i].hw_port_id / 4;
			priv->type	= nae_cfg->ports[i].iftype;
			priv->mgmt_port = nae_cfg->ports[i].mgmt;

			switch(nae_cfg->ports[i].iftype) {
				case SGMII_IF:
					priv->index = nae_cfg->ports[i].hw_port_id & 0x3;
					priv->phy.addr = nae_cfg->ports[i].hw_port_id;
					break;
				case XAUI_IF:
					nlm_hal_write_mac_reg(priv->node, (nae_cfg->ports[i].hw_port_id / 4),
							XGMAC, XAUI_MAX_FRAME_LEN , 0x01800600);
					priv->index = XGMAC;
					break;
				case INTERLAKEN_IF:
					priv->index = INTERLAKEN;
					priv->phy.addr = nae_cfg->ports[i].ext_phy_addr;
					if (nae_cfg->ports[i].hw_port_id == 0) {
						if (dev_alloc_name(dev, "ilk0-%d") < 0)
							printk("alloc name failed \n");
					}
					else {
						if (dev_alloc_name(dev, "ilk8-%d") < 0)
							printk("alloc name failed \n");
					}
					break;
				default:
					priv->index=0;
					break;
			}
			//nlm_print("port%d hw %d block %d index %d type %d \n",i, nae_cfg->ports[i].hw_port_id,
			//							priv->block, priv->index, priv->type);
			priv->nae_tx_qid	= nae_cfg->ports[i].txq;
			priv->nae_rx_qid 	= nae_cfg->ports[i].rxq;
			dev->features 		|= NETIF_F_LLTX;

			register_netdev(dev);

			dev_mac[node][i] = dev;
			xlp_mac_setup_hwaddr(priv);

			dummy_pktdata_addr[node] = nae_cfg->dummy_pktdata_addr;

			for(cpu = 0; cpu<NR_CPUS; cpu++){
				per_cpu_netdev[node][cpu][i] = dev;
			}
		}

	}

	entry = create_proc_read_entry("mac_stats", 0 /* def mode */ ,
				       nlm_root_proc /* parent */ ,
				       xlp_mac_proc_read /* proc read function */ ,
				       0	/* no client data */);
	if (!entry) {
		printk("[%s]: Unable to create proc read entry for xlp_mac!\n",
		       __FUNCTION__);
	}
	entry = create_proc_read_entry("nae_stat", 0, nlm_root_proc, nae_proc_read, 0);
	if (!entry) {
		printk("[%s]: Unable to create proc read entry for nae_proc!\n",
		       __FUNCTION__);
	}

	if(!enable_napi) {
		nlm_xlp_disable_napi();
		exclusive_vc = 1;
		/*spawn percpu kthread*/
		nlm_spawn_kthread();
	}

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
	if(perf_mode == NLM_TCP_MODE)
		nlm_prepad_len = PREPAD_LEN;
#endif

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
	if(perf_mode == NLM_TCP_MODE){
		entry = create_proc_entry("load_info", 0 /* def mode */ ,
				   nlm_root_proc /* parent */ );
		if(entry){
				entry->proc_fops = &nlm_load_balance_proc_fops;
		}
		nlm_init_load_balance();
	}
#endif

	if(replenish_freein_fifos() != 0) {
		printk("Replenishmemt of freein fifos failed\n");
	}
	return;
}

/**********************************************************************
 * nlm_xlp_nae_open -  called when bring up a device interface
 * @dev  -  this is per device based function
 *
 **********************************************************************/
static int  nlm_xlp_nae_open (struct net_device *dev)
{
	struct dev_data *priv = netdev_priv(dev);
	int i;
	int ret = 0;
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[priv->node];

	if(perf_mode == NLM_TCP_MODE) {
#ifdef TSO_ENABLED
		tso_enable(dev, 1);
#endif
		lro_init(dev);
	}

	if (priv->inited) {
		spin_lock_irq(&priv->lock);
		if(nae_cfg->owned)
			nlm_xlp_mac_set_enable(priv, 1);
		netif_tx_wake_all_queues(dev);
		spin_unlock_irq(&priv->lock);
		return 0;
	}

#ifdef ENABLE_NAE_PIC_INT
	{
		int port = priv->port;
		irq  = irt_irq_table[PIC_IRT_NA_INDEX(port)][0];
		if(request_irq( irq, nlm_xlp_nae_int_handler, IRQF_SHARED,dev->name, dev)){
			ret = -EBUSY;
			printk("can't get mac interrupt line (%d)\n",dev->irq);
		}
		dump_irt_entry(PIC_IRT_NA_INDEX(port));
	}
#endif

	netif_tx_start_all_queues(dev);

	STATS_SET(priv->stats.tx_packets, 0);
	STATS_SET(priv->stats.tx_errors, 0);
	STATS_SET(priv->stats.tx_bytes, 0);
	STATS_SET(priv->stats.tx_dropped, 0);
	STATS_SET(priv->stats.rx_packets, 0);
	STATS_SET(priv->stats.rx_errors, 0);
	STATS_SET(priv->stats.rx_bytes, 0);
	STATS_SET(priv->stats.rx_dropped, 0);
	STATS_SET(priv->stats.multicast, 0);
	STATS_SET(priv->stats.collisions, 0);

	for(i = 0; i < NR_CPUS; i++)
	{
		priv->cpu_stats[i].tx_packets	= 0;
		priv->cpu_stats[i].txc_packets	= 0;
		priv->cpu_stats[i].rx_packets	= 0;
		priv->cpu_stats[i].interrupts	= 0;

	}

	priv->inited = 1;

	if(nae_cfg->owned)
		{
#ifdef CONFIG_NLM_XLP_CAMARO_BOARD
		bcm_power_phy((priv->block * 4) + priv->index, 1, priv->node);
#endif
		nlm_xlp_mac_set_enable(priv, 1);
		}

	return ret;
}

/**********************************************************************
 * nlm_xlp_nae_stop -  called when bring down the interface
 * @dev  -  this is per device based function
 *
 **********************************************************************/
static int  nlm_xlp_nae_stop (struct net_device *dev)
{
	struct dev_data *priv = netdev_priv(dev);
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[priv->node];

	spin_lock_irq(&priv->lock);

	if(nae_cfg->owned)
	{
#ifdef CONFIG_NLM_XLP_CAMARO_BOARD
		bcm_power_phy((priv->block * 4) + priv->index, 0, priv->node);
#endif
		nlm_xlp_mac_set_enable(priv, 0);
	}
	priv->inited = 0;
	netif_tx_stop_all_queues(dev);

	spin_unlock_irq(&priv->lock);
	return 0;
}

/*This macro resets first 164 (offsetof(struct sk_buff, tail))bytes of skb header.*/
#define fast_reset_skbptrs(skb) \
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 0) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 1) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 2) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 3) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 4) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 5) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 6) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 7) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 8) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 9) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 10) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 11) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 12) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 13) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 14) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 15) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 16) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 17) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 18) = 0;\
		*(uint64_t *)(unsigned long)((uint64_t *)skb + 19) = 0;\
		*(uint32_t *)(unsigned long)((uint64_t *)skb + 20) = 0;\
/*
 * This helper macro resets SKB data pointers for reuse
 * as free-in buffer
*/
#define skb_reset_ptrs(skb) \
do { \
	struct skb_shared_info *shinfo; \
	\
	shinfo = skb_shinfo(skb); \
	\
	\
	/* Now reinitialize old skb, cut & paste from dev_alloc_skb */ \
	/*memset(skb, 0, offsetof(struct sk_buff, tail));*/ \
	fast_reset_skbptrs(skb);\
	skb->data = skb->head;  \
	skb_reset_tail_pointer(skb);\
	\
	atomic_set(&shinfo->dataref, 1); \
	shinfo->nr_frags  = 0; \
	shinfo->gso_size = 0; \
	shinfo->gso_segs = 0; \
	shinfo->gso_type = 0; \
	shinfo->ip6_frag_id = 0; \
	shinfo->frag_list = NULL; \
} while (0)


static inline int tso_xmit_skb(struct sk_buff *skb, struct net_device *dev)
{
	int mss  = 0, idx = 0, len, i ;
	struct skb_shared_info *sp = skb_shinfo(skb);
	struct iphdr *iph;
	struct dev_data *priv = netdev_priv(dev);
	uint64_t msg, mscmsg0, mscmsg1;
	uint64_t *p2pdesc = NULL;
	int cpu = hard_smp_processor_id();
	int  ret, retry_cnt = 0, qid;
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[priv->node];
	unsigned long __attribute__ ((unused)) mflags;
	uint32_t msec_port, send_msec = 0, msec_bypass = 0, pad_len, icv_len, param_index = 0;

#ifdef MACSEC_DEBUG
	printk("nae_cfg->sectag_offset = %d sectag_len = %d icv_len = %d\n",
	nae_cfg->sectag_offset[priv->port], nae_cfg->sectag_len[priv->port], nae_cfg->icv_len[priv->port]);
#endif
	if(priv->index == XGMAC)
		msec_port = (priv->port | 0xf) << (4 * priv->block) ;
	else
		msec_port = 1 << priv->port;

#ifdef MACSEC_DEBUG
	dump_buffer(skb->data, skb->len, "Org skb pkt:");
	printk("msec_port = %x priv->port = %d priv->block = %d priv->index = %d skb->len = %d nae_cfg->msec_port_enable = %x\n",
	msec_port, priv->port, priv->block, priv->index, skb->len, nae_cfg->msec_port_enable);
#endif
	//if(1/*nae_cfg->msec_port_enable & msec_port*/)/* check if port is tx port is enabled for msec else bypass MACSec*/
	if(nae_cfg->msec_port_enable & msec_port)/* check if port is tx port is enabled for msec else bypass MACSec*/
	{
		short ether_type = *(short*)(((char*)skb->data) + MAC_HEADER_LEN);

#ifdef MACSEC_DEBUG
	printk("skb->len = %d ether_type = %x\n", skb->len, ether_type);
#endif
		//if(1/*(ether_type & 0xffff) == PROTOCOL_TYPE_IP*/)/*Enable MACSEc processing*/
		if((ether_type & 0xffff) == PROTOCOL_TYPE_IP)/*Enable MACSEc processing*/
		{
			send_msec = 1;
			param_index = (priv->port)?priv->port:1;/* it should be between 1 -7 */

			pad_len =  nae_cfg->sectag_offset[priv->port] + nae_cfg->sectag_len[priv->port]; //12 + 8;
			icv_len = nae_cfg->icv_len[priv->port];

#ifdef MACSEC_DEBUG
	printk("pad_len = %d icv_len = %d ether_type = %x\n", pad_len, icv_len, ether_type);
#endif
		}
		else
			msec_bypass = 1;

	}

	p2pdesc = alloc_p2p_desc_mem(cpu);
	if(p2pdesc == NULL) {
		goto out_unlock;
	}
	tso_dbg("%s in gso_size %d nrfrags %d len %d p2pdesc %llx skb %llx headlen %d\n", __FUNCTION__,
			sp->gso_size, sp->nr_frags, skb->len, (uint64_t)p2pdesc, (uint64_t)skb, skb_headlen(skb));


	if (((mss = sp->gso_size) != 0) || (skb->ip_summed == CHECKSUM_PARTIAL)) {
		u32 iphdroff, tcphdroff, pyldoff, pcsum, tcp_packet = 1;

		if (skb_header_cloned(skb) &&
				pskb_expand_head(skb, 0, 0, GFP_ATOMIC)) {
			goto out_unlock;
		}

		iph = ip_hdr(skb);
		iphdroff = (char *)iph - (char *)skb->data;
		tcphdroff = iphdroff + ip_hdrlen(skb);
		if(ip_hdr(skb)->protocol == 0x6) {
			pyldoff = iphdroff + ip_hdrlen(skb) + sizeof(struct tcphdr) + tcp_optlen(skb);
			pcsum = pseuodo_chksum((uint16_t *)((char *)iph + 12), 0x6);
			tcp_hdr(skb)->check = 0;
		} else if(ip_hdr(skb)->protocol == 0x11) {
			pyldoff = iphdroff + ip_hdrlen(skb) + sizeof(struct udphdr);
			pcsum = pseuodo_chksum((uint16_t *)((char *)iph + 12), 0x11);
			udp_hdr(skb)->check = 0;
			tcp_packet = 0;
		} else {
			printk("Invalid packet in %s\n", __FUNCTION__);
			goto out_unlock;
		}

		tso_dbg("iphdroff %d tcphdroff %d pyldoff %d\n", iphdroff, tcphdroff, pyldoff);
		if(mss) {
			iph->check = 0;
			iph->tot_len = 0;
			mscmsg0 = nae_tso_desc0(MSC, 1, TSO_IP_TCP_CHKSUM, param_index,
				iphdroff, tcphdroff, (iphdroff + 10),
				pcsum, tcphdroff + 16, pyldoff);
			mscmsg1 = nae_tso_desc1(MSC, 2, 0, mss, 0, 0);
		} else if(tcp_packet) {
			mscmsg0 = nae_tso_desc0(MSC, 0, TCP_CHKSUM, param_index,
				iphdroff, tcphdroff, (iphdroff + 10),
				pcsum, tcphdroff + 16, pyldoff);
		} else {
			mscmsg0 = nae_tso_desc0(MSC, 0, UDP_CHKSUM, param_index,
				iphdroff, tcphdroff, (iphdroff + 10),
				pcsum, tcphdroff + 6, pyldoff);
		}

	}
	else if(send_msec || msec_bypass)
	{
		mscmsg0 = nae_tso_desc0(MSC, 0, 0, param_index,
	              	  0, 0, 0, 0, 0, 0);
	}

	if(((len = skb_headlen(skb)) != 0)) {
		if(send_msec)
		{
			memcpy((char*)&p2pdesc[P2P_SKB_OFF-8], skb->data, MAC_HEADER_LEN);
			idx = create_p2p_desc(virt_to_bus((char *)&p2pdesc[P2P_SKB_OFF-8]), pad_len, p2pdesc, idx);

			idx = create_p2p_desc(virt_to_bus((((char *)skb->data)+ MAC_HEADER_LEN)), (len - MAC_HEADER_LEN), p2pdesc, idx);
#ifdef MACSEC_DEBUG
			dump_buffer((char *)&p2pdesc[P2P_SKB_OFF-8], pad_len, "first_seg:");
			printk(" len = %d pad_len = %d icv_len = %d param_index = %d\n", len, pad_len, icv_len, param_index);
#endif
		}
		else{
			idx = create_p2p_desc(virt_to_bus((char *)skb->data), len, p2pdesc, idx);
		}
	}

	for (i = 0; i < sp->nr_frags; i++)  {
		skb_frag_t *fp = &sp->frags[i];
		tso_dbg("frags %d pageaddr %lx off %x size %d\n", i, (long)page_address(fp->page),
				fp->page_offset, fp->size);
		idx = create_p2p_desc(virt_to_bus(((char *)page_address(fp->page)) + fp->page_offset),
				fp->size, p2pdesc, idx);
	}

	if(send_msec)
	{
		if(!param_index)
			idx = create_p2p_desc(virt_to_bus((char *)&p2pdesc[P2P_SKB_OFF-2]), icv_len, p2pdesc, idx);
	}

	qid = nae_cfg->vfbtbl_sw_offset + (cpu % NLM_NCPUS_PER_NODE);
	{
		create_last_p2p_desc(p2pdesc, skb, idx);
		msg = nae_tx_desc(P2P, 0, qid, idx, virt_to_bus(p2pdesc));
	}


	tso_dbg("msg0 %llx p2pdesc0 %llx p2pdesc1 %llx p2pdesc2 %llx idx %d\n",
			msg, p2pdesc[0], p2pdesc[1], p2pdesc[2], idx);

	__sync();
retry_send:
	msgrng_access_enable(mflags);
	if(mss)
		ret = nlm_hal_send_msg3(priv->nae_tx_qid, 0, mscmsg0, mscmsg1, msg);
	else if(skb->ip_summed == CHECKSUM_PARTIAL)
		ret = nlm_hal_send_msg2(priv->nae_tx_qid, 0, mscmsg0, msg);
	else if(send_msec || msec_bypass)
	{
		ret = nlm_hal_send_msg2(priv->nae_tx_qid, 0, mscmsg0, msg);
	}
	else
	{
		ret = nlm_hal_send_msg1(priv->nae_tx_qid, 0, msg);
	}
	msgrng_access_disable(mflags);
	if(ret)	{
		xlp_poll_upper(cpu);
		retry_cnt++;
		if(retry_cnt >= 128) {
			goto out_unlock;
		}
		goto retry_send;
	}

	dev->trans_start = jiffies;
	STATS_ADD(priv->stats.tx_bytes, skb->len);
	STATS_ADD(priv->stats.tx_packets, idx);
	priv->cpu_stats[cpu].tx_packets += idx;

	return NETDEV_TX_OK;
out_unlock:

	dev_kfree_skb_any(skb);
	if(p2pdesc)
		free_p2p_desc_mem(cpu, p2pdesc);
	return NETDEV_TX_OK;
}


/**********************************************************************
 * nlm_xlp_nae_start_xmit -  transmit a packet from buffer
 * @dev  -  this is per device based function
 * @skb  -  data buffer to send
 **********************************************************************/
static int nlm_xlp_nae_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct dev_data *priv = netdev_priv(dev);
	int cpu = hard_smp_processor_id(), ret = 0;
	uint64_t msg0, msg1;
	int retry_count = 128;
	volatile int hw_repl = 0;
	int  offset, qid;
	unsigned long __attribute__ ((unused)) mflags;
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[priv->node];
#if 0
	uint32_t msec_port, send_msec = 0, pad_len, icv_len;


printk("nae_cfg->sectag_offset = %d sectag_len = %d icv_len = %d\n",nae_cfg->sectag_offset[priv->port], nae_cfg->sectag_len[priv->port], nae_cfg->icv_len[priv->port]);
printk("size of skb end = %llx head = %llx\n", (unsigned long)skb->end, (unsigned long)skb->head);
	if(priv->index == XGMAC)
		msec_port = (priv->port | 0xf) << (4 * priv->block) ;
	else
		msec_port = 1 << priv->port;

dump_buffer(skb->data, skb->len, "Org skb pkt:");
printk("msec_port = %x priv->port = %d priv->block = %d priv->index = %d skb->len = %d nae_cfg->msec_port_enable = %x\n",msec_port, priv->port, priv->block, priv->index, skb->len, nae_cfg->msec_port_enable);
	if(1/*nae_cfg->msec_port_enable & msec_port*/)/* check if port is tx port is enabled for msec else bypass MACSec*/
	{
		short ether_type = *(short*)(((char*)skb->data) + MAC_HEADER_LEN);

printk("skb->len = %d ether_type = %x\n", skb->len, ether_type);
		if((ether_type & 0xffff) == PROTOCOL_TYPE_IP)/*Enable MACSEc processing*/
		{
			send_msec = 1;
		/*	msec_pad_mem = (unsigned char*)cacheline_aligned_kmalloc(MAC_SEC_PADDING, GFP_KERNEL);
			msec_icv_mem = (unsigned char*)cacheline_aligned_kmalloc(nae_cfg->sectag_len[priv->port], GFP_KERNEL);
			*/

			pad_len = 12 + 8;
			icv_len = nae_cfg->icv_len[priv->port];

			memcpy(msec_pad_mem, skb->data, MAC_HEADER_LEN);
			//skb_reserve(skb, MAC_HEADER_LEN);
			//skb_push(skb, nae_cfg->sectag_offset[priv->port]);
printk("skb->len = %d ether_type = %x\n", skb->len, ether_type);
dump_buffer(msec_pad_mem, pad_len, "first_seg:");
		}
//		skb->len +=  nae_cfg->icv_len[priv->port];

	}
#endif

#ifdef ENABLE_SANITY_CHECKS
	if(!skb)
	{
		printk("[%s] skb is NULL\n",__FUNCTION__);
		return -1;
	}
	if(skb->len == 0)
	{
		printk("[%s] skb empty packet\n",__FUNCTION__);
		return -1;
	}
#endif
#ifdef TSO_ENABLED
	if(nlm_mode[CPU_INDEX(cpu)] == NLM_TCP_MODE){
		return tso_xmit_skb(skb, dev);
	}
#endif

#ifdef CONFIG_NLM_NET_OPTS
	if(skb->netl_skb && (last_rcvd_skb[CPU_INDEX(cpu)] == skb->netl_skb)
		&& !skb_shared(skb) && (last_rcvd_len[CPU_INDEX(cpu)] == skb->len)
		&& !skb_cloned(skb) && nae_cfg->vfbtbl_hw_nentries)
	{
		last_rcvd_skb[CPU_INDEX(cpu)] = NULL;
		last_rcvd_len[CPU_INDEX(cpu)] = 0;

		qid = get_hw_frfifo_queue_id(last_rcvd_node[CPU_INDEX(cpu)], nae_cfg, cpu, skb->truesize);
		msg0 = nae_tx_desc(P2D_NEOP, 0, qid,
				0, last_rcvd_skb_phys[CPU_INDEX(cpu)]);
		hw_repl = 1;

		Message("Tx, tx complete to nae, cpu %d len %d qid %d\n", cpu, skb->len, qid);

		fast_replenish_count[CPU_INDEX(cpu)]++;
	}
	else
#endif
	{
		qid = nae_cfg->vfbtbl_sw_offset + (cpu % NLM_NCPUS_PER_NODE);
		msg0 = nae_tx_desc(P2D_NEOP, 0, qid, 0, virt_to_bus(skb));

		Message("Tx, tx complete to cpu, cpu %d len %d qid %d\n", cpu, skb->len, qid);
	}

	{
		msg1 = nae_tx_desc(P2D_EOP, 0, NULL_VFBID, skb->len,
		       virt_to_bus(skb->data));
	}
	if(hw_repl) {
		/* reset the skb for next rx */

		//dst_release((struct dst_entry *)skb->_skb_dst);
		skb_dst_drop(skb);

		/* Reset all fields to 0, reset data pointers */
		skb_reset_ptrs(skb);

		offset = (((unsigned long)skb->data + CACHELINE_SIZE) & ~(CACHELINE_SIZE - 1));
		skb_reserve(skb, (offset - (unsigned long)skb->data));

		/*this buffer already has backptr...
		mac_put_skb_back_ptr(skb); */
		skb_reserve(skb, SKB_BACK_PTR_SIZE);
	}


retry_send:
	msgrng_access_enable(mflags);
	ret = nlm_hal_send_msg2(priv->nae_tx_qid, 0, msg0, msg1);
	msgrng_access_disable(mflags);
	if (ret)
	{
		xlp_poll_upper(cpu);
		retry_count--;
		if(retry_count){
			goto retry_send;
		}
		dev_kfree_skb_any(skb);
        }

	dev->trans_start = jiffies;
	STATS_ADD(priv->stats.tx_bytes, skb->len);
	STATS_INC(priv->stats.tx_packets);

	return NETDEV_TX_OK;
}

/**********************************************************************
 * nlm_xlp_set_multicast_list
 *
 **********************************************************************/
static void  nlm_xlp_set_multicast_list (struct net_device *dev)
{
	if (dev->flags & IFF_ALLMULTI) {
		/*
		 * Enable ALL multicasts.  Do this by inverting the
		 * multicast enable bit.
		 */
		return;
	}
	return;
}

static void xlp_mac_setup_hwaddr(struct dev_data *priv)
{
        struct net_device *dev = priv->dev;

        nlm_hal_write_mac_reg(priv->node, priv->block, priv->index, MAC_ADDR0_LO, (dev->dev_addr[5] << 24) |
				(dev->dev_addr[4] << 16) | (dev->dev_addr[3] << 8) | (dev->dev_addr[2]));

	nlm_hal_write_mac_reg(priv->node, priv->block, priv->index, MAC_ADDR0_HI, (dev->dev_addr[1] << 24) |
				(dev->dev_addr[0] << 16));

	nlm_hal_write_mac_reg(priv->node, priv->block, priv->index, MAC_ADDR0_MASK_LO, 0xFFFFFFFF);
	nlm_hal_write_mac_reg(priv->node, priv->block, priv->index, MAC_ADDR0_MASK_HI, 0xFFFFFFFF);

        nlm_hal_write_mac_reg(priv->node, priv->block, priv->index, MAC_FILTER_CONFIG, (1 << MAC_FILTER_BCAST_EN_POS) |
						 (1 << MAC_FILTER_MCAST_EN_POS) | (1 << MAC_FILTER_ADDR0_VALID_POS) );

}


/**********************************************************************
 * nlm_xlp_nae_ioctl
 *
 **********************************************************************/
static int  nlm_xlp_nae_ioctl (struct net_device *dev, struct ifreq *rq, int cmd)
{
	int rc = 0;
	printk("nlm_xlp_nae_ioctl called \n");
	switch (cmd) {
	case SIOCSHWTSTAMP:
		printk("HW time stamping supported by HW\n");
		return 0;
	default:
		rc = -EOPNOTSUPP;
		break;
	}

	return rc;
}

/**********************************************************************
 * nlm_xlp_nae_change_mtu
 * @dev   -  this is per device based function
 * @new_mtu -  this is new mtu to be set for the device
 **********************************************************************/
static int nlm_xlp_nae_change_mtu(struct net_device *dev, int new_mtu)
{
	struct dev_data *priv = netdev_priv(dev);
	unsigned long flags;
	unsigned long local_mtu;
	nlm_nae_config_ptr nae_cfg = nlm_node_cfg.nae_cfg[priv->node];

	if (enable_jumbo && (new_mtu > ETH_JUMBO_DATA_LEN || new_mtu < ETH_ZLEN)) {
		printk ("MTU should be between %d and %d\n", ETH_ZLEN, ETH_JUMBO_DATA_LEN);
		return -EINVAL;
	}

	if (!enable_jumbo && (new_mtu > ETH_DATA_LEN || new_mtu < ETH_ZLEN)) {
		printk ("MTU should be between %d and %d\n", ETH_ZLEN, ETH_DATA_LEN);
		return -EINVAL;
	}

	spin_lock_irqsave(&priv->lock, flags);

	local_mtu = (new_mtu+ETH_HLEN+ETH_FCS_LEN+SMP_CACHE_BYTES) & ~(SMP_CACHE_BYTES - 1);
	if (netif_running(dev))
	{
		netif_tx_stop_all_queues (dev);
		if(nae_cfg->owned)
			nlm_xlp_mac_set_enable(priv, 0); /* Disable MAC TX/RX */
	}

	if(priv->type==SGMII_IF){
		nlm_hal_set_sgmii_framesize(priv->node, priv->block, priv->index, local_mtu);
	}
	else if(priv->type==XAUI_IF){
		nlm_hal_set_xaui_framesize(priv->node, priv->block, local_mtu, local_mtu);
	}
	else if(priv->type==INTERLAKEN_IF){
		nlm_hal_set_ilk_framesize(priv->node, priv->block, priv->phy.addr, local_mtu);
	}
	else {
		spin_unlock_irqrestore(&priv->lock, flags);
		return -1;
	}

	dev->mtu = new_mtu;

	if (netif_running(dev))
	{
		netif_tx_start_all_queues (dev);
		if(nae_cfg->owned)
			nlm_xlp_mac_set_enable(priv, 1);
	}

	spin_unlock_irqrestore(&priv->lock, flags);
	return 0;
}

/**********************************************************************
 * nlm_xlp_mac_get_stats - wrap function for xlp_get_mac_stats
 * @dev   -  this is per device based function
 **********************************************************************/
static struct net_device_stats *nlm_xlp_mac_get_stats(struct net_device *dev)
{
	struct dev_data *priv = netdev_priv(dev);
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);

	xlp_get_mac_stats(dev, &priv->stats);

	/* XXX update other stats here */
	spin_unlock_irqrestore(&priv->lock, flags);

	return &priv->stats;
}

/**********************************************************************
 * nlm_xlp_nae_tx_timeout -  called when transmiter timeout
 * @dev  -  this is per device based function
 *
 **********************************************************************/
static void  nlm_xlp_nae_tx_timeout (struct net_device *dev)
{
	struct dev_data *priv = netdev_priv(dev);

	spin_lock_irq(&priv->lock);

	priv->stats.tx_errors++;

	spin_unlock_irq(&priv->lock);

	netif_tx_wake_all_queues(dev);

	printk(KERN_WARNING "%s: Transmit timed out\n", dev->name);
	return;
}

static int nlm_xlp_nae_set_hwaddr(struct net_device *dev, void *p)
{
	struct sockaddr *addr = (struct sockaddr *)p;
	struct dev_data *priv = netdev_priv(dev);
	int rc = 0;

	rc = eth_mac_addr(dev, p);

	if (rc)
		return rc;

	if (priv->type == SGMII_IF)
	{
	  nlm_hal_write_mac_reg(priv->node,priv->block, priv->index, MAC_ADDR0_LO,
				(addr->sa_data[5] << 24) |
				(addr->sa_data[4] << 16) |
				(addr->sa_data[3] << 8) |
				(addr->sa_data[2]));

	  nlm_hal_write_mac_reg(priv->node,priv->block, priv->index, MAC_ADDR0_HI,
				(addr->sa_data[1] << 24) |
				(addr->sa_data[0] << 16));
	}

	return rc;
}

#ifdef ENABLE_NAE_PIC_INT
/**********************************************************************
 * nlm_xlp_nae_int_handler -  interrupt handler
 * @irq     -  irq number
 * @dev_id  -  this device
 *
 **********************************************************************/
static irqreturn_t nlm_xlp_nae_int_handler(int irq, void *dev_id)
{
        struct net_device *dev;
        struct dev_data *priv;
	int i;
	int cpu = 0;

	cpu = hard_smp_processor_id();
	priv->cpu_stats[cpu].interrupts++;

	if(!dev_id)
	{
		printk("[%s]: NULL dev_id \n", __FUNCTION__ );
		return IRQ_HANDLED;
	}
	dev = (struct net_device*)dev_id;
	priv = netdev_priv(dev);

	i = find_irt_from_irq(irq);


	return IRQ_HANDLED;
}
#endif

/**********************************************************************
 * xlp_mac_proc_read -  proc file system read routine
 * @page     -  buffer address
 * @dev_id  -  this device
 *
 **********************************************************************/
static int xlp_mac_proc_read(char *page, char **start, off_t off,
			     int count, int *eof, void *data)
{
	int len = 0;
	off_t begin = 0;
	int i = 0, cpu = 0, node;
	struct net_device *dev = 0;
	struct dev_data *priv = 0;

	for(node = 0; node < NLM_MAX_NODES; node++) {
		for (i = 0; i < MAX_GMAC_PORT; i++) {

			dev = dev_mac[node][i];

			if(dev == 0) continue;

			priv = netdev_priv(dev);

			len += sprintf(page + len, "=============== port@%d ==================\n", i);

			len += sprintf(page + len,
					"per port@%d: %lu(rxp) %lu(rxb) %lu(txp) %lu(txb)\n",
					i,
					STATS_READ(priv->stats.rx_packets),
					STATS_READ(priv->stats.rx_bytes),
					STATS_READ(priv->stats.tx_packets),
					STATS_READ(priv->stats.tx_bytes));

			for (cpu = 0; cpu < NR_CPUS ; cpu++) {
				unsigned long tx = priv->cpu_stats[cpu].tx_packets;
				unsigned long txc = priv->cpu_stats[cpu].txc_packets;
				unsigned long rx = priv->cpu_stats[cpu].rx_packets;
				unsigned long ints = priv->cpu_stats[cpu].interrupts;

				if (!tx && !txc && !rx && !ints) continue;

				len += sprintf(page + len, "per cpu@%d: %lu(txp) %lu(txcp) %lu(rxp) %lu(int)\n",
						cpu, tx, txc, rx, ints);
			}
		}
	}

	*eof = 1;

	*start = page + (off - begin);
	len -= (off - begin);
	if (len > count)
		len = count;
	if (len < 0)
		len = 0;

	return len;
}



static int __devinit nlm_xlp_nae_pci_probe(struct pci_dev *pdev, const struct pci_device_id *ent)
{
        int result = 0;

        result = pci_enable_device(pdev);
	return result;
}

/**********************************************************************
 * nlm_xlp_nae_remove - driver remove routine
 * @pdev - pci device.
 **********************************************************************/
static void nlm_xlp_nae_remove(void)
{
	int i;
	struct net_device *dev = 0;
        struct dev_data *priv = 0;
	int node = 0;

	for(node = 0; node < NLM_MAX_NODES; node++) {
		for (i = 0; i < MAX_GMAC_PORT; i++)
		{
			dev = dev_mac[node][i];

			if (dev == 0) continue;

			priv = netdev_priv(dev);
			unregister_netdev(dev);
			free_netdev(dev);
		}
	}

	remove_proc_entry("mac_stats", nlm_root_proc /* parent dir*/ );

}

struct timer_list phy_int_timer;
static int last_phy_status[MAX_GMAC_PORT] = { -1 };

static void phy_st_timer_handler(unsigned long data)
{
        struct timer_list *timer = &phy_int_timer;
        struct net_device *dev;
        struct dev_data *priv;
	static struct nlm_hal_mii_info mii_info;
        int i;

	for(i=0; i<MAX_GMAC_PORT; i++){
		dev = dev_mac[0][i];
		if(dev){
			priv = netdev_priv(dev);
			if((priv->index != XGMAC) && (priv->index != INTERLAKEN) ){
                                 nlm_hal_restart_an(priv->node, i, &mii_info);
				if(mii_info.link_stat != last_phy_status[i]) {
					if ( mii_info.link_stat ) {
						netif_carrier_on(dev_mac[0][i]);
						nlm_xlp_mac_set_enable(priv, 1);
					} else {
						nlm_xlp_mac_set_enable(priv, 0);
						netif_carrier_off(dev_mac[0][i]);
					}
				}
				last_phy_status[i] = mii_info.link_stat;
			}
		}
	}

	timer->expires = jiffies + (HZ * 2);
	add_timer(timer);
}
void init_phy_state_timer(void *data)
{
        struct timer_list *timer = &phy_int_timer;

        init_timer(timer);
        timer->expires = jiffies + 10;
        timer->data = 0;
        timer->function = phy_st_timer_handler;
        add_timer(timer);
}

static struct pci_driver soc_driver = {
	.name             = XLP_SOC_MAC_DRIVER,
	.id_table         = soc_pci_table,
	.probe            = nlm_xlp_nae_pci_probe,
	.remove		  = NULL,
};

static int __init nlm_xlp_mac_init(void)
{

	nlm_xlp_nae_init();
	init_phy_state_timer(NULL);

	if(enable_napi)
		nlm_xlp_enable_napi();

	return pci_register_driver(&soc_driver);
}

static void __exit nlm_xlp_mac_exit(void)
{
	/* unregister mac driver */


	nlm_xlp_nae_remove();

	pci_unregister_driver(&soc_driver);
}

module_init(nlm_xlp_mac_init);
module_exit(nlm_xlp_mac_exit);

MODULE_AUTHOR("Netlogic Microsystems");
MODULE_DESCRIPTION("Netlogic XLP SoC Network driver ");
MODULE_LICENSE("Proprietary");
MODULE_VERSION("0.1");
