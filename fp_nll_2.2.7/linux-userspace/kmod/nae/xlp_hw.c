/*********************************************************************

  Copyright 2003-2010 Netlogic Microsystem, Inc. ("Netlogic"). All rights
  reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in
  the documentation and/or other materials provided with the
  distribution.

  THIS SOFTWARE IS PROVIDED BY Netlogic Microsystems, Inc. ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RMI OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.

  *****************************#NLM_2#**********************************/

#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/mii.h>
#include <linux/inet_lro.h>
#include <linux/net_tstamp.h>
#include <linux/clocksource.h>
#include <linux/timecompare.h>

#include <asm/netlogic/xlr_mac.h>
#include <asm/netlogic/hal/nlm_hal_nae.h>
#include <asm/netlogic/hal/nlm_eeprom.h>
#include "xlp_nae.h"

#define FACTOR             3
#define NLM_NUM_REG_DUMP 9 /* Register 0xa0 to 0xa8 */
#define NLM_EEPROM_DUMP    16
#define NLM_ETHTOOL_REG_LEN (NLM_NUM_REG_DUMP * 4)
#define PHY_STATUS_RETRIES 25000
#define NLM_EEPROM_LEN (NLM_EEPROM_DUMP * FACTOR)

#define DRV_NAME	"xlp_nae"
#define DRV_VERSION     "0.1"

static u32 xlp_get_link(struct net_device *dev);
static void nlm_xlp_mac_mii_write(struct dev_data *priv, int regidx, uint16_t regval);
static unsigned int nlm_xlp_mac_mii_read(struct dev_data *priv, int regidx);
void nlm_xlp_mac_set_enable(struct dev_data *priv, int flag);
static int xlp_enable_autoneg(struct net_device *dev, u32 adv);
static int xlp_set_link_speed(struct net_device *dev, int speed, int duplex);
extern struct eeprom_data *get_nlm_eeprom(void);
static int xlp_get_settings(struct net_device *dev, struct ethtool_cmd *cmd)
{
	struct dev_data *priv = netdev_priv(dev);
	struct nlm_hal_mii_info mii_info;
	if (priv->type != SGMII_IF) {
		cmd->supported = SUPPORTED_FIBRE|SUPPORTED_10000baseT_Full;
		cmd->advertising = SUPPORTED_FIBRE|SUPPORTED_10000baseT_Full;
		cmd->speed = SPEED_10000;
		cmd->port = PORT_FIBRE;
		cmd->duplex = DUPLEX_FULL;
		cmd->phy_address = priv->port;
		cmd->autoneg = AUTONEG_DISABLE;
		cmd->maxtxpkt = 0;
		cmd->maxrxpkt = 0;

	}else{

		cmd->supported = SUPPORTED_10baseT_Full |
			SUPPORTED_10baseT_Half |
			SUPPORTED_100baseT_Full | SUPPORTED_100baseT_Half |
			SUPPORTED_1000baseT_Full | SUPPORTED_MII |
			SUPPORTED_Autoneg | SUPPORTED_TP;

		cmd->advertising = priv->advertising;

		nlm_hal_status_ext_phy( priv->node, priv->phy.addr, &mii_info);
	       	priv->speed = mii_info.speed;
		cmd->duplex = mii_info.duplex;
		cmd->speed = (priv->speed == xlp_mac_speed_1000) ? SPEED_1000 :
		(priv->speed == xlp_mac_speed_100) ? SPEED_100: SPEED_10;


		cmd->port = PORT_TP;
		cmd->phy_address = mii_info.phyaddr;
		cmd->transceiver = XCVR_INTERNAL;
		cmd->autoneg = 1; /*Autoneg is always enabled by default*/
		cmd->maxtxpkt = 0;
		cmd->maxrxpkt = 0;
	}

	return 0;
}
static int xlp_enable_autoneg(struct net_device *dev, u32 adv)
{
	struct dev_data *priv = netdev_priv(dev);
	int mii_status;
	u32 adv1, adv2;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);
	nlm_xlp_mac_set_enable(priv, 0);
	/* advertising for 10/100 Mbps */
	adv1 = nlm_xlp_mac_mii_read(priv, MII_ADVERTISE);
	adv1 &= ~(ADVERTISE_ALL | ADVERTISE_100BASE4);
	/* advertising for 1000 Mbps */
	adv2 = nlm_xlp_mac_mii_read(priv, 0x9);
	adv2 &= ~(0x300);

	if(adv & ADVERTISED_10baseT_Half)
		adv1 |= ADVERTISE_10HALF;
	if(adv & ADVERTISED_10baseT_Full)
		adv1 |= ADVERTISE_10FULL;
	if(adv & ADVERTISED_100baseT_Full)
		adv1 |= ADVERTISE_100FULL;
	if(adv & ADVERTISED_100baseT_Half)
		adv1 |= ADVERTISE_100HALF;

	if(adv & ADVERTISED_1000baseT_Full)
		adv2 |= 0x200;
	if(adv & ADVERTISED_1000baseT_Half)
		adv2 |= 0x100;

	/* Set the advertising parameters */
	nlm_xlp_mac_mii_write(priv, MII_ADVERTISE, adv1);
	nlm_xlp_mac_mii_write(priv, 0x9, adv2);

	priv->advertising = adv1 | adv2;

	mii_status = nlm_xlp_mac_mii_read(priv, MII_BMCR);
	/* enable autoneg and force restart autoneg */
	mii_status |= (BMCR_ANENABLE | BMCR_ANRESTART);
	nlm_xlp_mac_mii_write(priv, MII_BMCR, mii_status);

	nlm_xlp_mac_set_enable(priv, 1);
	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;
}

static int xlp_set_link_speed(struct net_device *dev, int speed, int duplex)
{
	u32 adv;
	int ret =0;

	switch(speed) {
		case SPEED_10:
			if ( duplex == DUPLEX_FULL )
				adv = ADVERTISED_10baseT_Full;
			else
				adv = ADVERTISED_10baseT_Half;
			break;
		case SPEED_100:
			if ( duplex == DUPLEX_FULL )
				adv = ADVERTISED_100baseT_Full;
			else
				adv = ADVERTISED_100baseT_Half;
			break;
		case SPEED_1000:
			if ( duplex == DUPLEX_FULL )
				adv = ADVERTISED_1000baseT_Full;
			else
				adv = ADVERTISED_1000baseT_Half;
			break;
		default:
			ret = -EINVAL;
			return ret;
	}
	ret = xlp_enable_autoneg( dev,adv);
	return ret;

}

static int xlp_set_settings(struct net_device *dev, struct ethtool_cmd *cmd)
{
	int ret;
	struct dev_data *priv = netdev_priv(dev);

	if (priv->type != SGMII_IF) {
		return -EIO;
	}
	if (cmd->autoneg == AUTONEG_ENABLE) {
		ret = xlp_enable_autoneg(dev, cmd->advertising);
	}else {
		ret = xlp_set_link_speed(dev, cmd->speed, cmd->duplex);
	}
	return ret;
}

static int xlp_config_msec_tx(struct net_device *dev, struct ethtool_cmd *cmd)
{
	int ret = 0;
	if(is_nlm_xlp2xx())
	{
	//	struct dev_data *priv = netdev_priv(dev);
		printk("%s node = %d port_enable = %x preamble_len = %x packet_num = %d pn_thrshld = %d\n", __FUNCTION__, cmd->node, cmd->port_enable, cmd->preamble_len, cmd->packet_num, cmd->win_size_thrshld);
		nlm_hal_msec_tx_config(cmd->node, cmd->port_enable, cmd->preamble_len, cmd->packet_num, cmd->win_size_thrshld);
	}
	return ret;
}

static int xlp_config_msec_tx_mem(struct net_device *dev, struct ethtool_cmd *cmd)
{
	int ret = 0;
	if(is_nlm_xlp2xx())
	{
	//	struct dev_data *priv = netdev_priv(dev);
		printk("%s node = %d context = %x tci = %x sci = %llx key = %llx\n", __FUNCTION__, cmd->node, cmd->port, cmd->tci, cmd->sci, *(uint64_t*)cmd->key);
		nlm_hal_msec_tx_mem_config(cmd->node, cmd->port/*context*/, cmd->tci, cmd->sci, cmd->key);
	}
	return ret;
}

static int xlp_config_msec_rx(struct net_device *dev, struct ethtool_cmd *cmd)
{
	int ret = 0;
	if(is_nlm_xlp2xx())
	{
//		struct dev_data *priv = netdev_priv(dev);
		printk("%s node = %d port_enable = %x preamble_len = %x packet_num = %d pn_thrshld = %d\n", __FUNCTION__, cmd->node, cmd->port_enable, cmd->preamble_len, cmd->packet_num, cmd->win_size_thrshld);
		nlm_hal_msec_rx_config(cmd->node, cmd->port_enable, cmd->preamble_len, cmd->packet_num, cmd->win_size_thrshld);
	}
	return ret;
}

static int xlp_config_msec_rx_mem(struct net_device *dev, struct ethtool_cmd *cmd)
{
	int ret = 0;
	if(is_nlm_xlp2xx())
	{
//		struct dev_data *priv = netdev_priv(dev);
		printk("%s node = %d, port = %d, index = %d, sci = %llx,  sci_mask = %llx key = %llx\n",__FUNCTION__, cmd->node, cmd->port, cmd->index, cmd->sci, cmd->sci_mask, *(uint64_t*)cmd->key); 
		nlm_hal_msec_rx_mem_config(cmd->node, cmd->port, cmd->index, cmd->sci, cmd->key, cmd->sci_mask);
	}
	return ret;
}


static void xlp_get_drvinfo(struct net_device *dev,
				struct ethtool_drvinfo *info)
{
	strcpy(info->driver, DRV_NAME);
	strcpy(info->version, DRV_VERSION);
}

static int xlp_get_regs_len(struct net_device *dev)
{
	return NLM_ETHTOOL_REG_LEN;
}

static int xlp_get_eeprom_len(struct net_device *dev)
{
        return NLM_EEPROM_LEN;
}

static int xlp_get_eeprom(struct net_device *dev,
                                struct ethtool_eeprom *eeprom, u8* temp)
{
        int i=0;
        struct eeprom_data *nlm_eeprom=NULL;
        struct dev_data *priv = netdev_priv(dev);
        unsigned long flags;
        u8 buff[50];
        nlm_eeprom = get_nlm_eeprom();

        spin_lock_irqsave(&priv->lock, flags);

        eeprom_dump(nlm_eeprom, buff, eeprom->offset,eeprom->len);

        for(i=0;i<eeprom->len;i++)
        {
                temp[i] = buff[i];
        }
        spin_unlock_irqrestore(&priv->lock, flags);

        return 0;
}

static int xlp_set_eeprom(struct net_device *dev, struct ethtool_eeprom *eeprom, u8* temp)
{
        u8 data[6];
        struct eeprom_data *nlm_eeprom=NULL;
        nlm_eeprom = get_nlm_eeprom();
        eeprom_get_mac_addr(nlm_eeprom,data,0);
        if(eeprom->magic==0xAA){

                data[eeprom->offset]= *temp;
                eeprom_set_mac_addr(nlm_eeprom, data, 0);
        }

        return 0;
}

static void xlp_get_regs(struct net_device *dev,
				struct ethtool_regs *regs, void *p)
{
	u32 *data = (u32 *)p;
	int i;
	struct dev_data *priv = netdev_priv(dev);
	unsigned long flags;

	memset((void *)data, 0, NLM_ETHTOOL_REG_LEN);

	spin_lock_irqsave(&priv->lock, flags);
	for(i=0; i <= NLM_NUM_REG_DUMP; i++)
		*(data + i) = nlm_hal_read_mac_reg(priv->node, priv->block, priv->index,  R_TX_CONTROL + i);
	spin_unlock_irqrestore(&priv->lock, flags);
}
static u32 xlp_get_msglevel(struct net_device *dev)
{
	return 0; //mac_debug;
}
static void xlp_set_msglevel(struct net_device *dev, u32 value)
{
//	mac_debug = value;
}

static int xlp_nway_reset(struct net_device *dev)
{
	struct dev_data *priv = netdev_priv(dev);
	int mii_status;
	unsigned long flags;
	int ret = -EINVAL;

	if (priv->type != SGMII_IF)
		return -EIO;

	spin_lock_irqsave(&priv->lock, flags);
	mii_status = nlm_xlp_mac_mii_read(priv, MII_BMCR);
	if(mii_status & BMCR_ANENABLE)
	{
		nlm_xlp_mac_mii_write(priv,
				MII_BMCR, BMCR_ANRESTART | mii_status);
		ret = 0;
	}
	spin_unlock_irqrestore(&priv->lock, flags);
	return ret;
}

static u32 xlp_get_link(struct net_device *dev)
{
	struct dev_data *priv = netdev_priv(dev);
	unsigned long flags;
	struct nlm_hal_mii_info mii_info;
	if (priv->type != SGMII_IF)
		return -EIO;

	spin_lock_irqsave(&priv->lock, flags);
	nlm_hal_status_ext_phy( priv->node, priv->phy.addr, &mii_info);

	spin_unlock_irqrestore(&priv->lock, flags);

	if(mii_info.link_stat==1)
                return 1;

	return 0;
}
#define NLM_STATS_KEY_LEN  \
		(sizeof(struct net_device_stats) / sizeof(unsigned long))
static struct {
	        const char string[ETH_GSTRING_LEN];
} xlp_ethtool_stats_keys[NLM_STATS_KEY_LEN] = {
	{ "rx_packets" },
	{ "tx_packets" },
	{ "rx_bytes" },
	{ "tx_bytes" },
	{ "rx_errors" },
	{ "tx_errors" },
	{ "rx_dropped" },
	{ "tx_dropped" },
	{ "multicast" },
	{ "collisions" },
	{ "rx_length_errors" },
	{ "rx_over_errors" },
	{ "rx_crc_errors" },
	{ "rx_frame_errors" },
	{ "rx_fifo_errors" },
	{ "rx_missed_errors" },
	{ "tx_aborted_errors" },
	{ "tx_carrier_errors" },
	{ "tx_fifo_errors" },
	{ "tx_heartbeat_errors" },
	{ "tx_window_errors" },
	{ "rx_compressed" },
	{ "tx_compressed" }
};
static int xlp_get_stats_count (struct net_device *dev)
{
	return NLM_STATS_KEY_LEN;
}

static void xlp_get_strings (struct net_device *dev, u32 stringset, u8 *buf)
{
	switch (stringset) {
	case ETH_SS_STATS:
		memcpy(buf, &xlp_ethtool_stats_keys,
				sizeof(xlp_ethtool_stats_keys));
		break;
	default:
		printk(KERN_WARNING "%s: Invalid stringset %d\n",
				__func__, stringset);
		break;
	}
}


/**********************************************************************
 * xlp_get_mac_stats -  collect stats info from Mac stats register
 * @dev   -  this is per device based function
 * @stats -  net device stats structure
 **********************************************************************/
void xlp_get_mac_stats(struct net_device *dev, struct net_device_stats *stats)
{
	struct dev_data *priv = netdev_priv(dev);
#ifdef CONFIG_64BIT
	unsigned long long val;
#endif

        if (priv->type == INTERLAKEN_IF) {
                nlm_hal_get_ilk_mac_stats(priv->node, priv->block, priv->phy.addr, stats);
                return;
        }

	stats->tx_packets = nlm_hal_read_mac_reg(priv->node, priv->block, priv->index, TX_PACKET_COUNTER);
#ifdef CONFIG_64BIT
	val = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, 0x1f);
	stats->tx_packets |= ( val << 32);
#endif

	stats->rx_packets = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_PACKET_COUNTER);
#ifdef CONFIG_64BIT
	val = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, 0x1f);
	stats->rx_packets |= ( val << 32);
#endif

	stats->tx_bytes = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_BYTE_COUNTER);
#ifdef CONFIG_64BIT
	val = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, 0x1f);
	stats->tx_bytes |= ( val << 32);
#endif

	stats->rx_bytes = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_BYTE_COUNTER);
#ifdef CONFIG_64BIT
	val = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, 0x1f);
	stats->rx_bytes |= ( val << 32);
#endif

	stats->tx_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_FCS_ERROR_COUNTER) + 
		nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_JABBER_FRAME_COUNTER);
	stats->rx_dropped = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_DROP_PACKET_COUNTER);
	stats->tx_dropped = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_DROP_FRAME_COUNTER);

	stats->multicast = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_MULTICAST_PACKET_COUNTER);
#ifdef CONFIG_64BIT
	val = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, 0x1f);
	stats->multicast |= ( val << 32);
#endif

	stats->collisions = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_TOTAL_COLLISION_COUNTER);
	stats->rx_length_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_FRAME_LENGTH_ERROR_COUNTER);
	stats->rx_over_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_DROP_PACKET_COUNTER);
	stats->rx_crc_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_FCS_ERROR_COUNTER) + 
		 nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_JABBER_COUNTER);
	stats->rx_frame_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, RX_ALIGNMENT_ERROR_COUNTER);
	stats->rx_fifo_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index,RX_DROP_PACKET_COUNTER);
	stats->rx_missed_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index,RX_CARRIER_SENSE_ERROR_COUNTER);
	stats->rx_errors = (stats->rx_over_errors + stats->rx_crc_errors + stats->rx_frame_errors + stats->rx_fifo_errors +stats->rx_missed_errors);
	stats->tx_aborted_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_EXCESSIVE_COLLISION_PACKET_COUNTER);
	/*
	stats->tx_carrier_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_DROP_FRAME_COUNTER);
	stats->tx_fifo_errors = nlm_hal_read_mac_reg(priv->node,  priv->block, priv->index, TX_DROP_FRAME_COUNTER);
	*/
	return;
}

/**********************************************************************
 * xlp_get_ethtool_stats -  part of ethtool_ops member function
 * @dev   -  this is per device based function
 * @stats -  net device stats structure
 **********************************************************************/
static void xlp_get_ethtool_stats (struct net_device *dev,
			struct ethtool_stats *estats, u64 *stats)
{
	int i;
	struct dev_data *priv = netdev_priv(dev);
	unsigned long flags;
	unsigned long *tmp_stats;

	spin_lock_irqsave(&priv->lock, flags);

	xlp_get_mac_stats(dev, &priv->stats);


	spin_unlock_irqrestore(&priv->lock, flags);

	tmp_stats = (unsigned long *)&priv->stats;
	for(i=0; i < NLM_STATS_KEY_LEN; i++) {
		*stats = (u64)*tmp_stats;
		stats++;
		tmp_stats++;
	}
}

/**********************************************************************
 *  nlm_xlp_mac_mii_read - Read mac mii phy register
 *
 *  Input parameters:
 *  	   priv - priv structure
 *  	   phyaddr - PHY's address
 *  	   regidx = index of register to read
 *
 *  Return value:
 *  	   value read (16 bits), or 0xffffffff if an error occurred.
 ********************************************************************* */
static unsigned int nlm_xlp_mac_mii_read(struct dev_data *priv, int regidx)
{
        return nlm_hal_mdio_read(priv->node, NLM_HAL_EXT_MDIO, 0, BLOCK_7, LANE_CFG, priv->phy.addr, regidx);
}

/**********************************************************************
 *  nlm_xlp_mac_mii_write -Write mac mii PHY register.
 *
 *  Input parameters:
 *  	   priv - priv structure
 *  	   regidx - register within the PHY
 *  	   regval - data to write to register
 *
 *  Return value:
 *  	   nothing
 ********************************************************************* */
static void nlm_xlp_mac_mii_write(struct dev_data *priv, int regidx, uint16_t regval)
{
	nlm_hal_mdio_write(priv->node, NLM_HAL_EXT_MDIO, 0, BLOCK_7, LANE_CFG, priv->phy.addr, regidx, regval);
	return;
}

static struct ethtool_ops xlp_ethtool_ops= {
        .get_settings           = xlp_get_settings,
        .set_settings           = xlp_set_settings,
        .get_drvinfo            = xlp_get_drvinfo,
        .get_regs_len           = xlp_get_regs_len,
        .get_regs               = xlp_get_regs,
        .get_msglevel           = xlp_get_msglevel,
        .set_msglevel           = xlp_set_msglevel,
        .nway_reset             = xlp_nway_reset,
        .get_link               = xlp_get_link,
        .get_strings            = xlp_get_strings,
        .get_stats_count        = xlp_get_stats_count,
        .get_ethtool_stats      = xlp_get_ethtool_stats,
	.get_eeprom_len         = xlp_get_eeprom_len,
        .get_eeprom             = xlp_get_eeprom,
        .set_eeprom             = xlp_set_eeprom,
	.msec_tx_config 	= xlp_config_msec_tx,
	.msec_tx_mem_config 	= xlp_config_msec_tx_mem,
	.msec_rx_config 	= xlp_config_msec_rx,
	.msec_rx_mem_config 	= xlp_config_msec_rx_mem,

};

void xlp_set_ethtool_ops(struct net_device *netdev)
{
	SET_ETHTOOL_OPS(netdev, &xlp_ethtool_ops);
}


/**********************************************************************
 **********************************************************************/
void nlm_xlp_mac_set_enable(struct dev_data *priv, int flag)
{
	int inf;
	uint32_t speed = 0, duplex = 0, ifmode = 0;
	uint32_t netwk_inf = 0, mac_cfg2 = 0;
	struct nlm_hal_mii_info mii_info;

	if ((priv->type != SGMII_IF) && (priv->type != XAUI_IF))
		return;
	switch(priv->type) {
		case SGMII_IF:
			inf = (priv->block * 4) + priv->index;
			break;
		case XAUI_IF:
		case INTERLAKEN_IF:
			inf = priv->block;
			break;
		default:
			return;
	}

	if (flag) {
		if (priv->type == SGMII_IF) {
			if(nlm_hal_status_ext_phy(priv->node, inf, &mii_info))
			{
				//nlm_print("mac set enable speed %d duplex %d\n",speed, duplex);
				speed= mii_info.speed;
                                duplex= mii_info.duplex;
				ifmode = ((speed == 2) ? 2: 1);
				nlm_hal_mac_disable(priv->node, inf, priv->type);
			        netwk_inf  = read_gmac_reg(priv->node, inf, NETWK_INF_CTRL_REG);
				netwk_inf &= (~(0x3));
				write_gmac_reg(priv->node, inf , NETWK_INF_CTRL_REG, netwk_inf | speed);
				mac_cfg2 = read_gmac_reg(priv->node, inf, MAC_CONF2);
				mac_cfg2 &= (~((0x3 << 8) | 1));
				write_gmac_reg(priv->node, inf , MAC_CONF2,
					              mac_cfg2 | (ifmode << 8) | duplex);
	
			}
		}
		nlm_hal_mac_enable(priv->node, inf, priv->type);
		/* disabling the flow control */
		if(priv->type == XAUI_IF) {
			unsigned int xaui_cfg=nlm_hal_read_mac_reg(priv->node, inf, XGMAC, XAUI_CONFIG_1);
			xaui_cfg &= (~(XAUI_CONFIG_TCTLEN | XAUI_CONFIG_RCTLEN));
			nlm_hal_write_mac_reg(priv->node, inf, XGMAC, XAUI_CONFIG_1, xaui_cfg);
		}
	} else {
		nlm_hal_mac_disable(priv->node, inf, priv->type);
	}
}

