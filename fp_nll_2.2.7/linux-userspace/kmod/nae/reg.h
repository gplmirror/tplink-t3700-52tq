#ifndef REG_H
#define REG_H

// Reg info
#define INF_MAC_CONFIG1                     0
#define INF_MAC_CONFIG2                     1
#define INF_NETWK_INF_CTRL_REG              0x7f

#define NETIOR_MISC_REG1_ADDR       0x39
/* BAR address          */

#define NAE_BAR_ADDRESS             0
#define NETIOR_SOFTRESET            0x3
    //NETWORK INF CTRL REG(non of the values match PRM)
#define SOFTRESET(x)                        (x<<11)
#define STATS_EN(x)                         (x<<16)
#define TX_EN(x)                            (x<<2)
#define SPEED(x)                            (x&0x3)
    //MAC_CONFIG1
#define INF_SOFTRESET(x)                    (x<< 31)
#define INF_LOOP_BACK(x)                    (x<< 8)
#define INF_RX_ENABLE(x)                    (x<< 2)
#define INF_TX_ENABLE(x)                    (0x1)
    //MAC_CONFIG2

#define INF_PREMBL_LEN(x)                   ((x & 0xf)<<12)
#define INF_IFMODE(x)                       ((x & 0x3) << 8)
#define INF_LENCHK(x)                       (((x & 0x1)) << 4)
#define INF_PADCRCEN(x)                     ((x&0x1)<<2)
#define INF_PADCRC(x)                       ((x&0x1)<<1)
#define INF_FULLDUP(x)                      (x&0x1)
#define NETIOR_MISC_REG2_ADDR               (0x3a)
#define NAE_REG_ADDRS(r)              (NAE_BAR_ADDRESS&(0xffffff00000)|0x7<<13| ((r &0x3ff)<<2))
#define NAE_REG_TX_CONFIG              0x11
#define NAE_REG_TXIORCRDT_INIT         0x59
#define TXINITIORCR(x)                 (x & 0x7ffff) << 8



#endif
