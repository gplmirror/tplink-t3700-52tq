#ifndef _XLP_NAE_H
#define _XLP_NAE_H

#if 0
/*
 *      IEEE 802.3 Ethernet magic constants.  The frame sizes omit the preamble
 *      and FCS/CRC (frame check sequence).
 */

#define ETH_ALEN        6               /* Octets in one ethernet addr   */
#define ETH_HLEN        14              /* Total octets in header.       */
#define ETH_ZLEN        60              /* Min. octets in frame sans FCS */
#define ETH_DATA_LEN    1500            /* Max. octets in payload        */
#define ETH_FRAME_LEN   1514            /* Max. octets in frame sans FCS */
#define ETH_FCS_LEN     4               /* Octets in the FCS             */
#endif

#define NLM_TCP_MODE	1
#define NLM_RT_MODE	2
#define NLM_TCP_LOAD_BALANCE_MODE	3

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
#define PREPAD_LEN		16
#define NLM_LOAD_BALANCING_MAGIC	0xdeadbeefU
#else
#define PREPAD_LEN   		0
#endif
#define BYTE_OFFSET		2
#define ETH_JUMBO_DATA_LEN	16000

#define CACHELINE_SIZE		(1ULL << 6)
#define CACHELINE_ALIGNED(addr)	( ((addr) + (CACHELINE_SIZE-1)) & ~(CACHELINE_SIZE-1) )

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
#define KB(x)	(x<<10)
#define NLM_UCORE_SHARED_TABLE_SIZE ((KB(16))/(sizeof(uint32_t)))
#define NLM_UCORE_SHMEM_OFFSET	(KB(16))
#define NUM_LOAD_BALANCE_CPU	32
#endif

#define SKB_BACK_PTR_SIZE	CACHELINE_SIZE

#define NLM_RX_ETH_BUF_SIZE	(ETH_DATA_LEN+ETH_HLEN+ETH_FCS_LEN+BYTE_OFFSET+PREPAD_LEN+SKB_BACK_PTR_SIZE+CACHELINE_SIZE)
#define NLM_RX_JUMBO_BUF_SIZE	(ETH_JUMBO_DATA_LEN+ETH_HLEN+ETH_FCS_LEN+BYTE_OFFSET+PREPAD_LEN+SKB_BACK_PTR_SIZE+CACHELINE_SIZE)

extern unsigned long long netlib_vaddrb;
extern unsigned long long netlib_paddrb;
#define INIT_VBASE( vbase, pbase) {netlib_vaddrb = vbase ; netlib_paddrb = pbase;}
#define PHYS_TO_VIRT(paddr) 	(uint64_t)((paddr) - (netlib_paddrb) + (netlib_vaddrb))
#define VIRT_TO_PHYS(vaddr) 	(uint64_t)((vaddr) - (netlib_vaddrb) + (netlib_paddrb))

#define PADDR_BASE		0x100000ULL
#define PADDR_SIZE		0x200000
#define LRO_MAX_DESCS		8

struct cpu_stat {
        unsigned long tx_packets;
        unsigned long txc_packets;
        unsigned long rx_packets;
        unsigned long interrupts;
};

typedef enum xlp_net_types { TYPE_XLP_GMAC = 0, TYPE_XLP_XGMAC, TYPE_XLP_XAUI, TYPE_XLP_INTERLAKEN, MAX_XLP_NET_TYPES }xlp_interface_t;

typedef enum { xlp_mac_speed_10, xlp_mac_speed_100,
               xlp_mac_speed_1000, xlp_mac_speed_rsvd
} xlp_mac_speed_t;

typedef enum { xlp_mac_duplex_auto, xlp_mac_duplex_half,
               xlp_mac_duplex_full
} xlp_mac_duplex_t;

typedef enum { xlp_mac_fc_auto, xlp_mac_fc_disabled, xlp_mac_fc_frame,
               xlp_mac_fc_collision, xlp_mac_fc_carrier
} xlp_mac_fc_t;

struct phy_info {
        int addr;
        int mode;
        uint32_t *mii_addr;
        uint32_t *pcs_addr;
        uint32_t *serdes_addr;
};

#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
/*Flow Meta Info*/
struct flow_meta_info
{
	volatile uint64_t total_bytes_rcvd;
	volatile uint64_t last_sample_bytes;
	volatile uint64_t cpu_owner;
	uint64_t pad[5];
};

/*Active flow list*/
struct active_flow_list
{
	volatile int index_to_flow_meta_info[NLM_UCORE_SHARED_TABLE_SIZE];
	volatile uint64_t cpu_data_rate;
	spinlock_t lock;
	volatile uint64_t nr_active_flows;
	volatile uint64_t nr_flow_created;
	volatile uint64_t nr_flow_processed;
	uint64_t pad[3];
};
#endif


struct dev_data
{
        struct net_device *dev;
        struct net_device_stats stats;
        struct cpu_stat cpu_stats[NR_CPUS];
        struct timer_list link_timer;
        struct napi_struct napi;
        spinlock_t lock;
        unsigned short port;
	unsigned short inited;
	unsigned short node;
        unsigned short block;
        unsigned short index;
        unsigned short type;
        struct sk_buff* skb;
        int phy_oldlinkstat;
        __u8 hwaddr[6];

        xlp_mac_speed_t speed;  /* current speed */
        xlp_mac_duplex_t duplex;        /* current duplex */
        xlp_mac_fc_t flow_ctrl; /* current flow control setting */
        int advertising;
        struct phy_info phy;
        int nae_rx_qid;
        int nae_tx_qid;
	int hw_port_id;
	int mgmt_port;
	struct net_lro_mgr lro_mgr[NR_CPUS];
	struct net_lro_desc lro_arr[NR_CPUS][LRO_MAX_DESCS];
	
	/*1588 ptp timer*/
	struct cyclecounter cycles;
	struct timecounter clock;
	struct timecompare compare;
};

static inline void prefetch_local(const void *addr)
{
        __asm__ __volatile__(
        "       .set    mips4           \n"
        "       pref    %0, (%1)        \n"
        "       .set    mips0           \n"
        :
        : "i" (Pref_StoreStreamed), "r" (addr));
}

/**********************************************************************
 * nlm_xlp_alloc_skb_atomic -  Atomically allocates 64 bits cache aligned skb buffer
 * return - skb buffer address
 *
 **********************************************************************/
static __inline__ struct sk_buff *nlm_xlp_alloc_skb_atomic(int length, int node)
{
        int offset = 0;
	gfp_t gfp_mask = GFP_ATOMIC;

	struct sk_buff *skb = __alloc_skb(length + NET_SKB_PAD, gfp_mask, 0, node);
        if (!skb) 
                return NULL;

	skb_reserve(skb, NET_SKB_PAD);

        /* align the data to the next cache line */
        offset = ((unsigned long)skb->data + CACHELINE_SIZE) &
                ~(CACHELINE_SIZE - 1);
        skb_reserve(skb, (offset - (unsigned long)skb->data));
#ifdef CONFIG_NLM_NET_OPTS
        skb->netl_skb = skb;
#endif
        return skb;
}

static __inline__ void mac_put_skb_back_ptr(struct sk_buff *skb)
{
        uint64_t *back_ptr = (uint64_t *)skb->data;

        /* this function should be used only for newly allocated packets. It assumes
         * the first cacheline is for the back pointer related book keeping info
         */
        skb_reserve(skb, SKB_BACK_PTR_SIZE);
        *back_ptr = (uint64_t)(unsigned long)skb;
}

/* FMN send failure errors */
#define MSG_DST_FC_FAIL                 0x01
#define MSG_INFLIGHT_MSG_EX             0x02
#define MSG_TXQ_FULL                    0x04

static __inline__ void print_fmn_send_error(const char *str, uint32_t send_result)
{
	if(send_result & MSG_DST_FC_FAIL)
	{
		printk("[%s] Msg Destination flow control credit fail(send_result=%08x)\n",
		       str, send_result);
	}
	else if (send_result & MSG_INFLIGHT_MSG_EX) {
		printk("[%s] MSG_INFLIGHT_MSG_EX(send_result=%08x)\n", __func__, send_result);
	}
	else if (send_result & MSG_TXQ_FULL) {
		printk("[%s] TX message Q full(send_result=%08x)\n", __func__, send_result);
	}
	else {
		printk("[%s] Unknown send error type(send_result=%08x)\n", __func__, send_result);
	}
}

#endif
