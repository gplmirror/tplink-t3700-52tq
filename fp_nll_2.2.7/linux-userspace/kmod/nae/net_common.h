#ifndef NET_COMMON_H
#define NET_COMMON_H

#include <nlm_hal_nae.h>

#define MAX_FMN_CODE            -1
#define FMN_CREDIT_DEFAULT      8
#define FMN_POE_CREDIT_DEFAULT      9
#define MAX_FMN_ARRAY               50
#define SUCCESS                 0
#define FAIL                    -1
#define CPU0_VC                 0

#define CPU_Q_ID(cpu, vid) (cpu)

#define MAX_DEST_QID            50


#if 1
#include <asm/atomic.h>

#define STATS_SET(x,v)  //atomic64_set((atomic64_t *)&(x), (v))
#define STATS_ADD(x,v)  //atomic64_add((long)(v), (atomic64_t *)&(x))
#define STATS_INC(x)    //atomic64_inc((atomic64_t *)&(x))
#define STATS_READ(x)   (x)//atomic64_read((atomic64_t *)&(x))
#else
#define STATS_SET(x,v)  do { (x) = (v); } while (0)
#define STATS_ADD(x,v)  do { (x) += (v); } while (0)
#define STATS_INC(x)    do { (x) += 1; } while (0)
#define STATS_READ(x)   (x)
#endif

#define XLP_SOC_MAC_DRIVER "XLP Mac Driver"

/* On-Chip NAE PCI Header */
#undef PCI_NETL_VENDOR
#define PCI_NETL_VENDOR			0xfecc
#define PCI_DEVID_BASE			0
#define PCI_DEVID_OFF_NET		0

#define MAX_GMAC_PORT               	18
#define XLP_SGMII_RCV_CONTEXT_NUM	8


#define  DUMP_PKT(str, x, y)	{	\
	int i;      					\
        printk(" %s \n", str);                  	\
        for(i = 0; i < y; i++)				\
        {						\
                printk("%02x ", (x)[i]);		\
                if( i % 16 == 15)			\
                        printk("\n");			\
        }						\
	printk("\n"); }


#define NUM_XLP8XX_MGMT_PORTS	2

#define MAX_TSO_SKB_PEND_REQS	200
#define MAX_PACKET_SZ_PER_MSG	16384
#define P2P_EXTRA_DESCS		((PAGE_SIZE / MAX_PACKET_SZ_PER_MSG) + 4)
#define MSEC_EXTRA_MEM		(8) /* 8 * 64 bit = 512bits=64bytes. for sectag another one for icv */
#define P2P_SKB_OFF		(MAX_SKB_FRAGS + P2P_EXTRA_DESCS + MSEC_EXTRA_MEM - 1)
#define tso_dbg(fmt, args...)	//printk(fmt, ##args);
#define RX_UNCLASSIFIED_PKT 	(1<<5)
#define RX_IP_CSUM_VALID 	(1<<3)
#define RX_TCP_CSUM_VALID 	(1<<2)

struct p2p_desc_mem {
	void *mem;
	uint64_t dsize;
	uint64_t pad[6];
};

enum msc_opcodes { IP_CHKSUM = 1,
	TCP_CHKSUM,
	UDP_CHKSUM,
	SCTP_CRC,
	FCOE_CRC,
	IP_TCP_CHKSUM,
	TSO_IP_TCP_CHKSUM,
	IP_UDP_CHKSUM,
	IP_CHKSUM_SCTP_CRC
};

#define ETHER_FRAME_MIN_LEN	64

/* Use index of 8 as the offset because of n64 abi and 64B cacheline size */
#define CPU_INDEX(cpu) ((cpu) * 8)


#define napi_dbg(fmt, args...) { }

typedef struct fmn_credit_struct {
   unsigned int   s_qid;
   unsigned int   d_qid;
   unsigned int   flag;
   #define SET_UP_QUEUE         0x1
   #define SET_UP_CREDITS       0x2
   #define SET_UP_MULTI_DEST    0x4
   #define SET_UP_MULTI_SRC     0x8
   unsigned int   q_len;
#define FMN_QLEN_USE_DEFAULT      0
   unsigned int   credit;
} fmn_credit_type;

extern int init_gmac(unsigned int inf);
extern int init_tx_if_credit( /*uint32_t*/__u32 credit_val, unsigned int if_bmask);
extern int init_ucore(uint32_t ucore_mask, int if_num);
extern void init_ingress(void);
extern void init_egress(void);
extern int fmn_init(const fmn_credit_type *credit);
extern void *xlp_init_buffer( size_t size,
			      size_t pbase ,
			      uint64_t *vaddr_base);

extern void *init_nae_free_pool(int num_queue,
				unsigned char *pktmem ,
				int num_bytes,
				int num_desc);
extern void print_netreg(void);

#define DBG        1
#ifdef DBG
    #define log_dbg     printk
    #define log_pkt     printk
#else
    #define log_dbg(...)
    #define log_pkt(...)
//    #define log_err(...)
#endif
#define log_err
#define log_info   printk

#ifdef DBG
static __inline__ void press_key_to_continue(void) {
	log_dbg("press <enter> to continue...\n");
/*	getchar();*/
}
#else
#define press_key_to_continue()
#endif

#define NAE_RX_ENABLE 0x1
#define NAE_TX_ENABLE 0x1

struct xlp_msg {
	uint64_t entry[4];
};

static __inline__ void msg_print(uint32_t size, uint32_t code, uint32_t dest, struct xlp_msg *msg) {
	int i;
	log_dbg("  size = %u\n"
	       "  code = %u (0x%x)\n"
	       "  dest = %u (0x%x)\n",
	       size, code, code, dest, dest);
	for (i = 0; i < size && size <= 4; ++i) {
		log_dbg("  msg.entry%d = 0x%016llx\n",
		       i, msg->entry[i]);
	}
}

static __inline__ void poe_print(uint64_t msg0) {
	log_dbg("POE nextfid  = %llu (0x%llx)\n"
	       "    nextdist = %llu (0x%llx)\n"
	       "    nextdest = %llu (0x%llx)\n"
	       "    msgaddr  = 0x%llx\n"
	       "    fid      = %llu (0x%llx)\n",
	       (msg0 >> 48) & 0xffff, (msg0 >> 48) & 0xffff,
	       (msg0 >> 44) & 0xf, (msg0 >> 44) & 0xf,
	       (msg0 >> 32) & 0xfff, (msg0 >> 32) & 0xfff,
	       (msg0 >> 16) & 0xffff,
	       (msg0) & 0xffff, (msg0) & 0xffff);
}

static __inline__ void rx_print(uint64_t msg0) {
	log_dbg("RX  context = %llu\n"
	       "    length  = %llu (0x%llx)\n"
	       "    address = 0x%010llx\n"
	       "    unclass = %llu\n"
	       "    err     = %llu\n"
	       "    IPcksm  = %llu\n"
	       "    TCPcksm = %llu\n"
	       "    prepad  = %llu\n"
	       "    p2p     = %llu\n",
	       (msg0 >> 54) & 0x3ff,
	       (msg0 >> 40) & 0x3fff, (msg0 >> 40) & 0x3fff,
	       (msg0) & 0xffffffffc0ULL,
	       (msg0 >> 5) & 0x1,
	       (msg0 >> 4) & 0x1,
	       (msg0 >> 3) & 0x1,
	       (msg0 >> 2) & 0x1,
	       (msg0 >> 1) & 0x1,
	       (msg0) & 0x1);
}

static __inline__ void buf_print(unsigned char *buf, unsigned long len) {
	unsigned long i;
	for (i = 0; i < len; ++i) {
		log_dbg(" %02x", buf[i]);
		if (i % 8 == 7) log_dbg(" ");
		if (i % 32 == 31) log_dbg("\n");
	}
	log_dbg("\n");
}

#define CRC_LEN 4
#define BYTE_OFFSET 2

#define NULL_VFBID 127

static __inline__ uint64_t nae_tx_desc(unsigned int type,
	unsigned int rdex, unsigned int fbid, unsigned int len, uint64_t addr) {
	return ((uint64_t)(type & 0x3) << 62) |
	       ((uint64_t)(rdex & 0x1) << 61) |
	       ((uint64_t)(fbid & 0x7f) << 54) |
	       ((uint64_t)(len & 0x3fff) << 40) |
	       (addr&0xffffffffffULL);
}

static __inline__ void tx_print(uint64_t msg0) {
	log_dbg("TX  type    = %llu\n"
	       "    rdex    = %llu\n"
	       "    vfbid   = %llu\n"
	       "    length  = %llu (0x%llx)\n"
	       "    address = 0x%010llx\n",
	       ((msg0 >> 62) & 0x3),
	       ((msg0 >> 61) & 0x1),
	       ((msg0 >> 54) & 0x7f),
	       ((msg0 >> 40) & 0x3fff), ((msg0 >> 40) & 0x3fff),
	       (msg0) & 0xffffffffffULL);
}

extern void *fdt;

struct nae_port {
	int  valid;
	int  mgmt;
        int  num_free_desc;
        int  txq_range[2];
        int  rxq;
        int  hw_port_id;
};

struct nae_config {
	int fb_vc;
        int rx_vc;
	int num_ports;
	struct nae_port ports[18];
};

extern int initialize_nae(unsigned int *phys_cpu_map, int mode, int *jumbo_enabled);
extern void nlm_xlp_msgring_int_handler(unsigned int irq, struct pt_regs *regs);
extern int replenish_freein_fifos(void);


#endif
