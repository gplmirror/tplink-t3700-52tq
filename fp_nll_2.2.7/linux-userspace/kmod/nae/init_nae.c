#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/delay.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/net_tstamp.h>
#include <linux/clocksource.h>
#include <linux/timecompare.h>
#include <linux/inet_lro.h>
#include <asm/netlogic/msgring.h>
#include <asm/netlogic/cpumask.h>
#include <asm/netlogic/hal/nlm_hal_fmn.h>
#include <asm/netlogic/hal/nlm_hal_nae.h>
#include <asm/netlogic/hal/nlm_hal_xlp_dev.h>
#include <ops.h>
#include <asm/netlogic/xlp.h>
#include "net_common.h"
#include "xlp_nae.h"

unsigned int cpu_2_normal_frfifo[NLM_MAX_NODES][NLM_NCPUS_PER_NODE];
unsigned int cpu_2_jumbo_frfifo[NLM_MAX_NODES][NLM_NCPUS_PER_NODE];
static unsigned int lnx_normal_mask;
static unsigned int lnx_jumbo_mask;
extern int num_descs_per_normalq; 	
extern int num_descs_per_jumboq; 	

int nlm_configure_shared_freein_fifo(int node, nlm_nae_config_ptr nae_cfg);

/* This strucutre has been referenced in templates/hybrid_nae and ucore/hybrid_nae. 
 So whenever you modify, modify the above places also. */

struct nlm_nae_linux_shinfo {
	/* valid along with up and down status, ucore checks only for non-domain 0 */
	int valid;
	int rxvc;
	int domid;
	int mode;
	int jumbo_enabled;
	int node;
	/* logical cpu to physical cpu map */
	unsigned int lcpu_2_pcpu_map[NLM_NAE_SH_LCPU_TO_MAP_SZ];
	/* cpu to freein fifo map */
	unsigned int cpu_2_freeinfifo_map[NLM_NAE_SH_LCPU_TO_MAP_SZ];
	unsigned int cpu_2_jumbo_freeinfifo_map[NLM_NAE_SH_LCPU_TO_MAP_SZ];
};

struct nlm_nae_linux_shinfo lnx_shinfo[NLM_NAE_MAX_SHARED_DOMS + 1]; //1 extra for owned domain

/* As there is a port level fifo checkup done in NAE h/w, we need to fill up the port
 fifos ( 0, 4, 8, 12 & 16) with some dummy entries if it is not owned by anyone. 
 */
static int init_dummy_entries_for_unused_fifos(int node, nlm_nae_config_ptr nae_cfg)
{
	static unsigned long long msg;
	unsigned long __attribute__ ((unused)) mflags;
	int rv = 0, vc_index = 0, i, j, ret, code;
	unsigned int fifo_mask = 0;
	struct sk_buff * skb;
	int size = NLM_RX_JUMBO_BUF_SIZE, shdom;

	skb = nlm_xlp_alloc_skb_atomic(size, node);
	if(!skb) {
		printk("[%s] alloc skb failed\n",__FUNCTION__);
		panic("panic...");
		return -ENOMEM;
	}
	msg = (unsigned long long)virt_to_bus(skb->data) & 0xffffffffffULL;

	for(shdom = 0; shdom <= NLM_NAE_MAX_SHARED_DOMS; shdom++) {
		if(!nae_cfg->shinfo[shdom].valid)
			continue;

		fifo_mask |= nlm_hal_retrieve_freein_fifo_mask(fdt, node, 
				nae_cfg->shinfo[shdom].domid);
	}

	msgrng_access_enable(mflags);
	for(i = 0; i < nae_cfg->frin_total_queue; i++) {
		/* nothing to do, if it is owned by some domain */
		if((1 << i) & fifo_mask) 
			continue;

		vc_index = i + nae_cfg->frin_queue_base;

		for(j = 0; j < 4; j++) {
			__sync();
			if ( (ret = nlm_hal_send_msg1(vc_index, code, msg)) & 0x7) {
				print_fmn_send_error(__func__, ret);
				printk("Unable to send configured free desc, check freein carving (qid=%d)\n", vc_index);
				rv = -1;
				goto err;
			}
		}
		printk("Send %d dummy descriptors for queue %d(vc %d) of length %d\n", 
				j, i, vc_index, size);
	}
err:
	msgrng_access_disable(mflags);

	/* if not used */
	if(!vc_index)
		dev_kfree_skb_any(skb);
	return rv;
}


static int nlm_initialize_vfbid(int node, nlm_nae_config_ptr nae_cfg)
{
	int cpu =0, tblidx, i = 0;
	uint32_t vfbid_tbl[128];
	int start = nae_cfg->vfbtbl_sw_offset;
	int end = start + nae_cfg->vfbtbl_sw_nentries;
	int frin_q_base = nlm_node_cfg.nae_cfg[0]->frin_queue_base;
	
	/* For s/w replenishment, each nodes tx completes can send to his own node cpus only */
	for (tblidx = start; tblidx < end ; tblidx++, cpu++) {
		vfbid_tbl[tblidx] = (cpu * 4) + nae_cfg->fb_vc + (node * 1024);
	}
	nlm_config_vfbid_table(node, start, end - start, &vfbid_tbl[start]);
	/* For h/w replenishment, each node fills up 20 entries for all other nodes
	starting from node0's queue-id. Software should offset the hw-offset + rx-node id
	to get the actual index 
	 */
	start = nae_cfg->vfbtbl_hw_offset;
	end = start + nae_cfg->vfbtbl_hw_nentries;
	for(tblidx = start; tblidx < end; tblidx++, i++) {
		if(i >= NLM_NAE_MAX_FREEIN_FIFOS_PER_NODE) {
			i = 0;
			frin_q_base = 1024 + frin_q_base;
		}
		vfbid_tbl[tblidx] = frin_q_base + i;
	}
	nlm_config_vfbid_table(node, start, end - start, &vfbid_tbl[start]);

	/* NULL FBID Should map to cpu0 to detect NAE send message errors*/
	vfbid_tbl[127] = 0;
	nlm_config_vfbid_table(node, 127, 1, &vfbid_tbl[127]);
	/*IEEE-1588 timestamp*/
	vfbid_tbl[126] = 0;
	nlm_config_vfbid_table(node, 126, 1, &vfbid_tbl[126]);
	return 0;
}

static void dump_lnx_shinfo(int node)
{
	int i, pos, bitoff;
	return;
	printk("%s(node %d) in, valid %d rxvc %d domid %d jumbo-en %d mode %d node %d\n", 
			__FUNCTION__, node,
			lnx_shinfo[0].valid, lnx_shinfo[0].rxvc, lnx_shinfo[0].domid,
			lnx_shinfo[0].jumbo_enabled, lnx_shinfo[0].mode,  lnx_shinfo[0].node);
	for(i = 0; i < NLM_NCPUS_PER_NODE; i++) {
		pos = i / NLM_NAE_SH_LCPU_TO_MAP_SZ;
		bitoff = (i % NLM_NAE_SH_LCPU_TO_MAP_NVALS_PER_ENTRY) * 
			NLM_NAE_SH_LCPU_TO_MAP_SNG_VAL_SZ;
		printk(" node %d lcpu %d pcpu %d rxfifo %d jumbo-rxfifo %d \n", 
				node, i, (lnx_shinfo[0].lcpu_2_pcpu_map[pos] >> bitoff) & 0x1f,
				(lnx_shinfo[0].cpu_2_freeinfifo_map[pos] >> bitoff) & 0x1f,
				(lnx_shinfo[0].cpu_2_jumbo_freeinfifo_map[pos] >> bitoff) & 0x1f
				);
	}
	for(i = 0; i < NLM_NCPUS_PER_NODE; i++) {
		printk(" node %d cpu %d cpu2nor-fr %d cpu2jum-fr %d\n", node, i, cpu_2_normal_frfifo[node][i],
				cpu_2_jumbo_frfifo[node][i]);
	}
}
         

int initialize_nae(unsigned int *phys_cpu_map, int mode, int *jumbo_enabled)
{
	int dom_id = 0;
	int node = 0;
	unsigned long __attribute__ ((unused)) mflags;
	int i,pos, bitoff;
	int rv = -1;
	nlm_nae_config_ptr nae_cfg;

	msgrng_access_enable(mflags);

	nlm_hal_init_nae(fdt, dom_id);


	for(node = 0; node < NLM_MAX_NODES; node++) {
		nae_cfg = nlm_node_cfg.nae_cfg[node];
		if (nae_cfg == NULL) 
			continue;
		
		for(i = 0; i <= NLM_NAE_MAX_SHARED_DOMS; i++) {
			lnx_shinfo[i].valid = nae_cfg->shinfo[i].valid;
			lnx_shinfo[i].rxvc = nae_cfg->shinfo[i].rxvc;
			lnx_shinfo[i].domid = nae_cfg->shinfo[i].domid;
			memcpy(&lnx_shinfo[i].lcpu_2_pcpu_map, nae_cfg->shinfo[i].lcpu_2_pcpu_map, 
					sizeof(nae_cfg->shinfo[i].lcpu_2_pcpu_map));
			memcpy(&lnx_shinfo[i].cpu_2_freeinfifo_map, nae_cfg->shinfo[i].cpu_2_freeinfifo_map, 
					sizeof(nae_cfg->shinfo[i].cpu_2_freeinfifo_map));
		}

		lnx_normal_mask = nae_cfg->freein_fifo_dom_mask;

		/* if jumbo enabled , we use half of the linux owned freein fifos for jumbo skbs */
		if(*jumbo_enabled) {
			int mine = 1;
			for(i = 0; i < nae_cfg->frin_total_queue; i++) {
				if((1 << i) & nae_cfg->freein_fifo_dom_mask) {
					if(mine) {
						mine = 0;
						continue;
					}
					lnx_normal_mask &= (~(1 << i));
					lnx_jumbo_mask |= (1 << i);
					mine = 1;
				}
			}
			if(lnx_jumbo_mask) {
				nlm_hal_derive_cpu_to_freein_fifo_map(node, phys_cpu_map[node],
					       	lnx_normal_mask, cpu_2_normal_frfifo[node]);
				nlm_hal_derive_cpu_to_freein_fifo_map(node, phys_cpu_map[node],
					       	lnx_jumbo_mask, cpu_2_jumbo_frfifo[node]);
				memset(lnx_shinfo[0].cpu_2_freeinfifo_map, 0, 
						sizeof(lnx_shinfo[0].cpu_2_freeinfifo_map));
				for(i = 0; i < NLM_NCPUS_PER_NODE; i++) {
					pos = i / NLM_NAE_SH_LCPU_TO_MAP_SZ;
					bitoff = (i % NLM_NAE_SH_LCPU_TO_MAP_NVALS_PER_ENTRY) * 
						NLM_NAE_SH_LCPU_TO_MAP_SNG_VAL_SZ;
					lnx_shinfo[0].cpu_2_freeinfifo_map[pos] |= (cpu_2_normal_frfifo[node][i] << bitoff);
					lnx_shinfo[0].cpu_2_jumbo_freeinfifo_map[pos] |= (cpu_2_jumbo_frfifo[node][i] << bitoff);
				}
			} else {
				printk("Disabling Jumbo because of unavailability of freein-fifo\n");
				*jumbo_enabled = 0;
			}
		} 
		if(*jumbo_enabled == 0) {
			for(i = 0; i < NLM_NCPUS_PER_NODE; i++) {
				pos = i / NLM_NAE_SH_LCPU_TO_MAP_SZ;
				bitoff = (i % NLM_NAE_SH_LCPU_TO_MAP_NVALS_PER_ENTRY) *
					NLM_NAE_SH_LCPU_TO_MAP_SNG_VAL_SZ;
				cpu_2_normal_frfifo[node][i] = (lnx_shinfo[0].cpu_2_freeinfifo_map[pos] >> bitoff) & 0x1f;
			}
		}
#ifdef CONFIG_NLM_ENABLE_LOAD_BALANCING
		if(mode == NLM_TCP_MODE)
			lnx_shinfo[0].mode = NLM_TCP_LOAD_BALANCE_MODE;
		else
#endif
		lnx_shinfo[0].mode = mode;
		lnx_shinfo[0].jumbo_enabled = *jumbo_enabled;
		lnx_shinfo[0].node = node;
		if(nae_cfg->owned) {
			nlm_hal_write_ucore_shared_mem(node, (unsigned int *)lnx_shinfo, sizeof(lnx_shinfo)/sizeof(uint32_t));
			nlm_hal_restart_ucore(node, fdt);
		}

		dump_lnx_shinfo(node);
#ifdef IEEE_1588_PTP_ENABLED
		nlm_hal_prepad_enable(node, 3);
#endif
	}

	for(node = 0; node < NLM_MAX_NODES; node++) {
		nae_cfg = nlm_node_cfg.nae_cfg[node];
		if (nae_cfg == NULL) 
			continue;
		/* initialize my vfbid table */
		if(!(nae_cfg->flags & VFBID_FROM_FDT))
			nlm_initialize_vfbid(node, nae_cfg);
		
		if(nae_cfg->owned == 0)
			continue;

		/* Update RX_CONFIG for desc size */
		if(*jumbo_enabled)
			nlm_hal_init_ingress (node, (ETH_JUMBO_DATA_LEN+ETH_HLEN+ETH_FCS_LEN+SMP_CACHE_BYTES) & ~(SMP_CACHE_BYTES - 1));
		else
			nlm_hal_init_ingress (node, (ETH_DATA_LEN+ETH_HLEN+ETH_FCS_LEN+SMP_CACHE_BYTES) & ~(SMP_CACHE_BYTES - 1));

		if(nlm_configure_shared_freein_fifo(node, nae_cfg) != 0)
			goto err;

		init_dummy_entries_for_unused_fifos(node, nae_cfg);
#if 0	

		if(is_nlm_xlp2xx())
		{
			nlm_hal_msec_tx_default_config(node,/* port_enable*/0xff, /*unsigned int preamble_len*/0x5555, /*packet_num*/ 0x0, /*pn_thrshld*/0x7fffffff);
			nlm_hal_msec_rx_default_config(node, /*port_enable*/0xff, /*preamble_len*/0xaaaa, /*packet_num*/0x0, /*replay_win_size*/0x0);
		}
#endif
	}
	rv = 0;
err:
	msgrng_access_disable(mflags);
	return rv;
}

static int nlm_replenish_per_cpu_buffer(int node, nlm_nae_config_ptr nae_cfg, int qindex, int bufcnt)
{
	int i, port;
	int vc_index = 0;
	int __attribute__ ((unused)) mflags, code;
	struct xlp_msg msg;
	struct sk_buff * skb;
	int ret = 0;
	int size = NLM_RX_ETH_BUF_SIZE;

	if((1 << qindex) & lnx_jumbo_mask)
		size = NLM_RX_JUMBO_BUF_SIZE;

	/* For queue index 16 and 17, we still use  the port level descriptor info */
	if(qindex >= 16) {
		for(port = 0; port < nae_cfg->num_ports; port++) {
			if(nae_cfg->ports[port].hw_port_id == qindex)
				bufcnt = nae_cfg->ports[port].num_free_desc;
	 	}
	}

	for(i = 0; i < bufcnt; i++)
	{
		vc_index = qindex + nae_cfg->frin_queue_base;
		skb = nlm_xlp_alloc_skb_atomic(size, node);
		if(!skb)
		{
			printk("[%s] alloc skb failed\n",__FUNCTION__);
			break;
		}
		/* Store skb in back_ptr */
		mac_put_skb_back_ptr(skb);
		code = 0;

		/* Send the free Rx desc to the MAC */
		msgrng_access_enable(mflags);
		msg.entry[0] = (unsigned long long)virt_to_bus(skb->data) & 0xffffffffffULL;
		msg.entry[1]= msg.entry[2] = msg.entry[3] = 0;
		/* Send the packet to nae rx  */
		__sync();

		if ( (ret = nlm_hal_send_msg1(vc_index, code, msg.entry[0])) & 0x7)
		{
			print_fmn_send_error(__func__, ret);
			printk("Unable to send configured free desc, check freein carving (qid=%d)\n", vc_index);
			/* free the buffer and return! */
			dev_kfree_skb_any(skb);

			msgrng_access_disable(mflags);
			ret = -EBUSY;
			break;
		}
		msgrng_access_disable(mflags);
	}
	printk("Send %d descriptors for queue %d(vc %d) of length %d\n", bufcnt, qindex, vc_index, size);
	return ret;
}


int replenish_freein_fifos(void)
{
	int node, i, rv;
	nlm_nae_config_ptr nae_cfg;
	int max_descs_pqueue, num_descs;

	for(node = 0; node < NLM_MAX_NODES; node++) {
		nae_cfg = nlm_node_cfg.nae_cfg[node];
		if (nae_cfg == NULL) 
			continue;

		/* configure the descs */

		for(i = 0; i < nae_cfg->frin_total_queue; i++) {
			max_descs_pqueue = nae_cfg->freein_fifo_onchip_num_descs[i] +  nae_cfg->freein_fifo_spill_num_descs;
			if((1 << i) & lnx_normal_mask)
				num_descs = num_descs_per_normalq <= max_descs_pqueue ? num_descs_per_normalq : max_descs_pqueue;
			else if((1 << i) & lnx_jumbo_mask)
				num_descs = num_descs_per_jumboq <= max_descs_pqueue ? num_descs_per_jumboq : max_descs_pqueue;
			else 
				continue;
				
			rv = nlm_replenish_per_cpu_buffer(node, nae_cfg, i, num_descs);
		}
		if(rv != 0)
			break;
	}
	return rv;
}

static inline unsigned int fdt32_to_cpu(unsigned int x)
{
	#define _BYT(n)	((unsigned long long)((unsigned char *)&x)[n])
#ifdef __MIPSEL__
	return (_BYT(0) << 24) | (_BYT(1) << 16) | (_BYT(2) << 8) | _BYT(3);
#else
	return x;
#endif
}

int nlm_configure_shared_freein_fifo(int node, nlm_nae_config_ptr nae_cfg)
{
	unsigned int cnode, fmask, dsize, dppadsz, ndescs;
	unsigned long long paddr, psize, epaddr;
	unsigned int *t;
	int len = 0, i = 0;
	unsigned int owner_replenish = 0, paddr_info_len, desc_info_len;
	char *paddr_info, *desc_info;
	unsigned long long msg;
	unsigned long __attribute__ ((unused)) mflags;
	int vc_index, rv = 0, code = 0, descs, fifo;
	int shdom, err = 0;

	for(shdom = 0; shdom <= NLM_NAE_MAX_SHARED_DOMS; shdom++) {
		if(!nae_cfg->shinfo[shdom].valid)
			continue;
		/* ignore my own domain id */
		if(nae_cfg->shinfo[shdom].domid == 0)
			continue;

		rv = nlm_hal_retrieve_shared_freein_fifo_info(fdt,
				nae_cfg->shinfo[shdom].domid,
				&owner_replenish,
				&paddr_info, &paddr_info_len,
				&desc_info, &desc_info_len);
		if(rv != 0) 
			continue;

		if(!owner_replenish)
			continue;

		t = (unsigned int *)paddr_info;
		i = 0;
		do {
			/* extract the config */
			cnode = fdt32_to_cpu(t[i]);
			paddr = ((unsigned long long)fdt32_to_cpu(t[i + 1])) << 32;
			paddr |= ((unsigned long long)fdt32_to_cpu(t[i + 2]));
			psize = ((unsigned long long)fdt32_to_cpu(t[i + 3])) << 32;
			psize |= ((unsigned long long)fdt32_to_cpu(t[i + 4]));
		
			i += 5;
			len = i * 4;
			if(cnode == node)
				break;
		} while(len < paddr_info_len);

		printk("domid %d node %d addr %llx size %llx\n", 
					nae_cfg->shinfo[shdom].domid, cnode, paddr, psize); 

		epaddr = paddr + psize;
		t = (unsigned int *)desc_info;
		i = 0;
		do {

			/* extract the config */
			cnode = fdt32_to_cpu(t[i]);
			fmask = fdt32_to_cpu(t[i + 1]);
			dsize = fdt32_to_cpu(t[i + 2]);
			dppadsz = fdt32_to_cpu(t[i + 3]);
			ndescs = fdt32_to_cpu(t[i + 4]);
		 
			i += 5;
			len = i * 4;

			if(cnode != node)
				continue;

			printk("node %d fmask %x dsize %d dppadsz %d ndescs %d \n",
					cnode, fmask, dsize, dppadsz, ndescs);

			for(fifo = 0; fifo < NLM_NAE_MAX_FREEIN_FIFOS_PER_NODE; fifo++) {
				if(!((1 << fifo) & fmask)) 
					continue;
				msgrng_access_enable(mflags);
				vc_index = fifo + nae_cfg->frin_queue_base;
				for(descs = 0; descs < ndescs; descs++) {
					if((paddr + dsize) > epaddr) {
						msgrng_access_disable(mflags);
						printk("Error, descriptors buffer overflow \n");
						err = -1;
						goto err_exit;
					}
					msg = paddr + dppadsz;

					__sync();
					rv = nlm_hal_send_msg1(vc_index, code, msg);
					if(rv & 0x7) {
						msgrng_access_disable(mflags);
						printk("Unable to send configured free desc, \
							check freein carving (qid=%d)\n", vc_index);
						err = -1;
						goto err_exit;
					}

					paddr += dsize;

				}
				msgrng_access_disable(mflags);
				printk("Send %d descriptors for queue %d(%d) of length %d\n", 
						ndescs, fifo, vc_index, dsize); 

			}
		} while(len < desc_info_len);

	}

err_exit:
	return err;
}
