/*
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  Copyright (C) 2008 Wind River Systems, Inc.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/irq.h>
#include <linux/slab.h>
#include <linux/profile.h>
#include <linux/kernel_stat.h>
#include <linux/wrhv.h>
#include <linux/sched.h>
#include <linux/smp.h>
#include <vbi/vbi.h>
#include <linux/suspend.h>
#include <linux/cpu.h>
#include <linux/debugfs.h>
#include <vbi/private.h>

#define VIOAPIC_BASE_ADDR	(&wr_vb_control->vIoapic)

#ifdef CONFIG_SMP
enum wrhv_irq_action {
	WRHV_IRQ_SET_AFFINITY = 1,
	WRHV_IRQ_SHUTDOWN,
	WRHV_IRQ_STARTUP
};

struct wrhv_irq_struct {
	enum wrhv_irq_action action;
	int irq;
	int ocpu, ncpu;
	struct wrhv_irq_struct *next;
};

struct wrhv_irq_head {
	struct wrhv_irq_struct *head;
	struct wrhv_irq_struct **next;
};

static struct wrhv_irq_head wrhv_irq_head = {
	.head	= NULL,
	.next	= &(wrhv_irq_head.head),
};

static DEFINE_SPINLOCK(wrhv_irq_lock);
static void wrhv_irq_task(unsigned long arg);
static DECLARE_TASKLET(wrhv_irq_tasklet, wrhv_irq_task, 0);
#endif

static void wrhv_enable_irq(unsigned int irq)
{
	vbi_unmask_vioapic_irq(irq);
}

static void wrhv_disable_irq(unsigned int irq)
{
	vbi_mask_vioapic_irq(irq);
}

static void wrhv_ack_irq(unsigned int irq)
{
	vbi_ack_vioapic_irq(irq);
}

static void wrhv_maskack_irq(unsigned int irq)
{
	if (irq != 0)
		vbi_mask_vioapic_irq(irq);
	vbi_ack_vioapic_irq(irq);
}

static void wrhv_mask_irq(unsigned int irq)
{
	if (irq != 0)
		vbi_mask_vioapic_irq(irq);
}

static void wrhv_unmask_irq(unsigned int irq)
{
	if (irq != 0)
		vbi_unmask_vioapic_irq(irq);
}

#ifdef CONFIG_SMP
static void wrhv_mask_irq_ipi(void *p)
{
	unsigned int irq = *(unsigned int *)p;

	wrhv_mask_irq(irq);
}

static void wrhv_unmask_irq_ipi(void *p)
{
	unsigned int irq = *(unsigned int *)p;

	wrhv_unmask_irq(irq);
}

static void wrhv_irq_task(unsigned long arg)
{
	struct wrhv_irq_struct *list;
	struct wrhv_irq_head *head = &wrhv_irq_head;

	spin_lock(&wrhv_irq_lock);
	list = head->head;
	head->head = NULL;
	head->next = &(head->head);
	spin_unlock(&wrhv_irq_lock);

	while (list) {
		struct wrhv_irq_struct *cur = list;
		int irq = cur->irq;

		switch (cur->action) {
		case WRHV_IRQ_SET_AFFINITY:
		{
			int ocpu = cur->ocpu, ncpu = cur->ncpu;

			smp_call_function_single(ocpu, wrhv_mask_irq_ipi,
						&irq, 1);
			smp_call_function_single(ncpu, wrhv_unmask_irq_ipi,
						&irq, 1);
			vbi_vcore_irq_redirect(irq, ncpu);
			break;
		}
		case WRHV_IRQ_SHUTDOWN:
		{
			int ocpu = cur->ocpu;

			smp_call_function_single(ocpu, wrhv_mask_irq_ipi,
						&irq, 1);
			break;
		}
		case WRHV_IRQ_STARTUP:
		{
			int ocpu = cur->ocpu;

			smp_call_function_single(ocpu, wrhv_unmask_irq_ipi,
						&irq, 1);
			break;
		}
		default:
			printk(KERN_ERR "Unknown action for irq task\n");
		}

		list = cur->next;
		kfree(cur);
	}
}

/* Currently all the external interrupts are routed to cpu 0 and
 * handled by cpu0, so we need make sure the startup/shutdown functions
 * operate cpu 0's vioapic.
 */
static void smp_wrhv_shutdown_irq(unsigned int irq)
{
	struct irq_desc *desc = irq_to_desc(irq);
	int cpu = cpumask_first(desc->affinity);

	if (cpu == smp_processor_id())
		wrhv_mask_irq(irq);
	else {
		struct wrhv_irq_struct *data;
		struct wrhv_irq_head *head = &wrhv_irq_head;

		data = kmalloc(sizeof(*data), GFP_ATOMIC);
		if (!data)
			return;

		data->irq = irq;
		data->ocpu = cpu;
		data->next = NULL;
		data->action = WRHV_IRQ_SHUTDOWN;

		spin_lock(&wrhv_irq_lock);
		*head->next = data;
		head->next = &(data->next);
		spin_unlock(&wrhv_irq_lock);

		tasklet_schedule(&wrhv_irq_tasklet);
	}
}

static unsigned int smp_wrhv_startup_irq(unsigned int irq)
{
	struct irq_desc *desc = irq_to_desc(irq);
	int cpu = cpumask_first(desc->affinity);

	if (cpu == smp_processor_id())
		wrhv_unmask_irq(irq);
	else {
		struct wrhv_irq_struct *data;
		struct wrhv_irq_head *head = &wrhv_irq_head;

		data = kmalloc(sizeof(*data), GFP_ATOMIC);
		if (!data)
			return -ENOMEM;

		data->irq = irq;
		data->ocpu = cpu;
		data->next = NULL;
		data->action = WRHV_IRQ_STARTUP;

		spin_lock(&wrhv_irq_lock);
		*head->next = data;
		head->next = &(data->next);
		spin_unlock(&wrhv_irq_lock);

		tasklet_schedule(&wrhv_irq_tasklet);
	}

	return 0;
}

int wrhv_irq_set_affinity(unsigned int irq,
				const struct cpumask *dest)
{
	struct irq_desc *desc = irq_to_desc(irq);
	struct wrhv_irq_struct *p;
	int cpu;
	struct wrhv_irq_head *head = &wrhv_irq_head;

#ifdef CONFIG_PPC85xx_VT_MODE
	/* Currently we don't support set affinity in direct irq mode */
	if (wrhv_dir_irq) {
		printk(KERN_WARNING "Currently we don't support set affinity"
			" in direct irq mode on e500mc.\n");

		return -1;
	}
#endif

	if (cpumask_equal(desc->affinity, dest))
		return 0;

	cpu = cpumask_first(dest);
	/* we only support to bond the irq to signle vcore */
	if (!cpumask_equal(cpumask_of(cpu), dest))
		return -1;

	p = kzalloc(sizeof(*p), GFP_ATOMIC);
	if (!p) {
		printk(KERN_ERR "Can't get memory for set irq affinity\n");
		return -1;
	}

	p->irq = irq;
	p->ocpu = cpumask_first(desc->affinity);
	p->ncpu = cpu;
	p->next = NULL;
	p->action = WRHV_IRQ_SET_AFFINITY;

	/* irq_set_affinity is invoked with irq disabled */
	spin_lock(&wrhv_irq_lock);
	*head->next = p;
	head->next = &(p->next);
	spin_unlock(&wrhv_irq_lock);

	tasklet_schedule(&wrhv_irq_tasklet);

	return 0;
}
#endif

struct irq_chip wrhv_irq_chip = {
	.name		= "WRHV-PIC",
#ifdef CONFIG_SMP
	.startup	= smp_wrhv_startup_irq,
	.shutdown	= smp_wrhv_shutdown_irq,
	.set_affinity	= wrhv_irq_set_affinity,
#endif
	.mask		= wrhv_mask_irq,
	.ack		= wrhv_ack_irq,
	.disable	= wrhv_disable_irq,
	.enable		= wrhv_enable_irq,
	.unmask		= wrhv_unmask_irq,
	.mask_ack	= wrhv_maskack_irq,
	.eoi		= wrhv_ack_irq,
};

#ifdef CONFIG_SMP
struct irq_chip wrhv_ipi_irq_chip = {
	.name		= "WRHV-IPI-PIC",
	.mask		= wrhv_mask_irq,
	.disable	= wrhv_disable_irq,
	.enable		= wrhv_enable_irq,
	.unmask		= wrhv_unmask_irq,
	.mask_ack	= wrhv_maskack_irq,
	.eoi		= wrhv_ack_irq,
};
#endif

unsigned long wrhv_calculate_cpu_khz(void)
{
	printk(KERN_DEBUG "WRHV: Timestamp Frequency %u Hz\n",
		wr_vb_config->stamp_freq);
	return wr_vb_config->stamp_freq / 1000;
}

irqreturn_t __weak wrhv_timer_interrupt(int irq, void *dev_id)
{
	static DEFINE_PER_CPU(long long, mark_offset);
	int cpu;
	long long ticks;
	int lost_jiffies = 0;
	struct pt_regs *regs = get_irq_regs();

	cpu = smp_processor_id();
	ticks = wr_vb_status->tick_count;
	ticks -= per_cpu(mark_offset,cpu);
	lost_jiffies = ticks - 1;
	per_cpu(mark_offset,cpu) = wr_vb_status->tick_count;

	do {
		if (!smp_processor_id()) {
			write_seqlock(&xtime_lock);
			do_timer(1);
			write_sequnlock(&xtime_lock);
		}
		update_process_times(user_mode(regs));
		profile_tick(CPU_PROFILING);
		if (lost_jiffies > (2*HZ)) {
			printk(KERN_DEBUG "Time falling behind %d jiffies\n",
				lost_jiffies);
			break;
		}
	} while (--ticks > 0);

	if (lost_jiffies)
		account_steal_time(jiffies_to_cputime(lost_jiffies));
	return IRQ_HANDLED;
}

EXPORT_SYMBOL(vbi_vb_suspend);
EXPORT_SYMBOL(vbi_vb_write_mem);
EXPORT_SYMBOL(vbi_vb_find_board_config);
EXPORT_SYMBOL(vbi_vb_read_reg);
EXPORT_SYMBOL(vbi_vb_write_reg);
EXPORT_SYMBOL(vbi_vb_resume);

static int wrhv_suspend_valid(suspend_state_t state)
{
	return state == PM_SUSPEND_STANDBY || state == PM_SUSPEND_MEM;
}

static int wrhv_suspend_begin(suspend_state_t state)
{
	switch (state) {
		case PM_SUSPEND_STANDBY:
		case PM_SUSPEND_MEM:
			return 0;
		default:
			return -EINVAL;
	}
}

extern int softlockup_thresh;
static int softlockup_thresh_bk;
static void wrhv_save_softlockup_thresh(void)
{
	softlockup_thresh_bk = softlockup_thresh;
	softlockup_thresh = 0;
}

static void wrhv_restore_softlockup_thresh(void)
{
	softlockup_thresh = softlockup_thresh_bk;
}

static int wrhv_suspend_enter(suspend_state_t state)
{
	vbi_vb_suspend(VBI_BOARD_ID_GET(), 0);
	return 0;
}

#ifdef CONFIG_HOTPLUG_CPU
static cpumask_var_t wrhv_cpus;

static int wrhv_save_cpus(void)
{
	int cpu, first_cpu, error;

	cpumask_clear(wrhv_cpus);

	if (num_online_cpus() == 1)
		return 0;

	first_cpu = cpumask_first(cpu_online_mask);

	for_each_online_cpu(cpu) {
		if (cpu == first_cpu)
			continue;
		error = cpu_down(cpu);
		if (!error)
			cpumask_set_cpu(cpu, wrhv_cpus);
		else {
			printk(KERN_ERR "wrhv_bring_nonboot_cpu CPU%d down: %d\n",
				cpu, error);
			break;
		}
	}

	if (!error)
		BUG_ON(num_online_cpus() > 1);
	else
		printk(KERN_ERR "Non-boot CPUs are not disabled\n");

	return error;
}

static void wrhv_restore_cpus(void)
{
	int cpu, error;

	if (cpumask_empty(wrhv_cpus))
		return;

	for_each_cpu(cpu, wrhv_cpus) {
		error = cpu_up(cpu);
		if (!error) {
			printk("CPU%d is up\n", cpu);
			continue;
		}
		printk(KERN_WARNING "Error bringing CPU%d up: %d\n", cpu, error);
	}

}

#else
static inline void wrhv_save_cpus(void)
{}

static inline void wrhv_restore_cpus(void)
{}

#endif

static int wrhv_suspend_prepare(void)
{
	wrhv_save_softlockup_thresh();
	wrhv_save_cpus();

	return 0;
}

static int wrhv_suspend_prepare_late(void)
{
	return 0;
}

static void wrhv_suspend_end(void)
{
	wrhv_restore_cpus();
	wrhv_restore_softlockup_thresh();
}


static struct platform_suspend_ops wrhv_suspend_ops = {
	.valid = wrhv_suspend_valid,
	.begin = wrhv_suspend_begin,
	.prepare = wrhv_suspend_prepare,
	.prepare_late = wrhv_suspend_prepare_late,
	.enter = wrhv_suspend_enter,
	.end = wrhv_suspend_end,
};

int __weak wrhv_arch_late_init(void)
{
	return 0;
}

#define WINDRIVER_NAME "windriver"
/* sysfs ksets */
struct kset *windriver_kset;
EXPORT_SYMBOL(windriver_kset);
static int wrhv_init_sysfs(void)
{
	/* Create parent windriver kset */
	windriver_kset = kset_create_and_add(WINDRIVER_NAME, NULL, NULL);
	if (!windriver_kset)
		return -ENOMEM;

	return 0;
}

/* debugfs dentry's */
struct dentry *windriver_dentry;
EXPORT_SYMBOL(windriver_dentry);
static int wrhv_init_debugfs(void)
{
	windriver_dentry = debugfs_create_dir(WINDRIVER_NAME, NULL);
	if (!windriver_dentry)
		return -ENODEV;

	return 0;
}

static char wrhv_shell[] = "wrhv_shell";
static ssize_t hysh_show(struct kobject *kobj, struct attribute *attr,
	char *buf)
{
	sprintf(buf, "0\n");
	return 2;
}

static ssize_t hysh_action(struct kobject *kobj, struct attribute *attr,
	const char *buf, size_t len)
{
	wrhv_save_softlockup_thresh();
	wrhv_save_cpus();
	vbi_shell_start_debug(0);
	wrhv_restore_cpus();
	wrhv_restore_softlockup_thresh();
	return len;
}

static struct kobj_attribute hysh_attr = __ATTR(wrhv_shell, 0644, hysh_show, hysh_action);

static int wrhv_hysh_init_sysfs(void)
{
	int ret;
	ret = sysfs_create_file(&windriver_kset->kobj, &hysh_attr.attr);
	if (ret) {
		printk(KERN_ERR "wrhv_sysfs: create %s fail, ret = %d\n",
			wrhv_shell, ret);
		return -ENOMEM;
	}

	return 0;
}

int __init wrhv_late_init(void)
{
	wrhv_arch_late_init();
	suspend_set_ops(&wrhv_suspend_ops);
	wrhv_init_sysfs();
	wrhv_init_debugfs();
	wrhv_hysh_init_sysfs();
	return 0;
}
late_initcall(wrhv_late_init);
