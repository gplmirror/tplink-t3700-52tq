/*
 * ns.c - hypervisor naming service, client side interface
 *
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 */

/*
DESCRIPTION

This module implements a the client side of a simple naming service for the
internal thread managers of the hypervisor.

The interfaces formulate a message request and send it to the naming service
for processing.

*/

#include <linux/string.h>
#include <vbi/vbi.h>
#include <vbi/private.h>

#ifdef DEBUG
#define DEBUGM(fmt, args...)    printk(fmt, ##args)
#else
#define DEBUGM(fmt, args...)
#endif

/*
 * vbi_ns_register - register a service with the naming system
 *
 * This routine registers us as the provider of the specified service.
 * A message for the request is formulated and sent off to the name service
 * manager for processing.
 *
 */
int32_t vbi_ns_register(char *name, uint32_t revision)
{
	if (name == NULL)
		return -1;

	return vbi_ns_op(VBI_NS_REGISTER, name, revision, NULL);
}

/*
 * vbi_ns_unregister - un-register a service with the naming system
 *
 * This routine removes us as the provider of the specified service.
 * A message for the request is formulated and sent off to the name service
 * manager for processing.
 *
 */
int32_t vbi_ns_unregister(char *name, uint32_t revision)
{
	if (name == NULL)
		return -1;

	return vbi_ns_op(VBI_NS_UNREGISTER, name, revision, NULL);
}

/*
 * vbi_ns_lookup - look up a service provider using the naming system
 *
 * This routine uses the naming system to look up the context id of the
 * provider of the specified service.  A message for the request is
 * formulated and sent off to the name service manager for processing.
 *
 */
int32_t vbi_ns_lookup(char *name, uint32_t revision, VBI_NS_HANDLE *handle)
{
	if (name == NULL || handle == NULL)
		return -1;

	return vbi_ns_op(VBI_NS_LOOKUP , name, revision, handle);
}
