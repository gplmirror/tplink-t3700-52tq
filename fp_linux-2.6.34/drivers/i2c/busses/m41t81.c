/*
 * Copyright (C) 2009 Broadcom Corporation
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 * 
 * 
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/i2c.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/rtc.h>          /* get the user-level API */
#include <linux/init.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/time.h>

#include <linux/bcd.h>
#include <linux/types.h>
#include <linux/time.h>

#include <asm/time.h>
#include <asm/addrspace.h>
#include <asm/io.h>

#include <typedefs.h>
#include <osl.h>
#include <siutils.h>
#include <chipc_i2c.h>

/* M41T81 definitions */

/*
 * Register bits
 */

#define M41T81REG_SC_ST	0x80	/* stop bit */
#define M41T81REG_HR_CB	0x40	/* century bit */
#define M41T81REG_HR_CEB	0x80	/* century enable bit */
#define M41T81REG_CTL_S	0x20	/* sign bit */
#define M41T81REG_CTL_FT	0x40	/* frequency test bit */
#define M41T81REG_CTL_OUT	0x80	/* output level */
#define M41T81REG_WD_RB0	0x01	/* watchdog resolution bit 0 */
#define M41T81REG_WD_RB1	0x02	/* watchdog resolution bit 1 */
#define M41T81REG_WD_BMB0	0x04	/* watchdog multiplier bit 0 */
#define M41T81REG_WD_BMB1	0x08	/* watchdog multiplier bit 1 */
#define M41T81REG_WD_BMB2	0x10	/* watchdog multiplier bit 2 */
#define M41T81REG_WD_BMB3	0x20	/* watchdog multiplier bit 3 */
#define M41T81REG_WD_BMB4	0x40	/* watchdog multiplier bit 4 */
#define M41T81REG_AMO_ABE	0x20	/* alarm in "battery back-up mode" enable bit */
#define M41T81REG_AMO_SQWE	0x40	/* square wave enable */
#define M41T81REG_AMO_AFE	0x80	/* alarm flag enable flag */
#define M41T81REG_ADT_RPT5	0x40	/* alarm repeat mode bit 5 */
#define M41T81REG_ADT_RPT4	0x80	/* alarm repeat mode bit 4 */
#define M41T81REG_AHR_RPT3	0x80	/* alarm repeat mode bit 3 */
#define M41T81REG_AHR_HT	0x40	/* halt update bit */
#define M41T81REG_AMN_RPT2	0x80	/* alarm repeat mode bit 2 */
#define M41T81REG_ASC_RPT1	0x80	/* alarm repeat mode bit 1 */
#define M41T81REG_FLG_AF	0x40	/* alarm flag (read only) */
#define M41T81REG_FLG_WDF	0x80	/* watchdog flag (read only) */
#define M41T81REG_SQW_RS0	0x10	/* sqw frequency bit 0 */
#define M41T81REG_SQW_RS1	0x20	/* sqw frequency bit 1 */
#define M41T81REG_SQW_RS2	0x40	/* sqw frequency bit 2 */
#define M41T81REG_SQW_RS3	0x80	/* sqw frequency bit 3 */


/*
 * Register numbers
 */

#define M41T81REG_TSC	0x00	/* tenths/hundredths of second */
#define M41T81REG_SC	0x01	/* seconds */
#define M41T81REG_MN	0x02	/* minute */
#define M41T81REG_HR	0x03	/* hour/century */
#define M41T81REG_DY	0x04	/* day of week */
#define M41T81REG_DT	0x05	/* date of month */
#define M41T81REG_MO	0x06	/* month */
#define M41T81REG_YR	0x07	/* year */
#define M41T81REG_CTL	0x08	/* control */
#define M41T81REG_WD	0x09	/* watchdog */
#define M41T81REG_AMO	0x0A	/* alarm: month */
#define M41T81REG_ADT	0x0B	/* alarm: date */
#define M41T81REG_AHR	0x0C	/* alarm: hour */
#define M41T81REG_AMN	0x0D	/* alarm: minute */
#define M41T81REG_ASC	0x0E	/* alarm: second */
#define M41T81REG_FLG	0x0F	/* flags */
#define M41T81REG_SQW	0x13	/* square wave register */

#define M41T81_CCR_ADDRESS	0x68

#define EPOCH			2000
#define SYS_EPOCH		1900
#define err(format, arg...)     printk(KERN_ERR ": " format , ## arg)

static int      m41t81_use_count = 0;
static int      rtc_read_proc(char *page, char **start, off_t off, int count,
                int *eof, void *data);

static int m41t81_read(int devaddr, char *b)
{
    uint8_t buf[1];
    int err = 0;

    int chan = 0;
    int slaveaddr = M41T81_CCR_ADDRESS;
    /*
     * Start the command
     */

    buf[0] = devaddr;
    err = ksi2c_write(chan, slaveaddr, buf, 1);
    if (err < 0) {
       return err;
    }

    /*
     * Read the data byte
     */

    err = ksi2c_read(chan, slaveaddr, buf, 1);
    if (err < 0) {
	    return err;
    }

    *b = buf[0];

    return err;
}

static int m41t81_write(int devaddr, int b)
{
    uint8_t buf[2];
    int err;
    int polls;
    int chan = 0;
    int slaveaddr = M41T81_CCR_ADDRESS;
    /*
     * Start the command.  Keep pounding on the device until it
     * submits or the timer expires, whichever comes first.  The
     * datasheet says writes can take up to 10ms, so we'll give it 500.
     */

    buf[0] = devaddr;
    buf[1] = b;

    err = ksi2c_write(chan, slaveaddr, buf, 2);
    if (err < 0) {
        return err;
    }

    /*
     * Pound on the device with a current address read
     * to poll for the write complete
     */
    err = -1;

	for (polls = 0; polls < 300; polls++) {
		err = ksi2c_read(chan, slaveaddr, buf, 1);
		if (err == 0)
		break;
	}

    return err;
}

int m41t81_set_time(unsigned long t)
{
    struct rtc_time tm;
    unsigned long flags;
    unsigned char buffer;

    rtc_time_to_tm(t, &tm);

    /*
     * Note the write order matters as it ensures the correctness.
     * When we write sec, 10th sec is clear.  It is reasonable to
     * believe we should finish writing min within a second.
     */

    spin_lock_irqsave(&rtc_lock, flags);
    tm.tm_sec = bin2bcd(tm.tm_sec);
    m41t81_write(M41T81REG_SC, tm.tm_sec);

    tm.tm_min = bin2bcd(tm.tm_min);
    m41t81_write(M41T81REG_MN, tm.tm_min);

    tm.tm_hour = bin2bcd(tm.tm_hour);
    m41t81_read(M41T81REG_HR, &buffer);
    buffer &= 0xc0;
    tm.tm_hour = (tm.tm_hour & 0x3f) | buffer;
    m41t81_write(M41T81REG_HR, tm.tm_hour);

    /* tm_wday starts from 0 to 6 */
    if (tm.tm_wday == 0)
	 tm.tm_wday = 7;
    tm.tm_wday = bin2bcd(tm.tm_wday);
    m41t81_write(M41T81REG_DY, tm.tm_wday);

    tm.tm_mday = bin2bcd(tm.tm_mday);
    m41t81_write(M41T81REG_DT, tm.tm_mday);

    /* tm_mon starts from 0, *ick* */
    tm.tm_mon++;
    tm.tm_mon = bin2bcd(tm.tm_mon);
    m41t81_write(M41T81REG_MO, tm.tm_mon);

    /* we don't do century, everything is beyond 2000 */
    tm.tm_year %= 100;
    tm.tm_year = bin2bcd(tm.tm_year);
    m41t81_write(M41T81REG_YR, tm.tm_year);
    spin_unlock_irqrestore(&rtc_lock, flags);

    return 0;
}

unsigned long m41t81_get_time(void)
{
	unsigned char byte;
	unsigned int year, mon, day, hour, min, sec, temp;
	unsigned long flags;

	/*
	 * min is valid if two reads of sec are the same.
	 */
	for (;;) {
		spin_lock_irqsave(&rtc_lock, flags);
		m41t81_read(M41T81REG_SC, &byte);
		byte &= ~(M41T81REG_SC_ST);
		sec = byte;
		m41t81_read(M41T81REG_MN, &byte);
		min = byte;
		m41t81_read(M41T81REG_SC, &byte);
		byte &= ~(M41T81REG_SC_ST);
		temp = byte;
		if (sec == temp)
			break;
		spin_unlock_irqrestore(&rtc_lock, flags);
	}
	m41t81_read(M41T81REG_HR, &byte);
	hour = byte & 0x3f;
	m41t81_read(M41T81REG_DT, &byte);
	day = byte;
	m41t81_read(M41T81REG_MO, &byte);
	mon = byte;
	m41t81_read(M41T81REG_YR, &byte);
	year = byte;
	spin_unlock_irqrestore(&rtc_lock, flags);

	sec = bcd2bin(sec);
	min = bcd2bin(min);
	hour = bcd2bin(hour);
	day = bcd2bin(day);
	mon = bcd2bin(mon);
	year = bcd2bin(year);

	year += 2000;

	return mktime(year, mon, day, hour, min, sec);
}

int m41t81_probe(void)
{
    unsigned char tmp;

    if (ksi2c_init() == KSI2C_ERR_NONE) {
	ksi2c_open(0, KS_I2C_BUS_PIO, KS_I2C_SPEED_DEFAULT);
    } else {
	return -1;
    }

    if (m41t81_read(M41T81REG_SC, &tmp) < 0) {
	return -1;
    }
    /* enable chip if it is not enabled yet */
    m41t81_write(M41T81REG_SC, tmp & 0x7f);
    tmp = 0xff;
    m41t81_read(M41T81REG_SC, &tmp);

	if (tmp != 0xff) {
		return 0;
	} else {
		return -1;
	}
}

int
m41t81_rtc_open(struct inode *minode, struct file *mfile)
{
	if (m41t81_probe() < 0) {
		return -ENXIO;
   }

	if (m41t81_use_count > 0) {
		return -EBUSY;
	}

	++m41t81_use_count;
	return 0;
}

int m41t81_rtc_release(struct inode *minode, struct file *mfile)
{
	--m41t81_use_count;
	return 0;
}

static loff_t
m41t81_rtc_llseek(struct file *mfile, loff_t offset, int origint)
{
	return -ESPIPE;
}

static int
m41t81_rtc_ioctl(struct inode *inode, struct file *file, unsigned int cmd,
               unsigned long arg)
{
	struct rtc_time rtc_tm;
	unsigned long time;

	switch (cmd) {
	case RTC_RD_TIME:       /* Read the time/date from RTC  */
		time = m41t81_get_time();
		rtc_time_to_tm(time, &rtc_tm);
		return copy_to_user((void *) arg, &rtc_tm, sizeof (rtc_tm)) ?
							-EFAULT : 0;
	case RTC_SET_TIME:      /* Set the RTC */ 
		if (!capable(CAP_SYS_TIME))
			return -EACCES;

		if (copy_from_user(&rtc_tm, (struct rtc_time *) arg,
							sizeof (struct rtc_time)))
			return -EFAULT; 

		rtc_tm_to_time(&rtc_tm, &time);
		return m41t81_set_time(time);

	default:
		return -EINVAL;
	}
}

static const struct file_operations m41t81_rtc_fops = {
		.owner		=	THIS_MODULE,
		.llseek		=	m41t81_rtc_llseek,
		.ioctl		=	m41t81_rtc_ioctl,
		.open		=	m41t81_rtc_open,
		.release	=	m41t81_rtc_release,
};

static struct miscdevice m41t81_miscdev = {
		.minor		=	RTC_MINOR,
		.name		=	"rtc",
		.fops		=	&m41t81_rtc_fops,
};

static int __init m41t81_rtc_init(void)
{
	int ret;

	ret = misc_register(&m41t81_miscdev);
	if (ret) {
		err("Register misc driver failed, errno is %d\n", ret);
		return ret;
	}

	create_proc_read_entry("rtc", 0, 0, rtc_read_proc, NULL);

	return 0;
}

static void __exit m41t81_rtc_exit(void)
{
	remove_proc_entry("driver/rtc", NULL);
	misc_deregister(&m41t81_miscdev);
}

module_init(m41t81_rtc_init);
module_exit(m41t81_rtc_exit);

/*
 * Info exported via "/proc/driver/rtc".
 */

static int
rtc_proc_output(char *buf)
{
	char *p;
	unsigned long time;
	struct rtc_time tm;

	time = m41t81_get_time();
	rtc_time_to_tm(time, &tm);

	p = buf;
	/*
	 * There is no way to tell if the luser has the RTC set for local
	 * time or for Universal Standard Time (GMT). Probably local though.
	 */
	p += sprintf(p, "rtc_time\t: %02d:%02d:%02d\n"
					"rtc_date\t: %04d-%02d-%02d\n"
					"rtc_epoch\t: %04d\n",
					tm.tm_hour, tm.tm_min, tm.tm_sec,
					tm.tm_year + SYS_EPOCH, tm.tm_mon + 1, tm.tm_mday, EPOCH);


	return p - buf;
}

static int
rtc_read_proc(char *page, char **start, off_t off,
                int count, int *eof, void *data)
{
	int len = rtc_proc_output(page);
	if (len <= off + count)
		*eof = 1;
	*start = page + off;
	len -= off;
	if (len > count)
		len = count;
	if (len < 0)
		len = 0;
	return len;
}

MODULE_DESCRIPTION("ST Microelectronics M41T81 RTC I2C Client Driver");
MODULE_LICENSE("GPL");
