/*
 * IDT1338 rtc class driver
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *  NOTE: This driver has restrictions to work with MIPS BCM53000 platform only
 */
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/rtc.h>
#include <linux/i2c.h>
#include <linux/bcd.h>
#include <linux/slab.h>

#include <osl.h>
#include <siutils.h>
#include <chipc_i2c.h>

/* Using IDT1338 driver for IDT 1338 device, but register offsets are different */
#define IDT1338REG_SC    0x00    /* seconds */
#define IDT1338REG_MN    0x01    /* minute */
#define IDT1338REG_HR    0x02    /* hour/century */
#define IDT1338REG_DY    0x03    /* day of week */
#define IDT1338REG_DT    0x04    /* date of month */
#define IDT1338REG_MO    0x05    /* month */
#define IDT1338REG_YR    0x06    /* year */
#define IDT1338REG_SC_ST 0x80    /* stop bit */
#define IDT1338_CCR_ADDRESS      0x68
extern int keystone_xfer(struct i2c_adapter *adap, struct i2c_msg *msgs, int num);

static const struct i2c_device_id idt1338_id[] = {
	{ "rtc_idt1338", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, idt1338_id);

struct idt1338 {
	struct rtc_device *rtc;
};

static struct i2c_driver idt1338_driver;

static int idt1338_read(int devaddr, char *b)
{
    uint8_t buf[1];
    int err = 0;

    int chan = 0;
    int slaveaddr = IDT1338_CCR_ADDRESS;
    /*
     * Start the command
     */

    buf[0] = devaddr;
    err = ksi2c_write(chan, slaveaddr, buf, 1);
    if (err < 0) {
       return err;
    }

    /*
     * Read the data byte
     */

    err = ksi2c_read(chan, slaveaddr, buf, 1);
    if (err < 0) {
	    return err;
    }

    *b = buf[0];

    return err;
}

static int idt1338_write(int devaddr, int b)
{
    uint8_t buf[2];
    int err;
    int polls;
    int chan = 0;
    int slaveaddr = IDT1338_CCR_ADDRESS;
    /*
     * Start the command.  Keep pounding on the device until it
     * submits or the timer expires, whichever comes first.  The
     * datasheet says writes can take up to 10ms, so we'll give it 500.
     */

    buf[0] = devaddr;
    buf[1] = b;

    err = ksi2c_write(chan, slaveaddr, buf, 2);
    if (err < 0) {
        return err;
    }

    /*
     * Pound on the device with a current address read
     * to poll for the write complete
     */
    err = -1;

	for (polls = 0; polls < 300; polls++) {
		err = ksi2c_read(chan, slaveaddr, buf, 1);
		if (err == 0)
		break;
	}

    return err;
}

static int idt1338_set_time(struct device *dev, struct rtc_time *tm)
{
    unsigned char buffer;


    /*
     * Note the write order matters as it ensures the correctness.
     * When we write sec, 10th sec is clear.  It is reasonable to
     * believe we should finish writing min within a second.
     */

    tm->tm_sec = bin2bcd(tm->tm_sec);
    idt1338_write(IDT1338REG_SC, tm->tm_sec);

    tm->tm_min = bin2bcd(tm->tm_min);
    idt1338_write(IDT1338REG_MN, tm->tm_min);

    tm->tm_hour = bin2bcd(tm->tm_hour);
    idt1338_read(IDT1338REG_HR, &buffer);
    buffer &= 0xc0;
    tm->tm_hour = (tm->tm_hour & 0x3f) | buffer;
    idt1338_write(IDT1338REG_HR, tm->tm_hour);

    /* tm_wday starts from 0 to 6 */
    if (tm->tm_wday == 0)
	 tm->tm_wday = 7;
    tm->tm_wday = bin2bcd(tm->tm_wday);
    idt1338_write(IDT1338REG_DY, tm->tm_wday);

    tm->tm_mday = bin2bcd(tm->tm_mday);
    idt1338_write(IDT1338REG_DT, tm->tm_mday);

    /* tm_mon starts from 0, *ick* */
    tm->tm_mon++;
    tm->tm_mon = bin2bcd(tm->tm_mon);
    idt1338_write(IDT1338REG_MO, tm->tm_mon);

    /* we don't do century, everything is beyond 2000 */
    tm->tm_year %= 100;
    tm->tm_year = bin2bcd(tm->tm_year);
    idt1338_write(IDT1338REG_YR, tm->tm_year);

    return 0;
}

static int idt1338_get_time(struct device *dev, struct rtc_time *tm)
{
	unsigned char byte;
	unsigned int year, mon, wday, day, hour, min, sec, temp;

	/*
	 * min is valid if two reads of sec are the same.
	 */
	for (;;) {
		idt1338_read(IDT1338REG_SC, &byte);
		byte &= ~(IDT1338REG_SC_ST);
		sec = byte;
		idt1338_read(IDT1338REG_MN, &byte);
		min = byte;
		idt1338_read(IDT1338REG_SC, &byte);
		byte &= ~(IDT1338REG_SC_ST);
		temp = byte;
		if (sec == temp)
			break;
	}
	idt1338_read(IDT1338REG_HR, &byte);
	hour = byte & 0x3f;
        idt1338_read(IDT1338REG_DY, &byte);
        wday = byte&0x7;
	idt1338_read(IDT1338REG_DT, &byte);
	day = byte;
	idt1338_read(IDT1338REG_MO, &byte);
	mon = byte;
	idt1338_read(IDT1338REG_YR, &byte);
	year = byte;

	tm->tm_sec = bcd2bin(sec);
	tm->tm_min = bcd2bin(min);
	tm->tm_hour = bcd2bin(hour);
	tm->tm_wday = bcd2bin(wday)-1;
	tm->tm_mday = bcd2bin(day);
	tm->tm_mon = bcd2bin(mon)-1;
	tm->tm_year = 100+bcd2bin(year);
	return 0;
}

static const struct rtc_class_ops idt1338_ops = {
        .read_time      = idt1338_get_time,
        .set_time       = idt1338_set_time,
};

static int idt1338_probe(struct i2c_client *client)
{
	struct idt1338 *idt1338;
	int err = 0;

	if (!i2c_check_functionality(client->adapter,I2C_FUNC_I2C))
		return -ENODEV;

	idt1338 = kzalloc(sizeof(struct idt1338), GFP_KERNEL);
	if (!idt1338) 
		return -ENOMEM;

	i2c_set_clientdata(client, idt1338);

	idt1338->rtc = rtc_device_register(idt1338_driver.driver.name,
					   &client->dev, &idt1338_ops, 
					   THIS_MODULE);
	if (IS_ERR(idt1338->rtc)) {
		err = PTR_ERR(idt1338->rtc);
		kfree (idt1338);
		return err;
	}
	return 0;
}

static int idt1338_attach(struct i2c_adapter *adapter)
{
        struct i2c_board_info info;
        struct i2c_client *client;
        char *name,buffer[1];
	static int rtcInitialzed=0;
	struct i2c_msg msgs;

	if(rtcInitialzed)
		return 0;
        name="rtc_idt1338";
        memset(&info, 0, sizeof(struct i2c_board_info));
        info.addr = IDT1338_CCR_ADDRESS;
        info.platform_data=name;
        strlcpy(info.type,"rtc_idt1338", I2C_NAME_SIZE);
	if(adapter==NULL)
	{
		adapter=i2c_get_adapter(0);
		if(adapter==NULL)
	   	   return -1;
	}
        client = i2c_new_device(adapter, &info);
        if (client == NULL) {
                printk(KERN_ERR "i2c_new_device: failed to attach to i2c\n");
                return -1;
        }
       /*
         * Let i2c-core delete that device on driver removal.
         * This is safe because i2c-core holds the core_lock mutex for us.
         */
        list_add_tail(&client->detected, &idt1338_driver.clients);
	rtcInitialzed=1;
	idt1338_probe(client);
	/* reading one byte to ensure ksi2c_init is called and ready for access */
        msgs.addr=info.addr;
	msgs.flags=1; /* read */
	msgs.len=1;
	msgs.buf=&buffer[0];	
	keystone_xfer(adapter,&msgs,1);
        return 0;
}

static int idt1338_remove(struct i2c_client *client)
{
	struct idt1338 *idt1338 = i2c_get_clientdata(client);
	if (idt1338->rtc)
		rtc_device_unregister(idt1338->rtc);

	kfree(idt1338);
	return 0;
}

static struct i2c_driver idt1338_driver = {
	.driver	= {
		.name	= "rtc_idt1338",
	},
	.attach_adapter = idt1338_attach,
	.remove	= __devexit_p(idt1338_remove),
	.id_table = idt1338_id,
};

static int __init idt1338_init(void)
{
	int err;

	err = i2c_add_driver(&idt1338_driver);
	if (err)
		printk(KERN_ERR "IDT1338 RTC init err=%d\n", err);
	return err;
}

static void __exit idt1338_exit(void)
{
	i2c_del_driver(&idt1338_driver);
}

MODULE_DESCRIPTION("IDT 1338 RTC driver");
MODULE_LICENSE("GPL");

module_init(idt1338_init);
module_exit(idt1338_exit);
