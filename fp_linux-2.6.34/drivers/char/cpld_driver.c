/*
 * NAS Kernel Module
 * Copyright (c) 2010, Celestica Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/**************************************************************************
 *version control
 * V1.0
 * Beck implement this kernel module based on API Specification 0.3.
 * All the LED,SystemMonitor,Button,LCM WatchDog driver will be inserted as one module.
 * So it will not bring much impact to current Linux OS from customer.
 ***************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/cdev.h>
#include <linux/workqueue.h>
#include <linux/poll.h>
#include <linux/mutex.h>
#include <linux/fs.h>
#include <linux/jiffies.h>
#include <linux/miscdevice.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/delay.h>

#include <asm/uaccess.h>   /* copy_*_user */
#include <asm/io.h>

#include "cpld_driver.h"
#include <linux/io.h>
#include <linux/of.h>

MODULE_AUTHOR( "Beck He <bhe@Celestica.com>" );
MODULE_DESCRIPTION( "CPLD Driver" );
MODULE_LICENSE( "GPL" );

#define NAME         "cpld_driver"
#define CPLD_DRIVER_MINOR 159

DEFINE_MUTEX(cpld_driver_mutex);
static DECLARE_WAIT_QUEUE_HEAD(cpld_wait_queue);
static int cpld_used = 0;
static struct class * cpld_driver_class;

#define  CONFIG_IMMR        0xffe00000
#define GPIO_BASE_ADDR   CONFIG_IMMR + 0xf000
#define GPDIR_OFFSET        0x00
#define GPODR_OFFSET        0x04
#define GPDAT_OFFSET        0x08
#define GPIER_OFFSET        0x0c
#define GPIMR_OFFSET        0x10
#define GPICR_OFFSET        0x14

#define WDI_GPIO_MASK     0x81000081 //gpio 7
#define CPLD_GPIO_TCK      0x00080000 //gpio 12
#define CPLD_GPIO_TMS      0x00040000 //gpio 13
#define CPLD_GPIO_TDI       0x00020000 //gpio 14
#define CPLD_GPIO_TDO       0x00010000 //gpio 15

#define CPLD_BASE_ADDR    0xffb00000
#define UBOOT_OK_MASK     0x78
#define UBOOT_OK                0x50

#define CPLD1_VERSION_REG	0x00
#define CPLD2_VERSION_REG	0x00
#define CPLD3_VERSION_REG	0x04
#define CPLD_ID_MASK	0x03
#define CPLD_MINOR_VERSION	0x3f

static JTAGPort jtagdata;

static volatile void __iomem *gpio_base;
static void __iomem *cpld_base;
static void __iomem *led_cpld_base;
static void __iomem *lpsram_base;
#define LPSRAM_MAX_SIZE 128*1024

#define CPLD_DETECT_INTERVAL   1000 /*1s interval*/

static char readconut = 0;
static char writeconut = 0;
static char conut1 = 0;
static char conut2 = 0;

static int CPLDOpen( struct inode *inode, struct file *file )
{
#ifdef CPLD_DRIVER_DEBUG
	printk( KERN_ALERT NAME ":  CPLD Device Open");
#endif
	if (cpld_used == 1)
		return -EBUSY;

	mutex_lock(&cpld_driver_mutex);
	cpld_used = 1;
	mutex_unlock(&cpld_driver_mutex);

	return 0;
}

static int  CPLDClose(struct inode *inode, struct file *file)
{
#ifdef CPLD_DRIVER_DEBUG
	printk( KERN_ALERT NAME ": CPLD Device Close");
#endif
	cpld_used = 0;

	return 0;
}

static wait_queue_head_t queue;
static unsigned int CPLDPoll(struct file *filp, poll_table *wait)
{
	poll_wait(filp, &queue, wait);

	return 0;
}

static int GpioRead(struct file *filp, char __user *buf, size_t size, loff_t *f)
{
	int retval;
	int i;
	char *p,*q;
	unsigned long offset;


	mutex_lock(&cpld_driver_mutex);

	offset = *f;
	if ((size + offset) > LPSRAM_MAX_SIZE) {
		size = LPSRAM_MAX_SIZE - offset;
	} //add by lhx

	p = (char *)kzalloc(size, GFP_KERNEL); //change by lhx
	q = p;
	if (!p) {
		printk(KERN_ALERT NAME "kmalloc error!\n");
		mutex_unlock(&cpld_driver_mutex);
		return -EFAULT;
	}
	for (i = 0; i < size; i++)
		*p++ = readb(lpsram_base + offset + i);	//change by lhx
	retval = copy_to_user(buf, q, size * sizeof(char));
	if (retval) {
		printk(KERN_ALERT "copy_to_user error!\n");
	}

	mutex_unlock(&cpld_driver_mutex);
	return size;
}


static int GpioWrite(struct file *filp, const char __user * buf, size_t size, loff_t * f)
{
	int retval;
	int i;
	char *p;
	unsigned long offset;

	mutex_lock(&cpld_driver_mutex);
	offset = *f;
	if ((size + offset) > LPSRAM_MAX_SIZE) {
		size = LPSRAM_MAX_SIZE - offset;
	} //change by lhx	

	p = (char *)kzalloc(size, GFP_KERNEL);
	if (!p) {
		printk(KERN_ALERT NAME "kmalloc error!\n");
		mutex_unlock(&cpld_driver_mutex);
		return -EFAULT;
	}
	retval = copy_from_user(p, (char *)buf, size * sizeof(char));
	if (retval) {
		printk(KERN_ALERT "copy_to_user error!\n");
		mutex_unlock(&cpld_driver_mutex);
		return( -EFAULT );
	}
	for (i = 0; i < size; i++)
		writeb(*p++, lpsram_base + offset + i);

	mutex_unlock(&cpld_driver_mutex);
	return size;
}

static loff_t CPLDLseek(struct file *file, loff_t offset, int orig)
{
	switch (orig) {
	case SEEK_SET:
		break;
	case SEEK_CUR:
		offset += file->f_pos;
		break;
	case SEEK_END:
		offset += LPSRAM_MAX_SIZE;
		break;
	default:
		return -EINVAL;
	}

	if (offset >= 0 && offset <= LPSRAM_MAX_SIZE){

		return file->f_pos = offset;
	}

	return -EINVAL;
}

/*
 * read_sfp: read sfp registers
 * portID: port number(17-18)
 * devAddr: device addr(0,1,2,3)
 * reg: register
 * data: store read data
 * len: read data length
 * return: 0: success , -1: fail
 */
int read_sfp(int portID, char devAddr, char reg, char *data, int len)
{
	int count;
	char byte;
	void *temp;
	char portid, opcode, devaddr, cmdbyte0, ssrr, writedata, readdata, sfppscr;
	char qsfpaddr = 0xa0;

	if (led_cpld_base == NULL) {
		printk(KERN_ALERT NAME "ioremap error!\n");
		iounmap(led_cpld_base);
		return -1;
	}
	if ((reg + len) > 255) {
		printk(KERN_ALERT "read data len not larger than 255!\n");
		return -1;
	}
	if ((portID >= 1) && (portID <= 16)) {
		portid = 0x10;
		opcode = 0x12;
		devaddr = 0x14;
		cmdbyte0 = 0x18;
		ssrr = 0x1e;
		writedata = 0x20;
		readdata = 0x30;
		sfppscr = 0x84;
		if (portID > 8) {
			sfppscr = 0x86;
		}
	} else if ((portID >= 17) && (portID <= 32)) {
		portid = 0x40;
		opcode = 0x42;
		devaddr = 0x44;
		cmdbyte0 = 0x48;
		ssrr = 0x4e;
		writedata = 0x50;
		readdata = 0x60;
		sfppscr = 0xc4;
		if (portID > 24) {
			sfppscr = 0xc6;
		}
	} else {
		printk(KERN_ALERT "sfp num be sure in 1-32\n");
		return -1;
	}

	if ((readb(led_cpld_base + sfppscr) & (0x01 << ((portID - 1) % 8))) == (0x01 << ((portID - 1) % 8))) {
			printk(KERN_ALERT "Not sfp device!\n");
			return -1;
	}
	while ((readb(led_cpld_base + ssrr) & 0x40));
	if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ALERT "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
	}

	byte = 0x40 + portID;
	writeb(byte, led_cpld_base + portid);
	writeb(reg, led_cpld_base + cmdbyte0);
	while (len > 0) {
		count = (len >= 8) ? 8 : len;
		len -= count;
		byte = count * 16 + 1;
		writeb(byte, led_cpld_base + opcode);
		qsfpaddr |= 0x01;
		writeb(qsfpaddr, led_cpld_base + devaddr);

		while ((readb(led_cpld_base + ssrr) & 0x40)){
			udelay(100);
		}
		udelay(200);

		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ALERT "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
		temp = led_cpld_base + readdata;
		while (count-- > 0) {
			*(data++) = readb(temp);
			temp += 2;
		}
		if (len > 0) {
			reg += 0x08;
			writeb(reg, led_cpld_base + cmdbyte0);
		}

	}

	return 0;
}
EXPORT_SYMBOL(read_sfp);

/*
 * write_sfp: write sfp registers
 * portID: port number(17-18)
 * devAddr: device addr(0,1,3,4)
 * reg: register
 * data: store read data
 * len: write data length
 * return: 0: success , -1: fail
 */
int write_sfp(int portID, char devAddr, char reg, char *data, int len)
{
	int count;
	char byte;
	void *temp;
	char portid, opcode, devaddr, cmdbyte0, ssrr, writedata, readdata, sfppscr;
	char qsfpaddr = 0xa0;

	if (led_cpld_base == NULL) {
		printk(KERN_ALERT NAME "ioremap error!\n");
		iounmap(led_cpld_base);
		return -1;
	}
	if ((reg + len) > 255) {
		printk(KERN_ALERT "write data len not larger than 255!\n");
		return -1;
	}
	if ((portID >= 1) && (portID <= 16)) {
		portid = 0x10;
		opcode = 0x12;
		devaddr = 0x14;
		cmdbyte0 = 0x18;
		ssrr = 0x1e;
		writedata = 0x20;
		readdata = 0x30;
		sfppscr = 0x84;
		if (portID > 8) {
			sfppscr = 0x86;
		}
	} else if ((portID >= 17) && (portID <= 32)) {
		portid = 0x40;
		opcode = 0x42;
		devaddr = 0x44;
		cmdbyte0 = 0x48;
		ssrr = 0x4e;
		writedata = 0x50;
		readdata = 0x60;
		sfppscr = 0xc4;
		if (portID > 24) {
			sfppscr = 0xc6;
		}
	} else {
		printk(KERN_ALERT "sfp num be sure in 1-32\n");
		return -1;
	}

	if ((readb(led_cpld_base + sfppscr) & (0x01 << ((portID - 1) % 8))) == (0x01 << ((portID - 1) % 8))) {
			printk(KERN_ALERT "Not sfp device!\n");
			return -1;
	}
	while ((readb(led_cpld_base + ssrr) & 0x40));
	if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ALERT "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
	}
	
	byte = 0x40 + portID;
	writeb(byte, led_cpld_base + portid);
	writeb(reg, led_cpld_base + cmdbyte0);
	while (len > 0) {
		count = (len >= 8) ? 8 : len;
		len -= count;
		byte = (count << 4) + 1;
		writeb(byte, led_cpld_base + opcode);
		temp = led_cpld_base + writedata;
		while (count-- > 0) {
			writeb(*(data++), temp);
			temp += 0x02;
		}
		qsfpaddr&= 0xfe;
		writeb(qsfpaddr, led_cpld_base + devaddr);
	
		while ((readb(led_cpld_base + ssrr) & 0x40)){
			udelay(100);
		}
		udelay(5000);
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ALERT "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
		if (len > 0) {
			reg += 0x08;
			writeb(reg, led_cpld_base + cmdbyte0);
		}
	}

	return 0;
}
EXPORT_SYMBOL(write_sfp);

unsigned char read_cpld(char *cpldID, int reg)
{
	void __iomem *cpld_addr = NULL;
	unsigned char ret;

	mutex_lock(&cpld_driver_mutex);
	if (strcmp(cpldID, "reset") == 0) {
		cpld_addr = cpld_base;
	}
	else if (strcmp(cpldID, "led") == 0) {
		cpld_addr = led_cpld_base;
	}
	ret = readb(cpld_addr + reg);
	mutex_unlock(&cpld_driver_mutex);

	return ret;
}
EXPORT_SYMBOL(read_cpld);

int write_cpld(char *cpldID, int reg, char value)
{
	void __iomem *cpld_addr = NULL;

	mutex_lock(&cpld_driver_mutex);
	if (strcmp(cpldID, "reset") == 0) {
		cpld_addr = cpld_base;
	}
	else if (strcmp(cpldID, "led") == 0) {
		cpld_addr = led_cpld_base;
	}
	writeb(value, cpld_addr + reg);
	mutex_unlock(&cpld_driver_mutex);

	return 0;
}
EXPORT_SYMBOL(write_cpld);



static int CPLDIoctl( struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg )
{
	int err = 0;
	int retval = 0;
	RegData reg;
	u8 regvalue;
	u32 portvalue;
	SfpData sfpdata;
	/*
	** extract the type and number bitfields, and don't decode
	** wrong cmds: return ENOTTY (inappropriate ioctl) before access_ok()
	*/
	if ( _IOC_TYPE( cmd ) != SMALLSTONE_TYPE ) {
		return( -ENOTTY );
	}

	/*
	** the direction is a bitmask, and VERIFY_WRITE catches R/W
	** transfers. `Type' is user-oriented, while
	** access_ok is kernel-oriented, so the concept of "read" and
	** "write" is reversed
	*/
	if ( _IOC_DIR( cmd ) & _IOC_READ ) {
		err = !access_ok( VERIFY_WRITE,
				  ( void __user * )arg,
				  _IOC_SIZE( cmd ) );
	} else if ( _IOC_DIR( cmd ) & _IOC_WRITE ) {
		err = !access_ok( VERIFY_READ,
				  ( void __user * )arg,
				  _IOC_SIZE( cmd ) );
	}
	if ( err ) {
		return( -EFAULT );
	}

	switch (cmd) {

	case IOCTL_READ_REG: {

#ifdef CPLD_DRIVER_DEBUG
		printk( KERN_ALERT NAME "IOCTL_READ_REG received.\n" );
#endif

		mutex_lock(&cpld_driver_mutex);

		retval = copy_from_user( &reg,
					 ( const void * )arg,
					 sizeof( RegData) );

		if ( retval ) {
			mutex_unlock(&cpld_driver_mutex);
			return( -EFAULT );
		}
		if (reg.rw == 0) {
			if (reg.cpldID == 0) { //led cpld
				regvalue = readb(led_cpld_base + reg.regid);
			} else // reset cpld
			{
				regvalue = readb(cpld_base + reg.regid);
			}
			reg.value = regvalue;

			retval =  copy_to_user(
					  ( void  __user * )arg, &reg,
					  sizeof( RegData) );
		} else {
			printk( KERN_ALERT NAME "IOCTL_READ_REG error param.\n" );
		}

		mutex_unlock(&cpld_driver_mutex);
		break;
	}
	case IOCTL_WRITE_REG: {

#ifdef CPLD_DRIVER_DEBUG
		printk( KERN_ALERT NAME "IOCTL_WRITE_REG received.\n" );
#endif

		mutex_lock(&cpld_driver_mutex);

		retval = copy_from_user( &reg,
					 ( const void * )arg,
					 sizeof( RegData) );
		if ( retval ) {
			mutex_unlock(&cpld_driver_mutex);
			return( -EFAULT );
		}
		if (reg.rw == 1) {
			if (reg.cpldID == 0) { //led cpld
				writeb(reg.value, led_cpld_base + reg.regid );
			} else	//reset cpld
				writeb(reg.value, cpld_base + reg.regid );
		} else {
			printk( KERN_ALERT NAME "IOCTL_READ_REG error param.\n" );
		}
		mutex_unlock(&cpld_driver_mutex);
		break;
	}
	case IOCTL_READ_SFP_REG: {

#ifdef CPLD_DRIVER_DEBUG
		printk( KERN_ALERT NAME "IOCTL_READ_SFP_REG received.\n" );
#endif
		mutex_lock(&cpld_driver_mutex);
		retval = copy_from_user( &sfpdata,
					 ( const void * )arg,
					 sizeof( SfpData ));
		if ( retval ) {
			mutex_unlock(&cpld_driver_mutex);
			return( -EFAULT );
		}
		sfpdata.devAddr = readconut;
		conut1++;
		readconut = conut1 % 0x3;
/*		if (sfpdata.reg > 0x7f) {
			retval = write_sfp(sfpdata.portID, sfpdata.devAddr, 0x7f, &sfpdata.devAddr, 1);
			if (retval) {
				printk(KERN_ALERT NAME "write sfp page select reg error!\n");
				mutex_unlock(&cpld_driver_mutex);
				return (-EFAULT);
			}
		}*/
		if (sfpdata.rw == 0) {
			retval = read_sfp(sfpdata.portID, sfpdata.devAddr, sfpdata.reg, sfpdata.data, sfpdata.len);
			if (retval) {
				printk(KERN_ALERT NAME "read sfp error!\n");
				mutex_unlock(&cpld_driver_mutex);
				return (-EFAULT);
			}
			retval =  copy_to_user(
					  ( void  __user * )arg, &sfpdata,
					  sizeof( sfpdata) );
		} else {
			printk( KERN_ALERT NAME "IOCTL_READ_SFP_REG error param.\n" );
		}
		mutex_unlock(&cpld_driver_mutex);
		break;
	}
	case IOCTL_WRITE_SFP_REG: {
#ifdef CPLD_DRIVER_DEBUG
		printk( KERN_ALERT NAME "IOCTL_WRITE_REG received.\n" );
#endif
		mutex_lock(&cpld_driver_mutex);
		retval = copy_from_user( &sfpdata,
					 ( const void * )arg,
					 sizeof( sfpdata) );
		if ( retval ) {
			mutex_unlock(&cpld_driver_mutex);
			return( -EFAULT );
		}
		sfpdata.devAddr = writeconut;
		conut2++;
		writeconut = conut2 % 3;
		/*if (sfpdata.reg > 0x7f) {
			retval = write_sfp(sfpdata.portID, sfpdata.devAddr, 0x7f, &sfpdata.devAddr, 1);
			if (retval) {
				printk(KERN_ALERT NAME "write sfp page select reg error!\n");
				mutex_unlock(&cpld_driver_mutex);
				return (-EFAULT);
			}
		}*/

		if (sfpdata.rw == 1) {
			retval = write_sfp(sfpdata.portID, sfpdata.devAddr, sfpdata.reg, sfpdata.data, sfpdata.len);

			if (retval) {
				printk(KERN_ALERT NAME "write sfp error!\n");
				mutex_unlock(&cpld_driver_mutex);
				return (-EFAULT);
			}
		} else {
			printk( KERN_ALERT NAME "IOCTL_WRITE_SFP_REG error param.\n" );
		}

		mutex_unlock(&cpld_driver_mutex);
		break;
	}
	case IOCTL_READ_PORT: {

#ifdef CPLD_DRIVER_DEBUG
		printk( KERN_ALERT NAME "IOCTL_READ_PORT received.\n" );
#endif
		mutex_lock(&cpld_driver_mutex);
		retval = copy_from_user( &jtagdata,
					 ( const void * )arg,
					 sizeof( JTAGPort) );
		if ( retval ) {
			mutex_unlock(&cpld_driver_mutex);
			return( -EFAULT );
		}
		if (jtagdata.rw == 0) {
			portvalue = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
			jtagdata.value = (unsigned char)((portvalue & 0x00010000) ? 0x01 : 0x0);
			retval = copy_to_user(( void  *)arg, (void *) &jtagdata, sizeof(jtagdata));
		}
		else {
			printk(KERN_ALERT NAME "IOCTL_READ_JTAGDATA ERROR.\n");
		}
		mutex_unlock(&cpld_driver_mutex);
		break;
	}
	case IOCTL_WRITE_PORT: {
#ifdef CPLD_DRIVER_DEBUG
		printk( KERN_ALERT NAME "IOCTL_WRITE_PORT received.\n" );
#endif
		mutex_lock(&cpld_driver_mutex);
		retval = copy_from_user( &jtagdata,
					 ( const void * )arg,
					 sizeof( JTAGPort) );
		if ( retval ) {
			mutex_unlock(&cpld_driver_mutex);
			return( -EFAULT );
		}
		if (jtagdata.rw == 1) {
			portvalue = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
			switch (jtagdata.portid) {
			case 0x01: //pinTDI
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 17;
				else
					portvalue &= ~(0x1 << 17);
				//bit = 14;
				break;
			case 0x40: //pinTDO
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 16;
				else
					portvalue &= ~(0x1 << 16);
				//bit = 15;
				break;
			case 0x02: //pinTCK
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 19;
				else
					portvalue &= ~(0x1 << 19);
				//bit = 12;
				break;
			case 0x04: //pinTMS
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 18;
				else
					portvalue &= ~(0x1 << 18);
				//bit = 13;
				break;
			default:
				break;
			}
			*(volatile u32 *)(gpio_base + GPDAT_OFFSET) = portvalue;
		} else {
			printk(KERN_ALERT NAME "IOCTL_WRITE_JATGDATA ERROR.\n");
		}
		mutex_unlock(&cpld_driver_mutex);
		break;
	}

	default:  /* redundant, as cmd was checked against MAXNR */
		return( -ENOTTY );
	}
	return retval;
}

/*
** Kernel device registration data structures.
*/
static const struct file_operations fops = {
	.owner		= THIS_MODULE,
	.open		= CPLDOpen,
	.release             = CPLDClose,
	.read		= GpioRead,
	.write		= GpioWrite,
	.llseek		= CPLDLseek,
	.ioctl	              = CPLDIoctl,
	.poll               = CPLDPoll,
};

static struct miscdevice CPLDDriverDev = {
	.minor		= CPLD_DRIVER_MINOR,
	.name		= "cpld driver",
	.fops		= &fops,
};

static ssize_t cpldversion_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	u8 ret1 = 0;
	u8 ret2 = 0;
	u8 ret3 = 0;

	mutex_lock(&cpld_driver_mutex);
	ret1 = readb(cpld_base + CPLD1_VERSION_REG);
	ret2 = readb(led_cpld_base + CPLD2_VERSION_REG);
	ret3 = readb(led_cpld_base + CPLD3_VERSION_REG);
	mutex_unlock(&cpld_driver_mutex);

	return sprintf(buf, "cpld%01x: 0x%02x\ncpld%01x: 0x%02x\ncpld%01x: 0x%02x\n",\
		((ret1 >> 6) & CPLD_ID_MASK), (ret1 & CPLD_MINOR_VERSION), ((ret2 >> 6) & CPLD_ID_MASK),\
			(ret2 & CPLD_MINOR_VERSION), ((ret3 >> 6) & CPLD_ID_MASK), (ret3 & CPLD_MINOR_VERSION));
}

static ssize_t boardversion_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	u8 ret = 0;

	mutex_lock(&cpld_driver_mutex);
	ret = readb(cpld_base + 0x02);
	mutex_unlock(&cpld_driver_mutex);

	return sprintf(buf, "0x%02x\n", ret);
}


static ssize_t cpureset_store(struct class *cls, struct class_attribute *attr,
			      const char *buffer, size_t count)
{
	int value;
	u8 ret;

	mutex_lock(&cpld_driver_mutex);
	sscanf(buffer, "%d", &value);
	if (value == 0 ) {
		ret = readb(cpld_base + 0x10);
		ret &= ~0x01;
		writeb(ret, cpld_base + 0x10);
	} else {
		printk( KERN_ALERT NAME ":cpu reset value error\nWrite it to ��0�� to generate a soft reset.\n" );
	}

	mutex_unlock(&cpld_driver_mutex);

	return count;
}

static ssize_t sysled_store(struct class *cls, struct class_attribute *attr,
			    const char *buffer, size_t count)
{
	int value;
	u8 ret;

	mutex_lock(&cpld_driver_mutex);
	sscanf(buffer, "%d", &value);
	if (value <= 3 ) {
		ret = readb(cpld_base + 0x98);
		ret &= ~0x03;
		ret |= (u8)value;
		writeb(ret, cpld_base + 0x98);
	} else {
		printk( KERN_ALERT NAME ":sys led value error\nStatus LED Control. \n11: Off; \n10: 4Hz blink; \n01: 1Hz blink; \n00: On.\n" );
	}
	mutex_unlock(&cpld_driver_mutex);

	return count;
}
#define WDO_REG		0x10
#define WDO_MASK	0x04

static ssize_t wdo_kick(void)
{
	int gpiodata;

	/*kick watchdog*/
	gpiodata = in_be32(gpio_base + GPDAT_OFFSET);
	gpiodata &= ~WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);
	msleep(10);
	gpiodata |= WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);

	return 0;
}
static ssize_t wdo_enable_store(struct class *cls, struct class_attribute *attr,
				const char *buffer, size_t count)
{
	int value;
	u8 ret;

	mutex_lock(&cpld_driver_mutex);
	sscanf(buffer, "%d", &value);
	if (value == 0 ) {
		ret = readb(cpld_base + WDO_REG);
		ret |= WDO_MASK;
		writeb(ret, cpld_base + WDO_REG);
	} else {
		ret = readb(cpld_base + WDO_REG);
		ret &= ~WDO_MASK;
		wdo_kick();
		writeb(ret, cpld_base + WDO_REG);
	}
	mutex_unlock(&cpld_driver_mutex);

	return count;
}
static ssize_t wdo_kick_store(struct class *cls, struct class_attribute *attr, const char *buffer, size_t count)
{
	int gpiodata;

	mutex_lock(&cpld_driver_mutex);
	/*kick watchdog*/
	gpiodata = in_be32(gpio_base + GPDAT_OFFSET);
	gpiodata &= ~WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);
	msleep(10);
	gpiodata |= WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);

	mutex_unlock(&cpld_driver_mutex);

	return count;
}

/*sysfs function*/
static CLASS_ATTR(cpldversion, S_IRUGO | S_IWUSR, cpldversion_show, NULL );
static CLASS_ATTR(boardversion, S_IRUGO | S_IWUSR, boardversion_show, NULL );
//static CLASS_ATTR(sysreset,  S_IWUSR | S_IRUGO, NULL, sysreset_store);
static CLASS_ATTR(cpureset,  S_IWUSR | S_IRUGO, NULL, cpureset_store);
static CLASS_ATTR(sysled,  S_IWUSR | S_IRUGO, NULL, sysled_store);
static CLASS_ATTR(wdo_enable,  S_IWUSR | S_IRUGO, NULL, wdo_enable_store);
static CLASS_ATTR(wdo_kick,  S_IWUSR | S_IRUGO, NULL, wdo_kick_store);


static struct class_attribute *attrgroup[] = {
	&class_attr_cpldversion,
	&class_attr_boardversion,
//	&class_attr_sysreset,
	&class_attr_cpureset,
	&class_attr_sysled,
	&class_attr_wdo_enable,
	&class_attr_wdo_kick,
};

#define NUM_CPLD_FILES  6//sizeof(attrgroup)/sizeof(class_attribute)

void gpio_init(void)
{
	u32 temp = 0;
	gpio_base = ioremap(GPIO_BASE_ADDR, 24 );
	temp |= (WDI_GPIO_MASK | CPLD_GPIO_TCK | CPLD_GPIO_TMS | CPLD_GPIO_TDI);
	if (gpio_base) {
		*(volatile u32 *)(gpio_base + GPDIR_OFFSET) |= temp;
		udelay(30);
		*(volatile u32 *)(gpio_base + GPDAT_OFFSET) |= (temp & (~CPLD_GPIO_TCK));

	} else {
		printk(KERN_INFO "Mapping GPIO address fail\n");
	}
}

#define SMALLSTONE_CPLD_IRQ   0
static int CPLD_Init_Func(void)
{
	u8 ret;
	int status;
	int i;

	/*gpio mapping and init*/
	gpio_init();
	/*CPLD Adress Mapping*/
	cpld_base = ioremap(CPLD_BASE_ADDR, 256);
	if (cpld_base == NULL) {
		printk( KERN_ALERT NAME ":request cpld addr error\n" );
		goto CPLD_EXIT3;
	}
	led_cpld_base = ioremap(CPLD_I2C_LED_ADDR, 256);
	if (led_cpld_base == NULL) {
		printk( KERN_ALERT NAME ":request i2c led cpld addr error\n" );
		goto CPLD_EXIT2;
	}
	printk(KERN_ALERT NAME ":led_cpld_base = %p\n", led_cpld_base);
	lpsram_base = ioremap(LPSRAM_BASE_ADDR,LPSRAM_MAX_SIZE);
	if (lpsram_base == NULL) {
		printk( KERN_ALERT NAME ":request lpsram addr error\n" );

		goto CPLD_EXIT2;
	}
	/*u-boot have notified CPLD?*/
	ret = readb(cpld_base + 0x0a);
	if ((ret & UBOOT_OK_MASK) != UBOOT_OK) {
		printk( KERN_ALERT NAME ":cpld not ready error\n" );
		goto CPLD_EXIT2;
	}
	/*system led become stable green*/
#define GREEN_STABLE_LED 2
	ret &= ~0x07;
	ret |= (u8)GREEN_STABLE_LED;
	writeb(ret, cpld_base + 0x0a);

	/*sysfs interface*/
	for (i = 0; i < NUM_CPLD_FILES; i++) {
		status = class_create_file(cpld_driver_class, attrgroup[i]);
	}
	if ( status ) {
		printk( KERN_NOTICE NAME ":create cpld class file error\n" );
		goto CPLD_EXIT2;
	}

	/*register device*/
	status = misc_register(&CPLDDriverDev);
	if ( status ) {
		printk( KERN_NOTICE NAME ":ERROR register cpld device\n" );
		goto CPLD_EXIT1;
	}

	return 0;
CPLD_EXIT1:
	for (i = 0; i < NUM_CPLD_FILES; i++) {
		class_remove_file(cpld_driver_class, attrgroup[i]);
	}
CPLD_EXIT2:
	iounmap(cpld_base);
	iounmap(led_cpld_base);
	iounmap(lpsram_base);
CPLD_EXIT3:
	iounmap(gpio_base);

	return -1;
}

/*
** module load/initialization
*/
static int __init CPLDDriverInit( void )
{
	int ret;

	printk( KERN_ALERT NAME ":CPLD driver init\n" );
	cpld_driver_class = class_create(THIS_MODULE, "cpld_driver");
	if (IS_ERR(cpld_driver_class))
		return PTR_ERR(cpld_driver_class);
	ret = CPLD_Init_Func();
	if (ret < 0) {
		printk( KERN_ALERT NAME ":CPLD init error\n" );
		return ret;
	}

	return 0;
}

/*
** module unload
*/
static void __exit CPLDDriverExit( void )
{
	int i;

	printk( KERN_ALERT NAME ":CPLD driver exit\n" );
	misc_deregister(&CPLDDriverDev);
	iounmap(cpld_base);
	iounmap(led_cpld_base);
	iounmap(gpio_base);

	for (i = 0; i < NUM_CPLD_FILES; i++) {
		class_remove_file(cpld_driver_class, attrgroup[i]);
	}
	class_destroy(cpld_driver_class);
}

module_init( CPLDDriverInit );
module_exit( CPLDDriverExit );
