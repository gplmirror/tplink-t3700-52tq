/*
 * SS4200-E Hardware API
 * Copyright (c) 2009, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef __KENNISIS_CPLD_H__
#define __KENNISIS_CPLD_H__

//#define CONFIG_REDSTONE 1

#ifdef __cplusplus
extern "C" {
#endif


#ifndef NULL
#define NULL 0
#endif



/* Device type -- in the "User Defined" range. */
#define KENNISIS_TYPE                       ( 0x89 )
/* The IOCTL function codes from 0x800 to 0xFFF are for customer use. */
#define IOCTL_READ_REG                            _IOR(KENNISIS_TYPE,0x05,RegData)
#define IOCTL_WRITE_REG                        _IOW(KENNISIS_TYPE,0x08,RegData)
#define IOCTL_READ_PORT                      _IOR(KENNISIS_TYPE, 0, JTAGPort)
#define IOCTL_WRITE_PORT                  _IOW(KENNISIS_TYPE, 1, JTAGPort)


#ifdef CONFIG_REDSTONE
#define IOCTL_READ_SFP_REG                   _IOR(KENNISIS_TYPE,2,SfpData)
#define IOCTL_WRITE_SFP_REG                  _IOW(KENNISIS_TYPE,3,SfpData)
#ifdef CONFIG_PHYS_64BIT
#define CPLD_I2C_LED_ADDR               0xfffb40000ull
#define LPSRAM_BASE_ADDR                0xfffb80000ull
#else
#define CPLD_I2C_LED_ADDR               0xffb40000
#define LPSRAM_BASE_ADDR                0xffb80000
#endif
#endif



typedef struct
{
     int cpldID;
     int regid;   /*register id*/
     int value;   /*register value*/
     int rw;       /* 0: read  1: write*/

}RegData;

typedef struct
{
 	unsigned char          portid;                      /* TDI, TDO,TCK*/
 	unsigned char 		value;		        /*port value*/
 	int 		rw;	                  /* read  1: write*/
}JTAGPort;

#ifdef CONFIG_REDSTONE
typedef struct
{
	char portID;
	char devAddr;
	char reg;
	char data[256];
	int len;
	int rw;
}SfpData;
#endif


#ifdef __cplusplus
}
#endif

#endif /* __GPIO_H__ */
