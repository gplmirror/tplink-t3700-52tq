#ifndef __KENNISIS_THERMAL_H__
#define __KENNISIS_THERMAL_H__


#ifdef __cplusplus
extern "C" {
#endif

/*for redstone,open these two*/
//#undef CONFIG_KENNISIS_VAR
//#define CONFIG_REDSTONE
//#define THERMAL_DEBUG 1
//#define THERMAL_SIMULATION 1
#ifdef CONFIG_REDSTONE
typedef enum
{
   lm75_p2020=0,
   lm75_bcm56846,
   lm75_LIA,
   lm75_RIA,
   lm75_ROA,
   lm75_FIA,
   psu1,
   psu2,
   fan_chip,
   chipcounts,
}thermal_chips;
#else
typedef enum
{
   lm75_ambient1=0,
   lm75_ambient2,
   lm75_psuinlet1,
   lm75_psuinlet2,
   lm75_systemoulet,
   lm75_p2020,
   lm75_tcam,
   lm75_bcm56634,
   psu1,
   psu2,
   fan_chip,
   chipcounts,
}thermal_chips;
#endif



extern unsigned char read_cpld(char *,int);




#ifdef __cplusplus
}
#endif

#endif /* __KENNISIS_THERMAL_H__ */


