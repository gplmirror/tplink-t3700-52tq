#ifndef __THERMAL_DRIVER_H__
#define __THERMAL_DRIVER_H__


#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
   lm75_p2020=0,
   lm75_bcm56850,
   lm75_LIA,
   lm75_RIA,
   lm75_ROA,
   psu1,
   psu2,
   fan_chip,
   chipcounts,
}thermal_chips;

extern unsigned char read_cpld(char *,int);

#ifdef __cplusplus
}
#endif

#endif /* __THERMAL_DRIVER_H__ */


