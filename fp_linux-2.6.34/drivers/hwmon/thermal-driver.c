/*
   thermal-driver.c - Thermal function for celestica project, include
   fan control algorithm, hw monitoring.

    Copyright (c) 1998, 1999  Frodo Looijaard <frodol@dds.nl>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
//#define THERMAL_DEBUG

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/i2c.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/interrupt.h>
#include "thermal-driver.h"
#include <linux/delay.h>

#include <asm/uaccess.h>   /* copy_*_user */
#include <asm/io.h>
#include <linux/firmware.h>
/*
 * This driver handles all the thermal sensor and fan control chip function here.
 * Since all the chips is i2c interface, we will create all the i2c clients within one driver.
 * Some hardware monitor interface will be exported to user space for status monitor.
 * Also some fan control algorithm parameter tunning interface will be exported.
 */

MODULE_AUTHOR( "Beck He <bhe@Celestica.com>" );
MODULE_DESCRIPTION( "Thermal Driver" );
MODULE_LICENSE( "GPL" );

static int lm75counts = chipcounts - 1;
static struct i2c_client  *thermalclient[chipcounts];  /*define all clients here*/
static const unsigned short const i2c_address[] = {0x48, 0x4e, 0x4f, 0x4b, 0x49, 0x4c};

static int fan_chip_type;
/*list bus num for each client, base on i2c toplogy*/
typedef enum {
	I2C_BUS1,
	I2C_BUS2
} i2cbusnum;
static i2cbusnum busnum[] = {I2C_BUS2, I2C_BUS2, I2C_BUS2, I2C_BUS1, I2C_BUS2, I2C_BUS2, I2C_BUS1, I2C_BUS2};


/* Just auto detect one client*/
static const unsigned short normal_i2c[] = { 0x4b/*i2c_address[lm75_ambient2]*/,  I2C_CLIENT_END };



static const unsigned short normal_i2c1[] = { 0x4f/*i2c_address[lm75_ambient1]*/,  I2C_CLIENT_END };

static struct i2c_board_info /*__initdata*/ thermal_i2c_devices[] = {
	{I2C_BOARD_INFO("lm75_p2020",    0x48)},
	{I2C_BOARD_INFO("lm75_bcm56850",     0x4e)},
	{I2C_BOARD_INFO("lm75_LIA",     0x4f)},
	{I2C_BOARD_INFO("lm75_RIA",     0x4b)},
	{I2C_BOARD_INFO("lm75_ROA",  0x49)},
	{I2C_BOARD_INFO("psu1",               0x58)},
	{I2C_BOARD_INFO("psu2",               0x59)},
	{I2C_BOARD_INFO("fan chip",               0x4d)},
};

static const struct i2c_device_id i2cbus0_deviceid[] = {
	{ "ambient2", lm75_RIA, },

};
static const struct i2c_device_id i2cbus1_deviceid[] = {
	{ "ambient1", lm75_LIA,},
};

#define NAME         "thermal_driver"

DEFINE_MUTEX(thermal_driver_mutex);
static DECLARE_WAIT_QUEUE_HEAD(thermal_wait_queue);
static spinlock_t thermallock;
static struct workqueue_struct *polldev_wq;
static struct delayed_work  delaywork;
static int polldev_users;

static struct class  *thermal_class;

/* The LM75 registers */
#define LM75_TEMP_COUNT      8
#define LM75_REG_CONF		0x01
static const u8 LM75_REG_TEMP[3] = {
	0x00,		/* input */
	0x03,		/* max */
	0x02,		/* hyst */
};

#define EMC2305_CHIP    1
#define EMC2305_VENDOR  0x5d
#define EMC2305_DEVICE   0x34
#define EMC2305_FAN_COUNT		5
#define EMC2305_PWM_COUNT		5
#define EMC2305_VOLT_COUNT	9
#define EMC2305_PIN_CFG_REG_COUNT	4

#define EMC2305_REG_VENDOR 0xfe
#define EMC2305_REG_DEVICE 0xfd
#define EMC2305_REG_FANSTATUS 0x25
#define EMC2305_VENDOR  0x5d
#define EMC2305_DEVICE   0x34
#define EMC2305_REG_PWM(x)		(0x30 + (x*0x10))
#define FAN_STALL_MASK 0x1f  /*FAN 1,2,3,4,5 STALL STATUS*/
#define  FAN_ALGO_ENABLE   1
#define  FAN_ALGO_DISABLE  2

typedef struct {
	int fanspeed[EMC2305_FAN_COUNT];
	int pwm[EMC2305_FAN_COUNT];
	int volt[EMC2305_VOLT_COUNT];
	int pin_cfg[EMC2305_PIN_CFG_REG_COUNT];
	int fan_enabled;
	int fan_fail;  /*one or more fan fail*/
	unsigned long adt_last_update;
} FAN_CHIP;

#define UP_TREND       1
#define DOWN_TREND  2
#define FAN_CONTROL_ENABLE  1
#define FAN_CONTROL_DISABLE 2
typedef struct {
	long temp[LM75_TEMP_COUNT];
	long oldtemp[LM75_TEMP_COUNT]; /*temperature last time*/
	int maxtemp[LM75_TEMP_COUNT];
	int hysttemp[LM75_TEMP_COUNT];
	int temp_trend[LM75_TEMP_COUNT];
	int fan_control_enable[LM75_TEMP_COUNT];
	int last_trend[LM75_TEMP_COUNT];

	unsigned long lm75_last_update;

} LM75;

typedef struct {
	FAN_CHIP fanchip_data;
	LM75       lm75_data;
	int           curr_pwm_setting;
	int           target_pwm_setting;         /*real setting to hw monitor*/
	int           target_pwm[LM75_TEMP_COUNT]; /*target for each sensor*/
	int           fan_algo_enabled;

} thermal_data;

/*define the parameters for fan control algorithm, open them to user space*/
/*each temp sensnor have their own curve with pwm duty cycle, thermal team */
/*can fine tune them during the test procedure*/
typedef struct {
	int       pwm_duty_min;
	int       pwm_duty_max;
	int       low_temp;
	int       high_temp;
	int	      high_warning;
	int	      high_critical;

} thermal_debug_data;

/*pre-defined, will be finalized after thermal test*/
static thermal_debug_data algo_para0[LM75_TEMP_COUNT] = {
	{60, 100, 25, 40, 74, 79}, /*p2020*/
	{60, 100, 80, 100, 89, 94}, /*56850*/
	{65, 75, 32, 41, 53, 57},	/*LIA*/
	{60, 100, 25, 40, 53, 57},	/*RIA*/
	{65, 80, 26, 36, 55, 58},	/*ROA*/
	{55, 70, 29, 38, 100, 200},	/*psu1*/
	{35, 45, 34, 45, 100, 200},	/*psu2*/

};
static thermal_debug_data algo_para1[LM75_TEMP_COUNT] = {
	{60, 100, 25, 40, 74, 79}, /*p2020*/
	{60, 100, 80, 100, 84, 89}, /*56850*/
	{75, 100, 41, 48, 59, 62},	/*LIA*/
	{60, 100, 25, 40, 59, 62},	/*RIA*/
	{80, 100, 36, 46, 49, 52},	/*ROA*/
	{70, 100, 38, 47, 100, 200},	/*psu1*/
	{45, 65, 45, 49, 100, 200},	/*psu2*/

};


/* TEMP: 0.001C/bit (-55C to +125C)
   REG: (0.5C/bit, two's complement) << 7 */
#define LM75_TEMP_MIN (-55000)
#define LM75_TEMP_MAX 125000
static inline u16 LM75_TEMP_TO_REG(long temp)
{
	long ntemp = SENSORS_LIMIT(temp, LM75_TEMP_MIN, LM75_TEMP_MAX);
	ntemp += (ntemp < 0 ? -250 : 250);
	return (u16)((ntemp / 500) << 7);
}

static inline int LM75_TEMP_FROM_REG(u32 reg)
{
	/* use integer division instead of equivalent right shift to
	   guarantee arithmetic shift and preserve the sign */
	return ((s32)reg / 128) / 2;
}


/*-----------------------------------------------------------------------*/

static inline s32 i2c_thermal_read_byte_data(struct i2c_client *client, u8 reg)
{
	int temp;
	u8 data;
	int count = 10;
	temp = i2c_smbus_read_byte_data(client, reg);
	
	while ((temp < 0 || temp == 0xff) && count-- > 0){
		msleep(11);
		temp = i2c_smbus_read_byte_data(client, reg);
	}
	
	if (temp < 0) {
		return temp;
	}
	data = (u8) temp;
	return data;
}

/* All registers are word-sized, except for the configuration register.
   LM75 uses a high-byte first convention, which is exactly opposite to
   the SMBus standard. */
static int lm75_read_value(struct i2c_client *client, u8 reg)
{
	int value;

	if (reg == LM75_REG_CONF)
		return i2c_thermal_read_byte_data(client, reg);

	value = i2c_smbus_read_word_data(client, reg);
	return (value < 0) ? value : swab16(value);
}

short convert_short(char *s, int len)
{
	short i;
	short res = 0;

	for (i = 1; i < len; i++) {
		res = res * 2 + (s[i] - '0');
	}
	if (s[0] == '1')
		return -res;

	return res;
}

long convert_linear(char *data)
{
	char low, high;
	short N, Y;
	long ret;
	int temp = 1;
	char s[11];
	int i;

	low = data[0];
	high = data[1];
	if ((high & 0x80) > 0)
		high = ~high + 0x88;
	for (i = 0; i < 5; i++) {
		s[i] = (high & (0x80 >> i)) > 0 ? '1' : '0';
	}
	N = convert_short(s, 5);
	high = data[1];
	if ((high & 0x04) > 0) {
		high = ~high + 0x40;
		if (low != 0xff)
			low = ~low + 0x1;
		else {
			low = 0x00;
			high += 0x1;
		}

	}
	for (i = 5; i < 8; i++)
		s[i - 5] = (high & (0x80 >> i)) > 0 ? '1' : '0';
	for (i = 0; i < 8; i++)
		s[i + 3] = (low & (0x80 >> i)) > 0 ? '1' : '0';
	Y = convert_short(s, 11);
	if (N > 0) {
		while (N > 0) {
			temp = temp * 2;
			N--;
		}
		ret = Y * temp * 100;
	} else {
		N = 0 - N;
		while (N > 0) {
			temp = temp * 2;
			N--;
		}
		ret = (Y * 100) / temp;
	}

	return ret;
}

static thermal_data *psu_update_device(void)
{
	thermal_data *data;
	char arg[2];
	u8 value[14];
	u8 temp = 0;
	int retval;
	long rtn;
	int count = 10;

	data =  i2c_get_clientdata(thermalclient[0]);
	if (thermalclient[psu1] != NULL) {
		retval = i2c_smbus_read_word_data(thermalclient[psu1], 0x8d);
		while(retval < 0 && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_word_data(thermalclient[psu1], 0x8d);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU1 update fail!\n");
			return data;
		}
		arg[1] = (retval >> 8) & 0x00ff;
		arg[0] = retval & 0x00ff;
		rtn = convert_linear(arg);
		data->lm75_data.temp[psu1] = rtn / 100;
		retval = i2c_smbus_read_i2c_block_data(thermalclient[psu1], 0x9a, 14, value);
		while((retval < 0 || value[11] == 0xff) && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_i2c_block_data(thermalclient[psu1], 0x9a, 14, value);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU1 update fail!\n");
			return data;
		}
		temp = value[11];
	} else
		data->lm75_data.temp[psu1] = 0;
	if (thermalclient[psu2] != NULL) {
		retval = i2c_smbus_read_word_data(thermalclient[psu2], 0x8d);
		while(retval < 0 && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_word_data(thermalclient[psu2], 0x8d);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU2 update fail!\n");
			return data;
		}
		arg[1] = (retval >> 8) & 0x00ff;
		arg[0] = retval & 0x00ff;
		rtn = convert_linear(arg);
		data->lm75_data.temp[psu2] = rtn / 100;
		retval = i2c_smbus_read_i2c_block_data(thermalclient[psu2], 0x9a, 14, value);
		while((retval < 0 || value[11] == 0xff) && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_i2c_block_data(thermalclient[psu2], 0x9a, 14, value);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU2 update fail!\n");
			return data;
		}
	} else
		data->lm75_data.temp[psu2] = 0;
	if (temp > 0 && value[11] > 0) {
		if (temp != value[11]) {
			if ((temp == 0x42 || temp == 0x43) && (value[11] == 0x42 || value[11] == 0x43));
			else
				return data;
				//printk(KERN_ALERT "ERROR: Two PSUs Wind are different!\n");
		}
	}
	data->lm75_data.lm75_last_update = jiffies;

	return data;
}

//flag :0 only update control fan sensors, 1 update all sensors.
static  thermal_data *lm75_update_device(int flag, int idx)
{
	int i;
	int status;
	thermal_data *data;
	struct i2c_client *client;

	data =  i2c_get_clientdata(thermalclient[0]);
	if (1) {
#ifdef THERMAL_DEBUG
		printk( KERN_ALERT NAME ": LM75 Update Temp {\n");
#endif
		for (i = 0; i < lm75counts; i++) {
			if (data->lm75_data.fan_control_enable[i] != FAN_CONTROL_ENABLE && flag == 0)
				continue;
			if (i == psu1 || i == psu2)
				continue;
			if(i != idx && flag == 1)
				continue;
			client = thermalclient[i];
			status = lm75_read_value(client, LM75_REG_TEMP[0]);
			
			msleep(11);//zmzhan add
			if (status < 0) {
#ifdef THERMAL_DEBUG
				printk( KERN_ALERT NAME ":  Error LM75 Update Temp\n");
#endif
			} else {
				data->lm75_data.temp[i]  = status;
#ifdef THERMAL_DEBUG
				printk( KERN_ALERT NAME ": LM75 read Temp = %d\n", LM75_TEMP_FROM_REG(status));
#endif
				if(LM75_TEMP_FROM_REG(status) >= algo_para0[i].high_critical) {
					printk(KERN_WARNING "%s(%d C) Reach High Critical Temperature!\n", thermal_i2c_devices[i].type, LM75_TEMP_FROM_REG(status));
				} else  if(LM75_TEMP_FROM_REG(status) >= algo_para0[i].high_warning){
					printk(KERN_WARNING "%s(%d C) Reach High Warning Temperature!\n", thermal_i2c_devices[i].type, LM75_TEMP_FROM_REG(status));
				}
				if (LM75_TEMP_FROM_REG(status) > data->lm75_data.oldtemp[i]) {
					data->lm75_data.temp_trend[i] = UP_TREND;
					data->lm75_data.last_trend[i] = UP_TREND;
				} else if (LM75_TEMP_FROM_REG(status) == data->lm75_data.oldtemp[i]) {
					data->lm75_data.temp_trend[i] = data->lm75_data.last_trend[i];
				} else {
					data->lm75_data.temp_trend[i] = DOWN_TREND;
					data->lm75_data.last_trend[i] = DOWN_TREND;
				}
				data->lm75_data.oldtemp[i] = LM75_TEMP_FROM_REG(status);
			}
			status = lm75_read_value(client, LM75_REG_TEMP[1]);
			msleep(11);//zmzhan add
			if (status < 0) {
#ifdef THERMAL_DEBUG
				printk( KERN_ERR NAME ":  Error LM75 Update Max Temp\n ");
#endif
			} else
				data->lm75_data.maxtemp[i]  = status;

#ifdef THERMAL_DEBUG
			printk( KERN_ALERT NAME ": LM75 read Max Temp = %d\n", LM75_TEMP_FROM_REG(status));
#endif
			status = lm75_read_value(client, LM75_REG_TEMP[2]);
			msleep(11);//zmzhan add
			if (status < 0) {
#ifdef THERMAL_DEBUG
				printk( KERN_ERR NAME ":  Error LM75 Update Hyst Temp\n");
#endif
			} else
				data->lm75_data.hysttemp[i]  = status;
#ifdef THERMAL_DEBUG
			printk( KERN_ALERT NAME ": LM75 read Hyst Temp = %d\n", LM75_TEMP_FROM_REG(status));
#endif
		}
		data->lm75_data.lm75_last_update = jiffies;
	}

	return data;
}

static int set_psu_pwm(struct i2c_client *client, int pwm)
{
	int status;

	client->flags |= I2C_CLIENT_PEC;
	status = i2c_smbus_write_word_data(client, 0x3b, pwm);
#ifdef THERMAL_DEBUG
	if (status < 0)
		printk(KERN_ALERT NAME "set psu pwm error!\n");
#endif

	return 0;
}

/*set all pwm with same duty cycle*/
static int set_pwm(int pwm)
{

	thermal_data  *data;
	int i, status = 0;
	struct i2c_client *client = thermalclient[chipcounts - 1];

	data =  i2c_get_clientdata(client);
	data->curr_pwm_setting = pwm;
	for ( i = 0; i < EMC2305_PWM_COUNT; i++) {
		if (fan_chip_type == EMC2305_CHIP) {
			status = i2c_smbus_write_byte_data(client, EMC2305_REG_PWM(i), pwm);
			msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
			if (status < 0)
				printk(KERN_ERR NAME "set pwm error!\n");
#endif
		}
	}

	return 0;
}

static int EMC2305_REG_FAN(int fan)
{
	return 0x3e +  fan * 0x10;
}

/*
 * 16-bit registers on the emc2305 are high-byte first.
*/
static inline int emc2305_read_word_data(struct i2c_client *client, u8 reg)
{
	int foo;
	
	msleep(11);
	foo = i2c_thermal_read_byte_data(client, reg) << 8;

	foo |= (u16)i2c_thermal_read_byte_data(client, reg + 1);
	msleep(11);
	
	return foo;
}

static inline int emc2305_fanstall_status(struct i2c_client  *client)
{
	int status = 0;

	status = i2c_thermal_read_byte_data(client, EMC2305_REG_FANSTATUS);
	msleep(11);//zmzhan add
	status &= FAN_STALL_MASK;

	return status;
}


static  thermal_data *emc2305_update_device(void)
{
	int i;
	int fail_count = 0;
	thermal_data *data;
	struct i2c_client *client = thermalclient[fan_chip];

	data =  i2c_get_clientdata(thermalclient[0]);
	data->fanchip_data.fan_fail = 0;
	for (i = 0; i < EMC2305_FAN_COUNT; i++) {
		data->fanchip_data.fanspeed[i] = emc2305_read_word_data(client,
				EMC2305_REG_FAN(i)) >> 3;
		msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
		if (data->fanchip_data.fanspeed[i] < 0)
			printk(KERN_ERR NAME ": Error emc2305 update fanspeed \n");
#endif
	}

	/*fan stall*/
	if ((fail_count = emc2305_fanstall_status(client)) != 0) {
		for (i = 0; i < EMC2305_FAN_COUNT; i++) {
			if ((fail_count & (0x1 << i)) > 0)
				data->fanchip_data.fan_fail += 1;
		}
	}

	return data;
}

static ssize_t manual_pwm_store(struct class *cls, struct class_attribute *attr, const char *buffer, size_t count)
{
	int value;
	struct i2c_client *client = thermalclient[chipcounts - 1]; /*fan chip client*/
	thermal_data *data = i2c_get_clientdata(client);

	mutex_lock(&thermal_driver_mutex);
	sscanf(buffer, "%d", &value);
	if (value > 255) return 0;

	data->fan_algo_enabled = FAN_ALGO_DISABLE;
	set_pwm(value);
	mutex_unlock(&thermal_driver_mutex);

	return count;
}

static ssize_t ambient1_temp_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);

	data = lm75_update_device(1, lm75_p2020);

	mutex_unlock(&thermal_driver_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[0]));
}

static ssize_t ambient2_temp_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);

	data = lm75_update_device(1, lm75_bcm56850);

	mutex_unlock(&thermal_driver_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[1]));
}

static ssize_t LIA_temp_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);
	data = lm75_update_device(1, lm75_LIA);
	mutex_unlock(&thermal_driver_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[2]));
}

static ssize_t RIA_temp_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);
	data = lm75_update_device(1, lm75_RIA);
	mutex_unlock(&thermal_driver_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[3]));
}

static ssize_t systemoutlet_temp_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	data =  i2c_get_clientdata(thermalclient[0]);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[4]));
}



static ssize_t psu1_status_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	unsigned char flag;	

	mutex_lock(&thermal_driver_mutex);
	flag = read_cpld("reset", 0x80);
	if ((0x20 & flag) != 0){
		mutex_unlock(&thermal_driver_mutex);
		return sprintf(buf, "%d\n", 0); //Not present		
	}else if ((0x08 & flag) == 0){
		mutex_unlock(&thermal_driver_mutex);
		return sprintf(buf, "%d\n", 2); //Power not OK
	}else {
		mutex_unlock(&thermal_driver_mutex);
		return sprintf(buf, "%d\n", 1);//psu is present
	}
}

static ssize_t psu2_status_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	unsigned char flag;
	mutex_lock(&thermal_driver_mutex);
	flag = read_cpld("reset", 0x80);
	if ((0x10 & flag) != 0){
		mutex_unlock(&thermal_driver_mutex);
		return sprintf(buf, "%d\n", 0); //Not present		
	}else if ((0x04 & flag) == 0){
		mutex_unlock(&thermal_driver_mutex);
		return sprintf(buf, "%d\n", 2); //Power not OK
	}else {
		mutex_unlock(&thermal_driver_mutex);
		return sprintf(buf, "%d\n", 1);//psu is present
	}
}

static ssize_t psuinlet1_temp_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	data =  i2c_get_clientdata(thermalclient[0]);

	return sprintf(buf, "%ld\n",
		       data->lm75_data.temp[psu1]);
}

static ssize_t psuinlet2_temp_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	data =  i2c_get_clientdata(thermalclient[0]);

	return sprintf(buf, "%ld\n",
		       data->lm75_data.temp[psu2]);
}

/* datasheet says to divide this number by the fan reading to get fan rpm */
static inline int FAN_PERIOD_TO_RPM(int x)
{
	if (fan_chip_type == EMC2305_CHIP) {
		if (x > (0xf5c2 >> 3)) /*<1000rpm, fan fail*/
			return 0;
		return 3932160 * 2 / x;
	}

	return 0;
}

#define FAN1_INDEX 3
static ssize_t fan1speed_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);

	mutex_unlock(&thermal_driver_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[FAN1_INDEX]));
}

#define FAN2_INDEX 1
static ssize_t fan2speed_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);

	mutex_unlock(&thermal_driver_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[FAN2_INDEX]));
}

#define FAN3_INDEX 0
static ssize_t fan3speed_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);

	mutex_unlock(&thermal_driver_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[FAN3_INDEX]));
}


static ssize_t fan4speed_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);
	mutex_unlock(&thermal_driver_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[4]));
}

static ssize_t fan5speed_show(struct class *cls, struct class_attribute *attr,char *buf)
{
	thermal_data *data;

	mutex_lock(&thermal_driver_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);

	mutex_unlock(&thermal_driver_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[2]));
}


/*change the sys file name, but share the top six temp function with Kennisis, so the sys name may not match
the function name. */
static CLASS_ATTR(p2020_temp, S_IRUGO | S_IWUSR, ambient1_temp_show, NULL);
static CLASS_ATTR(bcm56850_temp, S_IRUGO | S_IWUSR, ambient2_temp_show, NULL);
static CLASS_ATTR(LIA_temp, S_IRUGO | S_IWUSR, LIA_temp_show, NULL);
static CLASS_ATTR(RIA_temp, S_IRUGO | S_IWUSR, RIA_temp_show, NULL);
static CLASS_ATTR(ROA_temp, S_IRUGO | S_IWUSR, systemoutlet_temp_show, NULL);
static CLASS_ATTR(psuinlet1_temp, S_IRUGO | S_IWUSR, psuinlet1_temp_show, NULL);
static CLASS_ATTR(psuinlet2_temp, S_IRUGO | S_IWUSR, psuinlet2_temp_show, NULL);
static CLASS_ATTR(psu1_status, S_IRUGO | S_IWUSR, psu1_status_show, NULL);
static CLASS_ATTR(psu2_status, S_IRUGO | S_IWUSR, psu2_status_show, NULL);
static CLASS_ATTR(fan1speed, S_IRUGO | S_IWUSR, fan1speed_show, NULL);
static CLASS_ATTR(fan2speed, S_IRUGO | S_IWUSR, fan2speed_show, NULL);
static CLASS_ATTR(fan3speed, S_IRUGO | S_IWUSR, fan3speed_show, NULL);
static CLASS_ATTR(fan4speed, S_IRUGO | S_IWUSR, fan4speed_show, NULL);
static CLASS_ATTR(fan5speed, S_IRUGO | S_IWUSR, fan5speed_show, NULL);
static CLASS_ATTR(manual_pwm, S_IRUGO | S_IWUSR, NULL, manual_pwm_store);

static struct class_attribute *attrgroup[] = {
	&class_attr_p2020_temp,
	&class_attr_bcm56850_temp,
	&class_attr_LIA_temp,
	&class_attr_RIA_temp,
	&class_attr_ROA_temp,
	&class_attr_psuinlet1_temp,
	&class_attr_psuinlet2_temp,
	&class_attr_psu1_status,
	&class_attr_psu2_status,
	&class_attr_fan1speed,
	&class_attr_fan2speed,
	&class_attr_fan3speed,
	&class_attr_fan4speed,
	&class_attr_fan5speed,
	&class_attr_manual_pwm,
};

#define NUM_THERMAL_FILES 15

/*Create all i2c client here*/
static int Create_All_Client(struct i2c_client *new_client)
{
	int  i;
	unsigned char flag;
	int first_client = lm75_p2020;
	int adap2_client = lm75_LIA;
	int adap1_client = lm75_RIA;

	struct i2c_client * tmpclient;
	struct i2c_adapter *adapter2 = thermalclient[adap2_client]->adapter; /*bus2*/
	struct i2c_adapter *adapter1 = new_client->adapter;                           /*bus1*/
	thermal_data *data = i2c_get_clientdata(new_client);

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "Create all clients here\n");
#endif
	thermalclient[adap1_client] = new_client;
	i2c_set_clientdata(thermalclient[adap1_client], data);
	i2c_set_clientdata(thermalclient[adap2_client], data);

	for (i = first_client; i < chipcounts; i++) {
		if (thermalclient[i] != NULL)
			continue;
		if (i == psu1) {
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x08) == 0)
			{
				thermalclient[i] = NULL;
				continue;
			}
		}
		if (i == psu2) {
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x04) == 0)
			{
				thermalclient[i] = NULL;
				continue;
			}
		}
		if (busnum[i] == I2C_BUS2)
			tmpclient =  i2c_new_device(adapter2, &thermal_i2c_devices[i]);
		else
			tmpclient =  i2c_new_device(adapter1, &thermal_i2c_devices[i]);
		if (tmpclient == NULL ) {
#ifdef THERMAL_DEBUG
			printk( KERN_ALERT NAME "add i2c new device error!,i=%d, addr=%p\n", i, &thermal_i2c_devices[i]);
#endif
			printk(KERN_ALERT NAME "i2c device %p can not register!\n", &thermal_i2c_devices[i]);
			continue;
		}
		i2c_set_clientdata(tmpclient, data);
		thermalclient[i] = tmpclient;
	}
#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "Create all clients return correct!\n");
#endif
	return 0;
}

static irqreturn_t thermal_lm75_irq(int irq, void *dev_id)
{

	disable_irq_nosync(irq);
	enable_irq(irq);
	return IRQ_HANDLED;
}

static int thermal_data_copy(thermal_debug_data *p1, thermal_debug_data *p2)
{
	p1->pwm_duty_min = p2->pwm_duty_min;
	p1->pwm_duty_max = p2->pwm_duty_max;
	p1->low_temp = p2->low_temp;
	p1->high_temp = p2->high_temp;

	return 0;
}

static int thermal_temp_change(thermal_debug_data *p1, thermal_debug_data *p2)
{
	p1->high_warning = p2->high_warning;
	p1->high_critical = p2->high_critical;

	return 0;
}
/*Check All LM75 Chips*/
static int THERMAL_IRQ_TEMP;  /* temp sensor interrupt,from CPLD*/
static int LM75_Init(void)
{
	int cur, conf, hyst;
	int i, j;
	struct i2c_client *new_client;
	thermal_data *data;
	u8 value[32];
	u8 temp;
	u8 temp1;
	int wind_redirect = 0; //1:f->b 2:b->f
	int wind_direction_hw=0;
	int retval, count = 10;
	unsigned long local_jiffies = jiffies;

	data =  i2c_get_clientdata(thermalclient[0]);
	data->lm75_data.lm75_last_update = local_jiffies;
#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "LM75 init here\n");
#endif
	for (i = 0; i < lm75counts; i++) {
		if (i == lm75_LIA || i == lm75_ROA || i == psu1 || i == psu2)
		{
			data->lm75_data.fan_control_enable[i] = FAN_CONTROL_ENABLE;
		} else {
			data->lm75_data.fan_control_enable[i] = FAN_CONTROL_DISABLE;
		}
	}

	for (i = 0; i < lm75counts; i++) {
		data->lm75_data.temp_trend[i] = UP_TREND; /*Default UP_TREND*/
		data->lm75_data.last_trend[i] = UP_TREND;//zmzhan add
	}
	for (i = 0; i < lm75counts; i++) {
		data->lm75_data.oldtemp[i] = 25;
	}
	for (i = 0; i < lm75counts; i++) {
		if (i != psu1 && i != psu2) {
			new_client = thermalclient[i];
			/* Unused addresses */
			cur = i2c_smbus_read_word_data(new_client, 0);
			msleep(11);
#ifdef THERMAL_DEBUG
			if (cur < 0)
				printk( KERN_ALERT NAME "LM75 init read cur error!\n");
#endif
			conf = i2c_thermal_read_byte_data(new_client, 1);
			msleep(11);
#ifdef THERMAL_DEBUG
			if (conf < 0)
				printk( KERN_ALERT NAME "LM75 init read conf error!\n");
#endif
			hyst = i2c_smbus_read_word_data(new_client, 2);
			msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
			if (hyst < 0)
				printk( KERN_ALERT NAME "LM75 init read hyst error!\n");
#endif
		} else {
			//add psu fan direction
			if (thermalclient[i] != NULL) {
				retval = i2c_smbus_read_i2c_block_data(thermalclient[i], 0x9a, 14, value);
				while((retval < 0 || value[11] == 0xff) && count-- > 0) {
					msleep(11);
					retval = i2c_smbus_read_i2c_block_data(thermalclient[i], 0x9a, 14, value);
				}
				count = 10;
				temp = value[11];
				temp1 = value[13];
				
				/* 0x80    STATUS_MFR_SPECIFIC  */
				wind_direction_hw = i2c_thermal_read_byte_data(thermalclient[i],0x80); 
				if (wind_direction_hw < 0){
					wind_direction_hw = 0;
				}
				else {
					wind_direction_hw &= 0x03; /*wind_direction_hw:0x02,f-->b; 0x01,b-->f*/
					if (wind_direction_hw != 0x01 || wind_direction_hw != 0x02)
						wind_direction_hw = 0;
				}
				
                /* BEFORE,judge wind direction via ID info at Reg 0x9a, while ,from NOW, this also is done by bit 0 and bit 1
				at Reg 0x80. For compatiblity, add "wind_direction_hw", same function with "wind_redirect". 
                Hope no hardware change any longer.	*/
				if ((wind_direction_hw&0x02) || ((temp == 0x44)||
					(temp == 0x32 && temp1 == 0x41)||
					(temp == 0x36 && temp1 == 0x41)||
					(temp == 0x32 && temp1 == 0x42)))
				{
					temp = 0;
					/*Consider such a case: 
					for some reason, HW changes the wind direction bit (0x80,bit[0])to be 1 in PSU ,
					which wind direction is assumed as front2back(f-->b), so this "if" statement 
					can address such ODD case. Chris wang */
					if ((wind_direction_hw&0x01)||(wind_redirect == 2)) {
						//printk(KERN_ALERT "ERROR: Two PSUSs wind are different!\n");
						continue;
					}
					wind_redirect = 1;
					thermal_data_copy(&algo_para0[psu2], &algo_para0[psu1]);
					thermal_data_copy(&algo_para1[psu2], &algo_para1[psu1]);
					for(j = 0; j < lm75counts; j++) {
						thermal_temp_change(&algo_para1[j], &algo_para0[j]);
					}
					data->lm75_data.fan_control_enable[lm75_ROA] = FAN_CONTROL_DISABLE;
				} else {
					temp = 0;
					
					/*Case: HW changes the wind direction bit(0x80, bit[1]) to be 1 in PSU
					. Chris wang */
					if ((wind_direction_hw&0x02)||(wind_redirect == 1)) {
						//printk(KERN_ALERT "ERROR: Two PSUSs wind are different!\n");
						continue;
					}
					wind_redirect = 2;
					thermal_data_copy(&algo_para0[psu1], &algo_para0[psu2]);
					thermal_data_copy(&algo_para1[psu1], &algo_para1[psu2]);
					for(j = 0; j < lm75counts; j++) {
						thermal_temp_change(&algo_para0[j], &algo_para1[j]);
					}
					data->lm75_data.fan_control_enable[lm75_LIA] = FAN_CONTROL_DISABLE;
				}
			}
		}
	}

	if (request_irq(THERMAL_IRQ_TEMP, thermal_lm75_irq, 0,
			"over temp", data) < 0) {
		printk( KERN_ALERT NAME "lm75 request irq fail\n" );
	}

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "LM75 init return correct!\n");
#endif
	return 0;
}

static irqreturn_t thermal_fanchip_irq(int irq, void *dev_id)
{
	disable_irq_nosync(irq);
	enable_irq(irq);
	return IRQ_HANDLED;
}

#define MAX_PWM 100
#define MIN_PWM  60
#define PSU1_MAX_PWM 65
#define PSU2_MAX_PWM 55
static int  THERMAL_IRQ_FANCHIP;/*Kennisis ADT7462 interrupt,from CPLD*/
static int EMC2305_Init(void)
{
	int status = 0;
	struct i2c_client *client ;

	client = thermalclient[fan_chip];

	status += i2c_smbus_write_byte_data(client, 0x38,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x48,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x58,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x68,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x78,  0x33);
	msleep(11);
#ifdef THERMAL_DEBUG
	if (status < 0)
		printk(KERN_ALERT NAME "EMC2305_Init error!\n");
#endif
	return status;
}

static int Fan_Chip_Init(void)
{
	struct i2c_client *client ;
	thermal_data *data;
	int vendor, device;
	int status = 0;

	client = thermalclient[fan_chip]; /*fan chip client*/
	data =  i2c_get_clientdata(client);

	vendor = i2c_thermal_read_byte_data(client, EMC2305_REG_VENDOR);
	device = i2c_thermal_read_byte_data(client, EMC2305_REG_DEVICE);

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "vendor=%x, device=%x\n", vendor, device);
#endif
	if (vendor == EMC2305_VENDOR && device == EMC2305_DEVICE) {
#ifdef THERMAL_DEBUG
		printk( KERN_ALERT NAME "Fan chip is EMC2305\n");
#endif
		status = EMC2305_Init();
		fan_chip_type = EMC2305_CHIP;
		goto exit;
	}
exit:
	if (request_irq(THERMAL_IRQ_FANCHIP, thermal_fanchip_irq, 0,
			"fan chip interrupt", data) < 0) {
		printk( KERN_ALERT NAME "fan chip request irq fail\n" );
	}
	/*initial max speed*/
	set_pwm(100 * 255 / 100);

	return status;
}

//#define THERMAL_SIMULATION  /*open this to simulate temperature change to prove the algorithm*/
#ifdef THERMAL_SIMULATION
static long sim_temp = 0xf00;
static int down_flag = 0;//zmzhan add
static int sim_index = 0;
#endif

static void calcuate_target_pwm(thermal_data *data)
{
	int i;
	unsigned char flag;
	int pwm_duty_min, pwm_duty_max, low_temp, high_temp;
	int k;
	long temp;//zmzhan add

	struct i2c_adapter *adapter2 = thermalclient[lm75_LIA]->adapter;
	struct i2c_adapter *adapter1 = thermalclient[lm75_RIA]->adapter;

	data->target_pwm_setting = 0;
	for (i = 0; i < lm75counts; i++) {
		if (i == psu1) {
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x08) == 0)
			{
				if (thermalclient[i] != NULL) {
					i2c_unregister_device(thermalclient[i]);
					thermalclient[i] = NULL;
				}
				continue;
			} else if (thermalclient[i] == NULL) {
				if (busnum[i] == I2C_BUS2)
					thermalclient[i] = i2c_new_device(adapter2, &thermal_i2c_devices[i]);
				else
					thermalclient[i] = i2c_new_device(adapter1, &thermal_i2c_devices[i]);
				if (thermalclient[i] == NULL) {
					//printk(KERN_ALERT "add psu1 device error!\n");
					return;
				}
				i2c_set_clientdata(thermalclient[i], data);
				continue;
			}
		}
		if (i == psu2) {
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x04) == 0)
			{
				if (thermalclient[i] != NULL) {
					i2c_unregister_device(thermalclient[i]);
					thermalclient[i] = NULL;
				}
				continue;
			} else if (thermalclient[i] == NULL) {
				if (busnum[i] == I2C_BUS2)
					thermalclient[i] = i2c_new_device(adapter2, &thermal_i2c_devices[i]);
				else
					thermalclient[i] = i2c_new_device(adapter1, &thermal_i2c_devices[i]);
				if (thermalclient[i] == NULL) {
					//printk(KERN_ALERT "add psu2 device error!\n");
					return;
				}
				i2c_set_clientdata(thermalclient[i], data);
				continue;
			}
		}

		if (data->lm75_data.fan_control_enable[i] == FAN_CONTROL_ENABLE) {
			if (i != psu1 && i != psu2) {
				temp = LM75_TEMP_FROM_REG(data->lm75_data.temp[i]);
			} else {
				temp = data->lm75_data.temp[i];
			}
			if (temp < algo_para0[i].high_temp) {
				pwm_duty_min = algo_para0[i].pwm_duty_min;
				pwm_duty_max = algo_para0[i].pwm_duty_max ;
				low_temp  = algo_para0[i].low_temp;
				high_temp = algo_para0[i].high_temp;
			} else {
				pwm_duty_min = algo_para1[i].pwm_duty_min;
				pwm_duty_max = algo_para1[i].pwm_duty_max ;
				low_temp  = algo_para1[i].low_temp;
				high_temp = algo_para1[i].high_temp;
			}
			if (data->lm75_data.temp_trend[i] == UP_TREND) {

				k = (pwm_duty_max - pwm_duty_min) * 10 / (high_temp - low_temp);

				if (i != psu1 && i != psu2) {
					data->target_pwm[i] = pwm_duty_min + k * (LM75_TEMP_FROM_REG(data->lm75_data.temp[i]) - low_temp) / 10;
					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;

					if (data->target_pwm[i] < pwm_duty_min) data->target_pwm[i] = pwm_duty_min;
					if (data->target_pwm[i] > data->target_pwm_setting)
						data->target_pwm_setting = data->target_pwm[i];
				} else {
					data->target_pwm[i] = pwm_duty_min + k * (data->lm75_data.temp[i] - low_temp) / 10;
					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;

				}
			} else { /*down trend*/
				k = (pwm_duty_max - pwm_duty_min) * 10 / (high_temp - low_temp);
				if (i != psu1 && i != psu2) {
					data->target_pwm[i] = pwm_duty_min + k * (LM75_TEMP_FROM_REG(data->lm75_data.temp[i]) - low_temp) / 10;

					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;

					if (data->target_pwm[i] < pwm_duty_min) data->target_pwm[i] = pwm_duty_min;
					if (data->target_pwm[i] > data->target_pwm_setting)
						data->target_pwm_setting = data->target_pwm[i];
				} else {
					data->target_pwm[i] = pwm_duty_min + k * (data->lm75_data.temp[i] - low_temp) / 10;

					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;

					if (data->target_pwm[i] < pwm_duty_min) data->target_pwm[i] = pwm_duty_min;
				}
			}
#ifdef THERMAL_SIMULATION
			if (i == sim_index) {
				printk(KERN_ALERT "sensor %d :\n ", i);
				printk(KERN_ALERT "temp=%d,k=%d,in=%d,pwm_duty_max=%d,pwm=%d\n ",
				       LM75_TEMP_FROM_REG(data->lm75_data.temp[i]), k, pwm_duty_min, pwm_duty_max, data->target_pwm[i]);
			}
#endif
		} else {
			data->target_pwm[i] = MIN_PWM;
		}
	}
	
	if (data->fanchip_data.fan_fail > 1)  data ->target_pwm_setting = MAX_PWM; /*max pwm when one or more fan fail*/
	set_pwm(data->target_pwm_setting * 255 / 100);
#ifdef THERMAL_SIMULATION
	printk(KERN_ALERT "pwm real value =%d \n ", data->target_pwm_setting);
#endif
	if (thermalclient[psu2] != NULL) {
		if (data->fanchip_data.fan_fail > 1)  data->target_pwm[psu2] = algo_para1[psu2].pwm_duty_max; /*max pwm when two or more fan fail*/
		set_psu_pwm(thermalclient[psu2], data->target_pwm[psu2]);
#ifdef THERMAL_SIMULATION
		printk(KERN_ALERT "psu2 pwm real value =%d \n ", data->target_pwm[psu2]);
#endif
	}
	if (thermalclient[psu1] != NULL) {
		if (data->fanchip_data.fan_fail > 1)  data->target_pwm[psu1] = algo_para1[psu1].pwm_duty_max; /*max pwm when two or more fan fail*/
		set_psu_pwm(thermalclient[psu1], data->target_pwm[psu1]);
#ifdef THERMAL_SIMULATION
		printk(KERN_ALERT "psu1 pwm real value =%d \n ", data->target_pwm[psu1]);
#endif
	}
}

#define TEMP_DETECT_INTERVAL   3000 /*15s interval*/
static void fan_delayed_work(struct work_struct *work)
{
	thermal_data *data;
	mutex_lock(&thermal_driver_mutex);
	
#ifndef THERMAL_SIMULATION
	data = lm75_update_device(1, lm75_ROA);
	data = lm75_update_device(0, lm75counts);
	data = psu_update_device();
#endif
	if (fan_chip_type == EMC2305_CHIP) {
		data = emc2305_update_device();
	}
#ifdef THERMAL_SIMULATION
	if (sim_temp > 0x3c00) {
		down_flag = 1;
		sim_temp = 0x3c00;
	} else if (sim_temp <= 0x1400) {
		down_flag = 0;
		sim_temp = 0x1400;
		for (;;) {
			if (++sim_index >= lm75counts)
				sim_index = 0;
			if (data->lm75_data.fan_control_enable[sim_index] == FAN_CONTROL_ENABLE) {
				break;
			}
		}
	}

	data->lm75_data.temp[sim_index] = sim_temp;
	if (down_flag == 0)
		sim_temp += 0x0100; /*increase 1 degree each time*/
	else
		sim_temp -= 0x0100;
	data->fan_algo_enabled = FAN_ALGO_ENABLE;
#endif
	if (FAN_ALGO_ENABLE == data->fan_algo_enabled ) {
		calcuate_target_pwm(data);
	}
	queue_delayed_work(polldev_wq, &delaywork,
			   msecs_to_jiffies(TEMP_DETECT_INTERVAL));
	mutex_unlock(&thermal_driver_mutex);
}

/*start work queue for fan control algorithm*/
static int start_delay_workqueue(void)
{
	int retval = 0;

	INIT_DELAYED_WORK(&delaywork, fan_delayed_work);
	if (!polldev_users) {
		polldev_wq = create_singlethread_workqueue("thermal_polldevd");
		if (!polldev_wq) {
			printk(KERN_ERR " failed to create fan control workqueue\n");
			retval = -ENOMEM;
			goto out;
		}
	}
	polldev_users++;
out:
	return retval;
}

static int stop_delay_workqueue(void)
{
	cancel_delayed_work_sync(&delaywork);

	if (!--polldev_users)
		destroy_workqueue(polldev_wq);

	return 0;
}

static int  Kick_Fan_Control_Alogrithm(void)
{
	int status;

	status = start_delay_workqueue();
	if (status < 0 ) return -1;
	queue_delayed_work(polldev_wq, &delaywork,
			   msecs_to_jiffies(TEMP_DETECT_INTERVAL));

	return 0;
}

static int Stop_Fan_Control_Alogrithm(void)
{
	stop_delay_workqueue();

	return 0;
}

static int thermal_detect(struct i2c_client *new_client, struct i2c_board_info *info)
{
	struct i2c_adapter *adapter = new_client->adapter;
	int status;

	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA |
				     I2C_FUNC_SMBUS_WORD_DATA))
		return -ENODEV;
	/*create all other i2c client here*/
	status = Create_All_Client(new_client);
	if (status < 0) {
		printk(KERN_ALERT NAME " Create all Client error!\n");
		return -ENODEV;
	}
	status = LM75_Init();
	if (status < 0) {
		printk(KERN_ALERT NAME " LM75 init error!\n");
		return -ENODEV;
	}
	status = Fan_Chip_Init();
	if (status < 0) {
		printk(KERN_ALERT NAME " Fan chip init error!\n");
		return -ENODEV;
	}
	/*start the fan control*/
	status = Kick_Fan_Control_Alogrithm();
	if (status < 0)
		return -ENODEV;
	/* NOTE: we treat "force=..." and "force_lm75=..." the same.
	 * Only new-style driver binding distinguishes chip types.
	 */
	strlcpy(info->type, "lm75-ambient1", I2C_NAME_SIZE);

	return 0;
}

/* device probe and removal */
static int
thermal_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	thermal_data *data;
	int status;
	int i;

	if (!i2c_check_functionality(client->adapter,
				     I2C_FUNC_SMBUS_BYTE_DATA | I2C_FUNC_SMBUS_WORD_DATA))
		return -EIO;
	data = kzalloc(sizeof(thermal_data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;
	data->fan_algo_enabled = FAN_ALGO_ENABLE;
	i2c_set_clientdata(client, data);

	THERMAL_IRQ_TEMP = client->irq;
	printk( KERN_ALERT NAME ":  Create totally %d class sys files\n", NUM_THERMAL_FILES);
	for (i = 0; i < NUM_THERMAL_FILES; i++) {
		status = class_create_file(thermal_class, attrgroup[i]);
	}

	thermal_detect(client, thermal_i2c_devices);

	if (status)
		goto exit_free;

	return 0;
exit_free:
	i2c_set_clientdata(client, NULL);
	kfree(data);
	return status;
}

static int thermal_remove(struct i2c_client *client)
{
	thermal_data *data = i2c_get_clientdata(client);
	struct i2c_client *tmpclient;
	u8 i;
	int first_client = lm75_p2020;

	Stop_Fan_Control_Alogrithm();

	for (i = 0; i < NUM_THERMAL_FILES; i++) {
		class_remove_file(thermal_class, attrgroup[i]);
	}

	for (i = first_client; i < chipcounts; i++) {
		tmpclient = thermalclient[i];
		i2c_set_clientdata(tmpclient, NULL);
		if (!tmpclient) {
			i2c_unregister_device(tmpclient);
		}
	}
	i2c_set_clientdata(client, NULL);
	kfree(data);

	return 0;
}


/*lm75ambient1 sensor locate in i2c bus 2, we must detect the adapter
in i2c bus 1 in another driver*/
static struct i2c_driver thermal_driver = {
	.driver = {
		.name	= "thermal"
	},
	.probe		= thermal_probe,
	.remove		= thermal_remove,
	.id_table	= i2cbus0_deviceid,
	.address_list	= normal_i2c,
};

static int
i2c_bus1_probe(struct i2c_client * client, const struct i2c_device_id * id)
{
	int status = 0;

	int adap2_client = lm75_LIA;

	thermalclient[adap2_client] = client;
	THERMAL_IRQ_FANCHIP = client ->irq;

	return status;
}

static int i2c_bus1_remove(struct i2c_client *client)
{
	i2c_set_clientdata(client, NULL);

	return 0;
}

/*detect bus1 adapter*/
static struct i2c_driver thermal_driver_bus1 = {
	.driver = {
		.name	= "i2c_bus1_adapter",
	},
	.probe		= i2c_bus1_probe,
	.remove		= i2c_bus1_remove,
	.address_list	= normal_i2c1,
	.id_table =  i2cbus1_deviceid,
};

/*-----------------------------------------------------------------------*/
/* module glue */

static int __init thermal_driver_init(void)
{
	int i;

	printk( KERN_ALERT NAME ": thermal driver init\n" );

	for (i = 0; i < chipcounts; i++) {
		thermalclient[i] = NULL;
	}

	thermal_class = class_create(THIS_MODULE, "thermal");
	if (IS_ERR(thermal_class))
		return PTR_ERR(thermal_class);

	if (i2c_add_driver(&thermal_driver_bus1) < 0)
		printk( KERN_ALERT NAME "detect i2c bus1 error\n" );

	return i2c_add_driver(&thermal_driver);
}

static void __exit thermal_driver_exit(void)
{
	printk( KERN_ALERT NAME ": thermal driver exit\n" );
	Stop_Fan_Control_Alogrithm();
	class_destroy(thermal_class);
	i2c_del_driver(&thermal_driver_bus1);
	i2c_del_driver(&thermal_driver);
}

module_init(thermal_driver_init);
module_exit(thermal_driver_exit);
