/*
   kennisis-thermal.c - Thermal function for kennisis project, include
   fan control algorithm, hw monitoring.

    Copyright (c) 1998, 1999  Frodo Looijaard <frodol@dds.nl>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
//#define THERMAL_DEBUG

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/i2c.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/interrupt.h>
#include "kennisis-thermal.h"
#include <linux/delay.h>

#include <asm/uaccess.h>   /* copy_*_user */
#include <asm/io.h>
#include <linux/firmware.h>
/*
 * This driver handles all the thermal sensor and fan control chip function here.
 * Since all the chips is i2c interface, we will create all the i2c clients within one driver.
 * Some hardware monitor interface will be exported to user space for status monitor.
 * Also some fan control algorithm parameter tunning interface will be exported.
 */

MODULE_AUTHOR( "Beck He <bhe@Celestica.com>" );
MODULE_DESCRIPTION( "Thermal Driver" );
MODULE_LICENSE( "GPL" );

/*for redstone,open these two*/
//#undef CONFIG_KENNISIS_VAR
//#define CONFIG_REDSTONE


//bobbie add for fix the address of volt detect pin
static int volt__reg_addr[] = {0xa3, 0xa7, 0x92, 0x8b, 0x90, 0x91, 0x93, 0x94, 0x95};

static int lm75counts = chipcounts - 1;

static struct i2c_client  *thermalclient[chipcounts];  /*define all clients here*/


#ifdef CONFIG_REDSTONE /*Redstone project*/
static const unsigned short const i2c_address[] = {0x48, 0x4e, 0x4f, 0x4b, 0x49, 0x4c};
#else
/*For Kennisis_VAR(P1.0) board, the fan chip will be EMC2305 and the adress will be different
    and one tempsensor address will also be changed*/
// #define CONFIG_KENNISIS_VAR /*move it to the linux config file?*/
#ifdef CONFIG_KENNISIS_VAR
static const unsigned short const i2c_address[] = {0x4f, 0x4b, 0x4a, 0x49, 0x4e, 0x48, 0x4c, 0x4d, 0x4d};
#else
static const unsigned short const i2c_address[] = {0x4f, 0x4b, 0x4a, 0x49, 0x4d, 0x48, 0x4c, 0x4e, 0x58};
#endif

#endif

static int fan_chip_type;
/*list bus num for each client, base on i2c toplogy*/
typedef enum {
	I2C_BUS1,
	I2C_BUS2
} i2cbusnum;
#ifndef CONFIG_REDSTONE
static i2cbusnum busnum[] = {I2C_BUS2, I2C_BUS1, I2C_BUS2, I2C_BUS1, I2C_BUS2, I2C_BUS2, I2C_BUS2, I2C_BUS1, I2C_BUS2, I2C_BUS1, I2C_BUS2};
#else
static i2cbusnum busnum[] = {I2C_BUS2, I2C_BUS1, I2C_BUS2, I2C_BUS1, I2C_BUS2, I2C_BUS2, I2C_BUS2, I2C_BUS1, I2C_BUS2};
#endif

/* Just auto detect one client*/
static const unsigned short normal_i2c[] = { 0x4b/*i2c_address[lm75_ambient2]*/,  I2C_CLIENT_END };
static const unsigned short normal_i2c1[] = { 0x4f/*i2c_address[lm75_ambient1]*/,  I2C_CLIENT_END };
#ifndef CONFIG_KERNEL_32_TO_34
static unsigned short ignore = I2C_CLIENT_END;

static const struct i2c_client_address_data addr_data = {
	.normal_i2c		= normal_i2c,
	.probe			= &ignore,
	.ignore			= &ignore,
};


static const struct i2c_client_address_data addr_data1 = {
	.normal_i2c		= normal_i2c1,
	.probe			= &ignore,
	.ignore			= &ignore,
};
#endif

#ifdef CONFIG_REDSTONE /*redstone project*/
static struct i2c_board_info /*__initdata*/ thermal_i2c_devices[] = {
	{I2C_BOARD_INFO("lm75_p2020",    0x48)},
	{I2C_BOARD_INFO("lm75_bcm56846",     0x4e)},
	{I2C_BOARD_INFO("lm75_LIA",     0x4f)},
	{I2C_BOARD_INFO("lm75_RIA",     0x4b)},
	{I2C_BOARD_INFO("lm75_ROA",  0x49)},
	{I2C_BOARD_INFO("lm75_FIA",          0x4c)},
	{I2C_BOARD_INFO("psu1",               0x58)},
	{I2C_BOARD_INFO("psu2",               0x59)},
	{I2C_BOARD_INFO("fan chip",               0x4d)},
};
#else  /*kennisis and kennisis-var project*/
/*create other clients with these information*/
#ifdef CONFIG_KENNISIS_VAR
#define FAN_CHIP_ADDRESS 0x4d
#define SYSOUTLET_SENSOR_ADDRESS 0x4e
#define BCM56634_ADDRESS	0x4d
#else
#define FAN_CHIP_ADDRESS 0x58
#define SYSOUTLET_SENSOR_ADDRESS 0x4d
#define BCM56634_ADDRESS	0x4e
#endif
static struct i2c_board_info /*__initdata*/ thermal_i2c_devices[] = {
	{I2C_BOARD_INFO("lm75_ambient1",    0x4f/* i2c_address[lm75_ambient1]*/)},
	{I2C_BOARD_INFO("lm75_ambient2",     0x4b/*i2c_address[lm75_ambient2]*/)},
	{I2C_BOARD_INFO("lm75_psuinlet1",     0x4a/*i2c_address[lm75_psuinlet1]*/)},
	{I2C_BOARD_INFO("lm75_psuinlet2",     0x49/* i2c_address[lm75_psuinlet2]*/)},
	{I2C_BOARD_INFO("lm75_systemoulet",  SYSOUTLET_SENSOR_ADDRESS/*i2c_address[lm75_systemoulet]*/)},
	{I2C_BOARD_INFO("lm75_p2020",          0x48/*i2c_address[lm75_p2020]*/)},
	{I2C_BOARD_INFO("lm75_tcam",            0x4c/*i2c_address[lm75_tcam]*/)},
	{I2C_BOARD_INFO("lm75_bcm56634",    BCM56634_ADDRESS/*i2c_address[lm75_bcm56634]*/)},
	{I2C_BOARD_INFO("psu1",               0x58)},
	{I2C_BOARD_INFO("psu2",               0x59)},
	{I2C_BOARD_INFO("fan chip",               FAN_CHIP_ADDRESS/* i2c_address[fanchip]*/)},
};
#endif

//bobbie add 0315
#ifdef CONFIG_REDSTONE
static const struct i2c_device_id i2cbus0_deviceid[] = {
	{ "ambient2", lm75_RIA, },

};
static const struct i2c_device_id i2cbus1_deviceid[] = {
	{ "ambient1", lm75_LIA,},
};
#else
static const struct i2c_device_id i2cbus0_deviceid[] = {
	{ "ambient2", lm75_ambient2, },

};
static const struct i2c_device_id i2cbus1_deviceid[] = {
	{ "ambient1", lm75_ambient1,},

};

#endif

#define NAME         "thermal_driver"

DEFINE_MUTEX(kennisis_thermal_mutex);
static DECLARE_WAIT_QUEUE_HEAD(thermal_wait_queue);
spinlock_t thermallock;
static struct workqueue_struct *polldev_wq;
static struct delayed_work  delaywork;
static int polldev_users;

static struct class  *thermal_class;

/* The LM75 registers */
#ifdef CONFIG_REDSTONE
#define LM75_TEMP_COUNT      8
#else
#define LM75_TEMP_COUNT      10
#endif
#define LM75_REG_CONF		0x01
static const u8 LM75_REG_TEMP[3] = {
	0x00,		/* input */
	0x03,		/* max */
	0x02,		/* hyst */
};

/* ADT7462 registers */
#define ADT7462_REG_DEVICE			0x3D
#define ADT7462_REG_VENDOR			0x3E
#define ADT7462_REG_REVISION			0x3F
#define ADT7462_REG_MIN_TEMP_BASE_ADDR		0x44
#define ADT7462_REG_MIN_TEMP_MAX_ADDR		0x47
#define ADT7462_REG_MAX_TEMP_BASE_ADDR		0x48
#define ADT7462_REG_MAX_TEMP_MAX_ADDR		0x4B
#define ADT7462_REG_TEMP_BASE_ADDR		0x88
#define ADT7462_REG_TEMP_MAX_ADDR		0x8F
#define ADT7462_REG_FAN_BASE_ADDR		0x98
#define ADT7462_REG_FAN_MAX_ADDR		0x9F
#define ADT7462_REG_FAN2_BASE_ADDR		0xA2
#define ADT7462_REG_FAN2_MAX_ADDR		0xA9
#define ADT7462_REG_FAN_ENABLE			0x07
#define ADT7462_REG_FAN_MIN_BASE_ADDR		0x78
#define ADT7462_REG_FAN_MIN_MAX_ADDR		0x7F
#define ADT7462_REG_CFG2			0x02
#define		ADT7462_FSPD_MASK		0x20
#define ADT7462_REG_PWM_BASE_ADDR		0xAA
#define ADT7462_REG_PWM_MAX_ADDR		0xAD
#define	ADT7462_REG_PWM_MIN_BASE_ADDR		0x28
#define ADT7462_REG_PWM_MIN_MAX_ADDR		0x2B
#define ADT7462_REG_PWM_MAX			0x2C
#define ADT7462_REG_PWM_TEMP_MIN_BASE_ADDR	0x5C
#define ADT7462_REG_PWM_TEMP_MIN_MAX_ADDR	0x5F
#define ADT7462_REG_PWM_TEMP_RANGE_BASE_ADDR	0x60
#define ADT7462_REG_PWM_TEMP_RANGE_MAX_ADDR	0x63
#define 	ADT7462_PWM_HYST_MASK		0x0F
#define 	ADT7462_PWM_RANGE_MASK		0xF0
#define		ADT7462_PWM_RANGE_SHIFT		4
#define ADT7462_REG_PWM_CFG_BASE_ADDR		0x21
#define ADT7462_REG_PWM_CFG_MAX_ADDR		0x24
#define		ADT7462_PWM_CHANNEL_MASK	0xE0
#define		ADT7462_PWM_CHANNEL_SHIFT	5
#define ADT7462_REG_PIN_CFG_BASE_ADDR		0x10
#define ADT7462_REG_PIN_CFG_MAX_ADDR		0x13
#define		ADT7462_PIN7_INPUT		0x01	/* cfg0 */
#define		ADT7462_DIODE3_INPUT		0x20
#define		ADT7462_DIODE1_INPUT		0x40
#define		ADT7462_VID_INPUT		0x80
#define		ADT7462_PIN22_INPUT		0x04	/* cfg1 */
#define		ADT7462_PIN21_INPUT		0x08
#define		ADT7462_PIN19_INPUT		0x10
#define		ADT7462_PIN15_INPUT		0x20
#define		ADT7462_PIN13_INPUT		0x40
#define		ADT7462_PIN8_INPUT		0x80
#define 	ADT7462_PIN23_MASK		0x03
#define		ADT7462_PIN23_SHIFT		0
#define		ADT7462_PIN26_MASK		0x0C	/* cfg2 */
#define		ADT7462_PIN26_SHIFT		2
#define		ADT7462_PIN25_MASK		0x30
#define		ADT7462_PIN25_SHIFT		4
//bobbie add 0318
#define		ADT7462_PIN25_INPUT		0x00
#define		ADT7462_PIN24_MASK		0xC0
#define		ADT7462_PIN24_SHIFT		6
#define		ADT7462_PIN26_VOLT_INPUT	0x08
#define		ADT7462_PIN25_VOLT_INPUT	0x20
#define		ADT7462_PIN28_SHIFT		6	/* cfg3 */
#define		ADT7462_PIN28_VOLT		0x5
#define ADT7462_REG_ALARM1			0xB8
#define 	ADT7462_LT_ALARM		0x02
#define		ADT7462_R1T_ALARM		0x04
#define		ADT7462_R2T_ALARM		0x08
#define		ADT7462_R3T_ALARM		0x10
#define ADT7462_REG_ALARM2			0xBB
#define		ADT7462_V0_ALARM		0x01
#define		ADT7462_V1_ALARM		0x02
#define		ADT7462_V2_ALARM		0x04
#define		ADT7462_V3_ALARM		0x08
#define		ADT7462_V4_ALARM		0x10
#define		ADT7462_V5_ALARM		0x20
#define		ADT7462_V6_ALARM		0x40
#define		ADT7462_V7_ALARM		0x80
#define ADT7462_REG_ALARM3			0xBC
#define		ADT7462_V8_ALARM		0x08
#define		ADT7462_V9_ALARM		0x10
#define		ADT7462_V10_ALARM		0x20
#define		ADT7462_V11_ALARM		0x40
#define		ADT7462_V12_ALARM		0x80
#define ADT7462_REG_ALARM4			0xBD
#define		ADT7462_F0_ALARM		0x01
#define		ADT7462_F1_ALARM		0x02
#define		ADT7462_F2_ALARM		0x04
#define		ADT7462_F3_ALARM		0x08
#define		ADT7462_F4_ALARM		0x10
#define		ADT7462_F5_ALARM		0x20
#define		ADT7462_F6_ALARM		0x40
#define		ADT7462_F7_ALARM		0x80
#define ADT7462_ALARM1				0x0000
#define ADT7462_ALARM2				0x0100
#define ADT7462_ALARM3				0x0200
#define ADT7462_ALARM4				0x0300
#define ADT7462_ALARM_REG_SHIFT			8
#define ADT7462_ALARM_FLAG_MASK			0x0F

#define ADT7462_TEMP_COUNT		4
#define ADT7462_TEMP_REG(x)		(ADT7462_REG_TEMP_BASE_ADDR + (x * 2))
#define ADT7462_TEMP_MIN_REG(x) 	(ADT7462_REG_MIN_TEMP_BASE_ADDR + (x))
#define ADT7462_TEMP_MAX_REG(x) 	(ADT7462_REG_MAX_TEMP_BASE_ADDR + (x))
#define TEMP_FRAC_OFFSET		6

#ifdef CONFIG_REDSTONE
#define ADT7462_FAN_COUNT		5
#else
#define ADT7462_FAN_COUNT		3
#endif
#define ADT7462_REG_FAN_MIN(x)		(ADT7462_REG_FAN_MIN_BASE_ADDR + (x))

//#define ADT7462_PWM_COUNT		4
#ifdef CONFIG_REDSTONE
#define ADT7462_PWM_COUNT		5
#else
#define ADT7462_PWM_COUNT		3
#endif
#define ADT7462_REG_PWM(x)		(ADT7462_REG_PWM_BASE_ADDR + (x))
#define ADT7462_REG_PWM_MIN(x)		(ADT7462_REG_PWM_MIN_BASE_ADDR + (x))
#define ADT7462_REG_PWM_TMIN(x)		\
	(ADT7462_REG_PWM_TEMP_MIN_BASE_ADDR + (x))
#define ADT7462_REG_PWM_TRANGE(x)	\
	(ADT7462_REG_PWM_TEMP_RANGE_BASE_ADDR + (x))

#define ADT7462_PIN_CFG_REG_COUNT	4
#define ADT7462_REG_PIN_CFG(x)		(ADT7462_REG_PIN_CFG_BASE_ADDR + (x))
#define ADT7462_REG_PWM_CFG(x)		(ADT7462_REG_PWM_CFG_BASE_ADDR + (x))

#define ADT7462_ALARM_REG_COUNT		4

#define ADT7462_VOLT_COUNT	9

#define ADT7462_VENDOR		0x41
#define ADT7462_DEVICE		0x62
/* datasheet only mentions a revision 4 */
#define ADT7462_REVISION	0x04

#define ADT7462_CHIP    1
#define EMC2305_CHIP    2

#define EMC2305_REG_VENDOR 0xfe
#define EMC2305_REG_DEVICE 0xfd
#define EMC2305_REG_FANSTATUS 0x25

#define EMC2305_VENDOR  0x5d
#define EMC2305_DEVICE   0x34
#define EMC2305_REG_PWM(x)		(0x30 + (x*0x10))
#ifdef CONFIG_REDSTONE
#define FAN_STALL_MASK 0x1f  /*FAN 1,2,3,4,5 STALL STATUS*/
#else
#define FAN_STALL_MASK 0x07  /*FAN 1,2,3 STALL STATUS*/
#endif

#define  FAN_ALGO_ENABLE   1
#define  FAN_ALGO_DISABLE  2

typedef struct {
	int fanspeed[ADT7462_FAN_COUNT];
	int pwm[ADT7462_FAN_COUNT];
	int volt[ADT7462_VOLT_COUNT];
	int pin_cfg[ADT7462_PIN_CFG_REG_COUNT];
	int fan_enabled;
	int fan_fail;  /*one or more fan fail*/
	unsigned long adt_last_update;
} FAN_CHIP;


#define UP_TREND       1
#define DOWN_TREND  2
#define FAN_CONTROL_ENABLE  1
#define FAN_CONTROL_DISABLE 2
typedef struct {
	long temp[LM75_TEMP_COUNT];
	long oldtemp[LM75_TEMP_COUNT]; /*temperature last time*/
	int maxtemp[LM75_TEMP_COUNT];
	int hysttemp[LM75_TEMP_COUNT];
	int temp_trend[LM75_TEMP_COUNT];
	int fan_control_enable[LM75_TEMP_COUNT];
	int last_trend[LM75_TEMP_COUNT];

	unsigned long lm75_last_update;

} LM75;

//#define FAN_CONTROL_SENSORS 5 /*Totally 8 sensors, 5 of them for fan control*/
typedef struct {
	FAN_CHIP fanchip_data;
	LM75       lm75_data;
	int           curr_pwm_setting;
	int           target_pwm_setting;         /*real setting to hw monitor*/
	int           target_pwm[LM75_TEMP_COUNT]; /*target for each sensor*/
	int           fan_algo_enabled;
	int			up_delay[LM75_TEMP_COUNT]; /* Ruyi */
	int			down_delay[LM75_TEMP_COUNT]; /* Ruyi */
	int			last_pwm[LM75_TEMP_COUNT]; /* Ruyi */

} thermal_data;

/*define the parameters for fan control algorithm, open them to user space*/
/*each temp sensnor have their own curve with pwm duty cycle, thermal team */
/*can fine tune them during the test procedure*/
typedef struct {
	int           pwm_duty_min;
	int           pwm_duty_max;
	int           low_temp;
	int           high_temp;
	int	      high_warning;
	int	      high_critical;

} thermal_debug_data;

#ifdef CONFIG_REDSTONE
/*pre-defined, will be finalized after thermal test*/
static thermal_debug_data algo_para0[LM75_TEMP_COUNT] = {
	{60, 100, 25, 40, 74, 79}, /*p2020*/
	{60, 100, 80, 100, 89, 94}, /*56846*/
	{65, 75, 32, 41, 53, 57},	/*LIA*/
	{60, 100, 25, 40, 53, 57},	/*RIA*/
	{65, 80, 26, 36, 55, 58},	/*ROA*/
	{60, 100, 25, 38, 100, 200}, /*FIA sensor*/
	{55, 70, 29, 38, 100, 200},	/*psu1*/
	{35, 45, 34, 45, 100, 200},	/*psu2*/

};
static thermal_debug_data algo_para1[LM75_TEMP_COUNT] = {
	{60, 100, 25, 40, 74, 79}, /*p2020*/
	{60, 100, 80, 100, 84, 89}, /*56846*/
	{75, 100, 41, 48, 59, 62},	/*LIA*/
	{60, 100, 25, 40, 59, 62},	/*RIA*/
	{80, 100, 36, 46, 49, 52},	/*ROA*/
	{60, 100, 25, 38, 100, 200}, /*FIA sensor*/
	{70, 100, 38, 47, 100, 200},	/*psu1*/
	{45, 65, 45, 49, 100, 200},	/*psu2*/

};
#else

/*pre-defined, will be finalized after thermal test*/
static thermal_debug_data algo_para0[LM75_TEMP_COUNT] = {
	{55, 90, 36, 46, 52, 55}, /*ambient 1*/
	{60, 100, 25, 40, 52, 55}, /*ambient 2*/
	{60, 100, 25, 40, 100, 200},
	{60, 100, 25, 40, 100, 200},
	{60, 100, 25, 40, 63, 64},
	{60, 100, 60, 80, 72, 77}, /*p2020 sensor*/
	{60, 100, 60, 80, 100, 200}, /*tcam sensor*/
	{60, 100, 60, 80, 100, 105}, /*bcm sensor*/
	{35, 55, 28, 43, 100, 200},	/*psu1*/
	{35, 55, 28, 43, 100, 200},	/*psu2*/

};
static thermal_debug_data algo_para1[LM75_TEMP_COUNT] = {
	{55, 90, 36, 46, 52, 55}, /*ambient 1*/
	{60, 100, 25, 40, 52, 55}, /*ambient 2*/
	{60, 100, 25, 40, 100, 200},
	{60, 100, 25, 40, 100, 200},
	{60, 100, 25, 40, 63, 64},
	{60, 100, 60, 80, 72, 77}, /*p2020 sensor*/
	{60, 100, 60, 80, 100, 200}, /*tcam sensor*/
	{60, 100, 60, 80, 100, 105}, /*bcm sensor*/
	{35, 55, 28, 43, 100, 200},	/*psu1*/
	{35, 55, 28, 43, 100, 200},	/*psu2*/

};
#endif

/* TEMP: 0.001C/bit (-55C to +125C)
   REG: (0.5C/bit, two's complement) << 7 */
#define LM75_TEMP_MIN (-55000)
#define LM75_TEMP_MAX 125000
static inline u16 LM75_TEMP_TO_REG(long temp)
{
	long ntemp = SENSORS_LIMIT(temp, LM75_TEMP_MIN, LM75_TEMP_MAX);
	ntemp += (ntemp < 0 ? -250 : 250);
	return (u16)((ntemp / 500) << 7);
}

static inline int LM75_TEMP_FROM_REG(u32 reg)
{
	/* use integer division instead of equivalent right shift to
	   guarantee arithmetic shift and preserve the sign */
	return ((s32)reg / 128) / 2;
}


/*-----------------------------------------------------------------------*/

static inline s32 i2c_thermal_read_byte_data(struct i2c_client *client, u8 reg)
{
	int temp;
	u8 data;
	int count = 10;
	temp = i2c_smbus_read_byte_data(client, reg);
	
	while ((temp < 0 || temp == 0xff) && count-- > 0){
		msleep(11);
		temp = i2c_smbus_read_byte_data(client, reg);
	}
	
	if (temp < 0) {
		return temp;
	}
	data = (u8) temp;
	return data;
}

/* register access */

/* All registers are word-sized, except for the configuration register.
   LM75 uses a high-byte first convention, which is exactly opposite to
   the SMBus standard. */
static int lm75_read_value(struct i2c_client *client, u8 reg)
{
	int value;

	if (reg == LM75_REG_CONF)
		return i2c_thermal_read_byte_data(client, reg);

	value = i2c_smbus_read_word_data(client, reg);
	return (value < 0) ? value : swab16(value);
}

#if 0
static int lm75_write_value(struct i2c_client *client, u8 reg, u16 value)
{
	if (reg == LM75_REG_CONF)
		return i2c_smbus_write_byte_data(client, reg, value);
	else
		return i2c_smbus_write_word_data(client, reg, swab16(value));
}
#endif

short convert_short(char *s, int len)
{
	short i;
	short res = 0;

	for (i = 1; i < len; i++) {
		res = res * 2 + (s[i] - '0');
	}
	if (s[0] == '1')
		return -res;

	return res;
}
long convert_linear(char *data)
{
	char low, high;
	short N, Y;
	long ret;
	int temp = 1;
	char s[11];
	int i;

	low = data[0];
	high = data[1];
	if ((high & 0x80) > 0)
		high = ~high + 0x88;
	for (i = 0; i < 5; i++) {
		s[i] = (high & (0x80 >> i)) > 0 ? '1' : '0';
	}
	N = convert_short(s, 5);
	high = data[1];
	if ((high & 0x04) > 0) {
		high = ~high + 0x40;
		if (low != 0xff)
			low = ~low + 0x1;
		else {
			low = 0x00;
			high += 0x1;
		}

	}
	for (i = 5; i < 8; i++)
		s[i - 5] = (high & (0x80 >> i)) > 0 ? '1' : '0';
	for (i = 0; i < 8; i++)
		s[i + 3] = (low & (0x80 >> i)) > 0 ? '1' : '0';
	Y = convert_short(s, 11);
	if (N > 0) {
		while (N > 0) {
			temp = temp * 2;
			N--;
		}
		ret = Y * temp * 100;
	} else {
		N = 0 - N;
		while (N > 0) {
			temp = temp * 2;
			N--;
		}
		ret = (Y * 100) / temp;
	}

	return ret;
}
static thermal_data *psu_update_device(void)
{
	thermal_data *data;
	char arg[2];
	u8 value[14];//zmzhan add
	u8 temp = 0; //zmzhan add
	int retval;
	long rtn;
	int count = 10;

	data =  i2c_get_clientdata(thermalclient[0]);
	if (thermalclient[psu1] != NULL) {
		retval = i2c_smbus_read_word_data(thermalclient[psu1], 0x8d);
		while(retval < 0 && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_word_data(thermalclient[psu1], 0x8d);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU1 update fail!\n");
			return data;
		}
		arg[1] = (retval >> 8) & 0x00ff;
		arg[0] = retval & 0x00ff;
		rtn = convert_linear(arg);
		data->lm75_data.temp[psu1] = rtn / 100;
		/* Ruyi */
		if (rtn/100 > data->lm75_data.oldtemp[psu1]) {
			if (data->lm75_data.last_trend[psu1] == DOWN_TREND) //ruyi
				data->up_delay[psu1] = data->lm75_data.oldtemp[psu1];
			data->lm75_data.temp_trend[psu1] = UP_TREND;
			data->lm75_data.last_trend[psu1] = UP_TREND;
			data->last_pwm[psu1] = data->target_pwm[psu1]; //ruyi
		} else if (rtn/100 == data->lm75_data.oldtemp[psu1]) {
			data->lm75_data.temp_trend[psu1] = data->lm75_data.last_trend[psu1];
		} else {
			if (data->lm75_data.last_trend[psu1] == UP_TREND) //ruyi
				data->down_delay[psu1] = data->lm75_data.oldtemp[psu1];
			data->lm75_data.temp_trend[psu1] = DOWN_TREND;
			data->lm75_data.last_trend[psu1] = DOWN_TREND;
			data->last_pwm[psu1] = data->target_pwm[psu1]; //ruyi
		}
		data->lm75_data.oldtemp[psu1] = rtn/100;
		//printk(KERN_ALERT "psu1 temp = %d\n", data->lm75_data.temp[psu1]);
		retval = i2c_smbus_read_i2c_block_data(thermalclient[psu1], 0x9a, 14, value);
		while((retval < 0 || value[11] == 0xff) && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_i2c_block_data(thermalclient[psu1], 0x9a, 14, value);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU1 update fail!\n");
			return data;
		}
		temp = value[11];
	} else
		data->lm75_data.temp[psu1] = 0;
	if (thermalclient[psu2] != NULL) {
		retval = i2c_smbus_read_word_data(thermalclient[psu2], 0x8d);
		while(retval < 0 && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_word_data(thermalclient[psu2], 0x8d);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU2 update fail!\n");
			return data;
		}
		arg[1] = (retval >> 8) & 0x00ff;
		arg[0] = retval & 0x00ff;
		rtn = convert_linear(arg);
		data->lm75_data.temp[psu2] = rtn / 100;
		if (rtn/100 > data->lm75_data.oldtemp[psu2]) {
			if (data->lm75_data.last_trend[psu2] == DOWN_TREND) //ruyi
				data->up_delay[psu2] = data->lm75_data.oldtemp[psu2];
			data->lm75_data.temp_trend[psu2] = UP_TREND;
			data->lm75_data.last_trend[psu2] = UP_TREND;
			data->last_pwm[psu2] = data->target_pwm[psu2]; //ruyi
		} else if (rtn/100 == data->lm75_data.oldtemp[psu2]) {
			data->lm75_data.temp_trend[psu2] = data->lm75_data.last_trend[psu2];
		} else {
			if (data->lm75_data.last_trend[psu2] == UP_TREND) //ruyi
				data->down_delay[psu2] = data->lm75_data.oldtemp[psu2];
			data->lm75_data.temp_trend[psu2] = DOWN_TREND;
			data->lm75_data.last_trend[psu2] = DOWN_TREND;
			data->last_pwm[psu2] = data->target_pwm[psu2]; //ruyi
		}
		data->lm75_data.oldtemp[psu2] = rtn/100;
		//printk(KERN_ALERT "psu2 temp = %d\n", data->lm75_data.temp[psu2]);
		retval = i2c_smbus_read_i2c_block_data(thermalclient[psu2], 0x9a, 14, value);
		while((retval < 0 || value[11] == 0xff) && count-- > 0) {
			msleep(11);
			retval = i2c_smbus_read_i2c_block_data(thermalclient[psu2], 0x9a, 14, value);
		}
		count = 10;
		if(retval < 0) {
			printk(KERN_ERR "ERROR: PSU2 update fail!\n");
			return data;
		}
	} else
		data->lm75_data.temp[psu2] = 0;
#ifdef CONFIG_REDSTONE
		if (temp != value[11]) {
			if ((temp == 0x42 || temp == 0x43) && (value[11] == 0x42 || value[11] == 0x43));
		else {
			//printk(KERN_ALERT "ERROR: Two PSUs Wind are different!\n");
			return data;
		}
	}
#endif
	data->lm75_data.lm75_last_update = jiffies;

	return data;
}
//flag :0 only update control fan sensors, 1 update all sensors.
static  thermal_data *lm75_update_device(int flag, int idx)
{
	int i;
	int status;
	thermal_data *data;
	struct i2c_client *client;

	data =  i2c_get_clientdata(thermalclient[0]);
	if (1) {
#ifdef THERMAL_DEBUG
		printk( KERN_ALERT NAME ": LM75 Update Temp {\n");
#endif
		for (i = 0; i < lm75counts; i++) {
			if (data->lm75_data.fan_control_enable[i] != FAN_CONTROL_ENABLE && flag == 0)
				continue;
			if (i == psu1 || i == psu2)
				continue;
			if(i != idx && flag == 1)
				continue;
			client = thermalclient[i];
			status = lm75_read_value(client, LM75_REG_TEMP[0]);
			msleep(11);//zmzhan add
			if (status < 0) {
#ifdef THERMAL_DEBUG
				printk( KERN_ALERT NAME ":  Error LM75 Update Temp\n");
#endif
			} else {
				data->lm75_data.temp[i]  = status;
#ifdef THERMAL_DEBUG
				printk( KERN_ALERT NAME ": LM75 read Temp = %d\n", LM75_TEMP_FROM_REG(status));
#endif
				if(LM75_TEMP_FROM_REG(status) >= algo_para0[i].high_critical) {
					printk(KERN_WARNING "%s(%d C) Reach High Critical Temperature!\n", thermal_i2c_devices[i].type, LM75_TEMP_FROM_REG(status));
				} else  if(LM75_TEMP_FROM_REG(status) >= algo_para0[i].high_warning){
					printk(KERN_WARNING "%s(%d C) Reach High Warning Temperature!\n", thermal_i2c_devices[i].type, LM75_TEMP_FROM_REG(status));
				}
				if (LM75_TEMP_FROM_REG(status) > data->lm75_data.oldtemp[i]) {
					if (data->lm75_data.last_trend[i] == DOWN_TREND) //ruyi
						data->up_delay[i] = data->lm75_data.oldtemp[i];
					data->lm75_data.temp_trend[i] = UP_TREND;
					data->lm75_data.last_trend[i] = UP_TREND;
					data->last_pwm[i] = data->target_pwm[i]; //ruyi
				} else if (LM75_TEMP_FROM_REG(status) == data->lm75_data.oldtemp[i]) {
					data->lm75_data.temp_trend[i] = data->lm75_data.last_trend[i];
				} else {
					if (data->lm75_data.last_trend[i] == UP_TREND) //ruyi
						data->down_delay[i] = data->lm75_data.oldtemp[i];
					data->lm75_data.temp_trend[i] = DOWN_TREND;
					data->lm75_data.last_trend[i] = DOWN_TREND;
					data->last_pwm[i] = data->target_pwm[i]; //ruyi
				}
				data->lm75_data.oldtemp[i] = LM75_TEMP_FROM_REG(status);
			}
			status = lm75_read_value(client, LM75_REG_TEMP[1]);
			msleep(11);//zmzhan add
			if (status < 0) {
#ifdef THERMAL_DEBUG
				printk( KERN_ERR NAME ":  Error LM75 Update Max Temp\n ");
#endif
			} else
				data->lm75_data.maxtemp[i]  = status;

#ifdef THERMAL_DEBUG
			printk( KERN_ALERT NAME ": LM75 read Max Temp = %d\n", LM75_TEMP_FROM_REG(status));
#endif
			status = lm75_read_value(client, LM75_REG_TEMP[2]);
			msleep(11);//zmzhan add
			if (status < 0) {
#ifdef THERMAL_DEBUG
				printk( KERN_ERR NAME ":  Error LM75 Update Hyst Temp\n");
#endif
			} else
				data->lm75_data.hysttemp[i]  = status;
#ifdef THERMAL_DEBUG
			printk( KERN_ALERT NAME ": LM75 read Hyst Temp = %d\n", LM75_TEMP_FROM_REG(status));
#endif
		}
		data->lm75_data.lm75_last_update = jiffies;
	}

	return data;
}

static int set_psu_pwm(struct i2c_client *client, int pwm)
{
	int status;

	client->flags |= I2C_CLIENT_PEC;
	status = i2c_smbus_write_word_data(client, 0x3b, pwm);
#ifdef THERMAL_DEBUG
	if (status < 0)
		printk(KERN_ALERT NAME "set psu pwm error!\n");
#endif

	return 0;
}

/*set all pwm with same duty cycle*/
static int set_pwm(int pwm)
{

	thermal_data  *data;
	int i, status = 0;
	struct i2c_client *client = thermalclient[chipcounts - 1];

	data =  i2c_get_clientdata(client);
	data->curr_pwm_setting = pwm;
	for ( i = 0; i < ADT7462_PWM_COUNT; i++) {
		if (fan_chip_type == ADT7462_CHIP) {
			status = i2c_smbus_write_byte_data(client, ADT7462_REG_PWM(i), pwm);
			msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
			if (status < 0)
				printk(KERN_ERR NAME "set pwm error!\n");
#endif
		} else if (fan_chip_type == EMC2305_CHIP) {
			status = i2c_smbus_write_byte_data(client, EMC2305_REG_PWM(i), pwm);
			msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
			if (status < 0)
				printk(KERN_ERR NAME "set pwm error!\n");
#endif
		}
	}

	return 0;
}

//bobbie del 0321 ADT_VOLT
static int EMC2305_REG_FAN(int fan)
{
	return 0x3e +  fan * 0x10;
}

/*
 * 16-bit registers on the emc2305 are high-byte first.
*/
static inline int emc2305_read_word_data(struct i2c_client *client, u8 reg)
{
	int foo;
	
	msleep(11);//zmzhan add
	foo = i2c_thermal_read_byte_data(client, reg) << 8;

	//printk("%d value is %d\n",reg,foo);
	foo |= (u16)i2c_thermal_read_byte_data(client, reg + 1);
	msleep(11);//zmzhan add
	//printk("final value is %d\n",foo);
	
	return foo;
}

static inline int emc2305_fanstall_status(struct i2c_client  *client)
{
	int status = 0;

	status = i2c_thermal_read_byte_data(client, EMC2305_REG_FANSTATUS);
	msleep(11);//zmzhan add
	status &= FAN_STALL_MASK;

	return status;
}


static  thermal_data *emc2305_update_device(void)
{
	int i;
	int speed = 0;
	int fail_count = 0;
	thermal_data *data;
	struct i2c_client *client = thermalclient[fan_chip];

	data =  i2c_get_clientdata(thermalclient[0]);
	data->fanchip_data.fan_fail = 0;
	for (i = 0; i < ADT7462_FAN_COUNT; i++) {
		speed = emc2305_read_word_data(client, EMC2305_REG_FAN(i)) >> 3;
		if (speed >= 0)
			data->fanchip_data.fanspeed[i] = speed;
		msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
		if (data->fanchip_data.fanspeed[i] < 0)
			printk(KERN_ERR NAME ": Error emc2305 update fanspeed \n");
#endif
	}

	/*fan stall*/
	if ((fail_count = emc2305_fanstall_status(client)) != 0) {
		for (i = 0; i < ADT7462_FAN_COUNT; i++) {
			if ((fail_count & (0x1 << i)) > 0)
				data->fanchip_data.fan_fail += 1;
		}
	}

	return data;
}

/* For some reason these registers are not contiguous. */
static int ADT7462_REG_FAN(int fan)
{
	if (fan < 4)
		return ADT7462_REG_FAN_BASE_ADDR + (2 * fan);
	return ADT7462_REG_FAN2_BASE_ADDR + (2 * (fan - 4));
}

/*
 * 16-bit registers on the ADT7462 are low-byte first.  The data sheet says
 * that the low byte must be read before the high byte.
 */
static inline int adt7462_read_word_data(struct i2c_client *client, u8 reg)
{
	u16 foo;
	foo = i2c_thermal_read_byte_data(client, reg);

	//printk("%d value is %d\n",reg,foo);
	foo |= ((u16)i2c_thermal_read_byte_data(client, reg + 1) << 8);
	//printk("final value is %d\n",foo);
	
	return foo;
}

static  thermal_data *adt7462_update_device(void)
{
	thermal_data *data;
	unsigned long local_jiffies = jiffies;
	struct i2c_client *client = thermalclient[fan_chip];
	int i;

	data =  i2c_get_clientdata(thermalclient[0]);
	data->fanchip_data.fan_fail = 0;
	for (i = 0; i < ADT7462_PWM_COUNT; i++) {
		data->fanchip_data.pwm[i] = i2c_thermal_read_byte_data(client,
					    ADT7462_REG_PWM(i));
	}
	for (i = 0; i < ADT7462_FAN_COUNT; i++) {
		data->fanchip_data.fanspeed[i] = adt7462_read_word_data(client,
						 ADT7462_REG_FAN(i));
		/*fanspeed < 10 rpm*/
		if (data->fanchip_data.fanspeed[i]  > 0xd2f0) data->fanchip_data.fan_fail = 1;
	}
	data->fanchip_data.fan_enabled = i2c_thermal_read_byte_data(client,
					 ADT7462_REG_FAN_ENABLE);
	for (i = 0; i < ADT7462_PIN_CFG_REG_COUNT; i++) {
		data->fanchip_data.pin_cfg[i] = i2c_thermal_read_byte_data(client,
						ADT7462_REG_PIN_CFG(i));
	}
	for (i = 0; i < ADT7462_VOLT_COUNT; i++) {

		data->fanchip_data.volt[i] = i2c_thermal_read_byte_data(client,
					     volt__reg_addr[i]);
	}
	data->fanchip_data.adt_last_update = local_jiffies;

	return data;
}
#if 0
/* sysfs attributes for hwmon */
#define  AMBIENT1_INDEX  0
static ssize_t ambient1_min_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[AMBIENT1_INDEX].pwm_duty_min = value;

	return count;
}

static ssize_t ambient1_max_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[AMBIENT1_INDEX].pwm_duty_max = value;

	return count;
}

#define  AMBIENT2_INDEX  1
static ssize_t ambient2_min_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[AMBIENT2_INDEX].pwm_duty_min = value;

	return count;
}

static ssize_t ambient2_max_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[AMBIENT2_INDEX].pwm_duty_max = value;

	return count;
}

#define  P2020_INDEX  5
static ssize_t p2020_min_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[P2020_INDEX].pwm_duty_min = value;

	return count;
}

static ssize_t p2020_max_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[P2020_INDEX] .pwm_duty_max = value;

	return count;
}

#define  TCAM_INDEX  6
static ssize_t tcam_min_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[TCAM_INDEX].pwm_duty_min = value;

	return count;
}

static ssize_t tcam_max_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[TCAM_INDEX] .pwm_duty_max = value;

	return count;
}

#define  BCM_INDEX  6
static ssize_t bcm56634_min_duty_store(struct class *cls,
 #ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[BCM_INDEX].pwm_duty_min = value;

	return count;
}

static ssize_t bcm56634_max_duty_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[BCM_INDEX] .pwm_duty_max = value;

	return count;
}

static ssize_t ambient1_low_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 	struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[AMBIENT1_INDEX].low_temp = value;

	return count;
}

static ssize_t ambient1_high_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 	struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[AMBIENT1_INDEX].high_temp = value;

	return count;
}

static ssize_t ambient2_low_temp_store(struct class *cls,
 #ifdef CONFIG_KERNEL_32_TO_34
 	struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[AMBIENT2_INDEX].low_temp = value;

	return count;
}

static ssize_t ambient2_high_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[AMBIENT2_INDEX].high_temp = value;

	return count;
}

static ssize_t p2020_low_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[P2020_INDEX] .low_temp = value;

	return count;
}

static ssize_t p2020_high_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[P2020_INDEX].high_temp = value;

	return count;
}

static ssize_t tcam_low_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[TCAM_INDEX].low_temp = value;

	return count;
}

static ssize_t tcam_high_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[TCAM_INDEX] .high_temp = value;

	return count;
}

static ssize_t bcm_low_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para0[BCM_INDEX].low_temp = value;

	return count;
}

static ssize_t bcm_high_temp_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;

	sscanf(buffer, "%d", &value);
	algo_para1[BCM_INDEX].high_temp = value;

	return count;
}
#endif
static ssize_t manual_pwm_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 const char *buffer, size_t count)
{
	int value;
	struct i2c_client *client = thermalclient[chipcounts - 1]; /*fan chip client*/
	thermal_data *data = i2c_get_clientdata(client);

	mutex_lock(&kennisis_thermal_mutex);
	sscanf(buffer, "%d", &value);
	if (value > 255) return 0;

	data->fan_algo_enabled = FAN_ALGO_DISABLE;
	set_pwm(value);
	mutex_unlock(&kennisis_thermal_mutex);

	return count;
}

static ssize_t ambient1_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
#ifdef CONFIG_REDSTONE
	data = lm75_update_device(1, lm75_p2020);
#else
	data = lm75_update_device(1, lm75_ambient1);
#endif
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[0]));
}

static ssize_t ambient2_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
#ifdef CONFIG_REDSTONE
	data = lm75_update_device(1, lm75_bcm56846);
#else
	data = lm75_update_device(1, lm75_ambient2);
#endif
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[1]));
}

#ifdef CONFIG_REDSTONE
static ssize_t LIA_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data = lm75_update_device(1, lm75_LIA);
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[2]));
}

static ssize_t RIA_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data = lm75_update_device(1, lm75_RIA);
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[3]));
}
#endif

static ssize_t systemoutlet_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	data =  i2c_get_clientdata(thermalclient[0]);
//redstone:ROA, kennisis:systemoutlet
	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[4]));
}

static ssize_t p2020_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
#ifdef CONFIG_REDSTONE
	data = lm75_update_device(1, lm75_FIA);
#else
	data = lm75_update_device(1, lm75_p2020);
#endif
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[5]));
}

static ssize_t psu1_status_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	mutex_lock(&kennisis_thermal_mutex);
	if (thermalclient[psu1] == NULL) {
		mutex_unlock(&kennisis_thermal_mutex);
		return sprintf(buf, "%d\n", 0);//psu is not present
	} else {
		if ((0x08 & read_cpld("reset", 0x80)) == 0){
			mutex_unlock(&kennisis_thermal_mutex);
			return sprintf(buf, "%d\n", 2); //Power not OK
		}else {
			mutex_unlock(&kennisis_thermal_mutex);
			return sprintf(buf, "%d\n", 1);//psu is present
		}
	}
}

static ssize_t psu2_status_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	mutex_lock(&kennisis_thermal_mutex);
	if (thermalclient[psu2] == NULL) {
		mutex_unlock(&kennisis_thermal_mutex);
		return sprintf(buf, "%d\n", 0);//psu is not present
	} else {
		if ((0x04 & read_cpld("reset", 0x80)) == 0){
			mutex_unlock(&kennisis_thermal_mutex);
			return sprintf(buf, "%d\n", 2); // Power not OK
		} else {
		mutex_unlock(&kennisis_thermal_mutex);
		return sprintf(buf, "%d\n", 1);//psu is present
	}
}
}

static ssize_t psuinlet1_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	data =  i2c_get_clientdata(thermalclient[0]);

	return sprintf(buf, "%ld\n",
		       data->lm75_data.temp[psu1]);
}

static ssize_t psuinlet2_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	data =  i2c_get_clientdata(thermalclient[0]);

	return sprintf(buf, "%ld\n",
		       data->lm75_data.temp[psu2]);
}

#ifdef CONFIG_KENNISIS_VAR
static ssize_t tcam_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);


	data = lm75_update_device(1, lm75_tcam);

	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[6]));
}

static ssize_t bcm_temp_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data = lm75_update_device(1, lm75_bcm56634);
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n",
		       LM75_TEMP_FROM_REG(data->lm75_data.temp[7]));
}
#endif

/* datasheet says to divide this number by the fan reading to get fan rpm */
static inline int FAN_PERIOD_TO_RPM(int x)
{
	if (fan_chip_type == ADT7462_CHIP) {
		return ((90000 * 60) / (x));
	} else if (fan_chip_type == EMC2305_CHIP) {
		if (x > (0xf5c2 >> 3)) /*<1000rpm, fan fail*/
			return 0;
		return 3932160 * 2 / x;
	}

	return 0;
}

#ifdef CONFIG_REDSTONE
#define FAN1_INDEX 3
#else
#define FAN1_INDEX 1
#endif

static ssize_t fan1speed_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);
#if 0
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
	} else if (fan_chip_type == EMC2305_CHIP) {
		data = emc2305_update_device();
	} else{
		mutex_unlock(&kennisis_thermal_mutex);
		return 0;
	}
#endif
	mutex_unlock(&kennisis_thermal_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[FAN1_INDEX]));
}

#ifdef CONFIG_REDSTONE
#define FAN2_INDEX 1
#else
#define FAN2_INDEX 0
#endif
static ssize_t fan2speed_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);
#if 0
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
	} else if (fan_chip_type == EMC2305_CHIP) {
		data = emc2305_update_device();
	} else{
		mutex_unlock(&kennisis_thermal_mutex);
		return 0;
	}
#endif
	mutex_unlock(&kennisis_thermal_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[FAN2_INDEX]));
}

#ifdef CONFIG_REDSTONE
#define FAN3_INDEX 0
#else
#define FAN3_INDEX 2
#endif
static ssize_t fan3speed_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);
#if 0
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
	} else if (fan_chip_type == EMC2305_CHIP) {
		data = emc2305_update_device();
	} else{
		mutex_unlock(&kennisis_thermal_mutex);
		return 0;
	}
#endif
	mutex_unlock(&kennisis_thermal_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[FAN3_INDEX]));
}


#ifdef CONFIG_REDSTONE /*5 fans in redstone*/
static ssize_t fan4speed_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);
#if 0
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
	} else if (fan_chip_type == EMC2305_CHIP) {
		data = emc2305_update_device();
	} else{
		mutex_unlock(&kennisis_thermal_mutex);
		return 0;
	}
#endif
	mutex_unlock(&kennisis_thermal_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[4]));
}

static ssize_t fan5speed_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;

	mutex_lock(&kennisis_thermal_mutex);
	data =  i2c_get_clientdata(thermalclient[0]);
#if 0
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
	} else if (fan_chip_type == EMC2305_CHIP) {
		data = emc2305_update_device();
	} else{
		mutex_unlock(&kennisis_thermal_mutex);
		return 0;
	}
#endif
	mutex_unlock(&kennisis_thermal_mutex);
	return sprintf(buf, "%d\n",
		       FAN_PERIOD_TO_RPM(data->fanchip_data.fanspeed[2]));
}
#endif

#if !defined (CONFIG_KENNISIS_VAR) && !defined(CONFIG_REDSTONE)    /*No voltage function in these two projects*/
#define MASK_AND_SHIFT(value, prefix)	\
	(((value) & prefix##_MASK) >> prefix##_SHIFT)

/* Multipliers are actually in uV, not mV. */
static int voltage_multiplier(FAN_CHIP  *data, int which)
{
	switch (which) {
	case 0:
		return 62500;//  12v
	case 1:
		return 26000;//  5v
	case 2:
		return 17200;//  3.3v
	case 3:
		return 13000; // 2.5v
	case 4:
		return 9400; //  1.8v
	case 5:
		return 7800; // 1.5v
	case 6:
		return 62500; // 1.2v
	case 7:
		return 7800; // 1.05v
	case 8:
		return 7800; // 1v
	defalut:
		return 0;
	}
}

static ssize_t vol1_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 0);
		x *=  data->fanchip_data.volt[0];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}

	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol2_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 1);
		x *=  data->fanchip_data.volt[1];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol3_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 2);
		x *=  data->fanchip_data.volt[2];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol4_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 3);
		x *=  data->fanchip_data.volt[3];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol5_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 4);
		x *=  data->fanchip_data.volt[4];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol6_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 5);
		x *=  data->fanchip_data.volt[5];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol7_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 6);
		x *=  data->fanchip_data.volt[6];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol8_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(& (data->fanchip_data), 7);
		x *=  data->fanchip_data.volt[7];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}

static ssize_t vol9_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
 struct class_attribute *attr,
#endif
 char *buf)
{
	thermal_data *data;
	int x;

	mutex_lock(&kennisis_thermal_mutex);
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
		x = voltage_multiplier(&(data->fanchip_data), 8);
		x *=  data->fanchip_data.volt[8];
		x /= 1000; /* convert from uV to mV */
	} else if (fan_chip_type == EMC2305_CHIP) {
		x = 0;
		printk( KERN_INFO NAME ":  EMC2305 chip not support voltage monitor");
	}
	mutex_unlock(&kennisis_thermal_mutex);

	return sprintf(buf, "%d\n", x);
}
#endif /*#if !defined (CONFIG_KENNISIS_VAR || CONFIG_REDSTONE)  */

/*fan speed attr */
#ifdef CONFIG_REDSTONE
/*change the sys file name, but share the top six temp function with Kennisis, so the sys name may not match
the function name. */
static CLASS_ATTR(p2020_temp, S_IRUGO | S_IWUSR, ambient1_temp_show, NULL);
static CLASS_ATTR(bcm56846_temp, S_IRUGO | S_IWUSR, ambient2_temp_show, NULL);
static CLASS_ATTR(LIA_temp, S_IRUGO | S_IWUSR, LIA_temp_show, NULL);
static CLASS_ATTR(RIA_temp, S_IRUGO | S_IWUSR, RIA_temp_show, NULL);
static CLASS_ATTR(ROA_temp, S_IRUGO | S_IWUSR, systemoutlet_temp_show, NULL);
static CLASS_ATTR(FIA_temp, S_IRUGO | S_IWUSR, p2020_temp_show, NULL);
static CLASS_ATTR(psuinlet1_temp, S_IRUGO | S_IWUSR, psuinlet1_temp_show, NULL);
static CLASS_ATTR(psuinlet2_temp, S_IRUGO | S_IWUSR, psuinlet2_temp_show, NULL);
static CLASS_ATTR(psu1_status, S_IRUGO | S_IWUSR, psu1_status_show, NULL);
static CLASS_ATTR(psu2_status, S_IRUGO | S_IWUSR, psu2_status_show, NULL);
#else
static CLASS_ATTR(ambient1_temp, S_IRUGO | S_IWUSR, ambient1_temp_show, NULL);
static CLASS_ATTR(ambient2_temp, S_IRUGO | S_IWUSR, ambient2_temp_show, NULL);
static CLASS_ATTR(systemoutlet_temp, S_IRUGO | S_IWUSR, systemoutlet_temp_show, NULL);
static CLASS_ATTR(p2020_temp, S_IRUGO | S_IWUSR, p2020_temp_show, NULL);
static CLASS_ATTR(tcam_temp, S_IRUGO | S_IWUSR, tcam_temp_show, NULL);
static CLASS_ATTR(bcm_temp, S_IRUGO | S_IWUSR, bcm_temp_show, NULL);
static CLASS_ATTR(psuinlet1_temp, S_IRUGO | S_IWUSR, psuinlet1_temp_show, NULL);
static CLASS_ATTR(psuinlet2_temp, S_IRUGO | S_IWUSR, psuinlet2_temp_show, NULL);
static CLASS_ATTR(psu1_status, S_IRUGO | S_IWUSR, psu1_status_show, NULL);
static CLASS_ATTR(psu2_status, S_IRUGO | S_IWUSR, psu2_status_show, NULL);
#endif

static CLASS_ATTR(fan1speed, S_IRUGO | S_IWUSR, fan1speed_show, NULL);
static CLASS_ATTR(fan2speed, S_IRUGO | S_IWUSR, fan2speed_show, NULL);
static CLASS_ATTR(fan3speed, S_IRUGO | S_IWUSR, fan3speed_show, NULL);
#ifdef CONFIG_REDSTONE
static CLASS_ATTR(fan4speed, S_IRUGO | S_IWUSR, fan4speed_show, NULL);
static CLASS_ATTR(fan5speed, S_IRUGO | S_IWUSR, fan5speed_show, NULL);
#endif

#if !defined (CONFIG_KENNISIS_VAR) && !defined(CONFIG_REDSTONE)
static CLASS_ATTR(volt_12v, S_IRUGO | S_IWUSR, vol1_show, NULL);
static CLASS_ATTR(volt_5v, S_IRUGO | S_IWUSR, vol2_show, NULL);
static CLASS_ATTR(volt_3v3, S_IRUGO | S_IWUSR, vol3_show, NULL);
static CLASS_ATTR(volt_2v5, S_IRUGO | S_IWUSR, vol4_show, NULL);
static CLASS_ATTR(volt_1v8, S_IRUGO | S_IWUSR, vol5_show, NULL);
static CLASS_ATTR(volt_1v5, S_IRUGO | S_IWUSR, vol6_show, NULL);
static CLASS_ATTR(volt_1v2, S_IRUGO | S_IWUSR, vol7_show, NULL);
static CLASS_ATTR(volt_1v05, S_IRUGO | S_IWUSR, vol8_show, NULL);
static CLASS_ATTR(volt_1v, S_IRUGO | S_IWUSR, vol9_show, NULL);
#endif

static struct class_attribute *attrgroup[] = {
#ifdef CONFIG_REDSTONE
	&class_attr_p2020_temp,
	&class_attr_bcm56846_temp,
	&class_attr_LIA_temp,
	&class_attr_RIA_temp,
	&class_attr_ROA_temp,
	&class_attr_FIA_temp,
	&class_attr_psuinlet1_temp,
	&class_attr_psuinlet2_temp,
	&class_attr_psu1_status,
	&class_attr_psu2_status,
#else
	&class_attr_ambient1_temp,
	&class_attr_ambient2_temp,
	&class_attr_systemoutlet_temp,
	&class_attr_p2020_temp,
	&class_attr_tcam_temp,
	&class_attr_bcm_temp,
	&class_attr_psuinlet1_temp,
	&class_attr_psuinlet2_temp,
	&class_attr_psu1_status,
	&class_attr_psu2_status,
#endif
	&class_attr_fan1speed,
	&class_attr_fan2speed,
	&class_attr_fan3speed,
#ifdef CONFIG_REDSTONE
	&class_attr_fan4speed,
	&class_attr_fan5speed,
#endif

#if !defined (CONFIG_KENNISIS_VAR) && !defined(CONFIG_REDSTONE)
	&class_attr_volt_12v,
	&class_attr_volt_5v,
	&class_attr_volt_3v3,
	&class_attr_volt_2v5,
	&class_attr_volt_1v8,
	&class_attr_volt_1v5,
	&class_attr_volt_1v2,
	&class_attr_volt_1v05,
	&class_attr_volt_1v,
#endif
};

#ifdef CONFIG_KENNISIS_VAR
#define NUM_THERMAL_FILES 13
#elif  defined CONFIG_REDSTONE
#define NUM_THERMAL_FILES 15
#else
#define NUM_THERMAL_FILES 24
#endif

#if 0
static CLASS_ATTR(ambient1_min_duty, S_IRUGO | S_IWUSR, NULL, ambient1_min_duty_store);
static CLASS_ATTR(ambient1_max_duty, S_IRUGO | S_IWUSR, NULL, ambient1_max_duty_store);
static CLASS_ATTR(ambient2_min_duty,  S_IWUSR | S_IRUGO, NULL, ambient2_min_duty_store);
static CLASS_ATTR(ambient2_max_duty,  S_IWUSR | S_IRUGO, NULL, ambient2_max_duty_store);
static CLASS_ATTR(p2020_min_duty,  S_IWUSR | S_IRUGO, NULL, p2020_min_duty_store);
static CLASS_ATTR(p2020_max_duty,  S_IWUSR | S_IRUGO, NULL, p2020_max_duty_store);
static CLASS_ATTR(tcam_min_duty, S_IRUGO | S_IWUSR, NULL, tcam_min_duty_store);
static CLASS_ATTR(tcam_max_duty, S_IRUGO | S_IWUSR, NULL, tcam_max_duty_store);
static CLASS_ATTR(bcm56634_min_duty,  S_IWUSR | S_IRUGO, NULL, bcm56634_min_duty_store);
static CLASS_ATTR(bcm56634_max_duty,  S_IWUSR | S_IRUGO, NULL, bcm56634_max_duty_store);


static CLASS_ATTR(ambient1_low_temp,  S_IWUSR | S_IRUGO, NULL, ambient1_low_temp_store);
static CLASS_ATTR(ambient1_high_temp,  S_IWUSR | S_IRUGO, NULL, ambient1_high_temp_store);
static CLASS_ATTR(ambient2_low_temp,  S_IWUSR | S_IRUGO, NULL, ambient2_low_temp_store);
static CLASS_ATTR(ambient2_high_temp,  S_IWUSR | S_IRUGO, NULL, ambient2_high_temp_store);
static CLASS_ATTR(p2020_low_temp,  S_IWUSR | S_IRUGO, NULL, p2020_low_temp_store);
static CLASS_ATTR(p2020_high_temp,  S_IWUSR | S_IRUGO, NULL, p2020_high_temp_store);
static CLASS_ATTR(tcam_low_temp,  S_IWUSR | S_IRUGO, NULL, tcam_low_temp_store);
static CLASS_ATTR(tcam_high_temp,  S_IWUSR | S_IRUGO, NULL, tcam_high_temp_store);
static CLASS_ATTR(bcm_low_temp,  S_IWUSR | S_IRUGO, NULL, bcm_low_temp_store);
static CLASS_ATTR(bcm_high_temp,  S_IWUSR | S_IRUGO, NULL, bcm_high_temp_store);
#endif
static CLASS_ATTR(manual_pwm,  S_IWUSR | S_IRUGO, NULL, manual_pwm_store);

static struct class_attribute *attrgroup1[] = {
#if 0  /*for debug ,remove it*/
	/*pwm duty cycle min and max for each sensor*/
	&class_attr_ambient1_min_duty,
	&class_attr_ambient1_max_duty,
	&class_attr_ambient2_min_duty,
	&class_attr_ambient2_max_duty,
	&class_attr_p2020_min_duty,
	&class_attr_p2020_max_duty,
	&class_attr_tcam_min_duty,
	&class_attr_tcam_max_duty,
	&class_attr_bcm56634_min_duty,
	&class_attr_bcm56634_max_duty,


	/*low temp and high temp for each sensor*/
	&class_attr_ambient1_low_temp,
	&class_attr_ambient1_high_temp,
	&class_attr_ambient2_low_temp,
	&class_attr_ambient2_high_temp,
	&class_attr_p2020_low_temp,
	&class_attr_p2020_high_temp,
	&class_attr_tcam_low_temp,
	&class_attr_tcam_high_temp,
	&class_attr_bcm_low_temp,
	&class_attr_bcm_high_temp,
#endif

// manually control pwm
	&class_attr_manual_pwm,
};

#define NUM_THERMAL_DEBUG_FILES  1//sizeof(attrgroup1)/sizeof(class_attribute)

/*Create all i2c client here*/
static int	 Create_All_Client(struct i2c_client *new_client)
{
	int  i;
	unsigned char flag;
#ifdef CONFIG_REDSTONE
	int first_client = lm75_p2020;
	int adap2_client = lm75_LIA;
	int adap1_client = lm75_RIA;
#else
	int first_client = lm75_ambient1;
	int adap2_client = lm75_ambient1;
	int adap1_client = lm75_ambient2;
#endif
	struct i2c_client * tmpclient;
	struct i2c_adapter *adapter2 = thermalclient[adap2_client]->adapter; /*bus2*/
	struct i2c_adapter *adapter1 = new_client->adapter;                           /*bus1*/
	thermal_data *data = i2c_get_clientdata(new_client);

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "Create all clients here\n");
#endif
	thermalclient[adap1_client] = new_client;
	i2c_set_clientdata(thermalclient[adap1_client], data);
	i2c_set_clientdata(thermalclient[adap2_client], data);

	for (i = first_client; i < chipcounts; i++) {
		if (thermalclient[i] != NULL)
			continue;
		if (i == psu1) {
#ifdef CONFIG_REDSTONE
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x20) != 0) //ruyi modify(0x08)
#endif
#ifdef CONFIG_KENNISIS_VAR
				flag = read_cpld("reset", 0x2e);
			if ((flag & 0x02) == 0)
#endif
			{
				thermalclient[i] = NULL;
				continue;
			}
		}
		if (i == psu2) {
#ifdef CONFIG_REDSTONE
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x10) != 0) //ruyi modify(0x04)
#endif
#ifdef CONFIG_KENNISIS_VAR
				flag = read_cpld("reset", 0x2e);
			if ((flag & 0x01) == 0)
#endif
			{
				thermalclient[i] = NULL;
				continue;
			}
		}
		if (busnum[i] == I2C_BUS2)
			tmpclient =  i2c_new_device(adapter2, &thermal_i2c_devices[i]);
		else
			tmpclient =  i2c_new_device(adapter1, &thermal_i2c_devices[i]);
		if (tmpclient == NULL ) {
#ifdef THERMAL_DEBUG
			printk( KERN_ALERT NAME "add i2c new device error!,i=%d, addr=%p\n", i, &thermal_i2c_devices[i]);
#endif
			printk(KERN_ALERT NAME "i2c device %p can not register!\n", &thermal_i2c_devices[i]);
			continue;
		}
		i2c_set_clientdata(tmpclient, data);
		thermalclient[i] = tmpclient;
	}
#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "Create all clients return correct!\n");
#endif
	return 0;
}


static irqreturn_t kennisis_lm75_irq(int irq, void *dev_id)
{

	disable_irq_nosync(irq);
	//schedule_work(&ds1307->work);
	//printk( KERN_ALERT NAME "lm75 over temperature detect\n" );
	enable_irq(irq);
	return IRQ_HANDLED;
}

static int thermal_data_copy(thermal_debug_data *p1, thermal_debug_data *p2)
{
	p1->pwm_duty_min = p2->pwm_duty_min;
	p1->pwm_duty_max = p2->pwm_duty_max;
	p1->low_temp = p2->low_temp;
	p1->high_temp = p2->high_temp;
	//p1->high_warning = p2->high_warning;
	//p1->high_critical = p2->high_critical;

	return 0;
}
static int thermal_temp_change(thermal_debug_data *p1, thermal_debug_data *p2)
{
	p1->high_warning = p2->high_warning;
	p1->high_critical = p2->high_critical;

	return 0;
}
/*Check All LM75 Chips*/
static int KENNISIS_IRQ_TEMP;  /*Kennisis temp sensor interrupt,from CPLD*/
#ifdef CONFIG_REDSTONE
extern int read_hw();
#endif
int wind_redirect = 0; //1:f->b 2:b->f
static int LM75_Init(void)
{
	int cur, conf, hyst;
	int i, j;
	struct i2c_client *new_client;
	thermal_data *data;
	u8 value[32];//zmzhan add
	u8 temp;//zmzhan add
	u8 temp1;
	int wind_direction_hw=0;
	int retval, count = 10;
	unsigned long local_jiffies = jiffies;

	data =  i2c_get_clientdata(thermalclient[0]);
	data->lm75_data.lm75_last_update = local_jiffies;
#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "LM75 init here\n");
#endif
	for (i = 0; i < lm75counts; i++) {
#ifdef CONFIG_REDSTONE
		if (i == lm75_LIA || i == lm75_ROA || i == psu1 || i == psu2)
#else
		if (i == lm75_ambient1 || i == psu1 || i == psu2)
#endif
		{
			data->lm75_data.fan_control_enable[i] = FAN_CONTROL_ENABLE; /*Totally 8 sensors, just 5 of them can control fan*/
		} else {
			data->lm75_data.fan_control_enable[i] = FAN_CONTROL_DISABLE;
		}
	}

	for (i = 0; i < lm75counts; i++) {
		data->lm75_data.temp_trend[i] = UP_TREND; /*Default UP_TREND*/
		data->lm75_data.last_trend[i] = UP_TREND;//zmzhan add
	}
	for (i = 0; i < lm75counts; i++) {
		data->lm75_data.oldtemp[i] = 25;
		data->last_pwm[i] = 60;
	}
	for (i = 0; i < lm75counts; i++) {
		if (i != psu1 && i != psu2) {
			new_client = thermalclient[i];
			/* Unused addresses */
			cur = i2c_smbus_read_word_data(new_client, 0);
			msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
			if (cur < 0)
				printk( KERN_ALERT NAME "LM75 init read cur error!\n");
#endif
			conf = i2c_thermal_read_byte_data(new_client, 1);
			msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
			if (conf < 0)
				printk( KERN_ALERT NAME "LM75 init read conf error!\n");
#endif
			hyst = i2c_smbus_read_word_data(new_client, 2);
			msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
			if (hyst < 0)
				printk( KERN_ALERT NAME "LM75 init read hyst error!\n");
#endif
		}
		else 
		{
			//add psu fan direction
			if (thermalclient[i] != NULL) 
			{
				retval = i2c_smbus_read_i2c_block_data(thermalclient[i], 0x9a, 14, value);
				while((retval < 0 || value[11] == 0xff) && count-- > 0) {
					msleep(11);
					retval = i2c_smbus_read_i2c_block_data(thermalclient[i], 0x9a, 14, value);
				}
				count = 10;
				temp = value[11];
				temp1 = value[13];

				
				/* 0x80    STATUS_MFR_SPECIFIC  */
				wind_direction_hw = i2c_thermal_read_byte_data(thermalclient[i],0x80); 
				if (wind_direction_hw < 0){
					wind_direction_hw = 0;
				}
				else {
					wind_direction_hw &= 0x03; /*wind_direction_hw:0x02,f-->b; 0x01,b-->f*/
					if (wind_direction_hw != 0x01 || wind_direction_hw != 0x02)
						wind_direction_hw = 0;
				}
				
                /* BEFORE,judge wind direction via ID info at Reg 0x9a, while ,from NOW, this also is done by bit 0 and bit 1
				at Reg 0x80. For compatiblity, add "wind_direction_hw", same function with "wind_redirect". 
                Hope no hardware change any longer.	*/
				if ((wind_direction_hw&0x02) || ((temp == 0x44)||(temp == 0x47) ||
					(temp == 0x32 && temp1 == 0x41)||
					(temp == 0x36 && temp1 == 0x41)||
					(temp == 0x32 && temp1 == 0x42)))
				{
					temp = 0;
					/*Consider such a case: 
					for some reason, HW changes the wind direction bit (0x80,bit[0])to be 1 in PSU ,
					which wind direction is assumed as front2back(f-->b), so this "if" statement 
					can address such ODD case. Chris wang */
					if ((wind_direction_hw&0x01)||(wind_redirect == 2)) {
						//printk(KERN_ALERT "ERROR: Two PSUSs wind are different!\n");
						continue;
					}
					wind_redirect = 1;
					//printk(KERN_ALERT "Wind direction is f->b\n");//zmzhan add
					thermal_data_copy(&algo_para0[psu2], &algo_para0[psu1]);
					thermal_data_copy(&algo_para1[psu2], &algo_para1[psu1]);
#ifdef CONFIG_REDSTONE
					if (2 != read_hw()){
#endif
						for(j = 0; j < lm75counts; j++) {
							thermal_temp_change(&algo_para1[j], &algo_para0[j]);
						}
#ifdef CONFIG_REDSTONE
					}
					else {
						algo_para0[0].high_warning = 74; algo_para0[0].high_critical = 79;
						algo_para0[1].high_warning = 86; algo_para0[1].high_critical = 91;
						algo_para0[2].high_warning = 55; algo_para0[2].high_critical = 59;
						algo_para0[3].high_warning = 55; algo_para0[3].high_critical = 59;
						algo_para0[4].high_warning = 56; algo_para0[4].high_critical = 60;
						for(j = 0; j < lm75counts; j++) {
							thermal_temp_change(&algo_para1[j], &algo_para0[j]);
						}
					}
#endif
#ifdef CONFIG_REDSTONE
					data->lm75_data.fan_control_enable[lm75_ROA] = FAN_CONTROL_DISABLE;
#endif
				}
				else { //if(temp == 0x43 || temp == 0x42) //psu type: KBB or KBC
					temp = 0;
					
					/*Case: HW changes the wind direction bit(0x80, bit[1]) to be 1 in PSU
					. Chris wang */
					if ((wind_direction_hw&0x02)||(wind_redirect == 1)) {
						//printk(KERN_ALERT "ERROR: Two PSUSs wind are different!\n");
						continue;
					}
					wind_redirect = 2;
					//printk(KERN_ALERT "Wind direction is b->f\n");//zmzhan add
					thermal_data_copy(&algo_para0[psu1], &algo_para0[psu2]);
					thermal_data_copy(&algo_para1[psu1], &algo_para1[psu2]);
#ifdef CONFIG_REDSTONE
					if (2 != read_hw()){
#endif
						for(j = 0; j < lm75counts; j++) {
							thermal_temp_change(&algo_para0[j], &algo_para1[j]);
						}
#ifdef CONFIG_REDSTONE
					}
					else {
						algo_para1[0].high_warning = 77; algo_para1[0].high_critical = 83;
						algo_para1[1].high_warning = 80; algo_para1[1].high_critical = 85;
						algo_para1[2].high_warning = 55; algo_para1[2].high_critical = 58;
						algo_para1[3].high_warning = 55; algo_para1[3].high_critical = 58;
						algo_para1[4].high_warning = 50; algo_para1[4].high_critical = 51;
						for(j = 0; j < lm75counts; j++) {
							thermal_temp_change(&algo_para0[j], &algo_para1[j]);
						}
					}
#endif
#ifdef CONFIG_REDSTONE
					data->lm75_data.fan_control_enable[lm75_LIA] = FAN_CONTROL_DISABLE;
#endif
				}
			}
		}
	}

	if (request_irq(KENNISIS_IRQ_TEMP, kennisis_lm75_irq, 0,
			"over temp", data) < 0) {
		printk( KERN_ALERT NAME "lm75 request irq fail\n" );
	}

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "LM75 init return correct!\n");
#endif
	return 0;
}

static irqreturn_t kennisis_fanchip_irq(int irq, void *dev_id)
{
	//struct i2c_client	*client = dev_id;
	//thermal_data	*data = i2c_get_clientdata(client);
	disable_irq_nosync(irq);
	//schedule_work(&ds1307->work);
	//printk( KERN_ALERT NAME "adt7462 interrupt detect\n" );
	enable_irq(irq);
	return IRQ_HANDLED;
}

/*Check ADT7462 Chip*/
#ifdef CONFIG_REDSTONE
#define MAX_PWM 100
#define MIN_PWM  60
#define PSU1_MAX_PWM 65
#define PSU2_MAX_PWM 55
#else
#define MAX_PWM 90
#define MIN_PWM  40
#define PSU1_MAX_PWM 35
#endif
static int  KENNISIS_IRQ_FANCHIP;/*Kennisis ADT7462 interrupt,from CPLD*/
static int ADT7462_Init(void)
{
	struct i2c_client *client ;
	thermal_data *data;
	int vendor, device, revision;
	unsigned long local_jiffies = jiffies;
	int i;
	int reg1;

	client = thermalclient[chipcounts - 1]; /*fan chip client*/
	data =  i2c_get_clientdata(client);
	data->fanchip_data.adt_last_update = local_jiffies;

	vendor = i2c_thermal_read_byte_data(client, ADT7462_REG_VENDOR);
	if (vendor != ADT7462_VENDOR)
		goto error_adt7462;

	device = i2c_thermal_read_byte_data(client, ADT7462_REG_DEVICE);
	if (device != ADT7462_DEVICE)
		goto error_adt7462;

	revision = i2c_thermal_read_byte_data(client,
					    ADT7462_REG_REVISION);
	if (revision != ADT7462_REVISION)
		goto error_adt7462;

//bobbie del temp 0321
	//set_pwm(MAX_PWM);
	for (i = 0; i < ADT7462_PIN_CFG_REG_COUNT; i++) {
		data->fanchip_data.pin_cfg[i] = i2c_thermal_read_byte_data(client,
						ADT7462_REG_PIN_CFG(i));
	}
	data->fanchip_data.fan_fail = 0;
	i2c_smbus_write_byte_data(client, 0x14,  0x00);
	i2c_smbus_write_byte_data(client, 0xbb,  0x00);
	i2c_smbus_write_byte_data(client, 0xbc,  0x00);
	i2c_smbus_write_byte_data(client, 0xc3,  0x00);
	i2c_smbus_write_byte_data(client, 0xc4,  0x00);
	//bobbie add 0322 for setting TACH 1 2 3
	i2c_smbus_write_byte_data(client, 0x07,  0x07);
	i2c_smbus_write_byte_data(client, 0x1D, 0x07);

	/*pwm min and max*/
	i2c_smbus_write_byte_data(client, 0x28,  0x00);
	i2c_smbus_write_byte_data(client, 0x29,  0x00);
	i2c_smbus_write_byte_data(client, 0x2a,  0x00);
	i2c_smbus_write_byte_data(client, 0x2b,  0x00);
	i2c_smbus_write_byte_data(client, 0x2c,  0xff);

	/*pwm high frequency*/
	i2c_smbus_write_byte_data(client, 0x02,  0x44);

	//set manul pwm
	i2c_smbus_write_byte_data(client, 0x21, 0xF1);
	i2c_smbus_write_byte_data(client, 0x22, 0xF1);
	i2c_smbus_write_byte_data(client, 0x23, 0xF1);

	// i2c_smbus_write_byte_data(client,
	//ADT7462_REG_PWM(0),0x80);//50%
	//  i2c_smbus_write_byte_data(client,
	//	ADT7462_REG_PWM(1),51);//20%
	//	  i2c_smbus_write_byte_data(client,
	//	ADT7462_REG_PWM(2),26);//10%
	/*add pin configure here*/
	//bobbie add 0321
	//pin7
	data->fanchip_data.pin_cfg[0] = data->fanchip_data.pin_cfg[0] & 0xfe;
	data->fanchip_data.pin_cfg[0] = data->fanchip_data.pin_cfg[0] & 0xbf;

	//pin15 2.5v
	data->fanchip_data.pin_cfg[1] = data->fanchip_data.pin_cfg[1] & 0xdf;
	//pin21
	data->fanchip_data.pin_cfg[1] = data->fanchip_data.pin_cfg[1] & 0xf7;
	//pin23 bit1 1 bit0 0 ---1.8v
	data->fanchip_data.pin_cfg[1] = data->fanchip_data.pin_cfg[1] & 0xfe;
	data->fanchip_data.pin_cfg[1] = data->fanchip_data.pin_cfg[1] | 0x02;

	//pin24 bit7 1 bit6 1 --1.5v
	data->fanchip_data.pin_cfg[2] = data->fanchip_data.pin_cfg[2] | ADT7462_PIN24_MASK;
	//pin25 bit5 0 bit4 0  --3.3v
	data->fanchip_data.pin_cfg[2] = data->fanchip_data.pin_cfg[2] & 0xcf;
	//pin26 bit3 0 bit2 1 --1.2v
	data->fanchip_data.pin_cfg[2] = data->fanchip_data.pin_cfg[2] & 0xf7;
	data->fanchip_data.pin_cfg[2] = data->fanchip_data.pin_cfg[2] | 0x04;

	//pin28 bit7 0 bit6 1 --1.5v
	data->fanchip_data.pin_cfg[3] = data->fanchip_data.pin_cfg[3] & 0x7f;
	data->fanchip_data.pin_cfg[3] = data->fanchip_data.pin_cfg[3] | 0x40;
	//pin29 bit5 0  bit4 1 --1.5v
	data->fanchip_data.pin_cfg[3] = data->fanchip_data.pin_cfg[3] & 0xdf;
	data->fanchip_data.pin_cfg[3] = data->fanchip_data.pin_cfg[3] | 0x10;


	for (i = 0; i < ADT7462_PIN_CFG_REG_COUNT; i++) {
		i2c_smbus_write_byte_data(client, ADT7462_REG_PIN_CFG(i),  data->fanchip_data.pin_cfg[i]);
	}

	/*setup complete*/
	reg1 = i2c_thermal_read_byte_data(client,
					0x01);
	reg1 = reg1 | 0x20;
	i2c_smbus_write_byte_data(client, 0x01,  reg1);

	return 0;

error_adt7462:
	printk( KERN_ALERT NAME "ADT7462 chip init error");
	return -1;
}

static int EMC2305_Init(void)
{
	int status = 0;
	struct i2c_client *client ;

	client = thermalclient[fan_chip];
	/*min driver 20%*/
	status += i2c_smbus_write_byte_data(client, 0x38,  0x33);
	msleep(11);//zmzhan add
	status += i2c_smbus_write_byte_data(client, 0x48,  0x33);
	msleep(11);//zmzhan add
	status += i2c_smbus_write_byte_data(client, 0x58,  0x33);
	msleep(11);//zmzhan add
	status += i2c_smbus_write_byte_data(client, 0x68,  0x33);
	msleep(11);//zmzhan add
	status += i2c_smbus_write_byte_data(client, 0x78,  0x33);
	msleep(11);//zmzhan add
#ifdef THERMAL_DEBUG
	if (status < 0)
		printk(KERN_ALERT NAME "EMC2305_Init error!\n");
#endif
	return status;
}

static int Fan_Chip_Init(void)
{
	struct i2c_client *client ;
	thermal_data *data;
	int vendor, device;
	int status = 0;

	client = thermalclient[fan_chip]; /*fan chip client*/
	data =  i2c_get_clientdata(client);
	vendor = i2c_thermal_read_byte_data(client, ADT7462_REG_VENDOR);
	device = i2c_thermal_read_byte_data(client, ADT7462_REG_DEVICE);
#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "Fan chip init here\n");
#endif
	if (vendor == ADT7462_VENDOR && device == ADT7462_DEVICE) {
#ifdef THERMAL_DEBUG
		printk( KERN_ALERT NAME "Fan chip is ADT7462\n");
#endif
		status = ADT7462_Init();
		fan_chip_type = ADT7462_CHIP;
		goto exit;
	}

	vendor = i2c_thermal_read_byte_data(client, EMC2305_REG_VENDOR);
	device = i2c_thermal_read_byte_data(client, EMC2305_REG_DEVICE);

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME "vendor=%x, device=%x\n", vendor, device);
#endif
	if (vendor == EMC2305_VENDOR && device == EMC2305_DEVICE) {
#ifdef THERMAL_DEBUG
		printk( KERN_ALERT NAME "Fan chip is EMC2305\n");
#endif
		status = EMC2305_Init();
		fan_chip_type = EMC2305_CHIP;
		goto exit;
	}
exit:
	if (request_irq(KENNISIS_IRQ_FANCHIP, kennisis_fanchip_irq, 0,
			"fan chip interrupt", data) < 0) {
		printk( KERN_ALERT NAME "fan chip request irq fail\n" );
	}
	/*initial max speed*/
	set_pwm(100 * 255 / 100);

	return status;
}

//#define THERMAL_SIMULATION  /*open this to simulate temperature change to prove the algorithm*/
#ifdef THERMAL_SIMULATION
static long sim_temp = 0xf00;
static int down_flag = 0;//zmzhan add
static int sim_index = 0;
#endif

static void calcuate_target_pwm(thermal_data *data)
{
	int i;
	unsigned char flag;
	int pwm_duty_min, pwm_duty_max, low_temp, high_temp;
	int k;
	long temp;//zmzhan add
#ifdef CONFIG_REDSTONE
	struct i2c_adapter *adapter2 = thermalclient[lm75_LIA]->adapter;
	struct i2c_adapter *adapter1 = thermalclient[lm75_RIA]->adapter;
#else
	struct i2c_adapter *adapter2 = thermalclient[lm75_ambient1]->adapter;
	struct i2c_adapter *adapter1 = thermalclient[lm75_ambient2]->adapter;
#endif
	// LM75 lm75data;
	//  ADT7462 adt7462data;
	//lm75data = data ->lm75_data;

	/*calcuate the pwm value for each sensor*/
	//printk(KERN_ALERT "calcuate_target_pwm run here.\n");//zmzhan add
	data->target_pwm_setting = 0;
	for (i = 0; i < lm75counts; i++) {
		if (i == psu1) {
#ifdef CONFIG_REDSTONE
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x20) != 0)
#endif
#ifdef CONFIG_KENNISIS_VAR
				flag = read_cpld("reset", 0x2e);
			if ((flag & 0x02) == 0)
#endif
			{
				if (thermalclient[i] != NULL) {
					i2c_unregister_device(thermalclient[i]);
					thermalclient[i] = NULL;
					//printk(KERN_ALERT "psu1 is not present now!\n");
				}
				continue;
			} else if (thermalclient[i] == NULL) {
				if (busnum[i] == I2C_BUS2)
					thermalclient[i] = i2c_new_device(adapter2, &thermal_i2c_devices[i]);
				else
					thermalclient[i] = i2c_new_device(adapter1, &thermal_i2c_devices[i]);
				if (thermalclient[i] == NULL) {
					//printk(KERN_ALERT "add psu1 device error!\n");
					return;
				}
				i2c_set_clientdata(thermalclient[i], data);
				//printk(KERN_ALERT "add psu1 device!\n");
				continue;
			}
		}
		if (i == psu2) {
#ifdef CONFIG_REDSTONE
			flag = read_cpld("reset", 0x80);
			if ((flag & 0x10) != 0)
#endif
#ifdef CONFIG_KENNISIS_VAR
				flag = read_cpld("reset", 0x2e);
			if ((flag & 0x01) == 0)
#endif
			{
				if (thermalclient[i] != NULL) {
					i2c_unregister_device(thermalclient[i]);
					thermalclient[i] = NULL;
					//printk(KERN_ALERT "psu2 is not present now!\n");
				}
				continue;
			} else if (thermalclient[i] == NULL) {
				if (busnum[i] == I2C_BUS2)
					thermalclient[i] = i2c_new_device(adapter2, &thermal_i2c_devices[i]);
				else
					thermalclient[i] = i2c_new_device(adapter1, &thermal_i2c_devices[i]);
				if (thermalclient[i] == NULL) {
					//printk(KERN_ALERT "add psu2 device error!\n");
					return;
				}
				i2c_set_clientdata(thermalclient[i], data);
				//printk(KERN_ALERT "add psu2 device!\n");
				continue;
			}
		}

		if (data->lm75_data.fan_control_enable[i] == FAN_CONTROL_ENABLE) {
			if (i != psu1 && i != psu2) {
				temp = LM75_TEMP_FROM_REG(data->lm75_data.temp[i]);
			} else {
				temp = data->lm75_data.temp[i];
			}

			if (wind_redirect == 1) {
				if (temp < algo_para0[i].high_temp) {
					pwm_duty_min = algo_para0[i].pwm_duty_min;
					pwm_duty_max = algo_para0[i].pwm_duty_max ;
					low_temp  = algo_para0[i].low_temp;
					high_temp = algo_para0[i].high_temp;
				} else {
					pwm_duty_min = algo_para1[i].pwm_duty_min;
					pwm_duty_max = algo_para1[i].pwm_duty_max ;
					low_temp  = algo_para1[i].low_temp;
					high_temp = algo_para1[i].high_temp;
				}
			} else if (wind_redirect == 2) {
				if (data->lm75_data.temp_trend[i] == UP_TREND) {
					if (temp < algo_para0[i].high_temp) {
						pwm_duty_min = algo_para0[i].pwm_duty_min;
						pwm_duty_max = algo_para0[i].pwm_duty_max ;
						low_temp  = algo_para0[i].low_temp;
						high_temp = algo_para0[i].high_temp;
					} else {
						pwm_duty_min = algo_para1[i].pwm_duty_min;
						pwm_duty_max = algo_para1[i].pwm_duty_max ;
						low_temp  = algo_para1[i].low_temp;
						high_temp = algo_para1[i].high_temp;
					}
				} else if (data->lm75_data.temp_trend[i] == DOWN_TREND) {
					if (i == psu1 || i == psu2) {
						if (temp < algo_para0[i].high_temp - 6) {
							pwm_duty_min = algo_para0[i].pwm_duty_min;
							pwm_duty_max = algo_para0[i].pwm_duty_max;
							low_temp  = algo_para0[i].low_temp - 6;
							high_temp = algo_para0[i].high_temp - 6;
						} else {
							pwm_duty_min = algo_para1[i].pwm_duty_min;
							pwm_duty_max = algo_para1[i].pwm_duty_max;
							low_temp  = algo_para1[i].low_temp - 6;
							high_temp = algo_para1[i].high_temp - 6;
						}
					}
					else if (i == lm75_ROA || i == lm75_LIA) {
						if (temp < algo_para0[i].high_temp - 3) {
							pwm_duty_min = algo_para0[i].pwm_duty_min;
							pwm_duty_max = algo_para0[i].pwm_duty_max;
							low_temp  = algo_para0[i].low_temp - 3;
							high_temp = algo_para0[i].high_temp - 3;
						} else {
							pwm_duty_min = algo_para1[i].pwm_duty_min;
							pwm_duty_max = algo_para1[i].pwm_duty_max;
							low_temp  = algo_para1[i].low_temp - 3;
							high_temp = algo_para1[i].high_temp - 3;
						}
					}
				}
			}
			if (data->lm75_data.temp_trend[i] == UP_TREND) {
				
				k = (pwm_duty_max - pwm_duty_min) * 10 / (high_temp - low_temp);

				if (i != psu1 && i != psu2) {
					data->target_pwm[i] = pwm_duty_min + k * (LM75_TEMP_FROM_REG(data->lm75_data.temp[i]) - low_temp) / 10;
					if (wind_redirect == 2) {
						if ((temp - data->up_delay[i]) <= 3) { //ruyi
	//						if ((temp - data->up_delay[i]) == 2) {
								if (data->last_pwm[i] < data->target_pwm[i]){
									if (data->target_pwm[i] > pwm_duty_max) 
										data->target_pwm[i] = pwm_duty_max;
									if (data->target_pwm[i] < pwm_duty_min) 
										data->target_pwm[i] = pwm_duty_min;
									data->target_pwm_setting = data->target_pwm[i];
									continue;
								}
	//							data->target_pwm_setting = data->target_pwm[i] = data->last_pwm[i];
	//							continue;
	//						}
							data->target_pwm_setting = data->target_pwm[i] = data->last_pwm[i];
							continue;
						}
					}

					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;

					if (data->target_pwm[i] < pwm_duty_min) data->target_pwm[i] = pwm_duty_min;
					if (data->target_pwm[i] > data->target_pwm_setting)
						data->target_pwm_setting = data->target_pwm[i];
				} else {
					data->target_pwm[i] = pwm_duty_min + k * (data->lm75_data.temp[i] - low_temp) / 10;
					if (wind_redirect == 2) {
						if ((temp - data->up_delay[i]) <= 6) { //ruyi
	//						if ((temp - data->up_delay[i]) == 2) {
								if (data->last_pwm[i] < data->target_pwm[i]){
									if (data->target_pwm[i] > pwm_duty_max) 
										data->target_pwm[i] = pwm_duty_max;
									if (data->target_pwm[i] < pwm_duty_min) 
										data->target_pwm[i] = pwm_duty_min;
									continue;
								}
								data->target_pwm[i] = data->last_pwm[i];
								continue;
	//						}
	//						data->target_pwm[i] = data->last_pwm[i];
	//						continue;
						}
					}

					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;
					if (data->target_pwm[i] < pwm_duty_min) data->target_pwm[i] = pwm_duty_min;
				}
			} else { /*down trend*/
				k = (pwm_duty_max - pwm_duty_min) * 10 / (high_temp - low_temp);
				if (i != psu1 && i != psu2) {
					data->target_pwm[i] = pwm_duty_min + k * (LM75_TEMP_FROM_REG(data->lm75_data.temp[i]) - low_temp) / 10;
					if (wind_redirect == 2) {
						if (temp >= algo_para1[i].high_temp) { //ruyi
							data->target_pwm_setting = data->target_pwm[i] = pwm_duty_max;
							continue;
						}

						if ((data->down_delay[i] - temp) <= 3) { //ruyi
							if (data->last_pwm[i] > data->target_pwm[i]){
								if (data->target_pwm[i] > pwm_duty_max) 
									data->target_pwm[i] = pwm_duty_max;
								if (data->target_pwm[i] < pwm_duty_min) 
									data->target_pwm[i] = pwm_duty_min;
								data->target_pwm_setting = data->target_pwm[i];
								continue;
							}
							data->target_pwm_setting = data->target_pwm[i] = data->last_pwm[i];
							continue;
						}
					}

					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;

					if (data->target_pwm[i] < pwm_duty_min) data->target_pwm[i] = pwm_duty_min;
					if (data->target_pwm[i] > data->target_pwm_setting)
						data->target_pwm_setting = data->target_pwm[i];
				} else {
					data->target_pwm[i] = pwm_duty_min + k * (data->lm75_data.temp[i] - low_temp) / 10;

					if (wind_redirect == 2) {
						if (temp >= algo_para1[i].high_temp) { //ruyi
							data->target_pwm[i] = pwm_duty_max;
							continue;
						}

						if ((data->down_delay[i] - temp) <= 6) { //ruyi
	//						if ((temp - data->up_delay[i]) == 2) {
								if (data->last_pwm[i] > data->target_pwm[i]){
									if (data->target_pwm[i] > pwm_duty_max) 
										data->target_pwm[i] = pwm_duty_max;
									if (data->target_pwm[i] < pwm_duty_min) 
										data->target_pwm[i] = pwm_duty_min;
									continue;
								}
								data->target_pwm[i] = data->last_pwm[i];
								continue;
	//						}
	//						data->target_pwm[i] = data->last_pwm[i];
	//						continue;
						}
					}

					if (data->target_pwm[i] > pwm_duty_max) data->target_pwm[i] = pwm_duty_max;

					if (data->target_pwm[i] < pwm_duty_min) data->target_pwm[i] = pwm_duty_min;
				}
			}
#ifdef THERMAL_SIMULATION
			if (i == sim_index) {
				printk(KERN_ALERT "sensor %d :\n ", i);
				printk(KERN_ALERT "temp=%d,k=%d,in=%d,pwm_duty_max=%d,pwm=%d\n ",
				       LM75_TEMP_FROM_REG(data->lm75_data.temp[i]), k, pwm_duty_min, pwm_duty_max, data->target_pwm[i]);
			}
#endif
		} else {
			data->target_pwm[i] = MIN_PWM;
		}
	}
#ifdef CONFIG_REDSTONE
	if (data->fanchip_data.fan_fail > 1)  data ->target_pwm_setting = MAX_PWM; /*max pwm when one or more fan fail*/
#else
	if (data->fanchip_data.fan_fail == 1)  data ->target_pwm_setting = MAX_PWM; /*max pwm when one or more fan fail*/
#endif
	set_pwm(data->target_pwm_setting * 255 / 100);
#ifdef THERMAL_SIMULATION
	printk(KERN_ALERT "pwm real value =%d \n ", data->target_pwm_setting);
#endif
	if (thermalclient[psu2] != NULL) {
#ifdef CONFIG_REDSTONE
		if (data->fanchip_data.fan_fail > 1)  data->target_pwm[psu2] = algo_para1[psu2].pwm_duty_max; /*max pwm when two or more fan fail*/
#else
		if (data->fanchip_data.fan_fail == 1)  data->target_pwm[psu2] = algo_para1[psu2].pwm_duty_max; /*max pwm when one or more fan fail*/
#endif
		set_psu_pwm(thermalclient[psu2], data->target_pwm[psu2]);
#ifdef THERMAL_SIMULATION
		printk(KERN_ALERT "psu2 pwm real value =%d \n ", data->target_pwm[psu2]);
#endif
	}
	if (thermalclient[psu1] != NULL) {
#ifdef CONFIG_REDSTONE
		if (data->fanchip_data.fan_fail > 1)  data->target_pwm[psu1] = algo_para1[psu1].pwm_duty_max; /*max pwm when two or more fan fail*/
#else
		if (data->fanchip_data.fan_fail == 1)  data->target_pwm[psu1] = algo_para1[psu1].pwm_duty_max; /*max pwm when one or more fan fail*/
#endif
		set_psu_pwm(thermalclient[psu1], data->target_pwm[psu1]);
#ifdef THERMAL_SIMULATION
		printk(KERN_ALERT "psu1 pwm real value =%d \n ", data->target_pwm[psu1]);
#endif
	}
}

#define TEMP_DETECT_INTERVAL   3000 /*15s interval*/
static void fan_delayed_work(struct work_struct *work)
{
	thermal_data *data;
	mutex_lock(&kennisis_thermal_mutex);
	//printk(KERN_ALERT "fan_delayed_work start run...\n");//zmzhan add
#ifndef THERMAL_SIMULATION
	data = lm75_update_device(1, lm75_ROA);
	data = lm75_update_device(0, lm75counts);
	data = psu_update_device();
#endif
	if (fan_chip_type == ADT7462_CHIP) {
		data = adt7462_update_device();
	} else if (fan_chip_type == EMC2305_CHIP) {
		data = emc2305_update_device();
	}
#ifdef THERMAL_SIMULATION
	if (sim_temp > 0x3c00) {
		//printk(KERN_ALERT "\ndown_flag = 1\n");//zmzhan add
		down_flag = 1;
		sim_temp = 0x3c00;
		//sim_index++;
	} else if (sim_temp <= 0x1400) {
		down_flag = 0;
		sim_temp = 0x1400;
		for (;;) {
			if (++sim_index >= lm75counts)
				sim_index = 0;
			if (data->lm75_data.fan_control_enable[sim_index] == FAN_CONTROL_ENABLE) {
				//printk(KERN_ALERT "sim_index = %d\n", sim_index);//zmzhan add
				break;
			}
		}
	}
	/*if (sim_index > lm75counts)
	{
		down_flag = (down_flag > 0) ? 0 : 1;
		if(down_flag == 0)
			sim_temp = 0x1500;
		else
			sim_temp = 0x10000;
		 sim_index=0;
	}*/
	data->lm75_data.temp[sim_index] = sim_temp;
	//data->lm75_data.temp_trend[sim_index] = UP_TREND;
	if (down_flag == 0)
		sim_temp += 0x0100; /*increase 1 degree each time*/
	else
		sim_temp -= 0x0100;
	data->fan_algo_enabled = FAN_ALGO_ENABLE;
#endif
	//printk(KERN_ALERT "compare fan_algo_enable...\n");//zmzhan add
	if (FAN_ALGO_ENABLE == data->fan_algo_enabled ) {
		//printk(KERN_ALERT "call calcute_ func...\n");//zmzhan add
		calcuate_target_pwm(data);
		/*change target_pwm_setting(0-100) to register setting(0_255)*/
		// set_pwm(data ->target_pwm_setting*255/100);
	}
	queue_delayed_work(polldev_wq, &delaywork,
			   msecs_to_jiffies(TEMP_DETECT_INTERVAL));
	mutex_unlock(&kennisis_thermal_mutex);
}

/*start work queue for fan control algorithm*/
static int start_delay_workqueue(void)
{
	int retval = 0;

	INIT_DELAYED_WORK(&delaywork, fan_delayed_work);
	//printk(KERN_ALERT NAME "delaywork INIT.\N");//ZMZHAN ADD
	if (!polldev_users) {
		polldev_wq = create_singlethread_workqueue("kennisis_polldevd");
		if (!polldev_wq) {
			printk(KERN_ERR " failed to create fan control workqueue\n");
			retval = -ENOMEM;
			goto out;
		}
	}
	polldev_users++;
out:
	return retval;
}

static int stop_delay_workqueue(void)
{
	cancel_delayed_work_sync(&delaywork);

	if (!--polldev_users)
		destroy_workqueue(polldev_wq);

	return 0;
}

static int  Kick_Fan_Control_Alogrithm(void)
{
	int status;

	status = start_delay_workqueue();
	if (status < 0 ) return -1;
	queue_delayed_work(polldev_wq, &delaywork,
			   msecs_to_jiffies(TEMP_DETECT_INTERVAL));

	return 0;
}

static int Stop_Fan_Control_Alogrithm(void)
{
	stop_delay_workqueue();

	return 0;
}

#if 0
static int load_algo_para(void)
{
	const struct firmware *fw = NULL;
	int status;
	struct device dev = thermalclient[0]->dev;
	const u8 *data;
	int pos;


	status = request_firmware(&fw, "thermal_config", &dev);

	if (fw == NULL) {
		printk( KERN_ALERT NAME "load firmware error");
		return -1;
	}

	data = fw ->data;

	for (pos = 0; pos < fw->size; pos++) {
		if (data[pos] == ':') {
			if (data[pos - 8] == 'a' && data[pos - 1] == '1') { /*ambient1*/
				algo_para[lm75_ambient1].pwm_duty_min = (data[pos + 1] - 30) * 10 + (data[pos + 2] - 30);
				algo_para[lm75_ambient1].pwm_duty_max = (data[pos + 4] - 30) * 100 + (data[pos + 5] - 30) * 10 + (data[pos + 6] - 30);
				algo_para[lm75_ambient1].low_temp = (data[pos + 8] - 30) * 10 + (data[pos + 9] - 30);
				algo_para[lm75_ambient1].high_temp = (data[pos + 10] - 30) * 10 + (data[pos + 11] - 30);
			} else if (data[pos - 8] == 'a' && data[pos - 1] == '2') { /*ambient2*/
				algo_para[lm75_ambient2].pwm_duty_min = (data[pos + 1] - 30) * 10 + (data[pos + 2] - 30);
				algo_para[lm75_ambient2].pwm_duty_max = (data[pos + 4] - 30) * 100 + (data[pos + 5] - 30) * 10 + (data[pos + 6] - 30);
				algo_para[lm75_ambient2].low_temp = (data[pos + 8] - 30) * 10 + (data[pos + 9] - 30);
				algo_para[lm75_ambient2].high_temp = (data[pos + 10] - 30) * 10 + (data[pos + 11] - 30);
			} else if (data[pos - 8] == 'p' ) { /*p2020*/
				algo_para[lm75_p2020].pwm_duty_min = (data[pos + 1] - 30) * 10 + (data[pos + 2] - 30);
				algo_para[lm75_p2020].pwm_duty_max = (data[pos + 4] - 30) * 100 + (data[pos + 5] - 30) * 10 + (data[pos + 6] - 30);
				algo_para[lm75_p2020].low_temp = (data[pos + 8] - 30) * 10 + (data[pos + 9] - 30);
				algo_para[lm75_p2020].high_temp = (data[pos + 10] - 30) * 10 + (data[pos + 11] - 30);
			} else if (data[pos - 8] == 't' ) { /*tcam*/
				algo_para[lm75_tcam].pwm_duty_min = (data[pos + 1] - 30) * 10 + (data[pos + 2] - 30);
				algo_para[lm75_tcam].pwm_duty_max = (data[pos + 4] - 30) * 100 + (data[pos + 5] - 30) * 10 + (data[pos + 6] - 30);
				algo_para[lm75_tcam].low_temp = (data[pos + 8] - 30) * 10 + (data[pos + 9] - 30);
				algo_para[lm75_tcam].high_temp = (data[pos + 10] - 30) * 10 + (data[pos + 11] - 30);
			}

			else if (data[pos - 8] == 'b' ) { /*bcm56634*/
				algo_para[lm75_bcm56634].pwm_duty_min = (data[pos + 1] - 30) * 10 + (data[pos + 2] - 30);
				algo_para[lm75_bcm56634].pwm_duty_max = (data[pos + 4] - 30) * 100 + (data[pos + 5] - 30) * 10 + (data[pos + 6] - 30);
				algo_para[lm75_bcm56634].low_temp = (data[pos + 8] - 30) * 10 + (data[pos + 9] - 30);
				algo_para[lm75_bcm56634].high_temp = (data[pos + 10] - 30) * 10 + (data[pos + 11] - 30);
			} else {
				/*use default algo para*/
			}
		}
	}
	release_firmware(fw);

	return 0;
}
#endif

//MODULE_DEVICE_TABLE(i2c, thermal_ids);

/* Return 0 if detection is successful, -ENODEV otherwise */
//static int thermal_detect(struct i2c_client *new_client, int kind,
//		       struct i2c_board_info *info)
static int thermal_detect(struct i2c_client *new_client, struct i2c_board_info *info)
{
	struct i2c_adapter *adapter = new_client->adapter;
	int status;

	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA |
				     I2C_FUNC_SMBUS_WORD_DATA))
		return -ENODEV;
	/*create all other i2c client here*/
	status = Create_All_Client(new_client);
	if (status < 0) {
		printk(KERN_ALERT NAME " Create all Client error!\n");
		return -ENODEV;
	}
	status = LM75_Init();
	if (status < 0) {
		printk(KERN_ALERT NAME " LM75 init error!\n");
		return -ENODEV;
	}
	status = Fan_Chip_Init();
	if (status < 0) {
		printk(KERN_ALERT NAME " Fan chip init error!\n");
		return -ENODEV;
	}
	/*start the fan control*/
	status = Kick_Fan_Control_Alogrithm();
	if (status < 0)
		return -ENODEV;
	/* NOTE: we treat "force=..." and "force_lm75=..." the same.
	 * Only new-style driver binding distinguishes chip types.
	 */
	strlcpy(info->type, "lm75-ambient1", I2C_NAME_SIZE);

	return 0;
}

/* device probe and removal */
static int
thermal_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	thermal_data *data;
	int status;
	int i;

	if (!i2c_check_functionality(client->adapter,
				     I2C_FUNC_SMBUS_BYTE_DATA | I2C_FUNC_SMBUS_WORD_DATA))
		return -EIO;
	data = kzalloc(sizeof(thermal_data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;
	data->fan_algo_enabled = FAN_ALGO_ENABLE;
	i2c_set_clientdata(client, data);
	//mutex_init(&data->update_lock);
	KENNISIS_IRQ_TEMP = client->irq;
	printk( KERN_ALERT NAME ":  Create totally %d class sys files\n", NUM_THERMAL_FILES);
	for (i = 0; i < NUM_THERMAL_FILES; i++) {
		status = class_create_file(thermal_class, attrgroup[i]);
	}

	for (i = 0; i < NUM_THERMAL_DEBUG_FILES; i++) {
		status = class_create_file(thermal_class, attrgroup1[i]);
	}

	thermal_detect(client, thermal_i2c_devices);

	if (status)
		goto exit_free;

	return 0;
exit_free:
	i2c_set_clientdata(client, NULL);
	kfree(data);
	return status;
}

static int thermal_remove(struct i2c_client *client)
{
	thermal_data *data = i2c_get_clientdata(client);
	struct i2c_client *tmpclient;
	u8 i;
#ifdef CONFIG_REDSTONE
	int first_client = lm75_p2020;
#else
	int first_client = lm75_ambient1;
#endif
	Stop_Fan_Control_Alogrithm();

	for (i = 0; i < NUM_THERMAL_FILES; i++) {
		class_remove_file(thermal_class, attrgroup[i]);
	}

	for (i = 0; i < NUM_THERMAL_DEBUG_FILES; i++) {
		class_remove_file(thermal_class, attrgroup1[i]);
	}

	for (i = first_client; i < chipcounts; i++) {
		tmpclient = thermalclient[i];
		i2c_set_clientdata(tmpclient, NULL);
		if (!tmpclient) {
			i2c_unregister_device(tmpclient);
		}
	}
	i2c_set_clientdata(client, NULL);
	kfree(data);

	return 0;
}

//bobbie 0316 del
//static const struct i2c_device_id thermal_ids[] = {
//	{ "kennisis_thermal", lm75_ambient1, },
//	{ /* LIST END */ }
//};
//end

/*lm75ambient1 sensor locate in i2c bus 2, we must detect the adapter
in i2c bus 1 in another driver*/
static struct i2c_driver thermal_driver = {
	.driver = {
		.name	= "thermal"
	},
	.probe		= thermal_probe,
	.remove		= thermal_remove,
	.id_table	= i2cbus0_deviceid,
	//bobbie change 0318
	//.detect		=thermal_detect,
#ifndef CONFIG_KERNEL_32_TO_34
	.address_data	= &addr_data,
#else
	.address_list	= normal_i2c,
#endif
};

static int
i2c_bus1_probe(struct i2c_client * client, const struct i2c_device_id * id)
{
	int status = 0;
#ifdef CONFIG_REDSTONE
	int adap2_client = lm75_LIA;
#else
	int adap2_client = lm75_ambient1;
#endif
	thermalclient[adap2_client] = client;
	KENNISIS_IRQ_FANCHIP = client ->irq;

	return status;
}

static int i2c_bus1_remove(struct i2c_client *client)
{
	i2c_set_clientdata(client, NULL);

	return 0;
}

/*detect bus1 adapter*/
static struct i2c_driver thermal_driver_bus1 = {
	.driver = {
		.name	= "i2c_bus1_adapter",
	},
	.probe		= i2c_bus1_probe,
	.remove		= i2c_bus1_remove,
	//.id_table	= thermal_ids_bus1,
	//.detect		=i2c_bus1_detect,
#ifndef CONFIG_KERNEL_32_TO_34
	.address_data	= &addr_data1,
#else
	.address_list	= normal_i2c1,
#endif
//bobbie add 0315
	.id_table =  i2cbus1_deviceid,
};

/*-----------------------------------------------------------------------*/
/* module glue */

static int __init kennisis_thermal_init(void)
{
	int i;

	printk( KERN_ALERT NAME ": thermal driver init\n" );

	for (i = 0; i < chipcounts; i++) {
		thermalclient[i] = NULL;
	}

	thermal_class = class_create(THIS_MODULE, "thermal");
	if (IS_ERR(thermal_class))
		return PTR_ERR(thermal_class);

	if (i2c_add_driver(&thermal_driver_bus1) < 0)
		printk( KERN_ALERT NAME "detect i2c bus1 error\n" );

	return i2c_add_driver(&thermal_driver);
}

static void __exit kennisis_thermal_exit(void)
{
	printk( KERN_ALERT NAME ": thermal driver exit\n" );
	Stop_Fan_Control_Alogrithm();
	class_destroy(thermal_class);
	i2c_del_driver(&thermal_driver_bus1);
	i2c_del_driver(&thermal_driver);
}

module_init(kennisis_thermal_init);
module_exit(kennisis_thermal_exit);
