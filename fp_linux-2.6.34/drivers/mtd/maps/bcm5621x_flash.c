/*
 * Flash mapping for BCM956218 SDK boards
 *
 * Copyright (C) 2006 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <generated/autoconf.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>

#include <asm/mach-bcm5621x/typedefs.h>
#include <asm/mach-bcm5621x/bcmutils.h>
#include <asm/mach-bcm5621x/sbconfig.h>
#include <asm/mach-bcm5621x/sbchipc.h>
#include <asm/mach-bcm5621x/sbutils.h>

/* Global SB handle */
extern void *bcm956218_sbh;
extern spinlock_t bcm956218_sbh_lock;

/* Convenience */
#define sbh bcm956218_sbh
#define sbh_lock bcm956218_sbh_lock

#define WINDOW_ADDR 0x1c000000
#define WINDOW_SIZE 0x2000000
#define BUSWIDTH 2
#define FLASH_BOOT_OFFSET_A1 0x0

static struct mtd_info *bcm956218_mtd;

static void bcm956218_map_write(struct map_info *map, const map_word datum,
				 unsigned long offs)
{
	if (map_bankwidth_is_1(map))
		writeb(datum.x[0], (void *) (map->virt + offs));
	else if (map_bankwidth_is_2(map))
		writew(datum.x[0], (void *) (map->virt + offs));
	else if (map_bankwidth_is_4(map))
		writel(datum.x[0], (void *) (map->virt + offs));
	else if (map_bankwidth_is_large(map))
		memcpy_toio(map->virt + offs, (void *) datum.x[0],
			    map->bankwidth);
	mb();
}

static map_word bcm956218_map_read(struct map_info *map, unsigned long offs)
{
	map_word r;

	if (map_bankwidth_is_1(map)) {
		u16 val;
		if (map->map_priv_2 == 1)
			r.x[0] = readb((void *) (map->map_priv_1 + offs));
		else {
			val =
			    readw((void *) (map->virt +
					    (offs & ~1)));
			if (offs & 1)
				r.x[0] = ((val >> 8) & 0xff);
			else
				r.x[0] = (val & 0xff);
		}
	} else if (map_bankwidth_is_2(map))
		r.x[0] = readw((void *) (map->virt + offs));
	else if (map_bankwidth_is_4(map))
		r.x[0] = readl((void *) (map->virt + offs));
	else if (map_bankwidth_is_large(map))
		memcpy_fromio(r.x, map->virt + offs, map->bankwidth);
	return r;
}

static void bcm956218_map_copy_from(struct map_info *map, void *to, unsigned long from, ssize_t len)
{
	from += map->virt;
	while (len --)
		*(char*)to ++ = readb(from ++);
}

static void bcm956218_map_copy_to(struct map_info *map, unsigned long to, const void *from, ssize_t len)
{
	to += map->virt;
	while (len --)
		writeb(*(char*)from ++, to ++);
}

static struct map_info bcm956218_map = {
	.name = "phys_mapped_flash",
	.size = WINDOW_SIZE,
	.bankwidth = BUSWIDTH,
	.read = bcm956218_map_read,
	.write = bcm956218_map_write,
	.copy_from = bcm956218_map_copy_from,
	.copy_to = bcm956218_map_copy_to
};

#ifdef CONFIG_MTD_PARTITIONS
/* map for BCM56218 A1 */
static struct mtd_partition bcm956218_r1_mtd_parts[] __initdata = {
	{
		.name = "kernel",
		.size = 0x220000,
		.offset = 0x80000
	}, {
		.name = "user",
		.size = 0x0d60000,
		.offset = 0x2a0000
	}, {
		.name = "boot",
		.size = 0x60000,
		.offset = 0x0,
		.mask_flags = MTD_WRITEABLE
	}, {
		.name = "env",
		.size = 0x8000,
		.offset = 0x60000
	}, {
		.name = "nvram",
		.size = 0x8000,
		.offset = 0x68000
	}, {
		.name = "vpd",
		.size = 0x10000,
		.offset = 0x70000
	}
};


#define BCM956218_PARTS_R1 (sizeof(bcm956218_r1_mtd_parts)/sizeof(struct mtd_partition))


#define PTABLE_MAGIC		0x5054424C /* 'PTBL' (big-endian) */
#define PTABLE_MAGIC_LE		0x4C425450 /* 'PTBL' (little-endian) */
#define PTABLE_PNAME_MAX	16
#define PTABLE_MAX_PARTITIONS	8

typedef struct partition_s {
	char		name[PTABLE_PNAME_MAX];
	uint32_t	offset;
	uint32_t	size;
	uint32_t	type;
	uint32_t	flags;
} partition_t;

typedef struct ptable_s {
	uint32_t	magic;
	uint32_t	version;
	uint32_t	chksum;
	uint32_t	reserved;
	partition_t part[PTABLE_MAX_PARTITIONS];
} ptable_t;

static struct mtd_partition bcm956218_mtd_user_parts[PTABLE_MAX_PARTITIONS];
static ptable_t ptable;
static int num_parts;

static int bcm956218_check_ptable(ptable_t *ptbl,
				  struct mtd_partition *part, 
				  ssize_t max_parts, ssize_t *num_parts)
{
	uint32_t chksum, *p32;
	int i, swapped = 0;

	if (ptbl->magic == PTABLE_MAGIC_LE)
		swapped = 1;
	else if (ptbl->magic != PTABLE_MAGIC)
		return -1;

	chksum = 0;
	p32 = (uint32_t*)ptbl;

	for (i = 0; i < sizeof(ptable_t)/4; i++) {
		chksum ^= p32[i];
		if (swapped)
			p32[i] = swab32(p32[i]);
	}

	if (chksum != 0)
		return -1;

	for (i = 0; i < max_parts && ptbl->part[i].size; i++) {
		part[i].name = ptbl->part[i].name;
		part[i].size = ptbl->part[i].size;
		part[i].offset = ptbl->part[i].offset;
	}

	*num_parts = i;
	return 0;
}

static int bcm956218_find_partitions(struct mtd_info *mi, 
				     struct mtd_partition *part, 
				     ssize_t max_parts, ssize_t *num_parts)
{
	int retlen, offset, boot_offset;

	if (mi == NULL || mi->read == NULL)
		return -1;

	boot_offset = FLASH_BOOT_OFFSET_A1;
	for (offset = boot_offset; offset < mi->size; offset += 0x8000) {
		if (mi->read(mi, offset, sizeof(ptable), &retlen, 
			    	(u_char *)&ptable) < 0) {
			return -1;
		}
		if (retlen != sizeof(ptable))
			return -1;

		if (bcm956218_check_ptable(&ptable, part, max_parts, 
					   num_parts) == 0) {
			if (*num_parts > 0) {
				printk("Found flash partition table "
					"at offset 0x%08x\n", offset);
				return 0;
			}
		}
	}
	printk("No valid flash partition table found - using default mapping\n");
	return -1;
}

#endif

int __init init_bcm956218_map(void)
{
	ulong flags;
	uint coreidx = 0;
	uint window_addr = 0, window_size = 0;
	int ret = 0;

	spin_lock_irqsave(&sbh_lock, flags);

	bcm956218_map.map_priv_2 = 0;
	window_addr = WINDOW_ADDR;
	window_size = WINDOW_SIZE;

	sb_setcoreidx(sbh, coreidx);
	spin_unlock_irqrestore(&sbh_lock, flags);
	bcm956218_map.phys = window_addr;
	bcm956218_map.virt = (unsigned long) ioremap(window_addr, window_size);
	if (!bcm956218_map.virt) {
		printk(KERN_ERR "pflash: ioremap failed\n");
		ret = -EIO;
		goto fail;
	}

	if (!(bcm956218_mtd = do_map_probe("cfi_probe", &bcm956218_map))) {
		printk(KERN_ERR "pflash: cfi_probe failed\n");
		ret = -ENXIO;
		goto fail;
	}

	bcm956218_mtd->owner = THIS_MODULE;

	printk(KERN_NOTICE "Flash device: 0x%x at 0x%x\n", bcm956218_mtd->size, window_addr);

#ifdef CONFIG_MTD_PARTITIONS
#ifdef CONFIG_MTD_CMDLINE_PARTS
       {
                static const char *part_probes[] = { "cmdlinepart", NULL, };
                struct mtd_partition    *parts = NULL;

                num_parts = 0;
                num_parts = parse_mtd_partitions(bcm956218_mtd, part_probes, &parts, 0);

                if (num_parts > 0) {
                        ret = add_mtd_partitions(bcm956218_mtd, parts, num_parts);
                        if (!ret) {
                          return 0;
                        }
                }
      }
#endif

	if (bcm956218_find_partitions(bcm956218_mtd, bcm956218_mtd_user_parts, 
				      PTABLE_MAX_PARTITIONS, &num_parts) == 0) {
		ret = add_mtd_partitions(bcm956218_mtd, bcm956218_mtd_user_parts, num_parts);
	}
	else {
		ret = add_mtd_partitions(bcm956218_mtd, bcm956218_r1_mtd_parts, BCM956218_PARTS_R1);
	}
	if (ret) {
		printk(KERN_ERR "pflash: add_mtd_partitions failed\n");
		goto fail;
	}
#endif

	return 0;

 fail:
	if (bcm956218_mtd)
		map_destroy(bcm956218_mtd);
	if (bcm956218_map.virt)
		iounmap((void *) bcm956218_map.virt);
	bcm956218_map.virt = 0;
	return ret;
}

static void __exit cleanup_bcm956218_map(void)
{
#ifdef CONFIG_MTD_PARTITIONS
	del_mtd_partitions(bcm956218_mtd);
#endif
	map_destroy(bcm956218_mtd);
	iounmap((void *) bcm956218_map.virt);
	bcm956218_map.virt = 0;
}

module_init(init_bcm956218_map);
module_exit(cleanup_bcm956218_map);
