/*
 * syscall.h - hypervisor system calls
 *
 * Copyright (c) 2007-2009 Wind River Systems, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 */

#ifndef _VBI_SYSCALL_H
#define _VBI_SYSCALL_H

#include <linux/linkage.h>
#include <vbi/types.h>
#include <vbi/arch.h>
#include <vbi/interface.h>
#include <vbi/syscalls.h>

#ifndef	_ASMLANGUAGE

/* Forward declaration */
struct vmmuConfig;

/* information about incoming message */

struct vbi_msg_info
{
	int32_t  id;		/* context id of sender */
	uint32_t type;		/* message type (msg/event) */
	size_t   slen;		/* length of sent buffer */
	size_t   rlen;		/* length of received buffer */
	uint16_t status;	/* message status information */
	uint16_t error;		/* extended error status */
	time_t   timestamp;	/* time message was sent */
	uint32_t nmsg;		/* number of queued messages remaining */
};

/* message status bits as reported in the "status" field */

#define	VBI_MSG_STATUS_OK		0
#define	VBI_MSG_STATUS_TRUNCATED	1
#define	VBI_MSG_STATUS_COPY_ERROR	2

/* extended error codes reported in "error" field */

#define	VBI_MSG_ERROR_INTLEVEL	1 /* operation not allowed at interrupt level */
#define	VBI_MSG_ERROR_BAD_ID    2 /* bad context id specified */
#define	VBI_MSG_ERROR_ABORTED   3 /* operation aborted */
#define	VBI_MSG_ERROR_TIMEOUT   4 /* operation timed out */
#define	VBI_MSG_ERROR_XCALL     5 /* cross call to remote cpu failed */

/* message types as reported in the "type" field */

#define	VBI_MSG_TYPE_REGULAR	1 /* regular message */
#define	VBI_MSG_TYPE_EVENT	2 /* event message */
#define	VBI_MSG_TYPE_REPLY	3 /* reply message */


/* modifiers for message processing */

struct vbi_msg_ctl
{
	uint32_t flags;		/* operation flags */
	uint32_t ordering;	/* order to receive messages */
	time_t timeout;		/* receive timeout */
};

/* message control flags */
#define VBI_MSG_CTL_FLAG_RETRY 1

/* memory read/write control structure */
struct vbi_mem_ctl
{
	VB_ALIGN_FIELD_64 (void *pBuffer, pad1); /* address of target context   */
	VB_ALIGN_FIELD_64 (void *pAddress, pad2);/* address of calling context  */
	uint64_t size_in;	    /* number of bytes to be read	    */
	uint64_t size_out;	    /* number of bytes successfully read    */
	uint32_t flags;	    /* data/instruction flush option	    */
};

#define VBI_DCACHE_FLUSH	0x0001
#define VBI_ICACHE_INV		0x0002

/* system call prototypes for use within a context */

extern asmlinkage int vbi_hy_ioctl(unsigned ioctl, void *arg1, void *arg2,
				void *arg3, void *arg4);
extern asmlinkage int vbi_io_apic_ioctl(unsigned ioctl, unsigned arg1,
				unsigned arg2);
extern asmlinkage int vbi_ctx_ctl(unsigned operation, unsigned arg1,
				unsigned arg2);
extern asmlinkage int32_t vbi_vb_mgmt(uint32_t cmd, uint32_t boardId,
				int32_t *outError, uint32_t flags, void * ctl);
extern asmlinkage int vbi_vtlb_op(unsigned int op, unsigned long arg1,
				unsigned long arg2, unsigned long arg3);

/*
 * Modified APIs for VBI 2.0
 */

/* Message send */
extern asmlinkage int32_t vbi_send(int32_t id, void *smsg, size_t slen,
			    void *rmsg, size_t rlen, struct vbi_msg_info *info,
			    struct vbi_msg_ctl *ctl);
/* message receive */
extern int32_t vbi_receive(void *rmsg, uint32_t rlen, struct vbi_msg_info *info,
				struct vbi_msg_ctl *ctl);
/* message reply */
extern asmlinkage int32_t vbi_reply(int32_t id, void *smsg, size_t slen,
				struct vbi_msg_ctl *ctl);

extern asmlinkage int32_t vbi_panic(const char *msg);
extern int32_t vbi_flush_dcache(void *saddr, size_t size);
extern int32_t vbi_flush_icache(void *saddr, size_t size);
extern asmlinkage int32_t vbi_kputs(const char *s);
extern asmlinkage int32_t vbi_kputc(int c);
extern int32_t vbi_config_vmmu(struct vmmuConfig * config);
extern int32_t vbi_enable_vmmu(uint32_t  vmmu_num);
extern int32_t vbi_disable_vmmu(uint32_t vmmu_num);
extern int32_t vbi_ns_register(char  *name, uint32_t  revision);
extern int32_t vbi_ns_unregister(char *name, uint32_t  revision);
extern int32_t vbi_ns_lookup(char *name, uint32_t  rev, VBI_NS_HANDLE *pHandle);
extern int32_t vbi_tlb_flush_vmmu(struct vmmuConfig * config, void *addr, size_t len);


/* Prior to vbi 2.0 these api were vbi_set_mmu_attr/Get */

extern asmlinkage int32_t  vbi_set_mem_attr(void *vaddr, size_t len, int32_t attr);
extern asmlinkage int32_t  vbi_get_mem_attr(void *vaddr, int32_t * attr);

/*
 * START: New APIs introduced for vbi 2.0
 */

extern asmlinkage void vbi_vcore_irq_unlock(void);
extern asmlinkage int32_t vbi_vcore_irq_lock(void);
extern int32_t vbi_update_text_cache(void *saddr, size_t size);
extern int32_t vbi_set_exc_base(void *excTblBase);

/* virtual board management API's */
extern asmlinkage int32_t vbi_vb_suspend(uint32_t id, int32_t core);
extern asmlinkage int32_t vbi_vb_reset(uint32_t id, int32_t core, uint32_t options);
extern asmlinkage int32_t vbi_vb_restart(uint32_t id, int32_t core);
extern asmlinkage int32_t vbi_vb_resume(uint32_t id, int32_t core);

/* read remote vb's memory */
extern asmlinkage int32_t vbi_vb_read_mem(struct vbi_mem_ctl *memCtl, uint32_t targetBoard);
extern asmlinkage int32_t vbi_vb_write_mem(struct vbi_mem_ctl *memCtl, uint32_t targetBoard);
extern asmlinkage void vbi_shell_start_debug(uint32_t  flags);

/* read/write remote vb's registers */
extern asmlinkage int32_t vbi_vb_read_reg(VBI_HREG_SET_CMPLX_QUALIFIED *regSet,
				       uint32_t targetBoard, int32_t core);
extern asmlinkage int32_t vbi_vb_write_reg(VBI_HREG_SET_CMPLX_QUALIFIED *regSet,
					uint32_t targetBoard, int32_t core);

/*
 * END: New APIs introduced for vbi 2.0
 */


#endif	/* _ASMLANGUAGE */
#endif  /* _VBI_SYSCALL_H */
