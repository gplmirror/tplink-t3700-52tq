#ifndef __ASM_GENERIC_SERIAL_H
#define __ASM_GENERIC_SERIAL_H

/*
 * This should not be an architecture specific #define, oh well.
 *
 * Traditionally, it just describes i8250 and related serial ports
 * that have this clock rate.
 */

#define BASE_BAUD (1843200 / 16)

#ifdef CONFIG_BCM53000
#define STD_COM_FLAGS (ASYNC_BOOT_AUTOCONF | ASYNC_SKIP_TEST)
#define BCM53000_SERIAL_PORT_DEFNS \
        {       .baud_base = 25000000/16, .irq = 8,                             \
                .flags = STD_COM_FLAGS, .iomem_base = (u8 *)0xb8000300,         \
                .iomem_reg_shift = 0, .io_type = SERIAL_IO_MEM},                \
                { .baud_base = 25000000/16, .irq = 8,                           \
                .flags = STD_COM_FLAGS, .iomem_base = (u8 *)0xb8000400,         \
                .iomem_reg_shift = 0, .io_type = SERIAL_IO_MEM},
#define SERIAL_PORT_DFNS                                \
    BCM53000_SERIAL_PORT_DEFNS
#endif

#endif /* __ASM_GENERIC_SERIAL_H */
