/*
 * x86 arch_vbi.h - x86 architecture specific definitions
 *
 * Copyright 2009 Wind River Systems, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 */

#ifndef _ASM_ARCH_VBI_H
#define _ASM_ARCH_VBI_H

#ifndef _ASMLANGUAGE

/* struct of system descriptor table registers (VBI_GDTR, VBI_IDTR, VBI_LDTR) */

struct VBI_XDTR
{
	uint16_t limit;		/* maximum size of the DT */
	size_t base;		/* address of DT */
	uint16_t pad;
} __attribute__((packed));

struct VBI_XDTR32
{
	uint16_t limit;
	uint32_t base;
	uint16_t pad;
} __attribute__((packed));

typedef struct VBI_XDTR vbi_gdtr;
typedef struct VBI_XDTR vbi_idtr;
typedef struct VBI_XDTR vbi_ldtr;

typedef struct VBI_XDTR32 vbi_gdtr32;
typedef struct VBI_XDTR32 vbi_idtr32;
typedef struct VBI_XDTR32 vbi_ldtr32;


/*
 *
 * VB_HREG_SET - hardware register set, for read/write
 *
 * Used by vbi_vb_read_reg/vbi_vb_write_reg to read/write registers in
 * another VB
 *
 *
 */

typedef struct			/* VBI_REG_SET - used for sys_regsRead/Write */
{
	uint32_t  eax;		/* 00: general register		*/
	uint32_t  ebx;		/* 04: general register		*/
	uint32_t  ecx;		/* 08: general register		*/
	uint32_t  edx;		/* 0C: general register		*/
	uint32_t  esi;		/* 10: general register		*/
	uint32_t  edi;		/* 14: general register		*/
	uint32_t  eip;		/* 18: program counter		*/
	uint32_t  ebp;		/* 1C: frame pointer register	*/
	uint32_t  esp;		/* 20: stack pointer register	*/
	uint32_t  eflags;	/* 24: status register		*/
	uint32_t  cr0;		/* 28: control register 0	*/
	uint32_t  cr3;		/* 2C: control register 3	*/
	uint32_t  cr4;		/* 30: control register 4	*/
	vbi_idtr32  idtr;	/* 34: IDT task register	*/
	vbi_gdtr32  gdtr;	/* 3C: GDT task register	*/
	vbi_ldtr32  ldtr;	/* 44: LDT task register	*/
	uint32_t  cs;		/* 4C: code segment		*/
	uint32_t  ss;		/* 50: stack segment		*/
	uint32_t  ds;		/* 54: data segment		*/
	uint32_t  es;		/* 58: E segment		*/
	uint32_t  fs;		/* 5C: F segment		*/
	uint32_t  gs;		/* 60: G segment		*/
	uint32_t  tr;		/* 64: task register		*/
} VBI_HREG_SET;


typedef struct			/* REG_SET - x86 register set	*/
{
	uint64_t   rax;		/* 00: general register		*/
	uint64_t   rbx;		/* 08: general register		*/
	uint64_t   rcx;		/* 10: general register		*/
	uint64_t   rdx;		/* 18: general register		*/
	uint64_t   rsp;		/* 20: stack pointer register	*/
	uint64_t   rbp;		/* 28: frame pointer register	*/
	uint64_t   rsi;		/* 30: general register		*/
	uint64_t   rdi;		/* 38: general register		*/
	uint64_t   r8;	 	/* 40: general register		*/
	uint64_t   r9;	 	/* 48: general register		*/
	uint64_t   r10;		/* 50: general register		*/
	uint64_t   r11;		/* 58: general register		*/
	uint64_t   r12;		/* 60: general register		*/
	uint64_t   r13;		/* 68: general register		*/
	uint64_t   r14;		/* 70: general register		*/
	uint64_t   r15;		/* 78: general register		*/
	uint64_t   rip;		/* 80: program counter		*/
	uint64_t   rflags;		/* 88: status register		*/
	uint64_t   cr0;		/* 90: control register 0	*/
	uint64_t   cr2;		/* 98: control register 2	*/
	uint64_t   cr3;		/* 100: control register 3	*/
	uint64_t   cr4;		/* 108: control register 4	*/
	vbi_idtr   idtr;	/* 110: IDT task register	*/
	vbi_gdtr   gdtr;	/* 11a: GDT task register	*/
	vbi_ldtr   ldtr;	/* 136: LDT task register	*/
	uint64_t   cs;		/* 140: code segment		*/
	uint64_t   ds;		/* 148: data segment		*/
	uint64_t   ss;		/* 150: stack segment		*/
	uint64_t   es;		/* 158: E segment		*/
	uint64_t   fs;		/* 160: F segment		*/
	uint64_t   gs;		/* 168: G segment		*/
	uint64_t   tr;		/* 170: Task register		*/
	/* xxx(gws): excluding FP support */
} VBI_HREG_SET_64;


/* complex register set definition */

typedef union
{
	VBI_HREG_SET    hreg32;	/* 32 bit register set */
	VBI_HREG_SET_64 hreg64;	/* 64 bit register set */
} VBI_HREG_SET_CMPLX;


typedef struct
{
	uint32_t vbiRegType;  /* 00: register set to use */
	uint32_t	    qualifier;   /* 04: optional field, used for alignment */

	VBI_HREG_SET_CMPLX vbiRegSet;
} VBI_HREG_SET_CMPLX_QUALIFIED;


#endif /* _ASMLANGUAGE */


/* x86 uses little endian byte ordering */

#define __VBI_BYTE_ORDER __VBI_LITTLE_ENDIAN

#define VBI_X86_MAX_VECTORS         256	/* maximum number of vectors */
#define VBI_ARCH_EXC_TABLE_SIZE     32
#define VBI_ARCH_IRQ_TABLE_SIZE     (VBI_X86_MAX_VECTORS - VBI_ARCH_EXC_TABLE_SIZE)

#define VBI_MAX_CORES			 8 /* maximum number of virtual cores */

#define VBI_IN_DIVIDE_ERROR		 0
#define VBI_IN_DEBUG			 1
#define VBI_IN_NON_MASKABLE		 2
#define VBI_IN_BREAKPOINT		 3
#define VBI_IN_OVERFLOW			 4
#define VBI_IN_BOUND			 5
#define VBI_IN_INVALID_OPCODE		 6
#define VBI_IN_NO_DEVICE		 7
#define VBI_IN_DOUBLE_FAULT		 8
#define VBI_IN_CP_OVERRUN		 9
#define VBI_IN_INVALID_TSS		10
#define VBI_IN_NO_SEGMENT		11
#define VBI_IN_STACK_FAULT		12
#define VBI_IN_PROTECTION_FAULT		13
#define VBI_IN_PAGE_FAULT		14
#define VBI_IN_RESERVED			15
#define VBI_IN_CP_ERROR			16
#define VBI_IN_ALIGNMENT		17
#define VBI_IN_MACHINE_CHECK		18
#define VBI_IN_SIMD			19

/* 19-31 Intel reserved exceptions  */

/* 32-255 user defined exceptions  */

#define VBI_IN_EXT_IRQ_BASE		32	/* local timer interrupt */

#define VBI_IN_EXT_IRQ0			(VBI_IN_EXT_IRQ_BASE + 0)
#define VBI_IN_EXT_IRQ1			(VBI_IN_EXT_IRQ_BASE + 1)
#define VBI_IN_EXT_IRQ2			(VBI_IN_EXT_IRQ_BASE + 2)
#define VBI_IN_EXT_IRQ3			(VBI_IN_EXT_IRQ_BASE + 3)
#define VBI_IN_EXT_IRQ4			(VBI_IN_EXT_IRQ_BASE + 4)
#define VBI_IN_EXT_IRQ5			(VBI_IN_EXT_IRQ_BASE + 5)
#define VBI_IN_EXT_IRQ6			(VBI_IN_EXT_IRQ_BASE + 6)
#define VBI_IN_EXT_IRQ7			(VBI_IN_EXT_IRQ_BASE + 7)
#define VBI_IN_EXT_IRQ8			(VBI_IN_EXT_IRQ_BASE + 8)
#define VBI_IN_EXT_IRQ9			(VBI_IN_EXT_IRQ_BASE + 9)
#define VBI_IN_EXT_IRQ10		(VBI_IN_EXT_IRQ_BASE + 10)
#define VBI_IN_EXT_IRQ11		(VBI_IN_EXT_IRQ_BASE + 11)
#define VBI_IN_EXT_IRQ12		(VBI_IN_EXT_IRQ_BASE + 12)
#define VBI_IN_EXT_IRQ13		(VBI_IN_EXT_IRQ_BASE + 13)
#define VBI_IN_EXT_IRQ14		(VBI_IN_EXT_IRQ_BASE + 14)
#define VBI_IN_EXT_IRQ15		(VBI_IN_EXT_IRQ_BASE + 15)
#define VBI_IN_EXT_IRQ16		(VBI_IN_EXT_IRQ_BASE + 16)
#define VBI_IN_EXT_IRQ17		(VBI_IN_EXT_IRQ_BASE + 17)
#define VBI_IN_EXT_IRQ18		(VBI_IN_EXT_IRQ_BASE + 18)
#define VBI_IN_EXT_IRQ19		(VBI_IN_EXT_IRQ_BASE + 19)
#define VBI_IN_EXT_IRQ20		(VBI_IN_EXT_IRQ_BASE + 20)
#define VBI_IN_EXT_IRQ21		(VBI_IN_EXT_IRQ_BASE + 21)
#define VBI_IN_EXT_IRQ22		(VBI_IN_EXT_IRQ_BASE + 22)
#define VBI_IN_EXT_IRQ23		(VBI_IN_EXT_IRQ_BASE + 23)
#define VBI_IN_EXT_IRQ24		(VBI_IN_EXT_IRQ_BASE + 24)
#define VBI_IN_EXT_IRQ25		(VBI_IN_EXT_IRQ_BASE + 25)
#define VBI_IN_EXT_IRQ26		(VBI_IN_EXT_IRQ_BASE + 26)
#define VBI_IN_EXT_IRQ27		(VBI_IN_EXT_IRQ_BASE + 27)
#define VBI_IN_EXT_IRQ28		(VBI_IN_EXT_IRQ_BASE + 28)
#define VBI_IN_EXT_IRQ29		(VBI_IN_EXT_IRQ_BASE + 29)
#define VBI_IN_EXT_IRQ30		(VBI_IN_EXT_IRQ_BASE + 30)
#define VBI_IN_EXT_IRQ31		(VBI_IN_EXT_IRQ_BASE + 31)
#define VBI_IN_EXT_IRQ32		(VBI_IN_EXT_IRQ_BASE + 32)
#define VBI_IN_EXT_IRQ33		(VBI_IN_EXT_IRQ_BASE + 33)
#define VBI_IN_EXT_IRQ34		(VBI_IN_EXT_IRQ_BASE + 34)
#define VBI_IN_EXT_IRQ35		(VBI_IN_EXT_IRQ_BASE + 35)
#define VBI_IN_EXT_IRQ36		(VBI_IN_EXT_IRQ_BASE + 36)
#define VBI_IN_EXT_IRQ37		(VBI_IN_EXT_IRQ_BASE + 37)
#define VBI_IN_EXT_IRQ38		(VBI_IN_EXT_IRQ_BASE + 38)
#define VBI_IN_EXT_IRQ39		(VBI_IN_EXT_IRQ_BASE + 39)
#define VBI_IN_EXT_IRQ40		(VBI_IN_EXT_IRQ_BASE + 40)
#define VBI_IN_EXT_IRQ41		(VBI_IN_EXT_IRQ_BASE + 41)
#define VBI_IN_EXT_IRQ42		(VBI_IN_EXT_IRQ_BASE + 42)
#define VBI_IN_EXT_IRQ43		(VBI_IN_EXT_IRQ_BASE + 43)
#define VBI_IN_EXT_IRQ44		(VBI_IN_EXT_IRQ_BASE + 44)
#define VBI_IN_EXT_IRQ45		(VBI_IN_EXT_IRQ_BASE + 45)
#define VBI_IN_EXT_IRQ46		(VBI_IN_EXT_IRQ_BASE + 46)
#define VBI_IN_EXT_IRQ47		(VBI_IN_EXT_IRQ_BASE + 47)
#define VBI_IN_EXT_IRQ48		(VBI_IN_EXT_IRQ_BASE + 48)
#define VBI_IN_EXT_IRQ49		(VBI_IN_EXT_IRQ_BASE + 49)
#define VBI_IN_EXT_IRQ50		(VBI_IN_EXT_IRQ_BASE + 50)
#define VBI_IN_EXT_IRQ51		(VBI_IN_EXT_IRQ_BASE + 51)
#define VBI_IN_EXT_IRQ52		(VBI_IN_EXT_IRQ_BASE + 52)
#define VBI_IN_EXT_IRQ53		(VBI_IN_EXT_IRQ_BASE + 53)
#define VBI_IN_EXT_IRQ54		(VBI_IN_EXT_IRQ_BASE + 54)
#define VBI_IN_EXT_IRQ55		(VBI_IN_EXT_IRQ_BASE + 55)
#define VBI_IN_EXT_IRQ56		(VBI_IN_EXT_IRQ_BASE + 56)
#define VBI_IN_EXT_IRQ57		(VBI_IN_EXT_IRQ_BASE + 57)
#define VBI_IN_EXT_IRQ58		(VBI_IN_EXT_IRQ_BASE + 58)
#define VBI_IN_EXT_IRQ59		(VBI_IN_EXT_IRQ_BASE + 59)
#define VBI_IN_EXT_IRQ60		(VBI_IN_EXT_IRQ_BASE + 60)
#define VBI_IN_EXT_IRQ61		(VBI_IN_EXT_IRQ_BASE + 61)
#define VBI_IN_EXT_IRQ62		(VBI_IN_EXT_IRQ_BASE + 62)
#define VBI_IN_EXT_IRQ63		(VBI_IN_EXT_IRQ_BASE + 63)
#define VBI_IN_EXT_IRQ64		(VBI_IN_EXT_IRQ_BASE + 64)
#define VBI_IN_EXT_IRQ65		(VBI_IN_EXT_IRQ_BASE + 65)
#define VBI_IN_EXT_IRQ66		(VBI_IN_EXT_IRQ_BASE + 66)
#define VBI_IN_EXT_IRQ67		(VBI_IN_EXT_IRQ_BASE + 67)
#define VBI_IN_EXT_IRQ68		(VBI_IN_EXT_IRQ_BASE + 68)
#define VBI_IN_EXT_IRQ69		(VBI_IN_EXT_IRQ_BASE + 69)
#define VBI_IN_EXT_IRQ70		(VBI_IN_EXT_IRQ_BASE + 70)
#define VBI_IN_EXT_IRQ71		(VBI_IN_EXT_IRQ_BASE + 71)
#define VBI_IN_EXT_IRQ72		(VBI_IN_EXT_IRQ_BASE + 72)
#define VBI_IN_EXT_IRQ73		(VBI_IN_EXT_IRQ_BASE + 73)
#define VBI_IN_EXT_IRQ74		(VBI_IN_EXT_IRQ_BASE + 74)
#define VBI_IN_EXT_IRQ75		(VBI_IN_EXT_IRQ_BASE + 75)
#define VBI_IN_EXT_IRQ76		(VBI_IN_EXT_IRQ_BASE + 76)
#define VBI_IN_EXT_IRQ77		(VBI_IN_EXT_IRQ_BASE + 77)
#define VBI_IN_EXT_IRQ78		(VBI_IN_EXT_IRQ_BASE + 78)
#define VBI_IN_EXT_IRQ79		(VBI_IN_EXT_IRQ_BASE + 79)
#define VBI_IN_EXT_IRQ80		(VBI_IN_EXT_IRQ_BASE + 80)
#define VBI_IN_EXT_IRQ81		(VBI_IN_EXT_IRQ_BASE + 81)
#define VBI_IN_EXT_IRQ82		(VBI_IN_EXT_IRQ_BASE + 82)
#define VBI_IN_EXT_IRQ83		(VBI_IN_EXT_IRQ_BASE + 83)
#define VBI_IN_EXT_IRQ84		(VBI_IN_EXT_IRQ_BASE + 84)
#define VBI_IN_EXT_IRQ85		(VBI_IN_EXT_IRQ_BASE + 85)
#define VBI_IN_EXT_IRQ86		(VBI_IN_EXT_IRQ_BASE + 86)
#define VBI_IN_EXT_IRQ87		(VBI_IN_EXT_IRQ_BASE + 87)
#define VBI_IN_EXT_IRQ88		(VBI_IN_EXT_IRQ_BASE + 88)
#define VBI_IN_EXT_IRQ89		(VBI_IN_EXT_IRQ_BASE + 89)
#define VBI_IN_EXT_IRQ90		(VBI_IN_EXT_IRQ_BASE + 90)
#define VBI_IN_EXT_IRQ91		(VBI_IN_EXT_IRQ_BASE + 91)
#define VBI_IN_EXT_IRQ92		(VBI_IN_EXT_IRQ_BASE + 92)
#define VBI_IN_EXT_IRQ93		(VBI_IN_EXT_IRQ_BASE + 93)
#define VBI_IN_EXT_IRQ94		(VBI_IN_EXT_IRQ_BASE + 94)
#define VBI_IN_EXT_IRQ95		(VBI_IN_EXT_IRQ_BASE + 95)
#define VBI_IN_EXT_IRQ96		(VBI_IN_EXT_IRQ_BASE + 96)
#define VBI_IN_EXT_IRQ97		(VBI_IN_EXT_IRQ_BASE + 97)
#define VBI_IN_EXT_IRQ98		(VBI_IN_EXT_IRQ_BASE + 98)
#define VBI_IN_EXT_IRQ99		(VBI_IN_EXT_IRQ_BASE + 99)
#define VBI_IN_EXT_IRQ100		(VBI_IN_EXT_IRQ_BASE + 100)
#define VBI_IN_EXT_IRQ101		(VBI_IN_EXT_IRQ_BASE + 101)
#define VBI_IN_EXT_IRQ102		(VBI_IN_EXT_IRQ_BASE + 102)
#define VBI_IN_EXT_IRQ103		(VBI_IN_EXT_IRQ_BASE + 103)
#define VBI_IN_EXT_IRQ104		(VBI_IN_EXT_IRQ_BASE + 104)
#define VBI_IN_EXT_IRQ105		(VBI_IN_EXT_IRQ_BASE + 105)
#define VBI_IN_EXT_IRQ106		(VBI_IN_EXT_IRQ_BASE + 106)
#define VBI_IN_EXT_IRQ107		(VBI_IN_EXT_IRQ_BASE + 107)
#define VBI_IN_EXT_IRQ108		(VBI_IN_EXT_IRQ_BASE + 108)
#define VBI_IN_EXT_IRQ109		(VBI_IN_EXT_IRQ_BASE + 109)
#define VBI_IN_EXT_IRQ110		(VBI_IN_EXT_IRQ_BASE + 110)
#define VBI_IN_EXT_IRQ111		(VBI_IN_EXT_IRQ_BASE + 111)
#define VBI_IN_EXT_IRQ112		(VBI_IN_EXT_IRQ_BASE + 112)
#define VBI_IN_EXT_IRQ113		(VBI_IN_EXT_IRQ_BASE + 113)
#define VBI_IN_EXT_IRQ114		(VBI_IN_EXT_IRQ_BASE + 114)
#define VBI_IN_EXT_IRQ115		(VBI_IN_EXT_IRQ_BASE + 115)
#define VBI_IN_EXT_IRQ116		(VBI_IN_EXT_IRQ_BASE + 116)
#define VBI_IN_EXT_IRQ117		(VBI_IN_EXT_IRQ_BASE + 117)
#define VBI_IN_EXT_IRQ118		(VBI_IN_EXT_IRQ_BASE + 118)
#define VBI_IN_EXT_IRQ119		(VBI_IN_EXT_IRQ_BASE + 119)
#define VBI_IN_EXT_IRQ120		(VBI_IN_EXT_IRQ_BASE + 120)
#define VBI_IN_EXT_IRQ121		(VBI_IN_EXT_IRQ_BASE + 121)
#define VBI_IN_EXT_IRQ122		(VBI_IN_EXT_IRQ_BASE + 122)
#define VBI_IN_EXT_IRQ123		(VBI_IN_EXT_IRQ_BASE + 123)
#define VBI_IN_EXT_IRQ124		(VBI_IN_EXT_IRQ_BASE + 124)
#define VBI_IN_EXT_IRQ125		(VBI_IN_EXT_IRQ_BASE + 125)
#define VBI_IN_EXT_IRQ126		(VBI_IN_EXT_IRQ_BASE + 126)
#define VBI_IN_EXT_IRQ127		(VBI_IN_EXT_IRQ_BASE + 127)
#define VBI_IN_EXT_IRQ128		(VBI_IN_EXT_IRQ_BASE + 128)
#define VBI_IN_EXT_IRQ129		(VBI_IN_EXT_IRQ_BASE + 129)
#define VBI_IN_EXT_IRQ130		(VBI_IN_EXT_IRQ_BASE + 130)
#define VBI_IN_EXT_IRQ131		(VBI_IN_EXT_IRQ_BASE + 131)
#define VBI_IN_EXT_IRQ132		(VBI_IN_EXT_IRQ_BASE + 132)
#define VBI_IN_EXT_IRQ133		(VBI_IN_EXT_IRQ_BASE + 133)
#define VBI_IN_EXT_IRQ134		(VBI_IN_EXT_IRQ_BASE + 134)
#define VBI_IN_EXT_IRQ135		(VBI_IN_EXT_IRQ_BASE + 135)
#define VBI_IN_EXT_IRQ136		(VBI_IN_EXT_IRQ_BASE + 136)
#define VBI_IN_EXT_IRQ137		(VBI_IN_EXT_IRQ_BASE + 137)
#define VBI_IN_EXT_IRQ138		(VBI_IN_EXT_IRQ_BASE + 138)
#define VBI_IN_EXT_IRQ139		(VBI_IN_EXT_IRQ_BASE + 139)
#define VBI_IN_EXT_IRQ140		(VBI_IN_EXT_IRQ_BASE + 140)
#define VBI_IN_EXT_IRQ141		(VBI_IN_EXT_IRQ_BASE + 141)
#define VBI_IN_EXT_IRQ142		(VBI_IN_EXT_IRQ_BASE + 142)
#define VBI_IN_EXT_IRQ143		(VBI_IN_EXT_IRQ_BASE + 143)
#define VBI_IN_EXT_IRQ144		(VBI_IN_EXT_IRQ_BASE + 144)
#define VBI_IN_EXT_IRQ145		(VBI_IN_EXT_IRQ_BASE + 145)
#define VBI_IN_EXT_IRQ146		(VBI_IN_EXT_IRQ_BASE + 146)
#define VBI_IN_EXT_IRQ147		(VBI_IN_EXT_IRQ_BASE + 147)
#define VBI_IN_EXT_IRQ148		(VBI_IN_EXT_IRQ_BASE + 148)
#define VBI_IN_EXT_IRQ149		(VBI_IN_EXT_IRQ_BASE + 149)
#define VBI_IN_EXT_IRQ150		(VBI_IN_EXT_IRQ_BASE + 150)
#define VBI_IN_EXT_IRQ151		(VBI_IN_EXT_IRQ_BASE + 151)
#define VBI_IN_EXT_IRQ152		(VBI_IN_EXT_IRQ_BASE + 152)
#define VBI_IN_EXT_IRQ153		(VBI_IN_EXT_IRQ_BASE + 153)
#define VBI_IN_EXT_IRQ154		(VBI_IN_EXT_IRQ_BASE + 154)
#define VBI_IN_EXT_IRQ155		(VBI_IN_EXT_IRQ_BASE + 155)
#define VBI_IN_EXT_IRQ156		(VBI_IN_EXT_IRQ_BASE + 156)
#define VBI_IN_EXT_IRQ157		(VBI_IN_EXT_IRQ_BASE + 157)
#define VBI_IN_EXT_IRQ158		(VBI_IN_EXT_IRQ_BASE + 158)
#define VBI_IN_EXT_IRQ159		(VBI_IN_EXT_IRQ_BASE + 159)
#define VBI_IN_EXT_IRQ160		(VBI_IN_EXT_IRQ_BASE + 160)
#define VBI_IN_EXT_IRQ161		(VBI_IN_EXT_IRQ_BASE + 161)
#define VBI_IN_EXT_IRQ162		(VBI_IN_EXT_IRQ_BASE + 162)
#define VBI_IN_EXT_IRQ163		(VBI_IN_EXT_IRQ_BASE + 163)
#define VBI_IN_EXT_IRQ164		(VBI_IN_EXT_IRQ_BASE + 164)
#define VBI_IN_EXT_IRQ165		(VBI_IN_EXT_IRQ_BASE + 165)
#define VBI_IN_EXT_IRQ166		(VBI_IN_EXT_IRQ_BASE + 166)
#define VBI_IN_EXT_IRQ167		(VBI_IN_EXT_IRQ_BASE + 167)
#define VBI_IN_EXT_IRQ168		(VBI_IN_EXT_IRQ_BASE + 168)
#define VBI_IN_EXT_IRQ169		(VBI_IN_EXT_IRQ_BASE + 169)
#define VBI_IN_EXT_IRQ170		(VBI_IN_EXT_IRQ_BASE + 170)
#define VBI_IN_EXT_IRQ171		(VBI_IN_EXT_IRQ_BASE + 171)
#define VBI_IN_EXT_IRQ172		(VBI_IN_EXT_IRQ_BASE + 172)
#define VBI_IN_EXT_IRQ173		(VBI_IN_EXT_IRQ_BASE + 173)
#define VBI_IN_EXT_IRQ174		(VBI_IN_EXT_IRQ_BASE + 174)
#define VBI_IN_EXT_IRQ175		(VBI_IN_EXT_IRQ_BASE + 175)
#define VBI_IN_EXT_IRQ176		(VBI_IN_EXT_IRQ_BASE + 176)
#define VBI_IN_EXT_IRQ177		(VBI_IN_EXT_IRQ_BASE + 177)
#define VBI_IN_EXT_IRQ178		(VBI_IN_EXT_IRQ_BASE + 178)
#define VBI_IN_EXT_IRQ179		(VBI_IN_EXT_IRQ_BASE + 179)
#define VBI_IN_EXT_IRQ180		(VBI_IN_EXT_IRQ_BASE + 180)
#define VBI_IN_EXT_IRQ181		(VBI_IN_EXT_IRQ_BASE + 181)
#define VBI_IN_EXT_IRQ182		(VBI_IN_EXT_IRQ_BASE + 182)
#define VBI_IN_EXT_IRQ183		(VBI_IN_EXT_IRQ_BASE + 183)
#define VBI_IN_EXT_IRQ184		(VBI_IN_EXT_IRQ_BASE + 184)
#define VBI_IN_EXT_IRQ185		(VBI_IN_EXT_IRQ_BASE + 185)
#define VBI_IN_EXT_IRQ186		(VBI_IN_EXT_IRQ_BASE + 186)
#define VBI_IN_EXT_IRQ187		(VBI_IN_EXT_IRQ_BASE + 187)
#define VBI_IN_EXT_IRQ188		(VBI_IN_EXT_IRQ_BASE + 188)
#define VBI_IN_EXT_IRQ189		(VBI_IN_EXT_IRQ_BASE + 189)
#define VBI_IN_EXT_IRQ190		(VBI_IN_EXT_IRQ_BASE + 190)
#define VBI_IN_EXT_IRQ191		(VBI_IN_EXT_IRQ_BASE + 191)
#define VBI_IN_EXT_IRQ192		(VBI_IN_EXT_IRQ_BASE + 192)
#define VBI_IN_EXT_IRQ193		(VBI_IN_EXT_IRQ_BASE + 193)
#define VBI_IN_EXT_IRQ194		(VBI_IN_EXT_IRQ_BASE + 194)
#define VBI_IN_EXT_IRQ195		(VBI_IN_EXT_IRQ_BASE + 195)
#define VBI_IN_EXT_IRQ196		(VBI_IN_EXT_IRQ_BASE + 196)
#define VBI_IN_EXT_IRQ197		(VBI_IN_EXT_IRQ_BASE + 197)
#define VBI_IN_EXT_IRQ198		(VBI_IN_EXT_IRQ_BASE + 198)
#define VBI_IN_EXT_IRQ199		(VBI_IN_EXT_IRQ_BASE + 199)
#define VBI_IN_EXT_IRQ200		(VBI_IN_EXT_IRQ_BASE + 200)
#define VBI_IN_EXT_IRQ201		(VBI_IN_EXT_IRQ_BASE + 201)
#define VBI_IN_EXT_IRQ202		(VBI_IN_EXT_IRQ_BASE + 202)
#define VBI_IN_EXT_IRQ203		(VBI_IN_EXT_IRQ_BASE + 203)
#define VBI_IN_EXT_IRQ204		(VBI_IN_EXT_IRQ_BASE + 204)
#define VBI_IN_EXT_IRQ205		(VBI_IN_EXT_IRQ_BASE + 205)
#define VBI_IN_EXT_IRQ206		(VBI_IN_EXT_IRQ_BASE + 206)
#define VBI_IN_EXT_IRQ207		(VBI_IN_EXT_IRQ_BASE + 207)
#define VBI_IN_EXT_IRQ208		(VBI_IN_EXT_IRQ_BASE + 208)
#define VBI_IN_EXT_IRQ209		(VBI_IN_EXT_IRQ_BASE + 209)
#define VBI_IN_EXT_IRQ210		(VBI_IN_EXT_IRQ_BASE + 210)
#define VBI_IN_EXT_IRQ211		(VBI_IN_EXT_IRQ_BASE + 211)
#define VBI_IN_EXT_IRQ212		(VBI_IN_EXT_IRQ_BASE + 212)
#define VBI_IN_EXT_IRQ213		(VBI_IN_EXT_IRQ_BASE + 213)
#define VBI_IN_EXT_IRQ214		(VBI_IN_EXT_IRQ_BASE + 214)
#define VBI_IN_EXT_IRQ215		(VBI_IN_EXT_IRQ_BASE + 215)
#define VBI_IN_EXT_IRQ216		(VBI_IN_EXT_IRQ_BASE + 216)
#define VBI_IN_EXT_IRQ217		(VBI_IN_EXT_IRQ_BASE + 217)
#define VBI_IN_EXT_IRQ218		(VBI_IN_EXT_IRQ_BASE + 218)
#define VBI_IN_EXT_IRQ219		(VBI_IN_EXT_IRQ_BASE + 219)
#define VBI_IN_EXT_IRQ220		(VBI_IN_EXT_IRQ_BASE + 220)
#define VBI_IN_EXT_IRQ221		(VBI_IN_EXT_IRQ_BASE + 221)
#define VBI_IN_EXT_IRQ222		(VBI_IN_EXT_IRQ_BASE + 222)
#define VBI_IN_EXT_IRQ223		(VBI_IN_EXT_IRQ_BASE + 223)

/* timer vector */

#define VBI_CLOCK_TIMER_VECTOR		0
#define VBI_IN_APIC_TIMER		(VBI_IN_EXT_IRQ0)

/* XXX this is less than desireable */
#define IS_GUEST_REG_64(pCtx) (pCtx->arch->

#endif /* _ASM_ARCH_VBI_H */
