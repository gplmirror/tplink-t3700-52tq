/*
 * Copyright (C) 2009 Broadcom Corporation
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 * 
 * 
 */
/*
 * OTP support.
 *
 * $Copyright Open Broadcom Corporation$
 *
 *  
 */

/* OTP regions */
#define OTP_HW_RGN	1
#define OTP_SW_RGN	2
#define OTP_CI_RGN	4
#define OTP_FUSE_RGN	8

#if !defined(BCMDONGLEHOST)
extern void *otp_init(si_t * sih);
#endif				/* !defined(BCMDONGLEHOST) */

extern int otp_status(void *oh);
extern int otp_size(void *oh);

extern int otp_read_region(void *oh, int region, uint16 * data,
			   uint * wlen);
extern uint16 otpr(void *oh, chipcregs_t * cc, uint wn);
extern int otp_nvread(void *oh, char *data, uint * len);

#ifdef BCMNVRAMW
#if !defined(BCMDONGLEHOST)
extern int otp_write_region(void *oh, int region, uint16 * data,
			    uint wlen);
extern int otp_fix_word16(void *oh, uint wn, uint16 mask, uint16 val);
extern int otp_write_rde(void *oh, int rde, uint bit, uint val);
extern int otpw(void *oh, int wn, uint16 data);
extern int otp_nvwrite(void *oh, uint16 * data, uint wlen);
#endif				/* !defined(BCMDONGLEHOST) */
#endif				/* BCMNVRAMW */

#if defined(WLTEST) || defined (BCMINTERNAL)
extern int otp_dump(void *oh, int arg, char *buf, uint size);
#endif
