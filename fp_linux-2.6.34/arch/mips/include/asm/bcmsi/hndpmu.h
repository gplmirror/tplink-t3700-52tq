/*
 * Copyright (C) 2009 Broadcom Corporation
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 * 
 * 
 */
/*
 * HND SiliconBackplane PMU support.
 *
 * $Copyright Open Broadcom Corporation$
 *
 *  
 */

#ifndef _hndpmu_h_
#define _hndpmu_h_

#if !defined(BCMDONGLEHOST)
#define SET_LDO_VOLTAGE_LDO1	1
#define SET_LDO_VOLTAGE_LDO2	2
#define SET_LDO_VOLTAGE_LDO3	3
#define SET_LDO_VOLTAGE_PAREF	4
#define SET_LDO_VOLTAGE_CLDO_PWM	5
#define SET_LDO_VOLTAGE_CLDO_BURST	6
#define SET_LDO_VOLTAGE_CBUCK_PWM	7
#define SET_LDO_VOLTAGE_CBUCK_BURST	8

extern void si_pmu_init(si_t *sih, osl_t *osh);
extern void si_pmu_chip_init(si_t *sih, osl_t *osh);
extern void si_pmu_pll_init(si_t *sih, osl_t *osh, uint32 xtalfreq);
extern void si_pmu_res_init(si_t *sih, osl_t *osh);
extern void si_pmu_swreg_init(si_t *sih, osl_t *osh);

extern uint32 si_pmu_force_ilp(si_t *sih, osl_t *osh, bool force);

extern uint32 si_pmu_si_clock(si_t *sih, osl_t *osh);
extern uint32 si_pmu_cpu_clock(si_t *sih, osl_t *osh);
extern uint32 si_pmu_mem_clock(si_t *sih, osl_t *osh);
extern uint32 si_pmu_alp_clock(si_t *sih, osl_t *osh);
extern uint32 si_pmu_ilp_clock(si_t *sih, osl_t *osh);

extern void si_pmu_set_switcher_voltage(si_t *sih, osl_t *osh,
					uint8 bb_voltage,
					uint8 rf_voltage);
extern void si_pmu_set_ldo_voltage(si_t *sih, osl_t *osh, uint8 ldo,
				   uint8 voltage);
extern void si_pmu_paref_ldo_enable(si_t *sih, osl_t *osh, bool enable);
extern uint16 si_pmu_fast_pwrup_delay(si_t *sih, osl_t *osh);
extern void si_pmu_rcal(si_t *sih, osl_t *osh);

extern void si_pmu_spuravoid(si_t *sih, osl_t *osh, bool spuravoid);

extern bool si_pmu_is_otp_powered(si_t *sih, osl_t *osh);
#endif				/* !defined(BCMDONGLEHOST) */

extern void si_pmu_otp_power(si_t *sih, osl_t *osh, bool on);
extern void si_sdiod_drive_strength_init(si_t *sih, osl_t *osh,
					 uint32 drivestrength);
extern void si_pmu_chipcontrol(si_t *sih, uint reg, uint32 mask,
			       uint32 val);


#endif				/* _hndpmu_h_ */
