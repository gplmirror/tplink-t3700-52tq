/*
 * Copyright (C) 2009 Broadcom Corporation
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 * 
 * 
 */
/*
 * Misc useful routines to access NIC local SROM/OTP .
 *
 * $Copyright Open Broadcom Corporation$
 *
 *  
 */

#ifndef	_bcmsrom_h_
#define	_bcmsrom_h_

#include "bcmsrom_fmt.h"

/* Prototypes */
extern int srom_var_init(si_t * sih, uint bus, void *curmap, osl_t * osh,
			 char **vars, uint * count);

extern int srom_read(si_t * sih, uint bus, void *curmap, osl_t * osh,
		     uint byteoff, uint nbytes, uint16 * buf,
		     bool check_crc);

extern int srom_write(si_t * sih, uint bus, void *curmap, osl_t * osh,
		      uint byteoff, uint nbytes, uint16 * buf);

/* parse standard PCMCIA cis, normally used by SB/PCMCIA/SDIO/SPI/OTP
 *   and extract from it into name=value pairs
 */
extern int srom_parsecis(osl_t * osh, uint8 ** pcis, uint ciscnt,
			 char **vars, uint * count);

#endif				/* _bcmsrom_h_ */
