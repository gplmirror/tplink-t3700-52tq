/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */
/*
 * MIPS 74k definitions
 *
 * $Copyright Open Broadcom Corporation$
 *
 *
 */

#ifndef	_mips74k_core_h_
#define	_mips74k_core_h_

#include <mipsinc.h>

#ifndef _LANGUAGE_ASSEMBLY

/* cpp contortions to concatenate w/arg prescan */
#ifndef PAD
#define	_PADLINE(line)	pad ## line
#define	_XSTR(line)	_PADLINE(line)
#define	PAD		_XSTR(__LINE__)
#endif				/* PAD */

typedef volatile struct {
    uint32 corecontrol;
    uint32 exceptionbase;
    uint32 PAD[1];
    uint32 biststatus;
    uint32 intstatus;
    uint32 intmask[6];
    uint32 nmimask;
    uint32 PAD[4];
    uint32 gpioselect;
    uint32 gpiooutput;
    uint32 gpioenable;
    uint32 PAD[101];
    uint32 clkcontrolstatus;
} mips74kregs_t;

#endif				/* _LANGUAGE_ASSEMBLY */

#endif				/* _mips74k_core_h_ */
