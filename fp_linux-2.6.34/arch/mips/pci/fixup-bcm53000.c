/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */
/*
 * Low-Level PCI and SB support for BCM53000 (Linux support code)
 *
 */
#include <linux/pci.h>
#include <asm/delay.h>
#include <asm/bcmsi/typedefs.h>
#include <asm/bcmsi/bcmutils.h>
#include <asm/bcmsi/hndsoc.h>
#include <asm/bcmsi/siutils.h>
#include <asm/bcmsi/hndpci.h>
#include <asm/bcmsi/pcicfg.h>
#include <asm/bcmsi/bcmdevs.h>
#include <asm/bcmsi/bcmnvram.h>
#include <asm/bcmsi/hndcpu.h>

/* Global SB handle */
extern void *bcm53000_sih;
extern spinlock_t bcm53000_sih_lock;

/* Convenience */
#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock

static u32 pci_iobase = 0x100;
static u32 pci_membase;

/* PCI port fixup */
static bool fixup_pci_flag[] = {
    FALSE,
    FALSE
};

static void bcm53000_fixup(struct pci_dev *d)
{
    struct pci_bus *b;
    struct list_head *ln;
    struct resource *res;
    unsigned int pos, reg, size;
    u32 *base, l, align, *pcie = NULL;
    u8 port, bar;

    b = d->bus;

    /* Fix up external PCI  and not the host bridge */
    /* only fix up resources for devices */
    if (b->number != 0) {
	port = PCIE_GET_PORT_BY_BUS(b->number);

	if (!fixup_pci_flag[port]) {
	    pci_membase = SI_PCI_MEM(port);
	    for (ln = b->devices.next; ln != &b->devices; ln = ln->next) {
		d = pci_dev_b(ln);
		if (PCI_SLOT(d->devfn) == 0) {
		    continue;
		}
		bar = 0;
		/* Fix up resource bases */
		for (pos = 0; pos < 6; pos++) {
		    res = &d->resource[pos];
		    base = (res->flags & IORESOURCE_IO) ?
			&pci_iobase : &pci_membase;
		    reg = PCI_BASE_ADDRESS_0 + (pos << 2);

		    pci_read_config_dword(d, reg, &l);

		    if (l & PCI_BASE_ADDRESS_MEM_TYPE_64) {
			pos++;
		    }
		    if (res->end) {
			size = res->end - res->start + 1;
			align =
			    (*base & (size - 1)) ? (*base +
						    size) & ~(size -
							      1) : *base;
			/* check if there's enough resource to assign */
			if ((align + size) <=
			    (SI_PCI_MEM(port) + SI_PCI_MEM_SZ)) {
			    *base = align;
			    res->start = *base;
			    res->end = res->start + size - 1;
			    *base += size;
			    pci_write_config_dword(d, reg, res->start);

			    pr_debug
				("PCI: reserve %s region #%d:%x@%x for device %s\n",
				 res->
				 flags & IORESOURCE_IO ? "I/O" : "mem",
				 bar, size, res->start, pci_name(d));

			} else {

			    pr_debug("PCI: Unable to reserve %s region #%d:%llx@%llx " "for device %s\n", res->flags & IORESOURCE_IO ? "I/O" : "mem", bar,	/* PCI BAR # */
				     (unsigned long long)
				     pci_resource_len(d, bar),
				     (unsigned long long)
				     pci_resource_start(d, bar),
				     pci_name(d));
			    break;
			}
		    }

		    bar++;
		}
		/* Fix up interrupt lines */
		pcie = si_setcore(sih, PCIE_CORE_ID, port);
		if (pcie)
		    d->irq = si_irq(sih) + 2;
		else {
		    d->irq = 2;
		    printk("ERROR: pcie core does not exist!\n");
		}
		pci_write_config_byte(d, PCI_INTERRUPT_LINE, d->irq);
		printk("dev %x irq %d\n", d->device, d->irq);
	    }
	    fixup_pci_flag[port] = TRUE;
	}
    }
}

DECLARE_PCI_FIXUP_FINAL(PCI_ANY_ID, PCI_ANY_ID, bcm53000_fixup);

int pcibios_plat_dev_init(struct pci_dev *dev)
{
    return 0;
}

int __init pcibios_map_irq(const struct pci_dev *dev, u8 slot, u8 pin)
{
    return (dev->irq + 2);
}

static void __init pcibios_fixup_resources(struct pci_dev *dev)
{
    ulong flags;
    uint coreidx;
    u16 pci_cmd;
    void *regs;

    if (dev->bus->number == 0) {
	/*
	 * Chipcommon, RAM controller and PCI bridge must not be reset!
	 */
	if (dev->device == MIPS_CORE_ID ||
	    dev->device == MIPS33_CORE_ID ||
	    dev->device == MIPS74K_CORE_ID ||
	    dev->device == DMEMC_CORE_ID ||
	    dev->device == PCIE_CORE_ID ||
	    dev->device == SOCRAM_CORE_ID || dev->device == CC_CORE_ID) {
	    return;
	}
	spin_lock_irqsave(&sih_lock, flags);
	coreidx = si_coreidx(sih);
	regs = si_setcoreidx(sih, PCI_SLOT(dev->devfn));
	if (!regs) {
	    si_setcoreidx(sih, coreidx);
	    spin_unlock_irqrestore(&sih_lock, flags);
	    return;
	}

	/*
	 * The USB core requires a special bit to be set during core
	 * reset to enable host (OHCI) mode. Resetting the SB core here
	 * is a hack for compatibility with vanilla usb-ohci so that it
	 * does not have to know about SB.  A driver that wants to  use
	 * the  USB core in device mode should know about SB and should
	 * reset the bit back to 0.
	 */
	if (si_coreid(sih) == USB_CORE_ID) {
	    /* PR10028 WAR: Disable currently selected USB core first */
	    si_core_disable(sih, si_core_cflags(sih, 0, 0));
	    si_core_reset(sih, 1 << 13, 0);
	}
	/*
	 * USB 2.0 special considerations:
	 *
	 * 1. Since the core supports both OHCI and EHCI functions, it must
	 *    only be reset once.
	 *
	 * 2. In addition to the standard SI reset sequence, the Host Control
	 *    Register must be programmed to bring the USB core and various
	 *    phy components out of reset.
	 */
	else if (si_coreid(sih) == USB20H_CORE_ID) {
	    if (!si_iscoreup(sih)) {
		si_core_reset(sih, 0, 0);
		writel(0x7ff, (uintptr) regs + 0x200);
		udelay(1);
	    }
	} else {
	    si_core_reset(sih, 0, 0);
	}
	si_setcoreidx(sih, coreidx);
	spin_unlock_irqrestore(&sih_lock, flags);
	return;
    }

    if (PCI_SLOT(dev->devfn) != 0)
	return;

    if (PCIE_IS_BUS_HOST_BRIDGE(dev->bus->number)) {
	printk("PCI: Fixing up bridge\n");

	/* Enable PCI bridge bus mastering and transparent access */
	pci_read_config_word(dev, PCI_COMMAND, &pci_cmd);
	pci_cmd |=
	    (PCI_COMMAND_IO | PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER);
	pci_write_config_word(dev, PCI_COMMAND, pci_cmd);

	/* Enable PCI bridge BAR1 prefetch and burst */
	pci_write_config_dword(dev, PCI_BAR1_CONTROL, 0x3);
    }

}

DECLARE_PCI_FIXUP_FINAL(PCI_ANY_ID, PCI_ANY_ID, pcibios_fixup_resources);
