/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */
/*
 * Broadcom bcm53000 MIPS boards configuration
 *
 * $Copyright Open Broadcom Corporation$
 *
 *
 */

#ifndef _bcm53000_h_
#define _bcm53000_h_

/* Virtual IRQ base, after last hw IRQ */
#define SBMIPS_VIRTIRQ_BASE	6

/* # IRQs, hw and sw IRQs */
#define SBMIPS_NUMIRQS	8

#endif				/* _bcm53000_h_ */
