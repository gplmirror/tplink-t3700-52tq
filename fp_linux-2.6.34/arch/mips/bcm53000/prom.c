/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */
/*
 * Early initialization code for BCM953000 boards
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <asm/bootinfo.h>
#include <asm/io.h>
#include <asm/reboot.h>
#include <typedefs.h>
#include <osl.h>
#include <bcmutils.h>
#include <hndsoc.h>
#include <siutils.h>
#include <hndcpu.h>
#include <mips74k_core.h>
#include <cfe_api.h>
#include <cfe_error.h>

#ifdef CONFIG_HIGHMEM
#define MAX_RAM_SIZE (0xffffffffULL)
#else
#define MAX_RAM_SIZE (0x1fffffffULL)
#endif

#define CL_SIZE                     COMMAND_LINE_SIZE
#define BCM53000_MAX_MEM_REGIONS 8
phys_t board_mem_region_addrs[BCM53000_MAX_MEM_REGIONS];
phys_t board_mem_region_sizes[BCM53000_MAX_MEM_REGIONS];
unsigned int board_mem_region_count;

int cfe_cons_handle;

#ifdef CONFIG_BLK_DEV_INITRD
extern unsigned long initrd_start, initrd_end;
#endif


#define MB << 20

#ifdef CONFIG_BCM53000_HIGHMEM

#define EXTVBASE        0xc0000000
#define ENTRYLO(x)      ((pte_val(pfn_pte((x) >> _PFN_SHIFT, PAGE_KERNEL_UNCACHED)) >> 6) | 1)
#define UNIQUE_ENTRYHI(idx) (CKSEG0 + ((idx) << (PAGE_SHIFT + 1)))


static unsigned long tmp_tlb_ent __initdata;

/* Initialize the wired register and all tlb entries to
 * known good state.
 */
  void __init
early_tlb_init(void)
{
  unsigned long  index;
  struct cpuinfo_mips *c = &current_cpu_data;

  tmp_tlb_ent = c->tlbsize;

  /*
   * initialize entire TLB to uniqe virtual addresses
   * but with the PAGE_VALID bit not set
   */
  write_c0_wired(0);
  write_c0_pagemask(PM_DEFAULT_MASK);

  write_c0_entrylo0(0);   /* not _PAGE_VALID */
  write_c0_entrylo1(0);

  for (index = 0; index < c->tlbsize; index++) {
    /* Make sure all entries differ. */
    write_c0_entryhi(UNIQUE_ENTRYHI(index+32));
    write_c0_index(index);
    mtc0_tlbw_hazard();
    tlb_write_indexed();
  }

  tlbw_use_hazard();

}

void __init
add_tmptlb_entry(unsigned long entrylo0, unsigned long entrylo1,
    unsigned long entryhi, unsigned long pagemask)
{
  /* write one tlb entry */
  --tmp_tlb_ent;
  write_c0_index(tmp_tlb_ent);
  write_c0_pagemask(pagemask);
  write_c0_entryhi(entryhi);
  write_c0_entrylo0(entrylo0);
  write_c0_entrylo1(entrylo1);
  mtc0_tlbw_hazard();
  tlb_write_indexed();
  tlbw_use_hazard();
}

#endif


static void ATTRIB_NORET cfe_linux_exit(void *arg)
{
    int warm = *(int *) arg;

    if (smp_processor_id()) {
	static int reboot_smp;

	/* Don't repeat the process from another CPU */
	if (!reboot_smp) {
	    /* Get CPU 0 to do the cfe_exit */
	    reboot_smp = 1;
	    smp_call_function(cfe_linux_exit, arg, 1);
	}
    } else {
	printk("Passing control back to CFE...\n");
	cfe_exit(warm, 0);
	printk("cfe_exit returned??\n");
    }
	while (1);
}

static void ATTRIB_NORET cfe_linux_restart(char *command)
{
    static const int zero;

    cfe_linux_exit((void *) &zero);
}

static void ATTRIB_NORET cfe_linux_halt(void)
{
    static const int one = 1;

    cfe_linux_exit((void *) &one);
}
#ifdef CONFIG_BCM53000_HIGHMEM
#define SI_SDRAM_R2 0x80000000
#endif
static __init void prom_meminit(void)
{
    u64 addr, size, type;	/* regardless of 64BIT_PHYS_ADDR */
    int mem_flags = 0;
    unsigned int idx;
    int rd_flag;
	unsigned long mem, extmem = 0, off, data;
#ifdef CONFIG_BLK_DEV_INITRD
    unsigned long initrd_pstart;
    unsigned long initrd_pend;

    initrd_pstart = CPHYSADDR(initrd_start);
    initrd_pend = CPHYSADDR(initrd_end);
    if (initrd_start && ((initrd_pstart > MAX_RAM_SIZE)
			 || (initrd_pend > MAX_RAM_SIZE))) {
	panic("initrd out of addressable memory");
    }
#endif				/* INITRD */
	off = (unsigned long)prom_init;
	data = *(unsigned long *)prom_init;
	/* Figure out memory size by finding aliases */
	for (mem = (1 MB); mem < (128 MB); mem <<= 1) {
		if (*(unsigned long *)(off + mem) == data)
			break;
		 }
#ifdef CONFIG_BCM53000_HIGHMEM
	if (mem == 128 MB) {
		early_tlb_init();
		/* Add one temporary TLB entries to map SDRAM Region 2.
		    *      Physical        Virtual
			*      0x80000000      0xc0000000      (1st: 256MB)
			*      0x90000000      0xd0000000      (2nd: 256MB)
			*/
		add_tmptlb_entry(ENTRYLO(SI_SDRAM_R2),
				ENTRYLO(SI_SDRAM_R2 + (256 MB)),
				EXTVBASE, PM_256M);
		off = EXTVBASE + __pa(off);
		for (extmem = (128 MB); extmem < (512 MB); extmem <<= 1) {
			if (*(unsigned long *)(off + extmem) == data)
				break;

		}
		extmem -= mem;

		/* Keep tlb entries back in consistent state */
		 early_tlb_init();

	}
#endif  /*  CONFIG_BCM53000_HIGHMEM */


    for (idx = 0;
	 cfe_enummem(idx, mem_flags, &addr, &size,
		     &type) != CFE_ERR_NOMORE; idx++) {
	rd_flag = 0;
	if (type == CFE_MI_AVAILABLE) {
	    /*
	     * See if this block contains (any portion of) the
	     * ramdisk
	     */
#ifdef CONFIG_BLK_DEV_INITRD
	    if (initrd_start) {
		if ((initrd_pstart > addr) &&
		    (initrd_pstart < (addr + size))) {
		    add_memory_region(addr,
				      initrd_pstart - addr, BOOT_MEM_RAM);
		    rd_flag = 1;
		}
		if ((initrd_pend > addr) && (initrd_pend < (addr + size))) {
		    add_memory_region(initrd_pend,
				      (addr + size) - initrd_pend,
				      BOOT_MEM_RAM);
		    rd_flag = 1;
		}
	    }
#endif
	    if (!rd_flag) {
		if (addr > MAX_RAM_SIZE)
		    continue;
		if (addr + size > MAX_RAM_SIZE)
		    size = MAX_RAM_SIZE - (addr + size) + 1;
		/*
		 * memcpy/__copy_user prefetch, which
		 * will cause a bus error for
		 * KSEG/KUSEG addrs not backed by RAM.
		 * Hence, reserve some padding for the
		 * prefetch distance.
		 */
		if (size > 512)
		    size -= 512;
		add_memory_region(addr, size, BOOT_MEM_RAM);
	    }
		board_mem_region_addrs[board_mem_region_count] = addr;
		board_mem_region_sizes[board_mem_region_count] = size;
		board_mem_region_count++;
		if (board_mem_region_count == BCM53000_MAX_MEM_REGIONS) {
		/*
		 * Too many regions.  Need to configure more
		 */
			while (1);
	    }
	}
    }
#ifdef CONFIG_BLK_DEV_INITRD
    if (initrd_start) {
	add_memory_region(initrd_pstart, initrd_pend - initrd_pstart,
			  BOOT_MEM_RESERVED);
    }
#endif
#ifdef CONFIG_BCM53000_HIGHMEM
	if (extmem) {
		/* We should deduct 0x1000 from the second memory
		 * region, because of the fact that processor does prefetch.
		 * Now that we are deducting a page from second memory
		 * region, we could add the earlier deducted 4KB (from first bank)
		 * to the second region (the fact that 0x80000000 -> 0x88000000
		 * shadows 0x0 -> 0x8000000)
		 */
		if (MIPS74K(current_cpu_data.processor_id) && (mem == (128 MB)))
			extmem -= 0x1000;
		add_memory_region(SI_SDRAM_R2 + (128 MB) - 0x1000 , extmem, BOOT_MEM_RAM);
	}
#endif
}

#ifdef CONFIG_BLK_DEV_INITRD
static int __init initrd_setup(char *str)
{
    char rdarg[64];
    int idx;
    char *tmp, *endptr;
    unsigned long initrd_size;

    /* Make a copy of the initrd argument so we can smash it up here */
    for (idx = 0; idx < sizeof(rdarg) - 1; idx++) {
	if (!str[idx] || (str[idx] == ' '))
	    break;
	rdarg[idx] = str[idx];
    }

    rdarg[idx] = 0;
    str = rdarg;

    /*
     *Initrd location comes in the form "<hex size of ramdisk in bytes>@<location in memory>"
     *  e.g. initrd=3abfd@80010000.  This is set up by the loader.
     */
    for (tmp = str; *tmp != '@'; tmp++) {
	if (!*tmp) {
	    goto fail;
	}
    }
    *tmp = 0;
    tmp++;
    if (!*tmp) {
	goto fail;
    }
    initrd_size = simple_strtoul(str, &endptr, 16);
    if (*endptr) {
	*(tmp - 1) = '@';
	goto fail;
    }
    *(tmp - 1) = '@';
    initrd_start = simple_strtoul(tmp, &endptr, 16);
    if (*endptr) {
	goto fail;
    }
    initrd_end = initrd_start + initrd_size;
    printk("Found initrd of %lx@%lx\n", initrd_size, initrd_start);
    return 1;
  fail:
    printk("Bad initrd argument.  Disabling initrd\n");
    initrd_start = 0;
    initrd_end = 0;
    return 1;
}

#endif

void __init prom_init(void)
{
    uint32_t cfe_ept, cfe_handle;
    unsigned int cfe_eptseal;
    int argc = fw_arg0;
    char **envp = (char **) fw_arg2;
    int *prom_vec = (int *) fw_arg3;
    char temp[8];

    _machine_restart = cfe_linux_restart;
    _machine_halt = cfe_linux_halt;

    /*
     * Check if a loader was used; if NOT, the 4 arguments are
     * what CFE gives us (handle, 0, EPT and EPTSEAL)
     */
    if (argc < 0) {
	cfe_handle = (uint32_t) (long) argc;
	cfe_ept = (long) envp;
	cfe_eptseal = (uint32_t) (unsigned long) prom_vec;
    } else {
	if ((int32_t) (long) prom_vec < 0) {
	    /*
	     * Old loader; all it gives us is the handle,
	     * so use the "known" entrypoint and assume
	     * the seal.
	     */
	    cfe_handle = (uint32_t) (long) prom_vec;
	    cfe_ept = (uint32_t) ((int32_t) 0x9fc00500);
	    cfe_eptseal = CFE_EPTSEAL;
	} else {
	    /*
	     * Newer loaders bundle the handle/ept/eptseal
	     * Note: prom_vec is in the loader's useg
	     * which is still alive in the TLB.
	     */
	    cfe_handle = (uint32_t) ((int32_t *) prom_vec)[0];
	    cfe_ept = (uint32_t) ((int32_t *) prom_vec)[2];
	    cfe_eptseal = (unsigned int) ((uint32_t *) prom_vec)[3];
	}
    }
    if (cfe_eptseal != CFE_EPTSEAL) {
		/* too early for panic to do any good */
		printk("CFE's entrypoint seal doesn't match. Spinning.");
		while (1);
    }
    cfe_init(cfe_handle, cfe_ept);
    /*
     * Get the handle for (at least) prom_putchar, possibly for
     * boot console
     */
    cfe_cons_handle = cfe_getstdhandle(CFE_STDHANDLE_CONSOLE);

    /*
     * Overwrite LINUX_CMDLINE if it's given from bootloader.
     * Since cfe_getenv() will overwrite the buffer even if not found,
     * we use a temp buffer to check if the varaible is available.
     */
    if (cfe_getenv("LINUX_CMDLINE", temp, sizeof(temp)) == 0) {
	cfe_getenv("LINUX_CMDLINE", arcs_cmdline, CL_SIZE);
    }
#ifdef CONFIG_BLK_DEV_INITRD
    {
	char *ptr;
	/* Need to find out early whether we've got an initrd.
	 * So scan the list looking now */
	for (ptr = arcs_cmdline; *ptr; ptr++) {
		while (*ptr == ' ')
			ptr++;

	    if (!strncmp(ptr, "initrd=", 7)) {
		initrd_setup(ptr + 7);
		break;
	    } else {
		while (*ptr && (*ptr != ' ')) {
		    ptr++;
		}
	    }
	}
    }
#endif				/* CONFIG_BLK_DEV_INITRD */

    /* Not sure this is needed, but it's the safe way. */
    arcs_cmdline[CL_SIZE - 1] = 0;


    prom_meminit();
}

void __init prom_free_prom_memory(void)
{
}
