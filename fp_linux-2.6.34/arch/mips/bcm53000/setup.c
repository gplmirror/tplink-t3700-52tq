/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */
/*
 * Generic setup routines for Broadcom MIPS boards
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <asm/bootinfo.h>
#include <asm/time.h>
#include <asm/reboot.h>

#ifdef CONFIG_MTD_PARTITIONS
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/minix_fs.h>
#include <linux/ext2_fs.h>
#include <linux/romfs_fs.h>
#include <linux/cramfs_fs.h>
#endif

#include <typedefs.h>
#include <bcmdevs.h>
#include <bcmutils.h>
#include <bcmnvram.h>
#include <siutils.h>
#include <hndmips.h>
#include <sbchipc.h>
#include <hndchipc.h>
#include <asm/bcmsi/cfe_api.h>
#include <asm/bcmsi/cfe_error.h>

extern void bcm53000_time_init(void);

/* Global SB handle */
si_t *bcm53000_sih = NULL;
spinlock_t bcm53000_sih_lock = SPIN_LOCK_UNLOCKED;
EXPORT_SYMBOL(bcm53000_sih);
EXPORT_SYMBOL(bcm53000_sih_lock);

/* Convenience */
#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock

#if (!defined(CONFIG_CHEETAH_KS_L2P)) && (!defined(CONFIG_CHEETAH_KS_10GS))
extern int m41t81_probe(void);
extern int m41t81_set_time(unsigned long);
extern unsigned long m41t81_get_time(void);

static int bcm953000_rtc_hookup(void)
{
	if (m41t81_probe() < 0) {
		return -ENXIO;
	} 
		return 0;
}

void read_persistent_clock(struct timespec *ts)
{
    /*return m41t81_get_time();*/
    m41t81_get_time();
    return;
}

int rtc_mips_set_time(unsigned long time)
{
    return m41t81_set_time(time);
}
#endif

void bcm53000_machine_restart(char *command)
{
    char name[128];
    printk("Please stand by while rebooting the system...\n");

    local_irq_disable();

    if (sih->chiprev != 0) {
      /* Using the GPIO pins wired to CPU/board reset to achieve system reboot */
      /* BCM953001R24, BCM953003EWAP using GPIO 7 */
      /* BCM953003C using GPIO 11 and 12 */
       cfe_getenv("boardtype", name, 128);
       if (!strncmp(name, "bcm953000", strlen("bcm953000")) ||
           !strncmp(name, "bcm953001r", strlen("bcm953001r"))) {
		*(volatile uint32*)(0xb8000064) = 0x00000080;
		*(volatile uint32*)(0xb8000068) = 0x00000080;
		*(volatile uint32*)(0xb8000064) = 0x00000000;
       } else if (!strncmp(name, "bcm953003c", strlen("bcm953003c"))) {
                *(volatile uint32*)(0xb8000064) = 0x00001800;
                *(volatile uint32*)(0xb8000068) = 0x0000f800;
                *(volatile uint32*)(0xb8000064) = 0x00000000;
       }
	/* Set the watchdog timer to reset immediately */
	si_watchdog(sih, 1);
    } else {
	/* Using the GPIO pins wired to CPU/board reset to achieve system reboot */
	/* BCM953003C using GPIO 11 and 12 */
	/* BCM953001R24M using GPIO 7 */
	cfe_getenv("boardtype", name, 128);
	if (!strncmp(name, "bcm953003c", strlen("bcm953003c"))) {
		*(volatile uint32*)(0xb8000064) = 0x00001800;
		*(volatile uint32*)(0xb8000068) = 0x0000f800;
		*(volatile uint32*)(0xb8000064) = 0x00000000;
	} else if (!strncmp(name, "bcm953001r", strlen("bcm953001r"))) {
		*(volatile uint32*)(0xb8000064) = 0x00000080;
		*(volatile uint32*)(0xb8000068) = 0x00000080;
		*(volatile uint32*)(0xb8000064) = 0x00000000;
	} else {
	    /* The boards bcm953003rsp and bcm953003rwap use */
	    /* GPIO 6 and 7 for the system reset */
		*(volatile uint32*)(0xb8000064) = 0x000000c0;
		*(volatile uint32*)(0xb8000068) = 0x000000c0;
		*(volatile uint32*)(0xb8000064) = 0x00000000;
	    /* Set the watchdog timer to reset immediately */
	    si_watchdog(sih, 1);
	}
   }
	while (1)
		;
}
#ifdef CONFIG_CHEETAH_MINIKS
void miniks_machine_restart(char *command)
{
  void * pointer;
  chipcregs_t *chipcommon;

  local_irq_disable();
  pointer = ioremap_nocache(0x18000000, PAGE_SIZE);
  chipcommon = (chipcregs_t *)pointer;
  if (chipcommon == NULL)
  {
    printk("Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
  else
  {
    chipcommon->gpiocontrol = 0x00;
    chipcommon->gpioout = 0xfeff;
    chipcommon->gpioouten = 0x100;
    /*System should reset here. Use cpu reset if not */
    printk("\n      Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
}
#endif

#ifdef CONFIG_CHEETAH_KS_L2P
void ksl2p_machine_restart(char *command)
{
  char *cpldbase;
  cpldbase = (char *)ioremap_nocache(0x1E000000, 0x16);
  local_irq_disable();
  if (cpldbase == NULL)
  {
    printk(" Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
  else
  {
    cpldbase[8] = 0x20;
    /*System should reset here. Use cpu reset if not */
    printk("\n        Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);

  }
}
#endif
#ifdef CONFIG_CHEETAH_KS_10GS
void ks10gs_machine_restart(char *command)
{
  void * pointer;
  chipcregs_t *chipcommon;

  local_irq_disable();
  pointer = ioremap_nocache(0x18000000, PAGE_SIZE);
  chipcommon = (chipcregs_t *)pointer;
  if (chipcommon == NULL)
  {
    printk("Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
  else
  {
    chipcommon->gpiocontrol = 0x00;
    chipcommon->gpioout = 0xf7ff;
    chipcommon->gpioouten = 0x800;
    /*System should reset here. Use cpu reset if not */
    printk("\n      Unable to do the system reset. Resetting CPU\n");
    si_watchdog(sih, 1);
  }
}
#endif
void bcm53000_machine_halt(void)
{
    printk("System halted\n");

    /* Disable interrupts and watchdog and spin forever */
    local_irq_disable();
    si_watchdog(sih, 0);
    while (1)
		;
}

#ifdef CONFIG_SERIAL_8250
static void __init serial_setup(si_t *sih)
{
    /*
     * Since 8250 UART driver will control divider of UART module to achieve
     * baud rate specified in kernel arguments, we simply use ALP clock
     * (so the baud base is clock / 2).
     */
    si_serial_init(sih, NULL, 0);
}
#endif				/* CONFIG_SERIAL_8250 */

/* Virtual IRQ base, after last hw IRQ */
#define SBMIPS_VIRTIRQ_BASE	6

void __init plat_mem_setup(void)
{
    /* Get global SB handle */
    sih = si_kattach(SI_OSH);

    /* Initialize clocks and interrupts */
    si_mips_init(sih, SBMIPS_VIRTIRQ_BASE);

#ifdef CONFIG_SERIAL_8250
    /* Initialize UARTs */
    serial_setup(sih);
#endif

#if (!defined(CONFIG_CHEETAH_KS_L2P)) && (!defined(CONFIG_CHEETAH_KS_10GS))
    bcm953000_rtc_hookup();
#endif

    /* Generic setup */
    _machine_restart = bcm53000_machine_restart;
#ifdef CONFIG_CHEETAH_KS_L2P
    _machine_restart = ksl2p_machine_restart;
#endif
#ifdef CONFIG_CHEETAH_MINIKS
    _machine_restart = miniks_machine_restart;
#endif
#ifdef CONFIG_CHEETAH_KS_10GS
    _machine_restart = ks10gs_machine_restart;
#endif
    _machine_halt = bcm53000_machine_halt;
    /* _machine_power_off = bcm53000_machine_halt; */
    pm_power_off = bcm53000_machine_halt;

    /* board_timer_setup = bcm53000_timer_setup; */
}

const char *get_system_type(void)
{
    static char s[32];

    if (bcm53000_sih) {
	sprintf(s, "Broadcom BCM%X chip rev %d", bcm53000_sih->chip,
		bcm53000_sih->chiprev);
	return s;
    } else
	return "Broadcom bcm53000";
}

void __init bus_error_init(void)
{
}
