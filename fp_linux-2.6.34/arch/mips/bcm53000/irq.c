/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */
/*
 * Generic interrupt control functions for Broadcom MIPS boards
 *
 */
/*
 *	MIPS IRQ	Source
 *      --------        ------------------
 *             0	Software
 *             1        Software
 *             2        Hardware (shared)
 *             3        Hardware
 *             4        Hardware
 *             5        Hardware
 *             6        Hardware
 *             7        Hardware (r4k timer)
 *
 *      MIPS IRQ        Linux IRQ
 *      --------        -----------
 *         0 - 1        0 - 1
 *             2        8 and above
 *         3 - 7        3 - 7
 *
 * MIPS has 8 IRQs as indicated and assigned above. SI cores
 * that use dedicated MIPS IRQ3 to IRQ6 are 1-to-1 mapped to
 * linux IRQ3 to IRQ6. SI cores sharing MIPS IRQ2 are mapped
 * to linux IRQ8 and above as virtual IRQs using the following
 * mapping:
 *
 *   <linux-IRQ#> = <SI-core-flag> + <base-IRQ> + 2
 *
 * where <base-IRQ> is specified in setup.c when calling
 * si_mips_init(), 2 is to offset the two software IRQs.
 *
 */

#include <generated/autoconf.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/kernel_stat.h>

#include <asm/mipsregs.h>

#ifdef CONFIG_KGDB
#include <asm/kgdb.h>
#endif

#include <typedefs.h>
#include <osl.h>
#include <bcmutils.h>
#include <hndsoc.h>
#include <siutils.h>
#include <hndcpu.h>
#include <mipsinc.h>
#include <mips74k_core.h>
#include "bcm53000.h"

/* cp0 SR register IM field */
#define SR_IM(irq)	(1 << ((irq) + STATUSB_IP0))

/* other local constants */
#define NUM_IRQS	32

/* local variables and functions */
static int mipsirq = -1;	/* MIPS core virtual IRQ number */
static uint32 shints = 0;	/* Set of shared interrupts */
static int irq2en = 0;		/* MIPS IRQ2 enable count */
static uint *mips_corereg = NULL;

/* global variables and functions */
extern si_t *bcm53000_sih;	/* defined in setup.c */


extern asmlinkage void brcmIRQ(void);

/*
 * Route interrupts to ISR(s).
 *
 * This function is entered with the IE disabled. It can be
 * re-entered as soon as the IE is re-enabled in function
 * handle_IRQ_envet().
 */
asmlinkage void plat_irq_dispatch(void)
{
    u32 pending, ipvec;
    uint32 flags = 0;

    unsigned int irq;

    /* Disable MIPS IRQs with pending interrupts */
    pending = read_c0_cause() & CAUSEF_IP;
    pending &= read_c0_status();
    clear_c0_status(pending);
    irq_disable_hazard();


    /*
     * Handle MIPS timer interrupt. Re-enable MIPS IRQ7
     * immediately after servicing the interrupt so that
     * we can take this kind of interrupt again later
     * while servicing other interrupts.
     */
    if (pending & CAUSEF_IP7) {
	do_IRQ(7);
	pending &= ~CAUSEF_IP7;
	set_c0_status(STATUSF_IP7);
	irq_enable_hazard();
    }

    /*
     * Build bitvec for pending interrupts. Start with
     * MIPS IRQ2 and add linux IRQs to higher bits to
     * make the interrupt processing uniform.
     */
    ipvec = pending >> CAUSEB_IP2;
    if (pending & CAUSEF_IP2) {

	/* Read intstatus */
	if (mips_corereg)
		flags =
		R_REG(NULL, &((mips74kregs_t *) mips_corereg)->intstatus);

	flags &= shints;
	ipvec |= (flags << SBMIPS_VIRTIRQ_BASE);
    }

    /* Shared interrupt bits are shifted to respective bit positions in
     * ipvec above. IP2 (bit 0) is of no significance, hence shifting the
     * bit map by 1 to the right.
     */
    ipvec >>= 1;

    /*
     * Handle all other interrupts. Re-enable disabled MIPS IRQs
     * after processing all pending interrupts.
     */
    for (irq = 3; ipvec != 0; irq++) {
	if (ipvec & 1)
		do_IRQ(irq);

	ipvec >>= 1;
    }
    set_c0_status(pending);
    irq_enable_hazard();

}

static void enable_brcm_irq(unsigned int irq)
{
    set_c0_status(SR_IM(irq));
    irq_enable_hazard();
}

static void disable_brcm_irq(unsigned int irq)
{
    clear_c0_status(SR_IM(irq));
    irq_disable_hazard();
}

static void ack_brcm_irq(unsigned int irq)
{
    /* Already done in brcm_irq_dispatch */
}

static void end_brcm_irq(unsigned int irq)
{
    /* Already done in brcm_irq_dispatch */
}


/* Control functions for linux IRQ8 and above */
static void enable_brcm_irq2(unsigned int irq)
{
    ASSERT(irq2en >= 0);
    if (irq2en++)
	return;
    enable_brcm_irq(2);
}

static void disable_brcm_irq2(unsigned int irq)
{
    ASSERT(irq2en > 0);
    if (--irq2en)
	return;
    disable_brcm_irq(2);
}

static void ack_brcm_irq2(unsigned int irq)
{
    /* Already done in brcm_irq_dispatch()! */
}

static void end_brcm_irq2(unsigned int irq)
{
    /* Already done in brcm_irq_dispatch()! */
}

static struct irq_chip brcm_irq_chip = {
	.name	=	"MIPS",
	.ack	=	ack_brcm_irq,
	.mask	=	disable_brcm_irq,
	.mask_ack	=	ack_brcm_irq,
	.unmask	=	enable_brcm_irq,
	.end	=	end_brcm_irq,

};

static struct irq_chip brcm_irq2_chip = {
	.name	=	"IRQ2",
	.ack	=	ack_brcm_irq2,
	.mask	=	disable_brcm_irq2,
	.mask_ack	=	ack_brcm_irq2,
	.unmask	=	enable_brcm_irq2,
	.end	=	end_brcm_irq2,

};

void __init arch_init_irq(void)
{
    struct cpuinfo_mips *c = &current_cpu_data;
    int i;
    uint32 coreidx, mips_core_id;
    void *regs;
    uint32 *intmask;
    if (MIPS74K(c->processor_id))
	mips_core_id = MIPS74K_CORE_ID;
    else {
	printk(KERN_ERR "MIPS CPU type %x unknown", c->processor_id);
	return;
    }

    /* Cache chipc and mips33 config registers */
    ASSERT(bcm53000_sih);
    coreidx = si_coreidx(bcm53000_sih);
    regs = si_setcore(bcm53000_sih, mips_core_id, 0);
    mipsirq = si_irq(bcm53000_sih);
    si_setcoreidx(bcm53000_sih, coreidx);

    /* Use intmask5 register to route the timer interrupt */
    /*  move to si_mips_init
       intmask = (uint32 *) &((mips74kregs_t *)regs)->intmask[5];
       W_REG(NULL, intmask, 1 << 31);
     */
    intmask = (uint32 *) &((mips74kregs_t *) regs)->intmask[0];
    shints = R_REG(NULL, intmask);

    /* Save the pointer to mips core registers */
    mips_corereg = regs;


    /* Install interrupt controllers */
    for (i = 0; i < NR_IRQS; i++) {
	set_irq_chip(i,
		     (i <
		      SBMIPS_NUMIRQS ? &brcm_irq_chip : &brcm_irq2_chip));
    }
}
