/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/serial_reg.h>
#include <linux/interrupt.h>
#include <asm/addrspace.h>
#include <asm/io.h>
#include <asm/time.h>

#include <typedefs.h>
#include <osl.h>
#include <bcmnvram.h>
#include <sbconfig.h>
#include <sbchipc.h>
#include <siutils.h>
#include <hndmips.h>
#include <hndcpu.h>


/* Global SI handle */
extern si_t *bcm53000_sih;
extern spinlock_t bcm53000_sih_lock;

/* Convenience */
#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock
#define WATCHDOG_MIN	3000	/* milliseconds */
#define WATCHDOG_MAX	5592	/* milliseconds */

extern int panic_timeout;
static int watchdog = 0;

void __init plat_time_init(void)
{
	unsigned int hz;
	/*
	 * Use deterministic values for initial counter interrupt
	 * so that calibrate delay avoids encountering a counter wrap.
	 */
	write_c0_count(0);
	write_c0_compare(0xffff);

	if (!(hz = si_cpu_clock(sih)))
		hz = 100000000;
	printk("CPU: BCM%04x rev %d at %d MHz\n", sih->chip, sih->chiprev,
			(hz + 500000) / 1000000);

	/* Set MIPS counter frequency for fixed_rate_gettimeoffset() */
	mips_hpt_frequency = hz / 2;

	/* Set watchdog interval in ms */
	watchdog = simple_strtoul(nvram_safe_get("watchdog"), NULL, 0);

	/* Ensure at least WATCHDOG_MIN
	 * Values above WATCHDOG_MAX overflow counter
	 */
	if (watchdog > 0) {
		if (watchdog < WATCHDOG_MIN)
			watchdog = WATCHDOG_MIN;

		if (watchdog > WATCHDOG_MAX)
			watchdog = WATCHDOG_MAX;
	}

	/* Set panic timeout in seconds */
	panic_timeout = watchdog / 1000;

}
