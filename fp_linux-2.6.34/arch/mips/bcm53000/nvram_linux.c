/*
 * Copyright (C) 2009 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
 *
 *
 */
/*
 * NVRAM variable manipulation (Linux kernel half)
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/slab.h>
#include <linux/bootmem.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/mtd/mtd.h>
#include <asm/addrspace.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/cacheflush.h>

#include <typedefs.h>
#include <bcmendian.h>
#include <bcmnvram.h>
#include <bcmutils.h>
#include <hndsoc.h>
#include <sbchipc.h>
#include <siutils.h>
#include <hndmips.h>
/*#include <sflash.h>*/

/* In BSS to minimize text size and page aligned so it can be mmap()-ed */
static char nvram_buf[NVRAM_SPACE] __attribute__ ((aligned(PAGE_SIZE)));

#ifdef MODULE

#define early_nvram_get(name) nvram_get(name)

#else				/* !MODULE */

/* Global SB handle */
extern void *bcm53000_sih;
extern spinlock_t bcm53000_sih_lock;
/* Convenience */
#define sih bcm53000_sih
#define sih_lock bcm53000_sih_lock
#define KB * 1024
#define MB * 1024 * 1024

/* Probe for NVRAM header */
static int __init early_nvram_init(void)
{
#ifdef CONFIG_QT
    struct nvram_header *header;
    header = (struct nvram_header *) nvram_buf;
    header->magic = 0xdadadede;
#else
    struct nvram_header *header;
    chipcregs_t *cc;
    /*struct sflash *info = NULL;*/
    int i;
    uint32 base, off, lim;
    u32 *src, *dst;

    if ((cc = si_setcoreidx(sih, SI_CC_IDX)) != NULL) {
	base = KSEG1ADDR(SI_FLASH2);

	switch (readl(&cc->capabilities) & CC_CAP_FLASH_MASK) {
	case PFLASH:
	    lim = SI_FLASH2_SZ;
	    break;

	case SFLASH_ST:
	case SFLASH_AT:
	   /* if ((info = sflash_init(sih, cc)) == NULL)*/
		return -1;
	    /*lim = info->size;*/
	    break;

	case FLASH_NONE:
	default:
	    return -1;
	}
    } else {
	/* extif assumed, Stop at 4 MB */
	base = KSEG1ADDR(SI_FLASH1);
	lim = SI_FLASH1_SZ;
    }

    off = FLASH_MIN;
    while (off <= lim) {
	/* Windowed flash access */
	header = (struct nvram_header *) KSEG1ADDR(base + off);
	if (header->magic == NVRAM_MAGIC && header->len <= NVRAM_SPACE) {

#ifdef CONFIG_CPU_LITTLE_ENDIAN
	    memcpy(nvram_buf, header, header->len);
#else
	    /* data is written little-endian (but header is not)  */
	    u32 *src = (u32 *) header;
	    u32 *dst = (u32 *) nvram_buf;

	    for (i = 0; i < sizeof(struct nvram_header); i += 4) {
		*dst++ = *src++;
	    }

	    for (; i < header->len && i < header->len; i += 4) {
		*dst++ = ltoh32(*src++);
	    }
#endif
	    if (nvram_calc_crc((struct nvram_header *) nvram_buf) ==
		(uint8) header->crc_ver_init) {
		goto found;
	    }
	}
	off <<= 1;
    }

    printk("early_nvram_init: NVRAM not found\n");
    return -1;

  found:
    src = (u32 *) header;
    dst = (u32 *) nvram_buf;
    for (i = 0; i < sizeof(struct nvram_header); i += 4)
	*dst++ = *src++;
    /* PR2620 WAR: Read data bytes as words */
    for (; i < header->len && i < NVRAM_SPACE; i += 4)
	*dst++ = ltoh32(*src++);

    return 0;
#endif
}

/* Early (before mm or mtd) read-only access to NVRAM */
static char *__init early_nvram_get(const char *name)
{
    char *var, *value, *end, *eq;

    if (!name)
	return NULL;
#if 0
    if (!nvram_buf[0])
	early_nvram_init();

    /* Look for name=value and return value */
    var = &nvram_buf[sizeof(struct nvram_header)];
    end = nvram_buf + sizeof(nvram_buf) - 2;
    end[0] = end[1] = '\0';
    for (; *var; var = value + strlen(value) + 1) {
	if (!(eq = strchr(var, '=')))
	    break;
	value = eq + 1;
	if ((eq - var) == strlen(name) &&
	    strncmp(var, name, (eq - var)) == 0)
	    return value;
    }

    return NULL;
#else
/* Too early? */
    if (sih == NULL)
	return NULL;

    if (!nvram_buf[0])
	if (early_nvram_init() != 0) {
	    printk("early_nvram_get: Failed reading nvram var %s\n", name);
	    return NULL;
	}

    /* Look for name=value and return value */
    var = &nvram_buf[sizeof(struct nvram_header)];
    end = nvram_buf + sizeof(nvram_buf) - 2;
    end[0] = end[1] = '\0';
    for (; *var; var = value + strlen(value) + 1) {
	if (!(eq = strchr(var, '=')))
	    break;
	value = eq + 1;
	if ((eq - var) == strlen(name)
	    && strncmp(var, name, (eq - var)) == 0)
	    return value;
    }

    return NULL;

#endif

}

#endif				/* !MODULE */

extern char *_nvram_get(const char *name);
extern int _nvram_set(const char *name, const char *value);
extern int _nvram_unset(const char *name);
extern int _nvram_getall(char *buf, int count);
extern int _nvram_commit(struct nvram_header *header);
extern int _nvram_init(void *sih);
extern void _nvram_exit(void);



/* Globals */
static spinlock_t nvram_lock = SPIN_LOCK_UNLOCKED;
static struct semaphore nvram_sem;
static unsigned long nvram_offset = 0;
static int nvram_major = -1;
static struct mtd_info *nvram_mtd = NULL;

int _nvram_read(void *buf)
{
    struct nvram_header *header = (struct nvram_header *) buf;
    size_t len;

    if (!nvram_mtd ||
	/* MTD_READ(nvram_mtd, nvram_mtd->size - NVRAM_SPACE, */
	nvram_mtd->read(nvram_mtd, nvram_mtd->size - NVRAM_SPACE,
			NVRAM_SPACE, &len, buf) ||
	len != NVRAM_SPACE || header->magic != NVRAM_MAGIC) {
	/* Maybe we can recover some data from early initialization */
	memcpy(buf, nvram_buf, NVRAM_SPACE);
    }
#ifndef CONFIG_CPU_LITTLE_ENDIAN
    /* data is written little-endian (but header is not) */
    {
	u32 *dst = (u32 *) buf;
	int i;
	for (i = 0; i < NVRAM_SPACE; i += 4) {
	    if (i > 4)
		*dst = ltoh32(*dst);
	    dst++;
	}
    }
#endif

    return 0;
}

struct nvram_tuple *_nvram_realloc(struct nvram_tuple *t, const char *name,
				   const char *value)
{
    if ((nvram_offset + strlen(value) + 1) > NVRAM_SPACE)
	return NULL;

    if (!t) {
	if (!
	    (t =
	     kmalloc(sizeof(struct nvram_tuple) + strlen(name) + 1,
		     GFP_ATOMIC)))
	    return NULL;

	/* Copy name */
	t->name = (char *) &t[1];
	strcpy(t->name, name);

	t->value = NULL;
    }

    /* Copy value */
    if (!t->value || strcmp(t->value, value)) {
	t->value = &nvram_buf[nvram_offset];
	strcpy(t->value, value);
	nvram_offset += strlen(value) + 1;
    }

    return t;
}

void _nvram_free(struct nvram_tuple *t)
{
    if (!t)
	nvram_offset = 0;
    else
	kfree(t);
}

int nvram_set(const char *name, const char *value)
{
    unsigned long flags;
    int ret;
    struct nvram_header *header;

    spin_lock_irqsave(&nvram_lock, flags);
    if ((ret = _nvram_set(name, value))) {
	/* Consolidate space and try again */
	if ((header = kmalloc(NVRAM_SPACE, GFP_ATOMIC))) {
	    if (_nvram_commit(header) == 0)
		ret = _nvram_set(name, value);
	    kfree(header);
	}
    }
    spin_unlock_irqrestore(&nvram_lock, flags);

    return ret;
}

char *real_nvram_get(const char *name)
{
    unsigned long flags;
    char *value;

    spin_lock_irqsave(&nvram_lock, flags);
    value = _nvram_get(name);
    spin_unlock_irqrestore(&nvram_lock, flags);

    return value;
}

char *nvram_get(const char *name)
{
    if (nvram_major >= 0)
	return real_nvram_get(name);
    else
	return early_nvram_get(name);
}

int nvram_unset(const char *name)
{
    unsigned long flags;
    int ret;

    spin_lock_irqsave(&nvram_lock, flags);
    ret = _nvram_unset(name);
    spin_unlock_irqrestore(&nvram_lock, flags);

    return ret;
}

static void erase_callback(struct erase_info *done)
{
    wait_queue_head_t *wait_q = (wait_queue_head_t *) done->priv;
    wake_up(wait_q);
}

#ifndef CONFIG_CPU_LITTLE_ENDIAN
void swap_nvram_data(struct nvram_header *header, int size)
{
    int i;
    u32 *ptr;

    ptr = (u32 *) header;
    for (i = 0; i < size; i += 4) {
	if (i > 4)
	    *ptr = htol32(*ptr);
	ptr++;
    }
}
#endif

int nvram_commit(void)
{
    char *buf;
    size_t erasesize, len;
    unsigned int i;
    int ret, hlen;
    struct nvram_header *header;
    unsigned long flags;
    u_int32_t offset;
    DECLARE_WAITQUEUE(wait, current);
    wait_queue_head_t wait_q;
    struct erase_info erase;

    if (!nvram_mtd) {
	printk("nvram_commit: NVRAM not found\n");
	return -ENODEV;
    }

    if (in_interrupt()) {
	printk("nvram_commit: not committing in interrupt\n");
	return -EINVAL;
    }

    /* Backup sector blocks to be erased */
    erasesize = ROUNDUP(NVRAM_SPACE, nvram_mtd->erasesize);
    if (!(buf = kmalloc(erasesize, GFP_KERNEL))) {
	printk("nvram_commit: out of memory\n");
	return -ENOMEM;
    }

    down(&nvram_sem);

    offset = nvram_mtd->size - erasesize;
    i = erasesize - NVRAM_SPACE;
    /* ret = MTD_READ(nvram_mtd, offset, i, &len, buf); */
    ret = nvram_mtd->read(nvram_mtd, offset, i, &len, buf);
    if (ret || len != i) {
	printk("nvram_commit: read error\n");
	ret = -EIO;
	goto done;

    }

    /* Regenerate NVRAM */
    header = (struct nvram_header *) (buf + erasesize - NVRAM_SPACE);
    spin_lock_irqsave(&nvram_lock, flags);
    ret = _nvram_commit(header);
    spin_unlock_irqrestore(&nvram_lock, flags);
    if (ret)
	goto done;

    hlen = header->len;

#ifndef CONFIG_CPU_LITTLE_ENDIAN
    swap_nvram_data(header, NVRAM_SPACE);
#endif

    /* Erase sector blocks */
    init_waitqueue_head(&wait_q);
    for (; offset < nvram_mtd->size - NVRAM_SPACE + hlen;
	 offset += nvram_mtd->erasesize) {
	erase.mtd = nvram_mtd;
	erase.addr = offset;
	erase.len = nvram_mtd->erasesize;
	erase.callback = erase_callback;
	erase.priv = (u_long) & wait_q;

	set_current_state(TASK_INTERRUPTIBLE);
	add_wait_queue(&wait_q, &wait);

	/* Unlock sector blocks */
	if (nvram_mtd->unlock)
	    nvram_mtd->unlock(nvram_mtd, offset, nvram_mtd->erasesize);

	/* if ((ret = MTD_ERASE(nvram_mtd, &erase))) { */
	if ((ret = nvram_mtd->erase(nvram_mtd, &erase))) {
	    set_current_state(TASK_RUNNING);
	    remove_wait_queue(&wait_q, &wait);
	    printk("nvram_commit: erase error\n");
	    goto done;
	}

	/* Wait for erase to finish */
	schedule();
	remove_wait_queue(&wait_q, &wait);
    }

    /* Write partition up to end of data area */
    offset = nvram_mtd->size - erasesize;
    i = erasesize - NVRAM_SPACE + hlen;
    /* ret = MTD_WRITE(nvram_mtd, offset, i, &len, buf); */
    ret = nvram_mtd->write(nvram_mtd, offset, i, &len, buf);
    if (ret || len != i) {
	printk("nvram_commit: write error\n");
	ret = -EIO;
	goto done;
    }

    offset = nvram_mtd->size - erasesize;
    /* ret = MTD_READ(nvram_mtd, offset, 4, &len, buf); */
    ret = nvram_mtd->read(nvram_mtd, offset, 4, &len, buf);

  done:
    up(&nvram_sem);
    kfree(buf);
    return ret;
}

int nvram_getall(char *buf, int count)
{
    unsigned long flags;
    int ret;

    spin_lock_irqsave(&nvram_lock, flags);
    ret = _nvram_getall(buf, count);
    spin_unlock_irqrestore(&nvram_lock, flags);

    return ret;
}

EXPORT_SYMBOL(nvram_get);
EXPORT_SYMBOL(nvram_getall);
EXPORT_SYMBOL(nvram_set);
EXPORT_SYMBOL(nvram_unset);
EXPORT_SYMBOL(nvram_commit);

static void dev_nvram_exit(void)
{
    int order = 0;
    struct page *page, *end;

#ifdef CONFIG_MTD
    if (nvram_mtd)
	put_mtd_device(nvram_mtd);
#endif

    while ((PAGE_SIZE << order) < NVRAM_SPACE)
	order++;
    end = virt_to_page(nvram_buf + (PAGE_SIZE << order) - 1);
    for (page = virt_to_page(nvram_buf); page <= end; page++)
	ClearPageReserved(page);

    _nvram_exit();
}

static int __init dev_nvram_init(void)
{
    int order = 0, ret = 0;
    struct page *page, *end;
    unsigned int i;

    /* Allocate and reserve memory to mmap() */
    while ((PAGE_SIZE << order) < NVRAM_SPACE)
	order++;
    end = virt_to_page(nvram_buf + (PAGE_SIZE << order) - 1);
    for (page = virt_to_page(nvram_buf); page <= end; page++)
	SetPageReserved(page);

#ifdef CONFIG_MTD
    /* Find associated MTD device */
    for (i = 0; i < MAX_MTD_DEVICES; i++) {
	nvram_mtd = get_mtd_device(NULL, i);
	if (nvram_mtd && ((int) nvram_mtd != -ENODEV)) {
	    if (!strcmp(nvram_mtd->name, "nvram") &&
		nvram_mtd->size >= NVRAM_SPACE)
		break;
	    put_mtd_device(nvram_mtd);
	}
    }
    if (i >= MAX_MTD_DEVICES)
	nvram_mtd = NULL;
#endif
    /* Initialize hash table lock */
    spin_lock_init(&nvram_lock);
    /* Initialize commit semaphore */
    init_MUTEX(&nvram_sem);
    /* Initialize hash table */
    if ((ret = _nvram_init(sih)) != 0)
	dev_nvram_exit();
    return ret;
}

module_init(dev_nvram_init);
module_exit(dev_nvram_exit);
