/*
 * BCM47XX Sonics SiliconBackplane MIPS core routines
 *
 * Copyright (C) 2004 Broadcom Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <asm/mach-bcm5621x/typedefs.h>
#include <asm//mach-bcm5621x/osl.h>
#include <asm/mach-bcm5621x/sbutils.h>
#include <asm/mach-bcm5621x/bcmdevs.h>
#include <asm/mach-bcm5621x/bcmutils.h>
#include <asm/mach-bcm5621x/sbconfig.h>
#include <asm/mach-bcm5621x/sbchipc.h>
#include <asm/mach-bcm5621x/sbmemc.h>

/* flash_waitcount */
#define	FW_W0_MASK	0x1f			/* waitcount0 */
#define	FW_W1_MASK	0x1f00			/* waitcount1 */
#define	FW_W1_SHIFT	8
#define	FW_W2_MASK	0x1f0000		/* waitcount2 */
#define	FW_W2_SHIFT	16
#define	FW_W3_MASK	0x1f000000		/* waitcount3 */
#define	FW_W3_SHIFT	24

#define INTERNAL_UART0_ONLY 0

/**********************************************************************
 * CP0 Registers
***********************************************************************/

#define C0_INX		0	/* CP0: TLB Index */
#define C0_RAND		1	/* CP0: TLB Random */
#define C0_TLBLO0	2	/* CP0: TLB EntryLo0 */
#define C0_TLBLO	C0_TLBLO0	/* CP0: TLB EntryLo0 */
#define C0_TLBLO1	3	/* CP0: TLB EntryLo1 */
#define C0_CTEXT	4	/* CP0: Context */
#define C0_PGMASK	5	/* CP0: TLB PageMask */
#define C0_WIRED	6	/* CP0: TLB Wired */
#define C0_BADVADDR	8	/* CP0: Bad Virtual Address */
#define C0_COUNT 	9	/* CP0: Count */
#define C0_TLBHI	10	/* CP0: TLB EntryHi */
#define C0_COMPARE	11	/* CP0: Compare */
#define C0_SR		12	/* CP0: Processor Status */
#define C0_STATUS	C0_SR	/* CP0: Processor Status */
#define C0_CAUSE	13	/* CP0: Exception Cause */
#define C0_EPC		14	/* CP0: Exception PC */
#define C0_PRID		15	/* CP0: Processor Revision Indentifier */
#define C0_CONFIG	16	/* CP0: Config */
#define C0_LLADDR	17	/* CP0: LLAddr */
#define C0_WATCHLO	18	/* CP0: WatchpointLo */
#define C0_WATCHHI	19	/* CP0: WatchpointHi */
#define C0_XCTEXT	20	/* CP0: XContext */
#define C0_DIAGNOSTIC	22	/* CP0: Diagnostic */
#define C0_BROADCOM	C0_DIAGNOSTIC	/* CP0: Broadcom Register */
#define C0_ECC		26	/* CP0: ECC */
#define C0_CACHEERR	27	/* CP0: CacheErr */
#define C0_TAGLO	28	/* CP0: TagLo */
#define C0_TAGHI	29	/* CP0: TagHi */
#define C0_ERREPC	30	/* CP0: ErrorEPC */

/*
 * Macros to access the system control coprocessor
 */

#define MFC0(source, sel)					\
({								\
	int __res;						\
	__asm__ __volatile__(					\
	".set\tnoreorder\n\t"					\
	".set\tnoat\n\t"					\
	".word\t"STR(0x40010000 | ((source)<<11) | (sel))"\n\t"	\
	"move\t%0,$1\n\t"					\
	".set\tat\n\t"						\
	".set\treorder"						\
	:"=r" (__res)						\
	:							\
	:"$1");							\
	__res;							\
})

#define MTC0(source, sel, value)				\
do {								\
	__asm__ __volatile__(					\
	".set\tnoreorder\n\t"					\
	".set\tnoat\n\t"					\
	"move\t$1,%z0\n\t"					\
	".word\t"STR(0x40810000 | ((source)<<11) | (sel))"\n\t"	\
	".set\tat\n\t"						\
	".set\treorder"						\
	:							\
	:"Jr" (value)						\
	:"$1");							\
} while (0)


/*
 * Cache Operations
 */

#ifndef Fill_I
#define Fill_I			0x14
#endif

#define cache_unroll(base,op)			\
	__asm__ __volatile__("			\
		.set noreorder;			\
		.set mips3;			\
		cache %1, (%0);			\
		.set mips0;			\
		.set reorder"			\
		:				\
		: "r" (base),			\
		  "i" (op));


static const uint32 sbips_bcm56218_irq_id[] = {
	SBIPS_INT_UART0,
	SBIPS_INT_UART1,
	SBIPS_INT_CMIC,
	SBIPS_INT_GPIO,
};

uint sb_irq(void *sbh, uint irq_src_id);

static const uint32 sbips_int_mask[] = {
	0,
	SBIPS_INT1_MASK,
	SBIPS_INT2_MASK,
	SBIPS_INT3_MASK,
	SBIPS_INT4_MASK
};

static const uint32 sbips_int_shift[] = {
	0,
	0,
	SBIPS_INT2_SHIFT,
	SBIPS_INT3_SHIFT,
	SBIPS_INT4_SHIFT
};

/*
 * Returns the MIPS IRQ assignment of the current core. If unassigned,
 * 0 is returned.
 */
uint
sb_irq(void *sbh, uint irq_src_id)
{
	uint idx;
	uint32 flag, sbipsflag = 0;
	uint irq = 0;
	chipcregs_t *cc;

	idx = sb_coreidx(sbh);

	flag = irq_src_id;
	if ((cc = sb_setcore(sbh, SB_CC, 0))) {
		sbipsflag = R_REG(&cc->sbipsflag);
	}

	for (irq = 1; irq <= 4; irq++) {
		if (((sbipsflag & sbips_int_mask[irq]) >> sbips_int_shift[irq])
			== flag)
			break;
	}
	/* No match, return Zer. */
	if (irq == 5)
		irq = 0;

	sb_setcoreidx(sbh, idx);

	return irq;
}

/* Clears the specified MIPS IRQ. */
static void
sb_clearirq(void *sbh, uint irq)
{
	chipcregs_t *cc;
	uint idx;

	idx = sb_coreidx(sbh);
	if ((cc = sb_setcore(sbh, SB_CC, 0))) {
		if (irq == 0)
			W_REG(&cc->sbintvec, 0);
		else
			OR_REG(&cc->sbipsflag, sbips_int_mask[irq]);
	}
	sb_setcoreidx(sbh, idx);
}

/*
 * Assigns the specified MIPS IRQ to the specified core. Shared MIPS
 * IRQ 0 may be assigned more than once.
 */
static void
sb_setirq(void *sbh, uint irq, uint sb_irq_id)
{
	chipcregs_t *cc;
	uint32 flag, idx;

	flag = sb_irq_id;

	idx = sb_coreidx(sbh);
	if ((cc = sb_setcore(sbh, SB_CC, 0))) {
		if (irq == 0)
			OR_REG(&cc->sbintvec, 1 << flag);
		else {
			flag <<= sbips_int_shift[irq];
			ASSERT(!(flag & ~sbips_int_mask[irq]));
			flag |= R_REG(&cc->sbipsflag) & ~sbips_int_mask[irq];
			W_REG(&cc->sbipsflag, flag);

			/* Setup GPIO interrupts */
			if (sb_irq_id == SBIPS_INT_GPIO) {
				W_REG(&cc->gpiocontrol, 0xffffffff);
				W_REG(&cc->gpiointmask, 3);
				W_REG(&cc->gpiointpolarity, 3);
				W_REG(&cc->gpioouten, 3);
			}
		}
	}
	sb_setcoreidx(sbh, idx);
}

/*
 * Initializes clocks and interrupts. SB and NVRAM access must be
 * initialized prior to calling.
 */
void
sb_mips_init(void *sbh)
{
	uint irq, idx;
	chipcregs_t *cc;

	/* Chip specific initialization */
	switch (sb_chip(sbh)) {
	case BCM56218_DEVICE_ID:
		/* reset uart */
		idx = sb_coreidx(sbh);
		if ((cc = sb_setcore(sbh, SB_CC, 0))) {
		//	W_REG(&cc->peripheralreset, 1);
		//	W_REG(&cc->peripheralreset, 3);
		}

		/* Clear interrupt map */
		for (irq = 0; irq <= 4; irq++)
			sb_clearirq(sbh, irq);

		sb_setirq(sbh, 1, SBIPS_INT_UART0);
		sb_setirq(sbh, 2, SBIPS_INT_UART1);
		sb_setirq(sbh, 3, SBIPS_INT_CMIC);
		sb_setirq(sbh, 4, SBIPS_INT_GPIO);

		sb_setcoreidx(sbh, idx);
		break;
	}
}

uint32
sb_mips_clock(void *sbh)
{
	uint idx;
	uint32 rate, ratio;
	chipcregs_t *cc;

	rate = sb_clock(sbh);
	idx = sb_coreidx(sbh);

	if ((cc = (chipcregs_t *) sb_setcore(sbh, SB_CC, 0))) {
		ratio = (R_REG(&cc->capabilities) >> 4) & 3;

		switch (ratio) {
		case 0:
			rate = rate*2;
			break;
		case 1:
			rate = (rate*9)/4;
			break;
		case 2:
			if ((R_REG(&cc->chipid) & 0x00f0) == 0x10) {
				rate = (rate*6)/4;
			} else {
				rate = (rate*10)/4;
			}
			break;
		case 3:
			break;
		}
	}

	sb_setcoreidx(sbh, idx);

	return rate;
}

bool
sb_mips_setclock(void *sbh, uint32 mipsclock, uint32 sbclock, uint32 pciclock)
{
	return FALSE;
}


/* returns the ncdl value to be programmed into sdram_ncdl for calibration */
uint32
sb_memc_get_ncdl(void *sbh)
{
	sbmemcregs_t *memc;
	uint32 ret = 0;
	uint32 config, rd, wr, misc, dqsg, cd, sm, sd;
	uint idx;

	idx = sb_coreidx(sbh);

	memc = (sbmemcregs_t *)sb_setcore(sbh, SB_MEMC, 0);
	if (memc == 0)
		goto out;

	config = R_REG(&memc->config);
	wr = R_REG(&memc->wrncdlcor);
	rd = R_REG(&memc->rdncdlcor);
	misc = R_REG(&memc->miscdlyctl);
	dqsg = R_REG(&memc->dqsgatencdl);

	rd &= MEMC_RDNCDLCOR_RD_MASK;
	wr &= MEMC_WRNCDLCOR_WR_MASK;
	dqsg &= MEMC_DQSGATENCDL_G_MASK;

	if (config & MEMC_CONFIG_DDR) {
		ret = (wr << 16) | (rd << 8) | dqsg;
	} else {
		cd = (rd == MEMC_CD_THRESHOLD) ? rd : (wr + MEMC_CD_THRESHOLD);
		sm = (misc & MEMC_MISC_SM_MASK) >> MEMC_MISC_SM_SHIFT;
		sd = (misc & MEMC_MISC_SD_MASK) >> MEMC_MISC_SD_SHIFT;
		ret = (sm << 16) | (sd << 8) | cd;
	}

out:
	/* switch back to previous core */
	sb_setcoreidx(sbh, idx);

	return ret;
}


