/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2008 Wind River Systems, Inc.
 *
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/serial_8250.h>

#define BCM_UART_FLAGS (UPF_BOOT_AUTOCONF | UPF_SKIP_TEST | UPF_IOREMAP)

extern void *bcm956218_sbh;
extern unsigned int sb_clock(void *bcm956218_sbh);

static struct plat_serial8250_port uart8250_data[] = {
	{
		.membase	= (void __iomem *)0xb8000300,
		.mapbase	= 0x18000300,
		.irq		= MIPS_CPU_IRQ_BASE + 0,
		.uartclk	= 133000000,
		.iotype		= UPIO_MEM,
		.flags		= BCM_UART_FLAGS,
		.regshift	= 0,
	},
	{
		.membase	= (void __iomem *)0xb8000400,
		.mapbase	= 0x18000400,
		.irq		= MIPS_CPU_IRQ_BASE + 4,
		.uartclk	= 133000000,
		.iotype		= UPIO_MEM,
		.flags		= BCM_UART_FLAGS,
		.regshift	= 0,
	},
	{ },
};

static struct platform_device uart8250_device = {
	.name			= "serial8250",
	.id			= PLAT8250_DEV_PLATFORM,
	.dev			= {
		.platform_data	= uart8250_data,
	},
};

static int __init uart8250_init(void)
{
        uart8250_data[0].uartclk = sb_clock(bcm956218_sbh);
        uart8250_data[1].uartclk = sb_clock(bcm956218_sbh);
	return platform_device_register(&uart8250_device);
}

module_init(uart8250_init);

