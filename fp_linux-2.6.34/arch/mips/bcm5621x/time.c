/*
 * Copyright (C) 2004 Broadcom Corporation
 * Copyright (C) 2008 Wind River Systems, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/serial_reg.h>
#include <linux/interrupt.h>
#include <asm/addrspace.h>
#include <asm/io.h>
#include <asm/time.h>

#include <asm/mach-bcm5621x/typedefs.h>
#include <asm/mach-bcm5621x/sbutils.h>
#include <asm/mach-bcm5621x/sbmips.h>

extern void *bcm956218_sbh;
extern spinlock_t bcm956218_sbh_lock;

/* Convenience */
#define sbh bcm956218_sbh
#define sbh_lock bcm956218_sbh_lock

extern int panic_timeout;
static int watchdog = 0;

void __init plat_time_init(void)
{
	unsigned int hz;

	/*
	 * Use deterministic values for initial counter interrupt
	 * so that calibrate delay avoids encountering a counter wrap.
	 */
	write_c0_count(0);
	write_c0_compare(0xffff);

	if (!(hz = sb_mips_clock(sbh)))
		hz = 100000000;

	printk("CPU: BCM%05d rev %d at %d MHz\n", sb_chip(sbh),
	       sb_chiprev(sbh), (hz + 500000) / 1000000);

	/* Set MIPS counter frequency for fixed_rate_gettimeoffset() */
	mips_hpt_frequency = hz / 2;

	/* Set watchdog interval in ms */
	watchdog = 0xffffffff;

	/* Set panic timeout in seconds */
	panic_timeout = watchdog / 1000;
}
