#include <linux/module.h>
#include <linux/highmem.h>
#include <linux/sched.h>
#include <linux/smp.h>
#include <asm/fixmap.h>
#include <asm/tlbflush.h>

static pte_t *kmap_pte;

unsigned long highstart_pfn, highend_pfn;

void *__kmap(struct page *page)
{
	void *addr;

	might_sleep();
	if (!PageHighMem(page))
		return page_address(page);
	addr = kmap_high(page);
	flush_tlb_one((unsigned long)addr);

	return addr;
}
EXPORT_SYMBOL(__kmap);

void __kunmap(struct page *page)
{
	BUG_ON(in_interrupt());
	if (!PageHighMem(page))
		return;
	kunmap_high(page);
}
EXPORT_SYMBOL(__kunmap);

/*
 * kmap_atomic/kunmap_atomic is significantly faster than kmap/kunmap because
 * no global lock is needed and because the kmap code must perform a global TLB
 * invalidation when the kmap pool wraps.
 *
 * However when holding an atomic kmap is is not legal to sleep, so atomic
 * kmaps are appropriate for short, tight code paths only.
 */

#ifdef CONFIG_BCM53000_HIGHMEM
/*
 * need an array per cpu, and each array has to be cache aligned
 */
struct kmap_map {
  struct page *page;
  void        *vaddr;
};

struct {
  struct kmap_map map[KM_TYPE_NR];
} ____cacheline_aligned_in_smp kmap_atomic_maps[NR_CPUS];

void *
kmap_atomic_page_address(struct page *page)
{
  int i;

        for (i = 0; i < KM_TYPE_NR; i++)
                if (kmap_atomic_maps[smp_processor_id()].map[i].page == page)
                        return kmap_atomic_maps[smp_processor_id()].map[i].vaddr;

        return (struct page *)0;
}
#endif
void *__kmap_atomic(struct page *page, enum km_type type)
{
#ifdef CONFIG_BCM53000_HIGHMEM
    unsigned int idx;
#else
	enum fixed_addresses idx;
#endif
	unsigned long vaddr;

	/* even !CONFIG_PREEMPT needs this, for in_atomic in do_page_fault */
	pagefault_disable();
	if (!PageHighMem(page))
		return page_address(page);

	debug_kmap_atomic(type);
	idx = type + KM_TYPE_NR*smp_processor_id();
#ifdef CONFIG_BCM53000_HIGHMEM
        vaddr = fix_to_virt_noalias(VALIAS_IDX(FIX_KMAP_BEGIN + idx), page_to_pfn(page));
#ifdef CONFIG_DEBUG_HIGHMEM
        if (!pte_none(*(kmap_pte-(virt_to_fix(vaddr) - VALIAS_IDX(FIX_KMAP_BEGIN)))))
                BUG();
#endif
        set_pte(kmap_pte-(virt_to_fix(vaddr) - VALIAS_IDX(FIX_KMAP_BEGIN)), \
                 mk_pte(page, kmap_prot));
#else
	vaddr = __fix_to_virt(FIX_KMAP_BEGIN + idx);
#ifdef CONFIG_DEBUG_HIGHMEM
	BUG_ON(!pte_none(*(kmap_pte - idx)));
#endif
	set_pte(kmap_pte-idx, mk_pte(page, PAGE_KERNEL));
#endif
	local_flush_tlb_one((unsigned long)vaddr);

#ifdef CONFIG_BCM53000_HIGHMEM
        kmap_atomic_maps[smp_processor_id()].map[type].page = page;
        kmap_atomic_maps[smp_processor_id()].map[type].vaddr = (void *)vaddr;
#endif
	return (void*) vaddr;
}
EXPORT_SYMBOL(__kmap_atomic);

void __kunmap_atomic(void *kvaddr, enum km_type type)
{
#ifdef CONFIG_BCM53000_HIGHMEM
    unsigned long vaddr = (unsigned long) kvaddr & PAGE_MASK;
    unsigned int idx = type + KM_TYPE_NR*smp_processor_id();
    struct page *page = kmap_atomic_maps[smp_processor_id()].map[type].page;

        if (vaddr < FIXADDR_START) {
                pagefault_enable();
                return;
        }

        if (vaddr != fix_to_virt_noalias(VALIAS_IDX(FIX_KMAP_BEGIN + idx), page_to_pfn(page)))
                BUG();
        /*
         * Protect against multiple unmaps
         * Can't cache flush an unmapped page.
         */
        if (kmap_atomic_maps[smp_processor_id()].map[type].vaddr) {
                kmap_atomic_maps[smp_processor_id()].map[type].page = (struct page *)0;
                kmap_atomic_maps[smp_processor_id()].map[type].vaddr = (void *) 0;

                flush_data_cache_page((unsigned long)vaddr);
        }
#ifdef CONFIG_DEBUG_HIGHMEM
        /*
         * force other mappings to Oops if they'll try to access
         * this pte without first remap it
         */
    pte_clear(&init_mm, vaddr, kmap_pte-(virt_to_fix(vaddr) - VALIAS_IDX(FIX_KMAP_BEGIN)));
    local_flush_tlb_one(vaddr);
#endif
#else
#ifdef CONFIG_DEBUG_HIGHMEM
	unsigned long vaddr = (unsigned long) kvaddr & PAGE_MASK;
	enum fixed_addresses idx = type + KM_TYPE_NR*smp_processor_id();

	if (vaddr < FIXADDR_START) { // FIXME
		pagefault_enable();
		return;
	}

	BUG_ON(vaddr != __fix_to_virt(FIX_KMAP_BEGIN + idx));

	/*
	 * force other mappings to Oops if they'll try to access
	 * this pte without first remap it
	 */
	pte_clear(&init_mm, vaddr, kmap_pte-idx);
	local_flush_tlb_one(vaddr);
#endif
#endif

	pagefault_enable();
}
EXPORT_SYMBOL(__kunmap_atomic);

/*
 * This is the same as kmap_atomic() but can map memory that doesn't
 * have a struct page associated with it.
 */
void *kmap_atomic_pfn(unsigned long pfn, enum km_type type)
{
#ifdef CONFIG_BCM53000_HIGHMEM
    unsigned int idx;
#else
	enum fixed_addresses idx;
#endif
	unsigned long vaddr;

	pagefault_disable();

	debug_kmap_atomic(type);
	idx = type + KM_TYPE_NR*smp_processor_id();
#ifdef CONFIG_BCM53000_HIGHMEM
        vaddr = fix_to_virt_noalias(VALIAS_IDX(FIX_KMAP_BEGIN + idx), pfn);

        set_pte(kmap_pte-(virt_to_fix(vaddr) - VALIAS_IDX(FIX_KMAP_BEGIN)), \
                 pfn_pte(pfn, kmap_prot));
#else
	vaddr = __fix_to_virt(FIX_KMAP_BEGIN + idx);
	set_pte(kmap_pte-idx, pfn_pte(pfn, PAGE_KERNEL));
#endif
	flush_tlb_one(vaddr);

	return (void*) vaddr;
}

struct page *__kmap_atomic_to_page(void *ptr)
{
#ifdef CONFIG_BCM53000_HIGHMEM
        unsigned long vaddr = (unsigned long)ptr;
#else
	unsigned long idx, vaddr = (unsigned long)ptr;
#endif
	pte_t *pte;

	if (vaddr < FIXADDR_START)
		return virt_to_page(ptr);

#ifdef CONFIG_BCM53000_HIGHMEM
        pte = kmap_pte - (virt_to_fix(vaddr) - VALIAS_IDX(FIX_KMAP_BEGIN));
#else
	idx = virt_to_fix(vaddr);
	pte = kmap_pte - (idx - FIX_KMAP_BEGIN);
#endif
	return pte_page(*pte);
}

void __init kmap_init(void)
{
	unsigned long kmap_vstart;

	/* cache the first kmap pte */
#ifdef CONFIG_BCM53000_HIGHMEM
        kmap_vstart = __fix_to_virt(VALIAS_IDX(FIX_KMAP_BEGIN));
#else
	kmap_vstart = __fix_to_virt(FIX_KMAP_BEGIN);
#endif
	kmap_pte = kmap_get_fixmap_pte(kmap_vstart);
}
