/*
 * MPC85xx SPI initialization.
 *
 * Copyright 2009 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */
#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/fsl_devices.h>

#include <sysdev/fsl_soc.h>

static int __init of_fsl_spi_probe(char *type, char *compatible, u32 sysclk,
				   struct spi_board_info *board_infos,
				   unsigned int num_board_infos,
				   void (*cs_control)(struct spi_device *spi,
						      bool on))
{
	struct device_node *np;
	unsigned int i = 0;
	char spi_name[32] = "mpc83xx_spi";

	np = of_find_compatible_node(NULL, NULL, "fsl,espi");
	if (np != NULL) {
		strcpy(spi_name, "fsl_espi");
		of_node_put(np);
	}

	for_each_compatible_node(np, type, compatible) {
		int ret;
		unsigned int j;
		const void *prop;
		struct resource res[2];
		struct platform_device *pdev;
		struct fsl_spi_platform_data pdata = {
			.cs_control = cs_control,
		};

		memset(res, 0, sizeof(res));

		pdata.sysclk = sysclk;

		prop = of_get_property(np, "reg", NULL);
		if (!prop)
			goto err;
		pdata.bus_num = *(u32 *)prop;

		prop = of_get_property(np, "cell-index", NULL);
		if (prop)
			i = *(u32 *)prop;

#if 0
		prop = of_get_property(np, "mode", NULL);
		if (prop && !strcmp(prop, "cpu-qe"))
			pdata.qe_mode = 1;
#endif

		for (j = 0; j < num_board_infos; j++) {
			if (board_infos[j].bus_num == pdata.bus_num)
				pdata.max_chipselect++;
		}

		if (!pdata.max_chipselect)
			continue;

		ret = of_address_to_resource(np, 0, &res[0]);
		if (ret)
			goto err;

		ret = of_irq_to_resource(np, 0, &res[1]);
		if (ret == NO_IRQ)
			goto err;

		pdev = platform_device_alloc(spi_name, i);
		if (!pdev)
			goto err;

		ret = platform_device_add_data(pdev, &pdata, sizeof(pdata));
		if (ret)
			goto unreg;

		ret = platform_device_add_resources(pdev, res,
						    ARRAY_SIZE(res));
		if (ret)
			goto unreg;

		ret = platform_device_add(pdev);
		if (ret)
			goto unreg;

		goto next;
unreg:
		platform_device_del(pdev);
err:
		pr_err("%s: registration failed\n", np->full_name);
next:
		i++;
	}

	return i;
}


int __init fsl_spi_init(struct spi_board_info *board_infos,
			unsigned int num_board_infos,
			void (*cs_control)(struct spi_device *spi,
					   bool on))
{
	u32 sysclk = -1;
	int ret;

#ifdef CONFIG_QUICC_ENGINE
	/* SPI controller is either clocked from QE or SoC clock */
	sysclk = get_brgfreq();
#endif
	if (sysclk == -1) {
		sysclk = fsl_get_sys_freq();
		if (sysclk == -1)
			return -ENODEV;
	}

	ret = of_fsl_spi_probe(NULL, "fsl,spi", sysclk, board_infos,
			       num_board_infos, cs_control);
	if (!ret)
		of_fsl_spi_probe("spi", "fsl_spi", sysclk, board_infos,
				 num_board_infos, cs_control);
	if (!ret)
		of_fsl_spi_probe(NULL, "fsl,espi", sysclk, board_infos,
				 num_board_infos, cs_control);

	return spi_register_board_info(board_infos, num_board_infos);
}


#if defined(CONFIG_FSL_ESPI) || defined(CONFIG_FSL_ESPI_MODULE)
static int __init mpc85xx_spi_init(void)
{
	struct device_node *np, *dp = NULL;
	struct mtd_partition *parts;
	struct flash_platform_data *spi_eeprom_pdata;
	struct spi_board_info *mpc85xx_spi_bdinfo;
	const u32 *iprop;
	char *sprop;
	int i, nr_parts, bd_num = 0, n = -1;

	np = of_find_compatible_node(NULL, NULL, "fsl,mpc8536-espi");

	if (!np)
		return 0;

	while ((dp = of_get_next_child(np, dp)))
		bd_num++;
	of_node_put(np);

	mpc85xx_spi_bdinfo =
		kzalloc(bd_num * sizeof(*mpc85xx_spi_bdinfo), GFP_KERNEL);
	if (mpc85xx_spi_bdinfo == NULL){
		printk(KERN_ERR "failed to allocate spi board info\n");
		return 0;
	}

	for_each_compatible_node(np, NULL, "fsl,espi-flash") {
		n++;
		iprop = of_get_property(np, "reg", NULL);
		(mpc85xx_spi_bdinfo + n)->chip_select = *iprop;
		iprop = of_get_property(np, "spi-max-frequency", NULL);
		(mpc85xx_spi_bdinfo + n)->max_speed_hz = *iprop;
		/* use parent's bus_num as its own bus_num */
		dp = of_get_parent(np);
		iprop = of_get_property(dp, "reg", NULL);
		(mpc85xx_spi_bdinfo + n)->bus_num = *iprop;
		/* Mode (clock phase/polarity/etc.) */
		if (of_find_property(np, "spi,cpha", NULL))
			(mpc85xx_spi_bdinfo + n)->mode |= SPI_CPHA;
		if (of_find_property(np, "spi,cpol", NULL))
			(mpc85xx_spi_bdinfo + n)->mode |= SPI_CPOL;
		/* Select device driver */
		sprop = (char *)of_get_property(np, "linux,modalias", NULL);
		if (sprop)
			strncpy((mpc85xx_spi_bdinfo + n)->modalias,
					sprop, sizeof((mpc85xx_spi_bdinfo + n)->modalias) - 1);
		else
			strncpy((mpc85xx_spi_bdinfo + n)->modalias,
					"spidev", sizeof((mpc85xx_spi_bdinfo + n)->modalias) - 1);

		spi_eeprom_pdata =
			kzalloc(sizeof(*spi_eeprom_pdata), GFP_KERNEL);
		if (spi_eeprom_pdata == NULL)
			continue;
		spi_eeprom_pdata->name = kzalloc(10, GFP_KERNEL);
		if (spi_eeprom_pdata->name == NULL) {
			kfree(spi_eeprom_pdata);
			continue;
		}

		(mpc85xx_spi_bdinfo + n)->platform_data = spi_eeprom_pdata;
		snprintf(spi_eeprom_pdata->name, 10, "SPIFLASH%d", n);

		nr_parts = 0;
		dp = NULL;
		while ((dp = of_get_next_child(np, dp)))
			nr_parts++;
		if (nr_parts == 0)
			continue;
		parts = kzalloc(nr_parts * sizeof(*parts), GFP_KERNEL);
		if (!parts)
			continue;

		i = 0;
		while ((dp = of_get_next_child(np, dp))) {
			const u32 *reg;
			const char *partname;
			int len;

			reg = of_get_property(dp, "reg", &len);
			if (!reg || (len != 2 * sizeof(u32))) {
				of_node_put(dp);
				kfree(parts);
				parts = NULL;
				break;
			}
			(parts + i)->offset = reg[0];
			(parts + i)->size = reg[1];

			partname = of_get_property(dp, "label", &len);
			if (!partname)
				partname = dp->name;
			(parts + i)->name = (char *)partname;

			if (of_get_property(dp, "read-only", &len))
				(parts + i)->mask_flags = MTD_WRITEABLE;

			i++;
		}
		spi_eeprom_pdata->parts = parts;
		spi_eeprom_pdata->nr_parts = nr_parts;
	}
	for_each_compatible_node(np, NULL, "fsl,espi-fpga") {
		n++;
		iprop = of_get_property(np, "reg", NULL);
		(mpc85xx_spi_bdinfo + n)->chip_select = *iprop;
		iprop = of_get_property(np, "spi-max-frequency", NULL);
		(mpc85xx_spi_bdinfo + n)->max_speed_hz = *iprop;
		/* use parent's bus_num as its own bus_num */
		dp = of_get_parent(np);
		iprop = of_get_property(dp, "reg", NULL);
		(mpc85xx_spi_bdinfo + n)->bus_num = *iprop;
		/* Mode (clock phase/polarity/etc.) */
		if (of_find_property(np, "spi,cpha", NULL))
			(mpc85xx_spi_bdinfo + n)->mode |= SPI_CPHA;
		if (of_find_property(np, "spi,cpol", NULL))
			(mpc85xx_spi_bdinfo + n)->mode |= SPI_CPOL;
		/* Select device driver */
		sprop = (char *)of_get_property(np, "linux,modalias", NULL);
		if (sprop)
			strncpy((mpc85xx_spi_bdinfo + n)->modalias,
					sprop, sizeof((mpc85xx_spi_bdinfo + n)->modalias) - 1);
		else
			strncpy((mpc85xx_spi_bdinfo + n)->modalias,
					"spidev", sizeof((mpc85xx_spi_bdinfo + n)->modalias) - 1);

		spi_eeprom_pdata =
			kzalloc(sizeof(*spi_eeprom_pdata), GFP_KERNEL);
		if (spi_eeprom_pdata == NULL)
			continue;
		spi_eeprom_pdata->name = kzalloc(10, GFP_KERNEL);
		if (spi_eeprom_pdata->name == NULL) {
			kfree(spi_eeprom_pdata);
			continue;
		}

		(mpc85xx_spi_bdinfo + n)->platform_data = spi_eeprom_pdata;
		snprintf(spi_eeprom_pdata->name, 10, "SPIFPGA%d", n);

		nr_parts = 0;
		dp = NULL;
		while ((dp = of_get_next_child(np, dp)))
			nr_parts++;
		if (nr_parts == 0)
			continue;
		parts = kzalloc(nr_parts * sizeof(*parts), GFP_KERNEL);
		if (!parts)
			continue;

		i = 0;
		while ((dp = of_get_next_child(np, dp))) {
			const u32 *reg;
			const char *partname;
			int len;

			reg = of_get_property(dp, "reg", &len);
			if (!reg || (len != 2 * sizeof(u32))) {
				of_node_put(dp);
				kfree(parts);
				parts = NULL;
				break;
			}
			(parts + i)->offset = reg[0];
			(parts + i)->size = reg[1];

			partname = of_get_property(dp, "label", &len);
			if (!partname)
				partname = dp->name;
			(parts + i)->name = (char *)partname;

			if (of_get_property(dp, "read-only", &len))
				(parts + i)->mask_flags = MTD_WRITEABLE;

			i++;
		}
		spi_eeprom_pdata->parts = parts;
		spi_eeprom_pdata->nr_parts = nr_parts;
	}

	fsl_spi_init(mpc85xx_spi_bdinfo, bd_num, NULL);

	/*
	 * Only free "struct spi_board_info", because it will be discarded
	 * after registered, but other info are still reserved.
	 */
	kfree(mpc85xx_spi_bdinfo);
	return 0;
}

device_initcall(mpc85xx_spi_init);
#endif /* CONFIG_FSL_ESPI || CONFIG_FSL_ESPI_MODULE */

