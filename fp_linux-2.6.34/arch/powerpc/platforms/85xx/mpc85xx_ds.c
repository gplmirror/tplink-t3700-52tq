/*
 * MPC85xx DS Board Setup
 *
 * Author Xianghua Xiao (x.xiao@freescale.com)
 * Roy Zang <tie-fei.zang@freescale.com>
 * 	- Add PCI/PCI Exprees support
 * Copyright 2007 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>
#include <linux/lmb.h>

#include <asm/system.h>
#include <asm/time.h>
#include <asm/machdep.h>
#include <asm/pci-bridge.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/i8259.h>
#include <asm/swiotlb.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>
#ifdef CONFIG_FXC_P2020
#include <linux/poll.h>
#include <linux/io.h>
#include <linux/delay.h>
#endif

#undef DEBUG

#ifdef DEBUG
#define DBG(fmt, args...) printk(KERN_ERR "%s: " fmt, __func__, ## args)
#else
#define DBG(fmt, args...)
#endif
#ifdef CONFIG_LVL7_BROADCOM_WDOG

#ifdef CONFIG_FXC_P2020

#define WATCHDOG_PULSE_WIDTH                  (5) /* 5 ms */
#define WATCHDOG_TIMER_INTERVAL               500 /* 500 ms */
#define CPU_P2020_GPIO_GPDIR_REG_OFFSET   (0xc00)
#define CPU_P2020_GPIO_GPDAT_REG_OFFSET   (0xc08)
#define WATCHDOG_ENABLE_GPIO_PIN             (12)
#define WATCHDOG_SERVICE_GPIO_PIN            (11)
static phys_addr_t *mem=0;
extern phys_addr_t get_immrbase(void);
#endif /* CONFIG_FXC_P2020*/
#ifdef CONFIG_DNI_P2020
#define WATCHDOG_TIMER_INTERVAL               1000 /* 1000 ms */
#endif /* CONFIG_DNI_P2020 */
#endif /* CONFIG_LVL7_BROADCOM_WDOG*/


#if defined(CONFIG_RTC_CLASS) && defined(CONFIG_RTC_DRV_DS1307)
static int __init mpc85xx_ds1307_rtc_init(void)
{
	struct device_node *np = NULL;
	struct resource r[2];
	int res_num = 0;
	int ret;
	struct platform_device *rtc_dev = NULL;

	memset(r, 0, sizeof(struct resource)*2);

	np = of_find_node_by_name(np, "rtc");
	if (NULL == np){
		printk(KERN_ERR "Could not find rtc-ds1307 node\n");	
		return -1;
	}

	ret = of_address_to_resource(np, 0, &r[0]);
	if (ret){
		printk(KERN_INFO "Could not get reg resource\n");
		return -1; 
	}else
		res_num++;

	ret = of_irq_to_resource(np, 0, &r[1]);
	if (ret <= 0)
		printk(KERN_ERR "Could not get irq resource\n");
	else
		res_num++; /* The IRQ# may not be provided in dts */

	rtc_dev = platform_device_register_simple("ds1307", 0, r, res_num); /* name MUST be "ds1307" */
	if (IS_ERR(rtc_dev)) {
		ret = PTR_ERR(rtc_dev);
		printk("mpc85xx_ds1307_rtc_init:rtc creation failure \n");       
             goto err;       
         }
 	 else
	     printk("mpc85xx_ds1307_rtc_init:rtc creation success \n");       

     return 0;       
 err:       
     return 0; /* we are not returning error here as this file is common to all platforms */ 		
}
arch_initcall(mpc85xx_ds1307_rtc_init);
#endif /* CONFIG_RTC_CLASS && CONFIG_RTC_DRV_DS1307 */

#ifdef CONFIG_PPC_I8259
static void mpc85xx_8259_cascade(unsigned int irq, struct irq_desc *desc)
{
	unsigned int cascade_irq = i8259_irq();

	if (cascade_irq != NO_IRQ) {
		generic_handle_irq(cascade_irq);
	}
	desc->chip->eoi(irq);
}
#endif	/* CONFIG_PPC_I8259 */

void __init mpc85xx_ds_pic_init(void)
{
	struct mpic *mpic;
	struct resource r;
	struct device_node *np;
#ifdef CONFIG_PPC_I8259
	struct device_node *cascade_node = NULL;
	int cascade_irq;
#endif
	unsigned long root = of_get_flat_dt_root();

	np = of_find_node_by_type(NULL, "open-pic");
	if (np == NULL) {
		printk(KERN_ERR "Could not find open-pic node\n");
		return;
	}

	if (of_address_to_resource(np, 0, &r)) {
		printk(KERN_ERR "Failed to map mpic register space\n");
		of_node_put(np);
		return;
	}

	if (of_flat_dt_is_compatible(root, "fsl,MPC8572DS-CAMP")) {
		mpic = mpic_alloc(np, r.start,
			MPIC_PRIMARY |
			MPIC_BIG_ENDIAN | MPIC_BROKEN_FRR_NIRQS,
			0, 256, " OpenPIC  ");
	} else {
		mpic = mpic_alloc(np, r.start,
			  MPIC_PRIMARY | MPIC_WANTS_RESET |
			  MPIC_BIG_ENDIAN | MPIC_BROKEN_FRR_NIRQS |
			  MPIC_SINGLE_DEST_CPU,
			0, 256, " OpenPIC  ");
	}

	BUG_ON(mpic == NULL);
	of_node_put(np);

	mpic_init(mpic);

#ifdef CONFIG_PPC_I8259
	/* Initialize the i8259 controller */
	for_each_node_by_type(np, "interrupt-controller")
	    if (of_device_is_compatible(np, "chrp,iic")) {
		cascade_node = np;
		break;
	}

	if (cascade_node == NULL) {
		printk(KERN_DEBUG "Could not find i8259 PIC\n");
		return;
	}

	cascade_irq = irq_of_parse_and_map(cascade_node, 0);
	if (cascade_irq == NO_IRQ) {
		printk(KERN_ERR "Failed to map cascade interrupt\n");
		return;
	}

	DBG("mpc85xxds: cascade mapped to irq %d\n", cascade_irq);

	i8259_init(cascade_node, 0);
	of_node_put(cascade_node);

	set_irq_chained_handler(cascade_irq, mpc85xx_8259_cascade);
#endif	/* CONFIG_PPC_I8259 */
}

#ifdef CONFIG_PCI
static int primary_phb_addr;
extern int uli_exclude_device(struct pci_controller *hose,
				u_char bus, u_char devfn);

static int mpc85xx_exclude_device(struct pci_controller *hose,
				   u_char bus, u_char devfn)
{
	struct device_node* node;
	struct resource rsrc;

	node = hose->dn;
	of_address_to_resource(node, 0, &rsrc);

	if ((rsrc.start & 0xfffff) == primary_phb_addr) {
		return uli_exclude_device(hose, bus, devfn);
	}

	return PCIBIOS_SUCCESSFUL;
}
#endif	/* CONFIG_PCI */

/*
 * Setup the architecture
 */
#ifdef CONFIG_SMP
extern void __init mpc85xx_smp_init(void);
#endif
static void __init mpc85xx_ds_setup_arch(void)
{
#ifdef CONFIG_PCI
	struct device_node *np;
	struct pci_controller *hose;
#endif
	dma_addr_t max = 0xffffffff;

	if (ppc_md.progress)
		ppc_md.progress("mpc85xx_ds_setup_arch()", 0);

#ifdef CONFIG_PCI
	for_each_node_by_type(np, "pci") {
		if (of_device_is_compatible(np, "fsl,mpc8540-pci") ||
		    of_device_is_compatible(np, "fsl,mpc8548-pcie") ||
		    of_device_is_compatible(np, "fsl,p2020-pcie")) {
			struct resource rsrc;
			of_address_to_resource(np, 0, &rsrc);
			if ((rsrc.start & 0xfffff) == primary_phb_addr)
				fsl_add_bridge(np, 1);
			else
				fsl_add_bridge(np, 0);

			hose = pci_find_hose_for_OF_device(np);
			max = min(max, hose->dma_window_base_cur +
					hose->dma_window_size);
		}
	}

	ppc_md.pci_exclude_device = mpc85xx_exclude_device;
#endif

#ifdef CONFIG_SMP
	mpc85xx_smp_init();
#endif

#ifdef CONFIG_SWIOTLB
	if (lmb_end_of_DRAM() > max) {
		ppc_swiotlb_enable = 1;
		set_pci_dma_ops(&swiotlb_dma_ops);
		ppc_md.pci_dma_dev_setup = pci_dma_dev_setup_swiotlb;
	}
#endif

	/* Suppress console message during boot to shorten 
	** boot time.
	*/
	console_loglevel = 4;

	printk("MPC85xx DS board from Freescale Semiconductor\n");
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init mpc8544_ds_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "MPC8544DS")) {
#ifdef CONFIG_PCI
		primary_phb_addr = 0xb000;
#endif
		return 1;
	}

	return 0;
}

static struct of_device_id __initdata mpc85xxds_ids[] = {
	{ .type = "soc", },
	{ .compatible = "soc", },
	{ .compatible = "simple-bus", },
	{ .compatible = "gianfar", },
	{},
};

static int __init mpc85xxds_publish_devices(void)
{
	return of_platform_bus_probe(NULL, mpc85xxds_ids, NULL);
}
machine_device_initcall(mpc8544_ds, mpc85xxds_publish_devices);
machine_device_initcall(mpc8572_ds, mpc85xxds_publish_devices);
machine_device_initcall(p2020_ds, mpc85xxds_publish_devices);
machine_arch_initcall(p2020_qc, mpc85xxds_publish_devices);
machine_device_initcall(p2020_dni, mpc85xxds_publish_devices);
machine_device_initcall(p2020_fxc, mpc85xxds_publish_devices);
machine_device_initcall(p2020_had, mpc85xxds_publish_devices);


machine_arch_initcall(mpc8544_ds, swiotlb_setup_bus_notifier);
machine_arch_initcall(mpc8572_ds, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_ds, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_dni, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_had, swiotlb_setup_bus_notifier);

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init mpc8572_ds_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,MPC8572DS")) {
#ifdef CONFIG_PCI
		primary_phb_addr = 0x8000;
#endif
		return 1;
	}

	return 0;
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p2020_ds_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P2020DS")) {
#ifdef CONFIG_PCI
		primary_phb_addr = 0x9000;
#endif
		return 1;
	}

	return 0;
}

define_machine(mpc8544_ds) {
	.name			= "MPC8544 DS",
	.probe			= mpc8544_ds_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

define_machine(mpc8572_ds) {
	.name			= "MPC8572 DS",
	.probe			= mpc8572_ds_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

define_machine(p2020_ds) {
	.name			= "P2020 DS",
	.probe			= p2020_ds_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};


#ifdef CONFIG_DNI_P2020 
#define CPLD_ADDRESS  0xffdf0000
static unsigned char *cpldPtr = 0;
static int __init p2020_cpld_init (void)
{
  cpldPtr = ioremap(CPLD_ADDRESS, 0x8000);
  return 0;
}
late_initcall(p2020_cpld_init);

void p2020_dni_restart(char *cmd)
{
	if (cpldPtr) {
        	local_irq_disable();
		out_8(cpldPtr+0x0020, 0x7f);
	} else {
		printk (KERN_EMERG "Error: reset control register not mapped."
				"Please power cycle board manually!\n");
	}

        while (1) ;
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p2020_dni_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P2020DNI")) {
#ifdef CONFIG_PCI
		primary_phb_addr = 0x9000;
#endif
		return 1;
	}

	return 0;
}
define_machine(p2020_dni) {
       .name                   = "P2020 DNI",
       .probe                  = p2020_dni_probe,
       .setup_arch             = mpc85xx_ds_setup_arch,
       .init_IRQ               = mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
       .pcibios_fixup_bus      = fsl_pcibios_fixup_bus,
#endif
       .get_irq                = mpic_get_irq,
       .restart                = p2020_dni_restart,
       .calibrate_decr         = generic_calibrate_decr,
       .progress               = udbg_progress,
};
#endif
#ifdef CONFIG_FXC_P2020
/*
 * Called very early, device-tree isn't unflattened
*/
static int __init p2020_fxc_probe(void)
{
      unsigned long root = of_get_flat_dt_root();

      if (of_flat_dt_is_compatible(root, "fsl,P2020FXC")) {
#ifdef CONFIG_PCI
              primary_phb_addr = 0x9000;
#endif
              return 1;
      }

      return 0;
}
define_machine(p2020_fxc) {
       .name                   = "P2020 FXC",
       .probe                  = p2020_fxc_probe,
       .setup_arch             = mpc85xx_ds_setup_arch,
       .init_IRQ               = mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
       .pcibios_fixup_bus      = fsl_pcibios_fixup_bus,
#endif
       .get_irq                = mpic_get_irq,
       .restart                = fsl_rstcr_restart,
       .calibrate_decr         = generic_calibrate_decr,
       .progress               = udbg_progress,
};
#endif

#ifdef CONFIG_QC_P2020
/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p2020_qc_probe(void)
{
        unsigned long root = of_get_flat_dt_root();

        if (of_flat_dt_is_compatible(root, "fsl,P2020QC")) {
#ifdef CONFIG_PCI
                primary_phb_addr = 0x9000;
#endif
                return 1;
        }

        return 0;
}
define_machine(p2020_qc) {
       .name                   = "P2020 QC",
       .probe                  = p2020_qc_probe,
       .setup_arch             = mpc85xx_ds_setup_arch,
       .init_IRQ               = mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
       .pcibios_fixup_bus      = fsl_pcibios_fixup_bus,
#endif
       .get_irq                = mpic_get_irq,
       .restart                = fsl_rstcr_restart,
       .calibrate_decr         = generic_calibrate_decr,
       .progress               = udbg_progress,
};
#endif
#ifdef CONFIG_LVL7_BROADCOM_WDOG

#ifdef CONFIG_FXC_P2020

/*
** Disable the watchdog feature
*/
void watchdog_disable(void)
{
	if(mem) {

		/* Disable the watchdog, but leave the memory window open, 
		 * so that we can explicitly keep the watchdog hardware 
		 * serviced.  This will prevent random reboots if other 
		 * systems touch the wrong GPIO pins, and interfere with 
		 * the watchdog.
		 */

		mem[CPU_P2020_GPIO_GPDAT_REG_OFFSET/4] &= ~(1<<(31-WATCHDOG_ENABLE_GPIO_PIN));
	}
}

/*
** Service watchdog. Generate Pulse at Watchdog GPIO Service Pin
*/
void watchdog_service(void)
{
	if (mem) {
	        /* ensure that the Watchdog GPIO pin is properly set for output */
                mem[CPU_P2020_GPIO_GPDIR_REG_OFFSET/4] |= (1<<(31-WATCHDOG_SERVICE_GPIO_PIN));

		/* Generate Pulse at Watchdog GPIO Service Pin*/
		mem[CPU_P2020_GPIO_GPDAT_REG_OFFSET/4]  &= ~(1<<(31-WATCHDOG_SERVICE_GPIO_PIN));
		mdelay(WATCHDOG_PULSE_WIDTH);
		mem[CPU_P2020_GPIO_GPDAT_REG_OFFSET/4]  |= (1<<(31-WATCHDOG_SERVICE_GPIO_PIN));
	}
}

/*
** Set up the watchdog.  This opens the memory window to allow the rest of the watchdog
** driver to manipulate the watchdog GPIO pins.
*/
watchdog_setup(void)
{
	if(mem==NULL) {
		mem = ioremap_nocache(((unsigned long)get_immrbase())+0xF000, 0x1000);
	}
}

/*
** Enable the watchdog feature
*/
int watchdog_enable(void)
{
	watchdog_setup ();

	if(mem==NULL) {
		printk(KERN_ALERT "Exiting... watchdog setup failed\n");
		return -1;
	}

	/* set the direction as output */
	mem[CPU_P2020_GPIO_GPDIR_REG_OFFSET/4] |= (1<<(31-WATCHDOG_ENABLE_GPIO_PIN));
	mem[CPU_P2020_GPIO_GPDIR_REG_OFFSET/4] |= (1<<(31-WATCHDOG_SERVICE_GPIO_PIN));

	watchdog_service();
	/* Enable the watchdog*/
	mem[CPU_P2020_GPIO_GPDAT_REG_OFFSET/4] |= (1<<(31-WATCHDOG_ENABLE_GPIO_PIN));
	watchdog_service();
	return 0;
}
/*
** Get the Watchdog Timer Interval in ms
*/
unsigned int watchdog_get_timer_interval(void)
{
	return WATCHDOG_TIMER_INTERVAL;
}
#endif /* CONFIG_FXC_P2020 */

#ifdef CONFIG_DNI_P2020 
/*
** Disable the watchdog feature
*/
void watchdog_disable(void)
{
	unsigned char data;
	if (cpldPtr) {
		data = in_8(cpldPtr+0x00c0);
		data &= ~(0x08);
		out_8(cpldPtr+0x00c0, data);
	} else {
		printk (KERN_EMERG "Error: cpld watchdog register not mapped.\n");
	}
}
/*
** Service watchdog. 
*/
void watchdog_service(void)
{
	unsigned char data;
	if (cpldPtr) {
		data = in_8(cpldPtr+0x00c0);
		data &= ~(0x01);
		out_8(cpldPtr+0x00c0, data);
	} else {
		printk (KERN_EMERG "Error: cpld watchdog register not mapped.\n");
	}
}
/*
** Enable the watchdog feature
*/
int watchdog_enable(void)
{
	if (cpldPtr) {
		out_8((cpldPtr+0x00c0), 0x58);
	} else {
		printk (KERN_EMERG "Error: cpld watchdog register not mapped.\n");
		return -1;
	}
	return 0;
}
/*
** Get the Watchdog Timer Interval in ms
*/
unsigned int watchdog_get_timer_interval(void)
{
	return WATCHDOG_TIMER_INTERVAL;
}
#endif /* CONFIG_DNI_P2020 */
#endif /* CONFIG_LVL7_BROADCOM_WDOG*/

#ifdef CONFIG_HADLEYG
#define HADLEYG_CPLD_ADDRESS  0x80000000
#define RESET_REG             0x23
#define WOLFF_CONTROL_REG     0x42
#define WOLFF_RESET_TIMEOUT   1000 /* 1000 msecs */
#define WOLFF_RESET_MASK       0x02
unsigned char *cpldPtr = NULL;

void p2020_had_restart(char *cmd)
{
	unsigned char data;
	if (cpldPtr) {
		local_irq_disable();

		/*
		 *  Forced Wolff reset before Box Reset. It was observed that the 
		 *  soft reset is not leading to the same switch state as Hard Reset
		 *  because Dma writes were not happening properly after soft reset.
		 *  Related Defect - LVL700208188 & LVL700210462
		*/
		data = in_8(cpldPtr+WOLFF_CONTROL_REG);
		/* Put Wolff in reset*/
		data &= ~(WOLFF_RESET_MASK);
		out_8(cpldPtr+WOLFF_CONTROL_REG, data);
		mdelay(WOLFF_RESET_TIMEOUT);
		/* Pull Wolff out of reset*/
		data |= (WOLFF_RESET_MASK);
		out_8(cpldPtr+WOLFF_CONTROL_REG, data);
		mdelay(WOLFF_RESET_TIMEOUT); /* wait for the device to stablize */

		/* Initiate HRESET*/
		out_8(cpldPtr+RESET_REG, 0x4c); /* Included Flash Reset too.LVL700210451(Issue 3)*/
	} else {
		printk (KERN_EMERG "Error: reset control register not mapped."
				"Please power cycle board manually!\n");
	}

        while (1) ;
}
/* HadleyG's watchdog handler */
void WatchdogHandler(struct pt_regs *regs)
{
	mtspr(SPRN_TCR, mfspr(SPRN_TCR)&(~TCR_WIE));
	printk (KERN_EMERG "Watchdog exception, resetting board....\n");
	p2020_had_restart(0);
}

static int __init hadleyg_cpld_init(void)
{
        cpldPtr = ioremap(HADLEYG_CPLD_ADDRESS, 1024);
        return 0;
}
late_initcall(hadleyg_cpld_init);


/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p2020_had_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P2020HAD")) {
#ifdef CONFIG_PCI
		primary_phb_addr = 0x9000;
#endif
		return 1;
	}

	return 0;
}
define_machine(p2020_had) {
       .name                   = "Hadleyg",
       .probe                  = p2020_had_probe,
       .setup_arch             = mpc85xx_ds_setup_arch,
       .init_IRQ               = mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
       .pcibios_fixup_bus      = fsl_pcibios_fixup_bus,
#endif
       .get_irq                = mpic_get_irq,
       .restart                = p2020_had_restart,
       .calibrate_decr         = generic_calibrate_decr,
       .progress               = udbg_progress,
};
#endif


