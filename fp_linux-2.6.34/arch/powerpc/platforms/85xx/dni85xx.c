/*
 * MPC85xx setup and early boot code plus other random bits.
 *
 * Maintained by Kumar Gala (see MAINTAINERS for contact information)
 *
 * Copyright 2005 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/reboot.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/major.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/initrd.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/fsl_devices.h>
#include <linux/mtd/physmap.h>

#include <asm/system.h>
#include <asm/pgtable.h>
#include <asm/page.h>
#include <asm/atomic.h>
#include <asm/time.h>
#include <asm/io.h>
#include <asm/machdep.h>
#include <asm/ipic.h>
#include <asm/pci-bridge.h>
#include <asm/irq.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/of_platform.h>
#include <asm/i8259.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>


#define BOARD_CCSRBAR   (0xe0000000)
#define CCSRBAR BOARD_CCSRBAR
#define BOARD_CPLD      (0xde000000)
char model[32];
static int dni85xx_reset_reg_offset;
static int dni85xx_pwr_reg_offset;
static int dni85xx_cpld_addr_size;
static int dni8536_config_select=0;

/* Kernel Logical Address of CPLD */
void *CPLD_ADDR = NULL;


#if defined(CONFIG_RTC_CLASS) && defined(CONFIG_RTC_DRV_DS1553)
static int __init mpc85xx_ds1553_rtc_init(void)
{
	struct device_node *np = NULL;
	struct resource r[2];
	int res_num = 0;
	int ret;
	struct platform_device *rtc_dev = NULL;

	memset(r, 0, sizeof(struct resource)*2);

	np = of_find_node_by_name(np, "rtc");
	if (NULL == np){
		printk(KERN_ERR "Could not find rtc-ds1553 node\n");	
		return -1;
	}

	ret = of_address_to_resource(np, 0, &r[0]);
	if (ret){
		printk(KERN_ERR "Could not get reg resource\n");
		return -1; 
	}else
		res_num++;

	ret = of_irq_to_resource(np, 0, &r[1]);
	if (ret <= 0)
		printk(KERN_ERR "Could not get irq resource\n");
	else
		res_num++; /* The IRQ# may not be provided in dts */

	rtc_dev = platform_device_register_simple("ds1553", 0, r, res_num); /* name MUST be "ds1553" */
	if (IS_ERR(rtc_dev)) {
		ret = PTR_ERR(rtc_dev);
		return -1;
	}

	return 0;
}
arch_initcall(mpc85xx_ds1553_rtc_init);
#endif /* CONFIG_RTC_CLASS && CONFIG_RTC_DRV_DS1553 */

#define NEED_CPLD_DRIVER
#ifdef NEED_CPLD_DRIVER

static unsigned int dni85xx_cpld_data = 0;
static unsigned char pwr_status = 0;

/* simple CPLD irq handler - just read register to make CPLD stop asserting */
irqreturn_t dni85xx_irq_handler(int irq, void *dev_id)
{
   volatile unsigned char *pwr_status_reg = (volatile unsigned char *)(CPLD_ADDR + dni85xx_pwr_reg_offset); 

   pwr_status = *pwr_status_reg;

   return IRQ_HANDLED;
}

static int __init dni85xx_cpld_init(void)
{
	  struct device_node *np = NULL;
	  struct resource r[2];
	  int res_num = 0;
	  int ret;
    int irq_num = 0;

	  struct platform_device *cpld_dev = NULL;

	  printk (KERN_DEBUG "dni85xx_cpld_init()\n");

	  memset(r, 0, sizeof(struct resource)*2);

	  np = of_find_node_by_name(np, "cpld");
	  if (NULL == np){
		  printk(KERN_ERR "Could not find cpld node\n");	
		  return -1;
	  }

	  ret = of_address_to_resource(np, 0, &r[0]);
	  if (ret){
		  printk(KERN_ERR "Could not get reg resource\n");
		  return -1; 
	  }else
		res_num++;

    /* map the address into kernel */
    CPLD_ADDR = ioremap(r[0].start, 1 + r[0].end - r[0].start);

    if (NULL == CPLD_ADDR){
      printk (KERN_ERR "dni85xx_cpld_init() - could not Map CPLD.\n");
      return -1;
      }else{
      printk (KERN_DEBUG "dni85xx_cpld_init() - PhyAddr: %x size:%x CPLD Mapped at 0x%08x.\n", r[0].start,(r[0].end - r[0].start), (unsigned int)CPLD_ADDR);
    }

      cpld_dev = platform_device_register_simple("cpld", 0, r, res_num); /* name MUST be "cpld" */
      if (IS_ERR(cpld_dev)) {
              ret = PTR_ERR(cpld_dev);
              return -1;
      }


    if(dni8536_config_select)
    {
       volatile unsigned char *cpldFlashBase = (volatile unsigned char *)((unsigned int)CPLD_ADDR + 0x6000);
        *cpldFlashBase = 0xff;
    }
    else
    {
	  /* Find our IRQ */
	  irq_num = of_irq_to_resource(np, 0, &r[1]);
	  if (irq_num <= 0){
		  printk(KERN_ERR "Could not get irq resource\n");
	      return -1;
	    }else{
		  res_num++; /* The IRQ# may not be provided in dts */
  	  }

	    printk (KERN_DEBUG "dni85xx_cpld_init() - irq = %d\n", irq_num);

	    /* hook to IRQ specified in Flat Device Tree */
	    ret = request_irq(irq_num, dni85xx_irq_handler, IRQF_DISABLED | IRQF_SHARED, "cpld", (void *)&dni85xx_cpld_data);
	
	    if (ret < 0){
			  printk(KERN_ERR "CPLD - request_irq() failed.\n");
	      return -1;
	    }else{
		  printk(KERN_DEBUG "CPLD - request_irq() succeeded.\n");
	    }
    }
     return 0;
}
late_initcall(dni85xx_cpld_init);
#endif

static void __init dni85xx_pic_init(void)
{
	struct mpic *mpic;
	struct resource r;
	struct device_node *np = NULL;

	np = of_find_node_by_type(np, "open-pic");

	if (np == NULL) {
		printk(KERN_ERR "Could not find open-pic node\n");
		return;
	}

	if (of_address_to_resource(np, 0, &r)) {
		printk(KERN_ERR "Failed to map mpic register space\n");
		of_node_put(np);
		return;
	}

	mpic = mpic_alloc(np, r.start,
			MPIC_PRIMARY | MPIC_WANTS_RESET | MPIC_BIG_ENDIAN,
			4, 0, " OpenPIC  ");
	BUG_ON(mpic == NULL);

	/* Return the mpic node */
	of_node_put(np);

	mpic_assign_isu(mpic, 0, r.start + 0x10200);
	mpic_assign_isu(mpic, 1, r.start + 0x10280);  /* for some reason was 10290 */
	mpic_assign_isu(mpic, 2, r.start + 0x10300);
	mpic_assign_isu(mpic, 3, r.start + 0x10380);
	mpic_assign_isu(mpic, 4, r.start + 0x10400);
	mpic_assign_isu(mpic, 5, r.start + 0x10480);
	mpic_assign_isu(mpic, 6, r.start + 0x10500);
	mpic_assign_isu(mpic, 7, r.start + 0x10580);
	mpic_assign_isu(mpic, 8, r.start + 0x10600);
	mpic_assign_isu(mpic, 9, r.start + 0x10680);
	mpic_assign_isu(mpic, 10, r.start + 0x10700);
	mpic_assign_isu(mpic, 11, r.start + 0x10780);
	mpic_assign_isu(mpic, 12, r.start + 0x10000);
	mpic_assign_isu(mpic, 13, r.start + 0x10080);
	mpic_assign_isu(mpic, 14, r.start + 0x10100);

	mpic_init(mpic);
}

void __init dni85xx_progress(char *s, unsigned short hex)
{
}

static struct of_device_id __initdata dni85xx_ids[] = {
        { .type = "soc", },
        { .compatible = "soc", },
        { .compatible = "simple-bus", },
        {},
};

static int __init dni85xx_publish_devices(void)
{
  return of_platform_bus_probe(NULL, dni85xx_ids, NULL);
}
machine_device_initcall(dni85xx, dni85xx_publish_devices);

/*
 * Setup the architecture
 */
static void __init mpc85xx_cds_setup_arch(void)
{
	struct device_node *cpu;
#ifdef CONFIG_PCI
	struct device_node *np;
#endif

  /* Init the progress */
#if 0
  /* Map CCSRBAR */
  settlbcam (num_tlbcam_entries - 1, CCSRBAR, CCSRBAR, 1048576, _PAGE_IO, 0);
#endif

	ppc_md.progress = dni85xx_progress;

	if (ppc_md.progress)
		ppc_md.progress("mpc85xx_cds_setup_arch()", 0);

	cpu = of_find_node_by_type(NULL, "cpu");
	if (cpu != 0) {
		const unsigned int *fp;

		fp = of_get_property(cpu, "clock-frequency", NULL);
		if (fp != 0)
			loops_per_jiffy = *fp / HZ;
		else
			loops_per_jiffy = 500000000 / HZ;
		of_node_put(cpu);
	}

#ifdef CONFIG_PCI
  for (np = NULL; (np = of_find_node_by_type(np, "pci")) != NULL;) {
    struct resource rsrc;
    of_address_to_resource(np, 0, &rsrc);
    if ((rsrc.start & 0xfffff) == 0x8000)
      fsl_add_bridge(np, 1);
    else
      fsl_add_bridge(np, 0);
   }
 
   /*	ppc_md.pcibios_fixup = mpc85xx_cds_pcibios_fixup;
	 ppc_md.pci_exclude_device = mpc85xx_exclude_device;*/
#endif

   /* Suppress console message during boot to shorten
    ** boot time.
    */
   console_loglevel = 4;
  
}

static void mpc85xx_cds_show_cpuinfo(struct seq_file *m)
{
	uint pvid, svid, phid1;
	uint memsize = total_memory;

	pvid = mfspr(SPRN_PVR);
	svid = mfspr(SPRN_SVR);

	seq_printf(m, "DNI85xx\n");
	seq_printf(m, "PVR\t\t: 0x%x\n", pvid);
	seq_printf(m, "SVR\t\t: 0x%x\n", svid);

	/* Display cpu Pll setting */
	phid1 = mfspr(SPRN_HID1);
	seq_printf(m, "PLL setting\t: 0x%x\n", ((phid1 >> 24) & 0x3f));

	/* Display the amount of memory */
	seq_printf(m, "Memory\t\t: %d MB\n", memsize / (1024 * 1024));
}


/*
 * Called very early, device-tree isn't unflattened
 */
static int __init
dni85xx_probe (void)
{
  unsigned long cplen;
  const char *dnimodel;
  unsigned long root = of_get_flat_dt_root ();

  dnimodel = of_get_flat_dt_prop (root, "compatible", &cplen);
  strncpy (model, dnimodel, cplen);
  printk ("dni85xx_probe: compatible Name %s\n", model);
  if (of_flat_dt_is_compatible (root, "DNI8536"))
    {
      dni85xx_reset_reg_offset = 0x2000;
      dni85xx_pwr_reg_offset = 0xC000;
      dni85xx_cpld_addr_size = 139264;;
      dni8536_config_select = 1;
      return 1;			/*success */
    }
  else
    {
      dni85xx_reset_reg_offset = 0x30;
      dni85xx_pwr_reg_offset = 0x20;
      return of_flat_dt_is_compatible (root, "DNI85xx");
    }
}

void
dni85xx_restart (char *cmd)
{
  if (CPLD_ADDR != NULL)
    {
      /* Use ioremap address for CPLD */
      volatile unsigned char *cpld_reset =
	(unsigned char *) (CPLD_ADDR + dni85xx_reset_reg_offset);

      /* reset board via CPLD register */
      udelay (1000);

      printk (KERN_EMERG "Writing CPLD (0x%08x) with 0x00.\n",
	      (unsigned int) cpld_reset);
      *cpld_reset = 0x00;
    }

  printk (KERN_EMERG "Reset didn't, spinning!\n");
  for (;;)
    /* Do nothing */ ;
}

static int __init
dni8536_mtd_setup (void)
{
  if (dni8536_config_select)
    {
      /* Set runtime MTD_PHYSMAP_SIZE, MTD_PHYSMAP_LEN, MTD_PHYSMAP_BANKWIDTH, Set_vpp */
      physmap_configure (0xfc000000, 0x4000000, 4, NULL);
    }
  return 0;
}

define_machine (dni85xx)
{
  .name = "DNI85xx",.probe = dni85xx_probe,.setup_arch = mpc85xx_cds_setup_arch,.init_IRQ = dni85xx_pic_init,.init_early = dni8536_mtd_setup,.show_cpuinfo = mpc85xx_cds_show_cpuinfo,.get_irq = mpic_get_irq,.restart = dni85xx_restart,.calibrate_decr = generic_calibrate_decr,.progress = NULL,	/* fixed up after CCSRBAR mapped */
.pcibios_fixup_bus = fsl_pcibios_fixup_bus,};


EXPORT (unsigned int CPLD_ADDR);
