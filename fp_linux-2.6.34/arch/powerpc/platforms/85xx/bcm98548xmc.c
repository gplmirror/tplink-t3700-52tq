/*
 * MPC85xx setup and early boot code plus other random bits.
 *
 * Maintained by Kumar Gala (see MAINTAINERS for contact information)
 *
 * Copyright 2005 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/reboot.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/major.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/initrd.h>
#include <linux/module.h>
#include <linux/fsl_devices.h>

#include <asm/system.h>
#include <asm/pgtable.h>
#include <asm/page.h>
#include <asm/atomic.h>
#include <asm/time.h>
#include <asm/io.h>
#include <asm/machdep.h>
#include <asm/ipic.h>
#include <asm/pci-bridge.h>
#include <asm/irq.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/of_platform.h>
#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>

#ifndef CONFIG_PCI
unsigned long isa_io_base = 0;
unsigned long isa_mem_base = 0;
#endif

static void __init bcm98548xmc_pic_init(void)
{
	struct mpic *mpic;
	struct resource r;
	struct device_node *np = NULL;

	np = of_find_node_by_type(np, "open-pic");

	if (np == NULL) {
		printk(KERN_ERR "Could not find open-pic node\n");
		return;
	}

	if (of_address_to_resource(np, 0, &r)) {
		printk(KERN_ERR "Failed to map mpic register space\n");
		of_node_put(np);
		return;
	}

	mpic = mpic_alloc(np, r.start,
			MPIC_PRIMARY | MPIC_WANTS_RESET | MPIC_BIG_ENDIAN,
			4, 0, " OpenPIC  ");
	BUG_ON(mpic == NULL);

	/* Return the mpic node */
	of_node_put(np);

	mpic_assign_isu(mpic, 0, r.start + 0x10200);
	mpic_assign_isu(mpic, 1, r.start + 0x10280);
	mpic_assign_isu(mpic, 2, r.start + 0x10300);
	mpic_assign_isu(mpic, 3, r.start + 0x10380);
	mpic_assign_isu(mpic, 4, r.start + 0x10400);
	mpic_assign_isu(mpic, 5, r.start + 0x10480);
	mpic_assign_isu(mpic, 6, r.start + 0x10500);
	mpic_assign_isu(mpic, 7, r.start + 0x10580);

	/* Used only for 8548 so far, but no harm in
	 * allocating them for everyone */
	mpic_assign_isu(mpic, 8, r.start + 0x10600);
	mpic_assign_isu(mpic, 9, r.start + 0x10680);
	mpic_assign_isu(mpic, 10, r.start + 0x10700);
	mpic_assign_isu(mpic, 11, r.start + 0x10780);

	/* External Interrupts */
	mpic_assign_isu(mpic, 12, r.start + 0x10000);
	mpic_assign_isu(mpic, 13, r.start + 0x10080);
	mpic_assign_isu(mpic, 14, r.start + 0x10100);

	mpic_init(mpic);

}

static struct of_device_id __initdata bcm98548xmc_ids[] = {
	{ .type = "soc", },
	{ .compatible = "soc", },
	{ .compatible = "simple-bus", },
	{ .compatible = "gianfar", },
	{},
};

static int __init bcm98548xmc_publish_devices(void)
{
	return of_platform_bus_probe(NULL, bcm98548xmc_ids, NULL);
}
machine_device_initcall(bcm98548xmc, bcm98548xmc_publish_devices);

/*
 * Setup the architecture
 */
static void __init bcm98548xmc_setup_arch(void)
{
	struct device_node *cpu;
#ifdef CONFIG_PCI
	struct device_node *np;
#endif

	if (ppc_md.progress)
		ppc_md.progress("bcm98548xmc_setup_arch()", 0);

	cpu = of_find_node_by_type(NULL, "cpu");
	if (cpu != 0) {
		const unsigned int *fp;

		fp = of_get_property(cpu, "clock-frequency", NULL);
		if (fp != 0)
			loops_per_jiffy = *fp / HZ;
		else
			loops_per_jiffy = 500000000 / HZ;
		of_node_put(cpu);
	}

#ifdef CONFIG_PCI
	for (np = NULL; (np = of_find_node_by_type(np, "pci")) != NULL;) {
            struct resource rsrc;
            of_address_to_resource(np, 0, &rsrc);
            if ((rsrc.start & 0xfffff) == 0x8000)
                fsl_add_bridge(np, 1);
            else
                fsl_add_bridge(np, 0);
        }
#endif
}

static void bcm98548xmc_show_cpuinfo(struct seq_file *m)
{
	uint pvid, svid, phid1;

	pvid = mfspr(SPRN_PVR);
	svid = mfspr(SPRN_SVR);

	seq_printf(m, "Vendor\t\t: Freescale Semiconductor\n");
	seq_printf(m, "PVR\t\t: 0x%x\n", pvid);
	seq_printf(m, "SVR\t\t: 0x%x\n", svid);

	/* Display cpu Pll setting */
	phid1 = mfspr(SPRN_HID1);
	seq_printf(m, "PLL setting\t: 0x%x\n", ((phid1 >> 24) & 0x3f));
}

static __be32 __iomem *rstcr;
static __be32 __iomem *rstdr;
static int __init bcm98548xmc_init_rstcr(void)
{
	/* map reset control register */
	printk(KERN_INFO "bcm98548xmc : map reset control register.\n");
	rstdr = ioremap(get_immrbase() + 0xE0040, 0xff);
	rstcr = ioremap(get_immrbase() + 0xE0030, 0xff);
	return 0;
}
arch_initcall(bcm98548xmc_init_rstcr);

void bcm98548xmc_restart(char *cmd)
{
	local_irq_disable();
	if (rstcr && rstdr) {
		/* set reset control register */
		out_be32(rstdr, ~0x0);	/* HRESET_REQ */
		out_be32(rstcr, 0x200);	/* HRESET_REQ */
		out_be32(rstdr, ~0x1);	/* HRESET_REQ */
		out_be32(rstdr, ~0x3);	/* HRESET_REQ */
	} else
		printk (KERN_EMERG "Error: reset control register not mapped."
				"Please power cycle board manually!\n");
	while(1);
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init bcm98548xmc_probe(void)
{
        return 1;
}

define_machine(bcm98548xmc) {
	.name		= "BCM98548 XMC",
	.probe		= bcm98548xmc_probe,
	.setup_arch	= bcm98548xmc_setup_arch,
	.init_IRQ	= bcm98548xmc_pic_init,
	.show_cpuinfo	= bcm98548xmc_show_cpuinfo,
	.get_irq	= mpic_get_irq,
	.restart	= bcm98548xmc_restart,
	.calibrate_decr = generic_calibrate_decr,
	.progress	= udbg_progress,
};

