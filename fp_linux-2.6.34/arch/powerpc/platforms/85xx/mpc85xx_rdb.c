/*
 * MPC85xx RDB Board Setup
 *
 * Copyright 2009 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>

#include <asm/system.h>
#include <asm/time.h>
#include <asm/machdep.h>
#include <asm/pci-bridge.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>

#undef DEBUG

#ifdef DEBUG
#define DBG(fmt, args...) printk(KERN_ERR "%s: " fmt, __func__, ## args)
#else
#define DBG(fmt, args...)
#endif


void __init mpc85xx_rdb_pic_init(void)
{
	struct mpic *mpic;
	struct resource r;
	struct device_node *np;
	unsigned long root = of_get_flat_dt_root();

	np = of_find_node_by_type(NULL, "open-pic");
	if (np == NULL) {
		printk(KERN_ERR "Could not find open-pic node\n");
		return;
	}

	if (of_address_to_resource(np, 0, &r)) {
		printk(KERN_ERR "Failed to map mpic register space\n");
		of_node_put(np);
		return;
	}

	if (of_flat_dt_is_compatible(root, "fsl,MPC85XXRDB-CAMP")) {
		mpic = mpic_alloc(np, r.start,
			MPIC_PRIMARY |
			MPIC_BIG_ENDIAN | MPIC_BROKEN_FRR_NIRQS,
			0, 256, " OpenPIC  ");
	} else {
		mpic = mpic_alloc(np, r.start,
		  MPIC_PRIMARY | MPIC_WANTS_RESET |
		  MPIC_BIG_ENDIAN | MPIC_BROKEN_FRR_NIRQS |
		  MPIC_SINGLE_DEST_CPU,
		  0, 256, " OpenPIC  ");
	}

	BUG_ON(mpic == NULL);
	of_node_put(np);

	mpic_init(mpic);

}

/*
 * Setup the architecture
 */
#ifdef CONFIG_SMP
extern void __init mpc85xx_smp_init(void);
#endif
static void __init mpc85xx_rdb_setup_arch(void)
{
	void __iomem *immap_pmuxcr;

#ifdef CONFIG_PCI
	struct device_node *np;
#endif

	if (ppc_md.progress)
		ppc_md.progress("mpc85xx_rdb_setup_arch()", 0);

#ifdef CONFIG_PCI
	for_each_node_by_type(np, "pci") {
		if (of_device_is_compatible(np, "fsl,mpc8548-pcie"))
			fsl_add_bridge(np, 0);
	}

#endif

#define MPC85XX_PMUXCR_OFFSET	0xE0060
#define MPC85XX_PMUXCR_SDHC_CD	0x40000000
#define MPC85XX_PMUXCR_SD_DATA	0x80000000
	immap_pmuxcr = ioremap(get_immrbase() + MPC85XX_PMUXCR_OFFSET, 0x4);
	if (!immap_pmuxcr)
		return;

	out_be32(immap_pmuxcr, in_be32(immap_pmuxcr) & ~MPC85XX_PMUXCR_SD_DATA);
	out_be32(immap_pmuxcr, in_be32(immap_pmuxcr) | MPC85XX_PMUXCR_SDHC_CD);

	iounmap(immap_pmuxcr);

#ifdef CONFIG_SMP
	mpc85xx_smp_init();
#endif

	/* Clear the ME bit in MSR to cause checkstop on machine check
	*/
	mtmsr(mfmsr() & ~0x1000);

	printk(KERN_INFO "MPC85xx RDB board from Freescale Semiconductor\n");
}

static struct of_device_id __initdata mpc85xxrdb_ids[] = {
	{ .type = "soc", },
	{ .compatible = "soc", },
	{ .compatible = "simple-bus", },
	{ .compatible = "gianfar", },
	{},
};

static int __init mpc85xxrdb_publish_devices(void)
{
	return of_platform_bus_probe(NULL, mpc85xxrdb_ids, NULL);
}
machine_device_initcall(p2020_rdb, mpc85xxrdb_publish_devices);
machine_device_initcall(p1020_rdb, mpc85xxrdb_publish_devices);

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p2020_rdb_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P2020RDB"))
		return 1;
	return 0;
}

static int __init p1020_rdb_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P1020RDB"))
		return 1;
	return 0;
}

define_machine(p2020_rdb) {
	.name			= "P2020 RDB",
	.probe			= p2020_rdb_probe,
	.setup_arch		= mpc85xx_rdb_setup_arch,
	.init_IRQ		= mpc85xx_rdb_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

define_machine(p1020_rdb) {
	.name			= "P1020 RDB",
	.probe			= p1020_rdb_probe,
	.setup_arch		= mpc85xx_rdb_setup_arch,
	.init_IRQ		= mpc85xx_rdb_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

#ifdef CONFIG_CH_P1010 
machine_device_initcall(ch_p1011, mpc85xxrdb_publish_devices);

/* #define CPLD_ADDRESS  0xffb00000 */
#define CPLD_ADDRESS  0xf0000000

void ch_p1011_restart(char *cmd)
{
        unsigned char *cpldPtr = ioremap(CPLD_ADDRESS, 0x8000);

        if (cpldPtr) {
      /*        local_irq_disable();*/
                out_8(cpldPtr+1, 0x1);
        } else {
                printk (KERN_EMERG "Error: reset control register not mapped."
                                "Please power cycle board manually!\n");
        }

        while (1) ;
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init ch_p1011_probe(void)
{
        unsigned long root = of_get_flat_dt_root();

        if (of_flat_dt_is_compatible(root, "fsl,CHP1011RDB")) {
                return 1;
        }
        return 0;
}

define_machine(ch_p1011) {
	.name			= "CH P1011",
	.probe			= ch_p1011_probe,
	.setup_arch		= mpc85xx_rdb_setup_arch,
	.init_IRQ		= mpc85xx_rdb_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= ch_p1011_restart,
	.calibrate_decr		= generic_calibrate_decr,
  .progress               = NULL,
};
#endif
