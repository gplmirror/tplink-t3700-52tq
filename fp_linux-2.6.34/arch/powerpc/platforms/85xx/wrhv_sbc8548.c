/*
 * Wind River SBC8548 HV setup and early boot code.
 *
 * Copyright 2010 Wind River Systems Inc.
 *
 * Based largely on the sbc8548 and 8572 support - Copyright 2007
 * Wind River Systems.
 *
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/reboot.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/major.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/initrd.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/fsl_devices.h>
#include <linux/of_platform.h>
#include <linux/lmb.h>

#include <asm/system.h>
#include <asm/pgtable.h>
#include <asm/page.h>
#include <asm/atomic.h>
#include <asm/time.h>
#include <asm/io.h>
#include <asm/machdep.h>
#include <asm/ipic.h>
#include <asm/pci-bridge.h>
#include <asm/irq.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/wrhv.h>
#include <vbi/vbi.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>

extern struct vb_config *wr_config;
extern struct vb_status *wr_status;
extern struct vb_control *wr_control;
extern int wrhv_set_law_base(int index, unsigned long long addr);
extern unsigned long long wrhv_get_law_base(int index);
extern int wrhv_set_law_attr(int index, unsigned int attr);
extern int wrhv_get_law_attr(int index);

static void __init wrhv_sbc8548_map_config(void)
{
	wrhv_mapping(); /* Map VB_CONFIG structure */
	vbi_init(wr_config);

	strncpy(cmd_line, VBI_BOOTLINE_ADDR_GET(), VB_MAX_BOOTLINE_LENGTH - 1);
	cmd_line[VB_MAX_BOOTLINE_LENGTH - 1] = 0;

	/* Save command line for /proc/cmdline */
	strlcpy(boot_command_line, cmd_line, COMMAND_LINE_SIZE);
}

static void __init sbc8548_pic_init(void)
{
	wrhv_init_irq();
}

/*
 * Setup the architecture
 */
static void __init sbc8548_setup_arch(void)
{
#ifdef CONFIG_PCI
	struct device_node *np;
	struct pci_controller *hose;
#endif
	dma_addr_t max = 0xffffffff;

	if (ppc_md.progress)
		ppc_md.progress("sbc8548_setup_arch()", 0);

	get_hv_bsp_server_handle();
	wrhv_cpu_freq = get_bsp_clock_freq();

#ifdef CONFIG_PCI
	for_each_node_by_type(np, "pci") {
		if (of_device_is_compatible(np, "fsl,mpc8540-pci") ||
		    of_device_is_compatible(np, "fsl,mpc8548-pcie")) {
			struct resource rsrc;
			of_address_to_resource(np, 0, &rsrc);
			if ((rsrc.start & 0xfffff) == 0x8000)
				fsl_add_bridge(np, 1);
			else
				fsl_add_bridge(np, 0);

			hose = pci_find_hose_for_OF_device(np);
			max = min(max, hose->dma_window_base_cur +
				hose->dma_window_size);

			ppc_setup_pci_law(np);
		}
	}
#endif

#ifdef CONFIG_SWIOTLB
	if (lmb_end_of_DRAM() > max) {
		ppc_swiotlb_enable = 1;
		set_pci_dma_ops(&swiotlb_dma_ops);
		ppc_md.pci_dma_dev_setup = pci_dma_dev_setup_swiotlb;
	}
#endif

	printk("WRHV - sbc8548 board from Freescale Semiconductor\n");
}

static void sbc8548_show_cpuinfo(struct seq_file *m)
{
	seq_printf(m, "Vendor\t\t: Wind River\n");
}

static struct of_device_id __initdata of_bus_ids[] = {
	{ .name = "soc", },
	{ .type = "soc", },
	{ .compatible = "simple-bus", },
	{ .compatible = "gianfar", },
	{},
};

static int __init declare_of_platform_devices(void)
{
	of_platform_bus_probe(NULL, of_bus_ids, NULL);

	return 0;
}
machine_device_initcall(sbc8548, declare_of_platform_devices);

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init sbc8548_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	wrhv_sbc8548_map_config();
	return of_flat_dt_is_compatible(root, "SBC8548");
}

define_machine(sbc8548) {
	.name			= "WRHV_SBC8548",
	.probe			= sbc8548_probe,
	.setup_arch		= sbc8548_setup_arch,
	.init_IRQ		= sbc8548_pic_init,
	.get_irq		= wrhv_vioapic_get_irq,
	.power_save		= wrhv_power_save,
	.earlycon_setup 	= wrhv_earlycon_setup,
	.show_cpuinfo		= sbc8548_show_cpuinfo,
	.restart		= wrhv_restart,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
	.enable_pci_law		= wrhv_enable_pci_law,
	.set_law_base		= wrhv_set_law_base,
	.get_law_base		= wrhv_get_law_base,
	.set_law_attr		= wrhv_set_law_attr,
	.get_law_attr		= wrhv_get_law_attr,
#endif
	.calibrate_decr 	= wrhv_calibrate_decr,
	.progress		= udbg_progress,
	.earlycon_setup 	= wrhv_earlycon_setup,
};
