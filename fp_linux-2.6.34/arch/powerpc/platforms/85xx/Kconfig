menuconfig FSL_SOC_BOOKE
	bool "Freescale Book-E Machine Type"
	depends on PPC_85xx || PPC_BOOK3E
	select FSL_SOC
	select PPC_UDBG_16550
	select MPIC
	select PPC_PCI_CHOICE
	select FSL_PCI if PCI
	select SERIAL_8250_SHARE_IRQ if SERIAL_8250
	default y

if FSL_SOC_BOOKE

config MPC8540_ADS
	bool "Freescale MPC8540 ADS"
	select DEFAULT_UIMAGE
	help
	  This option enables support for the MPC 8540 ADS board

config MPC8560_ADS
	bool "Freescale MPC8560 ADS"
	select DEFAULT_UIMAGE
	select CPM2
	help
	  This option enables support for the MPC 8560 ADS board

config MPC85xx_CDS
	bool "Freescale MPC85xx CDS"
	select DEFAULT_UIMAGE
	select PPC_I8259
	help
	  This option enables support for the MPC85xx CDS board

config MPC85xx_MDS
	bool "Freescale MPC85xx MDS"
	select DEFAULT_UIMAGE
	select PHYLIB
	select HAS_RAPIDIO
	select SWIOTLB
	help
	  This option enables support for the MPC85xx MDS board

config BCM98548XMC
        bool "Broadcom BCM98548XMC"
        select DEFAULT_UIMAGE
        help
          This option enables support for the Broadcom BCM98548XMC board

config QC8541
        bool "Quanta 8541"
        select DEFAULT_UIMAGE
        help
          This option enables support for the Quanta 8541 board

config QC8541_1G
	bool "Quanta 8541 (1GB)"
	select DEFAULT_UIMAGE
	select PPC_I8259
	help
	  This option enables support for the Quanta 8541 board

config QC8548
        bool "Quanta 8548"
        select DEFAULT_UIMAGE
        help
          This option enables support for the Quanta 8548 board

config MPC8536_DS
	bool "Freescale MPC8536 DS"
	select DEFAULT_UIMAGE
	select SWIOTLB
	help
	  This option enables support for the MPC8536 DS board

config MPC85xx_DS
	bool "Freescale MPC85xx DS"
	select PPC_I8259
	select DEFAULT_UIMAGE
	select FSL_ULI1575 if PCI
	select SWIOTLB
	help
	  This option enables support for the MPC85xx DS (MPC8544 DS) board

config MPC85xx_RDB
	bool "Freescale MPC85xx RDB"
	select PPC_I8259
	select DEFAULT_UIMAGE
	select FSL_ULI1575 if PCI
	select SWIOTLB
	help
	  This option enables support for the MPC85xx RDB (P2020 RDB) board

config DNI_P2020
	bool "Freescale P2020 from dni"
	select PPC_I8259
	select DEFAULT_UIMAGE
	help
	  This option enables support for the dni P2020 board

config FXC_P2020   
       bool "Freescale P2020 from Foxconn"
       select PPC_I8259
       select DEFAULT_UIMAGE
       help
         This option enables support for the Foxconn P2020 board

config QC_P2020
       bool "Freescale P2020 from Quanta"
       select PPC_I8259
       select DEFAULT_UIMAGE
       help
         This option enables support for the Quanta P2020 board

config HADLEYG
       bool "Freescale P2020 Hadleyg CPU card"
       select PPC_I8259
       select DEFAULT_UIMAGE
       select LVL7_BOOKE_WDOG
       help
         This option enables support for the HadleyG CPU

config P1010_RDB
	bool "Freescale P1010RDB"
	select PPC_I8259
	select DEFAULT_UIMAGE
	select FSL_ULI1575 if PCI
	select SWIOTLB
	help
	  This option enables support for the P1010 RDB board

config CH_P1010
	bool "Cheetah Freescale P1010 H2"
	select PPC_I8259
	select DEFAULT_UIMAGE
	select FSL_ULI1575 if PCI
	select SWIOTLB
	help
	  This option enables support for the Cheetah P1010 board

config DNI85xx
	bool "DNI85xx"
	select DEFAULT_UIMAGE
	help
	 This option enables support for the DNI85xx boards

config SOCRATES
	bool "Socrates"
	select DEFAULT_UIMAGE
	help
	  This option enables support for the Socrates board.

config KSI8560
        bool "Emerson KSI8560"
        select DEFAULT_UIMAGE
        help
          This option enables support for the Emerson KSI8560 board

config XES_MPC85xx
	bool "X-ES single-board computer"
	select DEFAULT_UIMAGE
	help
	  This option enables support for the various single-board
	  computers from Extreme Engineering Solutions (X-ES) based on
	  Freescale MPC85xx processors.
	  Manufacturer: Extreme Engineering Solutions, Inc.
	  URL: <http://www.xes-inc.com/>

config STX_GP3
	bool "Silicon Turnkey Express GP3"
	help
	  This option enables support for the Silicon Turnkey Express GP3
	  board.
	select CPM2
	select DEFAULT_UIMAGE

config TQM8540
	bool "TQ Components TQM8540"
	help
	  This option enables support for the TQ Components TQM8540 board.
	select DEFAULT_UIMAGE
	select TQM85xx

config TQM8541
	bool "TQ Components TQM8541"
	help
	  This option enables support for the TQ Components TQM8541 board.
	select DEFAULT_UIMAGE
	select TQM85xx
	select CPM2

config TQM8548
	bool "TQ Components TQM8548"
	help
	  This option enables support for the TQ Components TQM8548 board.
	select DEFAULT_UIMAGE
	select TQM85xx

config TQM8555
	bool "TQ Components TQM8555"
	help
	  This option enables support for the TQ Components TQM8555 board.
	select DEFAULT_UIMAGE
	select TQM85xx
	select CPM2

config TQM8560
	bool "TQ Components TQM8560"
	help
	  This option enables support for the TQ Components TQM8560 board.
	select DEFAULT_UIMAGE
	select TQM85xx
	select CPM2

config SBC8548
	bool "Wind River SBC8548"
	select DEFAULT_UIMAGE
	help
	  This option enables support for the Wind River SBC8548
	  board

config SBC8560
	bool "Wind River SBC8560"
	select DEFAULT_UIMAGE
	help
	  This option enables support for the Wind River SBC8560 board

config P4080_DS
	bool "Freescale P4080 DS"
	select DEFAULT_UIMAGE
	select PPC_FSL_BOOK3E
	select PPC_E500MC
	select PHYS_64BIT
	select SWIOTLB
	select MPC8xxx_GPIO
	select HAS_RAPIDIO
	help
	  This option enables support for the P4080 DS board

config WRHV_SBC8548
	bool "WindRiver SBC8548 (HV)"
	depends on WRHV
	select DEFAULT_UIMAGE
	select WRHV_E500
	select PARAVIRT_PTE
	select SWIOTLB
	help
	  This option enables support for the Wind River SBC8548
	  Hypervisor enabled board.

config WRHV_E500
	bool

endif # FSL_SOC_BOOKE

config TQM85xx
	bool
