#ifndef __PAEMSGHOST_H_
#define __PAEMSGHOST_H_

int iProcPaeMessageSend (uint8_t *message, uint16_t type, int len, uint16_t flags);
int iProcPaeMessageGet (uint8_t *message, uint32_t *len);

#endif /* __PAEMSGHOST_H_ */
