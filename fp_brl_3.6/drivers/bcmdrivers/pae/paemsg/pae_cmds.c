/******************************************************************************/
/*                                                                            */
/*  Copyright 2013  Broadcom Corporation                                      */
/*                                                                            */
/*     Unless you and Broadcom execute a separate written software license    */
/*     agreement governing  use of this software, this software is licensed   */
/*     to you under the terms of the GNU General Public License version 2     */
/*     (the GPL), available at                                                */
/*                                                                            */
/*          http://www.broadcom.com/licenses/GPLv2.php                        */
/*                                                                            */
/*     with the following added to such license:                              */
/*                                                                            */
/*     As a special exception, the copyright holders of this software give    */
/*     you permission to link this software with independent modules, and to  */
/*     copy and distribute the resulting executable under terms of your       */
/*     choice, provided that you also meet, for each linked independent       */
/*     module, the terms and conditions of the license of that module.        */
/*     An independent module is a module which is not derived from this       */
/*     software.  The special exception does not apply to any modifications   */
/*     of the software.                                                       */
/*     Notwithstanding the above, under no circumstances may you combine      */
/*     this software in any way with any other Broadcom software provided     */
/*     under a license other than the GPL, without Broadcom's express prior   */
/*     written consent.                                                       */
/*                                                                            */
/******************************************************************************/

#include <linux/err.h>
#include <linux/delay.h>
#include <linux/kobject.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/stat.h>
#include <linux/ctype.h>


#include <net/xfrm.h>
#include <linux/pfkeyv2.h>


#include "paemsg.h"
#include "pae_cmds.h"
#include "pae_regs.h"
#include "pae_shared.h"




#define CMD_TYPE_TCMADDR_R5 (0x40000060)
#define CMD_RESP_TCMADDR_R5 (0x40000064)
#define CMD_BUF_TCMADDR_R5  (0x40000068)
#define CMD_BUF_SIZE (512)
#define CMD_RESP_PENDING (0xffffffff)
#define MAX_CMD_RESP_ITER (1000)

#define R5_TICKS_PER_JIFFY (500000000 / 16 / HZ)  /* 500MHz, 16 cycles per clock incr */

static ssize_t pae_dev_attr_action_stats_store(struct device *dev, struct device_attribute *attr,
                                               const char *buf, size_t count);

static ssize_t pae_dev_attr_action_stats_show(struct device *dev,
                                              struct device_attribute *devattr, char *buf);

dev_attr_action_stats_t dev_attr_action_stats[PAE_NUM_ACTIONS][NUM_STATS_ATTRS] = {
    {
        {
            .name = "0.hits",
            .dev_attr = { .attr = {
                    .name = dev_attr_action_stats[0][0].name,
                    .mode = S_IRUSR | S_IWUSR,
                },
                          .show = pae_dev_attr_action_stats_show,
                          .store = pae_dev_attr_action_stats_store
            }
        },
        {
            .name = "0.bytes",
            .dev_attr = {
                .attr = {
                    .name = dev_attr_action_stats[0][1].name,
                    .mode = S_IRUSR | S_IWUSR,
                },
                .show = pae_dev_attr_action_stats_show,
                .store = pae_dev_attr_action_stats_store
            }
        },
        {
            .name = "0.elapsed",
            .dev_attr = {
                .attr = {
                    .name = dev_attr_action_stats[0][2].name,
                    .mode = S_IRUSR | S_IWUSR,
                },
                .show = pae_dev_attr_action_stats_show,
                .store = pae_dev_attr_action_stats_store
            }
        },
    }
};


static unsigned pae_default_mtu = 1500;

int pae_add_action_stats_sysfs(int idx)
{
    int i, rv;
    if (dev_attr_action_stats[idx][0].name[0]) {
        /* already in use */
        return -EEXIST;
    }

    sprintf(dev_attr_action_stats[idx][0].name, "%d.hits", idx);
    sprintf(dev_attr_action_stats[idx][1].name, "%d.bytes", idx);
    sprintf(dev_attr_action_stats[idx][2].name, "%d.elapsed", idx);

    for (i = 0; i < NUM_STATS_ATTRS; ++i) {
        sysfs_attr_init(&dev_attr_action_stats[idx].dev_attr.attr);
        dev_attr_action_stats[idx][i].dev_attr.attr.name = dev_attr_action_stats[idx][i].name;
        dev_attr_action_stats[idx][i].dev_attr.attr.mode = S_IRUSR | S_IWUSR;
        dev_attr_action_stats[idx][i].dev_attr.show = pae_dev_attr_action_stats_show;
        dev_attr_action_stats[idx][i].dev_attr.store = pae_dev_attr_action_stats_store;
        
        rv = pae_add_action_stats_attribute(&dev_attr_action_stats[idx][i].dev_attr.attr);
        if (rv) {
            break;
        }
    }
    return rv;
}


void pae_remove_action_stats_sysfs(int idx)
{
    int i;
    for (i = 0; i < NUM_STATS_ATTRS; ++i) {
        pae_remove_action_stats_attribute(&dev_attr_action_stats[idx][i].dev_attr.attr);
    }
    dev_attr_action_stats[idx][0].name[0] = 0;
}


void pae_set_default_mtu(unsigned mtu)
{
    pae_default_mtu = mtu;
}


unsigned pae_do_cmd(unsigned cmd)
{
    int i;

    pae_set_tcm_u32(CMD_RESP_TCMADDR_R5, CMD_RESP_PENDING);
    pae_set_tcm_u32(CMD_TYPE_TCMADDR_R5, cmd);

    for (i = 0; i < MAX_CMD_RESP_ITER; ++i) {
        if (pae_get_tcm_u32(CMD_TYPE_TCMADDR_R5) == 0) {
            break;
        }
        udelay(10);
    }

    if (pae_get_tcm_u32(CMD_TYPE_TCMADDR_R5) != 0) {
        printk(KERN_ERR "No response from PAE for command %u\n", cmd);
        return -ETIMEDOUT;
    }

    return pae_get_tcm_u32(CMD_RESP_TCMADDR_R5);
}


int pae_add_action_with_mtu(unsigned idx, unsigned action_type, unsigned mtu, u8 *action_data, unsigned action_data_len_bytes)
{
    unsigned action_data_len_words = (action_data_len_bytes + 3) >> 2;
    int i;
    int rv;

    if (action_data_len_bytes > CMD_BUF_SIZE - 8) {
        return -EINVAL;
    }

    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5, idx);
    pae_set_tcm_u8(CMD_BUF_TCMADDR_R5 + 4, action_type);
    pae_set_tcm_u8(CMD_BUF_TCMADDR_R5 + 5, action_data_len_words + 1); // +1 for action header word

    pae_set_tcm_u16(CMD_BUF_TCMADDR_R5 + 6, mtu);

    /* To avoid leaking previous contents in case the data isn't */
    /* an even number of words, zero the last word */
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5 + 8 + (4 * (action_data_len_bytes / 4)), 0);

    for (i = 0; i < action_data_len_bytes; ++i) {
        pae_set_tcm_u8(CMD_BUF_TCMADDR_R5 + 8 + i, action_data[i]);
    }

    rv = pae_do_cmd(PAE_ACTION_ADD);
    if (rv != 0) {
        return rv;
    }

    return pae_add_action_stats_sysfs(idx);
}


int pae_add_action(unsigned idx, int action_type, u8 *action_data, unsigned action_data_len_bytes)
{
    return pae_add_action_with_mtu(idx, action_type, pae_default_mtu, action_data, action_data_len_bytes);
}


int pae_delete_action(unsigned idx)
{
    int rv;
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5, idx);

    rv = pae_do_cmd(PAE_ACTION_DELETE);
    if (rv == 0) {
        pae_remove_action_stats_sysfs(idx);
    }
    return rv;
}


int pae_update_action(unsigned idx, unsigned offset, u8 *updated_data, unsigned update_len_bytes)
{
    int i;
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5, idx);
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5 + 4, offset);
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5 + 8, update_len_bytes);

    /* To avoid leaking previous contents in case the data isn't */
    /* an even number of words, zero the last word */
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5 + 12 + (4 * (update_len_bytes / 4)), 0);

    for (i = 0; i < update_len_bytes; ++i) {
        pae_set_tcm_u8(CMD_BUF_TCMADDR_R5 + 12 + i, updated_data[i]);
    }

    return pae_do_cmd(PAE_ACTION_UPDATE);
}

int pae_get_action_stats(unsigned idx, pae_action_stats_t *stats)
{
    uint32_t last_hit_time_r5, cur_time_r5;
    unsigned long delta_time_jiffies;

    if (idx >= PAE_NUM_ACTIONS) {
        return -EINVAL;
    }

    stats->bytes = pae_get_tcm_u32(ACTION_PERF_BASE +
                                   idx * ACTION_PERF_SIZE_PER +
                                   ACTION_PERF_BYTES_OFFSET);

    stats->packets = pae_get_tcm_u32(ACTION_PERF_BASE +
                                     idx * ACTION_PERF_SIZE_PER
                                     + ACTION_PERF_PACKETS_OFFSET);

    last_hit_time_r5  = pae_get_tcm_u32(ACTION_PERF_BASE +
                                        idx * ACTION_PERF_SIZE_PER
                                        + ACTION_PERF_HIT_TIME_OFFSET);

    cur_time_r5  = pae_get_tcm_u32(R5_CUR_TIME);
    
    delta_time_jiffies = (cur_time_r5 - last_hit_time_r5) / (R5_TICKS_PER_JIFFY);

    stats->last_hit_jiffies = jiffies - delta_time_jiffies;

    return 0;
}


uint32_t pae_read_memory(uint32_t addr)
{
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5, addr);
    return pae_do_cmd(PAE_MEMORY_READ);
}


uint32_t pae_write_memory(uint32_t addr, uint32_t val)
{
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5, addr);
    pae_set_tcm_u32(CMD_BUF_TCMADDR_R5 + 4, val);
    return pae_do_cmd(PAE_MEMORY_WRITE);
}


static unsigned parse_unsigned(const char *buf, size_t count, const char **remaining_buf, size_t *remaining_count, unsigned *value)
{
    char tmp_buf[16];
    int tmp_size=0;
    int got_nonspace = 0;

    const char *curs = buf;
    int rv;

    *remaining_count = count;
    while (*remaining_count && tmp_size < sizeof(tmp_buf) && *curs) {
        if (!isspace(*curs)) {
            tmp_buf[tmp_size++] = *curs;
            got_nonspace = 1;
        } else if (got_nonspace) {
            // break on space once we've seen something usable
            break;
        }
        curs++;
        --*remaining_count;
    }

    *remaining_buf = curs;

    if (tmp_size == sizeof(tmp_buf)) {
        printk(KERN_INFO "Number length too big\n");
        /* number was bigger than is sensible, so just bail */
        return -EINVAL;
    }
    
    if (!got_nonspace) {
        /* no non-whitespace */
        return -EINVAL;
    }

    tmp_buf[tmp_size] = 0;  /* null terminate */
    if ((rv = kstrtouint(tmp_buf, 0, value))) {
        printk(KERN_INFO "Invalid number\n");
        return -EINVAL;
    }

    /* printk("Returning value of %u\n", *value); */
    *remaining_buf = curs;
    
    return 0;
}


/* ADD ACTION SYSFS */

/* the result of the last add command, so it can be returned if/when the file is read */
static int add_cmd_result;

static ssize_t write_add_action(struct device *dev, struct device_attribute *attr,
                                const char *buf, size_t count)
{
    const char *curs = buf;
    unsigned idx, action_type, mtu;
    u8 action_buf[CMD_BUF_SIZE - 8];
    unsigned val, action_len = 0;
    size_t count_left = count;

    if (parse_unsigned(curs, count_left, &curs, &count_left, &idx)) {
        printk(KERN_INFO "Bad index\n");
        add_cmd_result = -1;
        return -EINVAL;
    }

    if (parse_unsigned(curs, count_left, &curs, &count_left, &action_type)) {
        printk(KERN_INFO "Bad action type\n");
        add_cmd_result = -1;
        return -EINVAL;
    }

    if (parse_unsigned(curs, count_left, &curs, &count_left, &mtu)) {
        printk(KERN_INFO "Bad MTU\n");
        add_cmd_result = -1;
        return -EINVAL;
    }

    while (parse_unsigned(curs, count_left, &curs, &count_left, &val) == 0) {
        if (val > 255) {
            printk(KERN_INFO "Action data must be bytes (value was %d)\n", val);
            break;
        }
        action_buf[action_len] = val;
        ++action_len;
    }

    add_cmd_result = pae_add_action_with_mtu(idx, action_type, mtu, action_buf, action_len);

    printk("Added action.  Result: %d\n", add_cmd_result);

    return count;
}


static ssize_t read_add_action(struct device *dev,
                               struct device_attribute *devattr, char *buf)
{
    return sprintf(buf, "%d\n", add_cmd_result);
}


struct device_attribute dev_attr_pae_action_add =
    __ATTR(action_add, S_IRUSR | S_IWUSR, read_add_action, write_add_action);


/* DELETE ACTION SYSFS */

/* the result of the last delete command, so it can be returned if/when the file is read */
static int delete_cmd_result;

static ssize_t write_delete_action(struct device *dev, struct device_attribute *attr,
                                   const char *buf, size_t count)
{
    const char *curs = buf;
    unsigned count_left = count;
    unsigned idx;
    
    if (parse_unsigned(curs, count_left, &curs, &count_left, &idx)) {
        printk(KERN_INFO "Bad index\n");
        delete_cmd_result = -1;
        return -EINVAL;
    }

    delete_cmd_result = pae_delete_action(idx);

    printk("Deleted action.  Result: %d\n", delete_cmd_result);

    return count;
}


static ssize_t read_delete_action(struct device *dev,
                               struct device_attribute *devattr, char *buf)
{
    return sprintf(buf, "%d\n", delete_cmd_result);
}


struct device_attribute dev_attr_pae_action_delete =
    __ATTR(action_delete, S_IRUSR | S_IWUSR, read_delete_action, write_delete_action);


/* UPDATE ACTION SYSFS */

/* the result of the last update command, so it can be returned if/when the file is read */
static int update_cmd_result;

static ssize_t write_update_action(struct device *dev, struct device_attribute *attr,
                                const char *buf, size_t count)
{
    const char *curs = buf;
    unsigned idx, offset;
    u8 update_buf[CMD_BUF_SIZE - 12];
    unsigned val, update_len = 0;
    size_t count_left = count;

    if (parse_unsigned(curs, count_left, &curs, &count_left, &idx)) {
        printk(KERN_INFO "Bad index\n");
        add_cmd_result = -1;
        return -EINVAL;
    }

    if (parse_unsigned(curs, count_left, &curs, &count_left, &offset)) {
        printk(KERN_INFO "Bad action type\n");
        add_cmd_result = -1;
        return -EINVAL;
    }

    while (parse_unsigned(curs, count_left, &curs, &count_left, &val) == 0) {
        if (val > 255) {
            printk(KERN_INFO "Update data must be bytes (value was %d)\n", val);
            break;
        }
        update_buf[update_len++] = val;
    }

    add_cmd_result = pae_update_action(idx, offset, update_buf, update_len);

    printk("Updated action.  Result: %d\n", add_cmd_result);

    return count;
}


static ssize_t read_update_action(struct device *dev,
                                  struct device_attribute *devattr, char *buf)
{
    return sprintf(buf, "%d\n", update_cmd_result);
}


struct device_attribute dev_attr_pae_action_update =
    __ATTR(action_update, S_IRUSR | S_IWUSR, read_update_action, write_update_action);


/* Action statistics SysFS */

static ssize_t pae_dev_attr_action_stats_store(struct device *dev, struct device_attribute *attr,
                                               const char *buf, size_t count)
{
    return count;
}

static ssize_t pae_dev_attr_action_stats_show(struct device *dev,
                                              struct device_attribute *devattr, char *buf)
{
    /* Use the attribute's name to determine what to return */
    /* e.g. 0.hits, 35.bytes, 100.elapsed */
    int rv;
    int idx;
    const char *periodpos = 0;
    const char *remaining;
    size_t remaining_count;
    pae_action_stats_t stats;
    
    periodpos = strchr(devattr->attr.name, '.');
    if (!periodpos) {
        printk("Bad filename '%s', no '.'\n", devattr->attr.name);
    }

    rv = parse_unsigned(devattr->attr.name, periodpos - devattr->attr.name, &remaining, &remaining_count, &idx);
    if (rv) {
        printk("Could not parse number in '%s'\n", devattr->attr.name);
        return 0;
    }

    rv = pae_get_action_stats(idx, &stats);
    if (rv) {
        printk("Failed to get stats\n");
        return 0;
    }
    
    switch (periodpos[1]) {
    case 'h':  /* "hits" */
        return sprintf(buf, "%lu", stats.packets);
        break;
    case 'b':  /* "bytes" */
        return sprintf(buf, "%lu", stats.bytes);
        break;
    case 'e':  /* "elapsed" */
        {
            unsigned long elapsed_jiffies = jiffies - stats.last_hit_jiffies;
            unsigned long elapsed_sec = elapsed_jiffies / HZ;
            unsigned long elapsed_ms = ((elapsed_jiffies - (elapsed_sec * HZ)) * 1000) / HZ;

            return sprintf(buf, "%lu.%03lu", elapsed_sec, elapsed_ms);
        }
        break;
    default:
        printk("Bad filename '%s' ('%c')\n", devattr->attr.name, remaining[1]);
        return 0;
    }
}


/* MEMORY SYSFS */

/* the result of the last memory command, so it can be returned if/when the file is read */
static unsigned memory_result;

static ssize_t write_memory(struct device *dev, struct device_attribute *attr,
                            const char *buf, size_t count)
{
    const char *curs = buf;
    unsigned count_left = count;
    unsigned addr, val;
    
    if (parse_unsigned(curs, count_left, &curs, &count_left, &addr)) {
        printk(KERN_INFO "Bad address\n");
        memory_result = 0;
        return -EINVAL;
    }

    if (parse_unsigned(curs, count_left, &curs, &count_left, &val)) {
        /* No value, treat as memory read */
        memory_result = pae_read_memory(addr);
        printk("Read PAE memory %08x: %08x\n", addr, memory_result);
        return count;
    } else {
        memory_result = pae_write_memory(addr, val);
        printk("Wrote PAE memory %08x: %08x (result %d)\n", addr, val, memory_result);
        return count;
    }
}


static ssize_t read_memory(struct device *dev,
                           struct device_attribute *devattr, char *buf)
{
    return sprintf(buf, "%d\n", memory_result);
}


struct device_attribute dev_attr_pae_memory =
    __ATTR(memory, S_IRUSR | S_IWUSR, read_memory, write_memory);
