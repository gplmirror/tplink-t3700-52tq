/******************************************************************************/
/*                                                                            */
/*  Copyright 2013  Broadcom Corporation                                      */
/*                                                                            */
/*     Unless you and Broadcom execute a separate written software license    */
/*     agreement governing  use of this software, this software is licensed   */
/*     to you under the terms of the GNU General Public License version 2     */
/*     (the GPL), available at                                                */
/*                                                                            */
/*          http://www.broadcom.com/licenses/GPLv2.php                        */
/*                                                                            */
/*     with the following added to such license:                              */
/*                                                                            */
/*     As a special exception, the copyright holders of this software give    */
/*     you permission to link this software with independent modules, and to  */
/*     copy and distribute the resulting executable under terms of your       */
/*     choice, provided that you also meet, for each linked independent       */
/*     module, the terms and conditions of the license of that module.        */
/*     An independent module is a module which is not derived from this       */
/*     software.  The special exception does not apply to any modifications   */
/*     of the software.                                                       */
/*     Notwithstanding the above, under no circumstances may you combine      */
/*     this software in any way with any other Broadcom software provided     */
/*     under a license other than the GPL, without Broadcom's express prior   */
/*     written consent.                                                       */
/*                                                                            */
/******************************************************************************/


#ifndef PAE_SHARED_H
#define PAE_SHARED_H

/* Shared values with firmware */

#define PAE_NUM_ACTIONS (1024) 
#define PAE_NUM_ERROR_BUFFERS (32) 
#define PAE_DEBUG_BUF_SIZE (1024) 
#define PAE_ERROR_BUF_MASK (0x000000ff) 

typedef enum {
    PAE_NO_MESSAGE         = 0,
    PAE_ACTION_ADD         = 1,
    PAE_ACTION_DELETE      = 2,
    PAE_ACTION_UPDATE      = 3,
    PAE_ACTION_ERROR_CLEAR = 4,
    PAE_FA_RULE_SET        = 5,
    PAE_LUE_RULE_ADD       = 7,
    PAE_LUE_RULE_DELETE    = 8,

    PAE_MEMORY_READ        = 9,
    PAE_MEMORY_WRITE       = 10,
} pae_msg_type_t;

typedef enum {
    PAE_ERR_NONE               = 0,
    PAE_ERR_BAD_STATE          = 1,  /* Signals unexpected/corrupted state in FW */
    PAE_ERR_INVALID_ACTION_IDX = 2,
    PAE_ERR_ACTION_IDX_IN_USE  = 3,
    PAE_ERR_MEMORY             = 4,
    PAE_ERR_INVALID_DATA       = 5,
    PAE_ERR_INVALID_DB         = 6,
    PAE_ERR_INVALID_DB_IDX     = 7,
    PAE_ERR_INVALID_FA_IDX     = 8
} pae_resp_type_t;

typedef enum {
    PAE_ACTION_TYPE_FORWARD        = 0,
    PAE_ACTION_TYPE_HEADER_REWRITE = 1,
    PAE_ACTION_TYPE_IPSEC          = 2
} pae_action_type_t;

#define R5_CUR_TIME   (0x40000004)
#define R5_IDLE_TIME  (0x40000008)
#define R5_IS_IDLE    (0x4000000c)
#define R5_IDLE_START (0x40000010)

#define ACTION_PERF_BASE (0x4000066c)
#define ACTION_PERF_SIZE_PER (4 * sizeof(uint32_t))
#define ACTION_PERF_BYTES_OFFSET (0)
#define ACTION_PERF_PACKETS_OFFSET (4)
#define ACTION_PERF_HIT_TIME_OFFSET (8)
#define ACTION_PERF_ERROR_STATE_OFFSET (c)


#endif /* PAE_SHARED_H */
