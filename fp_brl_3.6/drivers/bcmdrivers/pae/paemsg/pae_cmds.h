/******************************************************************************/
/*                                                                            */
/*  Copyright 2013  Broadcom Corporation                                      */
/*                                                                            */
/*     Unless you and Broadcom execute a separate written software license    */
/*     agreement governing  use of this software, this software is licensed   */
/*     to you under the terms of the GNU General Public License version 2     */
/*     (the GPL), available at                                                */
/*                                                                            */
/*          http://www.broadcom.com/licenses/GPLv2.php                        */
/*                                                                            */
/*     with the following added to such license:                              */
/*                                                                            */
/*     As a special exception, the copyright holders of this software give    */
/*     you permission to link this software with independent modules, and to  */
/*     copy and distribute the resulting executable under terms of your       */
/*     choice, provided that you also meet, for each linked independent       */
/*     module, the terms and conditions of the license of that module.        */
/*     An independent module is a module which is not derived from this       */
/*     software.  The special exception does not apply to any modifications   */
/*     of the software.                                                       */
/*     Notwithstanding the above, under no circumstances may you combine      */
/*     this software in any way with any other Broadcom software provided     */
/*     under a license other than the GPL, without Broadcom's express prior   */
/*     written consent.                                                       */
/*                                                                            */
/******************************************************************************/

#ifndef PAE_CMDS_H
#define PAE_CMDS_H

#include <linux/types.h>
#include <linux/device.h>
#include <linux/jiffies.h>

#include "pae_shared.h"

#define NUM_STATS_ATTRS (3)

typedef struct pae_action_stats_s {
    unsigned long bytes;
    unsigned long packets;
    unsigned long last_hit_jiffies;
} pae_action_stats_t;

/* Function interface */
void pae_set_default_mtu(unsigned mtu);
int pae_add_action(unsigned idx, int action_type, u8 *action_data, unsigned action_data_len_bytes);
int pae_add_action_with_mtu(unsigned idx, unsigned action_type, unsigned mtu, u8 *action_data, unsigned action_data_len_bytes);

int pae_delete_action(unsigned idx);
int pae_update_action(unsigned idx, unsigned offset, u8 *updated_data, unsigned update_len_bytes);

/* Get hit counts and last hit time for an action.   */
/*     Values are absolute, and will eventually wrap */
int pae_get_action_stats(unsigned idx, pae_action_stats_t *stats);

/* For debug use.  May be removed in future. */
uint32_t pae_read_memory(uint32_t addr);
uint32_t pae_write_memory(uint32_t addr, uint32_t val);

/* SysFS File interface */

typedef struct {
    char name[16];
    struct device_attribute dev_attr;
} dev_attr_action_stats_t;

int pae_add_action_stats_sysfs(int idx);
void pae_remove_action_stats_sysfs(int idx);

extern dev_attr_action_stats_t dev_attr_action_stats[PAE_NUM_ACTIONS][NUM_STATS_ATTRS];
extern struct device_attribute dev_attr_pae_action_add;
extern struct device_attribute dev_attr_pae_action_delete;
extern struct device_attribute dev_attr_pae_action_update;
extern struct device_attribute dev_attr_pae_action_clear_error;

/* For debug: Host can request PAE to read or write memory in its address space */
extern struct device_attribute dev_attr_pae_memory;





#endif /* PAE_CMDS_H */
