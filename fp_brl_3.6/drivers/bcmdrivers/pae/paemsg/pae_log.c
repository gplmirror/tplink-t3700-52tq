/******************************************************************************/
/*                                                                            */
/*  Copyright 2013  Broadcom Corporation                                      */
/*                                                                            */
/*     Unless you and Broadcom execute a separate written software license    */
/*     agreement governing  use of this software, this software is licensed   */
/*     to you under the terms of the GNU General Public License version 2     */
/*     (the GPL), available at                                                */
/*                                                                            */
/*          http://www.broadcom.com/licenses/GPLv2.php                        */
/*                                                                            */
/*     with the following added to such license:                              */
/*                                                                            */
/*     As a special exception, the copyright holders of this software give    */
/*     you permission to link this software with independent modules, and to  */
/*     copy and distribute the resulting executable under terms of your       */
/*     choice, provided that you also meet, for each linked independent       */
/*     module, the terms and conditions of the license of that module.        */
/*     An independent module is a module which is not derived from this       */
/*     software.  The special exception does not apply to any modifications   */
/*     of the software.                                                       */
/*     Notwithstanding the above, under no circumstances may you combine      */
/*     this software in any way with any other Broadcom software provided     */
/*     under a license other than the GPL, without Broadcom's express prior   */
/*     written consent.                                                       */
/*                                                                            */
/******************************************************************************/

#include <linux/err.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <net/xfrm.h>

#include "pae_log.h"
#include "pae_regs.h"

/* Locations in the address space of the R5 */
#define DEBUG_HEAD_TCMADDR_R5 (0x40000268)
#define DEBUG_BUF_TCMADDR_R5  (0x4000026c)
#define DEBUG_BUF_SIZE (1024)

static struct dentry *fw_debug_dir_dentry = 0;
static struct dentry *fw_debug_dentry = 0;

static ssize_t debugfs_read(struct file *f, char __user *user_buf, size_t count, loff_t *offset)
{
    int rv = 0;
    int num_copied = 0;
    u32 cur_head = pae_get_tcm_u32(DEBUG_HEAD_TCMADDR_R5);
    u32 buf_idx = *offset % DEBUG_BUF_SIZE;

	if (!user_buf) {
		return -EINVAL;
    }

	if (count == 0) {
		return 0;
    }

	if (!access_ok(VERIFY_WRITE, user_buf, count)) {
		return -EFAULT;
    }

    while (!rv && num_copied < count && buf_idx != cur_head) {
        u8 c = pae_get_tcm_u8(DEBUG_BUF_TCMADDR_R5 + buf_idx);
        rv = __put_user(c, user_buf + num_copied);
        ++num_copied;
        if (++buf_idx == DEBUG_BUF_SIZE) {
            buf_idx = 0;
        }
    }
    
    if (!rv) {
        *offset += num_copied;
        return num_copied;
    } else {
        return rv;
    }
}


static const struct file_operations debugfs_fops = {
    .read = debugfs_read
};


int pae_debug_setup()
{
    int rv = 0;

    fw_debug_dir_dentry = debugfs_create_dir("bcmiproc-pae", NULL);
    if (!fw_debug_dir_dentry) {
        printk("Failed to make PAE debug log\n");
        rv = -ENOMEM;
        goto failed_create_debug_dir;
    }

    fw_debug_dentry = debugfs_create_file("log", 0x600, fw_debug_dir_dentry, 0, &debugfs_fops);

    if (!fw_debug_dentry) {
        printk("Failed to make PAE debug log\n");
        rv = -ENOMEM;
        goto failed_create_debug;
    }

    return 0;

 failed_create_debug:
    debugfs_remove(fw_debug_dir_dentry);
    
 failed_create_debug_dir:
    return rv;
}


int pae_debug_exit()
{
    debugfs_remove(fw_debug_dentry);
    debugfs_remove(fw_debug_dir_dentry);

    return 0;
}
