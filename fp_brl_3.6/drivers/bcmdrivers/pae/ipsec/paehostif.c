/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *
 */
#include <linux/err.h>
#include <linux/module.h>
#include <net/ip.h>
#include <net/xfrm.h>
#include <net/esp.h>
#include <net/ah.h>
#include <linux/kernel.h>
#include <linux/pfkeyv2.h>
#include <linux/in6.h>
#include <linux/ipsec.h>
#include <net/icmp.h>
#include <net/protocol.h>
#include <net/udp.h>

#include "iprocspu.h"
#include "paemsg.h"
#include "paehost.h"
#include "paeipsecmsg.h"

#define BUFF_SIZE        1024

#define IPV4_TUNNEL_PRESET_LEN    12
uint8_t ipv4_tunnel_preset [IPV4_TUNNEL_PRESET_LEN] = 
    {0x45, IPV4_TOS_DEFAULT, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, IPV4_TTL_DEFAULT, 0x00, 0x00, 0x00};

#define FLABEL0       (IPV6_FLABEL_DEFAULT & 0xFF)
#define FLABEL1       (IPV6_FLABEL_DEFAULT & 0xFF00 >> 8)
#define FLABEL2       (IPV6_FLABEL_DEFAULT & 0xFF0000 >> 16)

#define IPV6_TUNNEL_PRESET_LEN   8 
uint8_t ipv6_tunnel_preset [IPV6_TUNNEL_PRESET_LEN] = 
    {0x6 , FLABEL2, FLABEL1, FLABEL0, 0x00, IPV6_HOP_DEFAULT, 0x00, 0x00};

#ifndef HTONL
#define HTONL(x)        ((x)=htonl(x))
#endif


//=================== Linux IPsec Stack PAE Interface =====================
#ifndef __KERNEL__
/*---------------------------------------------------------------
 * Name    : DataDump
 * Purpose : Hardware Crypto Encapsulation Callback
 * Input   : 
 * Return  : Error status
 * Remarks : 
 *--------------------------------------------------------------*/
void
DataDump(char *msg, void *buf, int len)
{
    int n;
    uint8_t *p = (uint8_t *) buf;

    printk("\n%s\n", msg);
    for (n = 0; n < len && n < 256; n++) {
        printk("%02X", *p++);
        if (!((n + 1) % 8))
            printk("\n");
    }
    printk("\n");
}
#endif /* __KERNEL__ */

/*---------------------------------------------------------------
 * Name    : Dump_skb
 * Purpose : Hardware Crypto Encapsulation Callback
 * Input   : 
 * Return  : Error status
 * Remarks : 
 *--------------------------------------------------------------*/
void
Dump_skb(struct sk_buff *s)
{
    // Looking at it as skb
    printk("skb %p: data %p tail %p len %d:%d", (void *)s, (void *)s->data, (void *)s->tail, s->len,
           s->tail - s->data);
    //printk ("head %p end %p", s, s->head, s->end);
    DataDump("skb data", s->data, 16);
}

int PaeHostIpsecInit(void)
{
    return PaeIpsecInit();
}

int PaeHostIpsecShutdown(void)
{
    return PaeIpsecShutdown();
}

int PaeHostIpsecDeleteSA (struct xfrm_state *x) 
{
    PAEIF_SAID said;
    // Fill the said
    said.spi = x->id.spi;
    said.flags.proto = x->id.proto;
    return PaeIpsecDeleteSA(&said);
}

int PaeHostIpsecListSA (PAEIF_SAID *said, int *len) 
{
    int status;
    int maxLen = MAX_PAE_SA_COUNT * sizeof(PAEIF_SAID);

    if (maxLen > *len)
        maxLen = *len;

    status = PaeIpsecListSA(said, &maxLen);
    if (status) {
        *len = 0;
        goto error;
    }
    *len = maxLen;

error:
    return status;
}


#ifdef USE_FHMAC_MODE
#define MAX_HMAC_CONTEXT             64
#define HMAC_INNER_CONTEXT           1
#define HMAC_OUTER_CONTEXT           2 

static int
create_hmac_context(struct xfrm_state *x, uint8_t *dst, int context)
{
    uint8_t ipad = 0x36;
    uint8_t opad = 0x5c;
    struct xfrm_algo_desc *aalgd = NULL;
    int i;

    uint8_t pad[MAX_HMAC_CONTEXT];

    if (!x->aalg) {
        printk(KERN_LEVEL "No auth alg\n");
        return -1;
    }

    aalgd = xfrm_aalg_get_byname(x->aalg->alg_name, 0),
    memcpy (pad, x->aalg->alg_key, x->aalg->alg_key_len / 8 );
    for (i = 0; i < MAX_HMAC_CONTEXT; i++) {
        if (context == HMAC_INNER_CONTEXT) {
            ctx[i] ^= ipad;
        }
        else if (context == HMAC_OUTER_CONTEXT) {
            ctx[i] ^= opad;
        }
        else {
            printk(KERN_LEVEL "Unknown HMAC context type %d\n", context);
            return -1;
        }
    }
    switch (aalgd->desc.sadb_alg_id) {
    case SADB_AALG_MD5HMAC:
        break;
    case SADB_AALG_SHA1HMAC:
        break;
    default:
        printk(KERN_LEVEL "HMAC algorithm %d not supported for FHMAC mode\n", 
                aalgd->desc.sadb_alg_id);
        return -1;
    }
    memcpy (dst, ctx, x->aalg->alg_key_len / 8);
    return 0;
}
#endif


/*-------------------------------------------------------------------------
 * Name    : PaeHostIpsecAddSA
 * Purpose : Create the PAE Context for the SA and submit to the PAE 
 * Input   : 
 * Return  : Error status
 * Remarks : 
 *------------------------------------------------------------------------*/
int PaeHostIpsecAddSA(struct xfrm_state *x)
{
    int status = PAEIF_STATUS_OK;
    uint8_t *buff;
    PAEIF_SAID sa_id;
    PAEIF_SAID *said = &sa_id;
    PAEIF_CRYPTOOP cryptoop;
    PAEIF_CRYPTOOP *op = &cryptoop;
    TLVWORD *tlv;
	static int count = 0;
    uint32_t *p;
	uint8_t *ptr;
    int n;

    printk(KERN_LEVEL "SA SPI %08x proto %d\n", ntohl(x->id.spi), x->id.proto);

    buff = kmalloc (BUFF_SIZE, GFP_KERNEL);
    if (!buff) {
        printk(KERN_LEVEL "Cannot allocate memory");
        status = PAEIF_HOST_ERROR_ALLOC;
        goto error;
    }
    memset((void *)buff, 0, BUFF_SIZE);
    memset((void *)said, 0, sizeof(PAEIF_SAID));
    memset((void *)op, 0, sizeof(PAEIF_CRYPTOOP));

    said->spi = x->id.spi;
    /* set the SA ID  */
    switch (x->id.proto) {
    case IPPROTO_ESP:
    case IPPROTO_AH:
        said->flags.proto = x->id.proto;
        break;
    default:
        printk(KERN_LEVEL "Unknown IP protocol %d\n", x->id.proto);
        status = PAEIF_HOST_ERROR_INPUT;
        goto error;
    }

    /* Specify IPv4 context */
    if (x->sel.family == AF_INET) {
        said->flags.ipv4 = 1;
        memcpy((void *)said->v4dstAddr, (void *)&x->id.daddr.a4, SIZE_IPV4_ADDR);
		if (count++ % 2) {
            said->flags.dir = PAEIF_DIR_INBOUND;
            printk(KERN_LEVEL "direction is inbound\n");
        } else {
            said->flags.dir = PAEIF_DIR_OUTBOUND;
            printk(KERN_LEVEL "direction is outbound\n");
        }
    } else if (x->sel.family == AF_INET6) {
        said->flags.ipv6 = 1;
        memcpy((void *)said->v6dstAddr, (void *)x->id.daddr.a6, SIZE_IPV6_ADDR);
    }

    /* check if extended sequence number is required */
    if (x->props.flags & XFRM_STATE_ESN) {
        said->flags.esn = 1;
    }

    /* Define the crypto operations for this SA */
    /* Specify the authentication protocol if any */
    if (x->aalg) {
        struct xfrm_algo_desc *aalgd;
        aalgd = xfrm_aalg_get_byname(x->aalg->alg_name, 0);
        if (!aalgd) {
            printk(KERN_LEVEL "Auth algorithm error\n");
            status = PAEIF_HOST_ERROR_INPUT;
            goto error;
        }
        switch (aalgd->desc.sadb_alg_id) {
        case SADB_AALG_NONE:
        case SADB_X_AALG_NULL:
            op->authAlg = PAEIF_AUTH_ALG_NULL;
            break;
        case SADB_AALG_MD5HMAC:
            op->authAlg = PAEIF_AUTH_ALG_MD5;
            op->authMode = PAEIF_AUTH_MODE_HMAC;
            break;
        case SADB_AALG_SHA1HMAC:
            op->authAlg = PAEIF_AUTH_ALG_SHA1;
            op->authMode = PAEIF_AUTH_MODE_HMAC;
            break;
        case SADB_X_AALG_SHA2_256HMAC:
            op->authAlg = PAEIF_AUTH_ALG_SHA256;
            op->authMode = PAEIF_AUTH_MODE_HMAC;
            break;
        case SADB_X_AALG_AES_XCBC_MAC:
            op->authAlg = PAEIF_AUTH_ALG_AES;
            op->authMode = PAEIF_AUTH_MODE_XCBC;
            break;
        default:
            printk(KERN_LEVEL "Unsuported HMAC algorithm %d\n",
                   aalgd->desc.sadb_alg_id);
            status = PAEIF_HOST_ERROR_AUTH_ALG;
            goto error;
        }
        /* copy the keys to the CRYPTOOP structure */
        op->icvLen = x->aalg->alg_trunc_len;
        if (x->aalg->alg_key_len > MAX_AUTH_KEY_SIZE) {
            status = PAEIF_HOST_ERROR_KEY_LENGTH;
            goto error;
        }
        if (op->authAlg != PAEIF_AUTH_ALG_NULL) {
			memcpy (op->authKey, x->aalg->alg_key, (x->aalg->alg_key_len + 7)/8); 
			op->authKeyLen = x->aalg->alg_key_len;
            said->flags.auth = 1;
		}
    }    

    /* Specify the authenticed encryption protocol if any */
    if (x->aead) {
        struct xfrm_algo_desc *aealgd;
        int icvLen;
        for (icvLen = 8; icvLen < 17; icvLen += 4) {
            aealgd = xfrm_aead_get_byname(x->aead->alg_name, icvLen, 0);
            if (!aealgd) {
                continue;
            }
            switch (aealgd->desc.sadb_alg_id) {
            case SADB_X_EALG_AES_GCM_ICV8:
            case SADB_X_EALG_AES_GCM_ICV12:
            case SADB_X_EALG_AES_GCM_ICV16:
                /* Encryption AND authentication algorithm rfc4106 */
                op->authAlg = PAEIF_ENCR_ALG_AES;
                op->authMode = PAEIF_ENCR_MODE_GCM;
                op->icvLen = icvLen;
                break;
            default:
                printk(KERN_LEVEL "Unsuported authenticated encryption algorithm %d\n",
                       aealgd->desc.sadb_alg_id);
                status = PAEIF_HOST_ERROR_ENCR_ALG;
                goto error;
                break;
            }
            if (!aealgd) {
                printk(KERN_LEVEL "Authicated encryption algorithm error\n");
                status = PAEIF_HOST_ERROR_INPUT;
                goto error;
            }
        }
        /* copy the keys to the CRYPTOOP structure */
        if (x->aead->alg_key_len > MAX_AUTH_KEY_SIZE) {
            status = PAEIF_HOST_ERROR_KEY_LENGTH;
            goto error;
        }
        memcpy (op->authKey, x->aead->alg_key, (x->aead->alg_key_len + 7)/8); 
		op->authKeyLen = x->aead->alg_key_len;
    }

    /* Set the encryption protocol */
    if (x->id.proto == IPPROTO_ESP) {
        struct xfrm_algo_desc *ealgd;
        ealgd = xfrm_ealg_get_byname(x->ealg->alg_name, 0);
        if (!ealgd) {
            printk(KERN_LEVEL "ESP but no encryption algorithm\n");
            status = PAEIF_HOST_ERROR_ENCR_ALG;
            goto error;
        }
        switch (ealgd->desc.sadb_alg_id) {
        case SADB_EALG_NULL:
            op->encrAlg = PAEIF_ENCR_ALG_NULL;
            op->encrMode = PAEIF_ENCR_MODE_CBC;
           break;
        case SADB_EALG_DESCBC:
            op->encrAlg = PAEIF_ENCR_ALG_DES;
            op->encrMode = PAEIF_ENCR_MODE_CBC;
            said->flags.ivWords = 2;
        break;
        case SADB_EALG_3DESCBC:
            op->encrAlg = PAEIF_ENCR_ALG_3DES;
            op->encrMode = PAEIF_ENCR_MODE_CBC;
            said->flags.ivWords = 2;
            break;
        case SADB_X_EALG_AESCBC:
            op->encrAlg = PAEIF_ENCR_ALG_AES;
            op->encrMode = PAEIF_ENCR_MODE_CBC;
            switch (x->ealg->alg_key_len) {
            case 128:
            case 192:
            case 256:
                said->flags.ivWords = 4;
                break;
            default:
                printk(KERN_LEVEL "AES CBC key length %d not supported\n", 
                        x->ealg->alg_key_len);
                status = PAEIF_HOST_ERROR_KEY_LENGTH;
                goto error;
                break;
            }
            break;
        case SADB_X_EALG_AESCTR:
            op->encrAlg = PAEIF_ENCR_ALG_AES;
            op->encrMode = PAEIF_ENCR_MODE_CTR;
            switch (x->ealg->alg_key_len) {
            case 128:
            case 192:
            case 256:
                break;
            default:
                printk(KERN_LEVEL "AES CTR key length %d not supported\n", 
                        x->ealg->alg_key_len);
                status = PAEIF_HOST_ERROR_KEY_LENGTH;
                goto error;
                break;
            }
            break;
        case SADB_X_EALG_NULL_AES_GMAC:
            /* Authentication only algorithm rfc4543 */
            op->authAlg = PAEIF_AUTH_ALG_AES;
            op->authMode = PAEIF_AUTH_MODE_GMAC;
            op->icvLen = 12;
            switch (x->ealg->alg_key_len) {
            case 128:
            case 192:
            case 256:
                break;
            default:
                printk(KERN_LEVEL "AES GMAC key length %d not supported\n", 
                        x->ealg->alg_key_len);
                status = PAEIF_HOST_ERROR_KEY_LENGTH;
                goto error;
                break;
            }
            break;
        default:
            printk(KERN_LEVEL "Unsuported encryption algorithm %d\n",
                   ealgd->desc.sadb_alg_id);
            status = PAEIF_HOST_ERROR_ENCR_ALG;
            goto error;
            break;
        }
        /* copy the keys to the CRYPTOOP structure */
        if (x->ealg->alg_key_len > MAX_ENC_KEY_SIZE) {
            status = PAEIF_HOST_ERROR_KEY_LENGTH;
            goto error;
        }
        if (op->encrAlg != PAEIF_ENCR_ALG_NULL) {
			memcpy (op->encrKey, x->ealg->alg_key, (x->ealg->alg_key_len + 7)/8); 
			op->encrKeyLen = x->ealg->alg_key_len;
            said->flags.encr = 1;
		}
    }

    if (x->props.mode == XFRM_MODE_TUNNEL) {
        said->flags.mode = PAEIF_MODE_TUNNEL;
    	if (said->flags.dir == PAEIF_DIR_OUTBOUND)
        	said->flags.tun_payload = 1;
	} else {
        said->flags.mode = PAEIF_MODE_TRANSPORT;
	}
    /* Determine which payloads will be added to the SA */

#ifdef USE_IPSEC_POLICY
    said->flags.spd_payload = 1;
#endif

#ifdef USE_IPSEC_NAT_TRAVERSAL
    if ((said->flags.ipv4) && (x->encap)) {
        said->flags.nat_payload = 1;
    }
#endif

	DataDump ("SAID", said, sizeof(PAEIF_SAID));
	//DataDump ("OP", op, sizeof(PAEIF_CRYPTOOP));
    status = PaeIpsecAddSA(said, op);
    if (status) {
        printk(KERN_LEVEL "PaeIpsecAddSA returned %d\n", status);
        goto error;
    }

    tlv = (TLVWORD *)buff;

    /* Check for tunnel mode and add TLV for policy payload */
    if ((said->flags.dir == PAEIF_DIR_OUTBOUND)
                && (x->props.mode == XFRM_MODE_TUNNEL)) {
        PAEIF_TUNNEL *tun =  (PAEIF_TUNNEL *)(buff + sizeof(TLVWORD));
		ptr = (uint8_t *)tun + sizeof(TUNNELFLAGS);
        if (said->flags.ipv4) {
            if (x->props.saddr.a4 || x->id.daddr.a4) {
                /* Create IPV4 policy payload */
                tun->flags.ipv4 = 1;
				memcpy(ptr, ipv4_tunnel_preset, IPV4_TUNNEL_PRESET_LEN);  
				*(ptr + 9) = x->id.proto;
				ptr += IPV4_TUNNEL_PRESET_LEN;
                memcpy((void*)ptr, &x->props.saddr.a4, SIZE_IPV4_ADDR);
                memcpy(ptr + SIZE_IPV4_ADDR, &x->id.daddr.a4, SIZE_IPV4_ADDR);
				tlv->length = sizeof(TUNNELFLAGS) + SIZE_IPV4_HEADER + sizeof(TLVWORD);
            } 
        } else if (said->flags.ipv6) {
            if (x->props.saddr.a6 || x->id.daddr.a6) {
                /* Create IPV6 policy payload */
                tun->flags.ipv6 = 1;
				memcpy(ptr, ipv6_tunnel_preset, IPV6_TUNNEL_PRESET_LEN);  
				*(ptr + 7) = x->id.proto;
				ptr += IPV6_TUNNEL_PRESET_LEN;
                memcpy(ptr, (void *)x->props.saddr.a6, SIZE_IPV6_ADDR);
                memcpy(ptr + SIZE_IPV6_ADDR, (void *)x->id.daddr.a6, SIZE_IPV6_ADDR);
                tlv->length = sizeof(TUNNELFLAGS) + SIZE_IPV6_HEADER + sizeof(TLVWORD);
            } 
        }
        tlv->type = TLV_TUNNEL_PAYLOAD;
        /* Any updates to send after the SA */
		
		//DataDump ("TUNNEL", tlv, tlv->length);
        status =  PaeIpsecAddPayload(said, tlv);
        if (status) {
            printk(KERN_LEVEL "PaeIpsecAddPayload returned %d\n", status);
            goto error;
        }
        /* Reset the buffer and tlv */
        memset((void *)buff, 0, BUFF_SIZE);
        tlv = (TLVWORD *)buff;
    }

#ifdef USE_IPSEC_POLICY
    /* Check for policy and add TLV for policy payload */
    if (said->flags.ipv4) {
        PAEIF_SPD *pol = (PAEIF_SPD *)(buff + sizeof(TLVWORD));
        if (x->props.saddr.a4 || x->id.daddr.a4) {
            /* Create IPV4 SPD payload for an IPv4 SA */
            pol->v4.flags.ipv4 = 1;
            if (x->props.saddr.a4) {
                *(uint32_t *)(pol->v4.srcAddr) = htonl(x->props.saddr.a4);
                pol->v4.flags.saddr_en = 1;
                if (x->sel.prefixlen_s < 32) {
                    pol->v4.flags.saddr_en_mask = 1;
                    pol->v4.flags.saddr_mask_bits = x->sel.prefixlen_s;
                }
            }
            if (x->id.daddr.a4) {
                *(uint32_t*)(pol->v4.dstAddr) = htonl(x->id.daddr.a4);
                pol->v4.flags.daddr_en = 1;
                if (x->sel.prefixlen_d < 32) {
                    pol->v4.flags.daddr_en = 1;
                    pol->v4.flags.daddr_en_mask = 1;
                    pol->v4.flags.daddr_mask_bits = x->sel.prefixlen_d;
                }
            }
            tlv->length = sizeof(PAEIF_SPDV4) + sizeof(TLVWORD);
            tlv->type = TLV_POLICY_PAYLOAD;

            /* Any updates to send after the SA */
			//DataDump ("SPD", tlv, tlv->length);
            status =  PaeIpsecAddPayload(said, tlv);
            if (status) {
                   printk(KERN_LEVEL "PaeIpsecAddPayload returned %d\n", status);
                goto error;
            }
            /* Reset the buffer and tlv */
            memset((void *)buff, 0, BUFF_SIZE);
            tlv = (TLVWORD *)buff;
        }
    }

    if (said->flags.ipv6) {
        PAEIF_SPD *pol = (PAEIF_SPD *)(buff + sizeof(TLVWORD));
        if (x->props.saddr.a6 || x->id.daddr.a6) {
            /* Create IPV6 SPD payload for an IPv6 SA */
            pol->v4.flags.ipv6 = 1;
            if (x->props.saddr.a6) {
                memcpy((void *)pol->v6.srcAddr, (void *)x->props.saddr.a6, SIZE_IPV6_ADDR);
                p = (uint32_t *)pol->v6.srcAddr;
                for (n = 0; n < (SIZE_IPV6_ADDR/SIZE_IPV4_ADDR); n++, p++) 
                    HTONL(*p);
                pol->v6.flags.saddr_en = 1;
                if (x->sel.prefixlen_s < 32) {
                    pol->v6.flags.saddr_en_mask = 1;
                    pol->v6.flags.saddr_mask_bits = x->sel.prefixlen_s;
                }
            }
            if (x->id.daddr.a6) {
                memcpy((void *)pol->v6.dstAddr, (void *)x->id.daddr.a6, SIZE_IPV6_ADDR);
                p = (uint32_t *)pol->v6.dstAddr;
                for (n = 0; n < (SIZE_IPV6_ADDR/SIZE_IPV4_ADDR); n++, p++) 
                    HTONL(*p);
                pol->v6.flags.daddr_en = 1;
                if (x->sel.prefixlen_d < 32) {
                    pol->v6.flags.daddr_en = 1;
                    pol->v6.flags.daddr_en_mask = 1;
                    pol->v6.flags.daddr_mask_bits = x->sel.prefixlen_d;
                }
            }
            tlv->length = sizeof(PAEIF_SPDV6) + sizeof(TLVWORD);
            tlv->type = TLV_POLICY_PAYLOAD;

            /* Any updates to send after the SA */
			//DataDump ("SPD", tlv, tlv->length);
            status =  PaeIpsecAddPayload(said, tlv);
            if (status) {
                   printk(KERN_LEVEL "PaeIpsecAddPayload returned %d\n", status);
                goto error;
            }
            /* Reset the buffer and tlv */
            memset((void *)buff, 0, BUFF_SIZE);
            tlv = (TLVWORD *)buff;
        } 
    }
#endif /* USE_IPSEC_POLICY */

    /* Check for UDP encapsulation for IPv4 only */
    if ((said->flags.ipv4) && (x->encap)) {
        struct udphdr *uh = NULL;
        printk (KERN_LEVEL "found esp encaptype %d\n", x->encap->encap_type);
        switch (x->encap->encap_type) {
#ifdef USE_IPSEC_NAT_TRAVERSAL
        case UDP_ENCAP_ESPINUDP_NON_IKE:
        case UDP_ENCAP_ESPINUDP:
            uh = (struct udphdr *)(buff + sizeof(TLVWORD));
            uh->source = x->encap->encap_sport;
            uh->dest = x->encap->encap_dport;
            tlv->type = TLV_UDP_PAYLOAD;
            tlv->length = sizeof(struct udphdr) + sizeof(TLVWORD);

            /* Any updates to send after the SA */
			//DataDump ("TLV3", tlv, tlv->length);
            status =  PaeIpsecAddPayload(said, tlv);
            if (status) {
                printk(KERN_LEVEL "PaeIpsecAddPayload returned %d\n", status);
                goto error;
            }
            /* Reset the buffer and tlv */
            memset((void *)buff, 0, BUFF_SIZE);
            tlv = (TLVWORD *)buff;
            break;
#endif /* USE_IPSEC_NAT_TRAVERSAL */

#ifdef USE_IPSEC_L2TP_ENCAP
        case UDP_ENCAP_L2TPINUDP:
            /* TBD */
            break;
#endif
        }
    }

error:
    if (status) {
	    printk(KERN_LEVEL "%s returned %d\n", __func__, status);
	}
    if (buff)
        kfree (buff);
    return status;
}

EXPORT_SYMBOL(DataDump);
EXPORT_SYMBOL(Dump_skb);
EXPORT_SYMBOL(PaeHostIpsecInit);
EXPORT_SYMBOL(PaeHostIpsecShutdown);
EXPORT_SYMBOL(PaeHostIpsecAddSA);
EXPORT_SYMBOL(PaeHostIpsecDeleteSA);
EXPORT_SYMBOL(PaeHostIpsecListSA);
