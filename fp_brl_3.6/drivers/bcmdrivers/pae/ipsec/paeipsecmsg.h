/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *
 */
#ifndef paeipsecmsg_H_
#define paeipsecmsg_H_

int PaeIpsecInit(void);
int PaeIpsecShutdown(void);
int PaeIpsecAddSA(PAEIF_SAID *sa_id, PAEIF_CRYPTOOP *op);
int PaeIpsecAddPayload(PAEIF_SAID *sa_id, TLVWORD *tlv);
int PaeIpsecDeleteSA(PAEIF_SAID *sa_id);
int PaeIpsecGetSAStats(PAEIF_SAID *sa_id);
int PaeIpsecGetStats(PAE_IPSEC_STATS *stats);
int PaeIpsecListSA(PAEIF_SAID *sa_id, int *maxLen);

#endif /* paeipsecmsg_H_ */
