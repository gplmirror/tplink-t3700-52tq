/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *
 */
#include <linux/err.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <net/xfrm.h>
#include <linux/pfkeyv2.h>
#include <linux/random.h>

#include "pae_spu.h"
#include "pae_ipsec.h"
#include "paemsg.h"
#include "paemgmt.h"
#include "paehost.h"
#include "paeipsecmsg.h"
#include "paemsghost.h"
#include "pae_cmds.h"
#include "pae_shared.h"

#define BUFF_SIZE        1024

//=================== Linux IPsec Stack PAE Interface =====================

#define SA_PENDING_SPD_PAYLOAD       1
#define SA_PENDING_TUNNEL_PAYLOAD    2
#define SA_PENDING_NAT_PAYLOAD       3
#define SA_PENDING_BRIDGE_PAYLOAD    4 

#define PAE_IPSEC_ACTION			2

typedef struct pae_sa_node_t PAESA_NODE; 

struct pae_sa_node_t {
    PAEIF_SAID said;
    uint8_t sactx[MAX_SA_LEN];
    int sa_length;
    uint32_t payload_flags;
    PAESA_NODE *next;
};

static PAESA_NODE *pae_sa_pending = NULL;

static PAESA_NODE * AddSANode(PAEIF_SAID *said, PAESA_NODE *root_node) 
{
    PAESA_NODE *new_node = NULL;
    PAESA_NODE *sa_node = root_node;

    new_node = PAE_ALLOC (sizeof(PAESA_NODE));
    memset ((void *)new_node, 0, sizeof(PAESA_NODE));
    if (!new_node) {
        printk(KERN_LEVEL "Cannot allocate memory");
        return NULL;
    }
    memcpy(&new_node->said, said, sizeof(PAEIF_SAID));
    new_node->sa_length = sizeof(PAEIF_SAID);
    new_node->next = NULL;
    new_node->payload_flags = 0;
    if (said->flags.tunnel) {
        new_node->payload_flags |= SA_PENDING_TUNNEL_PAYLOAD;
    }
    if (said->flags.spd) {
        new_node->payload_flags |= SA_PENDING_SPD_PAYLOAD;
    }
    if (said->flags.nat) {
        new_node->payload_flags |= SA_PENDING_NAT_PAYLOAD;
    }
    if (said->flags.bridge) {
        new_node->payload_flags |= SA_PENDING_BRIDGE_PAYLOAD;
    }

    /* insert it in the linked list of pending SA  */
    if (pae_sa_pending == NULL) {
        /* insert at the front */
        pae_sa_pending = new_node;    
    } else {
        while (sa_node->next) {
            sa_node = sa_node->next;
        }
        sa_node->next = new_node;
    }
    return new_node;
}

static PAESA_NODE * AddSAPending(PAEIF_SAID *said) {
    return AddSANode (said, pae_sa_pending);
}

static int CheckSAIDMatch(PAEIF_SAID *sa_id1, PAEIF_SAID *sa_id2)
{
    if ((sa_id1->spi == sa_id2->spi) 
            && (sa_id1->flags.esp == sa_id2->flags.esp)
            && (sa_id1->flags.ah == sa_id2->flags.ah)) {
        return 0;
    }
    return 1;
}

static int CheckSAIDFlagsMatch(PAEIF_SAID *sa_id1, PAEIF_SAID *sa_id2)
{
    if (sa_id1->flags.esp != sa_id2->flags.esp) 
        return 1;
    if (sa_id1->flags.ah != sa_id2->flags.ah) 
        return 1;
    if (sa_id1->flags.ipv4 != sa_id2->flags.ipv4) 
        return 1;
    if (sa_id1->flags.ipv6 != sa_id2->flags.ipv6) 
        return 1;
    if (sa_id1->flags.inbound != sa_id2->flags.inbound) 
        return 1;
    if (sa_id1->flags.tunnel != sa_id2->flags.tunnel) 
        return 1;
    if (sa_id1->flags.auth != sa_id2->flags.auth) 
        return 1;
    if (sa_id1->flags.encr != sa_id2->flags.encr) 
        return 1;
    if (sa_id1->flags.esn != sa_id2->flags.esn) 
        return 1;
    if (sa_id1->flags.spd != sa_id2->flags.spd) 
        return 1;
    if (sa_id1->flags.nat != sa_id2->flags.nat) 
        return 1;
    if (sa_id1->flags.bridge != sa_id2->flags.bridge) 
        return 1;
    if (sa_id1->flags.l2tp != sa_id2->flags.l2tp) 
        return 1;
    return 0;
}

static PAESA_NODE * GetSAPending(PAEIF_SAID *sa_id)
{
    PAESA_NODE *sa_node = pae_sa_pending;

    while (sa_node) {
        if (!CheckSAIDMatch(sa_id, &sa_node->said)) {
            /* This is the one we want */
            return sa_node;
        }
        sa_node = sa_node->next;
    }
    return NULL;
}

static int RemoveSAPending(PAESA_NODE *sa_pending)
{
    PAESA_NODE *sa_node = pae_sa_pending;

    if (pae_sa_pending == sa_pending) {
        pae_sa_pending = NULL;
        goto done;
    } else {
        while (sa_node->next) {
            if (sa_node->next == sa_pending) {
                sa_node->next = sa_pending->next;
                goto done;
            }
            sa_node = sa_node->next;
        }
    }
    return PAEIF_HOST_ERROR_SA_NOT_FOUND;
done:
    PAE_FREE(sa_pending);
    return 0;
}

static int PaeSendIpsecSA (PAESA_NODE *sa_node)
{
	PAEIF_SA_ACTION sa_action;
    //PAE_IPSEC_SA paesa;
    TLVWORD *tlv = NULL;
    SPUHEADER *spuh;
	uint32_t sa_len = 0;
    uint32_t encap_len;
    uint32_t status = PAE_STATUS_OK;
    uint8_t *ptr;
    int ivSize = 0;
	int n;
	static int idx = 0;

    /* Parse the SA add TLV and create the targt SA */
    memset (&sa_action, 0, sizeof(PAEIF_SA_ACTION));
    tlv = (TLVWORD *)sa_node->sactx;
	if (sa_node->said.flags.ipv4) {
		sa_len = PAEIF_SAID_IPV4_LEN;
	} else if (sa_node->said.flags.ipv6) {
		sa_len = PAEIF_SAID_IPV6_LEN;
	} else { 
        status = PAE_ERROR_SA_IP_VERSION;
		goto error;
	}
    memcpy(&sa_action.said, &sa_node->said, sa_len);

    //DataDump ("sa_node said", &sa_node->said, sizeof(PAEIF_SAID));
    ptr = (uint8_t *)tlv;
    while (tlv->length) {
        //printk(KERN_LEVEL "processsing SA payload type %d\n", tlv->type);
        //DataDump ("sa_action.sactx before", &sa_action.sactx, sizeof(PAE_IPSEC_SA));
        //DataDump ("payload", tlv, tlv->length);
        switch (tlv->type) {
        case TLV_SA_PAYLOAD:
            sa_action.sactx.dma_ctx_len = tlv->length - sizeof(TLVWORD);
            memcpy(&sa_action.sactx + sa_action.sactx.dma_start, ptr + sizeof(TLVWORD), sa_action.sactx.dma_ctx_len);
			sa_len += sa_action.sactx.dma_ctx_len;

			spuh = (SPUHEADER *)&sa_action.sactx + sa_action.sactx.dma_start;
			/* Reserve the location of  BDESC and BD at the end of the spuh */
			sa_action.sactx.bdesc_offset = sa_action.sactx.dma_ctx_len;
			sa_action.sactx.dma_ctx_len += sizeof(BD_HEADER) + sizeof(BDESC_HEADER);
			sa_len += sizeof(BD_HEADER) + sizeof(BDESC_HEADER);
            break;
        case TLV_POLICY_PAYLOAD:
            sa_action.sactx.spd_len = tlv->length - sizeof(TLVWORD);
            memcpy(&sa_action.sactx + sa_action.sactx.spd_offset, ptr + sizeof(TLVWORD), sa_action.sactx.spd_len);
			sa_len += sa_action.sactx.spd_len;
            break;
        case TLV_TUNNEL_PAYLOAD:
            /* copy it as part of the encap header */
			if (!sa_node->said.flags.inbound) {
				/* if we got a tunnel copy it at the end of the spuh */
				ptr = (uint8_t *)&sa_action.sactx + sa_action.sactx.dma_start + sa_action.sactx.dma_ctx_len;
				/* take account of the tunnel flag word */
				memcpy(ptr, (uint8_t *)tlv + 2*sizeof(TLVWORD), tlv->length - 2*sizeof(TLVWORD));
				sa_action.sactx.dma_ctx_len += tlv->length - 2*sizeof(TLVWORD);
				ptr += tlv->length - 2*sizeof(TLVWORD);

				/* add the IPSEC header */
				if (sa_node->said.flags.encr) {
					ivSize = sa_node->said.flags.ivLen;
				}
				/* Write the SPI and start sequence number */
				*(uint32_t*)ptr = sa_action.said.spi;
				ptr += 4;
				*(uint32_t*)ptr = htonl(0x1);
				ptr += 4;
				/* Write the first IV */
				for (n = 0; n < ivSize; n += 4) 
					*(uint32_t*)(ptr + n) = random32();

				/* skip space for SPI, sequence number and IV */
				encap_len = 2 * sizeof(uint32_t) + ivSize;
				sa_action.sactx.dma_ctx_len += encap_len;
				sa_len += encap_len;
			}
            break;
        case TLV_NAT_UDP_PAYLOAD:
            //TBD
            break;
        default:
            printk(KERN_LEVEL "Payload type %d not supported\n",
                    tlv->type);
            status = PAE_ERROR_TLV_TYPE;
            goto error;
        }
         
        //DataDump ("sactx after", &sa_action.sactx, sizeof(PAE_IPSEC_SA));
        /* Move on the next TLV */
        ptr += tlv->length;
        tlv = (TLVWORD *)ptr;
    }

    //status = iProcPaeMessageSend((uint8_t*)&paesa, PAE_MSG_TYPE_IPSEC_ADD_SA, sizeof(PAE_IPSEC_SA), 0);
    status = pae_add_action(idx, PAE_IPSEC_ACTION, (uint8_t*)&sa_action, sa_action.sactx.sa_len + sizeof(PAEIF_SAID));
    if (status) {
       printk(KERN_LEVEL "pae_add_action returned %d\n", status);
	   goto error;
    }
	idx++;
error:
    return status;
}

/*-------------------------------------------------------------------------
 * Name    : PaeIpsecAddPayload
 * Purpose : Process an SA Payload
 * Input   : 
 * Return  : Error status
 * Remarks : 
 *------------------------------------------------------------------------*/
int PaeIpsecAddPayload(PAEIF_SAID *sa_id, TLVWORD *tlv)
{
    int status = PAEIF_STATUS_OK;
    SPDFLAGS *spd_flags;
    PAEIF_SPD *spd;
    PAEIF_TUNNEL *tun;
    UDP_HEADER *uh;
    void *ptr;
    PAESA_NODE *sa_node;
    uint8_t ip_ver;

    sa_node = GetSAPending(sa_id);
    if (!sa_node) {
        printk(KERN_LEVEL "SA not found\n");
        status = PAEIF_HOST_ERROR_SA_NOT_FOUND;
        goto error;
    }

    if (CheckSAIDFlagsMatch(&sa_node->said, sa_id)) {
        printk(KERN_LEVEL "SA flags mismatch\n");
        status = PAEIF_HOST_ERROR_SA_FLAGS;
        goto error;
    }    

    ptr = &sa_node->said + sa_node->sa_length;
    switch (tlv->type) {
    case TLV_POLICY_PAYLOAD:
        /* Add the policy header */
        spd = (PAEIF_SPD *) ((uint8_t *)tlv + sizeof(TLVWORD));
        spd_flags = (SPDFLAGS *) spd;

        /* Perform sanity checks */
        if (spd_flags->ipv4) {
            if (tlv->length != sizeof(PAEIF_SPDV4) + sizeof(TLVWORD)) {
                goto spd_error;
            }
            /* It is an IPv4 policy */
        } else if (spd_flags->ipv6) {
            if (tlv->length != sizeof(PAEIF_SPDV6) + sizeof(TLVWORD)) {
                goto spd_error;
            }
            /* It is an IPv6 policy */
        } else {
            goto spd_error;
        }
        sa_node->payload_flags &= ~SA_PENDING_SPD_PAYLOAD;
        break;

    case TLV_TUNNEL_PAYLOAD:
        /* Add the tunnel header */
        tun = (PAEIF_TUNNEL *)((uint8_t *)tlv + sizeof(TLVWORD));
        ip_ver = (*((uint8_t *)tun + sizeof(TUNNELFLAGS))) & 0xF0 >> 2;
        /* Perform sanity checks */
        if (ip_ver == IP_VERSION_4) {
            if (tlv->length != sizeof(TUNNELFLAGS) + SIZE_IPV4_HEADER + sizeof(TLVWORD)) {
                goto tunnel_error;
            }
            /* It is an IPv4 tunnel */
        } else if (ip_ver == IP_VERSION_6) {
            if (tlv->length != sizeof(TUNNELFLAGS) + SIZE_IPV6_HEADER + sizeof(TLVWORD)) {
                goto tunnel_error;
            /* It is an IPv6 tunnel */
            }
        } else {
            goto tunnel_error;
        }
        sa_node->payload_flags &= ~SA_PENDING_TUNNEL_PAYLOAD;
        break;

    case TLV_NAT_UDP_PAYLOAD:
        /* Add the udp header */
        if (!sa_id->flags.ipv4) {
            printk(KERN_LEVEL "UDP NAT-Traversal is supported only in IPv4\n");
            status = PAEIF_HOST_ERROR_INPUT;
            goto nat_error;
        }
        uh = (UDP_HEADER *) ((uint8_t *)tlv + sizeof(TLVWORD));
        if (tlv->length != sizeof(UDP_HEADER)) {
            goto nat_error;
        }
        if (!uh->udp_src || !uh->udp_dst) {
            goto nat_error;
        }
        sa_node->payload_flags &= ~SA_PENDING_NAT_PAYLOAD;
        break;

    case TLV_EHTERIP_PAYLOAD:
        /* payload is opaque, no error checking */
        // TBD
        sa_node->payload_flags &= ~SA_PENDING_BRIDGE_PAYLOAD;
        break;
    }
    //DataDump ("payload", tlv, tlv->length);
    ptr = (uint8_t *)sa_node + sa_node->sa_length;
    memcpy ((void *)ptr, tlv, tlv->length);  
    sa_node->sa_length += tlv->length;
    //DataDump ("message after payload", sa_node, sa_node->sa_length);
    /* check if ready to send as is */
    if (!sa_node->payload_flags) {
       status = PaeSendIpsecSA (sa_node);
       if (status) {
           printk(KERN_LEVEL "PaeSendIpsecSA returned %d\n", status);
		   goto error;
       }
       RemoveSAPending (sa_node);
    }
    return 0;

spd_error:
    printk(KERN_LEVEL "SPD payload error\n");
    status = PAEIF_HOST_ERROR_SPD_PAYLOAD;
    goto error;

tunnel_error:
    printk(KERN_LEVEL "Tunnel payload error\n");
    status = PAEIF_HOST_ERROR_TUNNEL_PAYLOAD;
    goto error;

nat_error:
    printk(KERN_LEVEL "NAT-Traversal payload error\n");
    status = PAEIF_HOST_ERROR_NAT_PAYLOAD;
    goto error;

error:
    printk(KERN_LEVEL "%s returned %d\n", __func__, status);
    return status;
}

/*-------------------------------------------------------------------------
 * Name    : PaeIpsecActionSA
 * Purpose : Delete the IPsec action
 * Input   : 
 * Return  : Error status
 * Remarks : 
 *------------------------------------------------------------------------*/
int PaeIpsecDeleteSA(PAEIF_SAID *sa_id)
{
	int status;

    //status = iProcPaeMessageSend((uint8_t*)&paesa, PAE_MSG_TYPE_IPSEC_DELETE_SA, sizeof(PAESA), 0);
    return 0;
}

/*-------------------------------------------------------------------------
 * Name    : PaeIpsecAddSA
 * Purpose : Create the PAE Context for the SA and submit to the PAE 
 * Input   : 
 * Return  : Error status
 * Remarks : 
 *------------------------------------------------------------------------*/
int PaeIpsecAddSA(PAEIF_SAID *sa_id, PAEIF_CRYPTOOP *op)
{
    int status = PAEIF_STATUS_OK;
    uint8_t *buff;
    uint8_t *ptr;
    SPUHEADER *spuh;
    PAESA_NODE *sa_node;
    TLVWORD *tlv;
    uint32_t authAlg = 0;
    uint32_t encrAlg = 0;
    uint32_t protocol_bits = 0;
    uint32_t cipher_bits = 0;
    uint32_t ecf_bits = 0;
	uint8_t  sctx_size = 0;

	if (sa_id->flags.esp) {
		printk(KERN_LEVEL "SA ESP SPI %08x\n", sa_id->spi);
	}

    buff = (uint8_t *)PAE_ALLOC(BUFF_SIZE);
    if (!buff) {
        printk(KERN_LEVEL "Cannot allocate memory");
        status = PAEIF_HOST_ERROR_ALLOC;
        goto error;
    }
    memset(buff, 0, BUFF_SIZE);
    tlv = (TLVWORD*)buff;
    /* SPU header immediately follows the add header */
    spuh = (SPUHEADER *)(buff + sizeof(TLVWORD));
    /* set the message payload header parameters */
    tlv->length = sizeof(SPUHEADER) + sizeof(TLVWORD);
    tlv->type = TLV_SA_PAYLOAD;

    /* format the SPU headers */

    /* format master header word */
    spuh->mh.opCode = SPU_CRYPTO_OPERATION_GENERIC;
    spuh->mh.flags.SCTX_PR = 1;
    spuh->mh.flags.BDESC_PR = 1;
    spuh->mh.flags.BD_PR = 1;


    /* Use the emh to store the SPI of the SA */
    spuh->emh = sa_id->spi;

    /* Format sctx word 0 */
    if (sa_id->flags.esp) {
        protocol_bits |= 1 << PROTOCOL_ESP_SHIFT;
    }
    if (sa_id->flags.ah) {
        protocol_bits  |= 1 << PROTOCOL_AH_SHIFT;
    }
    protocol_bits |= SPU_SCTX_TYPE_GENERIC << SCTX_TYPE_SHIFT;
    sctx_size = 3; /* size so far in words */

    if (sa_id->flags.tunnel) {
        protocol_bits |= 1 << CAP_EN_SHIFT;
        protocol_bits |= 1 << PAD_EN_SHIFT;
//        protocol_bits |= 1 << GEN_ID_SHIFT;
        protocol_bits |= 1 << COPY_TOS_SHIFT;
        protocol_bits |= 1 << COPY_FLOW_SHIFT;
        protocol_bits |= 1 << UPDATE_EN_SHIFT;
	}

    if (!sa_id->flags.tunnel) {
        protocol_bits |= 1 << TRANSPORT_MODE_SHIFT;
    }
    
	if (sa_id->flags.esn) {
        protocol_bits |= 1 << AUTH_SEQ64_SHIFT;
    }

    /* Format sctx word 1 */
    if (sa_id->flags.inbound) {
        cipher_bits |= 1 << CIPHER_INBOUND_SHIFT;
    }

    /* Format sctx word 2 */
    if (sa_id->flags.inbound) {
        ecf_bits |= 1 << CHECK_ICV_SHIFT;
    }

    if (!sa_id->flags.inbound) {
		// Outbound
        ecf_bits |= 1 << INSERT_ICV_SHIFT;
    }

    /* Specify the authentication protocol if any */
    switch (op->authAlg) {
        case PAEIF_AUTH_ALG_NONE:
            break;
        case PAEIF_AUTH_ALG_MD5:
            if (op->authMode != PAEIF_AUTH_MODE_HMAC)
                goto autherror;
            authAlg = AALGINFO_HMAC_MD5;
            break;
        case PAEIF_AUTH_ALG_SHA1:
            if (op->authMode != PAEIF_AUTH_MODE_HMAC)
                goto autherror;
            authAlg = AALGINFO_HMAC_SHA1;
            break;
        case PAEIF_AUTH_ALG_SHA224:
            if (op->authMode != PAEIF_AUTH_MODE_HMAC)
                goto autherror;
            authAlg = AALGINFO_HMAC_SHA224;
            break;
        case PAEIF_AUTH_ALG_SHA256:
            if (op->authMode != PAEIF_AUTH_MODE_HMAC)
                goto autherror;
            authAlg = AALGINFO_HMAC_SHA256;
            break;
        case PAEIF_AUTH_ALG_AES:
            if (op->authMode == PAEIF_AUTH_MODE_XCBC)
                authAlg = AALGINFO_XCBC_MAC;
            else if (op->authMode == PAEIF_AUTH_MODE_GMAC) {
                switch (op->authKeyLen) {
                case 128:
                    authAlg = AALGINFO_AES128_GMAC;
                    break;
                case 192:
                    authAlg = AALGINFO_AES192_GMAC;
                    break;
                case 256:
                    authAlg = AALGINFO_AES256_GMAC;
                    break;
                default:
                    goto autherror;
                }
            } else {
                goto autherror;
            }
            break;
        default:
autherror:
            printk(KERN_LEVEL "Unsupported auth algorithm %d auth mode %d\n", op->authAlg, op->authMode);
            status = PAEIF_HOST_ERROR_AUTH_ALG;
            goto error;
    }    

    /* Set the encryption protocol */
    if (sa_id->flags.esp) {    
        switch (op->encrAlg) {
        case PAEIF_ENCR_ALG_NULL:
            encrAlg = EALGINFO_NULL;
           break;
        case PAEIF_ENCR_ALG_DES:
            if (op->encrMode != PAEIF_ENCR_MODE_CBC)
                goto encrerror;
            encrAlg = EALGINFO_DES_CBC;
            break;
        case PAEIF_ENCR_ALG_3DES:
            if (op->encrMode != PAEIF_ENCR_MODE_CBC)
                goto encrerror;
            encrAlg = EALGINFO_3DES_CBC;
            break;
        case PAEIF_ENCR_ALG_AES:
            switch (op->encrMode) {
            case PAEIF_ENCR_MODE_CBC:
                switch (op->encrKeyLen) {
                case 128:
                    encrAlg = EALGINFO_AES128_CBC;
                    break;
                case 192:
                    encrAlg = EALGINFO_AES192_CBC;
                    break;
                case 256:
                    encrAlg = EALGINFO_AES256_CBC;
                    break;
                default:
                    printk(KERN_LEVEL "Unsupported AES key length %d\n", op->encrKeyLen);
                    status = PAEIF_HOST_ERROR_KEY_LENGTH;
                    goto error;
                }
                break;
            case PAEIF_ENCR_MODE_CTR:
                switch (op->encrKeyLen) {
                case 128:
                    encrAlg = EALGINFO_AES128_CTR;
                    break;
                case 192:
                    encrAlg = EALGINFO_AES192_CTR;
                    break;
                case 256:
                    encrAlg = EALGINFO_AES256_CTR;
                    break;
                default:
                    printk(KERN_LEVEL "Unsupported AES key length %d\n", op->encrKeyLen);
                    status = PAEIF_HOST_ERROR_KEY_LENGTH;
                    goto error;
                }
                break;
            case PAEIF_ENCR_MODE_GCM:
                /* Encryption AND authentication algorithm rfc4106 */
                switch (op->encrKeyLen) {
                case 128:
                    encrAlg = EALGINFO_AES128_GCM;
                    break;
                case 192:
                    encrAlg = EALGINFO_AES192_GCM;
                    break;
                case 256:
                    encrAlg = EALGINFO_AES256_GCM;
                    break;
                default:
                    printk(KERN_LEVEL "Unsupported AES key length %d\n", op->encrKeyLen);
                    status = PAEIF_HOST_ERROR_KEY_LENGTH;
                    goto error;
                }
                break;
            default:
                goto encrerror;
            }
			break;
                
        default:
encrerror:
            printk(KERN_LEVEL "Unsupported encr alg %d mode %d\n", op->encrAlg, op->encrMode);
            status = PAEIF_HOST_ERROR_ENCR_ALG;
            goto error;
        }
    }	

    ptr = (uint8_t *)spuh + sizeof(SPUHEADER);

    /* Set the auth parameters in the cipher.flags */
    switch (authAlg) {
    case AALGINFO_NONE:
        /* Do nothing here */
        break;

#ifdef USE_FHMAC_MODE
        /* FHMAC uses inner and outer contexts not keys */
    case AALGINFO_FHMAC_MD5:
        cipher_bits |= HASH_ALG_MD5 << HASH_ALG_SHIFT;
        cipher_bits |= HASH_MODE_FHMAC << HASH_MODE_SHIFT;
        cipher_bits |= HASH_TYPE_FULL << HASH_TYPE_SHIFT;
        break 
    case AALGINFO_FHMAC_SHA1:
        cipher_bits |= HASH_ALG_SHA1 << HASH_ALG_SHIFT;
        cipher_bits |= HASH_MODE_FHMAC << HASH_MODE_SHIFT;
        cipher_bits |= HASH_TYPE_FULL << HASH_TYPE_SHIFT;
        break;
#endif
        /* All other HMAC mode */
    case AALGINFO_HMAC_SHA1:
        cipher_bits |= HASH_ALG_SHA1 << HASH_ALG_SHIFT;
        cipher_bits |= HASH_MODE_HMAC << HASH_MODE_SHIFT;
        cipher_bits |= HASH_TYPE_FULL << HASH_TYPE_SHIFT;
        break;
    case AALGINFO_HMAC_MD5:
        cipher_bits |= HASH_ALG_MD5 << HASH_ALG_SHIFT;
        cipher_bits |= HASH_MODE_HMAC << HASH_MODE_SHIFT;
        cipher_bits |= HASH_TYPE_FULL << HASH_TYPE_SHIFT;
        break;
    case AALGINFO_HMAC_SHA224:
        cipher_bits |= HASH_ALG_SHA224 << HASH_ALG_SHIFT;
        cipher_bits |= HASH_MODE_HMAC << HASH_MODE_SHIFT;
        cipher_bits |= HASH_TYPE_FULL << HASH_TYPE_SHIFT;
        break;
    case AALGINFO_HMAC_SHA256:
        cipher_bits |= HASH_ALG_SHA256 << HASH_ALG_SHIFT;
        cipher_bits |= HASH_MODE_HMAC << HASH_MODE_SHIFT;
        cipher_bits |= HASH_TYPE_FULL << HASH_TYPE_SHIFT;
        break;
    default:
        printk(KERN_LEVEL "Unsupported auth algorithm %d\n", authAlg);
        status = PAEIF_HOST_ERROR_AUTH_ALG;
        goto error;
        break;
    }
    
	/* Write the authentication key material */
    switch (authAlg) {
    case AALGINFO_NONE:
        /* Do nothing here */
        break;

#ifdef USE_FHMAC_MODE
        /* FHMAC uses inner and outer contexts not keys */
    case AALGINFO_FHMAC_MD5:
    case AALGINFO_FHMAC_SHA1:
        create_hmac_context(op->authKey, ptr, HMAC_INNER_CONTEXT);
        ptr += op->authKeyLen/8;
        create_hmac_context(x, ptr, HMAC_OUTER_CONTEXT);
        ptr += op->authKeyLen/8;
        tlv->length += 2 * op->authKeyLen/8;
        sctx_size += 2 * op->authKeyLen/32;
        ecf_bits |= 3 << ICV_SIZE_SHIFT;  
        break;
#endif
        /* All other HMAC mode */
    case AALGINFO_HMAC_SHA1:
    case AALGINFO_HMAC_MD5:
    case AALGINFO_HMAC_SHA224:
    case AALGINFO_HMAC_SHA256:
        memcpy(ptr, op->authKey, op->authKeyLen/8);
        ptr += op->authKeyLen/8;
        sctx_size += op->authKeyLen/32;
        ecf_bits |= 3 << ICV_SIZE_SHIFT;  
        tlv->length += op->authKeyLen/8;
        break;

    default:
        printk(KERN_LEVEL "Unsupported crypto algorithm %d\n", encrAlg);
        status = PAEIF_HOST_ERROR_AUTH_ALG;
        goto error;
        break;
    }

    /* Set the crypto parameters in the cipher.flags */
    switch (encrAlg) {
    case EALGINFO_NULL:
        /* Do nothing here */
        break;

    case EALGINFO_DES_CBC:
        cipher_bits |= CIPHER_ALG_DES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CBC << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_NONE << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_3DES_CBC:
        cipher_bits |= CIPHER_ALG_3DES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CBC << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_NONE << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES128_CBC:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CBC << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES128 << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES192_CBC:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CBC << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES192 << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES256_CBC:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CBC << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES256 << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES128_CTR:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CTR << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES128 << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES192_CTR:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CTR << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES192 << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES256_CTR:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_CTR << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES256 << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES128_GCM:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_GCM << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES128 << CIPHER_TYPE_SHIFT;
        break; 
    case EALGINFO_AES192_GCM:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_GCM << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES192 << CIPHER_TYPE_SHIFT;
        break;
    case EALGINFO_AES256_GCM:
        cipher_bits |= CIPHER_ALG_AES << CIPHER_ALG_SHIFT;
        cipher_bits |= CIPHER_MODE_GCM << CIPHER_MODE_SHIFT;
        cipher_bits |= CIPHER_TYPE_AES256 << CIPHER_TYPE_SHIFT;
        break;
    }

    /* copy the encryption keys in the SAD entry */
    if (encrAlg !=  EALGINFO_NULL) {
        memcpy(ptr, op->encrKey, op->encrKeyLen/8);
        ptr += op->encrKeyLen/8;
        sctx_size += op->encrKeyLen/32;
        tlv->length += op->encrKeyLen/8;
        if (op->encrMode == PAEIF_ENCR_MODE_CTR) {
			/* AES CTR structure follows keyes */
			sctx_size += CTR_IV_SIZE/4;  
			tlv->length += CTR_IV_SIZE;
			/* Create the counter NONCE value */
			*(uint32_t*)ptr = random32();
			ptr += 4;
			/* initialize the first IV */
			*(uint32_t*)ptr = random32();
			ptr += 4;
			*(uint32_t*)ptr = random32();
			ptr += 4;
			/* Initialize the counter value */
			*(uint32_t *)ptr = 1;  
			ptr += 4;
		}
    }
    protocol_bits |= sctx_size;
	spuh->sa.protocol.bits = protocol_bits;
	spuh->sa.cipher.bits = cipher_bits;
	spuh->sa.ecf.bits = ecf_bits;

    sa_node = GetSAPending (sa_id);
    if (!sa_node) {
        /* This is a new SA */
        sa_node = AddSAPending (sa_id);
        if (!sa_node) {
            status = PAEIF_HOST_ERROR_ALLOC;
            goto error;
        }
        memcpy(&sa_node->said, sa_id, sizeof(PAEIF_SAID));
        memcpy(&sa_node->sactx, tlv, tlv->length);
        sa_node->sa_length += tlv->length;
        //DataDump ("Initial SA", &sa_node->said, sa_node->sa_length);
    }
    /* check if ready to send as is */
    if (!sa_node->payload_flags) {
        status = PaeSendIpsecSA (sa_node);
        if (status) {
            printk(KERN_LEVEL "PaeSendIpsecSA returned %d\n", status);
			goto error;
        }
        RemoveSAPending (sa_node);
    }

error:
    if (status) 
        printk(KERN_LEVEL "%s is returning %d\n", __func__, status);
    if (buff)
        PAE_FREE (buff);
    return status;
}

EXPORT_SYMBOL(PaeIpsecAddSA);
EXPORT_SYMBOL(PaeIpsecAddPayload);
EXPORT_SYMBOL(PaeIpsecDeleteSA); 
