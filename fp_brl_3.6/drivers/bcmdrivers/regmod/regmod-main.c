
/*
 * regmod.c 
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

static int regmod_init(void)
{
	printk(KERN_ALERT "regmod_init\n");
	return 0;
}
static int regmod_exit(void)
{
	printk(KERN_ALERT "regmod_exit\n");
	return 0;
}
module_init(regmod_init);
module_exit(regmod_exit);
