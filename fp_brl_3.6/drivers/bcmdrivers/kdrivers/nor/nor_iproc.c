/*
 * NOR flash Kernel driver for Hurricane2 - DeerHound eval board
 *
 * (C) 2013 Haifeng Jiang <haifengj@broadcom.com>
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/slab.h>

#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <mach/hardware.h>
#include <asm/io.h>
#include <linux/delay.h>

#include "smc-reg.h"

#define S29GL_FLASH_SIZE 0x2000000
#define S29GL_FLASH_PHYS 0x20000000

static struct mtd_info *nor_mtd;

static struct map_info s29gl_map = {
	.name =		"S29GL",
	.bankwidth =	4,
	.size =		S29GL_FLASH_SIZE,
	.phys =		S29GL_FLASH_PHYS,
};

static struct mtd_partition s29gl_partitions[] = {
        {       /* 768 KB */
                .name = "boot",
                .size = 0x00C0000,
                .offset = 0,
                .mask_flags = MTD_WRITEABLE
        },{
                /* 128 KB */
                .name = "env",
                .size = 0x20000,
                .offset = 0xc0000,
                .mask_flags = MTD_WRITEABLE
        },{
                /* 128 KB */
                .name = "shmoo",
                .size = 0x20000,
                .offset = 0xe0000,
                .mask_flags = MTD_WRITEABLE
        },{
                /* 21 MB */
                .name = "rootfs",
                .size = 0x1500000,
                .offset = 0x100000,
                .mask_flags = MTD_WRITEABLE
        },{
                /* 10 MB */
                .name = "image",
                .size = MTDPART_SIZ_FULL,
                .offset = MTDPART_OFS_APPEND,
                .mask_flags = MTD_WRITEABLE
        }
};

#define NUM_PARTITIONS ARRAY_SIZE(s29gl_partitions)

volatile void __iomem *nor_io_base;

#define IO_ADDRESS(x) (int *)((int)nor_io_base + x)

/*
 * Initialize FLASH support
 */
static int __init s29gl_mtd_init(void)
{
    volatile int *nor_enable;

    nor_enable = ioremap(0x1803fc3c, 4);
    *nor_enable |= 0x0000000C;

    printk(KERN_INFO "S29GL-MTD: NOR_INTERFACE Enabled\n");

    udelay(1000);

    s29gl_map.virt = ioremap(s29gl_map.phys, s29gl_map.size);

    if (!s29gl_map.virt) {
        printk(KERN_ERR "S29GL-MTD: ioremap failed\n");
    	*nor_enable &= 0x00000003;	// revert to NAND mode
        return -EIO;
    }

    simple_map_init(&s29gl_map);

    // Probe for flash bankwidth 4
    printk (KERN_INFO "S29GL-MTD probing 32bit FLASH\n");
    nor_mtd = do_map_probe("cfi_probe", &s29gl_map);
    if (!nor_mtd) {
        printk (KERN_INFO "S29GL-MTD probing 16bit FLASH\n");
        // Probe for bankwidth 2
        s29gl_map.bankwidth = 2;
        nor_mtd = do_map_probe("cfi_probe", &s29gl_map);
    }

    if (nor_mtd) {
        nor_mtd->owner = THIS_MODULE;
        mtd_device_parse_register(nor_mtd, NULL, NULL,
					  s29gl_partitions, NUM_PARTITIONS);
        printk (KERN_INFO "S29GL-MTD MTD partitions parsed!\n");
	return 0;
    }

    printk (KERN_INFO "S29GL-MTD NO FLASH found!\n");
    *nor_enable &= 0x00000003;	// revert to NAND mode
    iounmap((void *)s29gl_map.virt);
    return -ENXIO;
}

/*
 * Cleanup
 */
static void __exit s29gl_mtd_cleanup(void)
{

    if (nor_mtd) {
        mtd_device_unregister(nor_mtd);
        map_destroy(nor_mtd);
    }

    if (s29gl_map.virt) {
        iounmap((void *)s29gl_map.virt);
        s29gl_map.virt = 0;
    }
}


module_init(s29gl_mtd_init);
module_exit(s29gl_mtd_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Haifeng Jiang <haifengj@broadcom.com>");
MODULE_DESCRIPTION("MTD map driver for Hurricane2 Deerhound evaluation boards");
