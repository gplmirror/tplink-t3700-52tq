/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * TDM interface to Iproc
 */
 
#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <typedefs.h>
#include <osl.h>
#include <bcmutils.h>
#include <siutils.h>
#include <sbhnddma.h>
#include <hnddma.h>
#include <hndsoc.h>
#include "i2s_regs.h"
#include "iproc-tdm.h"
#include <mach/reg_utils.h>
#include <linux/io.h>
#include <mach/io_map.h>

void __iomem *baseAddr;
iproc_tdm_info_t *tdm_info;

#define DMAREG(a, direction, fifonum)	( \
	(direction == DMA_TX) ? \
                (void *)(uintptr)&(a->regs->dmaregs[fifonum].dmaxmt) : \
                (void *)(uintptr)&(a->regs->dmaregs[fifonum].dmarcv))
				

int iproc_i2s_configure(void)
{
	uint32_t deviceControl, clkDivider, stxControl, i2sControl, tdmControl;
		
	// Read the registers
	deviceControl = readl(baseAddr + I2S_DEVCONTROL_REG);
	clkDivider = readl(baseAddr + I2S_CLOCKDIVIDER_REG);
	stxControl = readl(baseAddr + I2S_STXCTRL_REG);
	i2sControl = readl(baseAddr + I2S_CONTROL_REG);
	tdmControl = readl(baseAddr + I2S_TDMCONTROL_REG);

	// Select TDM mode
	deviceControl |= I2S_DC_TDM_SEL;
	// Set to Slave Mode, BITCLK driven by Codec
	deviceControl |= I2S_DC_BCLKD_IN;	
	// Set to full-duplex
	deviceControl |= 0x300;						// Full duplex

	// Setup TDM Control Register

	// Write DeviceControl, STX Control registers
	writel(deviceControl, baseAddr + I2S_DEVCONTROL_REG);
	writel(stxControl, baseAddr + I2S_STXCTRL_REG);
	writel(i2sControl, baseAddr + I2S_CONTROL_REG);
	
	DBG("%s: deviceControl: 0x%x\n", __FUNCTION__, readl(baseAddr + I2S_DEVCONTROL_REG));
	DBG("%s: stxControl 0x%x\n", __FUNCTION__, readl(baseAddr + I2S_STXCTRL_REG));
	DBG("%s: i2sControl 0x%x\n", __FUNCTION__, readl(baseAddr + I2S_CONTROL_REG));
	DBG("%s: tdmControl 0x%x\n", __FUNCTION__, readl(baseAddr + I2S_TDMCONTROL_REG));
	
	return 0;
}


int iproc_i2s_attach(struct platform_device *pdev)
{
	int retVal;
	uint32_t idm_i2s_reset_status_reg, sku_id;
	osl_t *osh = NULL;
	int dma_attach_err = 0;
	
	void __iomem *i2s_m0_idm_io_control_direct    		= IOMEM(HW_IO_PHYS_TO_VIRT(I2S_M0_IDM_IO_CONTROL_DIRECT));
	void __iomem *i2s_m0_idm_io_reset_control    		= IOMEM(HW_IO_PHYS_TO_VIRT(I2S_M0_IDM_RESET_CONTROL));
	void __iomem *i2s_m0_idm_io_reset_control_status    = IOMEM(HW_IO_PHYS_TO_VIRT(I2S_M0_IDM_RESET_STATUS));
	void __iomem *rom_s0_idm_io_status    				= IOMEM(HW_IO_PHYS_TO_VIRT(ROM_S0_IDM_IO_STATUS));
	
	DBG("%s\n", __FUNCTION__);
	
	printk("Checking platform type\n");
	sku_id = readl(rom_s0_idm_io_status);		// 1 - low sku, 2 - medium sku, 0 or 3 - high sku
	printk("sku_id: 0x%x\n", sku_id);
	
	// If platform is not equal to 12, bailing out
    if(sku_id == 1 || sku_id == 2) {
		printk("No I2S block on platform\n");
		return 0;
	}
	
	// Reset I2S block
	printk("Resetting i2s block\n");
	
	// Reset the device
	writel(1, i2s_m0_idm_io_reset_control);
	
	idm_i2s_reset_status_reg = readl(i2s_m0_idm_io_reset_control_status);
	printk("idmResetStatusReg:  %x\n", idm_i2s_reset_status_reg);
	
	printk("Exiting reset\n");
	// Exit Reset the device
	writel(0, i2s_m0_idm_io_reset_control);
	
	baseAddr = IOMEM(IPROC_I2S_REG_VA);
	
	// Create tdm_info structure
	tdm_info = kzalloc(sizeof(iproc_tdm_info_t), GFP_KERNEL);
	if (tdm_info == NULL) {
		DBG("%s: Error allocating tdm_info\n", __FUNCTION__);	
		unregister_chrdev(TDMDRV_MAJOR, TDMDRV_NAME);	
		return -ENOMEM;
	}
			
	tdm_info->irq = BCM5301X_I2S_INTERRUPT;

	DBG("%s: Attaching osl\n", __FUNCTION__);
	osh = osl_attach(pdev, PCI_BUS, FALSE);
	ASSERT(osh);

	tdm_info->osh = osh;
	
	tdm_info->sih = si_attach(0, tdm_info->osh, tdm_info->regsva, PCI_BUS, pdev,
	                        NULL, NULL);
	
	tdm_info->regs = (i2sregs_t *)si_setcore(tdm_info->sih, I2S_CORE_ID, 0);
	//si_core_reset(snd_bcm535x->sih, 0, 0);
	
	DBG("%s: Attaching DMA\n", __FUNCTION__);
									
	tdm_info->di[0] = dma_attach(tdm_info->osh, "i2s_dma", tdm_info->sih,
	                            DMAREG(tdm_info, DMA_TX, 0),
	                            DMAREG(tdm_info, DMA_RX, 0),
	                            64, 64, 512, -1, 0, 0, NULL);

	if (tdm_info->di[0] == NULL)
		DBG("%s: DMA Attach not successfull\n", __FUNCTION__);
	
	dma_attach_err |= (NULL == tdm_info->di[0]);
	
	/* Tell DMA that we're not using framed/packet data */
	dma_ctrlflags(tdm_info->di[0], DMA_CTRL_UNFRAMED /* mask */, DMA_CTRL_UNFRAMED /* value */);
	
	// Configure I2S block
	iproc_i2s_configure();

	return 0;
}

/*
 * Function Name: tdm_open
 * Description  : Called when an user application opens this device.
 * Returns      : 0 - success
 */
static int tdm_open(struct inode *inode, struct file *fp)
{

	return 0;
}

/*
 * Function Name: tdm_handle_ioctl
 * Description  : Main entry point to handle user applications IOCTL requests
 * Returns      : 0 - success or error
 */
static int tdm_handle_ioctl(struct inode *inode, struct file *filep,
				unsigned int command, unsigned long arg)
{

	return 0;
}

/* tdm driver file ops */
static struct file_operations tdm_fops =
{
	.unlocked_ioctl  = tdm_handle_ioctl,
	.open   = tdm_open,
};

static int __devinit iproc_tdm_probe(struct platform_device *pdev)
{
	DBG("%s\n", __FUNCTION__);
	
	// Initialize I2S
	iproc_i2s_attach(pdev);	
	return 0;
}

static int iproc_tdm_remove(struct platform_device *pdev)
{
	DBG("%s\n", __FUNCTION__);
	return 0;
}


static int iproc_tdm_suspend(struct platform_device *pdev, pm_message_t state)
{
	DBG("%s\n", __FUNCTION__);
	return 0;
}

static int iproc_tdm_resume(struct platform_device *pdev)
{
	DBG("%s\n", __FUNCTION__);
	return 0;
}

static u64 iproc_tdm_dmamask = DMA_BIT_MASK(32);

static struct platform_driver iproc_tdm_driver =
{
    .driver = {
        .name = "IPROC_TDM",
        .owner = THIS_MODULE,
    },
	.probe      = iproc_tdm_probe,
	.remove     = iproc_tdm_remove,
	.suspend    = iproc_tdm_suspend,
	.resume     = iproc_tdm_resume,
};

static struct platform_device iproc_tdm_device = {
	.name		=	"IPROC_TDM",
	.id		    =	-1,
	.dev =  {
		.init_name = "IPROC_TDM",
		.dma_mask = &iproc_tdm_dmamask,
		.coherent_dma_mask = DMA_BIT_MASK(32),
	},
};

static int __init iproc_tdm_init(void)
{
	int ret;
	
	DBG("%s\n", __FUNCTION__);
	
	DBG("%s  Registering platform driver\n", __FUNCTION__);	
	ret = platform_driver_register(&iproc_tdm_driver);
	if (ret) {
		DBG("Error registering iproc_tdm_driver %d .\n", ret);
		return ret;
	}
	
	DBG("%s  Registering platform device\n", __FUNCTION__);	
    ret = platform_device_register(&iproc_tdm_device);
	if (ret) {
		DBG("Error registering iproc_tdm_device %d .\n", ret);
	    platform_driver_unregister(&iproc_tdm_driver);
	}
	
	DBG("%s  Registering TDM character device\n", __FUNCTION__);
	ret = register_chrdev(TDMDRV_MAJOR, TDMDRV_NAME, &tdm_fops);
	if (ret) {
		DBG("%s Unable to get major number <%d>\n", __FUNCTION__, TDMDRV_MAJOR);
		return -ENOMEM;
	}
	
	return ret;
}

static void __exit iproc_tdm_exit(void)
{
	DBG("%s\n", __FUNCTION__);
	
	if (tdm_info)
		kfree(tdm_info);
		
	unregister_chrdev(TDMDRV_MAJOR, TDMDRV_NAME);
	platform_device_unregister(&iproc_tdm_device);	
	platform_driver_unregister(&iproc_tdm_driver);	
}

module_init(iproc_tdm_init);
module_exit(iproc_tdm_exit);

/* Module information */
MODULE_DESCRIPTION("IPROC TDM Driver");
MODULE_LICENSE("GPL");
