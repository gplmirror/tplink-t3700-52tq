/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/version.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/clkdev.h>
#include <asm/hardware/gic.h>


#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/irq.h>
#include <linux/input.h>
#include <linux/i2c/tsc2007.h>
#include <linux/spi/spi.h>
#include <mach/hardware.h>
#include <asm/mach/arch.h>
#include <asm/mach-types.h>
#include <mach/sdio_platform.h>
#include <mach/iproc.h>
#include <mach/socregs_ing_open.h>
#include <asm/io.h>
#include <mach/io_map.h>
#include <mach/reg_utils.h>
#include <linux/pwm.h>
#include <linux/amba/bus.h>


#include "northstar.h"
#include "common.h"

#if defined(CONFIG_IPROC_SD) || defined(CONFIG_IPROC_SD_MODULE)
#define IPROC_SDIO_PA   	IPROC_SDIO3_REG_BASE
#define SDIO_CORE_REG_SIZE	0x10000
#define BSC_CORE_REG_SIZE	0x1000
#define SDIO_IDM_IDM_RESET_CONTROL  (0x16800)
#define IPROC_SDIO_IRQ		(177)
#endif

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)
#define  CPLD_RESET_REG  1

extern void brcpld_irq_process_register(void *fptr);
extern void brcpld_irq_process_data(void *fptr);
extern void brcpld_irq_enable_register(void *fptr);
extern void brcpld_irq_loop_enable_register(void *fptr);
extern void brcpld_irq_disable_register(void *fptr);
void m6100_irq_process_fptr(int irqNo, void *ptr);
void cpldI2cAddressDetermine(void);
unsigned char cpldI2cAddress = 0;
s32 m6100_i2c_read_write(u8 address, u8 offset, u8 rd_wr, u8 rd_wr_mode, u8 *data);
struct i2c_adapter *adap;
#endif

/* This is the main reference clock 25MHz from external crystal */
static struct clk clk_ref = {
	.name = "Refclk",
	.rate = 25 * 1000000,   /* run-time override */
	.fixed = 1,
	.type  = 0,
};


static struct clk_lookup board_clk_lookups[] = {
	{
	.con_id         = "refclk",
	.clk            = &clk_ref,
	}
};

extern void __init northstar_timer_init(struct clk *clk_ref);

#if defined(CONFIG_IPROC_SD) || defined(CONFIG_IPROC_SD_MODULE)
/* sdio */
static struct sdio_platform_cfg sdio_platform_data = {
	.devtype = SDIO_DEV_TYPE_SDMMC,
};
static struct resource sdio_resources[] = {
	[0] = {
		.start	= IPROC_SDIO_PA,
		.end	= IPROC_SDIO_PA + BSC_CORE_REG_SIZE - 1,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= IPROC_SDIO_IRQ,
		.end	= IPROC_SDIO_IRQ,
		.flags	= IORESOURCE_IRQ,
	}
};

static struct platform_device board_sdio_device = {
	.name		=	"iproc-sdio",
	.id		=	0,
	.dev		=	{
		.platform_data	= &sdio_platform_data,
	},
	.num_resources  = ARRAY_SIZE(sdio_resources),
  	.resource	= sdio_resources,
};

static void setup_sdio(void)
{
    void __iomem *idm_base;
    struct platform_device *sdio_plat_dev[1];    
    idm_base = (void __iomem *)IPROC_IDM_REGISTER_VA;
    __raw_writel(0, idm_base + SDIO_IDM_IDM_RESET_CONTROL);
    sdio_plat_dev[0] = &board_sdio_device;
    platform_add_devices(sdio_plat_dev, 1);

}
#endif /* CONFIG_IPROC_SD || CONFIG_IPROC_SD_MODULE */

#if defined(CONFIG_IPROC_PWM) || defined(CONFIG_IPROC_PWM_MODULE)
static struct resource iproc_pwm_resources = {
	.start	= IPROC_CCB_PWM_CTL,
	.end	= IPROC_CCB_PWM_CTL + SZ_4K - 1,
	.flags	= IORESOURCE_MEM,
};

static struct platform_device board_pwm_device = {
	.name		= "iproc_pwmc",
	.id	        = -1,
  	.resource	= &iproc_pwm_resources,
  	.num_resources  = 1,
};
static struct pwm_lookup board_pwm_lookup[] = {
    PWM_LOOKUP("iproc_pwmc", 0,"iproc_pwmc","pwm-0"),
    PWM_LOOKUP("iproc_pwmc", 1,"iproc_pwmc","pwm-1"),        
    PWM_LOOKUP("iproc_pwmc", 2,"iproc_pwmc","pwm-2"),
    PWM_LOOKUP("iproc_pwmc", 3,"iproc_pwmc","pwm-3"),

};

#endif /* CONFIG_IPROC_PWM || CONFIG_IPROC_PWM_MODULE */

#if defined(CONFIG_IPROC_WDT) || defined(CONFIG_IPROC_WDT_MODULE)
/* watchdog */
static struct resource wdt_resources[] = {
	[0] = {
		.start	= IPROC_CCA_REG_BASE,
		.end	= IPROC_CCA_REG_BASE + 0x1000 - 1,
		.flags	= IORESOURCE_MEM,
	},
};

static struct platform_device board_wdt_device = {
	.name		=	"iproc_wdt",
	.id		    =	-1,
	.num_resources  = ARRAY_SIZE(wdt_resources),
  	.resource	= wdt_resources,
};
#endif /* CONFIG_IPROC_WDT || CONFIG_IPROC_WDT_MODULE */

#if defined(CONFIG_IPROC_CCB_WDT) || defined(CONFIG_IPROC_CCB_WDT_MODULE)
static AMBA_APB_DEVICE(sp805_wdt, "sp805-wdt", 0x00141805, 
								IPROC_CCB_WDT_REG_BASE, { }, NULL);
#endif

#if defined(CONFIG_IPROC_CCB_TIMER) || defined(CONFIG_IPROC_CCB_TIMER_MODULE)
static struct resource ccb_timer_resources[] = {
	[0] = {
		.start	= IPROC_CCB_TIM0_REG_VA,
		.end	= IPROC_CCB_TIM0_REG_VA + 0x1000 - 1,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= IPROC_CCB_TIM1_REG_VA,
		.end	= IPROC_CCB_TIM1_REG_VA + 0x1000 - 1,
		.flags	= IORESOURCE_MEM,
	},
	[2] = {
		.start	= IPROC_CCB_TIMER_INT_START,
		.end	= IPROC_CCB_TIMER_INT_START + IPROC_CCB_TIMER_INT_COUNT - 1,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device board_timer_device = {
	.name		=	"iproc_ccb_timer",
	.id		    =	-1,
	.num_resources  = ARRAY_SIZE(ccb_timer_resources),
  	.resource	= ccb_timer_resources,
};
#endif /* CONFIG_IPROC_CCB_TIMER || CONFIG_IPROC_CCB_TIMER_MODULE */

#if defined(CONFIG_IPROC_FA2)
#if defined(CONFIG_MACH_NSP)
static struct resource fa2_resources[] = {
    [0] = {
        .start  = CTF_CONTROL_REG, /* Macro is in socregs_nsp.h */
        .end    = CTF_CONTROL_REG + SZ_1K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = 178, 
        .end    = 178,
        .flags  = IORESOURCE_IRQ,
    }
};
#endif
#endif /* CONFIG_IPROC_FA2 */

#ifdef CONFIG_IPROC_I2C
/* SMBus (I2C/BSC) block */

#if defined(CONFIG_MACH_HX4) || defined(CONFIG_MACH_KT2)
/* Helix4 */
#ifdef CONFIG_IPROC_PCT_BOARD
static struct i2c_board_info iproc_pct_i2c_devs __initdata = {
                I2C_BOARD_INFO("m41st85", 0x68),
};
#endif
static struct resource smbus_resources[] = {
    [0] = {
        .start  = ChipcommonB_SMBus_Config, /* Macro is in socregs_hx4.h */
        .end    = ChipcommonB_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = 127, 
        .end    = 127,
        .flags  = IORESOURCE_IRQ,
    }
};

static struct resource smbus_resources1[] = {
    [0] = {
        .start  = ChipcommonB_SMBus1_SMBus_Config, /* Macro is in socregs_hx4.h */
        .end    = ChipcommonB_SMBus1_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM
    },
    [1] = {
        .start  = 128, /* macro in irqs.h (plat-iproc) */
        .end    = 128,
        .flags  = IORESOURCE_IRQ,
    }
};
#elif defined(CONFIG_MACH_NSP)
/* Northstar plus */
static struct resource smbus_resources[] = {
    [0] = {
        .start  = ChipcommonB_SMBus_Config, /* Macro is in socregs_hx4.h */
        .end    = ChipcommonB_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = 121, 
        .end    = 121,
        .flags  = IORESOURCE_IRQ,
    }
};
#else
/* Northstar */
static struct resource smbus_resources[] = {
    [0] = {
        .start  = CCB_SMBUS_START, /* Define this macro is socregs.h, or
                                    in iproc_regs.h */
        .end    = CCB_SMBUS_START + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = BCM_INT_ID_CCB_SMBUS, /* macro in irqs.h (plat-iproc) */
        .end    = BCM_INT_ID_CCB_SMBUS,
        .flags  = IORESOURCE_IRQ,
    }
};
#endif

/* Common to Northstar, Helix4 */
static struct platform_device board_smbus_device = {
    .name= "iproc-smb",
    .id = 0,
    .dev= {
        .platform_data  = NULL, /* Can be defined, if reqd */
    },
    .num_resources = ARRAY_SIZE(smbus_resources),
    .resource = smbus_resources,
};
#if defined(CONFIG_MACH_HX4) || defined(CONFIG_MACH_KT2)
static struct platform_device board_smbus_device1 = {
    .name= "iproc-smb",
    .id = 1,
    .dev= {
        .platform_data  = NULL, /* Can be defined, if reqd */
    },
    .num_resources = ARRAY_SIZE(smbus_resources1),
    .resource = smbus_resources1,
};

#endif /* CONFIG_MACH_HX4 */

#endif /* CONFIG_IPROC_I2C */

#if defined(CONFIG_IPROC_FA2)
#if defined(CONFIG_MACH_NSP)
static struct platform_device board_fa2_device = {
    .name= "fa2",
    .id = 0,
    .dev= {
        .platform_data  = NULL, /* Can be defined, if reqd */
    },
    .num_resources = ARRAY_SIZE(fa2_resources),
    .resource = fa2_resources,
};
#endif
#endif /* CONFIG_IPROC_FA2 */

#ifdef CONFIG_IPROC_USB3H
static struct resource bcm_xhci_resources[] = {
        [0] = {
                .start  = USB30_BASE,
                .end    = USB30_BASE + SZ_4K - 1,
                .flags  = IORESOURCE_MEM,
        },
        [1] = {
                .start  = BCM_INT_ID_USB3H2CORE_USB2_INT0,
                .end    = BCM_INT_ID_USB3H2CORE_USB2_INT0,
                .flags  = IORESOURCE_IRQ,
        },
};

static u64 xhci_dmamask = DMA_BIT_MASK(32);

static struct platform_device bcm_xhci_device = {
	.name		=	"bcm-xhci",
	.id		=	0,
	.dev		=	{
//		 .platform_data	= &xhci_platform_data,
		 .dma_mask       = &xhci_dmamask,
		 .coherent_dma_mask = DMA_BIT_MASK(32),
	},
  	.resource	= bcm_xhci_resources,
	.num_resources  = ARRAY_SIZE(bcm_xhci_resources),
};
#endif /* CONFIG_IPROC_USB3 */

#ifdef CONFIG_USB_EHCI_BCM

static u64 ehci_dmamask = DMA_BIT_MASK(32);

static struct resource usbh_ehci_resource[] = {
	[0] = {
		.start = IPROC_USB20_REG_BASE,
		.end = IPROC_USB20_REG_BASE + 0x0FFF,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = BCM_INT_ID_USB2H2CORE_USB2_INT,
		.end = BCM_INT_ID_USB2H2CORE_USB2_INT,
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device usbh_ehci_device =
{
	.name = "bcm-ehci",
	.id = 0,
	.resource = usbh_ehci_resource,
	.num_resources = ARRAY_SIZE(usbh_ehci_resource),
	.dev = {
		.dma_mask = &ehci_dmamask,
		.coherent_dma_mask = DMA_BIT_MASK(32),
	},
};
#endif

#ifdef CONFIG_USB_OHCI_BCM

static u64 ohci_dmamask = DMA_BIT_MASK(32);

static struct resource usbh_ohci_resource[] = {
	[0] = {
		.start = IPROC_USB20_REG_BASE + 0x1000,
		.end = IPROC_USB20_REG_BASE + 0x1000 + 0x0FFF,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = BCM_INT_ID_USB2H2CORE_USB2_INT,
		.end = BCM_INT_ID_USB2H2CORE_USB2_INT,
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device usbh_ohci_device =
{
	.name = "bcm-ohci",
	.id = 0,
	.resource = usbh_ohci_resource,
	.num_resources = ARRAY_SIZE(usbh_ohci_resource),
	.dev = {
		.dma_mask = &ohci_dmamask,
		.coherent_dma_mask = DMA_BIT_MASK(32),
	},
};
#endif

#ifdef CONFIG_DMAC_PL330
#include "../../../drivers/bcmdrivers/dma/pl330-pdata.h"
static struct iproc_pl330_data iproc_pl330_pdata =	{
	/* Non Secure DMAC virtual base address */
	.dmac_ns_base = IPROC_DMAC_REG_VA,
	/* Secure DMAC virtual base address */
	.dmac_s_base = IPROC_DMAC_REG_VA,
	/* # of PL330 dmac channels 'configurable' */
	.num_pl330_chans = 8,
	/* irq number to use */
	.irq_base = BCM_INT_ID_DMAC,
	/* # of PL330 Interrupt lines connected to GIC */
	.irq_line_count = 16,
};

static struct platform_device pl330_dmac_device = {
	.name = "iproc-dmac-pl330",
	.id = 0,
	.dev = {
		.platform_data = &iproc_pl330_pdata,
		.coherent_dma_mask  = DMA_BIT_MASK(64),
	},
};
#endif




void __init board_map_io(void)
{

	/*
	 * Install clock sources in the lookup table.
	 */
	clkdev_add_table(board_clk_lookups,
			ARRAY_SIZE(board_clk_lookups));

	/* Map machine specific iodesc here */
	northstar_map_io();
}


static struct platform_device *board_devices[] __initdata = {
#ifdef CONFIG_IPROC_I2C
    &board_smbus_device,
#if defined(CONFIG_MACH_HX4) || defined(CONFIG_MACH_KT2)
    &board_smbus_device1,
#endif
#endif /* CONFIG_IPROC_I2C */

#if defined(CONFIG_IPROC_FA2)
#if defined(CONFIG_MACH_NSP)
    &board_fa2_device,
#endif
#endif /* FA+ */

#if defined(CONFIG_IPROC_CCB_TIMER) || defined(CONFIG_IPROC_CCB_TIMER_MODULE)
    &board_timer_device,
#endif /* CONFIG_IPROC_CCB_TIMER || CONFIG_IPROC_CCB_TIMER_MODULE */
#if defined(CONFIG_IPROC_WDT) || defined(CONFIG_IPROC_WDT_MODULE)
    &board_wdt_device,
#endif /* CONFIG_IPROC_WDT || CONFIG_IPROC_WDT_MODULE */
#if defined(CONFIG_IPROC_PWM) || defined(CONFIG_IPROC_PWM_MODULE)
	&board_pwm_device,
#endif /* CONFIG_IPROC_PWM || CONFIG_IPROC_PWM_MODULE */
#ifdef CONFIG_IPROC_USB3H
	&bcm_xhci_device,
#endif
#ifdef CONFIG_USB_EHCI_BCM
	&usbh_ehci_device,
#endif
#ifdef CONFIG_USB_OHCI_BCM
	&usbh_ohci_device,
#endif
#ifdef CONFIG_DMAC_PL330
	&pl330_dmac_device,
#endif
};

static struct amba_device *amba_devs[] __initdata = {
#if defined(CONFIG_IPROC_CCB_WDT) || defined(CONFIG_IPROC_CCB_WDT_MODULE)
	&sp805_wdt_device,
#endif
};

static void __init board_add_devices(void)
{
	int i;

	platform_add_devices(board_devices, ARRAY_SIZE(board_devices));
#ifdef CONFIG_IPROC_PCT_BOARD
        i2c_register_board_info(0, &iproc_pct_i2c_devs, 1);
#endif
//    if (iproc_get_chipid() == 53010) {
//    }
	for (i = 0; i < ARRAY_SIZE(amba_devs); i++) {
		amba_device_register(amba_devs[i], &iomem_resource);
	}
}

#ifdef CONFIG_IPROC_IPGS7XXTX_BOARD
static struct spi_board_info max3421_spi_device[] = {
       {
               .modalias = "max3421-hcd",
               .platform_data = NULL,
               .controller_data = NULL,
               .max_speed_hz = 2 * 1000 * 1000,
               .bus_num = 0,
               .chip_select = 0,
               .mode = SPI_MODE_0,
               .irq = 123,
       },
};
#else
/* SPI device info of GSIO(SPI) interface */
static struct spi_board_info bcm5301x_spi_device[] = {
	{
		.modalias = "spidev",
		.platform_data = NULL,
		.controller_data = NULL,
		.max_speed_hz = 2 * 1000 * 1000,
		.bus_num = 0,
		.chip_select = 0,
		.mode = SPI_MODE_0,
	},
};
#endif

void __init board_timer_init(void)
{
	northstar_timer_init(&clk_ref);
}

struct sys_timer board_timer = {
	.init   = board_timer_init,
};

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)
#define NORTHSTAR_GPIO_PULLUP_REG_BASE       0x1800C1C0
#define NORTHSTAR_GPIO_PULLUP_REG_VAL        0xFCF
#define CPLD_MIN_ADDR                        0x31  /* Slot 1 i2c addr and it will incremnt 1 for next slot */
#define CPLD_MAX_ADDR                        0x3F  /* Slot is not plugged in */
#define CPLD_BLADE_MISC_REG                  0xc  
#define CPLD_ADDR                            0x30  /* base addr */
#define CCA_GPIO_OUT_EN                      0x68
#define GPIO_0_BIT                           0x1 
#define GPIO_1_BIT                           0x2

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)
static int __init m6100_usb_init (void)
{
  unsigned char data = 0;
        data = 0;
        /* i2c address will be taken care automatically in m6100_i2c_read_write function,
           sending 0 as address is not relavent */
        m6100_i2c_read_write(0, CPLD_BLADE_MISC_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
  return 0;
}
late_initcall(m6100_usb_init);
#endif

static volatile void *vaddr = NULL;
static void gpioRempapCheck(void)
{
   if(vaddr == NULL)
   { 
      vaddr = ioremap(IPROC_CCA_REG_BASE,0x100);
   }
#if defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)        
   {
     static volatile *vaddr_vega = NULL;
     if(vaddr_vega == NULL)
     { 
       vaddr_vega = ioremap_nocache(NORTHSTAR_GPIO_PULLUP_REG_BASE,0x10);
       if(vaddr_vega != NULL)
       { 
         u32 reg;
         /* setting 0 for bit-1 gpio1 as input */
          reg = ioread32(vaddr_vega);
          reg |= NORTHSTAR_GPIO_PULLUP_REG_VAL;
          iowrite32(reg, vaddr_vega);
          reg = ioread32(vaddr_vega);
          iounmap(vaddr_vega);
       }
     }
   }
#endif
}

void cpldI2cAddressDetermine(void)
{
  int err; u8 data;

  if(cpldI2cAddress >= CPLD_MIN_ADDR && cpldI2cAddress <= CPLD_MAX_ADDR)
    return;

  cpldI2cAddress = CPLD_ADDR | 1;
  err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
  if (err < 0)
  {
    cpldI2cAddress = CPLD_ADDR | 2;
    err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
    if (err < 0)
    {
      cpldI2cAddress = CPLD_ADDR | 3;
      err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
      if (err < 0)
      {
        cpldI2cAddress = CPLD_ADDR | 4;
        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
        if (err < 0)
        {
          cpldI2cAddress = CPLD_ADDR | 0xf;
          err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
          if(err < 0)
          {
            printk(KERN_CRIT "unable to determine the cpld i2c address err:%d **** \n",err);
          }
        }  
      }
    }
  } /* end of cpld i2c probe */
}

void m6100_enable_internal_irq(int irqNo, void *ptr)
{
    u32 reg;
    gpioRempapCheck();
    if(vaddr != NULL)
    {
        /* setting 0 for bit-1 gpio 0 & 1 as input */
        reg = ioread32(vaddr+CCA_GPIO_OUT_EN);
        reg &= ~((u32) 3 << 0);
        iowrite32(reg, (vaddr+CCA_GPIO_OUT_EN));

        /* setting gpio 0 & 1 as active low */
        reg = ioread32(vaddr+ChipcommonA_GPIOIntPolarity_BASE);
        reg |= ((u32) 3 << 0);
        iowrite32(reg, (vaddr+ChipcommonA_GPIOIntPolarity_BASE));

        /* setting gpio 0 & 1 interrupt mask */
        reg = ioread32(vaddr+ChipcommonA_GPIOIntMask_BASE);
        reg |= ((u32) 3 << 0);
        iowrite32(reg, (vaddr+ChipcommonA_GPIOIntMask_BASE));

        /* setting global interrupt enable */
        reg = ioread32(vaddr+IPROC_CCA_INT_MASK);
        reg |= 1;
        iowrite32(reg, (vaddr+IPROC_CCA_INT_MASK));
    }
    else
    {
      printk(KERN_INFO "***m6100_enable_internal_irq vaddr is NULL\n");
    }
}
void m6100_enable_loop_internal_irq(int irqNo, void *ptr)
{
    u32 reg;
    if(vaddr != NULL)
    {
      /* setting gpio1 as interrupt mask--disable interrupt */
         reg = ioread32(vaddr+ChipcommonA_GPIOIntMask_BASE);
         reg |= ((u32) 3 << 0);
         iowrite32(reg, (vaddr+ChipcommonA_GPIOIntMask_BASE));
    }
}
void m6100_disable_internal_irq(int irqNo)
{
    u32 reg;
    if(vaddr != NULL)
    {
         /* setting gpio1 as interrupt mask--disable interrupt */
         reg = ioread32(vaddr+ChipcommonA_GPIOIntMask_BASE);
         reg &= ~((u32) 3 << 0);
         iowrite32(reg, (vaddr+ChipcommonA_GPIOIntMask_BASE));
    }
}

void m6100_irq_process_fptr(int irqNo, void *ptr)
{
    u32 reg;
    u8 *ptr1 = (u8 *)ptr;
    if(vaddr != NULL)
    {
        /* setting global interrupt enable */
        reg = ioread32(vaddr+IPROC_CCA_INT_STS);
        if((reg & 0x1) == 1) /* gpio interrupt */
        { 
           ptr1[0] = 1;   /* GPIO interrupt */
        } 
        else
        {
            ptr1[0] = 0;
        } 
    }
}
#define CPLD_INTR_STA_REG   0x4
void m6100_irq_process_data(int irqNo, void *ptr)
{
    u32 reg;
    u8 *ptr1 = (u8 *)ptr;
    s32 err = 0;

    if(vaddr != NULL)
    {
        if(ptr1[0] == 1) /* gpio interrupt */
        {
           reg = ioread32(vaddr+ChipcommonA_GPIOInput_BASE);
           if( (reg & GPIO_1_BIT) == 0)
           {
               ptr1[0] = GPIO_1_BIT;   /* Some define value to inform that it is GPIO1 interrupt */
               err = m6100_i2c_read_write(cpldI2cAddress, CPLD_INTR_STA_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &ptr1[1]);
               if(err < 0)
               {
                  udelay(100);
                  err = m6100_i2c_read_write(cpldI2cAddress, CPLD_INTR_STA_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &ptr1[1]);
                  if(err < 0)
                  {
                     udelay(100);
                     err = m6100_i2c_read_write(cpldI2cAddress, CPLD_INTR_STA_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &ptr1[1]);
                     if(err < 0)
                        ptr1[1] = 0xff; /* returning that there is no interrupt */
                  }
               } 
           }   
           if( (reg & GPIO_0_BIT) == 0 )
           {
               ptr1[0] = GPIO_0_BIT;   /* Some define value to inform that it is GPIO0 interrupt */
           }
        }
    }
}

s32 m6100_i2c_read_write(u8 address, u8 offset, u8 rd_wr, u8 rd_wr_mode, u8 *data)
{
    int err = 0;
    union i2c_smbus_data i2cdata;
    if (adap == NULL)
    {
       adap = i2c_get_adapter(0);
    }
    cpldI2cAddressDetermine();
    i2cdata.byte = *data;  /* For write & read */
    err = i2c_smbus_xfer(adap, cpldI2cAddress, 0, rd_wr,
                   offset, rd_wr_mode, &i2cdata);
 
    *data = i2cdata.byte;  /* For read */
    if(err < 0)
          printk(KERN_INFO "m6100_i2c_read_write failure err: %x address:%x offset:%x rd_wr:%x rd_wr_mode:%x \n",
                                err, cpldI2cAddress, offset, rd_wr, rd_wr_mode);
    return err;
}

#define WATCHDOG_TIMER_INTERVAL               5000 /* 5 seconds ms */
#define CPLD_WDT_ENABLE                       0x8
#define CPLD_RST_WDT_REG                      0x1
/*
** Disable the watchdog feature
*/
void watchdog_disable(void)
{
    u8 data = 0; /* WDT disable */
    s32 err = 0;

        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);

        if (err < 0)
           printk (KERN_EMERG "Error:watchdog_disable: m6100_i2c_read_write access failure.\n");
        else
           printk (KERN_INFO "cpld watchdog disable success\n");
}
/*
** Service watchdog.
*/
void watchdog_service(void)
{

    u8 data = CPLD_WDT_ENABLE;
    s32 err = 0;

        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
        if(err < 0)
        {
            udelay(100);
            err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
            if(err < 0)
            {
                udelay(100);
                err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
            } 
        }
        if (err < 0)
           printk (KERN_EMERG "Error:watchdog_service:m6100_i2c_read_write access failure.\n");
}
/*
** Enable the watchdog feature
*/
int watchdog_enable(void)
{
    u8 data = CPLD_WDT_ENABLE;
    s32 err = 0;

        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);   
       
        if (err < 0) 
           printk (KERN_EMERG "Error: mtchdog_enable:6100_i2c_read_write access failure.\n");
        else
           printk (KERN_INFO "watchdog_enable: cpld watchdog register success\n");

        return err;
}
/*
** Get the Watchdog Timer Interval in ms
*/
unsigned int watchdog_get_timer_interval(void)
{
        return WATCHDOG_TIMER_INTERVAL;
}

#endif
void __init board_init(void)
{
	printk(KERN_DEBUG "board_init: Enter\n");

	/*
	 * Add common platform devices that do not have board dependent HW
	 * configurations
	 */
	board_add_common_devices(&clk_ref);

	board_add_devices();

#if defined(CONFIG_IPROC_SD) || defined(CONFIG_IPROC_SD_MODULE)   
	/* only bcm53012 support sdio */
	if ((__REG32(IPROC_IDM_REGISTER_VA + 0xd500) & 0xc) == 0x0) {
		setup_sdio();
	}
#endif

#if defined(CONFIG_IPROC_PWM) || defined(CONFIG_IPROC_PWM_MODULE)
        __raw_writel(0xf, IPROC_CCB_GPIO_REG_VA + IPROC_GPIO_CCB_AUX_SEL);
    	pwm_add_table(board_pwm_lookup, ARRAY_SIZE(board_pwm_lookup));
#endif  

	/* Register SPI device info */
#ifdef CONFIG_IPROC_IPGS7XXTX_BOARD
	spi_register_board_info(max3421_spi_device,
        ARRAY_SIZE(max3421_spi_device));
#else
	spi_register_board_info(bcm5301x_spi_device, 
	ARRAY_SIZE(bcm5301x_spi_device));
#endif

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD) 
        brcpld_irq_enable_register(&m6100_enable_internal_irq);
        brcpld_irq_process_register(&m6100_irq_process_fptr);
        brcpld_irq_loop_enable_register(&m6100_enable_loop_internal_irq);
        brcpld_irq_disable_register(&m6100_disable_internal_irq);
        brcpld_irq_process_data(&m6100_irq_process_data);
        gpioRempapCheck();
#endif
	printk(KERN_DEBUG "board_init: Leave\n");
}

MACHINE_START(IPROC, "Broadcom iProc")

// 	Used micro9 as a reference.  Micro9 removed these two fields,
//	and replaced them with a call to ep93xx_map_io(), which in turn
// 	calls iotable_init().  Northstar appears to have an equivalent
//	init (refer to northstar_io_desc[] array, in io_map.c
	.map_io = board_map_io,
	.init_irq = iproc_init_irq,
        .handle_irq     = gic_handle_irq,
	.timer  = &board_timer,
	.init_machine = board_init,
MACHINE_END
