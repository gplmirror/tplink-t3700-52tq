/* Automatically generated - do not edit */
#define CONFIG_L2C_AS_RAM	1
#define CONFIG_RUN_DDR_SHMOO2	1
#define CONFIG_CUSTOM_LINKER_SCRIPT	1
#define CONFIG_SYS_ARCH  "arm"
#define CONFIG_SYS_CPU   "armv7"
#define CONFIG_SYS_BOARD "bcm95615x"
#define CONFIG_SYS_VENDOR "broadcom"
#define CONFIG_SYS_SOC    "hurricane2"
#define CONFIG_BOARDDIR board/broadcom/bcm95615x
#include <config_cmd_defaults.h>
#include <config_defaults.h>
#include <configs/hurricane2.h>
#include <asm/config.h>
#include <config_fallbacks.h>
#include <config_uncmd_spl.h>
