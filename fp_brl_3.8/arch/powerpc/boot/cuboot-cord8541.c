/*
 * Old U-boot/CFE compatibility for 85xx boards
 *
 * Author: Ramakrishna Koduri (rakoduri@broadcom.com)
 *
 * Copyright (c) 2014 Broadcom Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */

#include "ops.h"
#include "stdio.h"
#include "cuboot.h"

#define TARGET_85xx
#include "ppcboot.h"

extern void fdt_purge(void);

static unsigned long initrd_start, initrd_end;

/** bd info for cord8541 board */
static bd_t bd = { 
        /** Some boards have 512MB and others have 1024MB RAM present on the board.
            Let's limit the amount of RAM memory on the system to 512MB.
	    We need this because we can not risk the bootmem allocation and flattened DTB regions.
	    The DTB memory fixup can figure out the actual RAM present on the board.
 	 */
	.bi_memstart    = 0x0,		/* start of DRAM memory */
	.bi_memsize     = 512*1024*1024,/* size	 of DRAM memory in bytes */
	.bi_flashstart  = 0xfe000000,	/* start of FLASH memory */
	.bi_flashsize   = 32*1024*1024,	/* size	 of FLASH memory */
	.bi_immr_base   = 0xe0000000,	/* base of IMMR register */
                        
	.bi_ip_addr     = 0x0,		/* IP Address */
	.bi_enetaddr  	= { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55 },	/* Ethernet address */
	.bi_ethspeed    = 1000,		/* Ethernet speed in Mbps */
	.bi_intfreq	= 666666667,	/* Internal Freq, 666 MHz */
	.bi_busfreq	= 333333333,	/* Bus Freq, 333 MHz */
	.bi_baudrate    = 9600,		/* Console Baudrate */
	.bi_enet1addr   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, /* second onboard ethernet port */
};

static void cord8541_platform_fixups(void)
{
	void *soc;

	dt_fixup_memory(bd.bi_memstart, bd.bi_memsize);
	dt_fixup_mac_address_by_alias("ethernet0", bd.bi_enetaddr);
	dt_fixup_mac_address_by_alias("ethernet1", bd.bi_enet1addr);
	dt_fixup_cpu_clocks(bd.bi_intfreq, bd.bi_busfreq / 8, bd.bi_busfreq);

	/* 
	 * Fixup the serial clock frequency.
	 */
	soc = find_node_by_devtype(NULL, "soc");
	if (soc) {
		void *serial = NULL;

		setprop(soc, "bus-frequency", &bd.bi_busfreq,
		        sizeof(bd.bi_busfreq));

		while ((serial = find_node_by_devtype(serial, "serial"))) {
			if (get_parent(serial) != soc)
				continue;

			setprop(serial, "clock-frequency", &bd.bi_busfreq,
			        sizeof(bd.bi_busfreq));
		}
	}
}

static void cord8541_initrd_info_get(void)
{
	void *chosen;

	/* Pick the chosen node that the device tree has */
	chosen = finddevice("/chosen");
	if (!chosen) {
		/* 
		 * This is a disaster. Handle deal with care.
		 *
		 * TODO: Alternative approach is to parse the cmdline 
		 *       to get the rd_start, rd_size parameters.
		 */
		goto fixup_initrd_start;
	}

	/* Extract initrd-start, initrd-end from legacy DTB */
	if (getprop(chosen, "linux,initrd-start", &initrd_start, sizeof(initrd_start)) < 0)
		goto fixup_initrd_start;

	if (getprop(chosen, "linux,initrd-end", &initrd_end, sizeof(initrd_end)) < 0)
		goto fixup_initrd_end;

	return;

	/* 
	 * We know the legacy CFE loads the initrd as follows:
	 * initrd at 0x8000000 and default size is 1000000.
	 * Let's play a safe gamble to increase the size of initrd to 4MB.
	 */
fixup_initrd_start:
	initrd_start = 0x8000000;

fixup_initrd_end:
	initrd_end   = initrd_start + 4*1024*1024;
}

/* Find out the size of the RAM that is being passed */
static void cord8541_memory_info_get(void)
{
        void *root, *memory;
        int naddr, nsize, i;
        u32 memreg[4];
	u64 start, size;

        root = finddevice("/");
        if (getprop(root, "#address-cells", &naddr, sizeof(naddr)) < 0)
                naddr = 2;

        if (naddr < 1 || naddr > 2) {
		/* Hmm. This is very ugly. We can't trust this DTB.
		 * We just go with defaults. Can't do beyond.
		 */
		return;
	}

        if (getprop(root, "#size-cells", &nsize, sizeof(nsize)) < 0)
                nsize = 1;

        if (nsize < 1 || nsize > 2) {
		/* Huh. This DTB is corrupted or malformed???
		 * We just go with defaults. We can't play with this.
		 */
		return;
	}

        memory = finddevice("/memory");
        if (!memory) {
		/* Worse. This node is not present. What's going on???
		 * We just go with defaults.
		 */
		return;
        }

        if (getprop(memory, "reg", memreg, (naddr + nsize)*sizeof(u32)) < 0) {
		 /* We just go with defaults. */
		return;
	}

	i = 0; start = size = 0;
	if (naddr == 2) {
		start = memreg[i++];
		start = start << 32;
	}

	start |= memreg[i++] & 0xffffffff;

	if (nsize == 2) {
		size = memreg[i++];
		size = size << 32;
	}

	size |= memreg[i++] & 0xffffffff;

	bd.bi_memstart = start;
	bd.bi_memsize  = size;
}

static void cord8541_macaddr_info_get(void)
{
	void *enet0, *enet1;
	u8 macaddr[6];

	enet0 = finddevice("/soc8548@e0000000/ethernet@00024000");
        if (enet0) {
		if (getprop(enet0, "local-mac-address", macaddr, 6) < 0) {
			/* We just go with defaults. */
			return;
		}
		memcpy(&bd.bi_enetaddr, macaddr, 6);
	}

	enet1 = finddevice("/soc8548@e0000000/ethernet@00025000");
        if (enet1) {
		if (getprop(enet1, "local-mac-address", macaddr, 6) < 0) {
			/* We just go with defaults. */
			return;
		}
		memcpy(&bd.bi_enet1addr, macaddr, 6);
	}
}

static void cord8541_legacy_dtb_info_get(void *cfe_blob)
{
	/* Grab the DTB that CFE has passed to us*/
	fdt_init(cfe_blob);

	/* Grab the initrd info */
	cord8541_initrd_info_get();

	/* Grab the memory info */
	cord8541_memory_info_get();

	/* Obtain the mac address info of enet0, enet1 */
	cord8541_macaddr_info_get();

	/* We are done with this DTB */
	fdt_purge();
}

void platform_init(unsigned long r3, unsigned long r4, unsigned long r5,
                   unsigned long r6, unsigned long r7)
{

	/* 
	 *  The CFE passes the board boot parameters as follows:
	 *  
	 *  r3 - DTB address        ###### Board info structure pointer or DTB info address
	 *  r4 - 0x0                ###### Starting address of the initramfs/initrd
	 *  r5 - 0x0                ###### Ending address of the initramfs/initrd
	 *  r6 - cmdline start      ###### Start of kernel command line string
	 *  r7 - cmdline end        ###### End of kernel command line string
	 */
	cuboot_init(r4, r5, r6, r7, bd.bi_memstart + bd.bi_memsize);
	cord8541_legacy_dtb_info_get((void *)r3);
	loader_info.initrd_addr = initrd_start;
	loader_info.initrd_size = initrd_start ? initrd_end - initrd_start : 0;

	/** Now get to the DTB that is part of the kernel */
	fdt_init(_dtb_start);
	serial_console_init();
	platform_ops.fixups = cord8541_platform_fixups;
}
