/*
 * MPC85xx setup and early boot code plus other random bits.
 *
 * Maintained by Kumar Gala (see MAINTAINERS for contact information)
 *
 * Copyright 2005, 2011-2012 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/reboot.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/major.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/initrd.h>
#include <linux/interrupt.h>
#include <linux/fsl_devices.h>
#include <linux/of_platform.h>

#include <asm/pgtable.h>
#include <asm/page.h>
#include <linux/atomic.h>
#include <asm/time.h>
#include <asm/io.h>
#include <asm/machdep.h>
#include <asm/ipic.h>
#include <asm/pci-bridge.h>
#include <asm/irq.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/i8259.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>

#include "mpc85xx.h"

#ifdef CONFIG_PCI
#ifdef CONFIG_PPC_I8259
static void mpc85xx_8259_cascade_handler(unsigned int irq,
					 struct irq_desc *desc)
{
	unsigned int cascade_irq = i8259_irq();

	if (cascade_irq != NO_IRQ)
		/* handle an interrupt from the 8259 */
		generic_handle_irq(cascade_irq);

	/* check for any interrupts from the shared IRQ line */
	handle_fasteoi_irq(irq, desc);
}

static irqreturn_t mpc85xx_8259_cascade_action(int irq, void *dev_id)
{
	return IRQ_HANDLED;
}

static struct irqaction mpc85xx_8259_irqaction = {
	.handler = mpc85xx_8259_cascade_action,
	.flags = IRQF_SHARED | IRQF_NO_THREAD,
	.name = "8259 cascade",
};
#endif /* PPC_I8259 */
#endif /* CONFIG_PCI */

#if defined(CONFIG_PPC_I8259) && defined(CONFIG_PCI)
static int mpc85xx_8259_attach(void)
{
	int ret;
	struct device_node *np = NULL;
	struct device_node *cascade_node = NULL;
	int cascade_irq;

	/* Initialize the i8259 controller */
	for_each_node_by_type(np, "interrupt-controller")
		if (of_device_is_compatible(np, "chrp,iic")) {
			cascade_node = np;
			break;
		}

	if (cascade_node == NULL) {
		printk(KERN_DEBUG "Could not find i8259 PIC\n");
		return -ENODEV;
	}

	cascade_irq = irq_of_parse_and_map(cascade_node, 0);
	if (cascade_irq == NO_IRQ) {
		printk(KERN_ERR "Failed to map cascade interrupt\n");
		return -ENXIO;
	}

	i8259_init(cascade_node, 0);
	of_node_put(cascade_node);

	/*
	 *  Hook the interrupt to make sure desc->action is never NULL.
	 *  This is required to ensure that the interrupt does not get
	 *  disabled when the last user of the shared IRQ line frees their
	 *  interrupt.
	 */
	if ((ret = setup_irq(cascade_irq, &mpc85xx_8259_irqaction))) {
		printk(KERN_ERR "Failed to setup cascade interrupt\n");
		return ret;
	}

	/* Success. Connect our low-level cascade handler. */
	irq_set_handler(cascade_irq, mpc85xx_8259_cascade_handler);

	return 0;
}
machine_device_initcall(mpc8541_cordcs, mpc85xx_8259_attach);
#endif /* CONFIG_PPC_I8259 */

//***********************************************************************
// NetApp Corduroy-CS Board Functions
//***********************************************************************
#if defined(CONFIG_SENSORS_M41T00)
#include <linux/m41t00.h>
#include <linux/rtc.h>
extern ulong    m41t00_get_rtc_time(void);
extern int      m41t00_set_rtc_time(ulong);

static struct m41t00_platform_data m41t00_pdata = {
        .type = M41T00_TYPE_M41T81,
        .i2c_addr = 0x68,
        .sqw_freq = 0x0,
};

static struct platform_device *m41t00_dev = NULL;


static int __init cord8541_device_init(void)
{
        int ret;

        m41t00_dev = platform_device_alloc(M41T00_DRV_NAME, -1);
        if (!m41t00_dev) {
                printk(KERN_WARNING "Kernel can't allocate memory for M41T00\n");
                return -1;
        }

        m41t00_dev->dev.platform_data = &m41t00_pdata;

        ret = platform_device_add(m41t00_dev);
        if(ret) {
                printk(KERN_WARNING "Kernel add device into platform failure!\n");
                platform_device_put(m41t00_dev);
                return -1;
        }
        return 0;
}

device_initcall(cord8541_device_init);

extern ulong    m41t00_get_rtc_time(void);
extern int      m41t00_set_rtc_time(ulong);

static void _rtc_drv_wrapper_get_time(struct rtc_time * rtime)
{
    ulong sec;
    extern enum system_state;

    if (unlikely(system_state == SYSTEM_RUNNING)) {
        return;
    }

    sec = m41t00_get_rtc_time();
    to_tm(sec, rtime);
    return;
}

static int _rtc_drv_wrapper_set_time(struct rtc_time * rtime)
{
    ulong sec;

    if (unlikely(system_state == SYSTEM_RUNNING)) {
        return;
    }

    sec = mktime(rtime->tm_year, rtime->tm_mon, rtime->tm_mday,
                 rtime->tm_hour, rtime->tm_min, rtime->tm_sec);
    m41t00_set_rtc_time(sec);
    return 0;
}

static int __init cord8541_rtc_hookup(void)
{
        struct timespec tv;

        ppc_md.get_rtc_time = _rtc_drv_wrapper_get_time;
        ppc_md.set_rtc_time = _rtc_drv_wrapper_set_time;

        tv.tv_nsec = 0;
        tv.tv_sec = m41t00_get_rtc_time();
        do_settimeofday(&tv);

        return 0;
}
late_initcall(cord8541_rtc_hookup);
#endif /* CONFIG_SENSORS_M41T00 */

#ifdef CONFIG_PCI
#define PCI_DEVICE_ID_BCM_56820 0xb820

static void __init cord8541_pci_irq_fixup(struct pci_dev *dev)
{
	unsigned char __iomem *cpld_base;

        if ((dev->vendor == PCI_VENDOR_ID_BROADCOM)  &&
            (dev->device == PCI_DEVICE_ID_BCM_56820)) {
#if 0   /* This is a hack to fixup the interrupt for this device. 
           We have updated the same in DTB.
           We do not need this anymore still kept as place holder for information.
         */
		/*
		 * Legacy CFE does not set the interrupt line
		 * for this device. Do the fixup here.
		 * The IRQ is derived as follows:
		 * virq -> irq_base + hirq + 1 -> (16 + 7 + 1)
		 */
		dev->irq = 24;
		pci_write_config_byte(dev, PCI_INTERRUPT_LINE, dev->irq);
#endif

		/* corduroy CS System CPLD base address is 0xF0000000 */
		cpld_base = ioremap( 0xF0000000 , 0xff);

		if (cpld_base) {
			/* Set Interrupt Control Register for enabling PCI interrupts */
			out_8(cpld_base+0x1, 0x80); /* Set PCI Interrupt Enable */
			udelay(1000);
			printk(KERN_INFO "Enabled PCI interrupts... \n");
		} else {
			printk (KERN_EMERG "Error: Interrupt control register not mapped!\n");
		}
	}
}
#endif

static int __init cord8541_probe(void)
{
        unsigned long cplen;
        const char *model;
	unsigned long root = of_get_flat_dt_root();

        model = of_get_flat_dt_prop(root,"model",&cplen);
        printk("Board Model Name:  %s\n", model);

        return of_flat_dt_is_compatible(root, "fsl,mpc8541");
}

static void __init cord8541_setup_arch(void)
{
    if (ppc_md.progress)
        ppc_md.progress("setup_arch: NetApp Corduroy-CS MPC8541 board", 0);

#ifdef CONFIG_PCI
	ppc_md.pci_irq_fixup = cord8541_pci_irq_fixup;
#endif

	fsl_pci_assign_primary();
}

static void cord8541_show_cpuinfo(struct seq_file *m)
{
        uint pvid, svid, phid1;
        uint memsize = total_memory;

        pvid = mfspr(SPRN_PVR);
        svid = mfspr(SPRN_SVR);

        seq_printf(m, "Vendor\t\t: Freescale Semiconductor\n");
        seq_printf(m, "PVR\t\t: 0x%x\n", pvid);
        seq_printf(m, "SVR\t\t: 0x%x\n", svid);

        /* Display cpu Pll setting */
        phid1 = mfspr(SPRN_HID1);
        seq_printf(m, "PLL setting\t: 0x%x\n", ((phid1 >> 24) & 0x3f));

        /* Display the amount of memory */
        seq_printf(m, "Memory\t\t: %d MB\n", memsize / (1024 * 1024));
}

void cord8541_restart(char *cmd)
{
    unsigned char __iomem *rstcr;

    /* corduroy CS System CPLD base address is 0xF0000000 */
    rstcr = ioremap( 0xF0000000 , 0xff);

    if (rstcr) {
       /* set reset control register */
       printk(KERN_INFO "Restarting Corduroy CS... \n");
       out_8(rstcr, 0x80); /* HRESET_REQ */
    } else {
       printk (KERN_EMERG "Error: reset control register not mapped, spinning!\n");
    }

    udelay(1000);
    printk (KERN_EMERG "Reset didn't Happen, spinning! Press <RESET> Button\n");

    for(;;)
      /* Do nothing */ ;
}

void cord8541_pcibios_fixup_bus(struct pci_bus *bus)
{
    fsl_pcibios_fixup_bus(bus);
}

void __init cord8541_pic_init(void)
{
    struct mpic *mpic = mpic_alloc(NULL, 0, MPIC_BIG_ENDIAN, 0, 256, " OpenPIC  ");
    BUG_ON(!mpic);
    mpic_init(mpic);
}

machine_arch_initcall(mpc8541_cordcs, mpc85xx_common_publish_devices);
define_machine(mpc8541_cordcs) {
	.name			= "cord8541",
	.probe			= cord8541_probe,
	.setup_arch		= cord8541_setup_arch,
	.show_cpuinfo		= cord8541_show_cpuinfo,
	.init_IRQ		= cord8541_pic_init,
	.get_irq		= mpic_get_irq,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= cord8541_pcibios_fixup_bus,
#endif
	.restart		= cord8541_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};
