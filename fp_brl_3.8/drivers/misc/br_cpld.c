/*
 *  This program is free software; upcan redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 */

#include <linux/compat.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/pci.h>
#include <linux/fs.h>
#include <linux/poll.h>
#include <linux/interrupt.h>
#include <linux/cdev.h>
#include <linux/miscdevice.h>
#include <linux/spinlock.h>

#include <asm/atomic.h>
#include <asm/io.h>
#include <asm/types.h>

#ifdef CONFIG_NLM_XLP
#define NLM_HAL_LINUX_KERNEL
#include <asm/netlogic/hal/nlm_hal.h>
#include <asm/netlogic/hal/nlm_hal_pic.h>
#include <asm/netlogic/hal/nlm_hal_xlp_dev.h>
#endif

#include <linux/br_cpld.h>

#define DEBUG
#undef DEBUG
#ifdef DEBUG
#define DPRINTF(x...)  printk(x)
#else
#define DPRINTF(x...)
#endif

static irqreturn_t brcpld_isr (int irq, void *data);
void  (*cpu_irq_process_fptr)(int,int,void*,void *, void *) = NULL;

extern volatile unsigned char *CPLD_ADDR;
#ifdef CONFIG_NLM_XLP
static volatile uint64_t gpio_mmio;
#endif

irqDat irqData;

irqNos irqStat;

spinlock_t lock;

struct semaphore intSem;
/*
 * File ops
 */

static long
brcpld_ioctl (struct file *file, unsigned int cmd, unsigned long arg)
{
  void __user *argp = (void __user *) arg;
  int retval;
  struct __brcpld_waitForInt wait;
  struct __brcpld_irqDetails irqDetails;
#ifdef CONFIG_NLM_XLP
  int32_t  gpio_reg_val;
#endif

  switch (cmd)
    {
    case BRCPLD_REG_IRQ:
      {
	int i = 0;
        DPRINTF (KERN_DEBUG "brcpld_ioctl:BRCPLD_REG_IRQ:-at- \n");
	retval =
	  copy_from_user (&irqDetails, argp,
			  sizeof (struct __brcpld_irqDetails));
	if (retval)
	  {
	    printk (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_REG_IRQ: copy_from_user %d \n",
		     retval);
	    return -EFAULT;
	  }
	DPRINTF (KERN_DEBUG "brcpld_ioctl:BRCPLD_REG_IRQ: irqNo:%d  \n",
		 irqDetails.irqNo);
#ifdef CONFIG_MIPS
       for (i = 0; i < NO_INT_SUPPORT; i++)
          {
            if (irqStat.supIrq[i]==0){
                irqStat.supIrq[i]= irqDetails.irqNo;
                 break;
            }
          }
#else
        for (i = 0; i < NO_INT_SUPPORT; i++)
          {
            if (irqStat.supIrq[i] == irqDetails.irqNo)
              break;
          }
#endif
	if (i >= NO_INT_SUPPORT)
	  {
	    printk (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_REG_IRQ: irq supported & provided didnt match: %d \n",
		     retval);
	    return -EINVAL;
	  }
	irqStat.enbIrq[i] = 1;
	irqStat.regOffset1[i] = irqDetails.regOffset1;
	irqStat.regOffset2[i] = irqDetails.regOffset2;
	irqStat.regActByIsrOrApp[i] = irqDetails.regActByIsrOrApp;
	irqStat.maskVal[i] = irqDetails.maskVal;

#ifdef CONFIG_NLM_XLP
        /* The Netlogic GPIO interrupts are all routed to irq 13 */
        retval = request_irq (PIC_IRT_GPIO_1_INDEX, brcpld_isr,
                              IRQF_SHARED | IRQF_DISABLED, "brcpld",
                              (void *) &irqData);

        irqStat.gpioMask[i] = 1 << (irqDetails.irqNo - 32);
        gpio_reg_val = nlm_hal_read_32bit_reg(gpio_mmio, XLP_GPIO_INTEN11);
        gpio_reg_val |= irqStat.gpioMask[i];
        nlm_hal_write_32bit_reg(gpio_mmio, XLP_GPIO_INTEN11, gpio_reg_val);

#else
	retval = request_irq (irqDetails.irqNo, brcpld_isr,
			      IRQF_SHARED | IRQF_DISABLED, "brcpld",
			      (void *) &irqData);
#endif

	if (retval)
	  {
	    printk (KERN_DEBUG "brcpld_ioctl:BRCPLD_REG_IRQ: retval: %d \n",
		     retval);
	    return -EINVAL;
	  }
	break;
      }
    case BRCPLD_EN_IRQ:
      {
	int i = 0;
	unsigned int irqNo;
	retval = copy_from_user (&irqNo, argp, sizeof (unsigned int));
	if (retval)
	  {
	    printk (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_EN_IRQ: copy_from_user %d \n",
		     retval);
	    return -EFAULT;
	  }
	for (i = 0; i < NO_INT_SUPPORT; i++)
	  {
	    if (irqStat.supIrq[i] == irqNo)
	      break;
	  }
	if (i >= NO_INT_SUPPORT)
	  {
	    DPRINTF (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_REG_IRQ: irq supported & provided didnt match: %d \n",
		     retval);
	    return -EINVAL;
	  }
	enable_irq (irqNo);
	break;
      }
    case BRCPLD_DIS_IRQ:
      {
	int i = 0;
	unsigned int irqNo;
	retval = copy_from_user (&irqNo, argp, sizeof (unsigned int));
	if (retval)
	  {
	    printk (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_DIS_IRQ: copy_from_user %d \n",
		     retval);
	    return -EFAULT;
	  }
	DPRINTF (KERN_DEBUG "brcpld_ioctl:BRCPLD_DIS_IRQ: irqNo:%d  \n",
		 irqNo);
	for (i = 0; i < NO_INT_SUPPORT; i++)
	  {
	    if (irqStat.supIrq[i] == irqNo)
	      break;
	  }
	if (i >= NO_INT_SUPPORT)
	  {
	    DPRINTF (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_REG_IRQ: irq supported & provided didnt match: %d \n",
		     retval);
	    return -EINVAL;
	  }
	disable_irq (irqNo);
	break;
      }
    case BRCPLD_WAIT_FOR_INTERRUPT:
      {
	unsigned long irqflags = 0;
	retval = copy_from_user (&wait, argp, sizeof (wait));
	if (retval)
	  {
	    printk (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_WAIT_FOR_INTERRUPT: copy_from_user %d \n",
		     retval);
	    return -EFAULT;
	  }
	retval = down_interruptible (&intSem);

	spin_lock_irqsave (&lock, irqflags);
	memcpy ((void *) &wait, (void *) &irqData, sizeof (irqData));
	memset (&irqData, 0, sizeof (irqDat));
	spin_unlock_irqrestore (&lock, irqflags);

	retval = copy_to_user (argp, &wait, sizeof (wait));
	if (retval)
	  {
	    printk (KERN_DEBUG
		     "brcpld_ioctl:BRCPLD_WAIT_FOR_INTERRUPT: copy_to_user %d \n",
		     retval);
	    return -EFAULT;
	  }
	break;
      }
    default:
      DPRINTF (KERN_DEBUG "brcpld_ioctl:default:-at- \n");
      return -ENOTTY;
    }

  return 0;
}

#ifdef CONFIG_COMPAT
static long
brcpld_compat_ioctl (struct file *filp, unsigned int cmd, unsigned long arg)
{
  DPRINTF (KERN_DEBUG "brcpld_compat_ioctl:-at- \n");
  if (_IOC_NR (cmd) <= 3 && _IOC_SIZE (cmd) == sizeof (compat_uptr_t))
    {
      cmd &= ~(_IOC_SIZEMASK << _IOC_SIZESHIFT);
      cmd |= sizeof (void *) << _IOC_SIZESHIFT;
    }
  return brcpld_ioctl (filp, cmd, (unsigned long) compat_ptr (arg));
}
#else
#define brcpld_compat_ioctl NULL
#endif

static int
brcpld_open (struct inode *inode, struct file *file)
{
  DPRINTF (KERN_DEBUG "brcpld_open:-at- \n");
  return 0;
}

static int
brcpld_close (struct inode *inode, struct file *file)
{
  DPRINTF (KERN_DEBUG "brcpld_close:-at- \n");
  return 0;
}


static struct file_operations brcpld_fops = {
  .open = brcpld_open,
  .release = brcpld_close,
  .unlocked_ioctl = brcpld_ioctl,
  .compat_ioctl = brcpld_compat_ioctl,
};
static irqreturn_t
brcpld_isr (int irq, void *data)
{
  int i = 0;
  unsigned long irqflags = 0;
/*  disable_irq (irq); */
#ifdef CONFIG_NLM_XLP
  int32_t  gpio_reg_val;
  gpio_reg_val = nlm_hal_read_32bit_reg(gpio_mmio, XLP_GPIO_INT_STAT1);
#endif
  for (i = 0; i < NO_INT_SUPPORT; i++)
    {
#ifdef CONFIG_NLM_XLP
      if (gpio_reg_val & irqStat.gpioMask[i])
#else
      if (irqStat.supIrq[i] == irq)
#endif
	break;
    }
  if (i >= NO_INT_SUPPORT)
    {
      DPRINTF (KERN_DEBUG
	       " received interrupt is not enabled, something wrong \n");
      return IRQ_NONE;
    }
  spin_lock_irqsave (&lock, irqflags);
  irqData.irqCount[i]++;
#ifdef CONFIG_NLM_XLP
  irqData.recdIrq[i] = irqStat.supIrq[i];
#else
  irqData.recdIrq[i] = irq;
#endif
  spin_unlock_irqrestore (&lock, irqflags);

/* Interrupt clearing is handled in  arch/powerpc/platforms/85xx/XXX.c for eg. p1010rdb.c */
  if (irqStat.regActByIsrOrApp[i]  && ( cpu_irq_process_fptr!=NULL) )
  {
    cpu_irq_process_fptr( irq,i,data,&irqData,&irqStat);
    up (&intSem);
    return IRQ_HANDLED;
  }
  if (irqStat.regActByIsrOrApp[i])
    {
      volatile unsigned char *regAddr1 =
	(volatile unsigned char *) (CPLD_ADDR + irqStat.regOffset1[i]);
      volatile unsigned char *regAddr2 =
	(volatile unsigned char *) (CPLD_ADDR + irqStat.regOffset2[i]);
      unsigned char reg1, reg2;
      if (irqStat.maskVal[i])
	{
	  reg1 = *regAddr1;
	  *regAddr1 = reg1 & irqStat.maskVal[i];	/* clearing particular bit */
	  reg2 = *regAddr2;
	  *regAddr2 = reg2 & irqStat.maskVal[i];	/* clearing particular bit */
	}
      else
	{
	  reg1 = *regAddr1;
	  reg2 = *regAddr2;
	  DPRINTF (KERN_DEBUG
		   "I received the interrupt: %d addr1: %x addr2:%x  offset1:%x offset2:%x reg1:%x reg2:%x  \n",
		   irq, (unsigned int) regAddr1, (unsigned int) regAddr2,
		   irqStat.regOffset1[i], irqStat.regOffset2[i], reg1, reg2);
	}
    }

#ifdef CONFIG_NLM_XLP
  nlm_hal_write_32bit_reg(gpio_mmio, XLP_GPIO_INT_STAT1, irqStat.gpioMask[i]);
  /* The CPLD on Camaro is generating multiple interrupts per module */
  /* insert/remove so only perform a sem give on the first interrupt */
  if (irqData.irqCount[i] == 1)
  {
    up (&intSem);
  }
#else
  up (&intSem);
#endif
  return IRQ_HANDLED;
}

void brcpld_irq_process_register(void *fptr)
{
  cpu_irq_process_fptr = fptr;
}

static struct miscdevice brcpld_dev = {
  .minor = 137,
  .name = "brcpld",
  .fops = &brcpld_fops,
};

static int __init
brcpld_init (void)
{
  int ret = 0;
#ifdef CONFIG_MIPS
#else
  int i = 0;
  struct resource r[2];
  struct device_node *np = NULL;
#endif

  sema_init (&intSem, 0);
  spin_lock_init (&lock);

  ret = misc_register (&brcpld_dev);
  if (ret)
    {
      printk (KERN_DEBUG "Broadcom CPLd: "
	       "Unable to register misc device.\n");
      return ret;
    }
  memset (&irqStat, 0, sizeof (irqStat));
  memset (&irqData, 0, sizeof (irqDat));

#ifdef CONFIG_MIPS
#else
  np = of_find_node_by_name (np, "cpld");
  if (NULL == np)
    {
      printk (KERN_ERR "brcpld_init: Could not find cpld node\n");
      return -1;
    }

  memset (r, 0, sizeof (struct resource) * 2);
  for (i = 0; i < NO_INT_SUPPORT; i++)
    {
      irqStat.supIrq[i] = of_irq_to_resource (np, i, &r[1]);
      DPRINTF (KERN_DEBUG " irq index: %d irqNo%d \n", i, irqStat.supIrq[i]);
    }
#endif
#ifdef CONFIG_NLM_XLP
  gpio_mmio = nlm_hal_get_dev_base(0, 0, XLP_PCIE_GIO_DEV, XLP_GIO_GPIO_FUNC);
#endif

  return 0;
}

static void __exit
brcpld_exit (void)
{
  int i =0;
  for(i=0;i<NO_INT_SUPPORT;i++)
  {
	if(irqStat.enbIrq[i])
		free_irq(irqStat.supIrq[i],(void *)&irqData);
 }	

  misc_deregister (&brcpld_dev);
  DPRINTF (KERN_DEBUG "brcpld: module successfully removed\n");
}

module_init (brcpld_init);
module_exit (brcpld_exit);
EXPORT_SYMBOL(brcpld_irq_process_register);
MODULE_AUTHOR ("Prafulla Kota <prafulla.kota@broadcom.com>");
MODULE_DESCRIPTION ("Broadcom Linux CPLD driver");
MODULE_LICENSE ("GPL");
MODULE_VERSION (CPLD_DRV_VER);
