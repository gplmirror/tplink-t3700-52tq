/*********************************************************************
* <pre>
* LL   VV  VV LL   7777777  (C) Copyright LVL7 Systems 2000-2006
* LL   VV  VV LL   7   77   All Rights Reserved.
* LL   VV  VV LL      77
* LL    VVVV  LL     77
* LLLLL  VV   LLLLL 77      Code classified LVL7 Confidential
* </pre>
**********************************************************************
**********************************************************************
*
* @filename     dim_util.c
*
* @purpose      dual image file utility
*
* @component    dim
*
* @comments     Stand alone utility called by rc.fastpath script to
#               read the contents of the dual image configuration file.
*
* @create       02/02/2007
*
* @author       bradyr
* @end
*
**********************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include "dim.h"

/* #define DEBUG_DIM */
/* ===================== Global Variables ==================== */

static dimImageInfo_t bootImage_g[MAX_BOOT_IMAGES];
static int numImages_g = 0;

int extract=0;
int display=0;
int activate=0;

/* ===================== Function Definitions ================ */

/*********************************************************************
* @purpose  Returns a string ending with a newline character from the 
*           buffer.
*
* @param    src     @b{(input)}  Name of the file containing the image
* @param    line    @b{(output)} Location to fill in the pointer to a 
*                                String ending with a newline 
*
* @returns  number where the next string begins in the buffer
* @returns  -1  if no newline character could be found with in limit 
*
* @notes    
* @end
*********************************************************************/
static int readline(char *src, char **line)
{
    int i = 0;

    *line = NULL;

    while ( (src[i] != '\n') && ( i < DIM_MAX_BOOTCFG_LINE_SIZE))
        i++;

    if( i == DIM_MAX_BOOTCFG_LINE_SIZE)
        return -1;

    *line = src;    /* point to the begining of the null terminated string */

    src[i] = '\0';  /* string is to be null terminated */

    return (i+1);   /* where to begin the next string */
}

/*********************************************************************
* @purpose  Updates the boot config file 
*
* @param    none
* 
* @returns  0   if the file is successfully updated
* @returns  -1  if the file could not be opened for writing
* @returns  -2  if the file could not be written
*
* @notes    A buffer is filled with data from each of the image desc. and 
*           is written to the file. Note that the file is not "updated'
*           but written again. This is because the typical file size 
*           being small (100-200 bytes) and the number of times the
*           file is updated is very few. Therefore the additional logic
*           required to "update' is not preferred.
* @end
*********************************************************************/

static int writeBootCfg(void)
{
    int filedesc;
    int act_size = 0;
    int j = 0, images=0;
    char *pBuf;

    char buffer[MAX_BOOTCFG_SIZE];
    dimImageInfo_t *pImage;

    /* open the boot.dim file from the flash */

    (void)remove (DIM_CFG_FILENAME);

    filedesc = open (DIM_CFG_FILENAME, (O_WRONLY | O_CREAT), 0);

    if ( filedesc < 0 )
    {
        return -1;
    }

    pBuf = &buffer[0];
    strcpy(pBuf,"");

    /* fill the buffer with Image details */

    bzero(pBuf, MAX_BOOTCFG_SIZE);

    for(j = 0; j < MAX_BOOT_IMAGES; j++)
    {
        if(images == numImages_g)
            break;

        pImage = &bootImage_g[j];

        /* if not a valid image , go to the next image*/

        if(strcmp(pImage->fileName, "") == 0x0)
            continue;

        images++;

        pBuf = strcat(pBuf, "---IMAGE---");
        pBuf = strcat(pBuf, "\n");
        pBuf = strcat(pBuf, pImage->currentState);
        pBuf = strcat(pBuf, "\n");
        pBuf = strcat(pBuf, pImage->nextState);
        pBuf = strcat(pBuf, "\n");
        pBuf = strcat(pBuf, pImage->fileName);
        pBuf = strcat(pBuf, "\n");
        pBuf = strcat(pBuf, pImage->numErrors);
        pBuf = strcat(pBuf, "\n");
        pBuf = strcat(pBuf, pImage->descr);
        pBuf = strcat(pBuf, "\n");
        
    } /* end for */

    act_size = strlen(pBuf);

    /* write the file and close it */

    j = write(filedesc, &buffer[0], act_size);

    (void)close (filedesc);

    return j;
}

/*********************************************************************
* @purpose  Reconstructs the boot config file 
*
* @param    none
* 
* @returns  void
*
* @notes    In systems using this utility, if boot.dim is unreadable, 
*           we don't know what was active. Put in the first of 
*           (image1, image2) that exists as active, the other as 
*           backup.
*
* @end
*********************************************************************/
static void reconstructBootCfg(void) {
  int fd;

  fd = open("/mnt/fastpath/image1", O_RDONLY);
  if (fd >= 0) {
    close(fd);
    dimImageAdd("image1");
  }
  fd = open("/mnt/fastpath/image2", O_RDONLY);
  if (fd >= 0) {
    close(fd);
    dimImageAdd("image2");
  }
}


/*********************************************************************
* @purpose  Initializes the dual boot image manager
*
* @param    none
*
* @returns  DIM_SUCCESS  on successful initialization
* @returns  DIM_IMAGE_DOESNOT_EXIST  if file doesn't exist or no 
*                                    images canbe found
* @return   DIM_FAILURE  on file system errors
*
* @notes    Reads any existing boot config file and updates internals.
*           If no such file exists, it simply returns after initializing
*           the number of images to be 0. File will be added when an
*           image is added to the list.
*           When called from a bootloader context (isBoot != 0), this 
*           function copies the nextStates of each of the images to to
*           their currentStates.Thus, the activated image becomes the 
*           active image for the new session.
*
* @end
*********************************************************************/
int dimInitialize(void)
{
    int filedesc;
    int act_size = 0;
    int i = 0, image_index = 0, j = 0;
    char *line;
    char *pBuf;

    char buffer[MAX_BOOTCFG_SIZE];
    dimImageInfo_t *pImage;

    /* clear the existing information */

    for(i = 0; i < MAX_BOOT_IMAGES; i++)
    {
        memset((void *) & bootImage_g[i], 0, sizeof(dimImageInfo_t));
    }

    numImages_g = 0;

    /* open the boot.dim file from the flash */

    filedesc = open (DIM_CFG_FILENAME, O_RDONLY, 0);

    if ( filedesc < 0 )
    {
      reconstructBootCfg();
      /* Might as well forge ahead */
      return DIM_SUCCESS;
    }

    bzero(&buffer[0], MAX_BOOTCFG_SIZE);

    /* read the file into the buffer */

    act_size =  (int)read (filedesc, buffer, MAX_BOOTCFG_SIZE);

    if (act_size <= 0 )
    {
      close(filedesc);
      reconstructBootCfg();
      /* Might as well forge ahead */
      return DIM_SUCCESS;
    }

    /* parse the buffer into the internal data structures */
    
    i = j = 0;

    pBuf = &buffer[0];
    image_index = 0;

    while(( i = readline(pBuf, &line)) > 0) 
    {
        pBuf += i;

        /* check if this line indicates the begining of a new image description */

        if(strcmp(line, "---IMAGE---") == 0)
        {
            /* Go to the next image and continue with its details */

            image_index++;

            if(image_index > MAX_BOOT_IMAGES)
                break;

            numImages_g++;

            j = 0;
            
            continue; 
        }

        /* Populate the Image details with the data from boot.dim */
        /* the image's next state will now be the current state for the 
         * image. the images next state will also has to be set
         * accordingly
         * */

        pImage = &bootImage_g[image_index - 1];

        switch (j) 
        {
            case 0  : strcpy(pImage->currentState, line); 
                      break;
            case 1  : strcpy(pImage->nextState, line); 
                      break;
            case 2  : strcpy(pImage->fileName, line); 
                      break;                      
            case 3  : strcpy(pImage->numErrors, line); 
                      break;
            case 4  : strcpy(pImage->descr, line); 
                      break;
            default :
                      break;
        }

        j++;
    }

    /* all the description is read into the memory */

    (void)close (filedesc);

    /* adjust the current and next states for the images 
     * if the next state and the current state of the image is not the same
     * it indicates that there is a re-boot since an image is made
     * active. 
     */  

    for(image_index = 0; image_index < numImages_g; image_index++)
    {
      pImage = &bootImage_g[image_index];

      strcpy(pImage->currentState, pImage->nextState);
    }

    /* we made some modifications, write back the configuration file */

    writeBootCfg();

    return DIM_SUCCESS;
}

/*********************************************************************
* @purpose  Retrieves the file name for the current active image 
*           
* @param    fileName    @b{(output)}  Location to copy the  
*                                     file name for the active image
*
* @returns  0   on successful execution
* @returns  -1  if no image is marked as active yet
*
* @notes    File name is copied.
*
* @end
*********************************************************************/
int dimActiveImageFileNameGet(char *fileName)
{
    int index = 0;
    
    /* If no images are added, return error */

    if(numImages_g == 0)
        return -1;

    /* If no image is marked as active, return error */
    
    for(index = 0; index < MAX_BOOT_IMAGES; index++)
    {
        if(strcmp(bootImage_g[index].currentState, "active") == 0x0)
            break;
    }
    
    if(index == MAX_BOOT_IMAGES)
        return -1;

    /* copy the file name */
    
    strcpy(fileName,bootImage_g[index].fileName);
    return 0;
}

/*********************************************************************
* @purpose  Retrieves the file name for the current backup image 
*           
* @param    fileName    @b{(output)}  Location to copy file name of the  
*                                     backup image
*
* @returns  0   on successful execution
* @returns  -1  if there is no backup image yet
*
* @notes    File name is copied.
*
* @end
*********************************************************************/
int dimBackupImageFileNameGet(char *fileName)
{
    int index = 0;

    /* If no images are added, return error */

    if(numImages_g == 0)
        return -1;

    /* If no image is marked as active, return error */
    
    for(index = 0; index < MAX_BOOT_IMAGES; index++)
    {
        if(strcmp(bootImage_g[index].currentState, "backup") == 0x0)
            break;
    }
    
    if(index == MAX_BOOT_IMAGES)
        return -1;

    /* copy the file name */
    
    strcpy(fileName,bootImage_g[index].fileName);
    return 0;
}

/*********************************************************************
* @purpose  Sets the supplied image as the active image for the 
*           subsequent re-boots
*           
* @param    fileName    @b{(input)}  image to be activated
*
* @returns  DIM_SUCCESS   on successful execution
* @returns  DIM_IMAGE_DOESNOT_EXIST  if the specified image could not be found
* @returns  DIM_INVALID_IMAGE  if the specified image is not the backup image
* @return   DIM_IMAGE_ACTIVE   if the backup image is already active
* @return   DIM_FAILURE        on internal error
*
* @notes    Internal data structures and the boot configuration file
*           are updated with the information. Image currently  
*           active is marked as backup and the specified image is 
*           marked as active. the number of errors for the backup 
*           image are cleared to 0. 
*
* @end
*********************************************************************/
int dimImageActivate(char *fileName)
{
    int index = 0, def_index = 0;
    
    /* If there are no images, return error */
    
    if(numImages_g == 0)
        return DIM_IMAGE_DOESNOT_EXIST;

    /* Verify that the image exists. Otherwise return error */

    if ((strcmp(fileName, DIM_IMAGE1_NAME) != 0) &&
        (strcmp(fileName, DIM_IMAGE2_NAME) != 0))
    {
      return DIM_IMAGE_DOESNOT_EXIST;
    }

    for(index = 0; index < MAX_BOOT_IMAGES; index++)
    {
        if(strcmp(bootImage_g[index].fileName, fileName) == 0x0)
            break;
    }

    if(index == MAX_BOOT_IMAGES)
        return DIM_IMAGE_DOESNOT_EXIST;

    /* verify that the image is a backup image */

    /*if(strcmp(bootImage_g[index].currentState, "backup") != 0x0)
        return DIM_INVALID_IMAGE; */


    /* verify that the image is not activated */

    if(strcmp(bootImage_g[index].nextState, "active") == 0x0)
        return DIM_IMAGE_ACTIVE;


    /* check if there is an image currently marked active */
    /* if so, mark it backup                              */

    for(def_index = 0; def_index < MAX_BOOT_IMAGES; def_index++)
    {
        if(strcmp(bootImage_g[def_index].nextState, "active") == 0x0)
            break;
    }

    if (def_index <  MAX_BOOT_IMAGES)
    {
        strcpy(bootImage_g[def_index].nextState, "backup");
        strcpy(bootImage_g[def_index].numErrors, "0");
    }

    /* Activate the current backup image */

    strcpy(bootImage_g[index].numErrors, "0");
    strcpy(bootImage_g[index].nextState, "active");

    /* update the current boot configuration file */

    writeBootCfg();

    return DIM_SUCCESS;
}


/*********************************************************************
* @purpose  Adds an image to the list of images 
*           
* @param    fileName    @b{(input)}  file for the backup image 
*
* @returns  0   on successful execution
* @returns  -1  if the allowed max number of images reached already
* 
* @notes    Internal data structures and the boot configuration file
*           are updated with the information. 
*
*           if there are no images existing, this will be added as the
*           active image.
*           if there are already images, this will be added as the 
*           backup image.
*           If a backup image is actiavted, it will not be overwritten.
*
* @end
*********************************************************************/
int dimImageAdd(char *fileName)
{
    int index = 0;

    /* The image names are fixed.
     * check if this name is one of the allowed names
     */ 
    

    if ((strcmp(fileName, DIM_IMAGE1_NAME) != 0) &&
        (strcmp(fileName, DIM_IMAGE2_NAME) != 0))
    {
      return -3;
    }

    if(numImages_g == 0)
    {
        numImages_g++;
        
        /* copy the image details */

        strcpy(bootImage_g[0].fileName, fileName);
        strcpy(bootImage_g[0].numErrors, "0");
        strcpy(bootImage_g[0].currentState, "active");
        strcpy(bootImage_g[0].nextState, "active");
        strcpy(bootImage_g[0].descr, "default image");

        /* update the bootcfg */

        writeBootCfg();
        return 0;
    }

    /* check if any file exists with the same name */
    /* if so return */

    for(index = 0; index < MAX_BOOT_IMAGES; index++)
    {
        if(strcmp(bootImage_g[index].fileName, fileName) == 0x0)
        {
            return 0;
        }
    }
    
    /* look for an empty slot and mark the image as backup */

    for(index = 0; index < MAX_BOOT_IMAGES; index++)
    {
      if(strcmp(bootImage_g[index].fileName, "") == 0x0)
      {
        numImages_g++;

        /* copy the image details */

        strcpy(bootImage_g[index].fileName, fileName);
        strcpy(bootImage_g[index].numErrors, "0");
        strcpy(bootImage_g[index].currentState, "backup");
        strcpy(bootImage_g[index].nextState, "backup");
        strcpy(bootImage_g[index].descr, "");

        /* update the bootcfg */

        writeBootCfg();

        return 0;
      }
    }

    return -1;
}

/*********************************************************************
* @purpose  Displays the boot configuration file 
*           
* @param    none
*
* @returns  0   on success
*           -1  on file system errors
*
* @notes    
*
* @end
*********************************************************************/
int dimShowBootCfg(void)
{
  int filedesc;
  int act_size = 0;
  int i = 0, j = 0;
  char *line;
  char *pBuf;

  char buffer[1024] = { 0 };

    bzero(buffer, sizeof(buffer));

  /* open the boot.dim file from the flash */

  filedesc = open (DIM_CFG_FILENAME, O_RDONLY, 0);

  if ( filedesc == -1 )
  {
    /* the file is empty. We need to formulate this */
    printf(" boot.dim doesn't exist on the disk \n");
    return -1;
  }

  /* read the file into the buffer */

  act_size =  (int)read (filedesc, buffer, MAX_BOOTCFG_SIZE);

  if (act_size == -1 )
  {
    printf(" boot.dim read error\n");
    close(filedesc);
    return -2;
  }

    printf("\nsize is %d \n", act_size);

  if (act_size == 0)
  {
    printf(" boot.dim empty\n");
    close(filedesc);
    return -3;
  }

  pBuf = &buffer[0];
  j = 0;

  while(( i = readline(pBuf, &line)) > 0) 
  {
    j++;
    printf("boot.dim %d :\t\t %s \n",j,line);
    pBuf += i;
  }

  /* all the description is read into the memory */

  (void)close (filedesc);

  return 0;
}

/* --------------------------------------------------------------------- */

void usage(void) {
  printf("\nusage: dim_util [<options>]*\n\n"
	 "Where <options> can be:\n\n"
	 "  -h                 display this text\n"
	 "  -r                 read active image file name\n"
	 "  -d                 display contains dim file\n\n"
	 "  -a                 activate backup image \n\n");
  exit(0);
};

int main(int argc, char* argv[]) {
  
  int arg=1;
  char* s;
  char* cmd;
  char activeFileName[DIM_MAX_FILENAME_SIZE];
  char backupFileName[DIM_MAX_FILENAME_SIZE];
  struct stat backupStat;
  int  rc = 1;  /* Default value is image1 */

  if(argc==1) {
    usage();
    exit(0);
  };


  while(arg<argc) {
    
    if(strcmp("-h",argv[arg])==0) {
      usage();
    };
       
    if(strcmp("-r",argv[arg])==0) {
      extract = 1;
    };

    if(strcmp("-d",argv[arg])==0) {
      display = 1;
    };

    if(strcmp("-a",argv[arg])==0) {
      activate = 1;
    };
    arg++;
  };

  if(display) {
    dimShowBootCfg();
  }

  if(extract) {
    if ((dimInitialize() == DIM_SUCCESS) &&
       (dimActiveImageFileNameGet(activeFileName) == DIM_SUCCESS)) {
      if (strncmp(activeFileName, DIM_IMAGE2_NAME, sizeof(DIM_IMAGE2_NAME)) == 0) {
        rc = 2;
      }
#ifdef DEBUG_DIM
      printf("activeFileName = %s, rc = %d\n", activeFileName, rc);
#endif
    
    } else {
      printf("Unable to read dim file.\n");
    }
  }

  if(activate) {
    if ((dimInitialize() == DIM_SUCCESS) &&
       (dimBackupImageFileNameGet(backupFileName) == DIM_SUCCESS)) {
#ifdef DEBUG_DIM
      printf("dimBackupImageFileNameGet returned %d, backup image = %s\n", rc, backupFileName);
#endif
      if ((stat(backupFileName, &backupStat) == 0) &&
          (dimImageActivate(backupFileName) == DIM_SUCCESS)) {
        if (strncmp(backupFileName, DIM_IMAGE2_NAME, sizeof(DIM_IMAGE2_NAME)) == 0) {
          rc = 2;
        }
        dimInitialize(); /* Move next state to current state */
#ifdef DEBUG_DIM
        printf("Backup image - %s activated, returning %d\n", backupFileName, rc);
#endif
      }
    } else {
      printf("Unable to read backup image file name.\n");
    }
  }

  return rc;
};
