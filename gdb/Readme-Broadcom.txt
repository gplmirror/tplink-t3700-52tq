#
# Instructions to build gdbclient(cross-gdb) & gdbserver
#

GDB Source: Download the gdbsource from web
Note: GDB tarball is copied onto the tree instead of direct source due to some files permission issues.
-------------------------------------------------------------------------------------------------------------------------------------
                            gbdclient
	(host is serverpc(x86_64-pc-linux-gnu), target(powerpc-wrs-linux-gnu/mips-wrs-linux-gnu)
-------------------------------------------------------------------------------------------------------------------------------------
Configure the host as x86_64 & target as powerpc/mips

Ex: 
[gdb-7.3]$./configure --host=x86_64-pc-linux-gnu --target=powerpc-wrs-linux-gnu
[gdb-7.3]$./configure --host=x86_64-pc-linux-gnu --target=mips-wrs-linux-gnu

	==> It may results errors related to "x86_64-linux-gnu-ar not found" ==> x86_64 host has ar link set to /tools/bin/ar , It requires to replace "x86_64_gnu_linux_ar" to "ar", in Main Makefiles

[gdb-7.3]$bsub -R r64bit -R rhel50 -I -q hyd-nwsoft make all-gdb -j4

output:
-------  
[prafulla@xl-hyd-11 gdb-7.3]$ll gdb/gdb
-rwxrwxr-x 1 prafulla clearusers 18602567 Aug 19 13:26 gdb/gdb

Rename gdb to cross-gdb and copy to respective target platform toolchain:
EX: /projects/nwsoft-toolchains/wrl_3.0/gto

-------------------------------------------------------------------------------------------------------------------------------------
                                        gbdserver
-------------------------------------------------------------------------------------------------------------------------------------
Configure the host as x86_64 & target as powerpc/mips

Ex:
[gdb-7.3]$./configure --host=x86_64-pc-linux-gnu --target=powerpc-wrs-linux-gnu
[gdb-7.3]$./configure --host=x86_64-pc-linux-gnu --target=mips-wrs-linux-gnu

cd gdb/gdbserver
provide CC path for respective target(ppc/mips)
[gdbserver]bsub -R r64bit -R rhel50 -I -q hyd-nwsoft make -j4 CC=/projects/nwsoft-toolchains/wrl_3.0/raptor/host-cross/mips-wrs-linux-gnu/bin/mips-wrs-linux-gnu-mips_softfp-glibc_std-gcc

output: gdbserver
[prafulla@xl-hyd-11 gdbserver]$ll gdbserver
-rwxrwxr-x 1 prafulla clearusers 645266 Aug 17 17:50 gdbserver

Ex: copy the gdbserver to respective target folder(e500v1,e500v2,mips32, mips74k)  : V:\prafulla-int_fastpath_6.4-xgs4-gto-wrl3_0\bsp\cpu\common\wrl_3.0\e500_v2

gdbserver requires libdl library for execution, Copy the respective target libdl to target folder 
EX: copy the Z:\projects\nwsoft-toolchains\wrl_3.0\gto\build\libc_bin\lib\libdl-2.8.so ==> V:\prafulla-int_fastpath_6.4-xgs4-gto-wrl3_0\bsp\cpu\common\wrl_3.0\e500_v2

-------------------------------------------------------------------------------------------------------------------------------------
			Modifications to fastpath files
------------------------------------------------------------------------------------------------------------------------------------
edit the cpiolist{decides what to be present under root file system}

file /lib/libdl-2.8.so rootfs/../../../../common/wrl_3.0/e500_v2/libdl-2.8.so 0755 0 0
slink /lib/libdl.so.2 libdl-2.8.so 0777 0 0 file /usr/bin/gdbserver rootfs/../../../../common/wrl_3.0/e500_v2/gdbserver 0755 0 0

-------------------------------------------------------------------------------------------------------------------------------------
                                        gbdserver & cross-gdb execution
-------------------------------------------------------------------------------------------------------------------------------------


http://www.rdub.broadcom.com/twiki/bin/view/Know/BroadcomLinuxFAQ
Debugging using GDB server 

In order to use the gdbserver for debugging purpose,
	 1) enable Networking and 2) GDB server in the FASTPATH Startup Debug menu.
		 When you choose to enable Networking you will be prompted to enter an IP address for the service port and a gateway.
  		When you choose to enable GDB server you will be prompted to enter the gdb target IP and port in the format 		specified. 

After specifying the information, quit from the debug menu and choose option 1 �Start FASTPATH Application�. Unlike regular startup, it would now start the application on gdbserver and wait for the gdb client to connect. 

On the host, run gdb from the same directory as the compiler, for example /projects/nwsoft-toolchains/wrl_3.0/gto/cross-gdb. 

If you run gdb from some subdirectory of objects (like <output directory>/objects/andl) then gdb should find all of the source files correctly. 

After starting gdb, use these two commands (editing the toolchain path as appropriate) to tell it where to load symbols and find the shared libraries: 

file ../../ipl/switchdrvr 

set solib-absolute-prefix /projects/nwsoft-toolchains/wrl_2.0/gto/export/dist/ 

Then "target remote x.x.x.x:y" will connect and allow you to start debugging
 copy the Z:\projects\nwsoft-toolchains\wrl_3.0\gto\build\libc_bin\lib\libdl-2.8.so 
          to : V:\prafulla-int_fastpath_6.4-xgs4-gto-wrl3_0\bsp\cpu\common\wrl_3.0\e500_v2 (Ex)

-------------------------------------------------------------------------------------------------------------------------------------
					Fastpath
-------------------------------------------------------------------------------------------------------------------------------------

Select option (1-11 or Q): d

Debug Options Menu

Telnet daemon is currently ENABLED (port 2323).
Halt on crash detect is currently DISABLED.
MALLOC_CHECK_ is currently DISABLED.
GDB server is currently ENABLED (tty 10.130.187.30:6011).
Networking is currently ENABLED (ip 10.130.187.30 netmask 255.255.0.0 gw 10.130.187.1).

1 - Enable/Disable Telnet daemon
2 - Enable/Disable Halt on crash detection
3 - Enable/Disable MALLOC_CHECK_
4 - Enable/Disable GDB server
5 - Enable/Disable Networking
6 - Configure GCOV
0 - Exit without change

Select option (1-6 or 0): 0
No change.

FASTPATH Startup Rev: 6.3
