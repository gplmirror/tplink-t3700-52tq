#ifndef BR_CPLD_H
#define BR_CPLD_H

#include <asm/types.h>

#define CPLD_DRV_VER            "R01.00"
/***********************************IOCTL Calls *********************************/
#define NO_INT_SUPPORT  10	/* This value is applicable for both fastpath & kernel, requires modification at both sides for any update */
struct __brcpld_waitForInt
{
  unsigned int intCount[NO_INT_SUPPORT];	/* From driver ==> irqCount */
  unsigned int irqNo[NO_INT_SUPPORT];	/* From driver ==> provides the irqNo from which the irq received */
  unsigned int reg1[NO_INT_SUPPORT]; 
  unsigned int reg2[NO_INT_SUPPORT];
};

struct __brcpld_irqDetails
{
  unsigned int irqNo;		/* irqNo will be requsted for registration */
  unsigned int regOffset1;	/* reg offset to read/write for status clear */
  unsigned int regOffset2;	/* reg offset to read/write for status clear */
  unsigned int regActByIsrOrApp;	/* reg act either read/write by ISR or App */
  unsigned int maskVal;		/* if maskVal =0, ISR should read else ISR wrill write maksval on register */
};


#define BRCPLD_IOC_MAGIC                'b'
#define BRCPLD_REG_IRQ                  _IOWR(BRCPLD_IOC_MAGIC, 0, struct __brcpld_irqDetails *)
#define BRCPLD_WAIT_FOR_INTERRUPT       _IOWR(BRCPLD_IOC_MAGIC, 1, struct __brcpld_waitForInt *)
#define BRCPLD_EN_IRQ                   _IOWR(BRCPLD_IOC_MAGIC, 2, unsigned int *)
#define BRCPLD_DIS_IRQ                  _IOWR(BRCPLD_IOC_MAGIC, 3, unsigned int *)
/***********************************IOCTL Calls *********************************/



#ifdef __KERNEL__
/* supported, enabled(fromApp),regOffest1&2,regAccessByISROrApplication,maskValue(0 for read, any other forWrite) */
typedef struct __irqNos
{

  unsigned int supIrq[NO_INT_SUPPORT];
  unsigned int enbIrq[NO_INT_SUPPORT];
  unsigned int regOffset1[NO_INT_SUPPORT];
  unsigned int regOffset2[NO_INT_SUPPORT];
  unsigned int regActByIsrOrApp[NO_INT_SUPPORT];	/* reg act either read/write by ISR or App */
  unsigned int maskVal[NO_INT_SUPPORT];	/* if maskVal =0, ISR should read else ISR wrill write maksval on register */
  unsigned int gpioMask[NO_INT_SUPPORT];
} irqNos;


typedef struct __irqData
{

  unsigned int irqCount[NO_INT_SUPPORT];
  unsigned int recdIrq[NO_INT_SUPPORT];	/* if maskVal =0, ISR should read else ISR wrill write maksval on register */
  unsigned int reg1[NO_INT_SUPPORT]; 
  unsigned int reg2[NO_INT_SUPPORT];
} irqDat;
#endif

#endif
