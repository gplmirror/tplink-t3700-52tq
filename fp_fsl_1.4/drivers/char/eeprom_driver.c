#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/ctype.h>
#include <linux/hwmon-sysfs.h>
#include <linux/mutex.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/err.h>
#include <linux/firmware.h>
#include <linux/delay.h>

//#define EEPROM_DEBUG

#define AGING_BASE_ADDR 0x1800
#define ERR_MAX_ITEMS 50
#define EEPROM_REFRESH_INTERVAL (15 * HZ)
u16 aging_base = AGING_BASE_ADDR;

MODULE_LICENSE("GPL");

static struct class *eeprom_class;
static struct i2c_client *eeprom_client;
static struct eeprom {
	u8 id[4];
	u8 pro_name[12];
	u8 errata[5];
	u8 date[6];
	u8 res_0;
	u32 version;
	u8 tempcal[8];
	u8 tempcalsys[2];
	u8 tempcalflags;
	u8 sn[21];
	u8 mac_count;
	u8 mac_flag;
	u8 mac[8][6];
	u32 crc;
}e;
static struct update_info {
	unsigned long pro_name_last_updated;
	unsigned long date_last_updated;
	unsigned long sn_last_updated;
	unsigned long mac0_last_updated;
	unsigned long mac1_last_updated;
	unsigned long mac2_last_updated;
}eeprom_update;
typedef struct err_items {
	u8 id;
	u8 err_count;
	s8 err_code;
	u8 err_time;
} ERROR_ITEMS;

DEFINE_MUTEX(eeprom_mutex);

static const struct i2c_device_id i2cbus0_deviceid[] = {
	{"eeprom", 0,},

};

extern s32 i2c_eeprom_xfer_read(struct i2c_adapter *adapter, u16 addr, u16 reg, int flag, u8 *data, u16 size);

static ssize_t eeprom_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int i;
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	
	res = i2c_eeprom_xfer_read(adapter, 0x50, 0x0000, 1, (void *)&e, sizeof(e));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	printk(KERN_ALERT "ID: %c%c%c%c \n", e.id[0], e.id[1], e.id[2], e.id[3]);
	printk(KERN_ALERT "serial_number: %s\n", e.sn);
	printk(KERN_ALERT "pro_name: %s\n", e.pro_name);
	printk(KERN_ALERT "Errata: %s\n", e.errata);
	printk(KERN_ALERT "Build date: 20%02x/%02x/%02x %02x:%02x:%02x %s\n", 
					   e.date[0], e.date[1], e.date[2], e.date[3] & 0x7f, 
					   e.date[4], e.date[5], e.date[3] & 0x80 ? "PM" : "");
	for (i = 0; i < min(e.mac_count, 8); i++) {
		u8 *p = e.mac[i];
		printk(KERN_ALERT "Eth%u: %02x:%02x:%02x:%02x:%02x:%02x\n", i, p[0], p[1], p[2], p[3], p[4], p[5]);
	}
	mutex_unlock(&eeprom_mutex);
	
	return 0;
}


static ssize_t serial_number_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;
	unsigned long local_jiffies = jiffies;

	mutex_lock(&eeprom_mutex);
	if(time_before(local_jiffies, eeprom_update.sn_last_updated + EEPROM_REFRESH_INTERVAL) && eeprom_update.sn_last_updated != 0)
		goto out;
	res = i2c_eeprom_xfer_read(adapter, 0x50, e.sn - e.id, 1, (void *)e.sn, sizeof(e.sn));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	eeprom_update.sn_last_updated = local_jiffies;

out:
	mutex_unlock(&eeprom_mutex);
		
	return sprintf(buf, "%s\n", e.sn);
}

static ssize_t date_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;
	unsigned long local_jiffies = jiffies;

	mutex_lock(&eeprom_mutex);
	if(time_before(local_jiffies, eeprom_update.date_last_updated + EEPROM_REFRESH_INTERVAL) && eeprom_update.date_last_updated != 0)
		goto out;
	
	res = i2c_eeprom_xfer_read(adapter, 0x50, e.date - e.id, 1, (void *)e.date, sizeof(e.date));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	eeprom_update.date_last_updated = local_jiffies;

out:
	mutex_unlock(&eeprom_mutex);
		
	return sprintf(buf, "build date: 20%02x/%02x/%02x %02x:%02x:%02x %s\n", e.date[0], e.date[1], e.date[2], e.date[3] & 0x7f, e.date[4], e.date[5], e.date[3] & 0x80 ? "PM" : "");
}

static ssize_t switch0_mac_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;
	unsigned long local_jiffies = jiffies;

	mutex_lock(&eeprom_mutex);
	if(time_before(local_jiffies, eeprom_update.mac0_last_updated + EEPROM_REFRESH_INTERVAL) && eeprom_update.mac0_last_updated != 0)
		goto out;
	
	res = i2c_eeprom_xfer_read(adapter, 0x50, e.mac[0] - e.id, 1, (void *)e.mac[0], sizeof(e.mac[0]));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	eeprom_update.mac0_last_updated = local_jiffies;

out:
	mutex_unlock(&eeprom_mutex);
		
	return sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x\n", e.mac[0][0], e.mac[0][1], e.mac[0][2], e.mac[0][3], e.mac[0][4], e.mac[0][5]);
}

static ssize_t cpu_mac_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;
	unsigned long local_jiffies = jiffies;

	mutex_lock(&eeprom_mutex);
	if(time_before(local_jiffies, eeprom_update.mac1_last_updated + EEPROM_REFRESH_INTERVAL) && eeprom_update.mac1_last_updated != 0)
		goto out;
	
	res = i2c_eeprom_xfer_read(adapter, 0x50, e.mac[1] - e.id, 1, (void *)e.mac[1], sizeof(e.mac[1]));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	eeprom_update.mac1_last_updated = local_jiffies;

out:
	mutex_unlock(&eeprom_mutex);
		
	return sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x\n", e.mac[1][0], e.mac[1][1], e.mac[1][2], e.mac[1][3], e.mac[1][4], e.mac[1][5]);
}

static ssize_t switch1_mac_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;
	unsigned long local_jiffies = jiffies;

	mutex_lock(&eeprom_mutex);
	if(time_before(local_jiffies, eeprom_update.mac2_last_updated + EEPROM_REFRESH_INTERVAL) && eeprom_update.mac2_last_updated != 0)
		goto out;
	
	res = i2c_eeprom_xfer_read(adapter, 0x50, e.mac[2] - e.id, 1, (void *)e.mac[2], sizeof(e.mac[2]));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	eeprom_update.mac2_last_updated = local_jiffies;

out:
	mutex_unlock(&eeprom_mutex);
		
	return sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x\n", e.mac[2][0], e.mac[2][1], e.mac[2][2], e.mac[2][3], e.mac[2][4], e.mac[2][5]);
}

static ssize_t aging_init_store(struct class *cls, const char *buffer, size_t count)
{
	int res;
	int i, len = 8;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);

	for (i = 0; i < count; i += len) {
		if ((count - i ) < 8)
			len = count - i;
		res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + i, 0, (void *)buffer, len * sizeof(char));
		if ( res < 0) {
			printk(KERN_ALERT "aging_init write error!\n");
			mutex_unlock(&eeprom_mutex);
			return -1;
		}
		buffer += len;
		msleep(11);
	}
	mutex_unlock(&eeprom_mutex);

	return count;
}

static ssize_t aging_flag_show(struct class *cls, char *buf)
{
	int res;
	unsigned char value;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);

	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base, 1, (void *)&value, sizeof(u8));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return sprintf(buf, "%d\n", value);
}

static ssize_t aging_flag_store(struct class *cls, const char *buffer, size_t count)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base, 0, (void *)buffer, sizeof(u8));
	if ( res < 0) {
		printk(KERN_ALERT "aging_flag write error!\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return count;
}

static ssize_t pow_off_times_show(struct class *cls, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);

	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 2, 1, (void *)buf, sizeof(u8));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return 1;
}

static ssize_t pow_off_times_store(struct class *cls, const char *buffer, size_t count)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 2, 0, (void *)buffer, sizeof(u8));
	if ( res < 0) {
		printk(KERN_ALERT "pow_off_times write error!\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return count;
}

static ssize_t aging_time_show(struct class *cls, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);

	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 4, 1, (void *)buf, sizeof(u16));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return sizeof(u16);
}

static ssize_t aging_time_store(struct class *cls, const char *buffer, size_t count)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 4, 0, (void *)buffer, sizeof(u16));
	if ( res < 0) {
		printk(KERN_ALERT "aging_time write error!\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return count;
}

static ssize_t remain_time_show(struct class *cls, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);

	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 6, 1, (void *)buf, sizeof(u16));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return sizeof(u16);
}

static ssize_t remain_time_store(struct class *cls, const char *buffer, size_t count)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 6, 0, (void *)buffer, sizeof(u16));
	if ( res < 0) {
		printk(KERN_ALERT "remain_time write error!\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return count;
}

static ssize_t error_items_show(struct class *cls, char *buf)
{
	int res;
	int i;
	int len = 0;
	ERROR_ITEMS *items;

	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	items = (ERROR_ITEMS *)kmalloc(sizeof(ERROR_ITEMS), GFP_KERNEL);

	for (i = 0; i < ERR_MAX_ITEMS; i++) {
		res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 20 + len, 1, (void *)items, sizeof(ERROR_ITEMS));
		if (res < 0) {
			printk(KERN_ALERT "error_items_show: eeprom read failed\n");
			mutex_unlock(&eeprom_mutex);
			kfree(items);
			return -1;
		}
		strncpy(buf, (char *)items, sizeof(ERROR_ITEMS));
		buf += sizeof(ERROR_ITEMS);
		len += sizeof(ERROR_ITEMS);
		udelay(1000);
	}
	kfree(items);
	mutex_unlock(&eeprom_mutex);

	return len;
}

static ssize_t error_items_store(struct class *cls, const char *buffer, size_t count)
{
	int res;
	ERROR_ITEMS *items = (ERROR_ITEMS *)buffer;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	res = i2c_eeprom_xfer_read(adapter, 0x50, aging_base + 20 + (items->id - 1) * sizeof(ERROR_ITEMS), 0, (void *)items, sizeof(ERROR_ITEMS));
	if ( res < 0) {
		printk(KERN_ALERT "error_items write error!\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	mutex_unlock(&eeprom_mutex);

	return count;
}

static ssize_t pro_name_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int res;
	struct i2c_adapter *adapter = eeprom_client->adapter;
	unsigned long local_jiffies = jiffies;

	mutex_lock(&eeprom_mutex);
	if(time_before(local_jiffies, eeprom_update.pro_name_last_updated + EEPROM_REFRESH_INTERVAL) && eeprom_update.pro_name_last_updated != 0)
		goto out;
	
	res = i2c_eeprom_xfer_read(adapter, 0x50, e.pro_name - e.id, 1, (void *)e.pro_name, sizeof(e.pro_name));
	if (res < 0) {
		printk(KERN_ALERT "eeprom read failed\n");
		mutex_unlock(&eeprom_mutex);
		return -1;
	}
	eeprom_update.pro_name_last_updated = local_jiffies;

out:
	mutex_unlock(&eeprom_mutex);
		
	return sprintf(buf, "%s\n", e.pro_name);
}

static ssize_t eeprom_test_show(struct class *cls, char *buf)
{
	int res;
	int i, len = 8, count = 256;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	for(i = 0; i < count; i += len) {
		if((count - i ) < 8)
			len = count -i;
		res = i2c_eeprom_xfer_read(adapter, 0x50, 0x1000 + i, 1, (void *)buf, len * sizeof(char));
		if( res < 0) {
			printk(KERN_ALERT "eeprom_test_show read error!\n");
			mutex_unlock(&eeprom_mutex);
			return -1;
		}
		buf += len;
		msleep(11);
	}

	mutex_unlock(&eeprom_mutex);

	return count;
}

static ssize_t eeprom_test_store(struct class *cls, const char *buffer, size_t count)
{
	int res;
	int i, len = 8;
	struct i2c_adapter *adapter = eeprom_client->adapter;

	mutex_lock(&eeprom_mutex);
	for(i = 0; i < count; i += len) {
		if((count - i ) < 8)
			len = count -i;
		res = i2c_eeprom_xfer_read(adapter, 0x50, 0x1000 + i, 0, (void *)buffer, len * sizeof(char));
		if( res < 0) {
			printk(KERN_ALERT "eeprom_test_store write error!\n");
			mutex_unlock(&eeprom_mutex);
			return -1;
		}
		buffer += len;
		msleep(11);
	}
	mutex_unlock(&eeprom_mutex);

	return count;
}

static CLASS_ATTR(eeprom, S_IRUGO | S_IWUSR, eeprom_show, NULL);
static CLASS_ATTR(serial_number, S_IRUGO | S_IWUSR, serial_number_show, NULL);
static CLASS_ATTR(date, S_IRUGO | S_IWUSR, date_show, NULL);
static CLASS_ATTR(switch0_mac, S_IRUGO | S_IWUSR, switch0_mac_show, NULL);
static CLASS_ATTR(cpu_mac, S_IRUGO | S_IWUSR, cpu_mac_show, NULL);
static CLASS_ATTR(switch1_mac, S_IRUGO | S_IWUSR, switch1_mac_show, NULL);
static CLASS_ATTR(pro_name, S_IRUGO | S_IWUSR, pro_name_show, NULL);
static CLASS_ATTR(aging_init, S_IRUGO | S_IWUSR, NULL, aging_init_store);
static CLASS_ATTR(aging_flag, S_IRUGO | S_IWUSR, aging_flag_show, aging_flag_store);
static CLASS_ATTR(error_items, S_IRUGO | S_IWUSR, error_items_show, error_items_store);
static CLASS_ATTR(pow_off_times, S_IRUGO | S_IWUSR, pow_off_times_show, pow_off_times_store);
static CLASS_ATTR(aging_time, S_IRUGO | S_IWUSR, aging_time_show, aging_time_store);
static CLASS_ATTR(remain_time, S_IRUGO | S_IWUSR, remain_time_show, remain_time_store);
static CLASS_ATTR(eeprom_test, S_IRUGO | S_IWUSR, eeprom_test_show, eeprom_test_store);


static struct class_attribute *attrgroup[] = {
	&class_attr_eeprom,
	&class_attr_serial_number,
	&class_attr_date,
	&class_attr_switch0_mac,
	&class_attr_cpu_mac,
	&class_attr_switch1_mac,
	&class_attr_pro_name,
	&class_attr_aging_init,
	&class_attr_aging_flag,
	&class_attr_error_items,
	&class_attr_pow_off_times,
	&class_attr_aging_time,
	&class_attr_remain_time,
	&class_attr_eeprom_test,
	NULL,
};

static int eeprom_remove(struct i2c_client *client)
{
	class_remove_file(eeprom_class, attrgroup[0]);
	i2c_unregister_device(eeprom_client);

	return 0;
}

static int eeprom_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int i=0;
	int status = 0;
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		printk(KERN_ALERT "i2c bus does not suuport the eeprom\n");
		return -1;
	}
	while (attrgroup[i]) {
		status = class_create_file(eeprom_class, attrgroup[i++]);
		if(status)
			goto exit_free;	
	}
	
	eeprom_update.pro_name_last_updated = 0;
	eeprom_update.date_last_updated = 0;
	eeprom_update.sn_last_updated = 0;
	eeprom_update.mac0_last_updated = 0;
	eeprom_update.mac1_last_updated = 0;
	eeprom_update.mac2_last_updated = 0;
	eeprom_client = client;
	if (eeprom_client == NULL) {
		printk(KERN_ALERT ":eeprom_client is NULL error!\n");
		return -1;
	}	

	return 0;
exit_free:
	printk(KERN_ALERT "eeprom probe error!\n");
	return status;
}

static struct i2c_driver eeprom_driver = {
	.driver = {
		.name = "eeprom"
	},
	.probe 		= eeprom_probe,
	.remove		= eeprom_remove,
	.id_table	= i2cbus0_deviceid,
};

static int __init smallstone_eeprom_init(void)
{
	eeprom_class = class_create(THIS_MODULE, "eeprom");
	if(IS_ERR(eeprom_class))
		return PTR_ERR(eeprom_class);
	
	return i2c_add_driver(&eeprom_driver);
}

static void __exit smallstone_eeprom_exit(void)
{
	printk(KERN_ALERT "eeprm driver exit!\n");
	i2c_del_driver(&eeprom_driver);
}

module_init(smallstone_eeprom_init);
module_exit(smallstone_eeprom_exit);
