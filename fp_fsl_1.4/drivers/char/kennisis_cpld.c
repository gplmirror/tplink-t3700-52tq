/*
 * NAS Kernel Module
 * Copyright (c) 2010, Celestica Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


/**************************************************************************
 *version control
 * V1.0
 * Beck implement this kernel module based on API Specification 0.3.
 * All the LED,SystemMonitor,Button,LCM WatchDog driver will be inserted as one module.
 * So it will not bring much impact to current Linux OS from customer.
 ***************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/cdev.h>
#include <linux/workqueue.h>
#include <linux/poll.h>
#include <linux/mutex.h>
#include <linux/fs.h>
#include <linux/jiffies.h>
#include <linux/miscdevice.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
//#include <linux/i2c.h>
//#define KERNEL_32


#include <asm/uaccess.h>   /* copy_*_user */
#include <asm/io.h>
///#include <asm-generic/rtc.h>
//#include <linux/bcd.h>

#include "kennisis_cpld.h"
#include <linux/io.h>
#include <linux/of.h>




MODULE_AUTHOR( "Beck He <bhe@Celestica.com>" );
MODULE_DESCRIPTION( "Kennisis CPLD" );

MODULE_LICENSE( "GPL" );

#define NAME         "kennisis_cpld"
#define KENNISIS_CPLD_MINOR 159
//#define KENNISIS_CPLD_DEBUG

DEFINE_MUTEX(kennisis_cpld_mutex);
static DECLARE_WAIT_QUEUE_HEAD(cpld_wait_queue);
//static struct workqueue_struct *cpld_wq;
//static struct delayed_work  delaywork;
//static int cpld_users = 0;
static int cpld_used = 0;
static struct class * kennisis_cpld_class;









#ifdef CONFIG_PHYS_64BIT
#define  CONFIG_IMMR        0xfffe00000ull
#else
#define  CONFIG_IMMR        0xffe00000
#endif
#define GPIO_BASE_ADDR   CONFIG_IMMR + 0xf000
#define GPDIR_OFFSET        0x00
#define GPODR_OFFSET        0x04
#define GPDAT_OFFSET        0x08
#define GPIER_OFFSET        0x0c
#define GPIMR_OFFSET        0x10
#define GPICR_OFFSET        0x14

#define WDI_GPIO_MASK     0x81000081 //gpio 7
#define CPLD_GPIO_TCK      0x00080000 //gpio 12
#define CPLD_GPIO_TMS      0x00040000 //gpio 13
#define CPLD_GPIO_TDI       0x00020000 //gpio 14
#define CPLD_GPIO_TDO       0x00010000 //gpio 15

#ifdef CONFIG_PHYS_64BIT
#define CPLD_BASE_ADDR    0xfffb00000ull
#else
#define CPLD_BASE_ADDR    0xffb00000
#endif
#define UBOOT_OK_MASK     0x78
#define UBOOT_OK                0x50


#ifdef CONFIG_REDSTONE
#define PORTID		0x10
#define OPCODE		0x12
#define DEVADDR		0x14
#define CMDBYTE0 	0x18
#define CMDBYTE1 	0x1a
#define CMDBYTE2	0x1c
#define SSRR		0x1e
#define WRITEDATA	0x20
#define READDATA	0x30
#define SFPPSCR 	0x40
#endif

#ifndef CONFIG_REDSTONE
static JTAGPort jtagdata;
#endif
static volatile void __iomem *gpio_base;
static void __iomem *cpld_base;
#ifdef CONFIG_REDSTONE
static void __iomem *led_cpld_base;
static void __iomem *lpsram_base;
#define LPSRAM_MAX_SIZE 128*1024
#endif

#define CPLD_DETECT_INTERVAL   1000 /*1s interval*/
#if 0
static void cpld_delayed_work(struct work_struct *work)
{
	int gpiodata;

	mutex_lock(&kennisis_cpld_mutex);

	/*kick watchdog*/
	gpiodata = in_be32(gpio_base + GPDAT_OFFSET);
	//      printk( KERN_ERR NAME "gpiodata=%d\n",gpiodata);
	gpiodata &= ~WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);
	msleep(10);
	gpiodata |= WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);

	/*cpld upgrade related port operation*/

	queue_delayed_work(cpld_wq, &delaywork,
			   msecs_to_jiffies(CPLD_DETECT_INTERVAL));

	mutex_unlock(&kennisis_cpld_mutex);


}

/*start work queue for fan control algorithm*/
static int start_cpld_delay_workqueue(void)
{
	int retval = 0;
	//unsigned long flags;


	mutex_lock(&kennisis_cpld_mutex);

	INIT_DELAYED_WORK(&delaywork, cpld_delayed_work);

	if (!cpld_users) {
		cpld_wq = create_singlethread_workqueue("kennisis_cpld");
		if (!cpld_wq) {
			printk(KERN_ERR " failed to create cpld workqueue\n");
			retval = -ENOMEM;
			goto out;
		}
	}

	cpld_users++;


out:
	mutex_unlock(&kennisis_cpld_mutex);
	return retval;
}

static int stop_cpld_delay_workqueue(void)
{
	cancel_delayed_work_sync(&delaywork);

	if (!--cpld_users)
		destroy_workqueue(cpld_wq);

	return 0;

}

static int  Kick_Period_Task(void)
{
	int status;

	status = start_cpld_delay_workqueue();
	if (status < 0 ) return -1;
	queue_delayed_work(cpld_wq, &delaywork,
			   msecs_to_jiffies(CPLD_DETECT_INTERVAL));

	/*Make CPLD detect watchdog signal this time*/
	/*debug          ret = readb(cpld_base+0x08);
	    ret &=~0x02;
	    writeb(ret,cpld_base+0x08);*/

#ifdef KENNISIS_CPLD_DEBUG
	printk( KERN_ERR NAME ":Kick Delayed Work");
#endif

	return 0;
}

static int Stop_Period_Task(void)
{
	stop_cpld_delay_workqueue();

	return 0;
}

static irqreturn_t kennisis_cpld_irq(int irq, void *dev_id)
{
	disable_irq_nosync(irq);
	printk( KERN_ERR NAME "cpld irq detect\n" );
	enable_irq(irq);

	return IRQ_HANDLED;
}
#endif 

static int CPLDOpen( struct inode *inode, struct file *file )
{
#ifdef KENNISIS_CPLD_DEBUG
	printk( KERN_INFO NAME ":  CPLD Device Open");
#endif
#if 0
	if (cpld_used == 1)
		return -EBUSY;
#endif
	mutex_lock(&kennisis_cpld_mutex);
	cpld_used = 1;
	mutex_unlock(&kennisis_cpld_mutex);

	return 0;
}

static int  CPLDClose(struct inode *inode, struct file *file)
{
#ifdef KENNISIS_CPLD_DEBUG
	printk( KERN_INFO NAME ": CPLD  Device Close");
#endif
	cpld_used = 0;

	return 0;
}

static wait_queue_head_t queue;
static unsigned int CPLDPoll(struct file *filp, poll_table *wait)
{
	poll_wait(filp, &queue, wait);

	return 0;
}

static int GpioRead(struct file *filp, char __user *buf, size_t size, loff_t *f)
{
	//JTAGPort jtagdata;
	int retval;
#ifdef CONFIG_REDSTONE
	int i;
	char *p,*q;
	unsigned long offset;
#else
	u32 portvalue;
#endif

	mutex_lock(&kennisis_cpld_mutex);
#ifdef CONFIG_REDSTONE
	offset = *f;
	if ((size + offset) > LPSRAM_MAX_SIZE) {
		size = LPSRAM_MAX_SIZE - offset;
	} //add by lhx

//	p = (char *)kmalloc(size, GFP_KERNEL);
	p = (char *)kzalloc(size, GFP_KERNEL); //change by lhx
	q = p;
	if (!p) {
		printk(KERN_ERR NAME "kmalloc error!\n");
		mutex_unlock(&kennisis_cpld_mutex);
		return -EFAULT;
	}
	for (i = 0; i < size; i++)
		*p++ = readb(lpsram_base + offset + i);	//change by lhx
//		*p++ = readb(lpsram_base + i);
	retval = copy_to_user(buf, q, size * sizeof(char));
	if (retval) {
		printk(KERN_WARNING "copy_to_user error!\n");
	}

#else
	retval = copy_from_user( &jtagdata, (JTAGPort *)buf, sizeof(JTAGPort));

	if ( retval ) {
		mutex_unlock(&kennisis_cpld_mutex);
		return( -EFAULT );
	}


	if (jtagdata.rw == 0) {
		portvalue = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
		jtagdata.value = (unsigned char)((portvalue & 0x00010000) ? 0x01 : 0x0);
		retval = copy_to_user(buf, &jtagdata, sizeof(JTAGPort));
		if (retval) {
			printk(KERN_WARNING "copy_to_user error!\n");
		}
	} else {
		printk( KERN_WARNING NAME "IOCTL_READ_REG error param.\n" );
	}
#endif
	mutex_unlock(&kennisis_cpld_mutex);
	return size;
}


static int GpioWrite(struct file *filp, const char __user * buf, size_t size, loff_t * f)
{
	//JTAGPort jtagdata;
	int retval;
#ifdef CONFIG_REDSTONE
	int i;
	char *p;
	unsigned long offset;
//	long maxsize;
#else
	u32 portvalue;
#endif

	mutex_lock(&kennisis_cpld_mutex);
#ifdef CONFIG_REDSTONE
//	maxsize = *f + size;
//	if (maxsize > LPSRAM_MAX_SIZE) 
//		maxsize = LPSRAM_MAX_SIZE;
	offset = *f;
	if ((size + offset) > LPSRAM_MAX_SIZE) {
		size = LPSRAM_MAX_SIZE - offset;
	} //change by lhx	

//	p = (char *)kmalloc(size, GFP_KERNEL);
	p = (char *)kzalloc(size, GFP_KERNEL);
	if (!p) {
		printk(KERN_ERR NAME "kmalloc error!\n");
		mutex_unlock(&kennisis_cpld_mutex);
		return -EFAULT;
	}
	retval = copy_from_user(p, (char *)buf, size * sizeof(char));
	if (retval) {
		printk(KERN_ERR "copy_to_user error!\n");
		mutex_unlock(&kennisis_cpld_mutex);
		return( -EFAULT );
	}
//	for (i = *f; i < maxsize; i++)
//		writeb(*p++, lpsram_base + i);
	for (i = 0; i < size; i++)
		writeb(*p++, lpsram_base + offset + i);


#else
	retval = copy_from_user( &jtagdata, (JTAGPort *)buf, sizeof(JTAGPort));

	if ( retval ) {
		printk(KERN_ERR "copy_from_user error!\n");
		mutex_unlock(&kennisis_cpld_mutex);
		return( -EFAULT );
	}

	if (jtagdata.rw == 1) {
		portvalue = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
		switch (jtagdata.portid) {
		case 0x01: //pinTDI
			if (jtagdata.value > 0x0)
				portvalue |= jtagdata.value << 17;
			else
				portvalue &= ~(0x1 << 17);
			break;
		case 0x40: //pinTDO
			if (jtagdata.value > 0x0)
				portvalue |= jtagdata.value << 16;
			else
				portvalue &= ~(0x1 << 16);
			break;
		case 0x02: //pinTCK
			if (jtagdata.value > 0x0)
				portvalue |= jtagdata.value << 19;
			else
				portvalue &= ~(0x1 << 19);
			break;
		case 0x04: //pinTMS
			if (jtagdata.value > 0x0)
				portvalue |= jtagdata.value << 18;
			else
				portvalue &= ~(0x1 << 18);
			break;
		}
		*(u32 *)(gpio_base + GPDAT_OFFSET) = portvalue;
	}
#endif
	mutex_unlock(&kennisis_cpld_mutex);
	return size;
}

static loff_t CPLDLseek(struct file *file, loff_t offset, int orig)
{
	switch (orig) {
	case SEEK_SET:
		break;
	case SEEK_CUR:
		offset += file->f_pos;
		break;
	case SEEK_END:
#ifdef CONFIG_REDSTONE
		offset += LPSRAM_MAX_SIZE;
#endif
		break;
	default:
		return -EINVAL;
	}
#ifdef CONFIG_REDSTONE
	if (offset >= 0 && offset <= LPSRAM_MAX_SIZE){

		return file->f_pos = offset;
	}
#else
	if (offset >= 0)
		return file->f_pos = offset;
#endif

	return -EINVAL;
}

#ifdef CONFIG_REDSTONE
/*
 * d2012_read_sfp: read sfp registers
 * portID: port number(17-18)
 * devAddr: device addr(a0/a2)
 * reg: register
 * data: store read data
 * len: read data length
 * return: 0: success , -1: fail
 */
int d2012_read_sfp(int portID, char devAddr, char reg, char *data, int len)
{
	int count;
	char byte;
//	u32 temp;
	void *temp;
	char portid, opcode, devaddr, cmdbyte0, ssrr, writedata, readdata, sfppscr;

	if (led_cpld_base == NULL) {
		printk(KERN_ERR NAME "ioremap error!\n");
		iounmap(led_cpld_base);
		return -1;
	}
	if ((reg + len) > 256) {
		printk(KERN_ERR "read data len not larger than 256!\n");
		return -1;
	}
	if ((portID >= 17) && (portID <= 30)) {
		portid = 0x10;
		opcode = 0x12;
		devaddr = 0x14;
		cmdbyte0 = 0x18;
		ssrr = 0x1e;
		writedata = 0x20;
		readdata = 0x30;
		sfppscr = 0x70;
		if ((readb(led_cpld_base + sfppscr + (portID - 17) * 2) & 0x04) == 0x04) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else if ((portID >= 31) && (portID <= 48)) {
		portid = 0x40;
		opcode = 0x42;
		devaddr = 0x44;
		cmdbyte0 = 0x48;
		ssrr = 0x4e;
		writedata = 0x50;
		readdata = 0x60;
		sfppscr = 0x70;
		if ((readb(led_cpld_base + sfppscr + (portID - 17) * 2) & 0x04) == 0x04) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else {
		printk(KERN_ERR "sfp num be sure in 17-48\n");
		return -1;
	}
	byte = 0x40 + portID;
	writeb(byte, led_cpld_base + portid);
	writeb(reg, led_cpld_base + cmdbyte0);
	while (len > 0) {
		count = (len >= 8) ? 8 : len;
		len -= count;
		byte = count * 16 + 1;
		writeb(byte, led_cpld_base + opcode);
		devAddr |= 0x01;
		writeb(devAddr, led_cpld_base + devaddr);
		while ((readb(led_cpld_base + ssrr) & 0x40))
		{
			udelay(100);
		}
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ERR "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
		temp = led_cpld_base + readdata;
		while (count-- > 0) {
			*(data++) = readb(temp);
			temp += 2;
		}
		if (len > 0) {
			reg += 0x08;
			writeb(reg, led_cpld_base + cmdbyte0);
		}
	}
	return 0;
}
/*
 * d2020_read_sfp: read sfp registers
 * portID: port number(1-52)
 * devAddr: device addr(a0/a2)
 * reg: register
 * data: store read data
 * len: read data length
 * return: 0: success , -1: fail
 */
int d2020_read_sfp(int portID, char devAddr, char reg, char *data, int len)
{
	int count;
	char byte;
//	u32 temp;
	void *temp;
	char portid, opcode, devaddr, cmdbyte0, ssrr, writedata, readdata, sfppscr;

	if (led_cpld_base == NULL) {
		printk(KERN_ERR NAME "ioremap error!\n");
		iounmap(led_cpld_base);
		return -1;
	}
	if ((reg + len) > 256) {
		printk(KERN_ERR "read data len not larger than 256!\n");
		return -1;
	}
    if (portID >= 1 && portID <= 18) {
        portid = 0x10;
		opcode = 0x12;
		devaddr = 0x14;
		cmdbyte0 = 0x18;
		ssrr = 0x1e;
		writedata = 0x20;
		readdata = 0x30;
		sfppscr = 0xb0;
		if (readb(led_cpld_base + sfppscr + portID + ((portID % 2) ? 1 : 0) - 2) & ((portID % 2) ? 0x04 : 0x40)) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	}else if ((portID >= 19) && (portID <= 36)) {
		portid = 0x40;
		opcode = 0x42;
		devaddr = 0x44;
		cmdbyte0 = 0x48;
		ssrr = 0x4e;
		writedata = 0x50;
		readdata = 0x60;
		sfppscr = 0xb0;
		if (readb(led_cpld_base + sfppscr + portID + ((portID % 2) ? 1 : 0) - 2) & ((portID % 2) ? 0x04 : 0x40)) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else if ((portID >= 37) && (portID <= 52)) {
		portid = 0x80;
		opcode = 0x82;
		devaddr = 0x84;
		cmdbyte0 = 0x88;
		ssrr = 0x8e;
		writedata = 0x90;
		readdata = 0xA0;
		if (portID > 48){
			sfppscr = 0xe0;
			if ((readb(led_cpld_base + sfppscr + (portID - 49) * 2) & 0x04) == 0x04) {
				printk(KERN_ERR "Not sfp device!\n");
				return -1;
			}
		}
		else {
			sfppscr = 0xb0;
			if (readb(led_cpld_base + sfppscr + portID + ((portID % 2) ? 1 : 0) - 2) & ((portID % 2) ? 0x04 : 0x40)) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
			}
		}

		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else {
		printk(KERN_WARNING "sfp num be sure in 1-52\n");
		return -1;
	}
    
	byte = 0x40 + portID;
	writeb(byte, led_cpld_base + portid);
	writeb(reg, led_cpld_base + cmdbyte0);
	while (len > 0) {
		count = (len >= 8) ? 8 : len;
		len -= count;
		byte = count * 16 + 1;
		writeb(byte, led_cpld_base + opcode);
		devAddr |= 0x01;
		writeb(devAddr, led_cpld_base + devaddr);
		while ((readb(led_cpld_base + ssrr) & 0x40))
		{
			udelay(100);
		}
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ERR "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
		temp = led_cpld_base + readdata;
		while (count-- > 0) {
			*(data++) = readb(temp);
			temp += 2;
		}
		if (len > 0) {
			reg += 0x08;
			writeb(reg, led_cpld_base + cmdbyte0);
		}
	}
	return 0;
}
/*
 * read_sfp: read sfp registers
 * portID: port number((17-18)/(1-52))
 * devAddr: device addr(a0/a2)
 * reg: register
 * data: store read data
 * len: read data length
 * return: 0: success , -1: fail
 */
int read_sfp(int portID, char devAddr, char reg, char *data, int len)
{
    int retval = 0;
//    char *buf;
    int gpiodata,ret;
    
	mutex_lock(&kennisis_cpld_mutex);
	gpiodata = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
	ret = (unsigned char)(gpiodata & 0x10000000 ? 0x1 : 0x0);
	ret += (unsigned char)(gpiodata & 0x08000000 ? 0x1 : 0x0) << 1;

    //sprintf(buf, "%s", ret);
    
    if (ret == 2)
        retval = d2020_read_sfp(portID, devAddr, reg, data, len);
    else
        retval = d2012_read_sfp(portID, devAddr, reg, data, len);

	mutex_unlock(&kennisis_cpld_mutex);

    if (retval)
        return -1;
    
    return 0;
}

EXPORT_SYMBOL(read_sfp);

/*
 * d2012_write_sfp: write sfp registers
 * portID: port number(17-18)
 * devAddr: device addr(a0/a2)
 * reg: register
 * data: store read data
 * len: write data length
 * return: 0: success , -1: fail
 */
int d2012_write_sfp(int portID, char devAddr, char reg, char *data, int len)
{
	int count;
	char byte;
//	u32 temp;
	void *temp; 
	char portid, opcode, devaddr, cmdbyte0, ssrr, writedata, readdata, sfppscr;

	if (led_cpld_base == NULL) {
		printk(KERN_ERR NAME "ioremap error!\n");
		iounmap(led_cpld_base);
		return -1;
	}
	if ((reg + len) > 256) {
		printk(KERN_ERR "write data len not larger than 256!\n");
		return -1;
	}
	if ((portID >= 17) && (portID <= 30)) {
		portid = 0x10;
		opcode = 0x12;
		devaddr = 0x14;
		cmdbyte0 = 0x18;
		ssrr = 0x1e;
		writedata = 0x20;
		readdata = 0x30;
		sfppscr = 0x70;
		if ((readb(led_cpld_base + sfppscr + (portID - 17) * 2) & 0x04) == 0x04) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else if ((portID >= 31) && (portID <= 48)) {
		portid = 0x40;
		opcode = 0x42;
		devaddr = 0x44;
		cmdbyte0 = 0x48;
		ssrr = 0x4e;
		writedata = 0x50;
		readdata = 0x60;
		sfppscr = 0x70;
		if ((readb(led_cpld_base + sfppscr + (portID - 17) * 2) & 0x04) == 0x04) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else {
		printk(KERN_ERR "sfp num be sure in 17-48\n");
		return -1;
	}
	byte = 0x40 + portID;
	writeb(byte, led_cpld_base + portid);
	writeb(reg, led_cpld_base + cmdbyte0);
	while (len > 0) {
		count = (len >= 8) ? 8 : len;
		len -= count;
		byte = (count << 4) + 1;
		writeb(byte, led_cpld_base + opcode);
		temp = led_cpld_base + writedata;
		while (count-- > 0) {
			writeb(*(data++), temp);
			temp += 0x02;
		}
		devAddr &= 0xfe;
		writeb(devAddr, led_cpld_base + devaddr);
		while ((readb(led_cpld_base + ssrr) & 0x40))
		{
			udelay(100);
		}
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ERR "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
		if (len > 0) {
			reg += 0x08;
			writeb(reg, led_cpld_base + cmdbyte0);
		}
	}
	return 0;
}
/*
 * d2020_write_sfp: write sfp registers
 * portID: port number(1-52)
 * devAddr: device addr(a0/a2)
 * reg: register
 * data: store read data
 * len: write data length
 * return: 0: success , -1: fail
 */
int d2020_write_sfp(int portID, char devAddr, char reg, char *data, int len)
{
	int count;
	char byte;
//	u32 temp;
	void *temp; 
	char portid, opcode, devaddr, cmdbyte0, ssrr, writedata, readdata, sfppscr;

	if (led_cpld_base == NULL) {
		printk(KERN_ERR NAME "ioremap error!\n");
		iounmap(led_cpld_base);
		return -1;
	}
	if ((reg + len) > 256) {
		printk(KERN_ERR "write data len not larger than 256!\n");
		return -1;
	}
    if (portID >= 1 && portID <= 18) {
        portid = 0x10;
		opcode = 0x12;
		devaddr = 0x14;
		cmdbyte0 = 0x18;
		ssrr = 0x1e;
		writedata = 0x20;
		readdata = 0x30;
		sfppscr = 0xB0;
		if (readb(led_cpld_base + sfppscr + portID + ((portID % 2) ? 1 : 0) - 2) & ((portID % 2) ? 0x04 : 0x40)) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
    } else if ((portID >= 19) && (portID <= 36)) {
		portid = 0x40;
		opcode = 0x42;
		devaddr = 0x44;
		cmdbyte0 = 0x48;
		ssrr = 0x4e;
		writedata = 0x50;
		readdata = 0x60;
		sfppscr = 0xB0;
		if (readb(led_cpld_base + sfppscr + portID + ((portID % 2) ? 1 : 0) - 2) & ((portID % 2) ? 0x04 : 0x40)) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else if ((portID >= 37) && (portID <= 52)) {
		portid = 0x80;
		opcode = 0x82;
		devaddr = 0x84;
		cmdbyte0 = 0x88;
		ssrr = 0x8e;
		writedata = 0x90;
		readdata = 0xA0;
		if (portID > 48){
			sfppscr = 0xe0;
			if ((readb(led_cpld_base + sfppscr + (portID - 49) * 2) & 0x04) == 0x04) {
				printk(KERN_ERR "Not sfp device!\n");
				return -1;
			}
		}
		else {
			sfppscr = 0xb0;
			if (readb(led_cpld_base + sfppscr + portID + ((portID % 2) ? 1 : 0) - 2) & ((portID % 2) ? 0x04 : 0x40)) {
			printk(KERN_ERR "Not sfp device!\n");
			return -1;
			}
		}
		while ((readb(led_cpld_base + ssrr) & 0x40));
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
	} else {
		printk(KERN_ERR "sfp num be sure in 1-52\n");
		return -1;
	}
	byte = 0x40 + portID;
	writeb(byte, led_cpld_base + portid);
	writeb(reg, led_cpld_base + cmdbyte0);
	while (len > 0) {
		count = (len >= 8) ? 8 : len;
		len -= count;
		byte = (count << 4) + 1;
		writeb(byte, led_cpld_base + opcode);
		temp = led_cpld_base + writedata;
		while (count-- > 0) {
			writeb(*(data++), temp);
			temp += 0x02;
		}
		devAddr &= 0xfe;
		writeb(devAddr, led_cpld_base + devaddr);
		while ((readb(led_cpld_base + ssrr) & 0x40))
		{
			udelay(100);
		}
		if ((readb(led_cpld_base + ssrr) & 0x80) == 0x80) {
			printk(KERN_ERR "I2C Master error!\n");
			writeb(0x00, led_cpld_base + ssrr);
			udelay(3000);
			writeb(0x01, led_cpld_base + ssrr);
			return -1;
		}
		if (len > 0) {
			reg += 0x08;
			writeb(reg, led_cpld_base + cmdbyte0);
		}
	}
	return 0;
}

int write_sfp(int portID, char devAddr, char reg, char *data, int len)
{
	int retval = 0;
//    char *buf;
    int gpiodata,ret;
    
	mutex_lock(&kennisis_cpld_mutex);
	gpiodata = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
	ret = (unsigned char)(gpiodata & 0x10000000 ? 0x1 : 0x0);
	ret += (unsigned char)(gpiodata & 0x08000000 ? 0x1 : 0x0) << 1;
    //sprintf(buf, "%s", ret);
    
    if (ret == 2)
        retval = d2020_write_sfp(portID, devAddr, reg, data, len);
    else
        retval = d2012_write_sfp(portID, devAddr, reg, data, len);

	mutex_unlock(&kennisis_cpld_mutex);

    if (retval)
        return -1;
    
	return 0;
}
EXPORT_SYMBOL(write_sfp);
#endif

unsigned char read_cpld(char *cpldID, int reg)
{
	void __iomem *cpld_addr = NULL;
	unsigned char ret;

	mutex_lock(&kennisis_cpld_mutex);
	if (strcmp(cpldID, "reset") == 0) {
		cpld_addr = cpld_base;
	}
#ifdef CONFIG_REDSTONE
	else if (strcmp(cpldID, "led") == 0) {
		cpld_addr = led_cpld_base;
	}
#endif
	ret = readb(cpld_addr + reg);
	mutex_unlock(&kennisis_cpld_mutex);

	return ret;
}
EXPORT_SYMBOL(read_cpld);

int write_cpld(char *cpldID, int reg, char value)
{
	void __iomem *cpld_addr = NULL;

	mutex_lock(&kennisis_cpld_mutex);
	if (strcmp(cpldID, "reset") == 0) {
		cpld_addr = cpld_base;
	}
#ifdef CONFIG_REDSTONE
	else if (strcmp(cpldID, "led") == 0) {
		cpld_addr = led_cpld_base;
	}
#endif
	writeb(value, cpld_addr + reg);
	mutex_unlock(&kennisis_cpld_mutex);

	return 0;
}
EXPORT_SYMBOL(write_cpld);

int read_hw(void)
{
	int gpiodata,ret;
	mutex_lock(&kennisis_cpld_mutex);
	gpiodata = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
	ret = (unsigned char)(gpiodata & 0x10000000 ? 0x1 : 0x0);
	ret += (unsigned char)(gpiodata & 0x08000000 ? 0x1 : 0x0) << 1;
	mutex_unlock(&kennisis_cpld_mutex);

	return ret;
}
EXPORT_SYMBOL(read_hw);

static long CPLDIoctl(struct file *filp, unsigned int cmd, unsigned long arg )
{
	int err = 0;
	int retval = 0;
	RegData reg;
	u8 regvalue;
	u32 portvalue;
	JTAGPort jtagdata;
#ifdef CONFIG_REDSTONE
	SfpData sfpdata;
#endif
	/*
	** extract the type and number bitfields, and don't decode
	** wrong cmds: return ENOTTY (inappropriate ioctl) before access_ok()
	*/
	if ( _IOC_TYPE( cmd ) != KENNISIS_TYPE ) {
		return( -ENOTTY );
	}

	/*
	** the direction is a bitmask, and VERIFY_WRITE catches R/W
	** transfers. `Type' is user-oriented, while
	** access_ok is kernel-oriented, so the concept of "read" and
	** "write" is reversed
	*/
	if ( _IOC_DIR( cmd ) & _IOC_READ ) {
		err = !access_ok( VERIFY_WRITE,
				  ( void __user * )arg,
				  _IOC_SIZE( cmd ) );
	} else if ( _IOC_DIR( cmd ) & _IOC_WRITE ) {
		err = !access_ok( VERIFY_READ,
				  ( void __user * )arg,
				  _IOC_SIZE( cmd ) );
	}
	if ( err ) {
		return( -EFAULT );
	}

	switch (cmd) {

	case IOCTL_READ_REG: {

#ifdef KENNISIS_CPLD_DEBUG
		printk( KERN_INFO NAME "IOCTL_READ_REG received.\n" );
#endif

		mutex_lock(&kennisis_cpld_mutex);

		retval = copy_from_user( &reg,
					 ( const void * )arg,
					 sizeof( RegData) );

		if ( retval ) {
			mutex_unlock(&kennisis_cpld_mutex);
			printk(KERN_ERR "copy from user error.\n");
			return( -EFAULT );
		}
		if (reg.rw == 0) {
#ifdef CONFIG_REDSTONE
			if (reg.cpldID == 0) { //led cpld
				regvalue = readb(led_cpld_base + reg.regid);
			} else // reset cpld
#endif
			{
				regvalue = readb(cpld_base + reg.regid);
			}
			reg.value = regvalue;

			retval =  copy_to_user(
					  ( void  __user * )arg, &reg,
					  sizeof( RegData) );
		} else {
			printk( KERN_WARNING NAME "IOCTL_READ_REG error param.\n" );
		}

		mutex_unlock(&kennisis_cpld_mutex);
		break;
	}
	case IOCTL_WRITE_REG: {

#ifdef KENNISIS_CPLD_DEBUG
		printk( KERN_INFO NAME "IOCTL_WRITE_REG received.\n" );
#endif

		mutex_lock(&kennisis_cpld_mutex);

		retval = copy_from_user( &reg,
					 ( const void * )arg,
					 sizeof( RegData) );
		if ( retval ) {
			mutex_unlock(&kennisis_cpld_mutex);
			return( -EFAULT );
		}
		if (reg.rw == 1) {
#ifdef CONFIG_REDSTONE
			if (reg.cpldID == 0) { //led cpld
				writeb(reg.value, led_cpld_base + reg.regid );
			} else	//reset cpld
#endif
				writeb(reg.value, cpld_base + reg.regid );
		} else {
			printk( KERN_WARNING NAME "IOCTL_READ_REG error param.\n" );
		}
		mutex_unlock(&kennisis_cpld_mutex);
		break;
	}
#ifdef CONFIG_REDSTONE
	case IOCTL_READ_SFP_REG: {

#ifdef KENNISIS_CPLD_DEBUG
		printk( KERN_INFO NAME "IOCTL_READ_SFP_REG received.\n" );
#endif
//		mutex_lock(&kennisis_cpld_mutex);
		retval = copy_from_user( &sfpdata,
					 ( const void * )arg,
					 sizeof( SfpData ));
		if ( retval ) {
			mutex_unlock(&kennisis_cpld_mutex);
			return( -EFAULT );
		}
		if (sfpdata.rw == 0) {
			retval = read_sfp(sfpdata.portID, sfpdata.devAddr, sfpdata.reg, sfpdata.data, sfpdata.len);
			if (retval) {
				printk(KERN_WARNING NAME "read sfp error!\n");
				mutex_unlock(&kennisis_cpld_mutex);
				return (-EFAULT);
			}
			retval =  copy_to_user(
					  ( void  __user * )arg, &sfpdata,
					  sizeof( sfpdata) );
		} else {
			printk( KERN_WARNING NAME "IOCTL_READ_SFP_REG error param.\n" );
		}
//		mutex_unlock(&kennisis_cpld_mutex);
		break;
	}
	case IOCTL_WRITE_SFP_REG: {
#ifdef KENNISIS_CPLD_DEBUG
		printk( KERN_INFO NAME "IOCTL_WRITE_REG received.\n" );
#endif
//		mutex_lock(&kennisis_cpld_mutex);
		retval = copy_from_user( &sfpdata,
					 ( const void * )arg,
					 sizeof( sfpdata) );
		if ( retval ) {
			mutex_unlock(&kennisis_cpld_mutex);
			return( -EFAULT );
		}
		if (sfpdata.rw == 1) {
			retval = write_sfp(sfpdata.portID, sfpdata.devAddr, sfpdata.reg, sfpdata.data, sfpdata.len);
			if (retval) {
				printk(KERN_WARNING NAME "write sfp error!\n");
				mutex_unlock(&kennisis_cpld_mutex);
				return (-EFAULT);
			}
		} else {
			printk( KERN_WARNING NAME "IOCTL_WRITE_SFP_REG error param.\n" );
		}

//		mutex_unlock(&kennisis_cpld_mutex);
		break;
	}
#endif
	case IOCTL_READ_PORT: {

#ifdef KENNISIS_CPLD_DEBUG
		printk( KERN_INFO NAME "IOCTL_READ_PORT received.\n" );
#endif
		mutex_lock(&kennisis_cpld_mutex);
		retval = copy_from_user( &jtagdata,
					 ( const void * )arg,
					 sizeof( JTAGPort) );
		if ( retval ) {
			mutex_unlock(&kennisis_cpld_mutex);
			return( -EFAULT );
		}
		if (jtagdata.rw == 0) {
			portvalue = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
			jtagdata.value = (unsigned char)((portvalue & 0x00010000) ? 0x01 : 0x0);
			retval = copy_to_user(( void  *)arg, (void *) &jtagdata, sizeof(jtagdata));
		}
		else {
			printk(KERN_ERR NAME "IOCTL_READ_JTAGDATA ERROR.\n");
		}
		mutex_unlock(&kennisis_cpld_mutex);
		break;
	}
	case IOCTL_WRITE_PORT: {
#ifdef KENNISIS_CPLD_DEBUG
		printk( KERN_INFO NAME "IOCTL_WRITE_PORT received.\n" );
#endif
		mutex_lock(&kennisis_cpld_mutex);
		retval = copy_from_user( &jtagdata,
					 ( const void * )arg,
					 sizeof( JTAGPort) );
		if ( retval ) {
			mutex_unlock(&kennisis_cpld_mutex);
			return( -EFAULT );
		}
		if (jtagdata.rw == 1) {
			portvalue = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
			switch (jtagdata.portid) {
			case 0x01: //pinTDI
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 17;
				else
					portvalue &= ~(0x1 << 17);
				//bit = 14;
				break;
			case 0x40: //pinTDO
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 16;
				else
					portvalue &= ~(0x1 << 16);
				//bit = 15;
				break;
			case 0x02: //pinTCK
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 19;
				else
					portvalue &= ~(0x1 << 19);
				//bit = 12;
				break;
			case 0x04: //pinTMS
				if (jtagdata.value > 0x0)
					portvalue |= jtagdata.value << 18;
				else
					portvalue &= ~(0x1 << 18);
				//bit = 13;
				break;
			default:
				break;
			}
			*(volatile u32 *)(gpio_base + GPDAT_OFFSET) = portvalue;
		} else {
			printk(KERN_WARNING NAME "IOCTL_WRITE_JATGDATA ERROR.\n");
		}
		mutex_unlock(&kennisis_cpld_mutex);
		break;
	}
#if 0
	case IOCTL_SET_PORT_FREQ:
	     {

	#ifdef KENNISIS_CPLD_DEBUG
	    printk( KERN_ERR NAME "IOCTL_SET_PORT_FREQ received.\n" );
	#endif

	   mutex_lock(&kennisis_cpld_mutex);

	    retval = copy_from_user( &jtagdata,
	                                   ( const void * )arg,
	                                    sizeof( JTAGPort) );

	   if( retval )
	    {
	      mutex_unlock(&kennisis_cpld_mutex);
	       return( -EFAULT );
	    }



	mutex_unlock(&kennisis_cpld_mutex);
	    break;
	 }
#endif
	default:  /* redundant, as cmd was checked against MAXNR */
		return( -ENOTTY );
	}
	return retval;
}

/*
** Kernel device registration data structures.
*/
static const struct file_operations fops = {
	.owner		= THIS_MODULE,
	.open		= CPLDOpen,
	.release             = CPLDClose,
	.read		= GpioRead,
	.write		= GpioWrite,
	.llseek		= CPLDLseek,
	.unlocked_ioctl	    = CPLDIoctl,
	.poll               = CPLDPoll,
};

static struct miscdevice kennisisCPLDDev = {
	.minor		= KENNISIS_CPLD_MINOR,
	.name		= "kennisis cpld",
	.fops		= &fops,
};

static ssize_t cpldversion_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
  char *buf)
{
	u8 ret = 0;

	mutex_lock(&kennisis_cpld_mutex);
	ret = readb(cpld_base + 0x00);
	mutex_unlock(&kennisis_cpld_mutex);

	return sprintf(buf, "%d", ret);
}

static ssize_t boardversion_show(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
  char *buf)
{
	u8 ret = 0;

	mutex_lock(&kennisis_cpld_mutex);
	ret = readb(cpld_base + 0x16);
	mutex_unlock(&kennisis_cpld_mutex);

	return sprintf(buf, "%d", ret);
}

static ssize_t sysreset_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
			      const char *buffer, size_t count)
{
	int value;
	u8 ret;

	mutex_lock(&kennisis_cpld_mutex);
	sscanf(buffer, "%d", &value);
	if (value == 0 ) {
#ifdef CONFIG_REDSTONE
		ret = readb(cpld_base + 0x10);
		ret &= ~0x02;
		writeb(ret, cpld_base + 0x10);
#else
		ret = readb(cpld_base + 0x08);
		ret &= ~0x04;
		writeb(ret, cpld_base + 0x08);
#endif
	} else {
		printk( KERN_WARNING NAME ":sys reset value error\n" );
	}

	mutex_unlock(&kennisis_cpld_mutex);

	return count;
}

static ssize_t cpureset_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
			      const char *buffer, size_t count)
{
	int value;
	u8 ret;

	mutex_lock(&kennisis_cpld_mutex);
	sscanf(buffer, "%d", &value);
	if (value == 0 ) {
#ifdef CONFIG_REDSTONE
		ret = readb(cpld_base + 0x10);
		ret &= ~0x01;
		writeb(ret, cpld_base + 0x10);
#else
		ret = readb(cpld_base + 0x08);
		ret &= ~0x01;
		writeb(ret, cpld_base + 0x08);
#endif
	} else {
		printk( KERN_WARNING NAME ":cpu reset value error\n" );
	}

	mutex_unlock(&kennisis_cpld_mutex);

	return count;
}

static ssize_t sysled_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
			    const char *buffer, size_t count)
{
	int value;
	u8 ret;

	mutex_lock(&kennisis_cpld_mutex);
	sscanf(buffer, "%d", &value);
#ifdef CONFIG_REDSTONE
	if (value <= 3 ) {
		ret = readb(cpld_base + 0x98);
		ret &= ~0x03;
		ret |= (u8)value;
		writeb(ret, cpld_base + 0x98);
#else
	if (value <= 5 ) {
		ret = readb(cpld_base + 0x0a);
		ret &= ~0x07;
		ret |= (u8)value;
		writeb(ret, cpld_base + 0x0a);
#endif
	} else {
		printk( KERN_WARNING NAME ":sys led value error\n" );
	}
	mutex_unlock(&kennisis_cpld_mutex);

	return count;
}
#ifdef CONFIG_REDSTONE
#define WDO_REG		0x10
#define WDO_MASK	0x04
#else
#define WDO_REG		0x08
#define WDO_MASK	0x02
#endif
static ssize_t wdo_kick(void)
{
	int gpiodata;

	/*kick watchdog*/
	gpiodata = in_be32(gpio_base + GPDAT_OFFSET);
	gpiodata &= ~WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);
	msleep(10);
	gpiodata |= WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);

	return 0;
}
static ssize_t wdo_enable_store(struct class *cls,
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
				const char *buffer, size_t count)
{
	int value;
	u8 ret;

	mutex_lock(&kennisis_cpld_mutex);
	sscanf(buffer, "%d", &value);
	if (value == 0 ) {
		ret = readb(cpld_base + WDO_REG);
		ret |= WDO_MASK;
		writeb(ret, cpld_base + WDO_REG);
	} else {
		ret = readb(cpld_base + WDO_REG);
		ret &= ~WDO_MASK;
		wdo_kick();
		writeb(ret, cpld_base + WDO_REG);
	}
	mutex_unlock(&kennisis_cpld_mutex);

	return count;
}
static ssize_t wdo_kick_store(struct class *cls, 
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
				const char *buffer, size_t count)
{
	int gpiodata;

	mutex_lock(&kennisis_cpld_mutex);
	/*kick watchdog*/
	gpiodata = in_be32(gpio_base + GPDAT_OFFSET);
	gpiodata &= ~WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);
	msleep(10);
	gpiodata |= WDI_GPIO_MASK;
	out_be32(gpio_base + GPDAT_OFFSET, gpiodata);

	mutex_unlock(&kennisis_cpld_mutex);

	return count;
}


	
	

/*sysfs function*/
static CLASS_ATTR(cpldversion, S_IRUGO | S_IWUSR, cpldversion_show, NULL );
static CLASS_ATTR(boardversion, S_IRUGO | S_IWUSR, boardversion_show, NULL );
static CLASS_ATTR(sysreset,  S_IWUSR | S_IRUGO, NULL, sysreset_store);
static CLASS_ATTR(cpureset,  S_IWUSR | S_IRUGO, NULL, cpureset_store);
static CLASS_ATTR(sysled,  S_IWUSR | S_IRUGO, NULL, sysled_store);
static CLASS_ATTR(wdo_enable,  S_IWUSR | S_IRUGO, NULL, wdo_enable_store);
static CLASS_ATTR(wdo_kick,  S_IWUSR | S_IRUGO, NULL, wdo_kick_store);

#ifdef CONFIG_REDSTONE
static ssize_t hw_type_show(struct class *cls,  
#ifdef CONFIG_KERNEL_32_TO_34
	struct class_attribute *attr,
#endif
char *buf)
{
	int gpiodata,ret;
	mutex_lock(&kennisis_cpld_mutex);
	gpiodata = *(volatile u32 *)(gpio_base + GPDAT_OFFSET);
	ret = (unsigned char)(gpiodata & 0x10000000 ? 0x1 : 0x0);
	ret += (unsigned char)(gpiodata & 0x08000000 ? 0x1 : 0x0) << 1;
	mutex_unlock(&kennisis_cpld_mutex);

	return sprintf(buf, "%x", ret);
}

static CLASS_ATTR(hw_type,  S_IWUSR | S_IRUGO, hw_type_show, NULL);

static struct class_attribute *attrgroup[] = {
	&class_attr_cpldversion,
	&class_attr_boardversion,
	&class_attr_sysreset,
	&class_attr_cpureset,
	&class_attr_sysled,
	&class_attr_wdo_enable,
	&class_attr_wdo_kick,
	&class_attr_hw_type,
};

#define NUM_CPLD_FILES  8//sizeof(attrgroup)/sizeof(class_attribute)
#else
#define NUM_CPLD_FILES  7//sizeof(attrgroup)/sizeof(class_attribute)
static struct class_attribute *attrgroup[] = {
	&class_attr_cpldversion,
	&class_attr_boardversion,
	&class_attr_sysreset,
	&class_attr_cpureset,
	&class_attr_sysled,
	&class_attr_wdo_enable,
	&class_attr_wdo_kick,
};
#endif

void gpio_init(void)
{
	u32 temp = 0;
	gpio_base = ioremap(GPIO_BASE_ADDR, 20 );
	temp |= (WDI_GPIO_MASK | CPLD_GPIO_TCK | CPLD_GPIO_TMS | CPLD_GPIO_TDI);
	if (gpio_base) {
		*(volatile u32 *)(gpio_base + GPDIR_OFFSET) |= temp;
		udelay(30);
		*(volatile u32 *)(gpio_base + GPDAT_OFFSET) |= (temp & (~CPLD_GPIO_TCK));
	} else {
		printk(KERN_INFO "Mapping GPIO address fail\n");
	}
}

#define KENNISIS_CPLD_IRQ   0
static int CPLD_Init_Func(void)
{
	u8 ret;
	int status;
	int i;

	/*gpio mapping and init*/
	gpio_init();
	/*CPLD Adress Mapping*/
	cpld_base = ioremap(CPLD_BASE_ADDR, 256);
	if (cpld_base == NULL) {
		printk( KERN_ERR NAME ":request cpld addr error\n" );
		goto CPLD_EXIT3;
	}
#ifdef CONFIG_REDSTONE
	led_cpld_base = ioremap(CPLD_I2C_LED_ADDR, 256);
	if (led_cpld_base == NULL) {
		printk( KERN_ERR NAME ":request i2c led cpld addr error\n" );
		goto CPLD_EXIT2;
	}
	printk(KERN_ERR NAME ":led_cpld_base = %p\n", led_cpld_base);
//	lpsram_base = ioremap(LPSRAM_BASE_ADDR, 20);
	lpsram_base = ioremap(LPSRAM_BASE_ADDR,LPSRAM_MAX_SIZE);
	if (lpsram_base == NULL) {
		printk( KERN_ERR NAME ":request lpsram addr error\n" );

		goto CPLD_EXIT2;
	}
#endif
	/*u-boot have notified CPLD?*/
	ret = readb(cpld_base + 0x0a);
	if ((ret & UBOOT_OK_MASK) != UBOOT_OK) {
		printk( KERN_ERR NAME ":cpld not ready error\n" );
		goto CPLD_EXIT2;
	}
	/*system led become stable green*/
#define GREEN_STABLE_LED 2
	ret &= ~0x07;
	ret |= (u8)GREEN_STABLE_LED;
	writeb(ret, cpld_base + 0x0a);
	/*kick period task*/
#if 0
//zmzhan comment out
	if(Kick_Period_Task() < 0)
	 {
	    printk( KERN_ERR NAME ":kick period task error\n" );

	    goto CPLD_EXIT2;
	 }
#endif
	/*sysfs interface*/
	for (i = 0; i < NUM_CPLD_FILES; i++) {
		status = class_create_file(kennisis_cpld_class, attrgroup[i]);
	}
	if ( status ) {
		printk( KERN_NOTICE NAME ":create cpld class file error\n" );
		goto CPLD_EXIT2;
	}

	/*CPLD irq*/
	/*Not used currently,disable it. When it required, we need to add interrupt info into DTS file
	and add function here to parse and map the interrupt to soft irq like i2c device in i2c-mpc.c*/
#if 0
	if (request_irq(KENNISIS_CPLD_IRQ, kennisis_cpld_irq, 0,
			"kennisis cpld", NULL) < 0) {

		printk( KERN_ERR NAME "kennisis cpld request irq fail\n" );
	}
#endif
	/*register device*/
	status = misc_register(&kennisisCPLDDev);
	if ( status ) {
		printk( KERN_NOTICE NAME ":ERROR register kennisis cpld device\n" );
		goto CPLD_EXIT1;
	}

	return 0;
CPLD_EXIT1:
	for (i = 0; i < NUM_CPLD_FILES; i++) {
		class_remove_file(kennisis_cpld_class, attrgroup[i]);
	}
CPLD_EXIT2:
	iounmap(cpld_base);
#ifdef CONFIG_REDSTONE
	iounmap(led_cpld_base);
	iounmap(lpsram_base);
#endif
CPLD_EXIT3:
	iounmap(gpio_base);

	return -1;
}

/*
** module load/initialization
*/
static int __init kennisisCPLDInit( void )
{
	int ret;

	printk( KERN_INFO NAME ":CPLD driver init\n" );
	kennisis_cpld_class = class_create(THIS_MODULE, "kennisis_cpld");
	if (IS_ERR(kennisis_cpld_class))
		return PTR_ERR(kennisis_cpld_class);
	ret = CPLD_Init_Func();
	if (ret < 0) {
		printk( KERN_ERR NAME ":CPLD init error\n" );
		return ret;
	}

	return 0;
}

/*
** module unload
*/
static void __exit kennisisCPLDExit( void )
{
	int i;

	printk( KERN_INFO NAME ":CPLD driver exit\n" );
	//Stop_Period_Task();
	misc_deregister(&kennisisCPLDDev);
	iounmap(cpld_base);
#ifdef CONFIG_REDSTONE
	iounmap(led_cpld_base);
#endif
	iounmap(gpio_base);

	for (i = 0; i < NUM_CPLD_FILES; i++) {
		class_remove_file(kennisis_cpld_class, attrgroup[i]);
	}
	class_destroy(kennisis_cpld_class);
}

module_init( kennisisCPLDInit );
module_exit( kennisisCPLDExit );
