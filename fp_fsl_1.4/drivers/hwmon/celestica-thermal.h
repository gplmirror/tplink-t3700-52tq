#ifndef __CELESTICA_THERMAL_H__
#define __CELESTICA_THERMAL_H__

#ifdef __cplusplus
extern "C" {
#endif

//#define THERMAL_DEBUG 1
//#define THERMAL_SIMULATION 1

#define LM75_TEMP_COUNT 7

typedef enum
{
    lm75_p2020=0,

#ifdef CONFIG_REDSTONE
    lm75_bcm56846,
#else
    lm75_bcm56850,
#endif

    lm75_LIA,
    lm75_RIA,
    lm75_ROA,
    psu1,
    psu2,
    fan_chip,
    num_devices,
}thermal_chips;

extern unsigned char read_cpld(char *,int);

#ifdef __cplusplus
}
#endif

#endif /* __CELESTICA_THERMAL_H__ */


