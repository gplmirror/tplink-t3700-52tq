/*
   celestica-thermal.c - Thermal function for Celestica project, include
   fan control algorithm, hw monitoring.

    Copyright (c) 1998, 1999  Frodo Looijaard <frodol@dds.nl>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/i2c.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/interrupt.h>
#include "celestica-thermal.h"
#include <linux/delay.h>

#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/firmware.h>

/*
 * This driver handles all the thermal sensor and fan control chip function herein.
 * All of the thermal chips reside on the I2C bus.  With the Linux 3.8 kernel, the
 * kernel creates the I2C adapters and auto generates the I2C clients when finding 
 * matching entries with the thermal_ids array contained herein.  This is accomplished 
 * using this driver's published thermal_probe() function. 
 *  																																										
 * Some hardware monitor interface will be exported to user space for status monitor.
 * Also some fan control algorithm parameter tunning interface will be exported.
 */

MODULE_AUTHOR( "Beck He <bhe@Celestica.com>" );
MODULE_DESCRIPTION( "Thermal Driver" );
MODULE_LICENSE( "GPL" );

static int lm75counts = num_devices - 1;
static struct i2c_client *thermalclient[num_devices];
static int fan_chip_type;


/* list of I2C devices supported by this driver */
static const unsigned short normal_i2c[] = { 0x48, 0x4e, 0x4f, 0x4b, 0x49, 0x58, 0x59, 0x4d, I2C_CLIENT_END };

static const struct i2c_device_id thermal_ids[] = {
	{ "lm75_p2020", lm75_p2020, },

#ifdef CONFIG_REDSTONE
	{ "lm75_bcm56846", lm75_bcm56846, },
#else
	{ "lm75_bcm56850", lm75_bcm56850, },
#endif

    { "lm75_LIA", lm75_LIA, },
	{ "lm75_RIA", lm75_RIA, },
	{ "lm75_ROA", lm75_ROA, },
	{ "psu1L", psu1, },
	{ "psu1R", psu2, },
	{ "emc2305", fan_chip, },
	{ /* LIST END */ }
};
MODULE_DEVICE_TABLE(i2c, thermal_ids);

#define NAME  "thermal_driver"

DEFINE_MUTEX(celestica_thermal_mutex);
static DECLARE_WAIT_QUEUE_HEAD(thermal_wait_queue);

spinlock_t thermallock;

static struct workqueue_struct *polldev_wq;
static struct delayed_work delaywork;
static int polldev_users;

static struct class  *thermal_class;

#define LM75_REG_CONF 0x01

static const u8 LM75_REG_TEMP[3] = {
	0x00,  /* input */
	0x03,  /* max */
	0x02,  /* hyst */
};

#define EMC2305_CHIP          2
#define EMC2305_REG_VENDOR    0xfe
#define EMC2305_REG_DEVICE    0xfd
#define EMC2305_REG_FANSTATUS 0x25

#define EMC2305_VENDOR        0x5d
#define EMC2305_DEVICE        0x34
#define EMC2305_REG_PWM(x)    (0x30 + (x*0x10))

#define FAN_STALL_MASK    0x1f  /* FAN 1,2,3,4,5 STALL STATUS */
#define FAN_ALGO_ENABLE   1
#define FAN_ALGO_DISABLE  2
#define FAN_COUNT         5
#define PWM_COUNT         5

typedef struct {
	int fanspeed[FAN_COUNT];
	int pwm[FAN_COUNT];
	int fan_enabled;
	int fan_fail;
} FAN_CHIP;

#define UP_TREND	1
#define DOWN_TREND	2
#define FAN_CONTROL_ENABLE	1
#define FAN_CONTROL_DISABLE	2

// LM75 instance data for each device
typedef struct {
	long temp;
	long oldtemp;
	int maxtemp;
	int hysttemp;
	int temp_trend;
	int fan_control_enable;
	int last_trend;

	unsigned long lm75_last_update;
} LM75;

typedef struct {
	int idx;
	FAN_CHIP fanchip_data;
	LM75 lm75_data;
	int target_pwm_setting;
	int curr_pwm_setting;
	int target_pwm;
	int fan_algo_enabled;
	int up_delay;
	int down_delay;
	int last_pwm;
} thermal_data;

/* define the parameters for fan control algorithm, open them to user space */
/* each temp sensnor have their own curve with pwm duty cycle, thermal team */
/* can fine tune them during the test procedure */
typedef struct {
	int pwm_duty_min;
	int pwm_duty_max;
	int low_temp;
	int high_temp;
	int	high_warning;
	int	high_critical;
} thermal_debug_data;

// Note: currently these are the same for REDSTONE and SMALLSTONE

/* pre-defined, will be finalized after thermal test */
static thermal_debug_data algo_para0[LM75_TEMP_COUNT] = {
	{60, 100, 25, 40, 74, 79},    /* p2020 */
	{60, 100, 80, 100, 89, 94},   /* 568xx */
	{65, 75, 32, 41, 53, 57},     /* LIA */
	{60, 100, 25, 40, 53, 57},    /* RIA */
	{65, 80, 26, 36, 55, 58},     /* ROA */
	{55, 70, 29, 38, 100, 200},   /* psu1 */
	{35, 45, 34, 45, 100, 200},   /* psu2 */
};

static thermal_debug_data algo_para1[LM75_TEMP_COUNT] = {
	{60, 100, 25, 40, 74, 79},    /* p2020 */
	{60, 100, 80, 100, 84, 89},   /* 568xx */
	{75, 100, 41, 48, 59, 62},    /* LIA */
	{60, 100, 25, 40, 59, 62},    /* RIA */
	{80, 100, 36, 46, 49, 52},    /* ROA */
	{70, 100, 38, 47, 100, 200},  /* psu1 */
	{45, 65, 45, 49, 100, 200},   /* psu2 */
};

/* TEMP: 0.001C/bit (-55C to +125C)
   REG: (0.5C/bit, two's complement) << 7 */
#define LM75_TEMP_MIN (-55000)
#define LM75_TEMP_MAX 125000

static inline u16 LM75_TEMP_TO_REG(long temp)
{
	long ntemp = SENSORS_LIMIT(temp, LM75_TEMP_MIN, LM75_TEMP_MAX);
	ntemp += (ntemp < 0 ? -250 : 250);
	return (u16)((ntemp / 500) << 7);
}

static inline int LM75_TEMP_FROM_REG(u32 reg)
{
	/* use integer division instead of equivalent right shift to
	   guarantee arithmetic shift and preserve the sign */
	return ((s32)reg / 128) / 2;
}

static inline s32 i2c_thermal_read_byte_data(struct i2c_client *client, u8 reg)
{
	int temp;
	u8 data;
	int count = 10;
	temp = i2c_smbus_read_byte_data(client, reg);
	
	while ((temp < 0 || temp == 0xff) && count-- > 0){
		msleep(11);
		temp = i2c_smbus_read_byte_data(client, reg);
	}
	
	if (temp < 0) {
		return temp;
	}
	data = (u8) temp;
	return data;
}

/* register access */

/* All registers are word-sized, except for the configuration register.
   LM75 uses a high-byte first convention, which is exactly opposite to
   the SMBus standard. */
static int lm75_read_value(struct i2c_client *client, u8 reg)
{
	int value;

	if (reg == LM75_REG_CONF)
		return i2c_thermal_read_byte_data(client, reg);

	value = i2c_smbus_read_word_data(client, reg);
	return (value < 0) ? value : swab16(value);
}

short convert_short(char *s, int len)
{
	short i;
	short res = 0;

	for (i = 1; i < len; i++) {
		res = res * 2 + (s[i] - '0');
	}
	if (s[0] == '1')
		return -res;

	return res;
}

long convert_linear(char *data)
{
	char low, high;
	short N, Y;
	long ret;
	int temp = 1;
	char s[11];
	int i;

	low = data[0];
	high = data[1];
	if ((high & 0x80) > 0)
		high = ~high + 0x88;
	for (i = 0; i < 5; i++) {
		s[i] = (high & (0x80 >> i)) > 0 ? '1' : '0';
	}
	N = convert_short(s, 5);
	high = data[1];
	if ((high & 0x04) > 0) {
		high = ~high + 0x40;
		if (low != 0xff)
			low = ~low + 0x1;
		else {
			low = 0x00;
			high += 0x1;
		}

	}
	for (i = 5; i < 8; i++)
		s[i - 5] = (high & (0x80 >> i)) > 0 ? '1' : '0';
	for (i = 0; i < 8; i++)
		s[i + 3] = (low & (0x80 >> i)) > 0 ? '1' : '0';
	Y = convert_short(s, 11);
	if (N > 0) {
		while (N > 0) {
			temp = temp * 2;
			N--;
		}
		ret = Y * temp * 100;
	} else {
		N = 0 - N;
		while (N > 0) {
			temp = temp * 2;
			N--;
		}
		ret = (Y * 100) / temp;
	}

	return ret;
}

static int psu_update_device(struct i2c_client *client)
{
	char arg[2];
	long rtn;
	int i;
	int val;
	u8 value[14];

	thermal_data *data;

	if ((client == NULL) || ((data = i2c_get_clientdata(client)) == NULL)) {
		return -1;
	}

	for (i = 0; i < 10; i++) {
		val = i2c_smbus_read_word_data(client, 0x8d);

		if (val < 0)
			msleep(11);
		else
			break;
	}

	if (val < 0)
		return -1;

	arg[1] = (val >> 8) & 0x00ff;
	arg[0] = val & 0x00ff;
	rtn = convert_linear(arg);
	data->lm75_data.temp = rtn / 100;

	if (rtn/100 > data->lm75_data.oldtemp) {
		if (data->lm75_data.last_trend == DOWN_TREND)
			data->up_delay = data->lm75_data.oldtemp;

		data->lm75_data.temp_trend = UP_TREND;
		data->lm75_data.last_trend = UP_TREND;
		data->last_pwm = data->target_pwm;

	} else if (rtn/100 == data->lm75_data.oldtemp) {
		data->lm75_data.temp_trend = data->lm75_data.last_trend;

	} else {
		if (data->lm75_data.last_trend == UP_TREND)
			data->down_delay = data->lm75_data.oldtemp;

		data->lm75_data.temp_trend = DOWN_TREND;
		data->lm75_data.last_trend = DOWN_TREND;
		data->last_pwm = data->target_pwm;
	}

	data->lm75_data.oldtemp = rtn / 100;

	for (i = 0; i < 10; i++) {
		val = i2c_smbus_read_i2c_block_data(client, 0x9a, 14, value);

		if ((val < 0) || (value[11] == 0xff))
			msleep(11);
		else
			break;
	}

	if (val < 0)
		return -1;

	data->lm75_data.lm75_last_update = jiffies;

	return 0;
}

static int lm75_update_device(struct i2c_client *client)
{
	int temp;
	thermal_data *data;

	if ((client == NULL) || ((data = i2c_get_clientdata(client)) == NULL)) {
		return -1;
	}

	if ((data->idx == psu1) || (data->idx == psu2))
		return 0;

	temp = lm75_read_value(client, LM75_REG_TEMP[0]);
	msleep(11);

	if (temp >= 0) {
		data->lm75_data.temp = temp;

#ifdef THERMAL_DEBUG
		printk( KERN_ALERT NAME ": LM75 read Temp = %d\n", LM75_TEMP_FROM_REG(temp));
#endif

		if (LM75_TEMP_FROM_REG(temp) >= algo_para0[data->idx].high_critical) {
			printk(KERN_WARNING "%s(%d C) Reach High Critical Temperature!\n", client->name, LM75_TEMP_FROM_REG(temp));
		} else if(LM75_TEMP_FROM_REG(temp) >= algo_para0[data->idx].high_warning) {
			printk(KERN_WARNING "%s(%d C) Reach High Warning Temperature!\n", client->name, LM75_TEMP_FROM_REG(temp));
		}

		if (LM75_TEMP_FROM_REG(temp) > data->lm75_data.oldtemp) {
			if (data->lm75_data.last_trend == DOWN_TREND)
				data->up_delay = data->lm75_data.oldtemp;

			data->lm75_data.temp_trend = UP_TREND;
			data->lm75_data.last_trend = UP_TREND;
			data->last_pwm = data->target_pwm;

		} else if (LM75_TEMP_FROM_REG(temp) == data->lm75_data.oldtemp) {
			data->lm75_data.temp_trend = data->lm75_data.last_trend;

		} else {
			if (data->lm75_data.last_trend == UP_TREND)
				data->down_delay = data->lm75_data.oldtemp;

			data->lm75_data.temp_trend = DOWN_TREND;
			data->lm75_data.last_trend = DOWN_TREND;
			data->last_pwm = data->target_pwm;
		}

		data->lm75_data.oldtemp = LM75_TEMP_FROM_REG(temp);
	}

	temp = lm75_read_value(client, LM75_REG_TEMP[1]);
	msleep(11);

	if (temp >= 0) {
		data->lm75_data.maxtemp = temp;

#ifdef THERMAL_DEBUG
		printk( KERN_ALERT NAME ": LM75 read Max Temp = %d\n", LM75_TEMP_FROM_REG(temp));
#endif

		temp = lm75_read_value(client, LM75_REG_TEMP[2]);
		msleep(11);

		if (temp >= 0) {
			data->lm75_data.hysttemp = temp;

#ifdef THERMAL_DEBUG
			printk( KERN_ALERT NAME ": LM75 read Hyst Temp = %d\n", LM75_TEMP_FROM_REG(temp));
#endif
		}
	}

	data->lm75_data.lm75_last_update = jiffies;

	return 0;
}

static int set_psu_pwm(struct i2c_client *client, int pwm)
{
	int status;

	client->flags |= I2C_CLIENT_PEC;
	status = i2c_smbus_write_word_data(client, 0x3b, pwm);

#ifdef THERMAL_DEBUG
	if (status < 0)
		printk(KERN_ALERT NAME " set_psu_pwm(%d) failed, status %d\n", pwm, status);
#endif

	return 0;
}

/* set all pwm with same duty cycle */
static int set_pwm(struct i2c_client *client, int pwm)
{
	int i;
	int status = -1;
	thermal_data *data;

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		data->curr_pwm_setting = pwm;

		for (i = 0; i < PWM_COUNT; i++) {
			if (fan_chip_type == EMC2305_CHIP) {
				status = i2c_smbus_write_byte_data(client, EMC2305_REG_PWM(i), pwm);
			}

			msleep(11);

#ifdef THERMAL_DEBUG
			if (status < 0) {
				printk(KERN_ERR NAME " set_pwm(%d) failed status=%d\n", pwm, status);
			}
#endif
		}
	}

	return (status);
}

static int EMC2305_REG_FAN(int fan)
{
	return (0x3e + fan * 0x10);
}

/*
 * 16-bit registers on the emc2305 are high-byte first.
*/
static inline int emc2305_read_word_data(struct i2c_client *client, u8 reg)
{
	int foo;
	
	msleep(11);
	foo = i2c_thermal_read_byte_data(client, reg) << 8;

	foo |= (u16)i2c_thermal_read_byte_data(client, reg + 1);
	msleep(11);
	
	return foo;
}

static inline int emc2305_fanstall_status(struct i2c_client  *client)
{
	int status = 0;

	status = i2c_thermal_read_byte_data(client, EMC2305_REG_FANSTATUS);
	msleep(11);
	status &= FAN_STALL_MASK;

	return status;
}

static int emc2305_update_device(struct i2c_client *client)
{
	int i;
	int fanspeed = 0;
	int fail_count = 0;
	thermal_data *data;

	if ((client == NULL) || ((data = i2c_get_clientdata(client)) == NULL)) {
		return -1;
	}

	data->fanchip_data.fan_fail = 0;

	for (i = 0; i < FAN_COUNT; i++) {
		fanspeed = emc2305_read_word_data(client, EMC2305_REG_FAN(i)) >> 3;
		if (fanspeed >= 0)
			data->fanchip_data.fanspeed[i] = fanspeed;

		msleep(11);

#ifdef THERMAL_DEBUG
		if (data->fanchip_data.fanspeed[i] < 0)
			printk(KERN_ERR NAME ": Error emc2305 update fanspeed \n");
#endif
	}

	/* fan stall */
	if ((fail_count = emc2305_fanstall_status(client)) != 0) {
		for (i = 0; i < FAN_COUNT; i++) {
			if ((fail_count & (0x1 << i)) > 0)
				data->fanchip_data.fan_fail += 1;
		}
	}

	return 0;
}

static ssize_t manual_pwm_store(struct class *cls, struct class_attribute *attr, const char *buffer, size_t count)
{
	int value;
	struct i2c_client *client = thermalclient[fan_chip];
	thermal_data *data;

	if ((client == NULL) || ((data = i2c_get_clientdata(client)) == NULL)) {
		return 0;
	}

	mutex_lock(&celestica_thermal_mutex);
	sscanf(buffer, "%d", &value);
	if (value > 255)
		return 0;

	data->fan_algo_enabled = FAN_ALGO_DISABLE;
	set_pwm(client, value);
	mutex_unlock(&celestica_thermal_mutex);

	return count;
}

static ssize_t ambient1_temp_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int status;
	long temp = 0;
	struct i2c_client *client;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[lm75_p2020];

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		status = lm75_update_device(client);
		if (!status) {
			temp = data->lm75_data.temp;
		}
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", LM75_TEMP_FROM_REG(temp));
}

static ssize_t ambient2_temp_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int status;
	long temp = 0;
	struct i2c_client *client;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);

#ifdef CONFIG_REDSTONE
	client = thermalclient[lm75_bcm56846];
#else
	client = thermalclient[lm75_bcm56850];
#endif

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		status = lm75_update_device(client);
		if (!status) {
			temp = data->lm75_data.temp;
		}
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", LM75_TEMP_FROM_REG(temp));
}


static ssize_t LIA_temp_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int status;
	long temp = 0;
	struct i2c_client *client;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[lm75_LIA];

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		status = lm75_update_device(client);

		if (!status) {
			temp = data->lm75_data.temp;
		}
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", LM75_TEMP_FROM_REG(temp));
}

static ssize_t RIA_temp_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int status;
	long temp = 0;
	struct i2c_client *client;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[lm75_RIA];

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		status = lm75_update_device(client);

		if (!status) {
			temp = data->lm75_data.temp;
		}
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", LM75_TEMP_FROM_REG(temp));
}


static ssize_t systemoutlet_temp_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	long temp = 0;
	struct i2c_client *client;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);

	client = thermalclient[lm75_ROA];

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		temp = data->lm75_data.temp;  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", LM75_TEMP_FROM_REG(temp));
}

static ssize_t psu1_status_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int psu_status = 0;
    unsigned char flag;
	struct i2c_client *client;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[psu1];

#ifdef CONFIG_REDSTONE
	if (client != NULL) {
		flag = read_cpld("reset", 0x80);

		if ((flag & 0x08) == 0)
			psu_status = 2;  // power not OK
		else
			psu_status = 1;  // PSU is present
	}
#else  // SMALLSTONE
	if (client != NULL) {
		flag = read_cpld("reset", 0x80);

		if ((flag & 0x20) != 0)
			psu_status = 0;  // PSU not present
		else if ((flag & 0x08) == 0)
			psu_status = 2;  // power not OK
		else
			psu_status = 1;  // PSU is present
	}
#endif

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", psu_status);
}

static ssize_t psu2_status_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	int psu_status = 0;
    unsigned char flag;
	struct i2c_client *client;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[psu2];

#ifdef CONFIG_REDSTONE
	if (client != NULL) {
        flag = read_cpld("reset", 0x80);

        if ((flag & 0x04) == 0)
            psu_status = 2;  // power not OK
		else
            psu_status = 1;  // PSU is present
	}
#else  // SMALLSTONE
    if (client != NULL) {
        flag = read_cpld("reset", 0x80);

        if ((flag & 0x10) != 0)
            psu_status = 0;  // PSU not present
        else if ((flag & 0x04) == 0)
            psu_status = 2;  // power not OK
        else
            psu_status = 1;  // PSU is present
    }
#endif

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", psu_status);
}

static ssize_t psuinlet1_temp_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	long temp = 0;
	struct i2c_client *client;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[psu1];

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		temp = data->lm75_data.temp;  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%ld\n", temp);
}

static ssize_t psuinlet2_temp_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	long temp = 0;
	struct i2c_client *client;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[psu2];

	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		temp = data->lm75_data.temp;  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%ld\n", temp);
}

/* datasheet says to divide this number by the fan reading to get fan rpm */
static inline int FAN_PERIOD_TO_RPM(int x)
{
	if (fan_chip_type == EMC2305_CHIP) {
		if (x > (0xf5c2 >> 3)) /* <1000rpm, fan fail */
			return 0;
		return 3932160 * 2 / x;
	}

	return 0;
}


static ssize_t fan1speed_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	struct i2c_client *client;
	thermal_data *data;

	int fanspeed = 0;
	int fan_index = 3;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[fan_chip];

	// Note: fan1speed was stored in the fanchip device data
	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		fanspeed = data->fanchip_data.fanspeed[fan_index];  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", FAN_PERIOD_TO_RPM(fanspeed));
}

static ssize_t fan2speed_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	struct i2c_client *client;
	thermal_data *data;

	int fanspeed = 0;
	int fan_index = 1;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[fan_chip];

	// Note: fan2speed was stored in the fanchip device data
	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		fanspeed = data->fanchip_data.fanspeed[fan_index];  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", FAN_PERIOD_TO_RPM(fanspeed));
}

static ssize_t fan3speed_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	struct i2c_client *client;
	thermal_data *data;

	int fanspeed = 0;
	int fan_index = 0;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[fan_chip];

	// Note: fan3speed was stored in the fanchip device data
	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		fanspeed = data->fanchip_data.fanspeed[fan_index];  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", FAN_PERIOD_TO_RPM(fanspeed));
}

static ssize_t fan4speed_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	struct i2c_client *client;
	thermal_data *data;

	int fanspeed = 0;
	int fan_index = 4;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[fan_chip];

	// Note: fan4speed was stored in the fanchip device data
	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		fanspeed = data->fanchip_data.fanspeed[fan_index];  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", FAN_PERIOD_TO_RPM(fanspeed));
}

static ssize_t fan5speed_show(struct class *cls, struct class_attribute *attr, char *buf)
{
	struct i2c_client *client;
	thermal_data *data;

	int fanspeed = 0;
	int fan_index = 2;

	mutex_lock(&celestica_thermal_mutex);
	client = thermalclient[fan_chip];

	// Note: fan5speed was stored in the fanchip device data
	if ((client != NULL) && ((data = i2c_get_clientdata(client)) != NULL)) {
		fanspeed = data->fanchip_data.fanspeed[fan_index];  // use last reading performed
	}

	mutex_unlock(&celestica_thermal_mutex);

	return sprintf(buf, "%d\n", FAN_PERIOD_TO_RPM(fanspeed));
}

/* change the sys file name, but share the top six temp function with Kennisis, so the sys name may not match
the function name. */
static CLASS_ATTR(p2020_temp, S_IRUGO | S_IWUSR, ambient1_temp_show, NULL);

#ifdef CONFIG_REDSTONE
static CLASS_ATTR(bcm56846_temp, S_IRUGO | S_IWUSR, ambient2_temp_show, NULL);
#else
static CLASS_ATTR(bcm56850_temp, S_IRUGO | S_IWUSR, ambient2_temp_show, NULL);
#endif

static CLASS_ATTR(LIA_temp, S_IRUGO | S_IWUSR, LIA_temp_show, NULL);
static CLASS_ATTR(RIA_temp, S_IRUGO | S_IWUSR, RIA_temp_show, NULL);
static CLASS_ATTR(ROA_temp, S_IRUGO | S_IWUSR, systemoutlet_temp_show, NULL);
static CLASS_ATTR(psuinlet1_temp, S_IRUGO | S_IWUSR, psuinlet1_temp_show, NULL);
static CLASS_ATTR(psuinlet2_temp, S_IRUGO | S_IWUSR, psuinlet2_temp_show, NULL);
static CLASS_ATTR(psu1_status, S_IRUGO | S_IWUSR, psu1_status_show, NULL);
static CLASS_ATTR(psu2_status, S_IRUGO | S_IWUSR, psu2_status_show, NULL);
static CLASS_ATTR(fan1speed, S_IRUGO | S_IWUSR, fan1speed_show, NULL);
static CLASS_ATTR(fan2speed, S_IRUGO | S_IWUSR, fan2speed_show, NULL);
static CLASS_ATTR(fan3speed, S_IRUGO | S_IWUSR, fan3speed_show, NULL);
static CLASS_ATTR(fan4speed, S_IRUGO | S_IWUSR, fan4speed_show, NULL);
static CLASS_ATTR(fan5speed, S_IRUGO | S_IWUSR, fan5speed_show, NULL);

static struct class_attribute *attrgroup[] = {
	&class_attr_p2020_temp,

#ifdef CONFIG_REDSTONE
    &class_attr_bcm56846_temp,
#else
    &class_attr_bcm56850_temp,
#endif

	&class_attr_LIA_temp,
	&class_attr_RIA_temp,
	&class_attr_ROA_temp,
	&class_attr_psuinlet1_temp,
	&class_attr_psuinlet2_temp,
	&class_attr_psu1_status,
	&class_attr_psu2_status,
	&class_attr_fan1speed,
	&class_attr_fan2speed,
	&class_attr_fan3speed,
	&class_attr_fan4speed,
	&class_attr_fan5speed,
};

#define NUM_THERMAL_FILES 14

static CLASS_ATTR(manual_pwm,  S_IWUSR | S_IRUGO, NULL, manual_pwm_store);

static struct class_attribute *attrgroup1[] = {
	&class_attr_manual_pwm,
};

#define NUM_THERMAL_DEBUG_FILES  1

static int thermal_data_copy(thermal_debug_data *p1, thermal_debug_data *p2)
{
	p1->pwm_duty_min = p2->pwm_duty_min;
	p1->pwm_duty_max = p2->pwm_duty_max;
	p1->low_temp = p2->low_temp;
	p1->high_temp = p2->high_temp;

	return 0;
}

static int thermal_temp_change(thermal_debug_data *p1, thermal_debug_data *p2)
{
	p1->high_warning = p2->high_warning;
	p1->high_critical = p2->high_critical;

	return 0;
}

#ifdef CONFIG_REDSTONE
extern int read_hw(void);
#endif

int wind_redirect = 0; //1:f->b 2:b->f

static int LM75_Init(struct i2c_client *client)
{
	int cur, conf, hyst;
	int i;
	int retval;
	u8 value[32];
	u8 temp;
	u8 temp1;
	int wind_direction_hw=0;
	unsigned long local_jiffies = jiffies;
	thermal_data *data;

	if ((client == NULL) || ((data = i2c_get_clientdata(client)) == NULL)) {
		printk( KERN_ALERT NAME "client does not exist\n");
		goto error_LM75;
	}

	data->lm75_data.lm75_last_update = local_jiffies;

	// determine if current device can control the fan
	if (data->idx == lm75_LIA || data->idx == lm75_ROA || data->idx == psu1 || data->idx == psu2)
	{
		data->lm75_data.fan_control_enable = FAN_CONTROL_ENABLE;
	} else {
		data->lm75_data.fan_control_enable = FAN_CONTROL_DISABLE;
	}

	data->lm75_data.temp_trend = UP_TREND; /* Default UP_TREND */
	data->lm75_data.last_trend = UP_TREND;

	data->lm75_data.oldtemp = 25;
	data->last_pwm = 60;

	if ((data->idx != psu1) && (data->idx != psu2)) {
		/* Unused addresses */
		cur = i2c_smbus_read_word_data(client, 0);
		msleep(11);

#ifdef THERMAL_DEBUG
		if (cur < 0) {
			printk( KERN_ALERT NAME "LM75 init read cur error!\n");
		}
#endif

		conf = i2c_thermal_read_byte_data(client, 1);
		msleep(11);

#ifdef THERMAL_DEBUG
		if (conf < 0) {
			printk( KERN_ALERT NAME "LM75 init read conf error!\n");
		}
#endif

		hyst = i2c_smbus_read_word_data(client, 2);
		msleep(11);

#ifdef THERMAL_DEBUG
		if (hyst < 0) {
			printk( KERN_ALERT NAME "LM75 init read hyst error!\n");
		}
#endif
	}
	else {
		for (i = 0; i < 10; i++) {
		  retval = i2c_smbus_read_i2c_block_data(client, 0x9a, 14, value);

		  if ((retval < 0) || (value[11] == 0xff))
			  msleep(11);
		  else
			  break;
		}

		temp = value[11];
		temp1 = value[13];

		/* 0x80 STATUS_MFR_SPECIFIC  */
		wind_direction_hw = i2c_thermal_read_byte_data(client, 0x80); 

		if (wind_direction_hw < 0) {
			wind_direction_hw = 0;
		}
		else {
			wind_direction_hw &= 0x03; /*wind_direction_hw:0x02,f-->b; 0x01,b-->f*/

			if (wind_direction_hw != 0x01 || wind_direction_hw != 0x02)
				wind_direction_hw = 0;
		}
				
		/* BEFORE,judge wind direction via ID info at Reg 0x9a, while ,from NOW, this also is done by bit 0 and bit 1
		   at Reg 0x80. For compatiblity, add "wind_direction_hw", same function with "wind_redirect". 
		   Hope no hardware change any longer.	*/
		if ((wind_direction_hw & 0x02) || ((temp == 0x44)||(temp == 0x47) ||
			(temp == 0x32 && temp1 == 0x41) || (temp == 0x36 && temp1 == 0x41) ||
			(temp == 0x32 && temp1 == 0x42)))
		{
			temp = 0;

			/* Consider such a case: 
			   for some reason, HW changes the wind direction bit (0x80,bit[0])to be 1 in PSU ,
			   which wind direction is assumed as front2back(f-->b), so this "if" statement 
			   can address such ODD case. Chris wang */
			if ((wind_direction_hw & 0x01) || (wind_redirect == 2)) {
				// printk(KERN_ALERT "ERROR: Two PSUSs wind are different!\n");
				goto exit_LM75;
			}

			wind_redirect = 1;

			// printk(KERN_ALERT "Wind direction is f->b\n");
			thermal_data_copy(&algo_para0[psu2], &algo_para0[psu1]);
			thermal_data_copy(&algo_para1[psu2], &algo_para1[psu1]);

#ifdef CONFIG_REDSTONE
			if (2 != read_hw()) {
				for (i = 0; i < lm75counts; i++) {
					thermal_temp_change(&algo_para1[i], &algo_para0[i]);
				}
			}
			else {
				algo_para0[0].high_warning = 74; algo_para0[0].high_critical = 79;
				algo_para0[1].high_warning = 86; algo_para0[1].high_critical = 91;
				algo_para0[2].high_warning = 55; algo_para0[2].high_critical = 59;
				algo_para0[3].high_warning = 55; algo_para0[3].high_critical = 59;
				algo_para0[4].high_warning = 56; algo_para0[4].high_critical = 60;

				for (i = 0; i < lm75counts; i++) {
					thermal_temp_change(&algo_para1[i], &algo_para0[i]);
				}
			}
#else // SMALLSTONE
			for (i = 0; i < lm75counts; i++) {
				thermal_temp_change(&algo_para1[i], &algo_para0[i]);
			}
#endif

			if (data->idx == lm75_ROA) {
				data->lm75_data.fan_control_enable = FAN_CONTROL_DISABLE;
			}
		}
		else {
			temp = 0;
					
			/* Case: HW changes the wind direction bit(0x80, bit[1]) to be 1 in PSU Chris wang */
			if ((wind_direction_hw & 0x02) || (wind_redirect == 1)) {
				// printk(KERN_ALERT "ERROR: Two PSUSs wind are different!\n");
				goto exit_LM75;
			}

			wind_redirect = 2;

			// printk(KERN_ALERT "Wind direction is b->f\n");
			thermal_data_copy(&algo_para0[psu1], &algo_para0[psu2]);
			thermal_data_copy(&algo_para1[psu1], &algo_para1[psu2]);

#ifdef CONFIG_REDSTONE
			if (2 != read_hw()) {
				for (i = 0; i < lm75counts; i++) {
					thermal_temp_change(&algo_para0[i], &algo_para1[i]);
				}
			}
			else {
				algo_para1[0].high_warning = 77; algo_para1[0].high_critical = 83;
				algo_para1[1].high_warning = 80; algo_para1[1].high_critical = 85;
				algo_para1[2].high_warning = 55; algo_para1[2].high_critical = 58;
				algo_para1[3].high_warning = 55; algo_para1[3].high_critical = 58;
				algo_para1[4].high_warning = 50; algo_para1[4].high_critical = 51;

				for (i = 0; i < lm75counts; i++) {
					thermal_temp_change(&algo_para0[i], &algo_para1[i]);
				}
			}
#else // SMALLSTONE
			for (i = 0; i < lm75counts; i++) {
				thermal_temp_change(&algo_para0[i], &algo_para1[i]);
			}
#endif

			if (data->idx == lm75_LIA) {
				data->lm75_data.fan_control_enable = FAN_CONTROL_DISABLE;
			}
		}
	}

exit_LM75:
	return 0;

error_LM75:
	printk( KERN_ALERT NAME "LM75 init error");
	return -1;
}

#define MAX_PWM      100
#define MIN_PWM      60
#define PSU1_MAX_PWM 65
#define PSU2_MAX_PWM 55

static int EMC2305_Init(struct i2c_client *client)
{
	int status = 0;
	thermal_data *data;

	if ((client == NULL) || ((data = i2c_get_clientdata(client)) == NULL)) {
		printk( KERN_ALERT NAME "client does not exist\n");
		goto error_emc2305;
	}

	printk(KERN_ALERT "Initializing EMC2305 Fan Chip\n");

	/* min driver 20% */
	status += i2c_smbus_write_byte_data(client, 0x38,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x48,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x58,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x68,  0x33);
	msleep(11);
	status += i2c_smbus_write_byte_data(client, 0x78,  0x33);
	msleep(11);
	return 0;

error_emc2305:
	printk( KERN_ALERT NAME "EMC2305 init error");
	return status;
}

static int Fan_Chip_Init(struct i2c_client *client)
{
	u8 vendor, device;
	int status = 0;
	thermal_data *data;

	if ((client == NULL) || ((data = i2c_get_clientdata(client)) == NULL)) {
		printk( KERN_ALERT NAME "client does not exist\n");
		goto error_fan_init;
	}

	vendor = i2c_thermal_read_byte_data(client, EMC2305_REG_VENDOR);
	device = i2c_thermal_read_byte_data(client, EMC2305_REG_DEVICE);

	if (vendor == EMC2305_VENDOR && device == EMC2305_DEVICE) {
		printk( KERN_ALERT NAME " Fan chip is EMC2305\n");
		status = EMC2305_Init(client);
		fan_chip_type = EMC2305_CHIP;
		goto fan_init_exit;
	}

	printk( KERN_ALERT NAME "Unknown Fan chip\n");
	goto error_fan_init;

fan_init_exit:
	/* initial max speed */
	set_pwm(client, 100 * 255 / 100);
	return 0;

error_fan_init:
	printk( KERN_ALERT NAME "init error");
	return -1;
}

#ifdef THERMAL_SIMULATION
static long sim_temp = 0xf00;
static int down_flag = 0;
static int sim_index = 0;
#endif

static void calculate_target_pwm(struct i2c_client *client)
{
	int i;
	int pwm_duty_min = 0;
	int pwm_duty_max = 0;
	int low_temp = 0;
	int high_temp = 0;
	int k;
	long temp;
	thermal_data *fan_data;  // fan_chip data
	thermal_data *data;      // current LM75 data
	thermal_data *psu_data;  // PSU-1 or PSU-2 data

	// Note: the client passed in is the the fan_chip
	if ((client == NULL) || ((fan_data = i2c_get_clientdata(client)) == NULL)) {
		return;
	}

	fan_data->target_pwm_setting = 0;

	// calculate the PWM value for each LM75 and save results in the fan_chip data
	for (i = 0; i < lm75counts; i++) {
		// skip fans that do not exist
		if ((thermalclient[i] == NULL) || ((data = i2c_get_clientdata(thermalclient[i])) == NULL))
			continue;

		// if fan is not enabled, then use the minimum PWM for this device
		if (data->lm75_data.fan_control_enable != FAN_CONTROL_ENABLE) {
			data->target_pwm = MIN_PWM;
			continue;
		}

		// get current temperature reading according to device type
		if ((data->idx != psu1) && (data->idx != psu2))
			temp = LM75_TEMP_FROM_REG(data->lm75_data.temp);  // LM75 device temperature
		else
			temp = data->lm75_data.temp;  // PSU-1 or PSU-2 temperature

		if (wind_redirect == 1) {
			if (temp < algo_para0[i].high_temp) {
				pwm_duty_min = algo_para0[i].pwm_duty_min;
				pwm_duty_max = algo_para0[i].pwm_duty_max;
				low_temp  = algo_para0[i].low_temp;
				high_temp = algo_para0[i].high_temp;
			} else {
				pwm_duty_min = algo_para1[i].pwm_duty_min;
				pwm_duty_max = algo_para1[i].pwm_duty_max;
				low_temp  = algo_para1[i].low_temp;
				high_temp = algo_para1[i].high_temp;
			}
		} else if (wind_redirect == 2) {
			// wind_direct 2, UP_TREND
			if (data->lm75_data.temp_trend == UP_TREND) {
				if (temp < algo_para0[i].high_temp) {
					pwm_duty_min = algo_para0[i].pwm_duty_min;
					pwm_duty_max = algo_para0[i].pwm_duty_max;
					low_temp  = algo_para0[i].low_temp;
					high_temp = algo_para0[i].high_temp;
				} else {
					pwm_duty_min = algo_para1[i].pwm_duty_min;
					pwm_duty_max = algo_para1[i].pwm_duty_max;
					low_temp  = algo_para1[i].low_temp;
					high_temp = algo_para1[i].high_temp;
				}

			// wind direct 2, DOWN_TREND
			} else if (data->lm75_data.temp_trend == DOWN_TREND) {
				// PSU-1 or PSU-2
				if ((data->idx == psu1) || (data->idx == psu2)) {
					if (temp < algo_para0[i].high_temp - 6) {
						pwm_duty_min = algo_para0[i].pwm_duty_min;
						pwm_duty_max = algo_para0[i].pwm_duty_max;
						low_temp  = algo_para0[i].low_temp - 6;
						high_temp = algo_para0[i].high_temp - 6;
					} else {
						pwm_duty_min = algo_para1[i].pwm_duty_min;
						pwm_duty_max = algo_para1[i].pwm_duty_max;
						low_temp  = algo_para1[i].low_temp - 6;
						high_temp = algo_para1[i].high_temp - 6;
					}
				}
				// LM75 ROA or LM75 LIA
				else if ((data->idx == lm75_ROA) || (data->idx == lm75_LIA)) {
					if (temp < algo_para0[i].high_temp - 3) {
						pwm_duty_min = algo_para0[i].pwm_duty_min;
						pwm_duty_max = algo_para0[i].pwm_duty_max;
						low_temp  = algo_para0[i].low_temp - 3;
						high_temp = algo_para0[i].high_temp - 3;
					} else {
						pwm_duty_min = algo_para1[i].pwm_duty_min;
						pwm_duty_max = algo_para1[i].pwm_duty_max;
						low_temp  = algo_para1[i].low_temp - 3;
						high_temp = algo_para1[i].high_temp - 3;
					}
				}
			}
		}  // end of else if (wind_direct == 2)

		if (data->lm75_data.temp_trend == UP_TREND) {
			k = (pwm_duty_max - pwm_duty_min) * 10 / (high_temp - low_temp);

			// UP_TREND and not PSU-1 or PSU-2
			if ((data->idx != psu1) && (data->idx != psu2)) {
				data->target_pwm = pwm_duty_min + k * (temp - low_temp) / 10;

				if (wind_redirect == 2) {
					if ((temp - data->up_delay) <= 3) {
						if (data->last_pwm < data->target_pwm) {
							if (data->target_pwm > pwm_duty_max) 
								data->target_pwm = pwm_duty_max;

							if (data->target_pwm < pwm_duty_min) 
								data->target_pwm = pwm_duty_min;

							fan_data->target_pwm_setting = data->target_pwm;
							continue;
						}

						fan_data->target_pwm_setting = data->target_pwm = data->last_pwm;
						continue;
					}
				}  // end of if (wind_direct == 2)

				if (data->target_pwm > pwm_duty_max)
					data->target_pwm = pwm_duty_max;

				if (data->target_pwm < pwm_duty_min)
					data->target_pwm = pwm_duty_min;

				if (data->target_pwm > fan_data->target_pwm_setting)
					fan_data->target_pwm_setting = data->target_pwm;

			} else {  // UP_TREND LM75 device processing
				data->target_pwm = pwm_duty_min + k * (data->lm75_data.temp - low_temp) / 10;

				if (wind_redirect == 2) {
					if ((temp - data->up_delay) <= 6) {
						if (data->last_pwm < data->target_pwm) {
							if (data->target_pwm > pwm_duty_max) 
								data->target_pwm = pwm_duty_max;

							if (data->target_pwm < pwm_duty_min) 
								data->target_pwm = pwm_duty_min;
							continue;
						}

						data->target_pwm = data->last_pwm;
						continue;
					}
				}

				if (data->target_pwm > pwm_duty_max)
					data->target_pwm = pwm_duty_max;

				if (data->target_pwm < pwm_duty_min)
					data->target_pwm = pwm_duty_min;
			}

		} else { /* DOWN_TREND processing */
			k = (pwm_duty_max - pwm_duty_min) * 10 / (high_temp - low_temp);

			// DOWN_TREND and not PSU-1 or PSU-2
			if ((data->idx != psu1) && (data->idx != psu2)) {
				data->target_pwm = pwm_duty_min + k * (temp - low_temp) / 10;

				if (wind_redirect == 2) {
					if (temp >= algo_para1[i].high_temp) {
						fan_data->target_pwm_setting = data->target_pwm = pwm_duty_max;
						continue;
					}

					if ((data->down_delay - temp) <= 3) {
						if (data->last_pwm > data->target_pwm) {
							if (data->target_pwm > pwm_duty_max) 
								data->target_pwm = pwm_duty_max;

							if (data->target_pwm < pwm_duty_min) 
								data->target_pwm = pwm_duty_min;

							fan_data->target_pwm_setting = data->target_pwm;
							continue;
						}

						fan_data->target_pwm_setting = data->target_pwm = data->last_pwm;
						continue;
					}
				}

				if (data->target_pwm > pwm_duty_max)
				  data->target_pwm = pwm_duty_max;

				if (data->target_pwm < pwm_duty_min)
				  data->target_pwm = pwm_duty_min;

				if (data->target_pwm > fan_data->target_pwm_setting)
				  fan_data->target_pwm_setting = data->target_pwm;

			} else {  // DOWN_TREND LM75 device processing
				data->target_pwm = pwm_duty_min + k * (data->lm75_data.temp - low_temp) / 10;

				if (wind_redirect == 2) {
					if (temp >= algo_para1[i].high_temp) {
						data->target_pwm = pwm_duty_max;
						continue;
					}

					if ((data->down_delay - temp) <= 6) {
						if (data->last_pwm > data->target_pwm) {
							if (data->target_pwm > pwm_duty_max)
								data->target_pwm = pwm_duty_max;

							if (data->target_pwm < pwm_duty_min) 
								data->target_pwm = pwm_duty_min;
							continue;
						}

						data->target_pwm = data->last_pwm;
						continue;
					}
				}

				if (data->target_pwm > pwm_duty_max)
					data->target_pwm = pwm_duty_max;

				if (data->target_pwm < pwm_duty_min)
					data->target_pwm = pwm_duty_min;
			}
		}

#ifdef THERMAL_SIMULATION
		if (i == sim_index) {
			printk(KERN_ALERT "sensor %d :\n ", i);
			printk(KERN_ALERT "temp=%d,k=%d,in=%d,pwm_duty_max=%d,pwm=%d\n ",
			    temp, k, pwm_duty_min, pwm_duty_max, data->target_pwm);
		}
#endif
	}  // end of for loop

	if (fan_data->fanchip_data.fan_fail > 1)
		fan_data->target_pwm_setting = MAX_PWM; /* max pwm when one or more fan fail */

	set_pwm(client, fan_data->target_pwm_setting * 255 / 100);

#ifdef THERMAL_DEBUG
	printk(KERN_ALERT "Calculated target PWM is [%d]\n", fan_data->target_pwm_setting);
#endif

	// setup PSU-2 PWM setting
	if ((thermalclient[psu2] != NULL) && ((psu_data = i2c_get_clientdata(thermalclient[psu2])) != NULL)) {
		if (fan_data->fanchip_data.fan_fail > 1)
			psu_data->target_pwm = algo_para1[psu2].pwm_duty_max; /* max pwm when two or more fan fail */

		set_psu_pwm(thermalclient[psu2], psu_data->target_pwm);

#ifdef THERMAL_DEBUG
		printk(KERN_ALERT "Setting PSU-2 PWM to [%d]\n", psu_data->target_pwm);
#endif
	}

	// setup PSU-1 PWM setting
	if ((thermalclient[psu1] != NULL) && ((psu_data = i2c_get_clientdata(thermalclient[psu1])) != NULL)) {
		if (fan_data->fanchip_data.fan_fail > 1)
			psu_data->target_pwm = algo_para1[psu1].pwm_duty_max; /* max pwm when two or more fan fail */

		set_psu_pwm(thermalclient[psu1], psu_data->target_pwm);

#ifdef THERMAL_DEBUG
		printk(KERN_ALERT "Setting PSU-1 PWM to [%d]\n", psu_data->target_pwm);
#endif
	}
}


#define TEMP_DETECT_INTERVAL   3000 /*15s interval*/
static void fan_delayed_work(struct work_struct *work)
{
	int i;
	thermal_data *data;

	mutex_lock(&celestica_thermal_mutex);

#ifndef THERMAL_SIMULATION
	lm75_update_device(thermalclient[lm75_ROA]);

	for (i = 0; i < lm75counts; i++) {
		lm75_update_device(thermalclient[i]);
	}

	psu_update_device(thermalclient[psu1]);
	psu_update_device(thermalclient[psu2]);
#endif

	if (fan_chip_type == EMC2305_CHIP) {
		emc2305_update_device(thermalclient[fan_chip]);
	}

	if ((thermalclient[fan_chip] != NULL) &&
		((data = i2c_get_clientdata(thermalclient[fan_chip])) != NULL)) {

#ifdef THERMAL_SIMULATION
		if (sim_temp > 0x3c00) {
			down_flag = 1;
			sim_temp = 0x3c00;

		} else if (sim_temp <= 0x1400) {
			down_flag = 0;
			sim_temp = 0x1400;

			while (1) {
				if (++sim_index >= lm75counts)
					sim_index = 0;

				if (data->lm75_data.fan_control_enable[sim_index] == FAN_CONTROL_ENABLE)
					break;
			}
		}

		data->lm75_data.temp[sim_index] = sim_temp;

		if (down_flag == 0)
			sim_temp += 0x0100; /*increase 1 degree each time*/
		else
			sim_temp -= 0x0100;

		data->fan_algo_enabled = FAN_ALGO_ENABLE;
#endif

		if (FAN_ALGO_ENABLE == data->fan_algo_enabled) {
			calculate_target_pwm(thermalclient[fan_chip]);
		}
	}

	queue_delayed_work(polldev_wq, &delaywork, msecs_to_jiffies(TEMP_DETECT_INTERVAL));
	mutex_unlock(&celestica_thermal_mutex);
}

/*start work queue for fan control algorithm*/
static int start_delay_workqueue(void)
{
	int retval = 0;

	INIT_DELAYED_WORK(&delaywork, fan_delayed_work);

	if (!polldev_users) {
		polldev_wq = create_singlethread_workqueue("celestica_polldevd");

		if (!polldev_wq) {
#ifdef THERMAL_DEBUG
			printk(KERN_ERR " failed to create fan control workqueue\n");
#endif
			retval = -ENOMEM;
			goto out;
		}
	}

	polldev_users++;
out:
	return retval;
}

static int stop_delay_workqueue(void)
{
	cancel_delayed_work_sync(&delaywork);

	if (!--polldev_users)
		destroy_workqueue(polldev_wq);

	return 0;
}

static int  Kick_Fan_Control_Algorithm(void)
{
	int status;

	status = start_delay_workqueue();
	if (status < 0)
		return -1;

	status = queue_delayed_work(polldev_wq, &delaywork, msecs_to_jiffies(TEMP_DETECT_INTERVAL));
	return 0;
}

static int Stop_Fan_Control_Algorithm(void)
{
	stop_delay_workqueue();
	return 0;
}

/* device probe and removal */
static int
thermal_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int status = 0;
	int i, idx;
	unsigned char flag;
	thermal_data *data;

#ifdef THERMAL_DEBUG
	printk("thermal_probe: %s : %02x client=%p adapter=%p driver=%p\n",
		client->name, client->addr, client, client->adapter, client->driver);
#endif

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_SMBUS_BYTE_DATA | I2C_FUNC_SMBUS_WORD_DATA))
		return -EIO;

	// allocate device local data storage for this device
	data = kzalloc(sizeof(thermal_data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	data->fan_algo_enabled = FAN_ALGO_ENABLE;
	i2c_set_clientdata(client, data);

	// find matching device entry for this device address
	for (idx = -1, i = 0; i < num_devices; i++) {
		if (client->addr == normal_i2c[i]) {
			idx = i;
			break;
		}
	}

	/* save device index in instance for quick lookup of device */
	data->idx = idx;

	switch (idx)
	{
	case psu1:
#ifdef CONFIG_REDSTONE
		flag = read_cpld("reset", 0x80);
		if ((flag & 0x20) != 0) {
			printk(NAME " CPLD indicates PSU-1 is not present\n");
			thermalclient[idx] = NULL;
			break;
		}
#else  // SMALLSTONE
		flag = read_cpld("reset", 0x80);
		if ((flag & 0x08) == 0) {
			printk(NAME " CPLD indicates PSU-1 is not present\n");
			thermalclient[idx] = NULL;
			break;
		}

#endif

		printk(NAME " CPLD indicates PSU-1 is present\n");
		thermalclient[idx] = client;
		status = LM75_Init(client);
		break;

	case psu2:
#ifdef CONFIG_REDSTONE
		flag = read_cpld("reset", 0x80);
		if ((flag & 0x10) != 0) {
			printk(NAME " CPLD indicates PSU-2 is not present\n");
			thermalclient[idx] = NULL;
			break;
		}
#else  // SMALLSTONE
		flag = read_cpld("reset", 0x80);
		if ((flag & 0x04) == 0)
		{
			printk(NAME " CPLD indicates PSU-2 is not present\n");
			thermalclient[idx] = NULL;
			break;
		}
#endif

		printk(NAME " CPLD indicates PSU-2 is present\n");
		thermalclient[idx] = client;
		status = LM75_Init(client);
		break;

	case -1:
		printk(NAME " : I2C device not found in device list\n");
		break;

	case fan_chip:
		thermalclient[idx] = client;
		status = Fan_Chip_Init(client);
		status = Kick_Fan_Control_Algorithm();
		break;

	default:
		thermalclient[idx] = client;
		status = LM75_Init(client);
		break;
	}

	return status;
}

static int thermal_remove(struct i2c_client *client)
{
	int i;
	struct i2c_client *tmp_client;
	thermal_data *data;

	Stop_Fan_Control_Algorithm();

	for (i = 0; i < NUM_THERMAL_FILES; i++) {
		class_remove_file(thermal_class, attrgroup[i]);
	}

	for (i = 0; i < NUM_THERMAL_DEBUG_FILES; i++) {
		class_remove_file(thermal_class, attrgroup1[i]);
	}

	// remove all clients previously created
	for (i = 0; i < num_devices; i++) {
		tmp_client = thermalclient[i];

		// if this client exists, delete device instance data
		if ((tmp_client != NULL) && ((data = i2c_get_clientdata(tmp_client)) != NULL)) {
			kfree(data);

			i2c_set_clientdata(tmp_client, NULL);
			i2c_unregister_device(tmp_client);

			thermalclient[i] = NULL;
		}
	}

	return 0;
}

static struct i2c_driver thermal_driver = {
	.class			= I2C_CLASS_HWMON,
	.driver = {
		.name = "thermal"
	},
	.probe			= thermal_probe,
	.remove			= thermal_remove,
	.id_table		= thermal_ids,
	.address_list	= normal_i2c
};

static int __init celestica_thermal_init(void)
{
	int i;
	int status = 0;

	for (i = 0; i < num_devices; i++) {
		thermalclient[i] = NULL;
	}

	thermal_class = class_create(THIS_MODULE, "thermal");
#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME " creating class (thermal\n");
#endif
	if (IS_ERR(thermal_class))
		return PTR_ERR(thermal_class);

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME " creating %d class files\n", NUM_THERMAL_FILES);
#endif
	for (i = 0; i < NUM_THERMAL_FILES; i++) {
		status = class_create_file(thermal_class, attrgroup[i]);
	}

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME " creating %d thermal debug files\n", NUM_THERMAL_DEBUG_FILES);
#endif
	for (i = 0; i < NUM_THERMAL_DEBUG_FILES; i++) {
		status = class_create_file(thermal_class, attrgroup1[i]);
	}

#ifdef THERMAL_DEBUG
	printk( KERN_ALERT NAME " adding (thermal_driver)\n");
#endif
	if (i2c_add_driver(&thermal_driver) < 0)
		printk( KERN_ALERT NAME "detect i2c bus error\n" );

	return (0);
}

static void __exit celestica_thermal_exit(void)
{
	printk( KERN_ALERT NAME ": thermal driver exit\n" );
	Stop_Fan_Control_Algorithm();
	class_destroy(thermal_class);
	i2c_del_driver(&thermal_driver);
}

module_init(celestica_thermal_init);
module_exit(celestica_thermal_exit);
