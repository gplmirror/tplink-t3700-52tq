/*
 * P1010RDB Board Setup
 *
 * Copyright 2011 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>

#include <asm/time.h>
#include <asm/machdep.h>
#include <asm/pci-bridge.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>

#ifdef CONFIG_CH_P1010 
#include <linux/br_cpld.h>
#endif

#include "mpc85xx.h"

void __init p1010_rdb_pic_init(void)
{
	struct mpic *mpic = mpic_alloc(NULL, 0, MPIC_BIG_ENDIAN |
	  MPIC_SINGLE_DEST_CPU,
	  0, 256, " OpenPIC  ");

	BUG_ON(mpic == NULL);

	mpic_init(mpic);
}


/*
 * Setup the architecture
 */
static void __init p1010_rdb_setup_arch(void)
{
	if (ppc_md.progress)
		ppc_md.progress("p1010_rdb_setup_arch()", 0);

	fsl_pci_assign_primary();

	printk(KERN_INFO "P1010 RDB board from Freescale Semiconductor\n");
}

machine_arch_initcall(p1010_rdb, mpc85xx_common_publish_devices);
machine_arch_initcall(p1010_rdb, swiotlb_setup_bus_notifier);

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p1010_rdb_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P1010RDB"))
		return 1;
	return 0;
}

define_machine(p1010_rdb) {
	.name			= "P1010 RDB",
	.probe			= p1010_rdb_probe,
	.setup_arch		= p1010_rdb_setup_arch,
	.init_IRQ		= p1010_rdb_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

#ifdef CONFIG_CH_P1010 
static char ch_model[32];

void *CPLD_ADDR = NULL;
void *GPIO_ADDR = NULL;
extern void brcpld_irq_process_register(void *fptr);

#define GPIO_GPIER 0xc
#define GPIO_GPIER_MASK 0xffffffff

void chp1010_process_cpld_irq(int iLine,int i, void *data,void *irqDataPtr,void *irqStatPtr)
{
	volatile unsigned int  *regAddr1;
	unsigned int data1;
	irqDat *irqData = (irqDat *)irqDataPtr;

	regAddr1 = (volatile unsigned int *) (GPIO_ADDR + GPIO_GPIER);
	data1 = *(volatile unsigned int *) (regAddr1);
	irqData->reg1[i] = data1;
 	*((unsigned int *) (regAddr1))  = (data1 & GPIO_GPIER_MASK);	/* clearing particular bit */

	/* Presently reset interrupt is used/enabled; 
           For Reset button there is no CPLD mask;
	   Other interrupts like SFP, Fan etc have the CPLD mask;
	   For such interrupts enable the below part of code to clear the interrupt.
	 */
#if 0
  irqNos *irqStat = (irqNos *)irqStatPtr;
  data1 = *(volatile unsigned int *) (CPLD_ADDR + irqStat->regOffset1[i]);
#endif
}

static int __init chp1010_cpld_init(void)
{
	struct resource r[2];
	struct device_node *np = NULL;
	int ret, res_num = 0;
	struct platform_device *cpld_dev = NULL;

	/* If the board is not CH P1010, We can ignore this */
	if (strncmp(ch_model, "fsl,P1010", strlen("fsl,P1010"))) {
		return -1;
	}

	np = of_find_node_by_name(np, "cpld");
	if (NULL == np)
	{
		printk(KERN_ERR "chp1010_cpld_init: Could not find CPLD node in DTB\n");	
		return -1;
	}

	ret = of_address_to_resource(np, 0, &r[0]);
	if (ret)
	{
		printk(KERN_ERR "chp1010_cpld_init: Could not get CPLD reg resource from DTB\n");
		return -1; 
	}

	/* map the address into kernel */
	CPLD_ADDR = ioremap(r[0].start, 1 + r[0].end - r[0].start);
	if (NULL == CPLD_ADDR)
	{
		printk (KERN_ERR "chp1010_cpld_init: Could not Map CPLD PhyAddr 0x%x.\n", r[0].start);
		return -1;
	}
	printk (KERN_INFO "chp1010_cpld_init: PhyAddr: 0x%x Size: 0x%x CPLD Mapped at 0x%08x.\n", 
		r[0].start, (r[0].end - r[0].start), (unsigned int)CPLD_ADDR);

	np = of_find_node_by_name(NULL, "gpio-controller");
	if (NULL == np)
	{
		printk(KERN_ERR "chp1010_cpld_init: Could not find gpio-controller node in DTB\n");	
		goto cpld_cleanup;
	}

	ret = of_address_to_resource(np, 0, &r[0]);
	if (ret)
	{
		printk(KERN_ERR "chp1010_cpld_init: Could not get GPIO reg resource from DTB\n");
		goto cpld_cleanup;
	}

	/* map the address into kernel */
	GPIO_ADDR = ioremap(r[0].start, 1 + r[0].end - r[0].start);
	if (NULL == GPIO_ADDR)
	{
		printk (KERN_ERR "chp1010_cpld_init: Could not Map GPIO PhyAddr 0x%x.\n", r[0].start);
		goto cpld_cleanup;
	} 
	printk (KERN_INFO  "chp1010_cpld_init: PhyAddr: 0x%x Size: 0x%x GPIO Mapped at 0x%08x.\n", 
                           r[0].start, (r[0].end - r[0].start), (unsigned int)GPIO_ADDR);

	res_num++;

	cpld_dev = platform_device_register_simple("cpld", 0, r, res_num); /* name MUST be "cpld" */
	if (IS_ERR(cpld_dev)) 
	{
		ret = PTR_ERR(cpld_dev);
		goto gpio_cleanup;
	}

	brcpld_irq_process_register(&chp1010_process_cpld_irq);
	return 0;

gpio_cleanup:
	iounmap(GPIO_ADDR); 
	
cpld_cleanup:
	iounmap(CPLD_ADDR); 
	return -1;
}
late_initcall(chp1010_cpld_init);

static void ch_p1010_restart(char *cmd)
{
	unsigned char *cpldPtr = CPLD_ADDR;
	if (cpldPtr) {
      /*  	local_irq_disable();*/
		out_8(cpldPtr+1, 0x1);
	} else {
		printk (KERN_EMERG "Error: reset control register not mapped."
				"Please power cycle board manually!\n");
	}

        while (1) ;
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init ch_p1010_probe(void)
{
	unsigned long cplen;
	const char *model;
	unsigned long root = of_get_flat_dt_root();

	model = of_get_flat_dt_prop(root, "model", &cplen);
	strncpy(ch_model, model, cplen);

	if (of_flat_dt_is_compatible(root, "fsl,CHP1010RDB")) {
		return 1;
	}
	return 0;
}

machine_arch_initcall(ch_p1010, mpc85xx_common_publish_devices);
machine_arch_initcall(ch_p1010, swiotlb_setup_bus_notifier);

define_machine(ch_p1010) {
       .name                   = "CH P1010",
       .probe                  = ch_p1010_probe,
       .setup_arch             = p1010_rdb_setup_arch,
       .init_IRQ               = p1010_rdb_pic_init,
#ifdef CONFIG_PCI
       .pcibios_fixup_bus      = fsl_pcibios_fixup_bus,
#endif
       .get_irq                = mpic_get_irq,
       .restart                = ch_p1010_restart,
       .calibrate_decr         = generic_calibrate_decr,
       .progress               = NULL,
};

/* Board CHP1011 handlers */
#define CHP1011_CPLD_ADDRESS  0xf0000000

void ch_p1011_restart(char *cmd)
{
        unsigned char *cpldPtr = ioremap(CHP1011_CPLD_ADDRESS, 4096);

        if (cpldPtr) {
      /*        local_irq_disable();*/
                out_8(cpldPtr+1, 0x1);
        } else {
                printk (KERN_EMERG "Error: reset control register not mapped."
                                "Please power cycle board manually!\n");
        }

        while (1) ;
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init ch_p1011_probe(void)
{
	unsigned long cplen;
	const char *model;
	unsigned long root = of_get_flat_dt_root();

	model = of_get_flat_dt_prop(root, "model", &cplen);
	strncpy(ch_model, model, cplen);

	if (of_flat_dt_is_compatible(root, "fsl,CHP1011RDB")) {
		return 1;
	}
	return 0;
}

machine_arch_initcall(ch_p1011, mpc85xx_common_publish_devices);
machine_arch_initcall(ch_p1011, swiotlb_setup_bus_notifier);

define_machine(ch_p1011) {
       .name                   = "CH P1011",
       .probe                  = ch_p1011_probe,
       .setup_arch             = p1010_rdb_setup_arch,
       .init_IRQ               = p1010_rdb_pic_init,
#ifdef CONFIG_PCI
       .pcibios_fixup_bus      = fsl_pcibios_fixup_bus,
#endif
       .get_irq                = mpic_get_irq,
       .restart                = ch_p1011_restart,
       .calibrate_decr         = generic_calibrate_decr,
       .progress               = NULL,
};
#endif
