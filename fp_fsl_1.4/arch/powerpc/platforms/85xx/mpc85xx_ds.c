/*
 * MPC85xx DS Board Setup
 *
 * Author Xianghua Xiao (x.xiao@freescale.com)
 * Roy Zang <tie-fei.zang@freescale.com>
 * 	- Add PCI/PCI Exprees support
 * Copyright 2007 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>

#include <asm/time.h>
#include <asm/machdep.h>
#include <asm/pci-bridge.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/i8259.h>
#include <asm/swiotlb.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>
#include "smp.h"

#include "mpc85xx.h"

#undef DEBUG

#ifdef DEBUG
#define DBG(fmt, args...) printk(KERN_ERR "%s: " fmt, __func__, ## args)
#else
#define DBG(fmt, args...)
#endif

#ifdef CONFIG_PPC_I8259
static void mpc85xx_8259_cascade(unsigned int irq, struct irq_desc *desc)
{
	struct irq_chip *chip = irq_desc_get_chip(desc);
	unsigned int cascade_irq = i8259_irq();

	if (cascade_irq != NO_IRQ) {
		generic_handle_irq(cascade_irq);
	}
	chip->irq_eoi(&desc->irq_data);
}
#endif	/* CONFIG_PPC_I8259 */

void __init mpc85xx_ds_pic_init(void)
{
	struct mpic *mpic;
#ifdef CONFIG_PPC_I8259
	struct device_node *np;
	struct device_node *cascade_node = NULL;
	int cascade_irq;
#endif
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,MPC8572DS-CAMP")) {
		mpic = mpic_alloc(NULL, 0,
			MPIC_NO_RESET |
			MPIC_BIG_ENDIAN |
			MPIC_SINGLE_DEST_CPU,
			0, 256, " OpenPIC  ");
	} else {
		mpic = mpic_alloc(NULL, 0,
			  MPIC_BIG_ENDIAN |
			  MPIC_SINGLE_DEST_CPU,
			0, 256, " OpenPIC  ");
	}

	BUG_ON(mpic == NULL);
	mpic_init(mpic);

#ifdef CONFIG_PPC_I8259
	/* Initialize the i8259 controller */
	for_each_node_by_type(np, "interrupt-controller")
	    if (of_device_is_compatible(np, "chrp,iic")) {
		cascade_node = np;
		break;
	}

	if (cascade_node == NULL) {
		printk(KERN_DEBUG "Could not find i8259 PIC\n");
		return;
	}

	cascade_irq = irq_of_parse_and_map(cascade_node, 0);
	if (cascade_irq == NO_IRQ) {
		printk(KERN_ERR "Failed to map cascade interrupt\n");
		return;
	}

	DBG("mpc85xxds: cascade mapped to irq %d\n", cascade_irq);

	i8259_init(cascade_node, 0);
	of_node_put(cascade_node);

	irq_set_chained_handler(cascade_irq, mpc85xx_8259_cascade);
#endif	/* CONFIG_PPC_I8259 */
}

#ifdef CONFIG_PCI
extern int uli_exclude_device(struct pci_controller *hose,
				u_char bus, u_char devfn);

static struct device_node *pci_with_uli;

static int mpc85xx_exclude_device(struct pci_controller *hose,
				   u_char bus, u_char devfn)
{
	if (hose->dn == pci_with_uli)
		return uli_exclude_device(hose, bus, devfn);

	return PCIBIOS_SUCCESSFUL;
}
#endif	/* CONFIG_PCI */

static void __init mpc85xx_ds_uli_init(void)
{
#ifdef CONFIG_PCI
	struct device_node *node;

	/* See if we have a ULI under the primary */

	node = of_find_node_by_name(NULL, "uli1575");
	while ((pci_with_uli = of_get_parent(node))) {
		of_node_put(node);
		node = pci_with_uli;

		if (pci_with_uli == fsl_pci_primary) {
			ppc_md.pci_exclude_device = mpc85xx_exclude_device;
			break;
		}
	}
#endif
}

/*
 * Setup the architecture
 */
static void __init mpc85xx_ds_setup_arch(void)
{
	if (ppc_md.progress)
		ppc_md.progress("mpc85xx_ds_setup_arch()", 0);

#ifdef CONFIG_PCI
	ppc_md.pci_exclude_device = mpc85xx_exclude_device;
#endif

	swiotlb_detect_4g();
	fsl_pci_assign_primary();
	mpc85xx_ds_uli_init();
	mpc85xx_smp_init();

	printk("MPC85xx DS board from Freescale Semiconductor\n");
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init mpc8544_ds_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "MPC8544DS")) {
		return 1;
	}

	return 0;
}

machine_arch_initcall(mpc8544_ds, mpc85xx_common_publish_devices);
machine_arch_initcall(mpc8572_ds, mpc85xx_common_publish_devices);
machine_arch_initcall(p2020_act, mpc85xx_common_publish_devices);
machine_arch_initcall(p2020_cel, mpc85xx_common_publish_devices);
machine_arch_initcall(p2020_ds, mpc85xx_common_publish_devices);
machine_arch_initcall(p2020_dni, mpc85xx_common_publish_devices);
machine_arch_initcall(p2020_fxc, mpc85xx_common_publish_devices);
machine_arch_initcall(p2020_qcp, mpc85xx_common_publish_devices);
machine_arch_initcall(bcm98548xmc, mpc85xx_common_publish_devices);

machine_arch_initcall(mpc8544_ds, swiotlb_setup_bus_notifier);
machine_arch_initcall(mpc8572_ds, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_act, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_cel, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_ds, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_dni, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_fxc, swiotlb_setup_bus_notifier);
machine_arch_initcall(p2020_qcp, swiotlb_setup_bus_notifier);
machine_arch_initcall(bcm98548xmc, swiotlb_setup_bus_notifier);

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init mpc8572_ds_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,MPC8572DS")) {
		return 1;
	}

	return 0;
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p2020_ds_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P2020DS")) {
		return 1;
	}

	return 0;
}

define_machine(mpc8544_ds) {
	.name			= "MPC8544 DS",
	.probe			= mpc8544_ds_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

define_machine(mpc8572_ds) {
	.name			= "MPC8572 DS",
	.probe			= mpc8572_ds_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

define_machine(p2020_ds) {
	.name			= "P2020 DS",
	.probe			= p2020_ds_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

static int __init p2020_act_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P2020ACT")) {
		return 1;
	}

	return 0;
}

define_machine(p2020_act) {
	.name			= "as5610_52x",
	.probe			= p2020_act_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

void p2020_dni_restart(char *cmd)
{
    struct device_node *np;
    const unsigned int *addr, *reg;
    const unsigned int *val;
    unsigned char *cpld_ptr = 0;

    /* lookup CPLD node information */
	np = of_find_node_by_name(NULL, "cpld");

    if (np != NULL) {
      /* get CPLD address and get virtual address */
      addr = of_get_property(np, "address", NULL);
      reg = of_get_property(np, "reset_reg", NULL);
      val = of_get_property(np, "reset_val", NULL);

      /* if all three parameters found, map CPLD address to kernel space */
      if ((addr != NULL) && (reg != NULL) && (val != NULL))
        cpld_ptr = ioremap(*addr, 0x8000);
    }

    if (cpld_ptr) {
        local_irq_disable();
        out_8(cpld_ptr + *reg, (unsigned char)*val);
    } else {
	    printk (KERN_EMERG "Error: reset control register not mapped."
		    "Please power cycle board manually!\n");
	}

	while (1) ;
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init p2020_dni_probe(void)
{
  	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "fsl,P2020DNI")) {
		return 1;
	}

	return 0;
}

define_machine(p2020_dni) {
	.name			= "P2020 DNI",
	.probe			= p2020_dni_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= p2020_dni_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};


static int __init p2020_cel_probe(void)
{
  	unsigned long root = of_get_flat_dt_root();

	return !!of_flat_dt_is_compatible(root, "fsl,P2020CEL");
}

define_machine(p2020_cel) {
	.name			= "P2020 CEL",
	.probe			= p2020_cel_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};


static int __init p2020_fxc_probe(void)
{
  	unsigned long root = of_get_flat_dt_root();

    if (of_flat_dt_is_compatible(root, "fsl,P2020FXC")) {
        return 1;
    }

    return 0;
}

define_machine(p2020_fxc) {
	.name			= "P2020 FXC",
	.probe			= p2020_fxc_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};


static int __init p2020_qcp_probe(void)
{
  	unsigned long root = of_get_flat_dt_root();

    if (of_flat_dt_is_compatible(root, "fsl,P2020QCP")) {
        return 1;
    }

    return 0;
}

define_machine(p2020_qcp) {
	.name			= "P2020 QCP",
	.probe			= p2020_qcp_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= mpc85xx_ds_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};

//***********************************************************************
// BCM98548XMC Board Functions
// **********************************************************************

void bcm98548xmc_restart(char *cmd)
{
	struct device_node *np;
	const unsigned int *rstcr_reg, *rstdr_reg;
	__be32 __iomem *rstcr = 0;
	__be32 __iomem *rstdr = 0;

	/* lookup reset information */
	np = of_find_node_by_name(NULL, "soc8548");

	if (np != NULL) {
		/* get RESET control and data registers */
		rstcr_reg = of_get_property(np, "rstcr", NULL);
		rstdr_reg = of_get_property(np, "rstdr", NULL);

		/* if both reset control and data registers found, map to kernel space */
		if ((rstcr_reg != NULL) && (rstdr_reg != NULL)) {
		  rstcr = ioremap(get_immrbase() + *rstcr_reg, 0xff);
		  rstdr = ioremap(get_immrbase() + *rstdr_reg, 0xff);
		}
	}

	if (rstcr && rstdr) {
		local_irq_disable();
		out_be32(rstdr, ~0x0);   /* HRESET_REQ */
		out_be32(rstcr, 0x200);  /* HRESET_REQ */
		out_be32(rstdr, ~0x1);   /* HRESET_REQ */
		out_be32(rstdr, ~0x3);   /* HRESET_REQ */

	} else {
		printk (KERN_EMERG "Error: reset control register not mapped."
				"Please power cycle board manually!\n");
	}

	while (1);
}


static void __init bcm98548xmc_pic_init(void)
{
	struct mpic *mpic;
	struct resource r;
	struct device_node *np = NULL;

	np = of_find_node_by_type(np, "open-pic");
	if (np == NULL) {
		printk(KERN_ERR "Could not find open-pic node\n");
		return;
	}

	if (of_address_to_resource(np, 0, &r)) {
		printk(KERN_ERR "Failed to map mpic register space\n");
		of_node_put(np);
		return;
	}

	mpic = mpic_alloc(NULL, 0, MPIC_BIG_ENDIAN | MPIC_SINGLE_DEST_CPU,
		4, 0, " OpenPIC  ");

	BUG_ON(mpic == NULL);

	/* Return the mpic node */
	of_node_put(np);

	mpic_assign_isu(mpic, 0, r.start + 0x10200);
	mpic_assign_isu(mpic, 1, r.start + 0x10280);
	mpic_assign_isu(mpic, 2, r.start + 0x10300);
	mpic_assign_isu(mpic, 3, r.start + 0x10380);
	mpic_assign_isu(mpic, 4, r.start + 0x10400);
	mpic_assign_isu(mpic, 5, r.start + 0x10480);
	mpic_assign_isu(mpic, 6, r.start + 0x10500);
	mpic_assign_isu(mpic, 7, r.start + 0x10580);

	/* Used only for 8548 so far, but no harm in
	 * allocating them for everyone */
	mpic_assign_isu(mpic, 8, r.start + 0x10600);
	mpic_assign_isu(mpic, 9, r.start + 0x10680);
	mpic_assign_isu(mpic, 10, r.start + 0x10700);
	mpic_assign_isu(mpic, 11, r.start + 0x10780);

	/* External Interrupts */
	mpic_assign_isu(mpic, 12, r.start + 0x10000);
	mpic_assign_isu(mpic, 13, r.start + 0x10080);
	mpic_assign_isu(mpic, 14, r.start + 0x10100);

	mpic_init(mpic);
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init bcm98548xmc_probe(void)
{
  	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "BCM98548XMC")) {
		return 1;
	}

	return 0;
}

define_machine(bcm98548xmc) {
	.name			= "BCM98548XMC",
	.probe			= bcm98548xmc_probe,
	.setup_arch		= mpc85xx_ds_setup_arch,
	.init_IRQ		= bcm98548xmc_pic_init,

#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif

	.get_irq		= mpic_get_irq,
	.restart		= bcm98548xmc_restart,
	.calibrate_decr	= generic_calibrate_decr,
	.progress		= udbg_progress,
};

