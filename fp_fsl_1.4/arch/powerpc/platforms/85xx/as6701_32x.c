/*
 * Accton as6701_32x Board Setup
 *
 * Copyright 2009,2012 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>

#include <asm/time.h>
#include <asm/machdep.h>
#include <asm/pci-bridge.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/qe.h>
#include <asm/qe_ic.h>
#include <asm/fsl_guts.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>
#include "smp.h"

#include "mpc85xx.h"

#undef DEBUG

#ifdef DEBUG
#define DBG(fmt, args...) printk(KERN_ERR "%s: " fmt, __func__, ## args)
#else
#define DBG(fmt, args...)
#endif


void __init as6701_32x_pic_init(void)
{
	struct mpic *mpic;
	unsigned long root = of_get_flat_dt_root();

#ifdef CONFIG_QUICC_ENGINE
	struct device_node *np;
#endif

	if (of_flat_dt_is_compatible(root, "fsl,MPC85XXRDB-CAMP")) {
		mpic = mpic_alloc(NULL, 0, MPIC_NO_RESET |
			MPIC_BIG_ENDIAN |
			MPIC_SINGLE_DEST_CPU,
			0, 256, " OpenPIC  ");
	} else {
		mpic = mpic_alloc(NULL, 0,
		  MPIC_BIG_ENDIAN |
		  MPIC_SINGLE_DEST_CPU,
		  0, 256, " OpenPIC  ");
	}

	BUG_ON(mpic == NULL);
	mpic_init(mpic);

#ifdef CONFIG_QUICC_ENGINE
	np = of_find_compatible_node(NULL, NULL, "fsl,qe-ic");
	if (np) {
		qe_ic_init(np, 0, qe_ic_cascade_low_mpic,
				qe_ic_cascade_high_mpic);
		of_node_put(np);

	} else
		pr_err("%s: Could not find qe-ic node\n", __func__);
#endif

}

/*
 * Setup the architecture
 */
static void __init as6701_32x_setup_arch(void)
{
#ifdef CONFIG_QUICC_ENGINE
    struct ccsr_guts __iomem *guts;
    struct device_node *np;
#endif

#if defined(CONFIG_QUICC_ENGINE) && defined(CONFIG_SPI_FSL_SPI)
	struct device_node *qe_spi;
#endif

	if (ppc_md.progress)
		ppc_md.progress("as6701_32x_setup_arch()", 0);

	mpc85xx_smp_init();

	fsl_pci_assign_primary();

#ifdef CONFIG_QUICC_ENGINE
	np = of_find_compatible_node(NULL, NULL, "fsl,qe");
	if (!np) {
		pr_err("%s: Could not find Quicc Engine node\n", __func__);
		goto qe_fail;
	}

	qe_reset();
	of_node_put(np);

	np = of_find_node_by_name(NULL, "par_io");
	if (np) {
		struct device_node *ucc;

		par_io_init(np);
		of_node_put(np);

		for_each_node_by_name(ucc, "ucc")
			par_io_of_config(ucc);

		/* To P1025 QE/TDM, the name of ucc nodes is "tdm@xxxx" */
		for_each_node_by_name(ucc, "tdm")
			par_io_of_config(ucc);
#ifdef CONFIG_SPI_FSL_SPI
		for_each_node_by_name(qe_spi, "spi")
			par_io_of_config(qe_spi);
#endif	/* CONFIG_SPI_FSL_SPI */
	}

	np = of_find_node_by_name(NULL, "global-utilities");
	if (np) {
		guts = of_iomap(np, 0);
		if (!guts)
			pr_err("as6701_32x: could not map global "
					"utilities register\n");
		else {
#if defined(CONFIG_UCC_GETH) || defined(CONFIG_SERIAL_QE)
			if (machine_is(p1025_rdb)) {
				/*
				 * P1025 has pins muxed for QE and other
				 * functions. To enable QE UEC mode, we
				 * need to set bit QE0 for UCC1 in Eth mode,
				 * QE0 and QE3 for UCC5 in Eth mode, QE9
				 * and QE12 for QE MII management singals
				 * in PMUXCR register.
				 */
				setbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(0) |
						MPC85xx_PMUXCR_QE(3) |
						MPC85xx_PMUXCR_QE(9) |
						MPC85xx_PMUXCR_QE(12));
			}
#endif

#ifdef CONFIG_FSL_UCC_TDM
			if (machine_is(p1021_rdb_pc) || machine_is(p1025_rdb)) {

				/* Clear QE12 for releasing the LBCTL */
				clrbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(12));
				/* TDMA */
				setbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(5) |
						  MPC85xx_PMUXCR_QE(11));
				/* TDMB */
				setbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(0) |
						  MPC85xx_PMUXCR_QE(9));
				/* TDMC */
				setbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(0));
				/* TDMD */
				setbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(8) |
						  MPC85xx_PMUXCR_QE(7));
			}
#endif	/* CONFIG_FSL_UCC_TDM */

#ifdef CONFIG_SPI_FSL_SPI
		if (of_find_compatible_node(NULL, NULL, "fsl,mpc8569-qe-spi")) {
			clrbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(12));
			/*QE-SPI*/
			setbits32(&guts->pmuxcr, MPC85xx_PMUXCR_QE(6) |
					  MPC85xx_PMUXCR_QE(9) |
					  MPC85xx_PMUXCR_QE(10));
		}
#endif	/* CONFIG_SPI_FSL_SPI */
			iounmap(guts);
		}
		of_node_put(np);
	}
qe_fail:
#endif	/* CONFIG_QUICC_ENGINE */

	printk(KERN_INFO "AS6701_32X board from Freescale Semiconductor\n");
}

machine_arch_initcall(as6701_32x, mpc85xx_common_publish_devices);

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init as6701_32x_probe(void)
{
	unsigned long root = of_get_flat_dt_root();

	if (of_flat_dt_is_compatible(root, "accton,as6701_32x"))
		return 1;
	return 0;
}

define_machine(as6701_32x) {
	.name			= "as6701_32x",
	.probe			= as6701_32x_probe,
	.setup_arch		= as6701_32x_setup_arch,
	.init_IRQ		= as6701_32x_pic_init,
#ifdef CONFIG_PCI
	.pcibios_fixup_bus	= fsl_pcibios_fixup_bus,
#endif
	.get_irq		= mpic_get_irq,
	.restart		= fsl_rstcr_restart,
	.calibrate_decr		= generic_calibrate_decr,
	.progress		= udbg_progress,
};
