/*
 * MPC85xx Quanta LB8 Board Setup
 *
 * Author Xianghua Xiao (x.xiao@freescale.com)
 * Roy Zang <tie-fei.zang@freescale.com>
 * 	- Add PCI/PCI Exprees support
 * Copyright 2007 Freescale Semiconductor Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/stddef.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/kdev_t.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>

#include <asm/time.h>
#include <asm/machdep.h>
#include <asm/pci-bridge.h>
#include <mm/mmu_decl.h>
#include <asm/prom.h>
#include <asm/udbg.h>
#include <asm/mpic.h>
#include <asm/i8259.h>
#include <asm/swiotlb.h>

#include <sysdev/fsl_soc.h>
#include <sysdev/fsl_pci.h>
#include "smp.h"

#include "mpc85xx.h"

#undef DEBUG

#ifdef DEBUG
#define DBG(fmt, args...) printk(KERN_ERR "%s: " fmt, __func__, ## args)
#else
#define DBG(fmt, args...)
#endif

/*
 * Setup the architecture
 */
static void __init mpc85xx_ds_setup_arch(void)
{
	if (ppc_md.progress)
		ppc_md.progress("qc8548_setup_arch()", 0);

	swiotlb_detect_4g();
	fsl_pci_assign_primary();
	mpc85xx_smp_init();
}

/*
 * Called very early, device-tree isn't unflattened
 */
static int __init mpc85xx_lb8_probe(void)
{
        unsigned long cplen;
        const char *model;
        unsigned long root = of_get_flat_dt_root();

        model = of_get_flat_dt_prop(root, "model", &cplen);
        printk("\nBoard Model Name:  %s\n", model);

        return of_flat_dt_is_compatible(root, "LB8");
}

static void __init mpc85xx_lb8_pic_init(void)
{
        struct mpic *mpic;
        struct resource r;
        struct device_node *np = NULL;

        np = of_find_node_by_type(np, "open-pic");

        if (np == NULL) {
                printk(KERN_ERR "Could not find open-pic node\n");
                return;
        }

        if (of_address_to_resource(np, 0, &r)) {
                printk(KERN_ERR "Failed to map mpic register space\n");
                of_node_put(np);
                return;
        }

        mpic = mpic_alloc(np, 0, MPIC_BIG_ENDIAN | MPIC_SINGLE_DEST_CPU, 4, 0, " OpenPIC  ");
        BUG_ON(mpic == NULL);

        /* Return the mpic node */
        of_node_put(np);

        mpic_assign_isu(mpic, 0, r.start + 0x10200);
        mpic_assign_isu(mpic, 1, r.start + 0x10280);
        mpic_assign_isu(mpic, 2, r.start + 0x10300);
        mpic_assign_isu(mpic, 3, r.start + 0x10380);
        mpic_assign_isu(mpic, 4, r.start + 0x10400);
        mpic_assign_isu(mpic, 5, r.start + 0x10480);
        mpic_assign_isu(mpic, 6, r.start + 0x10500);
        mpic_assign_isu(mpic, 7, r.start + 0x10580);

        mpic_assign_isu(mpic, 8, r.start + 0x10600);
        mpic_assign_isu(mpic, 9, r.start + 0x10680);
        mpic_assign_isu(mpic, 10, r.start + 0x10700);
        mpic_assign_isu(mpic, 11, r.start + 0x10780);

        /* External Interrupts */
        mpic_assign_isu(mpic, 12, r.start + 0x10000);
        mpic_assign_isu(mpic, 13, r.start + 0x10080);
        mpic_assign_isu(mpic, 14, r.start + 0x10100);

        mpic_init(mpic);
}

static void mpc85xx_lb8_show_cpuinfo(struct seq_file *m)
{
	uint pvid, svid, phid1;

	pvid = mfspr(SPRN_PVR);
	svid = mfspr(SPRN_SVR);

	seq_printf(m, "PVR\t\t: 0x%x\n", pvid);
	seq_printf(m, "SVR\t\t: 0x%x\n", svid);

	/* Display cpu Pll setting */
	phid1 = mfspr(SPRN_HID1);
	seq_printf(m, "PLL setting\t: 0x%x\n", ((phid1 >> 24) & 0x3f));
}

#define MPC85xx_LB8_CCSRBAR (0xe0000000)
#define CCSRBAR MPC85xx_LB8_CCSRBAR

void mpc85xx_lb8_restart(char *cmd)
{
	volatile uint *gpoutdr;
	uint8_t __iomem *ccsrbar;

	//ccsrbar = ioremap(CCSRBAR, 1024*1024);
	ccsrbar = ioremap(CCSRBAR + 0xe0000, 4096);
	if (!ccsrbar) {
		printk(KERN_EMERG "lb8_restart: Failed to map CCSRBAR 0x%x\n", CCSRBAR);
	} else {
		//gpoutdr = (uint *)(ccsrbar + 0xe0040);
		gpoutdr = (uint *)(ccsrbar + 0x40);

		printk(KERN_INFO "Restarting Quanta LB8 board... \n");
		udelay(2000); /* Wait for 2ms to let the print message to go */

		/* For reset, set PCI2_AD9 (=GPOUT 14) low. */
		local_irq_disable();

		*gpoutdr &= ~(1<<(31-14));
		udelay(10000);
	}
	printk (KERN_EMERG "Reset didn't happen, spinning! Power Cycle the board manually\n");

	for(;;)
	   ; /* Do nothing */
}

machine_arch_initcall(qc8548_lb8, mpc85xx_common_publish_devices);
machine_arch_initcall(qc8548_lb8, swiotlb_setup_bus_notifier);

define_machine(qc8548_lb8) {
        .name           = "Quanta-qc8548",
        .probe          = mpc85xx_lb8_probe,
        .setup_arch     = mpc85xx_ds_setup_arch,
        .show_cpuinfo   = mpc85xx_lb8_show_cpuinfo,
        .init_IRQ       = mpc85xx_lb8_pic_init,
#ifdef CONFIG_PCI
        .pcibios_fixup_bus = fsl_pcibios_fixup_bus,
#endif
        .get_irq        = mpic_get_irq,
        .restart        = mpc85xx_lb8_restart,
        .calibrate_decr = generic_calibrate_decr,
        .progress       = udbg_progress,
};
