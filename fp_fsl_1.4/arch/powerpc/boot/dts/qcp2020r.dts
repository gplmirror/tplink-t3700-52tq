 /*
 * Quanta LY2R P2020 Device Tree Source
 *
 * Copyright 2011-2012 Freescale Semiconductor Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *	 notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *	 notice, this list of conditions and the following disclaimer in the
 *	 documentation and/or other materials provided with the distribution.
 *     * Neither the name of Freescale Semiconductor nor the
 *	 names of its contributors may be used to endorse or promote products
 *	 derived from this software without specific prior written permission.
 *
 *
 * ALTERNATIVELY, this software may be distributed under the terms of the
 * GNU General Public License ("GPL") as published by the Free Software
 * Foundation, either version 2 of that License or (at your option) any
 * later version.
 *
 * THIS SOFTWARE IS PROVIDED BY Freescale Semiconductor ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Freescale Semiconductor BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/dts-v1/;

/ {
	compatible = "fsl,P2020QCP";
	#address-cells = <0x2>;
	#size-cells = <0x2>;
	interrupt-parent = <0x1>;
	model = "fsl,P2020QCP";

	cpus {
		power-isa-version = "2.03";
		power-isa-b;
		power-isa-e;
		power-isa-atb;
		power-isa-cs;
		power-isa-e.le;
		power-isa-e.pm;
		power-isa-ecl;
		power-isa-mmc;
		power-isa-sp;
		power-isa-sp.fd;
		power-isa-sp.fs;
		power-isa-sp.fv;
		mmu-type = "power-embedded";
		#address-cells = <0x1>;
		#size-cells = <0x0>;

		PowerPC,P2020@0 {
			device_type = "cpu";
			reg = <0x0>;
			next-level-cache = <0x1>;
		};

		PowerPC,P2020@1 {
			device_type = "cpu";
			reg = <0x1>;
			next-level-cache = <0x1>;
		};
	};

	aliases {
		serial0 = "/soc@ffe00000/serial@4500";
		serial1 = "/soc@ffe00000/serial@4600";
		ethernet0 = "/soc@ffe00000/ethernet@24000";
		pci2 = &pci2;
	};

	memory {
		device_type = "memory";
	};

	localbus@ffe05000 {
		#address-cells = <0x2>;
		#size-cells = <0x1>;
		compatible = "fsl,p2020-elbc", "fsl,elbc", "simple-bus";
		reg = <0x0 0xffe05000 0x0 0x1000>;
		interrupts = <0x13 0x2 0x0 0x0>;
		ranges = <0x0 0x0 0x0 0xe8000000 0x08000000>;

		nor@0,0 {
			#address-cells = <1>;
			#size-cells = <1>;
			compatible = "cfi-flash";
			reg = <0x0 0x0 0x8000000>;
			bank-width = <0x2>;
			device-width = <0x1>;

			partition@0 {
				reg = <0x0 0x04000000>;
				label = "jffs2";
			};
			partition@7b60000 {
				reg = <0x7b60000 0x400000>;
				label = "onie";
			};
			partition@7f60000 {
				reg = <0x7f60000 0x20000>;
				label = "u-boot-env";
			};
			partition@7f80000 {
				reg = <0x7f80000 0x80000>;
				label = "u-boot";
			};
		};
	};

	soc@ffe00000 {
		#address-cells = <0x1>;
		#size-cells = <0x1>;
		device_type = "soc";
		compatible = "fsl,p2020-immr", "simple-bus";
		ranges = <0x0 0x0 0xffe00000 0x100000>;
		bus-frequency = <0x0>;

		memory-controller@2000 {
			compatible = "fsl,p2020-memory-controller";
			reg = <0x2000 0x1000>;
			interrupts = <0x12 0x2 0x0 0x0>;
		};

		i2c@3000 {
			#address-cells = <0x1>;
			#size-cells = <0x0>;
			cell-index = <0x0>;
			compatible = "fsl-i2c";
			reg = <0x3000 0x100>;
			interrupts = <0x2b 0x2 0x0 0x0>;
			interrupt-parent = <0x2>;
			dfsrr;

			ddr3@51 {
				compatible = "at,24c02";
				reg = <0x51>;
			};
			rtc@68 {
				compatible = "dallas,ds1339";
				reg = <0x68>;
			};
		};

		i2c@3100 {
			#address-cells = <0x1>;
			#size-cells = <0x0>;
			cell-index = <0x1>;
			compatible = "fsl-i2c";
			reg = <0x3100 0x100>;
			interrupts = <0x2b 0x2>;
			interrupt-parent = <0x2>;
			dfsrr;
		};

   		serial@4500 {
			cell-index = <0x0>;
			device_type = "serial";
			compatible = "fsl,ns16550", "ns16550";
			reg = <0x4500 0x100>;
			clock-frequency = <0x0>;
			interrupts = <0x2a 0x2 0x0 0x0>;
		};
		serial@4600 {
			cell-index = <0x1>;
			device_type = "serial";
			compatible = "fsl,ns16550", "ns16550";
			reg = <0x4600 0x100>;
			clock-frequency = <0x0>;
			interrupts = <0x2a 0x2 0x0 0x0>;
		};

		l2-cache-controller@20000 {
			compatible = "fsl,p2020-l2-cache-controller";
			reg = <0x20000 0x1000>;
			cache-line-size = <0x20>;
			cache-size = <0x80000>;
			cache-sram-addr = <0x80000000>;
			interrupt-parent = <0x2>;
			interrupts = <0x10 0x2>;
			linux,phandle = <0x1>;
		};

		usb@22000 {
			#address-cells = <1>;
			#size-cells = <0>;
			compatible = "fsl-usb2-dr";
			reg = <0x22000 0x1000>;
			interrupt-parent = <0x2>;
			interrupts = <28 0x2>;
			phy_type = "ulpi";
		};


		ethernet@24000 {
			#address-cells = <0x1>;
			#size-cells = <0x1>;
			cell-index = <0x0>;
			device_type = "network";
			model = "eTSEC";
			compatible = "gianfar";
			reg = <0x24000 0x1000>;
			ranges = <0x0 0x24000 0x1000>;
			local-mac-address = [00 00 00 00 00 00];
			interrupts = <29 2 30 2 34 2>;
			interrupt-parent = <0x2>;
			phy-connection-type = "sgmii";
			phy-handle = <&phy0>;
		};

		mdio@24520 {
			#address-cells = <0x1>;
			#size-cells = <0x0>;
			compatible = "fsl,gianfar-mdio";
			reg = <0x24520 0x20>;
			phy0: ethernet-phy@0 {
				reg = <0x1>;
				device_type = "ethernet-phy";
			};
		};

		sdhci@2e000 {
			compatible = "fsl,p2020-esdhc", "fsl,esdhc";
			reg = <0x2e000 0x1000>;
			interrupts = <0x48 0x2>;
			interrupt-parent = <0x2>;
			clock-frequency = <0x0>;
		};

		pic@40000 {
			interrupt-controller;
			#address-cells = <0x0>;
			#interrupt-cells = <0x2>;
			reg = <0x40000 0x40000>;
			compatible = "chrp,open-pic";
			device_type = "open-pic";
			linux,phandle = <0x2>;
		};

		msi@41600 {
			compatible = "fsl,mpic-msi";
			reg = <0x41600 0x80>;
			msi-available-ranges = <0x0 0x100>;
			interrupts = <0xe0 0x0 0xe1 0x0 0xe2 0x0 0xe3 0x0 0xe4 0x0 0xe5 0x0 0xe6 0x0 0xe7 0x0>;
			interrupt-parent = <0x2>;
		};

		global-utilities@e0000 {
			compatible = "fsl,p2020-guts", "fsl,mpc8548-guts";
			reg = <0xe0000 0x1000>;
			fsl,has-rstcr;
		};
	};

	pci2: pcie@ffe0a000 {
		compatible = "fsl,mpc8548-pcie";
		device_type = "pci";
		#interrupt-cells = <0x1>;
		#size-cells = <0x2>;
		#address-cells = <0x3>;
		reg = <0x0 0xffe0a000 0x0 0x1000>;
		bus-range = <0 255>;
		ranges = <0x2000000 0x0 0xc0000000 0x0 0xc0000000 0x0 0x20000000
				0x1000000 0x0 0x0 0x0 0xffc20000 0x0 0x10000>;
		clock-frequency = <33333333>;
		interrupt-parent = <0x2>;
		interrupts = <0x1a 0x2>;
		interrupt-map-mask = <0xf800 0x0 0x0 0x7>;
		interrupt-map = <
			0x0 0x0 0x0 0x1 0x2 0x0 0x1
			0x0 0x0 0x0 0x2 0x2 0x1 0x1
			0x0 0x0 0x0 0x3 0x2 0x2 0x1
			0x0 0x0 0x0 0x4 0x2 0x3 0x1
			>;
		pcie@0 {
			#size-cells = <0x2>;
			#address-cells = <0x3>;
			reg = <0x0 0x0 0x0 0x0 0x0>;
			device_type = "pci";
			ranges = <
				0x2000000 0x0 0xc0000000 0x2000000 0x0
				0xc0000000 0x0 0x20000000 0x1000000 0x0
				0x0 0x1000000 0x0 0x0 0x0 0x10000 >;
		};
	};
};

