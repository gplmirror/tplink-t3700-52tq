/*
 * P2041RDB Device Tree Source
 *
 * Copyright 2011-2012 Freescale Semiconductor Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Freescale Semiconductor nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *
 * ALTERNATIVELY, this software may be distributed under the terms of the
 * GNU General Public License ("GPL") as published by the Free Software
 * Foundation, either version 2 of that License or (at your option) any
 * later version.
 *
 * THIS SOFTWARE IS PROVIDED BY Freescale Semiconductor ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Freescale Semiconductor BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/include/ "fsl/ly5si-pre.dtsi"

/ {
	model = "fsl,P2041RDB";
	compatible = "fsl,P2041RDB";
	#address-cells = <2>;
	#size-cells = <2>;
	interrupt-parent = <&mpic>;

	aliases {
		ethernet0 = "/soc@ffe000000/fman@400000/ethernet@e0000";
		phy_sgmii_1f = "/soc@ffe000000/fman@400000/mdio@e1120/ethernet-phy@1f";
		fman0 = "/soc@ffe000000/fman@400000";
	};

	memory {
		device_type = "memory";
	};

	dcsr: dcsr@f00000000 {
		ranges = <0x00000000 0xf 0x00000000 0x01008000>;
	};

	bportals: bman-portals@ff4000000 {
		ranges = <0x0 0xf 0xf4000000 0x200000>;
	};

	qportals: qman-portals@ff4200000 {
		ranges = <0x0 0xf 0xf4200000 0x200000>;
	};

	soc: soc@ffe000000 {
		ranges = <0x00000000 0xf 0xfe000000 0x1000000>;
		reg = <0xf 0xfe000000 0 0x00001000>;
/*
		spi@110000 {
			flash@0 {
				#address-cells = <1>;
				#size-cells = <1>;
				compatible = "spansion,s25sl12801";
				reg = <0>;
				spi-max-frequency = <40000000>;
				partition@u-boot {
					label = "u-boot";
					reg = <0x00000000 0x00100000>;
					read-only;
				};
				partition@kernel {
					label = "kernel";
					reg = <0x00100000 0x00500000>;
					read-only;
				};
				partition@dtb {
					label = "dtb";
					reg = <0x00600000 0x00100000>;
					read-only;
				};
				partition@fs {
					label = "file system";
					reg = <0x00700000 0x00900000>;
				};
			};
		};
*/
		i2c@118000 {
/*
			lm75b@48 {
				compatible = "nxp,lm75a";
				reg = <0x48>;
			};
			eeprom@50 {
				compatible = "at24,24c256";
				reg = <0x50>;
			};
*/			
			rtc@68 {
				compatible = "dallas,ds1338";
				reg = <0x68>;
			};
			
		};

		i2c@118100 {
/*			
			eeprom@50 {
				compatible = "at24,24c256";
				reg = <0x50>;
			};
*/
		};

//		usb1: usb@211000 {
//			dr_mode = "host";
//		};

		fman@400000 {
			#address-cells = <0x1>;
			#size-cells = <0x1>;
			cell-index = <0x0>;
			compatible = "fsl,fman", "simple-bus";
			ranges = <0x0 0x400000 0x100000>;
			reg = <0x400000 0x100000>;
			clock-frequency = <0x0>;
			interrupts = <0x60 0x2 0x0 0x0 0x10 0x2 0x1 0x1>;

			ethernet@e0000 {
				tbi-handle = <0xc>;
				phy-handle = <0xd>;
				phy-connection-type = "sgmii";
				cell-index = <0x0>;
				compatible = "fsl,fman-1g-mac";
				reg = <0xe0000 0x1000>;
				fsl,port-handles = <0xe 0xf>;
				ptimer-handle = <0x10>;
				linux,phandle = <0x24>;
				phandle = <0x24>;
			};

			mdio@e1120 {
                #address-cells = <0x1>;
                #size-cells = <0x0>;
                compatible = "fsl,fman-tbi";
                reg = <0xe1120 0xee0>;
                interrupts = <0x64 0x1 0x0 0x0>;

                tbi-phy@8 {
					reg = <0x8>;
					device_type = "tbi-phy";
					linux,phandle = <0xc>;
					phandle = <0xc>;
				};

				ethernet-phy@0 {
					reg = <0x0>;
					linux,phandle = <0x1e>;
					phandle = <0x1e>;
				};

				ethernet-phy@1 {
					reg = <0x1>;
					linux,phandle = <0x1a>;
					phandle = <0x1a>;
				};

				ethernet-phy@2 {
					reg = <0x2>;
					linux,phandle = <0xd>;
					phandle = <0xd>;
				};

				ethernet-phy@3 {
					reg = <0x3>;
					linux,phandle = <0x12>;
					phandle = <0x12>;
				};

				ethernet-phy@4 {
					reg = <0x4>;
					linux,phandle = <0x16>;
					phandle = <0x16>;
				};

				ethernet-phy@1c {
					reg = <0x1c>;
				};

				ethernet-phy@1d {
					reg = <0x1d>;
				};

				ethernet-phy@1e {
					reg = <0x1e>;
				};

				ethernet-phy@1f {
					reg = <0x1f>;
				};
			};

			ethernet@e2000 {
				tbi-handle = <0x11>;
				phy-handle = <0x12>;
				phy-connection-type = "sgmii";
				cell-index = <0x1>;
				compatible = "fsl,fman-1g-mac";
				reg = <0xe2000 0x1000>;
				fsl,port-handles = <0x13 0x14>;
				ptimer-handle = <0x10>;
				linux,phandle = <0x25>;
				phandle = <0x25>;
			};

			mdio@e3120 {
				#address-cells = <0x1>;
				#size-cells = <0x0>;
				compatible = "fsl,fman-tbi";
				reg = <0xe3120 0xee0>;
				interrupts = <0x64 0x1 0x0 0x0>;

				tbi-phy@8 {
					reg = <0x8>;
					device_type = "tbi-phy";
					linux,phandle = <0x11>;
					phandle = <0x11>;
				};
			};
		};
	};

	rio: rapidio@ffe0c0000 {
		reg = <0xf 0xfe0c0000 0 0x11000>;

		port1 {
			ranges = <0 0 0xc 0x20000000 0 0x10000000>;
		};
		port2 {
			ranges = <0 0 0xc 0x30000000 0 0x10000000>;
		};
	};

	lbc: loalbus@ffe124000 {
		reg = <0xf 0xfe124000 0 0x1000>;
		/* NOR and NAND Flashes */
		ranges = <0 0 0xf 0xe8000000 0x08000000>;
		flash@0,0 {
			#address-cells = <1>;
			#size-cells = <1>;
			compatible = "cfi-flash";
			reg = <0x0 0x0 0x8000000>;
			bank-width = <2>;
			device-width = <2>;

			partition@0 {
				reg = <0x0 0x04000000>;
				label = "jffs2";
			};
			partition@7f60000 {
				reg = <0x7f60000 0x20000>;
				label = "u-boot-env";
			};
			partition@7f80000 {
				reg = <0x7f80000 0x80000>;
				label = "u-boot";
			};
		};
	};

	pci0: pcie@ffe200000 {
		reg = <0xf 0xfe200000 0 0x1000>;
		ranges = <0x02000000 0 0xe0000000 0xc 0x00000000 0x0 0x20000000
			  0x01000000 0 0x00000000 0xf 0xf8000000 0x0 0x00010000>;
		pcie@0 {
			ranges = <0x02000000 0 0xe0000000
				  0x02000000 0 0xe0000000
				  0 0x20000000

				  0x01000000 0 0x00000000
				  0x01000000 0 0x00000000
				  0 0x00010000>;
		};
	};

	pci1: pcie@ffe201000 {
		reg = <0xf 0xfe201000 0 0x1000>;
		ranges = <0x02000000 0x0 0xe0000000 0xc 0x20000000 0x0 0x20000000
			  0x01000000 0x0 0x00000000 0xf 0xf8010000 0x0 0x00010000>;
		pcie@0 {
			ranges = <0x02000000 0 0xe0000000
				  0x02000000 0 0xe0000000
				  0 0x20000000

				  0x01000000 0 0x00000000
				  0x01000000 0 0x00000000
				  0 0x00010000>;
		};
	};

	pci2: pcie@ffe202000 {
		reg = <0xf 0xfe202000 0 0x1000>;
		ranges = <0x02000000 0 0xe0000000 0xc 0x40000000 0 0x20000000
			  0x01000000 0 0x00000000 0xf 0xf8020000 0 0x00010000>;
		pcie@0 {
			ranges = <0x02000000 0 0xe0000000
				  0x02000000 0 0xe0000000
				  0 0x20000000

				  0x01000000 0 0x00000000
				  0x01000000 0 0x00000000
				  0 0x00010000>;
		};
	};

	fsl,dpaa {
		compatible = "fsl,p2041-dpaa", "fsl,dpaa";

		ethernet@0 {
			compatible = "fsl,p2041-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <0x24>;
		};

		ethernet@1 {
			compatible = "fsl,p2041-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <0x25>;
		};
	};
};

/include/ "fsl/ly5si-post.dtsi"

/include/ "fsl/qoriq-dpaa-res1.dtsi"
